/* eslint-disable camelcase */
'use strict';

const path = require('path');
const gcd = require('../lib/grpc-client-delegate');
const app = require('../../server/server');
const PROTO_ROOT = path.join(process.env.MAUM_ROOT,
  app.settings.grpc.protos.entry);
const GRPC_HOSTS = app.settings.grpc.hosts;
const protoLoader = require('@grpc/proto-loader');
const grpc = require('grpc');
const uuidv1 = require('uuid/v1');

const intentFinder = grpc.loadPackageDefinition(
  protoLoader.loadSync(
    path.join(PROTO_ROOT, 'maum/m2u/router/v3/intentfinder.proto'),
    {
      includeDirs: [PROTO_ROOT],
      keepCase: true,
      longs: String,
      enums: String,
      defaults: true,
      oneofs: true
    }))
  .maum.m2u.router.v3;

/** make entry points **/
module.exports = function (Grpc) {
  function prepareGrpcClientDelegate() {
    let result = gcd.init(PROTO_ROOT, GRPC_HOSTS);

    if (result.error) {
      console.log(
        '\x1b[93m\x1b[41mFailed to prepare gRPC client delegate.\x1b[0m',
        result.error);
    } else {
      console.log('\x1b[32m<< gRPC Client Delegate is READY >>\x1b[0m');
    }
  }

  prepareGrpcClientDelegate();

  function callConsoleFunction(serviceFunc, param) {
    return gcd.callFunction('maum.m2u.console.' + serviceFunc, param);
  }

  /*** DialogAgentManager APIs ***/

  Grpc.getDialogAgentManagerAllList = function (next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/getDialogAgentManagerAllList');
  };

  Grpc.remoteMethod(
    'getDialogAgentManagerAllList', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentManagerInfo = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/getDialogAgentManagerInfo', {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentManagerInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentManagerList = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/getDialogAgentManagerList', {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentManagerList', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.insertDialogAgentManagerInfo = function (DialogAgentManager, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/insertDialogAgentManagerInfo',
      DialogAgentManager);
  };

  Grpc.remoteMethod(
    'insertDialogAgentManagerInfo', {
      accepts: [
        {
          arg: 'DialogAgentManager',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentWithDialogAgentInstanceList = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/getDialogAgentWithDialogAgentInstanceList',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentWithDialogAgentInstanceList', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.updateDialogAgentManagerInfo = function (DialogAgentManager, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/updateDialogAgentManagerInfo',
      DialogAgentManager);
  };

  Grpc.remoteMethod(
    'updateDialogAgentManagerInfo', {
      accepts: [
        {
          arg: 'DialogAgentManager',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.deleteDialogAgentManager = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/deleteDialogAgentManager', {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteDialogAgentManager', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.stopDialogAgentManager = function (KeyList, next) {
    return callConsoleFunction('DialogAgentManagerAdmin/stopDialogAgentManager',
      KeyList);
  };

  Grpc.remoteMethod(
    'stopDialogAgentManager', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.startDialogAgentManager = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/startDialogAgentManager', KeyList);
  };

  Grpc.remoteMethod(
    'startDialogAgentManager', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.restartDialogAgentManager = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentManagerAdmin/restartDialogAgentManager', KeyList);
  };

  Grpc.remoteMethod(
    'restartDialogAgentManager', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** DialogAgent APIs ***/

  Grpc.getDialogAgentAllList = function (next) {
    return callConsoleFunction('DialogAgentAdmin/getDialogAgentAllList');
  };

  Grpc.remoteMethod(
    'getDialogAgentAllList', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentInfo = function (Key, next) {
    return callConsoleFunction('DialogAgentAdmin/getDialogAgentInfo',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentActivationInfoList = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentAdmin/getDialogAgentActivationInfoList', {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentActivationInfoList', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.insertDialogAgentInfo = function (DialogAgentInfo, next) {
    return callConsoleFunction('DialogAgentAdmin/insertDialogAgentInfo',
      DialogAgentInfo);
  };

  Grpc.remoteMethod(
    'insertDialogAgentInfo', {
      accepts: [
        {
          arg: 'DialogAgentInfo',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.updateDialogAgentInfo = function (DialogAgentInfo, next) {
    return callConsoleFunction('DialogAgentAdmin/updateDialogAgentInfo',
      DialogAgentInfo);
  };

  Grpc.remoteMethod(
    'updateDialogAgentInfo', {
      accepts: [
        {
          arg: 'DialogAgentInfo',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.deleteDialogAgent = function (KeyList, next) {
    return callConsoleFunction('DialogAgentAdmin/deleteDialogAgent', {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteDialogAgent', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getRuntimeParameters = function (DialogAgentName, next) {
    return callConsoleFunction('DialogAgentAdmin/getRuntimeParameters',
      DialogAgentName);
  };

  Grpc.remoteMethod(
    'getRuntimeParameters', {
      accepts: [
        {
          arg: 'DialogAgentName',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getUserAttributes = function (DialogAgentName, next) {
    return callConsoleFunction('DialogAgentAdmin/getUserAttributes',
      DialogAgentName);
  };

  Grpc.remoteMethod(
    'getUserAttributes', {
      accepts: [
        {
          arg: 'DialogAgentName',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getExecutableDA = function (next) {
    return callConsoleFunction('DialogAgentAdmin/getExecutableDa');
  };

  Grpc.remoteMethod(
    'getExecutableDA', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.stopDialogAgent = function (KeyList, next) {
    return callConsoleFunction('DialogAgentAdmin/stopDialogAgent', KeyList);
  };

  Grpc.remoteMethod(
    'stopDialogAgent', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.startDialogAgent = function (KeyList, next) {
    return callConsoleFunction('DialogAgentAdmin/startDialogAgent', KeyList);
  };

  Grpc.remoteMethod(
    'startDialogAgent', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.restartDialogAgent = function (KeyList, next) {
    return callConsoleFunction('DialogAgentAdmin/restartDialogAgent', KeyList);
  };

  Grpc.remoteMethod(
    'restartDialogAgent', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** Chatbot APIs ***/

  Grpc.getChatbotAllList = function (next) {
    return callConsoleFunction('ChatbotAdmin/getChatbotAllList');
  };

  Grpc.remoteMethod(
    'getChatbotAllList', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getMyChatbotAllList = function (WorkspaceId, next) {
    return callConsoleFunction('ChatbotAdmin/getMyChatbotAllList', {workspace_id: WorkspaceId});
  };

  Grpc.remoteMethod(
    'getMyChatbotAllList', {
      accepts: [
        {arg: 'WorkspaceId', type: 'number', required: true},
      ],
      returns: {root: true},

      isStatic: true
    }
  );

  Grpc.getChatbotInfo = function (Key, next) {
    return callConsoleFunction('ChatbotAdmin/getChatbotInfo', {name: Key});
  };

  Grpc.remoteMethod(
    'getChatbotInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.insertChatbotInfo = function (Chatbot, next) {
    return callConsoleFunction('ChatbotAdmin/insertChatbotInfo', Chatbot);
  };

  Grpc.remoteMethod(
    'insertChatbotInfo', {
      accepts: [
        {
          arg: 'Chatbot',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.updateChatbotInfo = function (Chatbot, next) {
    return callConsoleFunction('ChatbotAdmin/updateChatbotInfo', Chatbot);
  };

  Grpc.remoteMethod(
    'updateChatbotInfo', {
      accepts: [
        {
          arg: 'Chatbot',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.deleteChatbot = function (KeyList, next) {
    return callConsoleFunction('ChatbotAdmin/deleteChatbot', {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteChatbot', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** Simple Classifier APIs ***/

  Grpc.getSimpleClassifierAllList = function (next) {
    return callConsoleFunction(
      'SimpleClassifierAdmin/getSimpleClassifierAllList');
  };

  Grpc.remoteMethod(
    'getSimpleClassifierAllList', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getSimpleClassifierInfo = function (Key, next) {
    return callConsoleFunction('SimpleClassifierAdmin/getSimpleClassifierInfo',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getSimpleClassifierInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.insertSimpleClassifierInfo = function (SimpleClassifier, next) {
    return callConsoleFunction(
      'SimpleClassifierAdmin/insertSimpleClassifierInfo', SimpleClassifier);
  };

  Grpc.remoteMethod(
    'insertSimpleClassifierInfo', {
      accepts: [
        {
          arg: 'SimpleClassifier',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.updateSimpleClassifierInfo = function (SimpleClassifier, next) {
    return callConsoleFunction(
      'SimpleClassifierAdmin/updateSimpleClassifierInfo', SimpleClassifier);
  };

  Grpc.remoteMethod(
    'updateSimpleClassifierInfo', {
      accepts: [
        {
          arg: 'SimpleClassifier',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.deleteSimpleClassifier = function (KeyList, next) {
    return callConsoleFunction('SimpleClassifierAdmin/deleteSimpleClassifier',
      {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteSimpleClassifier', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** Dialog Agent Instance APIs ***/

  Grpc.getDialogAgentInstanceAllList = function (next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/getDialogAgentInstanceAllList');
  };

  Grpc.remoteMethod(
    'getDialogAgentInstanceAllList', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentInstanceByChatbotName = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/getDialogAgentInstanceByChatbotName',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentInstanceByChatbotName', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentInstanceInfo = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/getDialogAgentInstanceInfo', {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentInstanceInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.getDialogAgentInstanceResourceList = function (Key, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/getDialogAgentInstanceResourceList',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getDialogAgentInstanceResourceList', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.insertDialogAgentInstanceInfo = function (DialogAgentInstance, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/insertDialogAgentInstanceInfo',
      DialogAgentInstance);
  };

  Grpc.remoteMethod(
    'insertDialogAgentInstanceInfo', {
      accepts: [
        {
          arg: 'DialogAgentInstance',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.updateDialogAgentInstanceInfo = function (DialogAgentInstance, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/updateDialogAgentInstanceInfo',
      DialogAgentInstance);
  };

  Grpc.remoteMethod(
    'updateDialogAgentInstanceInfo', {
      accepts: [
        {
          arg: 'DialogAgentInstance',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.deleteDialogAgentInstance = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/deleteDialogAgentInstance', {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteDialogAgentInstance', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.stopDialogAgentInstance = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/stopDialogAgentInstance', KeyList);
  };

  Grpc.remoteMethod(
    'stopDialogAgentInstance', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.startDialogAgentInstance = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/startDialogAgentInstance', KeyList);
  };

  Grpc.remoteMethod(
    'startDialogAgentInstance', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.restartDialogAgentInstance = function (KeyList, next) {
    return callConsoleFunction(
      'DialogAgentInstanceAdmin/restartDialogAgentInstance', KeyList);
  };

  Grpc.remoteMethod(
    'restartDialogAgentInstance', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );


  /**
   * CustomScript Models 조회
   * @param next
   * @returns {*} =>  CustomScriptResultList
   */
  Grpc.getCustomScriptModels = function (next) {
    return callConsoleFunction('CustomScriptAdmin/getCustomScriptModels')
  };

  Grpc.remoteMethod(
    'getCustomScriptModels', {
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * CustomScript Categories 조회
   * @param  CustomScript 모델 값
   * @param next
   * @returns {*} => CustomScriptResultList
   */
  Grpc.getCustomScriptCategories = function (Key, next) {
    return callConsoleFunction('CustomScriptAdmin/getCustomScriptCategories', Key)
  };

  Grpc.remoteMethod(
    'getCustomScriptCategories', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * DNN model list 조회
   * @param next
   * @returns {*} => DnnModelList
   */
  Grpc.getDnnModels = function (next) {
    return callConsoleFunction('DnnAdmin/getDnnModels');
  };

  Grpc.remoteMethod(
    'getDnnModels', {
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * DNN model Categories 조회
   * @param  DNN 모델 값
   * @param next
   * @returns {*} => DnnModelCategories
   */
  Grpc.getDnnCategories = function (Model, next) {
    return callConsoleFunction('DnnAdmin/getDnnCategories', Model);
  };

  Grpc.remoteMethod(
    'getDnnCategories', {
      accepts: [
        {arg: 'Model', type: 'object', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** Intent Finder Policy APIs ***/

  /**
   * IntentFinderPolicy의 전체 리스트 호출
   * @param next
   * @returns {*} => {IntentFinderPolicyList}
   */
  Grpc.getIntentFinderPolicyAllList = function (next) {
    return callConsoleFunction(
      'IntentFinderPolicyAdmin/getIntentFinderPolicyAllList');
  };

  Grpc.remoteMethod(
    'getIntentFinderPolicyAllList', {
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinder Policy Name으로 조회
   * @param {*} Key IntentFinder Policy Name 값
   * @param {*} next => {IntentFinderPolicy}
   */
  Grpc.getIntentFinderPolicyInfo = function (Key, next) {
    return callConsoleFunction(
      'IntentFinderPolicyAdmin/getIntentFinderPolicyInfo',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getIntentFinderPolicyInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinderPolicy Insert API
   * @param IntentFinderPolicy
   * @param next
   * @returns {*} => Insert 요청한 IntentFinderPolicy 객체
   */
  Grpc.insertIntentFinderPolicyInfo = function (IntentFinderPolicy, next) {
    return callConsoleFunction(
      'IntentFinderPolicyAdmin/insertIntentFinderPolicyInfo',
      IntentFinderPolicy);
  };

  Grpc.remoteMethod(
    'insertIntentFinderPolicyInfo', {
      accepts: [
        {
          arg: 'IntentFinderPolicy',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinderPolicy Update
   * @param IntentFinderPolicy 객체
   * @param next
   * @returns {*} => param으로 요청 보낸 IntentFinderPolicy 객체
   */
  Grpc.updateIntentFinderPolicyInfo = function (IntentFinderPolicy, next) {
    return callConsoleFunction(
      'IntentFinderPolicyAdmin/updateIntentFinderPolicyInfo',
      IntentFinderPolicy);
  };

  Grpc.remoteMethod(
    'updateIntentFinderPolicyInfo', {
      accepts: [
        {
          arg: 'IntentFinderPolicy',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinderPolicyList Delete
   * @param KeyList => {삭제할 IntentFinderPolicy name 을 List로 가짐}
   * @param next
   */
  Grpc.deleteIntentFinderPolicies = function (KeyList, next) {
    return callConsoleFunction(
      'IntentFinderPolicyAdmin/deleteIntentFinderPolicies', {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteIntentFinderPolicies', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** IntentFinder Instance APIs ***/

  /**
   * IntentFinder Instance List 조회
   */
  Grpc.getIntentFinderInstanceList = function (next) {
    return callConsoleFunction(
      'IntentFinderInstanceAdmin/getIntentFinderInstanceList');
  };

  Grpc.remoteMethod(
    'getIntentFinderInstanceList', {
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinder Instance Name으로 조회
   * @param {*} Key IntentFinder Instance Name 값
   * @param {*} next
   */
  Grpc.getIntentFinderInstanceInfo = function (Key, next) {
    return callConsoleFunction(
      'IntentFinderInstanceAdmin/getIntentFinderInstanceInfo',
      {name: Key});
  };

  Grpc.remoteMethod(
    'getIntentFinderInstanceInfo', {
      accepts: [
        {arg: 'Key', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinder Instance 등록
   * @param {*} IntentFinderInstance 객체
   * @param {*} next
   */
  Grpc.insertIntentFinderInstance = function (IntentFinderInstance, next) {
    return callConsoleFunction(
      'IntentFinderInstanceAdmin/insertIntentFinderInstance',
      IntentFinderInstance);
  };

  Grpc.remoteMethod(
    'insertIntentFinderInstance', {
      accepts: [
        {
          arg: 'IntentFinderInstance',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinder Instance 수정
   * @param {*} IntentFinderInstance 객체
   * @param {*} next
   */
  Grpc.updateIntentFinderInstance = function (IntentFinderInstance, next) {
    return callConsoleFunction(
      'IntentFinderInstanceAdmin/updateIntentFinderInstance',
      IntentFinderInstance);
  };

  Grpc.remoteMethod(
    'updateIntentFinderInstance', {
      accepts: [
        {
          arg: 'IntentFinderInstance',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinder Instance 복수 삭제
   * @param {*} KeyList 삭제할 Instance Name List
   * @param {*} next
   */
  Grpc.deleteIntentFinderInstances = function (KeyList, res) {
    return callConsoleFunction(
      'IntentFinderInstanceAdmin/deleteIntentFinderInstances',
      {key_list: KeyList});
  };

  Grpc.remoteMethod(
    'deleteIntentFinderInstances', {
      accepts: [
        {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * IntentFinder Test (findIntent API)
   * find* 로 시작하는 Method를 만들시 loopback 에서 run sdk 실행시 return 타입이 Observable<Grpc>로
   * 생기는 문제 발생하여 findIntent => proveIntent로 변경
   * @param FindIntentRequest => {FindIntentRequest + 접속하려는 host 주소}
   * @param next
   * @returns {boolean} => {FindIntentResponse}
   */
  Grpc.proveIntent = function (FindIntentRequest, next) {
    return new Promise((resolve, reject) => {
      let client = new intentFinder.IntentFinder(FindIntentRequest.host,
        grpc.credentials.createInsecure());

      let request = {
        utter: FindIntentRequest.utter,
        chatbot: 'test',
        exclude_skills: [],
        skip_steps: []
      };
      let metadata = new grpc.Metadata();
      metadata.add('x-operation-sync-id', uuidv1());

      client.findIntent(request, metadata, function (err, res) {
        let result;
        if (err) {
          if (err.statusCode) {
            result = {code: err.statusCode, detail: err.message};
          } else if (err.code && err.message) {
            result = {code: err.code, detail: `gRPC:${err.message}`};
          } else {
            result = {code: 9004, detail: 'gRPC sync call failed.'};
          }
          reject(result);
        } else {
          resolve(res);
        }
      });
    });
  };


  Grpc.remoteMethod(
    'proveIntent', {
      accepts: [
        {
          arg: 'FindIntentRequest',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** SupervisorMonitor APIs ***/
  Grpc.getSupervisorServerGroupList = function (next) {
    return gcd.callFunction(
      'maum.supervisor.SupervisorMonitor/getSupervisorServerGroupList');
  };
  Grpc.remoteMethod(
    'getSupervisorServerGroupList', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.startSupervisorProcess = function (supervisorProcessKey, next) {
    return gcd.callFunction(
      'maum.supervisor.SupervisorMonitor/startSupervisorProcess',
      supervisorProcessKey);
  };
  Grpc.remoteMethod(
    'startSupervisorProcess', {
      accepts: [
        {
          arg: 'SupervisorProcessKey',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.stopSupervisorProcess = function (supervisorProcessKey, next) {
    return gcd.callFunction(
      'maum.supervisor.SupervisorMonitor/stopSupervisorProcess',
      supervisorProcessKey);
  };
  Grpc.remoteMethod(
    'stopSupervisorProcess', {
      accepts: [
        {
          arg: 'SupervisorProcessKey',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** Facade APIs ***/

  Grpc.getFacadeChatbots = function (next) {
    return gcd.callFunction('maum.m2u.facade.ChatbotFinder/getChatbots');
  };

  Grpc.remoteMethod(
    'getFacadeChatbots', {
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.openDialogService = function (Caller, next) {
    return gcd.callFunction('maum.m2u.facade.DialogService/open', Caller);
  };
  Grpc.remoteMethod(
    'openDialogService', {
      accepts: [
        {arg: 'Caller', type: 'object', required: true, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.sendSimpleTextTalk = function (args, next) {
    return gcd.callFunction('maum.m2u.facade.DialogService/simpleTextTalk',
      args.TextUtter, args.Metadata);
  };
  Grpc.remoteMethod(
    'sendSimpleTextTalk', {
      accepts: [
        {
          arg: 'TextUtter',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
        {
          arg: 'Metadata',
          type: 'object',
          required: false,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.sendTalkAnalyze = function (args, next) {
    return gcd.callFunction('maum.m2u.facade.TalkService/analyze',
      args.TalkQuery, args.Metadata);
  };
  Grpc.remoteMethod(
    'sendTalkAnalyze', {
      accepts: [
        {
          arg: 'TalkQuery',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
        {
          arg: 'Metadata',
          type: 'object',
          required: false,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Grpc.closeDialogService = function (SessionKey, next) {
    return gcd.callFunction('maum.m2u.facade.DialogService/close', SessionKey);
  };
  Grpc.remoteMethod(
    'closeDialogService', {
      accepts: [
        {
          arg: 'SessionKey',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /*** TA HMD Model APIs ***/

  /**
   * HMD Model 전체 조회 API
   * @param next
   * @returns {*} => HMDModelList
   */
  Grpc.getHMDModels = function (next) {
    return gcd.callFunction('maum.brain.hmd.HmdClassifier/getModels');
  };

  Grpc.remoteMethod(
    'getHMDModels', {
      returns: {root: true},
      isStatic: true
    }
  );

  /*** STT Model APIs ***/
  Grpc.getSTTModels = function (next) {
    return gcd.callFunction('maum.brain.stt.SttModelResolver/getModels');
  };

  Grpc.remoteMethod(
    'getSTTModels', {
      returns: {root: true},
      isStatic: true
    }
  );

  /*** NLP3 APIs ***/

  /**
   * NLP3 NaturalLanguageProcessingService의 Analyze Grpc 요청
   * @param InputText
   * @param next
   * @returns {*}
   */
  Grpc.analyze = function (InputText, next) {
    return gcd.callFunction(
      'maum.brain.nlp.NaturalLanguageProcessingService/analyze', InputText);
  };

  Grpc.remoteMethod(
    'analyze', {
      accepts: [
        {
          arg: 'Document',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * NLP3 NaturalLanguageProcessingService의 GetPosTaggedList Grpc 요청
   * @param Document
   * @param next
   * @returns {*}
   */
  Grpc.getPosTaggedList = function (Document, next) {
    return gcd.callFunction(
      'maum.brain.nlp.NaturalLanguageProcessingService/getPosTaggedList',
      Document);
  };

  Grpc.remoteMethod(
    'getPosTaggedList', {
      accepts: [
        {
          arg: 'Document',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * NLP3 NaturalLanguageProcessingService의 GetNerTaggedList Grpc 요청
   * @param Document
   * @param next
   * @returns {*}
   */
  Grpc.getNerTaggedList = function (Document, next) {
    return gcd.callFunction(
      'maum.brain.nlp.NaturalLanguageProcessingService/getNerTaggedList',
      Document);
  };

  Grpc.remoteMethod(
    'getNerTaggedList', {
      accepts: [
        {
          arg: 'Document',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * NLP3 NaturalLanguageProcessingService의 GetPosNerTaggedList Grpc 요청
   * @param Document
   * @param next
   * @returns {*}
   */
  Grpc.getPosNerTaggedList = function (Document, next) {
    return gcd.callFunction(
      'maum.brain.nlp.NaturalLanguageProcessingService/getPosNerTaggedList',
      Document);
  };

  Grpc.remoteMethod(
    'getPosNerTaggedList', {
      accepts: [
        {
          arg: 'Document',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
      ],
      returns: {root: true},
      isStatic: true
    }
  );
};
