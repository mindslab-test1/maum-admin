'use strict';

module.exports = function(Group) {
  Group.observe('before save', (ctx, next) => {
    let ConsoleUser = Group.app.models.ConsoleUser;

    if (ctx.hasOwnProperty('data')) {
      ctx.data.lastUpdated = Date.now();
    }

    if (ctx.isNewInstance) {
      if (ctx.instance._skipCreate) {
        return next();
      }

      //사용자 이름이 생성가능한 이름인지 체크한다.
      if (!ConsoleUser.isValidName(ctx.instance.name)) {
        let err = new Error('This group name is invalid', ctx.instance);
        err.status = 430;
        err.data = ctx.instance;
        return next(err, null);
      }

      //사용자 이름 허용금지어를 체크한다.
      if (ConsoleUser.blackList.indexOf(ctx.instance.name) !== -1) {
        let err = new Error('This group name is blocked to use.', ctx.instance);
        err.status = 431;
        err.data = ctx.instance;
        return next(err, null);
      }

      // 사용자 이름에서 username 변환하여 가져온다.
      ctx.instance.name = ConsoleUser.getUsername(ctx.instance.name);

      let Workspace = ConsoleUser.app.models.Workspace;
      Workspace.findOne({where: {path: ctx.instance.name}}, (error, obj) => {
        if (error) {
          console.warn('in checking workspace', error);
          let err = new Error('Error in checking workspace duplication.', ctx.instance);
          err.status = 432;
          err.data = ctx.instance;
          return next(err, null);
        }
        if (obj) {
          console.info('duplicated workspace', obj);
          let err = new Error('This group name is already used by others', ctx.instance);
          err.status = 432;
          err.data = ctx.instance;
          return next(err, null);
        }
      });
    }

    next();
  });

  Group.observe('after save', (ctx, next) => {
    if (ctx.isNewInstance) {
      // UserImpl 생성시 Wallet의 포인트들도 생성해준다
      let Workspace = Group.app.models.Workspace;
      Workspace.create({
        ownerType: Workspace.GroupType,
        ownerId: ctx.instance.id,
        path: ctx.instance.name
      }, (err, ws) => {
        if (err) {
          console.log(`Error while creating workspacec of group ${ctx.instance.name}`);
          console.warn(err);
          return next(err, null);
        } else {
          console.log('create ws', ws);
          let GroupMember = Group.app.models.GroupMember;
          GroupMember.create({
            groupId: ctx.instance.id,
            userId: ctx.instance.ownerId,
            state: 'accepted',
            role: 'owner',
          }, (err, member) => {
            if (err) {
              console.log(`Error while creating workspacec of group ${ctx.instance.name}`);
              console.warn(err);
              return next(err, null);
            } else {
              console.debug(member);
              return next(null, member);
            }
          });
        }
      });
    } else {
      return next();
    }
  });
};
