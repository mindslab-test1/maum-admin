'use strict';

module.exports = function(Workspace) {
  Workspace.UserType = 'ConsoleUser';
  Workspace.GroupType = 'Group';
};
