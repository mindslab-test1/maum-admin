'use strict';
const bcrypt = require('bcryptjs');
const models = require('../server').models;
//const app = require('../server').app;

const appServer = require('../server');

const aDayInMilliseconds = 24 * 60 * 60 * 1000;

module.exports = function(ConsoleUser) {
  let connector;

  ConsoleUser.blackList = [
    // 'admin',
    'ghost',
    'dashboard',
    'files',
    'groups',
    'help',
    'profile',
    'projects',
    'search',
    'public',
    'assets',
    'u',
    's',
    'teams',
    'chatbot',
    'chatbots',
    'intent-finders',
    'dialog',
    'dialog-agent',
    'dialog-agents',
    'dialog-agent-instances',
    'services',
    'repository',
    'chatbot-builder',
    'auth',
    'signin',
    'ws',
    'market',
    'markets',
    'product',
    'products',
    'workspace',
    'workspaces',
    'info',
    'test',
    'ci',
    'build',
    'builds',
    'qa',
    'quality',
    'create',
    'products',
    'ping',
  ];

  /**
   * 사용자 Name을 regex로 변환하여 username을 얻는다.
   * 예: !!@@somename  =>  somename
   *
   * @param {string} name
   * @return {string|null} 사용자 이름이 변환된 상태로 반환횐다.
   */
  ConsoleUser.getUsername = function(name) {
    //먼저 문자열의 모든 공백을 제거한다.
    name = name.replace(/\s/gi, '');

    //사용자 이름 변환
    var NAMESPACE_REGEX_STR =
      /(?:[a-zA-Z0-9_\.][a-zA-Z0-9_\-\.]*[a-zA-Z0-9_\-]|[a-zA-Z0-9_])/;

    var ret = NAMESPACE_REGEX_STR.exec(name);
    if (ret === null) {
      return null;
    } else {
      return ret[0];
    }
  };

  /**
   * 사용자 Name 이 URL에서 허용 가능한 네임스페이스인지 검사한다.
   *
   * @param {string} name
   * @return {boolean} 이름이 정상적인지 확인한다.
   */
  ConsoleUser.isValidName = function(name) {
    //네임스페이스 허용문구 테스트
    const NAMESPACE_NAME_REGEX = /[\p{Alnum}\p{Pd}_.]*/;
    return NAMESPACE_NAME_REGEX.test(name);
  };

  ConsoleUser.observe('before save', (ctx, next) => {
    if (ctx.hasOwnProperty('data')) {
      ctx.data.lastUpdated = Date.now();
    }

    if (ctx.isNewInstance) {
      if (ctx.instance._skipCreate) {
        return next();
      }

      //사용자 이름이 생성가능한 이름인지 체크한다.
      if (!ConsoleUser.isValidName(ctx.instance.username)) {
        let err = new Error('This username is invalid', ctx.instance);
        err.status = 430;
        err.data = ctx.instance;
        return next(err, null);
      }

      //사용자 이름 허용금지어를 체크한다.
      if (ConsoleUser.blackList.indexOf(ctx.instance.username) !== -1) {
        let err = new Error('This username is blocked to use.', ctx.instance);
        err.status = 431;
        err.data = ctx.instance;
        return next(err, null);
      }

      //사용자 이름에서 username 변환하여 가져온다.
      ctx.instance.username = ConsoleUser.getUsername(ctx.instance.username);

      let Workspace = ConsoleUser.app.models.Workspace;
      Workspace.findOne({where: {path: ctx.instance.username}}, (error, obj) => {
        if (error) {
          console.warn('in checking workspace', error);
          let err = new Error('Error in checking workspace duplication.', ctx.instance);
          err.status = 432;
          err.data = ctx.instance;
          return next(err, null);
        }
        if (obj) {
          console.info('duplicated workspace', obj);
          let err = new Error('This username is already used by others', ctx.instance);
          err.status = 432;
          err.data = ctx.instance;
          return next(err, null);
        }
      });
    }

    next();
  });

  ConsoleUser.observe('after save', (ctx, next) => {
    return next();
    /*if (ctx.isNewInstance) {
      // UserImpl 생성시 Wallet의 포인트들도 생성해준다
      let Workspace = ConsoleUser.app.models.Workspace;
      Workspace.create({
        ownerType: Workspace.UserType,
        ownerId: ctx.instance.id,
        path: ctx.instance.username
      }, (err, obj) => {
        if (err) {
          console.log(`Error while creating workspacec of user ${ctx.instance.username}`)
          console.warn(err);
        } else {
          console.debug(obj);
        }
        return next();
      });
    } else {
      return next();
    }*/
  });


  ConsoleUser.afterRemote('login', function(ctx, auth, next) {
    models.SignInHistory.create(
      Object.assign({
        username: ctx.args.credentials.username,
        signed: true
      }, getRemoteInformation(ctx)));
    next();
  });

  ConsoleUser.afterRemoteError('login', function(ctx, next) {
    models.SignInHistory.create(
      Object.assign({
        username: ctx.args.credentials.username,
        signed: false,
        msg: ctx.error.message
      }, getRemoteInformation(ctx)));
    next();
  });

  function getRemoteInformation(ctx) {
    return {
      remote: ctx.req.connection.remoteAddress ||
        ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  }

  ConsoleUser.beforeRemote('changePassword', function(ctx /*, auth , next*/) {
    return new Promise((resolve, reject) => {
      models.PasswordHistory.find({where: {userId: ctx.args.id}},
        (err, result) => {
          if (result) {
            if (result.some(entry => {
              return bcrypt.compareSync(ctx.args.newPassword, entry.password);
            })) {
              const err = new Error('Password used too recently');
              Object.assign(err, {
                code: 'RECENT_PASSWORD',
                statusCode: 400,
              });
              reject(err);
            } else {
              ConsoleUser.findById(ctx.args.id, (err, result) => {
                if (result) {
                  ctx.hashedValue = result.password;
                }
              });
              resolve();
            }
          } else {
            reject(err);
          }
        });
    });
  });

  ConsoleUser.afterRemote('changePassword', function(ctx, auth, next) {
    if (ctx.args.hasOwnProperty('oldPassword')) {
      const userId = ctx.args.id;
      const filter = {
        where: {userId},
        order: 'lastUpdated ASC'
      };
      models.PasswordHistory.find(filter, (err, result) => {
        if (result) {
          if (result.length < 5) {
            let entry = Object.assign({
              userId,
              password: ctx.hashedValue
            }, getRemoteInformation(ctx));
            models.PasswordHistory.create(entry);
          } else {
            let entry = Object.assign({
              password: ctx.hashedValue,
              lastUpdated: Date.now()
            }, getRemoteInformation(ctx));
            models.PasswordHistory.update({id: result[0].id}, entry);
          }
        }
      });
    }
    next();
  });

  ConsoleUser.isPasswordStale = function(UserId, FreshDaysBy, next) {
    if (typeof FreshDaysBy !== 'number') {
      FreshDaysBy = 90;
    }
    return new Promise((resolve, reject) => {
      ConsoleUser.findById(UserId, (err, result) => {
        if (result) {
          let gap = (Date.now() - result.lastUpdated.valueOf()) /
            aDayInMilliseconds;
          resolve(gap < FreshDaysBy);
        } else {
          resolve(false);
        }
      });
    });
  };

  ConsoleUser.remoteMethod(
    'isPasswordStale',
    {
      accepts: [
        {arg: 'UserId', type: 'string', required: true},
        {arg: 'FreshDaysBy', type: 'number', required: false},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.getUserRoles = function(UserId, next) {
    return new Promise((resolve, reject) => {
      let query = {
        and: [{principalType: 'USER'}, {principalId: UserId}]
      };
      models.RoleMapping.find({where: query}, (err, mappings) => {
        if (err) {
          reject(err);
        } else {
          resolve(mappings.map(item => item.roleId));
        }
      });
    })
      .then(roleIds => new Promise((resolve, reject) => {
        if (roleIds.length < 1) {
          resolve({});
          return;
        }
        let ids = roleIds.map(id => {
          return {id};
        });
        let where = {where: {or: ids}};
        models.Role.find(where, (err, roles) => {
          if (err) {
            reject(err);
          }
          let _roles = {};
          roles.forEach(role => _roles[role.name] = true);
          resolve(_roles);
        });
      }));
  };

  ConsoleUser.remoteMethod(
    'getUserRoles',
    {
      accepts: [
        {arg: 'UserId', type: 'string', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.getCurrentRoles = function(next) {
    return ConsoleUser.getUserRoles(ConsoleUser.app.userId, next);
  };

  ConsoleUser.remoteMethod(
    'getCurrentRoles',
    {
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.getRoleList = function(next) {
    return new Promise(
      resolve => resolve(require('../lib/acl-assets').roleList));
  };

  ConsoleUser.remoteMethod(
    'getRoleList',
    {
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.createWithRoles = function(Args, next) {
    let Account = Args.Account;
    let Roles = Args.Roles;

    return new Promise((resolve, reject) => {
      ConsoleUser.create(Account, (err, user) => {
        if (err) {
          reject(err);
        } else {
          resolve(user);
        }
      });
    })
      .then(user => new Promise((resolve, reject) => {
        if (Roles.length < 1) {
          resolve();
          return;
        }
        let where = {
          where: {
            or: Object.keys(Roles).filter(key => Roles[key] === true)
              .map(key => {
                return {name: key};
              })
          }
        };
        models.Role.find(where, (err, roles) => {
          if (err) {
            reject(err);
            return;
          }
          resolve({userId: user.id, roles});
        });
      }))
      .then(ws => {
        let roleMappings = [];
        ws.roles.forEach(role => {
          roleMappings.push(
            new Promise((resolve, reject) =>
              role.principals.create({
                principalType: 'USER',
                principalId: ws.userId
              }, (err, principal) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(principal);
              })
            )
          );
        });
        return Promise.all(roleMappings);
      })
      .then(() => Promise.resolve({result: true}));
  };

  ConsoleUser.remoteMethod(
    'createWithRoles',
    {
      accepts: [
        {
          arg: 'Account',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
        {arg: 'Roles', type: 'object', required: false, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  ConsoleUser.patchAttributesWithRoles = function(Args/*, next*/) {
    let Account = Args.Account;
    let Roles = Args.Roles;

    return new Promise((resolve, reject) => {
      ConsoleUser.updateAll({id: Account.id}, Account, (err, count) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    })
      .then(() => new Promise((resolve, reject) => {
        if (Roles.length < 1) {
          resolve();
          return;
        }
        let where = {
          where: {
            or: Object.keys(Roles).map(key => {
              return {name: key};
            })
          }
        };
        models.Role.find(where, (err, roles) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(roles);
        });
      }))
      .then(roles => new Promise((resolve, reject) => {
        if (!roles || roles.length < 1) {
          resolve();
          return;
        }
        let deleteRoles = roles.filter(role => Roles[role.name] === false);
        if (deleteRoles.length < 1) {
          resolve(roles);
          return;
        }
        // console.log('delete:' , deleteRoles.map(role => role.name));
        let where = {
          and: [
            {principalType: 'USER'},
            {principalId: Account.id},
            {
              or: deleteRoles.map(role => {
                return {roleId: role.id};
              })
            }
          ]
        };
        models.RoleMapping.destroyAll(where, err => {
          if (err) {
            reject(err);
            return;
          }
          resolve(roles);
        });
      }))
      .then(roles => {
        if (!roles || roles.length < 1) {
          Promise.resolve();
          return;
        }
        let addRoles = roles.filter(role => Roles[role.name] === true);
        if (addRoles.length < 1) {
          Promise.resolve();
          return;
        }
        // console.log('add:' , addRoles.map(role => role.name));
        let roleMappings = [];
        addRoles.forEach(role => {
          roleMappings.push(
            new Promise((resolve, reject) =>
              role.principals.create({
                principalType: 'USER',
                principalId: Account.id
              }, (err, principal) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(principal);
              })
            )
          );
        });
        return Promise.all(roleMappings);
      })
      .then(() => Promise.resolve({result: true}));
  };

  ConsoleUser.remoteMethod(
    'patchAttributesWithRoles',
    {
      accepts: [
        {
          arg: 'Account',
          type: 'object',
          required: true,
          http: {source: 'body'}
        },
        {arg: 'Roles', type: 'object', required: false, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /** login with sso **/
  ConsoleUser.ssoLogin = function(username, email, name, req, res) {
    return new Promise((resolve, reject) => {
      console.log('ssoLogin 들어옴');
      models.ConsoleUser.findOne({where: {username}})
        .then(findUser => {
          //console.log(findUser);
          if (findUser === null) {
            console.log('create!!!');
            models.ConsoleUser.create({
              username,
              password: username,
              email,
              activated: 1
            }, function(err, userObj) {
              if (err) {
                console.log('error', err);
                return reject(err);
              }

              let chain = new Promise((resolve2, reject2) => {
                let where = {
                  where: {name: 'dashboard^R'}
                };
                models.Role.find(where, (err, roles) => {
                  if (err) {
                    reject2(err);
                    return;
                  }
                  resolve2({user: userObj, roles});
                });
              });
              let tempRoles = {roles: {}};
              let tempUser = userObj;

              chain = chain.then(ws => {
                tempUser = ws.user;
                tempRoles = ws.roles;
                // console.log('ws : ', ws);
                let roleMappings = [ws.user];
                ws.roles.forEach(role => {
                  roleMappings.push(
                    new Promise((resolve3, reject3) =>
                      models.RoleMapping.create({
                        principalType: 'USER',
                        principalId: ws.user.id,
                        roleId: role.id
                      }, (err, principal) => {
                        // console.log('principal : ', principal);
                        if (err) {
                          console.log('create roleMappings error');
                          reject3(err);
                          return;
                        }
                        console.log('create roleMappings success');
                        resolve3(principal);
                      })
                    )
                  );
                });
                return Promise.all(roleMappings);
              });

              chain = chain.then(rm => {
                return new Promise((resolve4, reject4) => {
                  models.Group.find({}
                    , (err, group) => {
                      if (err) {
                        console.log('group is null');
                        reject4(err);
                        return;
                      }
                      console.log('find group success');
                      resolve4({group: group, user: rm[0]});
                    }
                  );
                });
              }).catch(error =>
                console.log(error)
              );

              chain = chain.then(rg => {
                return new Promise((resolve5, reject5) => {
                  models.GroupMember.create(
                    {groupId: rg.group[0].id,
                      role: 'member',
                      state: 'accepted',
                      userId: rg.user.id}
                    , (err, groupMem) => {
                      if (err) {
                        console.log('fail create group member');
                        reject5(err);
                        return;
                      }
                      console.log('create group member success');
                      return resolve5(groupMem);
                    }
                  );
                });
              }).catch(error => {
                console.log(error);
              });

              chain.then(groupMem => {
                models.ConsoleUser.login(
                  {
                    username: userObj.username,
                    password: userObj.username
                  }, 'user', function(err, loginResult) {
                    if (err) {
                      console.log(err);
                      return reject(err);
                    }

                    let userData = loginResult;
                    let tempRole = {
                      roles: undefined
                    };
                    tempRole = tempRoles[0].name;
                    // console.log('userResult : ', loginResult.user);
                    let user = {
                      username: tempUser.username,
                      email: tempUser.email,
                      activated: tempUser.activated,
                      id: tempUser.id,
                      roles: {
                        'dashboard^R': true
                      }
                    };

                    // console.log('loginResult :', loginResult)
                    // console.log('user : ', user);
                    res.cookie('$LoopBackSDK$created',
                      JSON.stringify(loginResult.created), {
                        encode(value) {
                          return value;
                        }
                      });
                    res.cookie('$LoopBackSDK$id', loginResult.id);
                    res.cookie('$LoopBackSDK$ttl', loginResult.ttl);
                    res.cookie('$LoopBackSDK$userId', loginResult.userId);
                    res.cookie('$LoopBackSDK$rememberMe', true);
                    res.cookie('$LoopBackSDK$user', JSON.stringify(user), {
                      encode(value) {
                        return value;
                      }
                    });
                    return resolve(loginResult);
                  }
                );
              });
            });
          } else {
            console.log('try login');
            models.ConsoleUser.login(
              {
                username: findUser.username,
                password: findUser.username
              }, 'user', function(err, loginResult) {
                if (err) {
                  console.log(err);
                  return reject(err);
                }
                // console.log('findUser : ', findUser);
                let query = {
                  and: [{principalType: 'USER'}, {principalId: findUser.id}]
                };
                let chain = new Promise((resolve1, reject1) => {
                  models.RoleMapping.find({where: query}, (err, mappings) => {
                    if (err) {
                      reject1(err);
                    } else {
                      // console.log('mappings : ', mappings);
                      resolve1(mappings.map(item => item.roleId));
                    }
                  });
                });

                let tempRoles;
                let chain2 = chain.then(
                  roleIds => new Promise((resolve2, reject2) => {
                    // console.log('roleIds : ', roleIds);
                    if (roleIds.length < 1) {
                      resolve({});
                      return;
                    }
                    let ids = roleIds.map(id => {
                      return {id};
                    });
                    let where = {where: {or: ids}};

                    models.Role.find(where, (err, roles) => {
                      if (err) {
                        reject2(err);
                      }
                      let _roles = {};
                      roles.forEach(role => _roles[role.name] = true);
                      tempRoles = _roles;
                      // console.log('_roles: ', _roles);
                      resolve2(_roles);
                    });
                  }));

                chain2.then(() => {
                  // console.log('chain roles :', roles);
                  // console.log('temp roles : ', tempRoles);

                  let user = {
                    username: findUser.username,
                    email: findUser.email,
                    activated: findUser.activated,
                    id: findUser.id,
                    roles: tempRoles
                  };
                  // console.log('user: ', user);
                  res.cookie('$LoopBackSDK$created',
                    JSON.stringify(loginResult.created), {
                      encode(value) {
                        return value;
                      }
                    });
                  res.cookie('$LoopBackSDK$id', loginResult.id);
                  res.cookie('$LoopBackSDK$ttl', loginResult.ttl);
                  res.cookie('$LoopBackSDK$userId', loginResult.userId);
                  res.cookie('$LoopBackSDK$rememberMe', 'true');
                  res.cookie('$LoopBackSDK$user', JSON.stringify(user), {
                    encode(value) {
                      return value;
                    }
                  });
                  return resolve(findUser);
                });
              });
          }
        })
        .catch(err => reject(err));
    });
  };

  ConsoleUser.remoteMethod(
    'ssoLogin', {
      http: {verb: 'post'},
      accepts: [
        {arg: 'username', type: 'string'},
        {arg: 'email', type: 'string'},
        {arg: 'name', type: 'string'},
        {arg: 'req', type: 'object', http: {source: 'req'}},
        {arg: 'res', type: 'object', http: {source: 'res'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  //////////////////////////////////////////////////////////
  /**
   * UserGroup 테이블 조회
   * */
  ConsoleUser.getUserGroup = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = appServer.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select 
          UGM.groupId, 
          UG.name, 
          UGM.userId, 
          UGM.userStatus, 
          UGM.userRole
        from ${database}.UserGroupMapping UGM
        inner join ${database}.UserGroup UG
        on UGM.groupId = UG.id
        where UGM.userId = '${Args.queryParams.userId}';`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  ConsoleUser.remoteMethod(
    'getUserGroup',
    {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /**
   * Workspace 테이블 조회
   * */
  ConsoleUser.getMyWorkspace = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = appServer.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      // let query = `select
      //     id,
      //     name,
      //     groupId,
      //     ownerId,
      //     createdId,
      //     createdAt
      //   from ${database}.workspace
      //   where groupId = '${Args.queryParams.groupId}';`;

      let query = `select
       WS.id,
       case
         when (WS.type='user') then (select username from ConsoleUser where id=WS.ownerId)
         when (WS.type='group') then (select name from UserGroup where id=WS.groupId)
         else ''
       end workspace,
       WS.type as type,
       CU.username as owner
      from ${database}.Workspace WS
      left outer join ${database}.UserGroupMapping UGM
      on WS.groupId = UGM.groupId
      left outer join ${database}.ConsoleUser CU
      on WS.ownerId = CU.id
      where (WS.type = 'user' and WS.id = '${Args.queryParams.userId}')
         or (WS.type = 'group' and UGM.userId = '${Args.queryParams.userId}');`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  ConsoleUser.remoteMethod(
    'getMyWorkspace', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );
};
