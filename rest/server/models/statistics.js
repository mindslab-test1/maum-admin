'use strict';

const app = require('../server');

module.exports = function(Statistics) {
  let connector, type;

  Statistics.assignConnector = function(Args, next) {
    return new Promise((resolve, reject) => {
      let targetKey = Object.keys(app.datasources)
        .find(key => app.datasources[key].settings.name ===
        Statistics.config.targetDataSource);

      if (targetKey) {
        connector = app.datasources[targetKey].connector;
        type = connector.name;
      }
      resolve(type);
      resolve(Statistics);

      if (typeof type === 'string') {
        resolve(type);
      } else {
        reject(new Error('Connector is not Ready!'));
      }
    });
  };

  Statistics.remoteMethod(
    'assignConnector', {
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      reject(
        new Error('Don\'t USE this API. Implement the Query in REST module.'));
    });
  };

  Statistics.remoteMethod(
    'executeQuery', {
      accepts: [
        {arg: 'Query', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeSessionQuery = function(QueryParams, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query;
      if (connector.name === 'oracle') {
        query = `SELECT
                  sesssec,
                  id,
                  chatbot,
                  accessfrom,
                  peerip,
                  talkcount,
                  user,
                  TO_CHAR(startAt, 'YYYY-MM-DD HH24:MI') as startAt,
                  TO_CHAR(endAt, 'YYYY-MM-DD HH24:MI')   as endAt
                FROM "SESSION"
                WHERE startAt >= TO_DATE('${QueryParams.startAt}', 'YYYY-MM-DD HH24:MI') 
                AND endAt < TO_DATE('${QueryParams.endAt}', 'YYYY-MM-DD HdH24:MI') + INTERVAL '1' DAY`;
      } else {
        query = `select *, 
          date_format(startAt, '%Y-%m-%d %H:%i') as startAt, 
          date_format(endAt, '%Y-%m-%d %H:%i') as endAt, 
          accessFrom as channel 
          from ${database}.Session 
          where startAt >= '${QueryParams.startAt}' 
          and endAt < '${QueryParams.endAt}' + interval 1 day`;
      }

      if (QueryParams.searchOption !== undefined &&
        QueryParams.keyword !== undefined && QueryParams.searchOption !==
        'accessFrom') {
        query += ` and ${QueryParams.searchOption} like '%${QueryParams.keyword}%'`;
      } else if (QueryParams.searchOption !== undefined &&
        QueryParams.keyword !== undefined && QueryParams.searchOption ===
        'accessFrom') {
        query += ` and accessFrom = '${QueryParams.keyword}'`;
      }

      query += ' order by startAt desc, endAt desc';

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          let result = [];
          resultSet.forEach(row => {
            var key,
              keys = Object.keys(row);
            var n = keys.length;
            var newobj = {};
            while (n--) {
              key = keys[n];
              newobj[key.toUpperCase()] = row[key];
            }
            result.push(newobj);
          });
          resolve(result);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeSessionQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeSessionV3Query = function(QueryParams, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query;
      if (connector.name === 'oracle') {
        query = `SELECT 
                  SESSSEC, 
                  id, 
                  userId, 
                  deviceId, 
                  deviceType, 
                  deviceVersion, 
                  channel, 
                  peer, 
                  chatbot, 
                  intentFinderPolicyName, 
                  lastSkill, 
                  lastAgentKey,
                  TO_CHAR(startAt, 'YYYY-MM-DD HH24:MI') as startAt,
                  TO_CHAR(lastTalkedAt, 'YYYY-MM-DD HH24:MI') as lastTalkedAt
                FROM SESSIONV3
                WHERE startAt >= TO_DATE('${QueryParams.startAt}', 'YYYY-MM-DD HH24:MI') 
                AND lastTalkedAt < TO_DATE('${QueryParams.endAt}', 'YYYY-MM-DD HH24:MI') + INTERVAL '1' DAY`;
      } else {
        query = `SELECT *, 
          DATE_FORMAT(startAt, '%Y-%m-%d %H:%i') AS startAt, 
          DATE_FORMAT(lastTalkedAt, '%Y-%m-%d %H:%i') AS lastTalkedAt
          FROM ${database}.SessionV3 
          WHERE startAt >= '${QueryParams.startAt}' 
          AND lastTalkedAt < '${QueryParams.endAt}' + INTERVAL 1 DAY`;
      }

      if (QueryParams.searchOption !== undefined &&
        QueryParams.keyword !== undefined && QueryParams.searchOption !==
        'channel') {
        query += ` and ${QueryParams.searchOption} like '%${QueryParams.keyword}%'`;
      } else if (QueryParams.searchOption !== undefined &&
        QueryParams.keyword !== undefined && QueryParams.searchOption ===
        'channel') {
        query += ` and channel = '${QueryParams.keyword}'`;
      }

      query += ' order by startAt desc, lastTalkedAt desc';

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          let result = [];
          resultSet.forEach(row => {
            var key,
              keys = Object.keys(row);
            var n = keys.length;
            var newobj = {};
            while (n--) {
              key = keys[n];
              newobj[key.toUpperCase()] = row[key];
            }
            result.push(newobj);
          });
          resolve(result);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeSessionV3Query', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeChatListQuery = function(queryParams, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query;
      if (connector.name === 'oracle') {
        query = `SELECT
                  sesssec,
                  sid,
                  seq,
                  skill,
                  clresult,
                  audio,
                  audiorecordfile,
                  samplerate,
                  context,
                  intention,
                  lang,
                  inmeta,
                  outmeta,
                  intext,
                  outtext,
                  warned,
                  feedback,
                  accuracy,
                  user,
                  TO_CHAR(startAt, 'HH24:MI:SS') as startAt,
                  TO_CHAR(endAt, 'HH24:MI:SS') as endAt
                FROM Talk
                WHERE sid = ${queryParams.sessionId}`;
      } else {
        query = `SELECT *,
          DATE_FORMAT(startAt, '%H:%i%:%s') as startAt,
          DATE_FORMAT(endAt, '%H:%i:%s') as endAt
        FROM ${database}.Talk
        WHERE sid = ${queryParams.sessionId}`;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          let result = [];
          resultSet.forEach(row => {
            var key,
              keys = Object.keys(row);
            var n = keys.length;
            var newobj = {};
            while (n--) {
              key = keys[n];
              newobj[key.toUpperCase()] = row[key];
            }
            result.push(newobj);
          });
          resolve(result);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeChatListQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeChatV3ListQuery = function(queryParams, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query;
      if (connector.name === 'oracle') {
        query = `SELECT
                  sesssec,
                  seq,
                  sessionId,
                  opsyncId,
                  lang,
                  skill,
                  intent,
                  closeSkill,
                  inText,
                  inputType,
                  outText,
                  speechOut,
                  reprompt,
                  extraout,
                  statuscode,
                  dialogresult,
                  feedback,
                  accuracy,
                  transitchatbot,
                  TO_CHAR(STARTTIME, 'HH24:MI:SS') as startTime,
                  TO_CHAR(ENDTIME, 'HH24:MI:SS')   as endTime
                FROM TalkV3
                WHERE sessionId = ${queryParams.sessionId}`;
      } else {
        query = `SELECT *,
          DATE_FORMAT(startTime, '%H:%i%:%s') as startTime,
          DATE_FORMAT(endTime, '%H:%i:%s') as endTime
        FROM ${database}.TalkV3
        WHERE sessionId = ${queryParams.sessionId}`;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          let result = [];
          resultSet.forEach(row => {
            var key,
              keys = Object.keys(row);
            var n = keys.length;
            var newobj = {};
            while (n--) {
              key = keys[n];
              newobj[key.toUpperCase()] = row[key];
            }
            result.push(newobj);
          });
          resolve(result);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeChatV3ListQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeOverallQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select
          date_format(a.endAt, '%Y-%m-%d %H:%i:%s') as 'date',
          a.sid as 'sessionID',
          a.seq as 'talkID',
          a.inText,
          a.outText,
          a.outMeta as 'metaInfo',
          b.accessFrom as 'channel',
          case when instr(a.outMeta, '"out.da.engine.name":"None"')=0
              then 'Success'
              else 'Fail' end
            as 'result',
          a.feedback as 'satisfaction',
          a.accuracy
        from ${database}.Talk a left join ${database}.Session b
          on b.id=a.sid
        where date(a.endAt)
          between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        order by date desc, sessionID desc, talkID desc;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeOverallQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeResponseQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }
      /*
        select date, sum(f) as fail, sum(s) as success from (
          select
            DATE_FORMAT(endAt, '%Y%m%d') as date,
            instr(outMeta, '"out.da.engine.name":"None"')>0 as f,
            instr(outMeta, '"out.da.engine.name":"None"')=0 as s
          from Talk
          where date(endAt) between '2017-07-01' and '2017-12-01'
        ) A
        group by date;
      */

      let query = `select date, sum(f) as Fail, sum(s) as Success
        from (
          select
            date_format(endAt, '%Y-%m-%d') as date,
            instr(outMeta, '"out.da.engine.name":"None"')>0 as f,
            instr(outMeta, '"out.da.engine.name":"None"')=0 as s
          from ${database}.Talk
          where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        ) sub group by date
        union
        select null, sum(f) as Fail, sum(s) as Success
        from (
          select
            date_format(endAt, '%Y-%m-%d') as date,
            instr(outMeta, '"out.da.engine.name":"None"')>0 as f,
            instr(outMeta, '"out.da.engine.name":"None"')=0 as s
          from ${database}.Talk
          where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        ) bbb;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeResponseQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeEngineQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = '';

      /*
      select
      date,
      coalesce(sum(case when keyword = "여권" then count end), 0) as '여권',
      coalesce(sum(case when keyword = "축제" then count end), 0) as '축제',
      coalesce(sum(case when keyword = "업무" then count end), 0) as '업무'
      from Statistics stat
      where engine = 'sds' and date(day) between '2017-01-01 00:00:00' and '2017-02-01 23:59:00'
      group by date
      union
      select
      null,
      coalesce(sum(case when keyword = "여권" then count end), 0) as '여권',
      coalesce(sum(case when keyword = "축제" then count end), 0) as '축제',
      coalesce(sum(case when keyword = "업무" then count end), 0) as '업무'
      from Statistics stat
      where engine = 'sds' and date(day) between '2017-01-01 00:00:00' and '2017-02-01 23:59:00';*/

      // Engine === all 인 경우 모든 엔진에 대한 category 값을 검색합니다.
      if (Args.queryParams.engine.toLowerCase() === 'all') {
        query = 'select date_format(day, \'%Y-%m-%d\') as \'date\',';
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);

        query += `from ${database}.Statistics stat
        where date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        group by date`;

        query += ' union select null,';
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);
        query += `from ${database}.Statistics stat
        where date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}';`;
      } else {
        // Engine 별 category 값을 검색합니다.
        query = 'select date_format(day, \'%Y-%m-%d\') as \'date\',';
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);

        query += `from ${database}.Statistics stat
        where engine = '${Args.queryParams.engine}' and
        date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        group by date`;

        query += ' union select null,';
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);
        query += `from ${database}.Statistics stat
        where engine = '${Args.queryParams.engine}' and
        date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}';`;
      }

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeEngineQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeChannelQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select
          distinct accessFrom
        from Session
        where date(startAt)
          between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        order by accessFrom`;

      const queryChannel = channels => {
        const lastIndex = channels.length - 1;
        let accessFrom;
        query = 'select date_format(startAt, \'%Y-%m-%d\') as \'date\',';
        channels.forEach((channel, index) => {
          accessFrom = channel.accessFrom;
          query += `count(case when accessFrom=${accessFrom} then 0 end)`;
          query += ` as '${accessFrom}'`;
          if (index < lastIndex) {
            query += ',';
          }
        });

        query += ` from ${database}.Session
          where date(startAt) between
            '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
          group by date`;

        query += ' union select null,';
        channels.forEach((channel, index) => {
          accessFrom = channel.accessFrom;
          query += `count(case when accessFrom=${accessFrom} then 0 end)`;
          query += ` as '${accessFrom}'`;
          if (index < lastIndex) {
            query += ',';
          }
        });

        query += ` from ${database}.Session
          where date(startAt) between
            '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'`;

        connector.execute(query, (err, resultSet) => {
          if (err) {
            reject(err);
          } else {
            resolve(resultSet);
          }
        });
      };

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          queryChannel(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeChannelQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeSatisfactionLineQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query =
        `select 
          date_format(endAt, '%Y-%m-%d') as 'date',
          round(avg(feedback), 2) as 'satisfaction', 
          count(feedback) as 'count', 
          sum(feedback) as 'sum'
        from ${database}.Talk
        where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
          and feedback!=0
          group by date;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeSatisfactionLineQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.updateAccuracy = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let whenQuery = '';
      let sidQuery = '';
      let seqQuery = '';

      for (let i = 0; i < Args.queryParams.length; i++) {
        whenQuery +=
          `when sid = ${Args.queryParams[i].sessionId} and seq = ${Args.queryParams[i].talkId}
            then ${Args.queryParams[i].accuracy} `;
        sidQuery += `${Args.queryParams[i].sessionId},`;
        seqQuery += `${Args.queryParams[i].talkId},`;
      }

      let commaSid = sidQuery.substr(0, sidQuery.length - 1);
      let commaSeq = seqQuery.substr(0, seqQuery.length - 1);

      let query = `update ${database}.Talk
       set accuracy = case ${whenQuery} else accuracy end
       where sid in (${commaSid}) and seq in (${commaSeq});`;

      // console.log('== Accuracy Update Query');
      // console.log(query);

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'updateAccuracy', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeAccuracyLineQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select
        date_format(endAt, '%Y-%m-%d') as 'date',
        round(avg(accuracy) , 2) as 'accuracy', 
        count(accuracy) as count, 
        sum(accuracy) as sum
        from ${database}.Talk
        where date(endAt)
        between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        and accuracy IS NOT NULL
        group by date;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeAccuracyLineQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeDailySessionQuery = function(next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `SELECT
          DATE_FORMAT(startAt,'%Y-%m-%d') as date
          , count(id) as totCnt, sec_to_time((sum(TIMEDIFF(endAt, startAt)))) as totTime
          , sum(talkCount) as totCall
        FROM Session
        group by date
        order by date
        desc limit 10;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeDailySessionQuery', {
      description: 'get daliy session info from Session Table',
      returns: {
        arg: 'data', type: 'object', root: true
      },
      http: {
        path: '/executeDailySessionQuery', verb: 'get'
      },
      isStatic: true
    });

  Statistics.executeDailySessionDownloadQuery = function(Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select
          date_format(startAt,'%Y-%m-%d') as date
          , count(id) as '토탈 세션(수)'
          , sec_to_time((sum(timediff(endAt, startAt)))) as '총 접속시간'
          , sum(talkCount) as '총 호출(수)'
        from Session
        where
          date_format(startAt,'%Y-%m-%d') >= date_format(date_add('${Args.queryParams.startAt}',interval +1 day),'%Y-%m-%d')
          and date_format(startAt,'%Y-%m-%d') <= date_format(date_add('${Args.queryParams.endAt}',interval +1 day),'%Y-%m-%d')
        group by date
        order by date desc;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeDailySessionDownloadQuery', {
      accepts: [
        {
          arg: 'queryParams',
          type: 'object',
          required: true,
          http: {source: 'body'}
        }
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeTimeSessionQuery = function(next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources.mindslab;
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      var timeAry = [120, 110, 100, 90, 80, 70, 60, 50, 40, 30];

      let query = '';
      for (let index = 0; index < timeAry.length; index++) {
        query += `select
	        date_format(date_add(now(),interval -'${timeAry[index]}' minute),'%H:%i') as time
          , sum(
		      case
			    when
            endAt >= date_format(date_add(now(), interval -'${timeAry[index] +
        10}' minute), '%Y-%m-%d %H:%i')
            and startAt <= date_format(date_add(now(), interval -'${timeAry[index]}' minute), '%Y-%m-%d %H:%i')
          then 1
          else 0
		      end) as sessionCnt
        from Session`;
        if (index != timeAry.length - 1) {
          query += ' union ';
        }
      }

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeTimeSessionQuery', {
      description: 'get Time session info from Session Table',
      returns: {
        arg: 'data', type: 'object', root: true
      },
      http: {
        path: '/executeTimeSessionQuery', verb: 'get'
      },
      isStatic: true
    });
};
