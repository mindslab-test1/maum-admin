'use strict';
const app = require('../server');

module.exports = function(Model) {
  Model.observe('before save', function beforeSave(ctx, next) {
    let instance = ctx.instance || ctx.data;
    let defaultFn = ctx.Model.definition.properties.id.defaultFn;
    let dbName = app.datasources.mindslab.settings.connector;

    if (ctx.isNewInstance && dbName === 'oracle') {
      let tableName = ctx.Model.definition.settings.oracle.table;
      let ds = instance.getDataSource();

      console.log('id generator table name :', tableName);

      if (defaultFn === 'uuidv4') {
        return next();
      } else {
        let sql = `select ${tableName}_SEQ.NEXTVAL as ID from dual`;
        console.log('table name : ', tableName);

        ds.connector.query(sql, function(err, results) {
          if (err) {
            console.error(err);
            return next(err);
          }
          instance.id = results[0].ID;
          return next();
        });
      }
    } else if (ctx.isNewInstance && dbName === 'mysql') {
      let tableName = ctx.Model.definition.settings.mysql.table;
      let ds = instance.getDataSource();

      console.log('id generator table name :', tableName);

      if (defaultFn === 'uuidv4') {
        return next();
      } else {
        let sql1 = `select next_val ID from ${tableName}_SEQ`;
        let sql = `update ${tableName}_SEQ set next_val = (next_val +1)`;

        ds.connector.query(sql1, function(err, results1) {
          if (err) {
            console.error(err);
          }

          ds.connector.query(sql, function(err) {
            if (err) {
              console.error(err);
              return next(err);
            }
            instance.id = results1[0].ID;
            return next();
          });
        });
      }
    } else {
      return next();
    }
  });
};
