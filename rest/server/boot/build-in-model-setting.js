const loopback = require('loopback');

module.exports = function (app) {
  // ACL setting
  let acl = app.models.ACL;
  acl.mixin('IdGenerator');
  acl.definition.settings.forceId = false;

  /*if (acl.dataSource.connector.name === 'oracle') {
    acl.mixin('IdGenerator');
    acl.definition.settings.forceId = false;
  }*/
  Object.assign(acl.definition.settings, {
    mysql: {
      table: 'MAI_ACL'
    },
    oracle: {
      scheme: 'MAUM',
      table: 'MAI_ACL'
    }
  });

  Object.assign(acl.definition.properties.id, {mysql: {columnName: 'ID'}, oracle: {columnName: 'ID'}});
  Object.assign(acl.definition.properties.model, {mysql: {columnName: 'MODEL'}, oracle: {columnName: 'MODEL'}});
  Object.assign(acl.definition.properties.property, {mysql: {columnName: 'PROPERTY'}, oracle: {columnName: 'PROPERTY'}});
  Object.assign(acl.definition.properties.accessType, {mysql: {columnName: 'ACCESS_TYPE'}, oracle: {columnName: 'ACCESS_TYPE'}});
  Object.assign(acl.definition.properties.permission, {mysql: {columnName: 'PERMISSION'}, oracle: {columnName: 'PERMISSION'}});
  Object.assign(acl.definition.properties.principalType, {mysql: {columnName: 'PRINCIPAL_TYPE'}, oracle: {columnName: 'PRINCIPAL_TYPE'}});
  Object.assign(acl.definition.properties.principalId, {mysql: {columnName: 'PRINCIPAL_ID'}, oracle: {columnName: 'PRINCIPAL_ID'}});

  // Role setting
  let role = app.models.Role;
  role.mixin('IdGenerator');
  role.definition.settings.forceId = false;
  /*if (role.dataSource.connector.name === 'oracle') {
    role.mixin('IdGenerator');
    role.definition.settings.forceId = false;
  }*/

  Object.assign(role.definition.settings, {
    mysql: {
      table: 'MAI_ROLE'
    },
    oracle: {
      scheme: 'MAUM',
      table: 'MAI_ROLE'
    }
  });

  Object.assign(role.definition.properties.id, {mysql: {columnName: 'ID'}, oracle: {columnName: 'ID'}});
  Object.assign(role.definition.properties.name, {mysql: {columnName: 'NAME'}, oracle: {columnName: 'NAME'}});
  Object.assign(role.definition.properties.description, {mysql: {columnName: 'DESCRIPTION'}, oracle: {columnName: 'DESCRIPTION'}});
  Object.assign(role.definition.properties.created, {mysql: {columnName: 'CREATED_AT'}, oracle: {columnName: 'CREATED_AT'}});
  Object.assign(role.definition.properties.modified, {mysql: {columnName: 'UPDATED_AT'}, oracle: {columnName: 'UPDATED_AT'}});

  Object.assign(role.definition.settings.relations, {
    principals: {
      type: 'hasMany',
      model: 'MAIRoleMapping',
      foreignKey: 'ROLE_ID'
    }
  });

  // RoleMapping setting
  let roleMapping = app.models.RoleMapping;
  roleMapping.mixin('IdGenerator');
  roleMapping.definition.settings.forceId = false;
  /*if (roleMapping.dataSource.connector.name === 'oracle') {
    roleMapping.mixin('IdGenerator');
    roleMapping.definition.settings.forceId = false;
  }*/

  Object.assign(roleMapping.definition.settings, {
    mysql: {
      table: 'MAI_ROLE_MAPPING'
    },
    oracle: {
      scheme: 'MAUM',
      table: 'MAI_ROLE_MAPPING'
    }
  });

  roleMapping.belongsTo(roleMapping, {role: {foreignKey: 'ROLE_ID'}});

  Object.assign(roleMapping.definition.rawProperties.id, {updateOnly: false, generate: false});
  Object.assign(roleMapping.definition.properties.id, {updateOnly: false, generate: false});
  Object.assign(roleMapping.definition.properties.principalType, {mysql: {columnName: 'PRINCIPAL_TYPE'}, oracle: {columnName: 'PRINCIPAL_TYPE'}});
  Object.assign(roleMapping.definition.properties.principalId, {mysql: {columnName: 'PRINCIPAL_ID'}, oracle: {columnName: 'PRINCIPAL_ID'}});
  Object.assign(roleMapping.definition.properties.roleId, {mysql: {columnName: 'ROLE_ID'}, oracle: {columnName: 'ROLE_ID'}});
  Object.assign(roleMapping.definition.properties.roleMappingId, {mysql: {columnName: 'ROLE_MAPPING_ID'}, oracle: {columnName: 'ROLE_MAPPING_ID'}});

  //Object.assign(roleMapping.definition.properties.roleMappingId, {mysql: {columnName: 'ROLE_MAPPING_ID'}, oracle: {columnName: 'ROLE_MAPPING_ID'}});

  let accessToken = app.models.AccessToken;

  Object.assign(accessToken.definition.settings, {
    mysql: {
      table: 'MAI_ACCESS_TOKEN'
    },
    oracle: {
      scheme: 'MAUM',
      table: 'MAI_ACCESS_TOKEN'
    }
  });

  accessToken.belongsTo(accessToken, {user: {foreignKey: 'USER_ID'}});

  Object.assign(accessToken.definition.properties.id, {mysql: {columnName: 'ID'}, oracle: {columnName: 'ID'}});
  Object.assign(accessToken.definition.properties.ttl, {mysql: {columnName: 'TTL'}, oracle: {columnName: 'TTL'}});
  Object.assign(accessToken.definition.properties.scopes, {mysql: {columnName: 'SCOPES'}, oracle: {columnName: 'SCOPES'}});
  Object.assign(accessToken.definition.properties.created, {mysql: {columnName: 'CREATED_AT'}, oracle: {columnName: 'CREATED_AT'}});
  Object.assign(accessToken.definition.properties.userId, {mysql: {columnName: 'USER_ID'}, oracle: {columnName: 'USER_ID'}});
  Object.assign(accessToken.definition.properties.accessTokenId, {mysql: {columnName: 'ACCESS_TOKEN_ID'}, oracle: {columnName: 'ACCESS_TOKEN_ID'}});

};
