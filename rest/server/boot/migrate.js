'use strict';
const aclAssets = require('../lib/acl-assets');
const process = require('process');

module.exports = function(app) {
  // 모델 마이그레이션
  migrateModels(app)
    .then(() => defineRoles(app))
    .then(() => defineAcls(app))
    .then(() => registerDefaultUsers(app))
    .catch(console.error);
};

/**
 *  사용자 관련 모델 database table을 생성한다,
 *  단 기존 express orm에서 생성된 table은  생성 하지 않는다.
 * @param app
 * @param skipAccessToken
 * @returns {Promise.<T>}
 */
function migrateModels(app) {
  let chain = Promise.resolve();
  app.models().forEach(model => {
    let dataSource = model.dataSource;
    if (!dataSource) {
      return;
    }

    let connectorName = dataSource.connector.name;
    if (!process.env.MIGRATE && connectorName != 'memory') {
      return;
    }

    let modelName = model.modelName;
    if (connectorName != 'memory' && modelName == 'AccessToken' && process.env.MIGRATE != 'ALL') {
      console.log('\x1b[41m** AccessToken migration skipped (MIGRATE=ALL otherwise)\x1b[0m');
      return;
    }
    if (model.config.migratable !== false) {
      chain = chain
        .then(() => dataSource.automigrate(modelName))
        .then(() => console.log(`* ${modelName} migrated in ${connectorName}`));
    }
  });
  return chain;
}


/**
 *  Role을 생성한다.
 * @param app
 * @returns {Promise.<T>}
 */
function defineRoles(app) {
  let consoleUser = app.models.ConsoleUser;
  let chain = Promise.resolve();

  if (consoleUser.dataSource.connector.name == 'memory' || process.env.MIGRATE) {
    let roleModel = app.models.Role;
    chain = chain.then(() =>
      new Promise((resolve, reject) =>
        roleModel.create({name: 'admin'},
          (err, role) => {
            // console.log(`${role.id} ${role.name}`);
            if (err) {
              reject(err);
            }
            resolve(role);
          }
        )
      )
    );

    aclAssets.roleList.forEach(roles => {
      for (let i = 0; i < roles[1].length; i++) {
        chain = chain.then(() =>
          new Promise((resolve, reject) =>
            roleModel.create({name: `${roles[0]}^${roles[1].charAt(i)}`},
              (err, role) => {
                // console.log(`${role.id} ${role.name}`);
                if (err) {
                  reject(err);
                }
                resolve(role);
              }
            )
          )
        );
      }
    });

    chain.then(() => {
      console.log('* Roles are created.');
    });
  }

  return chain;
}


/**
 *  접근권한을 설정한다.
 * @param app
 * @returns {Promise.<T>}
 */
function defineAcls(app) {
  let consoleUser = app.models.ConsoleUser;
  let chain = Promise.resolve();

  if (consoleUser.dataSource.connector.name == 'memory' || process.env.MIGRATE) {
    aclAssets.grpcAcls.forEach(propAcl => {
      chain = chain.then(() =>
        new Promise((resolve, reject) =>
          app.models.ACL.create(
            {
              model: 'Grpc',
              property: propAcl[0],
              accessType: '*',
              permission: 'ALLOW',
              principalType: 'ROLE',
              principalId: propAcl[1],
            },
            (err, acl) => {
              // console.log(`${acl.property} ${acl.principalId}`);
              if (err) {
                console.log(err);
                reject(err);
              }
              resolve(acl);
            }
          )
        )
      );
    });

    chain.then(() => {
      console.log('* ACLs are defined.');
    });
  }

  return chain;
}

function makeid(length) {
  let text = '';
  let possible = 'abcdefghijklmnopqrstuvwxyz0123456789';

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

/**
 *  기본 사용자를 생성한다.
 * @param app
 * @returns {Promise.<T>}
 */
function registerDefaultUsers(app) {
  let consoleUser = app.models.ConsoleUser;
  let chain = Promise.resolve();

  if (consoleUser.dataSource.connector.name == 'memory' || process.env.MIGRATE) {
    let RoleMapping = app.models.RoleMapping;
    chain = chain
      .then(() =>
        new Promise((resolve, reject) =>
          consoleUser.create([
            {username: 'admin', name: '어드민', password: '1234', email: 'admin@console.com', activated: true},
            {username: 'manager', name: '관리자', password: '1234', email: 'manager@console.com', activated: true},
            {username: 'guest', name: '손님', password: '1234', email: 'guest@console.com', activated: true}
          ], (err, users) => {
            if (err) {
              reject(err);
            }
            resolve(users);
          }
          )
        )
      )
      .then(users =>
        new Promise((resolve, reject) => {
          let Group = app.models.Group;
          let groups = [];

          // 만들어진 사용자를 기반으로 새로운 workspace를 생성하도록 한다.
          users.forEach(u => {
            for (let i = 0; i < 3; i++) {
              groups.push({
                name: makeid(10),
                description: `${makeid(11)}  ${makeid(5)} ${makeid(3)} ${makeid(6)}.`,
                ownerId: u.id
              });
            }
          });

          // 만들어진 workspaces를 생성한다.
          Group.create(groups, err => {
            if (err) {
              reject(err);
            }
            resolve(users);
          });
        })
      )
      .then(users =>
        new Promise((resolve, reject) =>
          app.models.Role.find((err, roles) => {
            if (err) {
              return reject(err);
            }
            return resolve({users, roles});
          })
        )
      )
      .then(ws =>
        new Promise((resolve, reject) => {
          let id = ws.users.find(user => user.username === 'admin').id;
          let role = ws.roles.find(userRole => userRole.name === 'admin');
          role.principals.create({
            principalType: RoleMapping.USER,
            principalId: id
          }, function(err) {
            if (err) {
              reject(err);
            }
            resolve(ws);
          });
        }
        ))
      .then(ws => {
        let id = ws.users.find(user => user.username === 'manager').id;
        let roleMappings = [];
        ws.roles.forEach(role => {
          if (role.name === 'admin') {
            return;
          }
          roleMappings.push(
            new Promise((resolve, reject) =>
              role.principals.create({
                principalType: RoleMapping.USER,
                principalId: id
              }, function(err) {
                if (err) {
                  reject(err);
                }
                resolve();
              })
            )
          );
        });
        return Promise.all(roleMappings).then(() => new Promise(resolve => resolve(ws)));
      })
      .then(ws => {
        let id = ws.users.find(user => user.username === 'guest').id;
        let roleMappings = [];
        ws.roles.forEach(role => {
          if (!role.name.endsWith('^R')) {
            return;
          }
          roleMappings.push(
            new Promise((resolve, reject) =>
              role.principals.create({
                principalType: RoleMapping.USER,
                principalId: id
              }, function(err) {
                if (err) {
                  reject(err);
                }
                resolve(ws);
              })
            )
          );
        });
        return Promise.all(roleMappings);
      })
      .then(() => {
        console.log('* Dummy Data is Ready');
        if (consoleUser.dataSource.connector.name != 'memory') {
          console.log('\x1b[41m** Re-run without MIGRATE for production\x1b[0m');
          // eslint-disable-next-line no-process-exit
          process.exit(0);
          // throw new Error{success: 'MIGRATION DONE!'};
        }
      });
  }

  return chain;
}
