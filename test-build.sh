REPO_ROOT=$(pwd)
maum_root=${MAUM_ROOT}
export REPO_ROOT

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set centos as default."
  OS=centos
fi

#--rest--
base=${REPO_ROOT}/rest
target=admin-rest
target_dir=${MAUM_ROOT}/apps/${target}
test -d ${target_dir} || mkdir -p ${target_dir}
(cd ${base} && \
 npm remove @angular/cli && \
 npm install && \
 npm run prod-sdk && \
 npm rebuild --target=8.15.1 --target_platform=linux --target_arch=x64 --target_libc=glibc --update-binary && \
 npm prune --production
)
(test -d ${target_dir} && sudo rm -rf ${target_dir})
(cd ${base} && rsync -ar package.json server node_modules ${target_dir})
if [ "${OS}" = "centos" ] && [ -z ${DOCKER_MAUM_BUILD} ]; then
  sudo chcon -Rv unconfined_u:object_r:var_t ${MAUM_ROOT}/apps
fi

##  #--web--
##  base=${REPO_ROOT}/web
##  target=admin-web
##  target_dir=${MAUM_ROOT}/apps/${target}
##  sudo rm -rf ${target_dir}
##  cd ${REPO_ROOT}
##  test -d ${target_dir} || mkdir -p ${target_dir}
##  (cd ${base} && npm install)
##  (cd ${base} && node --max_old_space_size=12288 ./node_modules/.bin/ng build --prod --output-path=${target_dir} --output-hashing=all)
##  if [ "${OS}" = "centos" ] && [ -z ${DOCKER_MAUM_BUILD} ]; then
##   sudo chcon -Rv unconfined_u:object_r:httpd_sys_content_t ${MAUM_ROOT}/apps/${target}
##  else
##   sudo chown -R www-data.www-data ${MAUM_ROOT}/apps/${target}
##  fi
##  
