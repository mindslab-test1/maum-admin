create table ACL
(
	id int auto_increment primary key,
	model varchar(512) null,
	property varchar(512) null,
	accessType varchar(512) null,
	permission varchar(512) null,
	principalType varchar(512) null,
	principalId varchar(512) null
);

create table AccessToken
(
	id varchar(255) not null primary key,
	ttl int null,
	scopes text null,
	created datetime null,
	userId int null
);

create table ConsoleUser
(
	id int auto_increment primary key,
	username varchar(512) null,
	password varchar(512) not null,
	email varchar(512) not null,
	activated tinyint(1) null,
	lastUpdated datetime not null
);

create table Engine
(
	id int auto_increment primary key,
	name varchar(255) not null,
	categories text not null,
	bookmarkList text null,
	constraint name unique (name)
);

create table PasswordHistory
(
	id int auto_increment primary key,
	userId int null,
	password varchar(512) null,
	lastUpdated datetime null
);

create table Role
(
	id int auto_increment primary key,
	name varchar(512) not null,
	description varchar(512) null,
	created datetime null,
	modified datetime null
);

create table RoleMapping
(
	id int auto_increment primary key,
	principalType varchar(512) null,
	principalId varchar(255) null,
	roleId int null
);

create index principalId
	on RoleMapping (principalId);

create table SignInHistory
(
	id int auto_increment primary key,
	remote varchar(512) null,
	agent varchar(512) null,
	signed tinyint(1) null,
	msg varchar(512) null,
	username varchar(255) null,
	created datetime null
);

create index username
	on SignInHistory (username);

create table UserGroup
(
    id int auto_increment
    primary key,
    name varchar(512) null,
    ownerId int null
)
charset = utf8;

create table UserGroupMapping
(
    groupId int not null,
    userId int not null,
    userStatus varchar(10) null,
    userRole varchar(512) null,
    primary key (groupId, userId)
)
charset = utf8;

create table Workspace
(
    id int auto_increment
    primary key,
    type varchar(11) null,
    ownerId int null,
    createdId int null,
    createdAt datetime null,
    groupId int null
)
charset = utf8;
