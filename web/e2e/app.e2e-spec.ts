import {AdminWebPage} from './app.po';

describe('admin-web App', function () {
  let page: AdminWebPage;

  beforeEach(() => {
    page = new AdminWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
