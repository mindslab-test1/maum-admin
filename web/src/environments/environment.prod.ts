export const environment: {
  production: boolean,
  apiPath: string,
  apiBaseUrl: string,
  wsBaseUrl: string,
  msgCodeApiUrl: string,
  apiUrl: string,
  wsUrl: string,
  frontRestApiUrl: string,
  maumAiApiUrl: string
} = loadJSON('/assets/config/maum-admin-prod-config.json');
export const menuTree = loadJSON('/assets/config/maum-admin-menu-config.json');

function loadJSON(filePath) {
  const json = loadTextFileAjaxSync(filePath, 'application/json');
  let config = JSON.parse(json);
  config['apiUrl'] = config.apiPath;
  config['wsUrl'] = config.apiPath;
  config['frontRestApiUrl'] = config.apiPath;
  config['maumAiApiUrl'] = config.apiPath;
  return config;
}

function loadTextFileAjaxSync(filePath, mimeType) {
  const xmlhttp = new XMLHttpRequest();
  xmlhttp.open('GET', filePath, false);
  if (mimeType != null) {
    if (xmlhttp.overrideMimeType) {
      xmlhttp.overrideMimeType(mimeType);
    }
  }
  xmlhttp.send();
  if (xmlhttp.status === 200) {
    return xmlhttp.responseText;
  } else {
    return null;
  }
}
