// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment: {
  production: boolean,
  apiPath: string,
  apiBaseUrl: string,
  wsBaseUrl: string,
  msgCodeApiUrl: string,
  apiUrl: string,
  wsUrl: string,
  frontRestApiUrl: string,
  maumAiApiUrl: string
} = loadJSON('/assets/config/maum-admin-config.json');
export const menuTree = loadJSON('/assets/config/maum-admin-menu-config.json');

function loadJSON(filePath) {
  const json = loadTextFileAjaxSync(filePath, 'application/json');
  let config = JSON.parse(json);
  if (!config.apiBaseUrl) {
    config['apiUrl'] = config.apiPath;
  }
  if (!config.apiBaseUrl) {
    config['wsUrl'] = config.apiPath;
  }
  if (!config.frontRestApiUrl) {
    config['frontRestApiUrl'] = config.apiPath;
  }
  if (!config.maumAiApiUrl) {
    config['maumAiApiUrl'] = config.apiPath;
  }

  return config;
}

function loadTextFileAjaxSync(filePath, mimeType) {
  const xmlhttp = new XMLHttpRequest();
  xmlhttp.open('GET', filePath, false);
  if (mimeType != null) {
    if (xmlhttp.overrideMimeType) {
      xmlhttp.overrideMimeType(mimeType);
    }
  }
  xmlhttp.send();
  if (xmlhttp.status === 200) {
    return xmlhttp.responseText;
  } else {
    return null;
  }
}
