import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material';
import {MltAuthService} from './mlt.auth.service';
import {AlertComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';

@Injectable()
export class MltAuthGuard implements CanActivate {

  modulePath: string;

  constructor(private router: Router,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog,
              private mltAuthService: MltAuthService) {
  }

  getModulePath() {
    return this.modulePath;
  }

  setModulePath(_modulePath: string) {
    this.modulePath = _modulePath;
  }

  // #@ mlt.authguard.ts에서 workspaceid, mltApiUrl, userRole 없는경우 먼저조회해오도록 수정
  setUserInfoSetting() {
    this.mltAuthService.getAllWorkspace().subscribe(workspaceRes => {
      this.storage.set('workspace', workspaceRes);
      this.storage.set('workspaceId', workspaceRes[0].id);
      this.mltAuthService.getUserById().subscribe(res => {
        res['userRoleRelEntities'].forEach(userRoleRelEntity => {
          if (userRoleRelEntity.roleEntity.workspaceId === workspaceRes[0].id) {
            this.storage.set('userRole', userRoleRelEntity.roleEntity.menuRoleEntities);
          }
        });
      })
    })
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;

    const mltApiUrl = this.storage.get('mltApiUrl');
    if (mltApiUrl === null || mltApiUrl === undefined) {
      this.storage.set('mltApiUrl', 'http://127.0.0.1:9881/api')
    }
    const userRole = this.storage.get('userRole');
    const workspaceId = this.storage.get('m2uWorkspaceId');
    if (userRole === null || userRole === undefined || workspaceId === null || workspaceId === undefined) {
      this.setUserInfoSetting();
    }
    let hasMenueRole = false;
    let confUrl = '';
    let statRoot = state.root.firstChild;
    while (statRoot.routeConfig) {
      confUrl += statRoot.routeConfig.path === '' ? '' : '/' + statRoot.routeConfig.path;

      if (statRoot.firstChild !== null) {
        statRoot = statRoot.firstChild;
      } else {
        this.modulePath = confUrl;
        break;
      }
    }

    let urls = state.url.split('/');

    // 빈 공백 제거
    urls.forEach((url, index) => {
      if (url.trim() === '') {
        urls.splice(index, 1);
      }
    });

    // 모듈 까지의 path 가져옴
    let moduleUrl = `/${urls[0]}/${urls[1]}`;
    this.setModulePath(moduleUrl);
    let userPaths = [];

    if (userRole !== null) {
      userRole.forEach((menu) => {
        const path = menu.menuEntity.path;

        let regex = new RegExp(`${moduleUrl}/*`);
        if (regex.test(path)) {
          userPaths.push(path);
        }

        if (confUrl === path) {
          hasMenueRole = true;
        }
      });

      hasMenueRole = hasMenueRole;
    } else {
      hasMenueRole = false;
    }
    return true;
    // if (true) {
    //   if (userPaths.length > 0) {
    //
    //     // Angular xx.route.ts 에서 명시된 처음 path 랑  db 에 path 값을 비교하여 데이터가 일치하지 않을시에
    //     // db 값에 0 번째 항목의 Path 값으로 navigate 해준다.
    //     if (!userPaths.find(userPath => userPath === confUrl)) {
    //       this.router.navigateByUrl(userPaths[0]);
    //       return true;
    //     }
    //
    //     // TODO dashboard 고정 체크
    //     if (confUrl === this.modulePath && confUrl !== '/mlt/dashboard') {
    //       this.router.navigateByUrl(userPaths[0]);
    //       return true;
    //     }
    //     return true;
    //   } else {
    //     if (!hasMenueRole) {
    //       console.log(123123);
    //       this.openAlertDialog('Failed', 'You don\'t have permission to access', 'error');
    //       return false;
    //     }
    //   }
    // }
    //
    // return false;
  }

  canActivateChild(next: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;

    const userRole = this.storage.get('userRole');
    let hasMenueRole = false;
    let confUrl = '';

    let statRoot = state.root.firstChild;
    while (statRoot.routeConfig) {
      confUrl += statRoot.routeConfig.path === '' ? '' : '/' + statRoot.routeConfig.path;
      if (statRoot.firstChild !== null) {
        statRoot = statRoot.firstChild;
      } else {
        break;
      }
    }

    let urls = state.url.split('/');

    // 빈 공백 제거
    urls.forEach((url, index) => {
      if (url.trim() === '') {
        urls.splice(index, 1);
      }
    });

    // 모듈 까지의 path 가져옴
    let moduleUrl = `/${urls[0]}/${urls[1]}`;
    this.setModulePath(moduleUrl);
    let userPaths = [];

    if (userRole !== null) {
      userRole.forEach((menu) => {
        const path = menu.menuEntity.path;

        let regex = new RegExp(`${moduleUrl}/*`);
        if (regex.test(path)) {
          userPaths.push(path);
        }

        if (confUrl === path) {
          hasMenueRole = true;
        }
      });

      hasMenueRole = hasMenueRole;
    } else {
      hasMenueRole = false;
    }

    if (true) {
      // if (userPaths.length > 0) {
      //   // Angular xx.route.ts 에서 명시된 처음 path 랑  db 에 path 값을 비교하여 데이터가 일치하지 않을시에
      //   // db 값에 0 번째 항목의 Path 값으로 navigate 해준다.
      //   if (!userPaths.find(userPath => userPath === confUrl)) {
      //     this.router.navigateByUrl(userPaths[0]);
      //     return true;
      //   }
      //
      //   // dashboard가 아닐시에 0번째 항목으로 navigate 해준다.
      //   if (confUrl === this.modulePath && confUrl !== '/mlt/dashboard') {
      //     this.router.navigateByUrl(userPaths[0]);
      //     return true;
      //   }
      //   return true;
      // } else {
      //   if (!hasMenueRole) {
      //     this.openAlertDialog('Failed', 'You don\'t have permission to access', 'error');
      //     return false;
      //   }
      // }
    }
    return true;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
