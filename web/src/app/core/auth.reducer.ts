import {
  AUTH_UPDATE
} from './actions';

const authDefault = {
  user: null
};

export function authReducer(state = authDefault, action) {
  switch (action.type) {
    case AUTH_UPDATE:
      return action.payload;

    default:
      return state;
  }
}
