import {ENGINE_DELETE, ENGINE_UPDATE, ENGINE_CREATE, ENGINE_UPDATE_ALL} from './actions';

export function EngineReducer(state = [], action) {
  switch (action.type) {
    case ENGINE_CREATE:
      return [action.payload, ...state];

    case ENGINE_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.name === item.name ? updatedItem : item);

    case ENGINE_UPDATE_ALL:
      return action.payload;

    case ENGINE_DELETE:
      let deletedId = action.payload;
      return state.filter(item => deletedId !== item.id);

    default:
      return state;
  }
}
