import {TestBed, ComponentFixture, inject} from '@angular/core/testing';
import {HttpErrorInterceptor} from './http-error.interceptor';
import {MatDialog, MatSnackBar} from '@angular/material';


describe('HttpError.Interceptor', () => {
  let component: HttpErrorInterceptor;
  let fixture: ComponentFixture<HttpErrorInterceptor>;
  let testBedService: MatDialog;
  let componentService: MatDialog;


  beforeEach(() => {
    // refine the test module by declaring the test component
    TestBed.configureTestingModule({
      declarations: [HttpErrorInterceptor],
      providers: [MatDialog, MatSnackBar]
    });

    // Configure the component with another set of Providers
    TestBed.overrideComponent(
      HttpErrorInterceptor,
      {
        set: {
          providers: [
            {provide: MatSnackBar},
            {provide: MatDialog}
          ]
        }
      }
    );

    // create component and test fixture
    fixture = TestBed.createComponent(HttpErrorInterceptor);

    // get test component from the fixture
    component = fixture.componentInstance;

    // AuthService provided to the TestBed
    testBedService = TestBed.get(MatDialog);

    // AuthService provided by Component, (should return MockAuthService)
    componentService = fixture.debugElement.injector.get(MatDialog);

  });
  it('should create an instance', () => {
    expect(component instanceof HttpErrorInterceptor).toBeTruthy();
  });
});
