import {Injectable} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent, HttpErrorResponse
} from '@angular/common/http';

import {EMPTY, Observable, throwError} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {ShowErrorService} from './show-error.service';
import {Router} from '@angular/router';
import {getErrorString} from '../shared';


@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private showError: ShowErrorService,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!request.headers.has('Content-Type')) {
      request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
    }

    return next.handle(request);
    /*return next.handle(request).pipe(
      // `PIPE`에서 에러에 대한 처리를 수행한다.
      catchError((error: HttpErrorResponse) => {
        console.log('HTTP error', error);

        // 상수가 없어서g 그냥 숫자로 씁니다.
        if (error.status === 401) {
          this.router.navigateByUrl('/auth/signin');
          // 이 경우에는 에러를 더 이상 다음으로 내보내지 않고 중지한다.
          return EMPTY;
        } else {
          let reason = getErrorString(error.error);
          let title = `HTTP Error ${error.status} ${error.statusText}`;
          this.showError.openDialog(title, reason);
          return throwError(error);
        }
      })
    );*/
  }
}
