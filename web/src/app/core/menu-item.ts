/**
 * 메뉴ITEM
 */
import {RouterLink} from '@angular/router';

export interface MenuItem {
  //
  // MENU DISPLAY
  //
  /**
   * 메뉴에 뿌려질 텍스트
   * 필수 필드
   */
  label?: string;
  /**
   * 메뉴의 ICON
   * 없을 수도 있다.
   */
  icon?: string;

  /**
   * 내용이 채워지면 badge를 보여준다.
   */
  badge?: string;

  /**
   * 메뉴 구분자로서만 동작할 것인가?
   */
  divider?: boolean;

  /**
   * 메뉴에 대한 설명
   */
  description?: string;

  /**
   * 메뉴객체 전체의 특이한 스타일 적용
   */
  styleClass?: string;

  /**
   * badge에 대한 스타일 적용
   */
  badgeStyleClass?: string;

  //
  // LINK OR TARGET
  //

  /**
   * 최종적으로 처리할 라우터 경로
   */
  routerLink?: any; // RouterPath 또는 문자열

  /**
   * routerLinkUrl
   */
  routerLinkUrl?: string;

  /**
   * 라우터 링크 옵션을 애당초 버튼에 걸어버릴 수 있다.
   */
  routerLinkActiveOptions?: any;

  /**
   * url: 클릭했을 때 외부로 이동할 곳
   */
  url?: string;

  /**
   * 페이지 링크로 이동할 경우에 대한 TARGET
   */
  target?: string;

  //
  // 메뉴 접근 권한에 관한 문제
  //

  /**
   * requestedRole
   *
   * 여러 개의 문자열이 들어가는 경우,
   * 해당 조건을 모두 만족해야만 쓸 수 있다.
   */
  roles?: string[];

  //
  // STATE
  //

  /**
   * 화면에 보이게 할 것인가 여부
   */
  visible?: boolean;


  /**
   * 현재 ACTIVE 상채인가?
   * 이게 ACTIVE 하면, 상위도 ACTIVE 할 수 있다.
   */
  selected?: boolean;

  /**
   * open
   * 현재의 하위 메뉴가 선택된 경우여서 자동으로 자기도 선택된 경우인가?
   */
  open?: boolean;

  /**
   * 펼처진 것인가?
   */
  expanded?: boolean;

  /**
   * option
   * 각 메뉴의 상황별 옵션을 지정한다.
   */
  option?: any;

  //
  // PARENT & CHILD REPATION SHIP
  //

  /**
   * 부모의 메뉴 객체
   * 전체 구조를 주입할 때, parent를 모두 계산하도록 한다.
   */
  parent?: MenuItem;

  /**
   * 하위 메뉴 ITEM
   */
  children?: MenuItem[];

  /**
   * 자식 중에 지난번에 선택되었던 메뉴
   */
  lastChild?: MenuItem;

  /**
   * `depth`는 주입될 때 계산된다.
   */
  depth?: number;
}
