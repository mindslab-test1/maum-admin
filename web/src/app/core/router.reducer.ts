import {ActivatedRouteSnapshot} from '@angular/router';
import {ROUTER_UPDATE, ROUTER_LOADED, ROUTER_LOADING} from './actions';
import {AppNavs} from '../app-routing.module';
import {environment} from '../../environments/environment';

const routerDefault = {
  components: '',
  url: '',
  navs: AppNavs.splice(0),
  queryParams: {},
  params: {},
  fragment: '',
  loading: true,
  hasNoLayout: true,
};

export function routerReducer(state = routerDefault, action) {

  switch (action.type) {
    case ROUTER_UPDATE:
      let route: ActivatedRouteSnapshot = action.payload;
      let routes: ActivatedRouteSnapshot[] = route.pathFromRoot;
      let updateState: any = {};

      // 전체 queryParams
      updateState.queryParams = route.queryParams;

      // 전체 params를 합침
      updateState.params = routes.reduce((obj, r) => Object.assign(obj, r.params), {});

      // 전체 component들
      updateState.components = routes.filter(r => !!r.component).map((r: any) => r.component.name).join('/');
      // console.log(updateState.components);

      // 컴포넌트 마운트, params 변화가 있으면
      let componentsUpdated = updateState.components !== state.components;
      let paramsUpdated = JSON.stringify(updateState.params) !== JSON.stringify(state.params);

      // https://github.com/mindslab-ai/minds-voc/issues/423
      if (environment.production || componentsUpdated || paramsUpdated) {

        // nav 상태를 업데이트, 비용상 mutable하게 다룸
        updateState.url = routes
        .map(r => r.url.map(url => url.path))
        .reduce((urls, url) => urls.concat(url), [])
        .filter(url => url !== '');

        updateNavs(state.navs, updateState.params, updateState.url);
        // console.log(JSON.stringify(state.navs, null, 2));

        // 로딩 상태로 갱신
        updateState.loading = componentsUpdated || state.loading;

        // 레이아웃 상태 갱신
        // let data: any = route.data;
        updateState.hasNoLayout = routes.some(r => {
          let data: any = r.data;
          return !!data.hasNoLayout;
        });
      }

      return Object.assign({}, state, updateState);

    case ROUTER_LOADING:
      return Object.assign({}, state, {loading: true});

    case ROUTER_LOADED:
      return Object.assign({}, state, {loading: false});

    default:
      return state;
  }
}

// routes, params를 받아서 navs의 active, open, realpaths을 갱신한다.
function updateNavs(navs: any[], params: any, realpaths: string[]) {
  navs.forEach(nav => {
    // realpaths 갱신
    nav.realpaths = nav.paths.map(path => {
      if (path[0] !== ':') {
        return path;
      }

      let name = path.substr(1);
      if (name in params) {
        return params[name];
      }

      return path;
    });

    // active 갱신
    nav.active = realpaths ? nav.realpaths.join('/') === realpaths.slice(0, nav.realpaths.length).join('/') : false;

    // open 갱신
    nav.open = nav.active;

    // 자식 재귀 호출
    if (nav.children) {
      updateNavs(nav.children, params, nav.active ? realpaths : null);
    }
  });
}
