import {ACNT_DELETE, ACNT_UPDATE, ACNT_CREATE, ACNT_UPDATE_ALL} from './actions';

export function accountReducer(state = [], action) {
  switch (action.type) {
    case ACNT_CREATE:
      return [action.payload, ...state];

    case ACNT_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.id === item.id ? updatedItem : item);

    case ACNT_UPDATE_ALL:
      return action.payload;

    case ACNT_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.id) === -1);

    default:
      return state;
  }
}
