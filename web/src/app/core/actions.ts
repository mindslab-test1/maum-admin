// actions for web-console
export const AUTH_UPDATE = 'AUTH_UPDATE';

export const ROUTER_UPDATE = 'ROUTER_UPDATE';
export const ROUTER_LOADED = 'ROUTER_LOADED';
export const ROUTER_LOADING = 'ROUTER_LOADING';

export const DAM_DELETE = 'DAM_DELETE';
export const DAM_CREATE = 'DAM_CREATE';
export const DAM_UPDATE = 'DAM_UPDATE';
export const DAM_UPDATE_ALL = 'DAM_UPDATE_ALL';

export const DA_DELETE = 'DA_DELETE';
export const DA_CREATE = 'DA_CREATE';
export const DA_UPDATE = 'DA_UPDATE';
export const DA_UPDATE_ALL = 'DA_UPDATE_ALL';

export const SC_DELETE = 'SC_DELETE';
export const SC_CREATE = 'SC_CREATE';
export const SC_UPDATE = 'SC_UPDATE';
export const SC_UPDATE_ALL = 'SC_UPDATE_ALL';

export const CB_DELETE = 'CB_DELETE';
export const CB_UPDATE = 'CB_UPDATE';
export const CB_CREATE = 'CB_CREATE';
export const CB_UPDATE_ALL = 'CB_UPDATE_ALL';

export const DAI_DELETE = 'DAI_DELETE';
export const DAI_UPDATE = 'DAI_UPDATE';
export const DAI_CREATE = 'DAI_CREATE';
export const DAI_UPDATE_ALL = 'DAI_UPDATE_ALL';

export const ITF_DELETE = 'ITF_DELETE';
export const ITF_UPDATE = 'ITF_UPDATE';
export const ITF_CREATE = 'ITF_CREATE';
export const ITF_UPDATE_ALL = 'ITF_UPDATE_ALL';

export const AP_DELETE = 'AP_DELETE';
export const AP_UPDATE = 'AP_UPDATE';
export const AP_CREATE = 'AP_CREATE';
export const AP_UPDATE_ALL = 'AP_UPDATE_ALL';

export const ACNT_DELETE = 'ACNT_DELETE';
export const ACNT_UPDATE = 'ACNT_UPDATE';
export const ACNT_CREATE = 'ACNT_CREATE';
export const ACNT_UPDATE_ALL = 'ACNT_UPDATE_ALL';

export const MODEL_DELETE = 'MODEL_DELETE';
export const MODEL_CREATE = 'MODEL_CREATE';
export const MODEL_UPDATE = 'MODEL_UPDATE';
export const MODEL_UPDATE_ALL = 'MODEL_UPDATE_ALL';

export const ENGINE_DELETE = 'ENGINE_DELETE';
export const ENGINE_UPDATE = 'ENGINE_UPDATE';
export const ENGINE_CREATE = 'ENGINE_CREATE';
export const ENGINE_UPDATE_ALL = 'ENGINE_UPDATE_ALL';

export const HMD_DELETE = 'HMD_DELETE';
export const HMD_CREATE = 'HMD_CREATE';
export const HMD_UPDATE = 'HMD_UPDATE';
export const HMD_UPDATE_ALL = 'HMD_UPDATE_ALL';

