import {Inject, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {Observable} from 'rxjs';
import * as CryptoJS from 'crypto-js';
import {HttpClient} from '@angular/common/http';

import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class MltAuthService {
  API_URL;

  constructor(private http: HttpClient, private router: Router,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private datePipe: DatePipe) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllWorkspace() {
    return this.http.get(this.API_URL + '/auth/get-all-workspace/');
  }

  getUserById() {
    return this.http.get(this.API_URL + '/user/id/admin');
  }

  // signin(auth: Auth): Observable<any> {
  //   let data = `!mlt500#00!00429`;
  //   let key = CryptoJS.enc.Utf8.parse(data);
  //   let iv = CryptoJS.enc.Utf8.parse(data);
  //   let encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(auth.password), key,
  //     {
  //       keySize: 256 / 8,
  //       iv: iv,
  //       mode: CryptoJS.mode.CBC,
  //       padding: CryptoJS.pad.Pkcs7
  //     });
  //
  //   let decrypted = CryptoJS.AES.decrypt(encrypted, key, {
  //     keySize: 256 / 8,
  //     iv: iv,
  //     mode: CryptoJS.mode.CBC,
  //     padding: CryptoJS.pad.Pkcs7
  //   });
  //
  //   // Save the Login in to an array
  //   let secureUsercreds = {id: auth.id, password: encrypted.toString()};
  //
  //   const apiUrl = this.storage.get('mltApiUrl') + '/auth/signin';
  //
  //   return this.http.post(apiUrl, secureUsercreds)
  //   .map(response => {
  //     const res = response;
  //     if (res['message'] === 'LOGIN_SUCCESS') {
  //       this.acceptAuthenticated();
  //       this.storage.set('users', res['users']);
  //       this.storage.set('workspaces', res['workspaces']);
  //       res['users'].userRoleRelEntities.forEach(userRoleRelEntity => {
  //         if (userRoleRelEntity.roleEntity.workspaceId === res['workspaces'][0].id) {
  //           this.storage.set('userRole', userRoleRelEntity.roleEntity.menuRoleEntities);
  //         }
  //       });
  //     } else {
  //       this.rejectAuthenticated();
  //     }
  //     return res;
  //   });
  // }
  //
  // signOut(): any {
  //   return new Promise(resolve => {
  //     this.rejectAuthenticated();
  //     this.storage.remove('users');
  //     this.storage.remove('workspaces');
  //     this.storage.remove('userRole');
  //     this.storage.remove('workspaceId');
  //     this.goHome();
  //     resolve();
  //   });
  // }

  // goHome(): void {
  //   if (this.isAuthenticated()) {
  //     this.router.navigateByUrl('/mlt/dashboard');
  //   } else {
  //     this.router.navigateByUrl('/auth/signin');
  //   }
  // }

  // protected acceptAuthenticated(): void {
  //   this.storage.set('isAuthenticated', true);
  // }
  //
  // protected rejectAuthenticated(): void {
  //   this.storage.set('isAuthenticated', false);
  // }

  isAuthenticated(): Observable<boolean> {
    return this.storage.get('isAuthenticated');
  }
}
