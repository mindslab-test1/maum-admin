import {Injectable, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router, RouterState} from '@angular/router';
import {MenuItem} from './menu-item'
import {filter, map, mergeMap} from 'rxjs/operators';

function injectParent(item: MenuItem): void {
  if (!item.depth) {
    item.depth = 1;
  }
  if (item.children) {
    item.children.forEach((child) => {
      child.parent = item;
      child.depth = (child.parent.depth + 1);
      injectParent(child);
    });
  } else {
    item.children = []
  }
}


@Injectable({
  providedIn: 'root'
})
export class MenuService {
  /**
   * 현재 메뉴 서비스의 Scheme을 지정합니다.
   */
  private scheme: string;

  /**
   * 메뉴 계층 구조
   */
  private tree: MenuItem[];

  /**
   * 현재의 ActiveMenu
   */
  private current: MenuItem = null;

  /**
   * 현재 선택된 메뉴의 트리 구조
   */
  private currentPathTree: MenuItem[] = [];

  /**
   * LAYOUT을 보여줄지 말지
   */
  private _hasNoLayout = false;

  /**
   * SIDEBAR를 보여줄지 말지
   */
  private _hasNoSidebar = false;

  /**
   * 생성자
   * @param router Router 모듈의 Router Injection
   */
  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route: ActivatedRoute) => {
        // console.log('activated route', route);
        // console.log('ACTIVATED ROUTE', route, route.firstChild);
        console.log('TREE routeConfig', 0, route.routeConfig);
        if (route.routeConfig) {
          console.log('TREE Path', 0, '[', route.routeConfig.path, ']');
        }
        // console.log('TREE', route.data);
        route = route.firstChild;
        let idx = 1;
        while (route.firstChild) {
          // console.log('ACTIVATED ROUTE', route, route.firstChild);
          console.log('TREE routeConfig', idx, route.routeConfig);
          if (route.routeConfig) {
            console.log('TREE Path', idx, '[', route.routeConfig.path, ']');
          }
          // console.log('TREE', route.data);
          route = route.firstChild;
          idx++;
        }
        // console.log('FINAL', route);
        // route.data.
        return route;
        // route.firstChild
      }),
      mergeMap((route) => route.data),
    ).subscribe((data) => {
      console.log('ROUTER EVENT data', data);
      if (data) {
        if ('hasNoLayout' in data) {
          this._hasNoLayout = data['hasNoLayout'];
        } else {
          this._hasNoLayout = false;
        }
        if ('hasNoSidebar' in data) {
          this._hasNoSidebar = data['hasNoSidebar'];
        } else {
          this._hasNoSidebar = false;
        }
      } else {
        this._hasNoLayout = false;
        this._hasNoSidebar = false;
      }
      ;
    });

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((event: NavigationEnd) => event.url)
    ).subscribe(url => {
      this.scan(url);
    });

    /*
          .filter((event) => event instanceof NavigationEnd)
          .map(() => this.activatedRoute)
          .map((route) => {
            console.log('activated route', route);
            while (route.firstChild) {
              console.log('ACTIVATED ROUTE', route, route.firstChild);
              route = route.firstChild;
            }
            console.log('FINAL', route);
            return route;
          })
          .filter((route) => route.outlet === 'primary')
          .mergeMap((route) => route.data)
          .subscribe((data) => {
            console.log('ROUTER EVENT data', data);
            if (data) {
              if ('hasNoLayout' in data) {
                this._hasNoLayout = data['hasNoLayout'];
              } else {
                this._hasNoLayout = false;
              }
              if ('hasNoSidebar' in data) {
                this._hasNoSidebar = data['hasNoSidebar'];
              } else {
                this._hasNoSidebar = false;
              }
            } else {
              this._hasNoLayout = false;
              this._hasNoSidebar = false;
            };
          });
    */

    /*
        this.router.events.pipe(filter(event => event instanceof NavigationEnd))
          .subscribe((event: NavigationEnd) => {
            console.log('====== MenuService EVENT', event);
            console.log('====== route State', this.router.routerState);
            console.log('====== active route', this.activatedRoute);

            let data = this.activatedRoute.firstChild.routeConfig.data;
            if (data) {
              if ('hasNoLayout' in data) {
                this._hasNoLayout = data['hasNoLayout'];
              } else {
                this._hasNoLayout = false;
              }
              if ('hasNoSidebar' in data) {
                this._hasNoSidebar = data['hasNoSidebar'];
              } else {
                this._hasNoSidebar = false;
              }
            } else {
              this._hasNoLayout = false;
              this._hasNoSidebar = false;
            };
          });
    */

  }

  hasNoSidebar() {
    return this._hasNoSidebar;
  }

  setNoSidebar(v: boolean): void {
    this._hasNoSidebar = v;
  }

  hasNoLayout(): boolean {
    return this._hasNoLayout;
  }

  setNoLayout(v: boolean): void {
    this._hasNoLayout = v;
  }

  getTree(): MenuItem[] {
    return this.tree;
  }

  getSchme(): string {
    return this.scheme;
  }

  setScheme(scheme: string, tree: MenuItem[]) {
    if (tree !== null) {
      // 모든 TREE에 대해서 부모 자식 관계를 만들어 낸다.
      tree.forEach((item) => injectParent(item));

      // 이것은 가장 왼쪽의 메뉴는 없지만,
      // 화면은 나오는 경우에 대한 처리를 하기 위해서
      // 두는 방법이다.
      // 두번째 STEP의 메뉴는 나오지만 첫번째 선책은 아무것도 되어 있지 않다.
      tree.push({
        label: 'hidden',
        visible: false,
        children: null,
        depth: 1,
        selected: false,
        routerLink: null,
      });
      this.tree = tree;
    }
  }


  scan(url: string): void {
    function isMatchUrl(item: MenuItem, url2: string): boolean {
      if (item.routerLink) {
        // routerLinkUrl 값을 계산한다.
        // 한번 해 놓으면 영원히 유지된다.
        if (!item.routerLinkUrl) {
          if (item.routerLink instanceof Array) {
            let linkUrl = item.routerLink.join('/');
            item.routerLinkUrl = `/${linkUrl}`;
          }
        }
        return url2.indexOf(item.routerLinkUrl) === 0;
      } else {
        return false;
      }
    }

    // `depth first scan`을 수행하면서
    // 모든 TREE를 찾아다니면서 일치하는 놈들을 찾는다.
    function deepScan(item: MenuItem, url3, acc: any[]) {
      if (item.children && item.children.length > 0) {
        item.children.forEach((ii) => deepScan(ii, url3, acc));
      }
      if (isMatchUrl(item, url3)) {
        acc.push(item);
      }
    }

    // 이미 선택된 메뉴가 있고, URL이 현재의 URL 체계와 일치하면
    // 더 이상 SCAN을 하지 않는다.
    if (this.current && isMatchUrl(this.current, url)) {
      console.log('SCAN match current', this.current, url);
      return;
    }

    let found: MenuItem[] = [];
    this.tree.forEach((ii) => deepScan(ii, url, found));

    // 발견된 것이 있을 경우에 가장 긴 경로를 선택한다.
    if (found.length > 0) {
      found.sort((a: MenuItem, b: MenuItem) => {
        if (a.routerLinkUrl.length > b.routerLinkUrl.length) {
          return 1;
        }
        if (a.routerLinkUrl.length < b.routerLinkUrl.length) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });

      // 이 menu는 이미 선택된 것이다.
      console.log('SCAN FOUND', found[0], url, 'found LEN', found.length);
      this.select(found[0], true);
    } else {
      console.error('SCAN NOT FOUND', url);
    }
  }

  /**
   * 모든 자식들을 현재의 메뉴에 추가해준다.
   *
   * @param at 메뉴를 추가할 위치
   * @param children
   */
  setChildren(at: MenuItem, children: MenuItem[]): void {
    this.clearChildren(at);
    children.forEach((v) => {
      this.addChild(at, v)
    });
  }

  /**
   * ChildMenu를 하나 추가한다.
   *
   * @param at 추가할 메뉴 Item을 지정한다.
   * @param item 새로운 메뉴 ITEM
   */
  addChild(at: MenuItem, item: MenuItem): void {
    item.parent = at;
    item.depth = at.depth + 1;
    at.children.push(item);
  }

  clearChildren(at: MenuItem) {
    console.log('clear Children', at, at.children.length, at.children);
    at.children = [];
  }

  getCurrent(): MenuItem {
    return this.current;
  }

  getCurrentPathTree(): MenuItem[] {
    return this.currentPathTree;
  }

  select(at: MenuItem, skip = false): void {
    console.log('selected item ', at);
    if (skip === false) {
      this._hasNoSidebar = false;
    }
    // 만일 MenuItem이 단순히 `SUB MENU`를 위한 메뉴일 뿐이라면
    // 자식 메뉴 중의 첫번째 메뉴를 선택하도록 한다.
    // 가장 먼저 routerLink를 가지고 있는 메뉴를 선택한다.
    if (!at.routerLink && !at.url) {
      if (at.children.length > 0) {
        // 지난번에 선택된 자식을 기본으로 가져온다.
        let found = at.lastChild;
        if (!found) {
          // 지난번에 선택했던 자식이 없으면
          found = at.children.find(elem => {
            return elem.routerLink !== null;
          });
        }
        if (found) {
          console.log('in select', 'found', found);
          this.select(found);
          this.router.navigate(found.routerLink);
          return;
        } else {
          console.error('OOPS', 'NOT FOUND link menu', at);
        }
      }
    }

    this.current = at;
    //at.selected = true;
    let temp: MenuItem[] = [];
    let p = at;
    while (p !== null) {
      //p.selected = true;
      temp.push(p);

      if (p.parent) {
        // 지난번의 마지막 자식을 지정해준다.
        p.parent.lastChild = p;
        p = p.parent;
      } else {
        p = null;
      }
    }
    this.currentPathTree = temp.reverse();
  }

}
