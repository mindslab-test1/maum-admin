import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {SDKBrowserModule} from './sdk/index';
import {CookieModule} from 'ngx-cookie';
import {CsvService} from 'angular2-json2csv';
import {StoreModule} from '@ngrx/store';
import {
  accountReducer,
  apReducer,
  AuthGuard,
  authReducer,
  AuthService,
  cbReducer,
  ClipboardService,
  daiReducer,
  damReducer,
  daReducer,
  EngineReducer,
  itfReducer,
  routerReducer,
  scReducer,
  SDKService,
} from './index';
import {MltAuthGuard} from 'app/core/mlt.auth.guard';
import {MltAuthService} from './mlt.auth.service';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {SharedModule} from 'app/shared/shared.module';
import {MenuService} from './menu.service';
import {ShowErrorService} from './show-error.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpErrorInterceptor} from './http-error.interceptor';
import {DialogDesignService} from '../modules/dialog-model-design/design/services/dialog-design.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CookieModule.forRoot(),
    SDKBrowserModule.forRoot(), // loopback SDK

    // reducers
    StoreModule.forRoot({
      router: routerReducer,
      auth: authReducer,
      dams: damReducer,
      das: daReducer,
      cbs: cbReducer,
      scs: scReducer,
      dais: daiReducer,
      itfs: itfReducer,
      aps: apReducer,
      accounts: accountReducer,
      engines: EngineReducer,
      //skills: SkillReducer,
    }),
    SharedModule

    // dev tool
    // StoreDevtoolsModule.instrumentOnlyWithExtension(), // http://zalmoxisus.github.io/redux-devtools-extension/
  ],
  exports: [],
  providers: [
    AuthGuard,
    AuthService,
    CsvService,
    ClipboardService,
    ShowErrorService,
    SDKService,
    MltAuthGuard,
    MltAuthService,
    StorageBrowser,
    DatePipe,
    MenuService,
    DialogDesignService,
    {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true}
  ],
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
