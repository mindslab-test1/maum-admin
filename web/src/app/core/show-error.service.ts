import {Injectable} from '@angular/core';
import {ErrorDialogComponent} from '../shared';
import {MatDialog, MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ShowErrorService {

  constructor(private dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  openDialog(title: string, message: string): void {
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      width: '300px',
      data: {title, message}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      let animal;
      animal = result;
    });
  }

  showSnack(message: string,
            action: string,
            duration = 3000,
            config?: MatSnackBarConfig): void {
    let localConfig: MatSnackBarConfig = {};
    if (config) {
      localConfig = {...config, ...{duration}}
    } else {
      localConfig = {duration}
    }
    const snackRef = this.snackBar.open(message, action, localConfig);

    snackRef.afterDismissed().subscribe(dismiss => {
      console.log('snack dismiss', dismiss);
    });
    snackRef.onAction().subscribe(() => {
      console.log('on action');
    });
  }

}
