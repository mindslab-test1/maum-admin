import {CB_DELETE, CB_UPDATE, CB_CREATE, CB_UPDATE_ALL} from './actions';

export function cbReducer(state = [], action) {
  switch (action.type) {
    case CB_CREATE:
      return [action.payload, ...state];

    case CB_UPDATE:
      let updatedItem = action.payload;
      return state.map(item => updatedItem.name === item.name ? updatedItem : item);

    case CB_UPDATE_ALL:
      return action.payload;

    case CB_DELETE:
      let deletedIds = action.payload;
      return state.filter(item => deletedIds.indexOf(item.name) === -1);

    default:
      return state;
  }
}
