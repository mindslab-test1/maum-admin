import {Inject, Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {ROUTER_UPDATE} from 'app/core/actions';
import {MltAuthService} from 'app/core/mlt.auth.service';
import {StorageBrowser} from 'app/shared/storage/storage.browser';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  // init: Observable<any> = null;

  constructor(private store: Store<any>,
              private router: Router,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private mltAuthService: MltAuthService,
              private authService: AuthService) {
    // sync auth state at first load, and delay canActivate,
    // because canActivate needs auth info to check ACL of routes

    // this.init = fromPromise(
    //   new Promise(resolve => {
    //     this.authService.sync(resolve);
    //   })
    // );
  }

  // workspaces, workspaceId 추가
  setUserInfoSetting() {
    this.mltAuthService.getAllWorkspace().subscribe(workspaceRes => {
      this.storage.set('workspace', workspaceRes);
      this.storage.set('workspaceId', workspaceRes[0].id);
      console.log('=============', workspaceRes[0].id);
      this.mltAuthService.getUserById().subscribe(res => {
        res['userRoleRelEntities'].forEach(userRoleRelEntity => {
          if (userRoleRelEntity.roleEntity.workspaceId === workspaceRes[0].id) {
            this.storage.set('userRole', userRoleRelEntity.roleEntity.menuRoleEntities);
          }
        });
      })
    })
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
    // console.log('canActivate triggered for', route.routeConfig);

    // mlt workspaces, workspaceId가 없는 경우 생성로직
    const mltApiUrl = this.storage.get('mltApiUrl');
    if (mltApiUrl === null || mltApiUrl === undefined) {
      this.storage.set('mltApiUrl', 'http://127.0.0.1:9881/api')
    }
    const userRole = this.storage.get('userRole');
    const workspaceId = this.storage.get('m2uWorkspaceId');
    if (userRole === null || userRole === undefined || workspaceId === null || workspaceId === undefined) {
      this.setUserInfoSetting();
    }
    // path를 추출(예 : m2u/dashboard)
    let paths = route.pathFromRoot
    .map(m => m.url)
    .reduce((arr, url) => arr.concat(url), [])
    .map(url => url.path);

    // console.log('#@ canActivate route :', route, ', state :', state, ', paths :', paths.join('/'));

    // authService.check() 호출
    return this.authService.check().toPromise()
    .then(user => {
      // console.log('check promise users', users);

      // check router's data.ACL configuration
      let data: any = route.data;
      let params: any = route.root.firstChild.params;

      // role이 저장된 data.ACL로 권한 체크를 한다
      let passed = this.authService.checkWithACL(data.ACL);
      // console.log('AuthGuard resolved passed :', passed, ', route :', route, ', data.ACL :', data.ACL);

      // 권한 체크에 실패한 경우(checkWithACL)
      if (passed === false) {
        // failed to access
        // if data.fallback is exists
        // fallback 경로가 있는지 체크
        if (!!data.fallback && data.fallback instanceof Array) {
          let parentPaths = route.parent.pathFromRoot
          .map(m => m.url)
          .reduce((arr, url) => arr.concat(url), [])
          .map(url => url.path);
          let fallbackPaths = parentPaths.concat(data.fallback);

          // falback에 등록된 url로 이동합니다 (m2u/denied)
          console.log('AuthGuard fallback:', fallbackPaths.join('/'));
          this.router.navigateByUrl('denied');
          return false;
        }

        console.error('AuthGuard stuck');
        this.authService.goHome();
        return false;
      } else {
        // update router state
        this.store.dispatch({type: ROUTER_UPDATE, payload: route});
        return true;
      }
    })
    .catch(err => {
      let url = paths.join('/');

      if (url === 'auth' || url === 'auth/signin') {
        return true;
      }
      console.error('get current get err', err);
      this.router.navigateByUrl('/auth/signin');
      return false;
    });
  }

  // canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
  //   return this.canActivate(route, state);
  // }
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }
}
