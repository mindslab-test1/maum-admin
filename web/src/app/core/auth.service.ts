import {map} from 'rxjs/operators';
import {Component, Injectable, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {ConsoleUserApi, LoopBackAuth} from '../core/sdk/index';
import {AUTH_UPDATE} from './actions';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material';
import {tap} from 'rxjs/internal/operators/tap';
import {switchMap} from 'rxjs/internal/operators/switchMap';
import {GroupsService} from '../admin/services/groups/groups.service';

const DefaultSeconds = 30 * 60;

@Injectable()
export class AuthService {
  static ROLE = {
    EVERYONE: '$everyone',
    UNAUTHENTICATED: '$unauthenticated',
    AUTHENTICATED: '$authenticated',
    ADMIN: 'admin',
    CREATE: '^C',
    READ: '^R',
    UPDATE: '^U:block', // 임시로 막음.
    DELETE: '^D',
    EXECUTE: '^X:block', // 임시로 막음.
  };

  /**
   * Check whether the role is allowed.
   * @param {id: string|type: string} id or role type.
   * @param {type: string|role: string[]} role type or array of roles.
   * @param {roles: string[]|null} array of roles or null.
   * @return {boolean} The resulting allowed.
   */
  static isAllowed(id: any, type: any, roles?: any): boolean {
    if (arguments.length === 3) {
      return roles[id + type] === true || roles[AuthService.ROLE.ADMIN] === true;
    } else if (arguments.length === 2) {
      return arguments[1][arguments[0]] === true;
    }
  }

  /**
   * Check whether the role is not allowed.
   * @param {id: string|type: string} id or role type.
   * @param {type: string|role: string[]} role type or array of roles.
   * @param {roles: string[]|null} array of roles or null.
   * @return {boolean} The resulting allowed.
   */
  static isNotAllowed(id: any, type: any, roles?: any): boolean {
    if (arguments.length === 3) {
      return roles[id + type] !== true && roles[AuthService.ROLE.ADMIN] !== true;
    } else if (arguments.length === 2) {
      return arguments[1][arguments[0]] !== true;
    }
  }

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private userApi: ConsoleUserApi,
              private authApi: LoopBackAuth,
              private groupsService: GroupsService,
              private snackBar: MatSnackBar) {
  }

  isTokenValid: any = (ticks: number = Date.now()): boolean => {
    let token = this.authApi.getToken();
    return !!token.id && ticks < new Date(token.created).getTime() + token.ttl * 1000;
  };

  // validateToken() {
  //   if (!this.isTokenValid()) {
  //     this.clear();
  //   }
  // }

  elongateTokenTTL: any = (seconds: number = DefaultSeconds): void => {
    let token = this.authApi.getToken();
    if (token.id) {
      token.created = new Date().toISOString();
      token.ttl = seconds;
      this.authApi.setToken(token);
    }
  };

  // 권한 체크에 범용적으로 이용 할 수 있는 메소드
  // check(rule: (auth: any, router: any) => boolean): Observable<boolean> {
  //   return this.store.select(s => s).map(s => rule(s.auth, s.router));
  // }

  check(): Observable<any> {
    return this.userApi.getCurrent();
  }

  checkWithACL(acl: any = []): boolean {
    // console.log('#@ acl :', acl);
    let roles = acl.roles;

    if (!roles || roles.length === 0) {
      // return Observable.of(true);
      return true
    }

    // #@ 2018.12.06 SDKToken이 사라지는 이슈 때문에 임시 주석처리
    // this.validateToken();

    // return this.check((auth, router) => {
    let user = JSON.parse(localStorage.getItem('user'));
    // console.log('check acl users:', users, ', roles:', roles);
    let passed = roles.some(role => {
      // console.log('#@ current role :', role);
      switch (role) {
        case AuthService.ROLE.EVERYONE:
          return true;

        case AuthService.ROLE.UNAUTHENTICATED:
          return !user;

        case AuthService.ROLE.AUTHENTICATED:
          return !!user;

        default:
          // admin의 경우에는 true를 return 합니다
          // 그외 USER는 사용자의 RoleMapping과 현재 role을 체크합니다
          return !!user && user.roles &&
            (user.roles[AuthService.ROLE.ADMIN] === true || user.roles[role] === true);
      }
    });

    // if (passed) {
    //   this.elongateTokenTTL();
    // }

    return passed;
    // });
  }

  /**
   * 로그인이 완료 되면 Home으로 이동합니다
   * */
  goHome(): void {
    // this.store.select(s => s.auth).take(1)
    // .subscribe(auth => {

    // localStorage에서 users 정보를 갖고 옵니다
    let user = JSON.parse(localStorage.getItem('user'));
    console.log('#@ go home users :', user);

    /////////////////////////////////////////////////////////////////////////////
    // // #@ 로그인이 완료되면 UserGroup과 Workspace를 조회하여 설정한다
    // // 1.UserGroup 조회
    // let tempGroupId = '';
    // let tempUserId = users.id
    // let queryForUserGroup = {
    //   'queryParams': {
    //     'userId': tempUserId
    //   }
    // };
    // this.consoleUserApi.getUserGroup(queryForUserGroup).subscribe(
    //   resultSet => {
    //     // console.log('#@ resultSet :', resultSet);
    //     localStorage.setItem('m2uUserGroup', JSON.stringify(resultSet));
    //
    //     // console.log('#@ resultSet[0].groupId :', resultSet[0].groupId);
    //     tempGroupId = resultSet[0].groupId;
    //
    //     // #@ 2.Workspace 조회
    //     let queryForWorkspace = {
    //       'queryParams': {
    //         'groupId': tempGroupId
    //       }
    //     };
    //     this.consoleUserApi.getWorkspace(queryForWorkspace).subscribe(
    //       resultSet => {
    //         // console.log('#@ resultSet :', resultSet);
    //         localStorage.setItem('m2uWorkspace', JSON.stringify(resultSet));
    //       }, err => {
    //         console.log('#@ getWorkspace err :', err);
    //       }
    //     );
    //     }, err => {
    //       console.log('#@ getUserGroup err :', err);
    //     }
    // );
    /////////////////////////////////////////////////////////////////////////////

    if (user) {
      let freshDaysBy = 60;
      let userId = user.id;
      // this.consoleUserApi.isPasswordStale(this.consoleUserApi.getCurrentId(), freshDaysBy)
      this.userApi.isPasswordStale(userId, freshDaysBy)
      .subscribe(
        res => {
          // console.debug('go home, is password stale success', res);
          if (res) {
            // 로그인 성공이후 dashboard로 이동
            // this.router.navigateByUrl('m2u/dashboard');
            // 로그인 성공이후 workspaces 선택 페이지로 이동 (workspace가 없으면 더이상 메뉴이동이 되지 않는다)
            // this.router.navigateByUrl('m2u-builder/profile/workspaces');
            this.router.navigateByUrl('dashboard');
          } else {
            // 패스워드 변경 권고사항을 전달합니다
            this.snackBar.open(`It's been over 90 days since you changed
              your password. Change password please.`, 'Confirm', {duration: 10000});

            this.router.navigateByUrl('m2u-builder/profile');
          }
        },
        err => {
          // console.debug('go home, is password stale failed', err);
          console.error(err.message);
        });
    } else {
      // console.debug('go home, users not found');
      this.router.navigateByUrl('auth/signin');
    }
  }

  // authentication methods
  clear(): void {
    this.authApi.clear();
    this.store.dispatch({type: AUTH_UPDATE, payload: {user: null}});
  }

  sync(callback: (user: any) => void = null): void {
    // this.validateToken();
    let user = this.userApi.isAuthenticated() && this.userApi.getCachedCurrent() || null;

    // failed to fetch users
    if (!!user && user.activated) {
      // this.elongateTokenTTL();
      localStorage.setItem('user', JSON.stringify(user));
      this.store.dispatch({type: AUTH_UPDATE, payload: {user: user}});
      if (callback) {
        console.log('active users', user, 'sync callback is', callback);
        callback(user);
      }
    } else {
      this.clear();
      if (callback) {
        callback(null);
      }
    }
  }

  login(credentials, errorCallback: (err: any) => void = null): void {
    /*
        let include = {
          relation: 'user',
          scope: {
            include: 'workspace'
          }
        };
    */
    let include = 'user';

    this.userApi.login(credentials, include)
    .pipe(
      // 간단한 로그 출력
      tap(authResult => console.log('after login', authResult)),
      // 로그인 한 결과물인 authResult에서 user를 꺼내서 다음으로 넘김
      map(authResult => authResult.user),
      // user를 넘긴 다음에 map을 변경 시킴..
      switchMap((user: any) => {
          console.log('in merge map', user);
          // this.checkLoginFailCount(user);
          // 아래의 결과물을 묶어서 user 대신에 새로운 map으로 반환함.
          // 아래의 3개의 Observer를 동시에 수행하고, 그 결과물을 합침.
          return forkJoin(
            this.userApi.getCurrentRoles(),
            // this.userApi.getWorkspace(this.userApi.getCurrentId(), {}),
            // this.userApi.getWorkspaces(this.userApi.getCurrentId()),
            this.groupsService.getWorkspaces(this.userApi.getCurrentId())
          ).pipe(
            // 모든 결과물들이 하나로 합쳐짐.
            // data[1]은 객체이고, data[2]는 배열로 리턴됨.
            // 위를 합쳐서 workspaces 로 합쳐서 처리함.
            // 따라서 user에 모든 것을 통함합.
            map(data => {
              user.roles = data[0];
              user.workspaces = data[1];
              localStorage.setItem('m2uWorkspace', data[1][0].path);
              localStorage.setItem('m2uWorkspaceId', data[1][0].id);
              // user.workspace = data[1];
              return user;
            })
          )
        }
      ),
      // 데이터 검증
      tap(data => console.log(data))
    )
    // 최종 데이터 처리
    .subscribe(
      user => {
        this.authApi.setUser(user);
        console.log('user final', user);
        console.log('before sync', this.authApi.getToken());
        this.sync(() => {
          console.log('before go home', this.authApi.getToken());
          this.goHome()
        })
      }, err => {
        if (errorCallback) {
          errorCallback(err);
        }
      }
    );
  }

  checkLoginFailCount(user): Promise<any> {
    return new Promise<any>(resolve => {
      if (user.activated === true || user.failedCount >= 5) {
        let err = new Error('account locked');
        err['code'] = 'ACCOUNT_LOCKED';
        throw err;
      }
      resolve(user)
    });
  }

  logout(): void {
    console.log('#@ logout');

    this.userApi.logout()
    .subscribe(() => {
      this.router.navigateByUrl('/auth/signin');
      // localStorage에서 users 정보를 삭제합니다
      localStorage.removeItem('user');
    }, err => {
      this.router.navigateByUrl('/auth/signin');
      // localStorage에서 users 정보를 삭제합니다
      localStorage.removeItem('user');
    });
  }
}
