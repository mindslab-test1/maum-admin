import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardM2uComponent} from './components/dashboard-m2u/dashboard-m2u.component';
import {DashboardMltComponent} from './components/dashboard-mlt/dashboard-mlt.component';

// dashboardRoute 변수 선언
const routes: Routes = [
  // {
  //   // sideNav를 선택하면 /dashboard 로 이동합니다
  //   path: '',
  //   component: DashboardM2uComponent,
  //   data: {hasNoSidebar: true}  // sidebar를 삭제 합니다
  // },
  // {
  //   path: 'dash-mlt',
  //   component: DashboardMltComponent,
  //   data: {hasNoSidebar: true}
  // }
  {
    path: '',
    children: [
      {
        path: '',
        component: DashboardM2uComponent,
      },
      {
        path: 'mlt',
        component: DashboardMltComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
