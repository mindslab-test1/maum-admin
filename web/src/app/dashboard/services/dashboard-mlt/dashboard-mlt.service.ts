import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from '../../../shared/storage/storage.browser';
import {environment} from '../../../../environments/environment';

@Injectable()
export class DashboardMltService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getSttFileGroups(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getSttFileGroups', param);
  }

  getSttFileCount(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getSttFileCount', param);
  }

  getTranscriptCompleteCount(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getTranscriptCompleteCount', param);
  }

  getQualityAssuranceCompleteCount(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getQualityAssuranceCompleteCount', param);
  }

  getDnnDics(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getDnnModels', param);
  }

  getHmdDics(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getHmdModels', param);
  }

  getDnnTrainings(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getDnnTrainings', param);
  }

  getHistories(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getHistories', param);
  }

  getSttTrainingModels(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/get-SttTrainingModels', param);
  }

  getSttTrainedModels(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/get-SttTrainedModels', param);
  }

  getSttFileList(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/get-SttFiles', param);
  }

  getBsqaSkills(): Observable<any> {
    return this.http.get(this.API_URL + '/dashboard2/getBsqaSkills');
  }

  getBsqaIndexingStatus(): Observable<any> {
    return this.http.get(this.API_URL + '/dashboard2/getBsqaIndexingStatus');
  }

  getMrcGroups(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getMrcGroups', param);
  }

  getMrcTrainings(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dashboard2/getMrcTrainings', param);
  }
}
