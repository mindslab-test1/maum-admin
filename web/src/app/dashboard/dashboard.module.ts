import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardM2uComponent} from './components/dashboard-m2u/dashboard-m2u.component';
import {SharedModule} from '../shared/shared.module';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {MatCardModule, MatIconModule, MatProgressBarModule} from '@angular/material';

import {DashboardMltService} from './services/dashboard-mlt/dashboard-mlt.service';
import {DashboardMltComponent} from './components/dashboard-mlt/dashboard-mlt.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    DashboardM2uComponent,
    DashboardMltComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    MatCardModule,
    MatIconModule,
    HttpClientModule,
    MatProgressBarModule
  ],
  exports: [
    DashboardM2uComponent,
    DashboardMltComponent
  ],
  providers: [
    DashboardMltService
  ],
  entryComponents: []
})
export class DashboardModule {
}
