import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TranscriptionEntity} from '../../entity/transcription/transcription.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {FileManagementEntity} from '../../entity/file-management/file-management.entity';
import {environment} from 'environments/environment';

@Injectable()
export class AmQualityAssuranceService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/getFileGroups', param);
  }

  getTranscriptionList(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/getTranscriptionList', param);
  }

  getTranscript(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/getTranscript', param);
  }

  commit(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/commit', param);
  }

  updateAlert(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/update-alert', param);
  }

  deleteAlert(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/am/quality-assurance/delete-alert', param);
  }
}
