import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SttEntity} from '../../entity/stt/stt.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class SttService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getSttList(): Observable<any> {
    return this.http.get(this.API_URL + '/stt/getSttList');
  }

  getStt(sttId: number): Observable<any> {
    return this.http.get(this.API_URL + '/stt/getStt/' + sttId);
  }

  insertStt(sttEntity: SttEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/insertStt', sttEntity);
  }

  updateStt(sttEntity: SttEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/updateStt', sttEntity);
  }

  deleteStt(sttId: number): Observable<any> {
    return this.http.get(this.API_URL + '/stt/deleteStt' + sttId);
  }

}
