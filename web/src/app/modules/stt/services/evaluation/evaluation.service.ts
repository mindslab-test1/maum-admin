import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EvaluationEntity} from '../../entity/evaluation/evaluation.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class EvaluationService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getFileGroups(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getFileGroups', param);
  }

  getLmModels(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getLmModels', param);
  }

  getAmModels(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getAmModels', param);
  }

  getAnalysisResultList(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/getAnalysisResultList', param);
  }

  getEvaluationResultList(param: EvaluationEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/get-evaluation-result', param);
  }

  deleteEvalResultList(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/delete-evaluation-result', param);
  }

  evaluate(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/stt/evaluation/evaluate', param);
  }
}
