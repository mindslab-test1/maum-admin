import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TranscriptionEntity} from '../../entity/transcription/transcription.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {FileManagementEntity} from '../../entity/file-management/file-management.entity';
import {environment} from 'environments/environment';

@Injectable()
export class LmTranscriptionService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/transcription/getFileGroups', param);
  }

  getTranscriptionList(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/transcription/getTranscriptionList', param);
  }

  getTranscript(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/transcription/getTranscript', param);
  }

  commit(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/transcription/commit', param);
  }

  updateAlert(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/transcription/update-alert', param);
  }

  deleteAlert(param: TranscriptionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/stt/lm/transcription/delete-alert', param);
  }
}
