import {NgModule} from '@angular/core';

import {SttService} from './services/stt/stt.service';
import {AnalysisService} from './services/analysis/analysis.service';
import {SttRouting} from './stt.routing';

import {AnalysisComponent} from './components/analysis/analysis.component';
import {EvaluationComponent} from './components/evaluation/evaluation.component';
import {AnalysisResultDialogComponent} from './components/analysis-result-dialog/analysis-result-dialog.component';
import {EvaluationService} from './services/evaluation/evaluation.service';
import {HttpClientModule} from '@angular/common/http';
import {LmFileManagementComponent} from './components/lm-file-management/lm-file-management.component';
import {LmFileManagementService} from './services/lm-file-management/lm-file-management.service';
import {LmTranscriptionComponent} from './components/lm-transcription/lm-transcription.component';
import {LmTrainingComponent} from './components/lm-training/lm-training.component';
import {LmTranscriptionService} from './services/lm-transcription/lm-transcription.service';
import {LmTranscriptionDetailComponent} from './components/lm-transcription-detail/lm-transcription-detail.component';
import {AmFileManagementComponent} from './components/am-file-management/am-file-management.component';
import {AmFileManagementService} from './services/am-file-management/am-file-management.service';
import {SttFileGroupDialogComponent} from './components/stt-file-group-dialog/stt-file-group-dialog.component';
import {LmTrainingService} from './services/lm-training/lm-training.service';
import {AmTranscriptionService} from './services/am-transcription/am-transcription.service';
import {AmTranscriptionComponent} from './components/am-transcription/am-transcription.component';
import {AmTranscriptionDetailComponent} from './components/am-transcription-detail/am-transcription-detail.component';
import {AmQualityAssuranceComponent} from './components/am-quality-assurance/am-quality-assurance.component';
import {AmQualityAssuranceDetailComponent} from './components/am-quality-assurance-detail/am-quality-assurance-detail.component';
import {AmQualityAssuranceService} from './services/am-quality-assurance/am-quality-assurance.service';
import {AmTrainingComponent} from './components/am-training/am-training.component';
import {AmTrainingService} from './services/am-training/am-training.service';
import {AnalysisResultComponent} from './components/analysis-result/analysis-result.component';
import {EvaluationResultComponent} from './components/evaluation-result/evaluation-result.component';
import {STTEvaluationDetailDialogComponent} from './components/evaluation-detail-dialog/evaluation-detail-dialog.component';
import {DiffMatchPatchModule} from 'ng-diff-match-patch';
import {SharedModule} from 'app/shared/shared.module';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatOptionModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSelectModule,
  MatToolbarModule,
  MatInputModule
} from '@angular/material';

@NgModule({
  imports: [
    SharedModule,
    SttRouting,
    HttpClientModule,
    DiffMatchPatchModule,
    MatOptionModule,
    MatIconModule,
    MatProgressBarModule,
    MatCardModule,
    MatSelectModule,
    MatToolbarModule,
    MatButtonModule,
    MatRadioModule,
    MatInputModule
  ],
  providers: [
    SttService,
    AnalysisService,
    EvaluationService,
    LmFileManagementService,
    LmTranscriptionService,
    AmFileManagementService,
    LmTrainingService,
    AmTranscriptionService,
    AmQualityAssuranceService,
    AmTrainingService],
  declarations: [
    AnalysisComponent,
    EvaluationComponent,
    AnalysisResultDialogComponent,
    LmFileManagementComponent,
    LmTranscriptionComponent,
    LmTrainingComponent,
    AnalysisResultComponent,
    LmTranscriptionDetailComponent,
    AmFileManagementComponent,
    SttFileGroupDialogComponent,
    AmTranscriptionComponent,
    AmTranscriptionDetailComponent,
    AmQualityAssuranceComponent,
    AmQualityAssuranceDetailComponent,
    AmTrainingComponent,
    EvaluationResultComponent,
    STTEvaluationDetailDialogComponent
  ],
  exports: [
    AnalysisComponent,
    EvaluationComponent,
    AnalysisResultDialogComponent,
    LmFileManagementComponent,
    LmTranscriptionComponent,
    LmTrainingComponent,
    LmTranscriptionDetailComponent,
    AmFileManagementComponent,
    SttFileGroupDialogComponent,
    AmTranscriptionComponent,
    AmTranscriptionDetailComponent,
    AmQualityAssuranceComponent,
    AmQualityAssuranceDetailComponent,
    AmTrainingComponent,
    AnalysisResultComponent,
    EvaluationResultComponent,
    STTEvaluationDetailDialogComponent
  ],
  entryComponents: [
    AnalysisResultDialogComponent,
    SttFileGroupDialogComponent,
    STTEvaluationDetailDialogComponent
  ]

})
export class SttModule {
}

