import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {AnalysisComponent} from './components/analysis/analysis.component';
import {EvaluationComponent} from './components/evaluation/evaluation.component';
import {LmFileManagementComponent} from './components/lm-file-management/lm-file-management.component';
import {LmTranscriptionComponent} from './components/lm-transcription/lm-transcription.component';
import {LmTrainingComponent} from './components/lm-training/lm-training.component';
import {LmTranscriptionDetailComponent} from './components/lm-transcription-detail/lm-transcription-detail.component';
import {AmFileManagementComponent} from './components/am-file-management/am-file-management.component';
import {AmTranscriptionComponent} from './components/am-transcription/am-transcription.component';
import {AmTranscriptionDetailComponent} from './components/am-transcription-detail/am-transcription-detail.component';
import {AmQualityAssuranceComponent} from './components/am-quality-assurance/am-quality-assurance.component';
import {AmQualityAssuranceDetailComponent} from './components/am-quality-assurance-detail/am-quality-assurance-detail.component';
import {AmTrainingComponent} from './components/am-training/am-training.component';
import {AnalysisResultComponent} from './components/analysis-result/analysis-result.component';
import {EvaluationResultComponent} from './components/evaluation-result/evaluation-result.component';

const sttRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'lm/file-management'
      },
      {
        path: 'lm',
        data: {
          nav: {
            name: 'LM',
            comment: 'Language Model',
          }
        },
        children: [
          {
            path: 'file-management',
            component: LmFileManagementComponent,
            data: {
              nav: {
                name: 'FileManagement',
                comment: '파일관리',
                icon: 'insert_drive_file',
              }
            }
          },
          {
            path: 'transcription',
            component: LmTranscriptionComponent,
            data: {
              nav: {
                name: 'Transcription',
                comment: '전사파일 관리',
                icon: 'create',
              }
            }
          },
          {
            path: 'transcription/:id',
            component: LmTranscriptionDetailComponent
          },
          {
            path: 'training',
            component: LmTrainingComponent,
            data: {
              nav: {
                name: 'Training',
                comment: '학습',
                icon: 'school',
              }
            }
          },
        ]
      },
      {
        path: 'am',
        data: {
          nav: {
            name: 'AM',
            comment: 'Acoustic Model',
          }
        },
        children: [
          {
            path: 'file-management',
            component: AmFileManagementComponent,
            data: {
              nav: {
                name: 'FileManagement',
                comment: '파일관리',
                icon: 'insert_drive_file',
              }
            }
          },
          {
            path: 'transcription',
            component: AmTranscriptionComponent,
            data: {
              nav: {
                name: 'Transcription',
                comment: '전사파일 관리',
                icon: 'create',
              }
            }
          },
          {
            path: 'transcription/:id',
            component: AmTranscriptionDetailComponent
          },
          {
            path: 'quality-assurance',
            component: AmQualityAssuranceComponent,
            data: {
              nav: {
                name: 'QualityAssurance',
                comment: '전사파일 관리',
                icon: 'create',
              }
            }
          },
          {
            path: 'quality-assurance/:id',
            component: AmQualityAssuranceDetailComponent
          },
          {
            path: 'training',
            component: AmTrainingComponent,
            data: {
              nav: {
                name: 'Training',
                comment: '학습',
                icon: 'school',
              }
            }
          },
        ]
      },
      {
        path: 'analysis',
        data: {
          nav: {
            name: 'Analysis',
            comment: 'Acoustic Model',
          }
        },
        children: [
          {
            path: 'analyze',
            component: AnalysisComponent,
            data: {
              nav: {
                name: 'Analyze',
                comment: '분석도구',
                icon: 'insert_chart',
              }
            }
          },
          {
            path: 'result',
            component: AnalysisResultComponent,
            data: {
              nav: {
                name: 'Result',
                comment: '분석도구',
                icon: 'insert_chart',
              }
            }
          }
        ]
      },
      {
        path: 'evaluation',
        data: {
          nav: {
            name: 'Evaluation',
            comment: '평가',
            icon: 'play_circle_outline',
          }
        },
        children: [
          {
            path: 'evaluate',
            component: EvaluationComponent,
            data: {
              nav: {
                name: 'Evaluation',
                comment: '평가',
                icon: 'play_circle_outline',
              }
            }
          },
          {
            path: 'result',
            component: EvaluationResultComponent,
            data: {
              nav: {
                name: 'Result',
                comment: '평가',
                icon: 'play_circle_outline',
              }
            }
          }
        ]
      },
    ]
  },
];

export const SttRouting: ModuleWithProviders = RouterModule.forChild(sttRoutes);
