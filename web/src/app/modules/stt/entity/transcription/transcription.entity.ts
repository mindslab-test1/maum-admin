import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class TranscriptionEntity extends PageParameters {
  id: string;
  ids: string[] = [];
  fileId: string;
  fileGroupId?: string;
  workspaceId: string;
  name: string;
  alert: string;
  version: string;
}
