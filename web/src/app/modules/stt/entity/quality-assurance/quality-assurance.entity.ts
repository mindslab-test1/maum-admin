import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class QualityAssuranceEntity extends PageParameters {
  id: string;
  ids: string[] = [];
  fileId: string;
  workspaceId: string;
  name: string;
  alert: string;
  version: string;
}
