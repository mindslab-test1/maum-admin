import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {EvaluationEntity} from '../../entity/evaluation/evaluation.entity';
import {EvaluationService} from '../../services/evaluation/evaluation.service';
import {AnalysisResultDialogComponent} from '../analysis-result-dialog/analysis-result-dialog.component';
import {AlertComponent, ConfirmComponent, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';


@Component({
  selector: 'app-stt-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EvaluationComponent implements OnInit, AfterViewInit {

  @ViewChild('fileTableComponent') fileTableComponent: TableComponent;
  @ViewChild('resultDataTableComponent') resultDataTableComponent: TableComponent;

  fileGroup = [];
  selectedGroup = new EvaluationEntity();

  amModels = [];
  lmModels = [];

  selectedAmModel = new EvaluationEntity();
  selectedLmModel = new EvaluationEntity();

  searchKeyword: string;

  files: any[] = [];
  resultData: any[] = [];

  actions: any;
  filePageTotalCount: number;

  resultDataSource: MatTableDataSource<any>;

  resultDataPage: MatPaginator;
  resultDataPageTotalCount: number;
  resultDataSort: MatSort;

  param = new EvaluationEntity();

  nameSearchKeyword: string;

  resultDataHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true, isSort: true},
    {attr: 'fileEntity.name', name: 'Name', isSort: true},
    {attr: 'fileGroupEntity.name', name: 'FileGroup', isSort: true},
    {attr: 'sttAmModelEntity.name', name: 'AM Model', isSort: true},
    {attr: 'sttLmModelEntity.name', name: 'LM Model', isSort: true},
    {attr: 'createdAt', name: 'Created at', format: 'date', isSort: true},
  ];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private evaluationService: EvaluationService,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.getFileGroups();
    this.getModels();
  }

  ngAfterViewInit() {
    this.getResultDataList();

    this.store.dispatch({type: ROUTER_LOADED});
  }

  getFileGroups = () => {
    this.param = new EvaluationEntity();
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');

    return new Promise(resolve => {
      this.evaluationService.getFileGroups(this.param).subscribe(res => {
          if (res && res.length > 0) {
            this.fileGroup = res;
            this.fileGroup.forEach(group => {
              group.rate = JSON.parse(group.meta).rate;
            })
          }
          resolve();
        },
        err => {
          this.openAlertDialog('Error', 'Failed Get File Groups', 'error');
        }
      );
    });
  };

  getModels = () => {
    this.param = new EvaluationEntity();
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');

    this.evaluationService.getLmModels(this.param).subscribe(res => {
        if (res) {
          this.lmModels = res;

        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get STT LmModels', 'error');
      }
    );

    this.evaluationService.getAmModels(this.param).subscribe(res => {
        if (res) {
          this.amModels = res;

        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get STT AmModels', 'error');
      }
    );
  };

  getResultDataList = (keyword?, pageIndex?) => {
    if (pageIndex !== undefined) {
      this.resultDataPage.pageIndex = pageIndex;
    }

    this.param = new EvaluationEntity();
    this.param.pageIndex = this.resultDataPage.pageIndex;
    this.param.pageSize = this.resultDataPage.pageSize;
    this.param.fileGroupId = this.selectedGroup.id;
    this.param.orderDirection = this.resultDataSort.direction === '' ||
    this.resultDataSort.direction === undefined ? 'desc' : this.resultDataSort.direction;
    this.param.orderProperty = this.resultDataSort.active === '' ||
    this.resultDataSort.active === undefined ? 'createdAt' : this.resultDataSort.active;
    this.param.amModelId = this.selectedAmModel.id;
    this.param.lmModelId = this.selectedLmModel.id;
    this.param.fileGroupId = this.selectedGroup.id;
    this.param.name = this.nameSearchKeyword;
    this.evaluationService.getAnalysisResultList(this.param).subscribe(res => {
        if (res) {
          this.resultData = res['content'];
          this.resultData.forEach(item => {
          });
          this.resultDataPageTotalCount = res['totalElements'];
          this.resultDataTableComponent.isCheckedAll = false;
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get Result DataList', 'error');
      }
    );
  };

  evaluate = () => {
    const paramArr = [];

    const resultArr = this.getCheckedFile(this.resultDataTableComponent);
    if (resultArr.length !== 0) {
      this.dialog.open(ConfirmComponent)
      .afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return false;
        }
        this.evaluationService.evaluate(resultArr).subscribe(res => {
          this.openAlertDialog('success', 'Success Evaluate Selected Files', 'success');
        })
      });
    } else {
      this.openAlertDialog('Notice', 'Result has to be selected', 'notice');
      return null;
    }
  };

  getCheckedFile = (tableComponent: TableComponent) => {
    return tableComponent.rows.filter(item => item.isChecked);
  };
  setResultDataPage = (page: MatPaginator) => {
    this.resultDataPage = page;
    if (this.resultData.length > 0) {
      this.getResultDataList(null);
    }
  };

  setSortResultData(matSort?: MatSort) {
    this.resultDataSort = matSort;
    if (this.resultData.length > 0) {
      this.getResultDataList(null);
    }
  }

  openModal(row: any) {
    let ref: any = this.dialog.open(AnalysisResultDialogComponent);
    ref.componentInstance.data = row;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
