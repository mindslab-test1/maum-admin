import {Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AnalysisEntity} from '../../entity/analysis/analysis.entity';
import {MatDialog, MatDialogRef} from '@angular/material';
import {EvaluationService} from '../../services/evaluation/evaluation.service';
import {AlertComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {TranscriptTextComponent} from 'app/shared/components/transcript-text/transcript.text.component';

@Component({
  selector: 'app-stt-evaluation-detail-dialog',
  templateUrl: 'evaluation-detail-dialog.component.html',
})
export class STTEvaluationDetailDialogComponent implements OnInit {

  @ViewChild('transcriptTextComponent') transcriptTextComponent: TranscriptTextComponent;
  @ViewChild('diffContainer') diffContainer: ElementRef;

  title: String;
  audioURL: string;
  langCode = 'kor';

  fileId: String;

  param: AnalysisEntity;
  data: any;

  transcriptResult;
  engineResult;
  row;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              public dialogRef: MatDialogRef<any>,
              private evaluationService: EvaluationService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    // apply data when update
    let data = this.data = this.dialogRef.componentInstance.data;
    const audioFile = '/stt/am/file-manage/download-file/';
    let base = this.storage.get('mltApiUrl');
    let ws = this.storage.get('m2uWorkspaceId');
    if (data) {
      this.row = data;
      this.audioURL = `${base}${audioFile}${ws}/${this.row.sttAnalysisResultEntity.fileId}`;
      this.transcriptResult = this.row.sttAnalysisResultEntity.sttTranscriptEntity.version;
      this.engineResult = this.row.sttAnalysisResultEntity.text;
    }
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
