import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {FileManagementEntity} from '../../entity/file-management/file-management.entity';
import {MatDialog} from '@angular/material';
import {SttTrainingEntity} from '../../entity/stt-training/stt-training.entity';
import {LmTrainingService} from '../../services/lm-training/lm-training.service';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AlertComponent, ConfirmComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-stt-lm-training',
  templateUrl: './lm-training.component.html',
  styleUrls: ['./lm-training.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LmTrainingComponent implements OnInit, OnDestroy, AfterViewInit {


  trainGroup: any;
  selectedFile: any;
  actions: any;
  workspaceId: string;
  progress: any;
  intervalHandler: any;
  interTime: any;
  models: SttTrainingEntity[] = [];
  trainings: SttTrainingEntity[] = [];
  param: FileManagementEntity = new FileManagementEntity();

  trainType: string;

  selectSearchFormControl: FormControl = new FormControl();

  isTraining = false;
  private _onDestroy = new Subject<void>();
  public filteredFileGroupCodes: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private lmTrainingService: LmTrainingService,
              private dialog: MatDialog,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.interTime = 2000;
    this.getFileGroups();
    this.initGetAllProgress();

    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterFileGroup();
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngAfterViewInit() {
  }

  private filterFileGroup() {
    if (!this.trainGroup) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredFileGroupCodes.next(this.trainGroup.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredFileGroupCodes.next(
      this.trainGroup.filter(fileGroup => fileGroup.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.syncProgress();
    }, this.interTime);
  }

  getFileGroups() {
    this.param = new FileManagementEntity();
    this.param.workspaceId = this.workspaceId;

    this.lmTrainingService.getFileGroups(this.param).subscribe(res => {
        if (res && res.length > 0) {
          this.trainGroup = res;
          this.trainGroup.forEach(group => {
            group.rate = JSON.parse(group.meta).rate;
          })
        }
        this.filteredFileGroupCodes.next(this.trainGroup.slice());

      },
      err => {
        this.openAlertDialog('Error', 'Failed Get FileGroups', 'error');
      }
    );
  }

  trainGroupFiles = () => {
    if (this.selectedFile === undefined) {
      this.openAlertDialog('Notice', 'File Group Has To Be Selected', 'notice');
      return false;
    }
    if (this.trainType === undefined) {
      this.openAlertDialog('Notice', 'Train Type Has To Be Selected', 'notice');
      return false;
    }
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      let openParam = {
        'fileGroupId': this.selectedFile.id,
        'trainType': this.trainType,
        'workspaceId': this.storage.get('m2uWorkspaceId')
      };
      this.lmTrainingService.trainGroupFiles(openParam).subscribe(res => {
          if (res) {
            this.openAlertDialog('Success', 'Success Train GroupFiles', 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', 'STT Connection Exception', 'error');
        }
      );
    });
  };

  stopTraining(key) {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      this.lmTrainingService.stop(key).subscribe(result => {
          this.openAlertDialog('Success', 'Success Stop Training', 'success');
        },
        err => {
          if (err.status === 502) {
            clearInterval(this.intervalHandler);
          }
          this.openAlertDialog('Error', 'STT Connection Exception', 'error');
        }
      );
    });
  }

  removeTraining(key) {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      this.lmTrainingService.close(key).subscribe(result => {
          this.openAlertDialog('Success', 'Success Delete Training', 'success');
          this.getTrainedSttModels();
        },
        err => {
          if (err.status === 502) {
            clearInterval(this.intervalHandler);
          }
          this.openAlertDialog('Error', 'STT Connection Exception', 'error');
        }
      );
    });
  }

  removeModel(id) {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Model? All related data will be deleted';
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      this.lmTrainingService.delete(id).subscribe(result => {
          this.openAlertDialog('Success', 'Success Delete Model', 'success');
          this.getTrainedSttModels();
        },
        err => {
          this.openAlertDialog('Error', 'Failed Delete Model', 'error');
        }
      );
    });
  }

  getTrainedSttModels = () => {
    this.lmTrainingService.getTrainedSttModels(this.workspaceId).subscribe(result => {
        if (!(this.models.length === result['tmList'].length)) {
          this.models.length = 0;
          if (result['tmList']) {
            this.models = result['tmList'];
          }
        } else if (this.models.length === result['tmList'].length) {
        }
      },
      err => {
        if (err.status === 502) {
          clearInterval(this.intervalHandler);
        }
        this.openAlertDialog('Error', 'STT Connection Exception', 'error');
      }
    );
  };

  initGetAllProgress() {
    this.lmTrainingService.getAllProgress(this.workspaceId).subscribe(
      res => {
        if (res) {
          this.trainings = res['tmList'];
          if (this.trainings.length !== 0) {
            this.isTraining = true;
          }
        }
        this.getTrainedSttModels();
        this.triggerInterval();
      },
      err => {
        this.openAlertDialog('Error', 'STT Connection Exception', 'error');
      });
  }

  syncProgress = () => {
    this.lmTrainingService.getAllProgress(this.workspaceId).subscribe(result => {
        if (result) {
          if (this.trainings.length === 0) {
            this.isTraining = false;
            clearInterval(this.intervalHandler);
            this.interTime = 5000;
            this.triggerInterval();
            this.trainings = result['tmList'];
          } else {
            clearInterval(this.intervalHandler);
            this.interTime = 450;
            this.isTraining = true;
            this.triggerInterval();
            if (this.trainings.length <= result['tmList'].length) {
              result['tmList'].forEach(sttModel => {
                let training: SttTrainingEntity = this.trainings.find(item => item.sttKey === sttModel.sttKey);
                if (training) {
                  training['progress'] = sttModel.progress;
                } else {
                  this.trainings.push(sttModel);
                }
              });
            } else if (this.trainings.length > result['tmList'].length) {
              this.trainings.forEach((sttModel, index) => {
                let training: SttTrainingEntity = result['tmList'].find(item => item.sttKey === sttModel.sttKey);
                if (training) {
                  training['progress'] = sttModel['progress'];
                } else {
                  this.trainings.splice(index, 1);
                }
              });
            }
          }
        }
        this.getTrainedSttModels();
      },
      err => {
        this.isTraining = false;
        if (err.status === 502) {
          clearInterval(this.intervalHandler);
        }
        this.openAlertDialog('Error', 'STT Connection Exception', 'error');
      });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
