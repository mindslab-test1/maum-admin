import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TranscriptionEntity} from '../../entity/transcription/transcription.entity';
import {AmTranscriptionService} from '../../services/am-transcription/am-transcription.service';
import {LmTranscriptionService} from '../../services/lm-transcription/lm-transcription.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranscriptAlertDialogComponent} from 'app/shared/components/dialog/transcript-alert-dialog.component';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {GitComponent} from 'app/shared/components/git/git.component';
import {TranscriptTextComponent} from 'app/shared/components/transcript-text/transcript.text.component';
import {AlertComponent} from 'app/shared';
import {environment} from '../../../../../environments/environment';


@Component({
  selector: 'app-stt-am-transcription-detail',
  templateUrl: './am-transcription-detail.component.html',
  styleUrls: ['./am-transcription-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AmTranscriptionDetailComponent implements OnInit, AfterViewInit {

  // main values

  @ViewChild('transcriptTextComponent') transcriptTextComponent: TranscriptTextComponent;
  @ViewChild('gitComponent') gitComponent: GitComponent;
  actions: any[] = [];

  transcript: any = null;

  langCode = 'kor';
  param: TranscriptionEntity = new TranscriptionEntity();

  selectedCode: string;
  audioURL: string;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private amTranscriptionService: AmTranscriptionService,
              private router: Router,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.getTranscript(params.get('id'));
    });

    this.actions = [
      {
        text: 'Bigger Text',
        icon: 'zoom_in',
        callback: this.transcriptTextComponent.zoomIn,
      },
      {
        text: 'Smaller Text',
        icon: 'zoom_out',
        callback: this.transcriptTextComponent.zoomOut,
      },
      {
        text: 'Clear Errors',
        icon: 'format_clear',
        callback: this.transcriptTextComponent.clearErrors,
      },
      {
        text: 'Save',
        icon: 'save',
        callback: this.commit,
      },
      {
        text: 'Save with Alert',
        icon: 'error_outline',
        callback: this.updateAlert,
      },
      {
        text: 'Back to List',
        icon: 'keyboard_backspace',
        callback: this.backToList,
      },
    ]
  }

  ngAfterViewInit() {
  }

  doAction = (action) => {
    if (action) {
      action.callback();
    }
  };

  getTranscript = (id) => {
    if (!id || typeof (id) === 'undefined') {
      return false;
    }
    this.param = new TranscriptionEntity();
    this.param.id = id;

    this.amTranscriptionService.getTranscript(this.param).subscribe(res => {
        if (res) {
          const audioFile = '/stt/am/file-manage/download-file/';
          let base = environment.maumAiApiUrl;
          let ws = this.storage.get('m2uWorkspaceId');

          this.transcript = res;
          this.audioURL = `${base}${audioFile}${ws}/${res.fileId}`;
          if (this.transcript.version) {
            this.transcriptTextComponent.setContent(
              this.transcript.version.toString().replace(/\n/g, '<br>'));
          }
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get Transcript', 'error');
        console.log('error', err);
      }
    );
  };

  commit = () => {
    this.param = new TranscriptionEntity();
    this.param.id = this.transcript.id;
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');
    this.param.version = this.transcriptTextComponent.text;

    this.amTranscriptionService.commit(this.param).subscribe(res => {
        if (res) {
          this.transcript = res;
          this.openAlertDialog('Success', 'Success Transcript Commit', 'success');
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Version commit', 'error');
        console.log('error', err);
      }
    );
  };

  updateAlert = () => {
    let data = {};
    data['row'] = this.transcript;
    data['service'] = this.amTranscriptionService;
    data['entity'] = new TranscriptionEntity();

    let dialogRef = this.dialog.open(TranscriptAlertDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.transcript = result;
      }
    });
  };

  backToList = () => {
    let param = `fileGroupId=${encodeURI(this.transcript.fileGroupId.toString())}`;
    this.router.navigate(['m2u-builder', 'stt', 'am', 'transcription', param]);
  };


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
