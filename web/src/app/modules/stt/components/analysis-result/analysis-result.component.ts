import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AnalysisEntity} from '../../entity/analysis/analysis.entity';
import {AnalysisService} from '../../services/analysis/analysis.service';
import {AnalysisResultDialogComponent} from '../analysis-result-dialog/analysis-result-dialog.component';
import {AlertComponent, ConfirmComponent, DownloadService, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-stt-analysis-result',
  templateUrl: './analysis-result.component.html',
  styleUrls: ['./analysis-result.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AnalysisResultComponent implements OnInit, AfterViewInit {

  @ViewChild('fileTableComponent') fileTableComponent: TableComponent;
  @ViewChild('resultDataTableComponent') resultDataTableComponent: TableComponent;

  fileGroup = [];
  selectedGroup = new AnalysisEntity();

  amModels = [];
  lmModels = [];

  selectedAmModel = new AnalysisEntity();
  selectedLmModel = new AnalysisEntity();

  searchKeyword: string;

  files: any[] = [];
  resultData: any[] = [];

  actions: any;

  fileName: string;
  resultModel: string;

  fileSource: MatTableDataSource<any>;
  filePage: MatPaginator;
  filePageTotalCount: number;

  resultDataSource: MatTableDataSource<any>;

  resultDataPage: MatPaginator;
  resultDataPageTotalCount: number;

  filesSort: MatSort;
  resultDataSort: MatSort;

  param = new AnalysisEntity();
  fileGroupSearchKeyword: string;
  lmModelSearchKeyword: string;
  amModelSearchKeyword: string;
  nameSearchKeyword: string;

  resultDataHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true, isSort: true},
    {attr: 'fileEntity.name', name: 'Name', isSort: true},
    {attr: 'fileGroupEntity.name', name: 'FileGroup', isSort: true},
    {attr: 'sttAmModelEntity.name', name: 'AM Model', isSort: true},
    {attr: 'sttLmModelEntity.name', name: 'LM Model', isSort: true},
    {attr: 'createdAt', name: 'Created at', format: 'date', isSort: true},
    {attr: 'result', name: 'Result', isButton: true},
  ];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private analysisService: AnalysisService,
              private downloadService: DownloadService,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actions = [
      {type: 'download', text: 'Download Results', icon: 'file_download', callback: this.download},
      {type: 'clear', text: 'Clear Results', icon: 'delete_sweep', callback: this.deleteResult}
    ];
  }

  ngAfterViewInit() {
    this.getResultDataList();

    this.store.dispatch({type: ROUTER_LOADED});
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  getResultDataList = (keyword?, pageIndex?) => {
    if (pageIndex !== undefined) {
      this.resultDataPage.pageIndex = pageIndex;
    }

    this.param = new AnalysisEntity();
    this.param.pageIndex = this.resultDataPage.pageIndex;
    this.param.pageSize = this.resultDataPage.pageSize;
    this.param.fileGroupId = this.selectedGroup.id;
    this.param.orderDirection = this.resultDataSort.direction === '' ||
    this.resultDataSort.direction === undefined ? 'desc' : this.resultDataSort.direction;
    this.param.orderProperty = this.resultDataSort.active === '' ||
    this.resultDataSort.active === undefined ? 'createdAt' : this.resultDataSort.active;
    this.param.amModelName = this.amModelSearchKeyword;
    this.param.lmModelName = this.lmModelSearchKeyword;
    this.param.fileGroupName = this.fileGroupSearchKeyword;
    this.param.name = this.nameSearchKeyword;

    this.analysisService.getAnalysisResultList(this.param).subscribe(res => {
        if (res) {
          this.resultData = res['content'];
          this.resultData.forEach(item => {
            if (item['text'] !== 'undefined') {
              item['buttonName'] = 'view';
            }
          });
          this.resultDataPageTotalCount = res['totalElements'];
          this.resultDataTableComponent.isCheckedAll = false;
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Get Result DataList', 'error');
      }
    );
  };

  download = () => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      this.downloadService.downloadCsvFile(this.resultDataTableComponent.rows, this.resultDataHeader, 'result');
    });
  };

  deleteResult = () => {
    const paramArr = [];
    const resultArr = this.getCheckedFile(this.resultDataTableComponent);
    if (this.resultData.length === 0) {
      this.resultDataTableComponent.isCheckedAll = false;
      this.openAlertDialog('Notice', 'Result has to have data', 'notice');
      return null;
    }
    if (resultArr.length !== 0) {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want to remove analyze result data? All related data will be deleted';
      ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return false;
        }

        resultArr.forEach(item => {
          this.param = new AnalysisEntity();
          this.param.id = item.id;
          paramArr.push(this.param);
        });

        let originalPageIndex: number = this.resultDataPage.pageIndex;
        if (paramArr.length === this.resultDataTableComponent.rows.length) {
          if (this.resultDataPage.pageIndex === 0) {
            this.resultDataPage.pageIndex = 0;
          } else {
            this.resultDataPage.pageIndex -= 1;
          }
        }

        this.analysisService.deleteResult(paramArr).subscribe(res => {
          this.getResultDataList();
          this.openAlertDialog('Success', 'Success delete result file', 'success');
        }, err => {
          this.resultDataPage.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Failed delete result file', 'error');
        });
      });
    }
  };

  getCheckedFile = (tableComponent: TableComponent) => {
    return tableComponent.rows.filter(item => item.isChecked);
  };

  setResultDataPage = (page: MatPaginator) => {
    this.resultDataPage = page;
    if (this.resultData.length > 0) {
      this.getResultDataList(null);
    }
  };

  setSortResultData(matSort?: MatSort) {
    this.resultDataSort = matSort;
    if (this.resultData.length > 0) {
      this.getResultDataList(null);
    }
  }

  openModal(row: any) {
    let ref: any = this.dialog.open(AnalysisResultDialogComponent);
    ref.componentInstance.data = row;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
