import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject, OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TranscriptionEntity} from '../../entity/transcription/transcription.entity';
import {LmTranscriptionService} from '../../services/lm-transcription/lm-transcription.service';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {FileManagementEntity} from '../../entity/file-management/file-management.entity';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {AmTranscriptionService} from '../../services/am-transcription/am-transcription.service';
import {AlertComponent, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {GitComponent} from 'app/shared/components/git/git.component';
import {TranscriptTextComponent} from 'app/shared/components/transcript-text/transcript.text.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';


@Component({
  selector: 'app-stt-am-transcription',
  templateUrl: './am-transcription.component.html',
  styleUrls: ['./am-transcription.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AmTranscriptionComponent implements OnInit, AfterViewInit, OnDestroy {

  // main values
  @ViewChild('tableComponent') tableComponent: TableComponent;

  tableHeader: any[] = [];
  tableData: any[] = [];
  tableDataSource: MatTableDataSource<any>;
  tablePage: MatPaginator;
  tableSort: MatSort;
  totalCount: number;


  @ViewChild('transcriptTextComponent') transcriptTextComponent: TranscriptTextComponent;
  @ViewChild('gitComponent') gitComponent: GitComponent;
  actions: any[] = [];

  langCode = 'kor';
  param: TranscriptionEntity = new TranscriptionEntity();

  fileGroupCodes = [];
  selectedCode: string;
  searchKeyword = '';

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredFileGroupCodes: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private amTranscriptionService: AmTranscriptionService,
              private cdr: ChangeDetectorRef,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.tableHeader = [
      {attr: 'checkbox', name: 'Checkbox', checkbox: true},
      {attr: 'no', name: 'No.', no: true},
      {attr: 'name', name: 'Name', isSort: true},
      {attr: 'alert', name: 'Alert', width: '20%', isSort: true},
      {attr: 'fileEntity.creatorId', name: 'Uploader', width: '10%'},
      {attr: 'updaterId', name: 'UpdaterId', width: '10%', isSort: true},
      {attr: 'action', name: 'Action', width: '7%', isButton: true},
      {attr: 'updatedAt', name: 'Updated At', width: '10%', format: 'date', isSort: true}
    ];

    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterFileGroup();
    });

    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.getFileGroups().then(() => {
      this.route.queryParams.subscribe(
        query => {
          if (query['fileGroupId'] !== undefined && query['fileGroupId'] !== null) {
            this.selectedCode = query['fileGroupId'];
            this.cdr.detectChanges();
          } else {
            if (this.fileGroupCodes.length !== 0) {
              this.selectedCode = this.fileGroupCodes[0].id;
            }
          }
          this.getList();
        });
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterFileGroup() {
    if (!this.fileGroupCodes) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredFileGroupCodes.next(this.fileGroupCodes.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredFileGroupCodes.next(
      this.fileGroupCodes.filter(fileGroup => fileGroup.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }


  doAction = (action) => {
    if (action) {
      action.callback();
    }
  };

  getList = (keyword?: string, pageIndex?) => {
    if (pageIndex !== undefined) {
      this.tablePage.pageIndex = pageIndex;
    }
    this.param = new TranscriptionEntity();
    this.param.fileGroupId = this.selectedCode;
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');
    this.param.pageIndex = this.tablePage.pageIndex;
    this.param.pageSize = this.tablePage.pageSize;
    this.param.orderDirection = this.tableSort.direction === '' ||
    this.tableSort.direction === undefined ? 'desc' : this.tableSort.direction;
    this.param.orderProperty = this.tableSort.active === '' ||
    this.tableSort.active === undefined ? 'createdAt' : this.tableSort.active;
    this.param.name = keyword === null || typeof (keyword) === 'undefined' ? '' : keyword;

    this.amTranscriptionService.getTranscriptionList(this.param).subscribe(res => {
        if (res) {
          this.tableData = res['content'];
          this.tableData.forEach((item) => {
            if (item.transcriberId === null) {
              item['buttonName'] = 'Edit';
            } else {
              item['buttonName'] = 'Complete';
              item['buttonColor'] = 'accent';
            }
          });
          this.totalCount = res['totalElements'];
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get Transcriptions FileList', 'error');
        console.log('error', err);
      }
    );
  };

  getFileGroups = () => {
    let param: FileManagementEntity = new FileManagementEntity();
    param = new FileManagementEntity();
    param.workspaceId = this.storage.get('m2uWorkspaceId');
    this.cdr.detectChanges();
    return new Promise(resolve => {
      this.amTranscriptionService.getFileGroups(param).subscribe(res => {
          if (res && res.length > 0) {
            this.fileGroupCodes = res;
            this.fileGroupCodes.forEach(group => {
              group.rate = JSON.parse(group.meta).rate;
            });
            if (!this.selectedCode) {
              this.selectedCode = '';
            }
          }
          this.filteredFileGroupCodes.next(this.fileGroupCodes.slice());

          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Failed Get File Groups', 'error');
          console.log('error', err);
        }
      );
    });
  };

  goDetail(row) {
    this.router.navigate(['m2u-builder', 'stt', 'am', 'transcription', row.id]);
  }

  setTablePage = (page: MatPaginator) => {
    this.tablePage = page;

    if (this.tableData.length > 0) {
      this.getList();
    }
  };

  setSort = (sort: MatSort) => {
    this.tableSort = sort;

    if (this.tableData.length > 0) {
      this.getList();
    }
  };

  getCheckedFile = (tableComponent: TableComponent) => {
    return tableComponent.rows.filter(item => (item.isChecked && item.sttTranscriptEntities.length > 0));
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
