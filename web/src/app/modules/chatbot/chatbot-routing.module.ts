import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ChatbotsComponent} from './components/chatbots/chatbots.component';
import {ChatbotUpsertComponent} from './components/chatbot-upsert/chatbot-upsert.component';
import {ChatbotDetailComponent} from './components/chatbot-detail/chatbot-detail.component';
import {ChatbotTestComponent} from './components/chatbot-test/chatbot-test.component';
import {DaInstancesComponent} from './components/chatbot-dai/da-instances/da-instances.component';
import {DaInstanceDetailComponent} from './components/chatbot-dai/da-instance-detail/da-instance-detail.component';
import {DaInstanceUpsertComponent} from './components/chatbot-dai/da-instance-upsert/da-instance-upsert.component';

const routes: Routes = [
  {
    path: 'test',
    component: ChatbotTestComponent,
  },
  {
    path: '',
    children: [
      {
        path: '',
        component: ChatbotsComponent,
        data: {
          blahBlah: 'TEST DATA'
        }
      },
      {
        path: 'new',
        component: ChatbotUpsertComponent,
      },
      {
        path: ':id/edit',
        component: ChatbotUpsertComponent,
      },
      {
        path: ':id/test',
        component: ChatbotTestComponent,
      },
      {
        path: ':id/dais',
        component: DaInstancesComponent,
      },
      {
        path: ':id/dais/new',
        component: DaInstanceUpsertComponent
      },
      {
        path: ':id/dais/edit',
        component: DaInstanceUpsertComponent
      },
      {
        path: ':id/dais/:daid',
        component: DaInstanceDetailComponent,
      },
      {
        path: ':id',
        component: ChatbotDetailComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatbotRoutingModule {
}
