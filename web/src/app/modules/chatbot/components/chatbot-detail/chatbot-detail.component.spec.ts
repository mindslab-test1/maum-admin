import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ChatbotDetailComponent} from './chatbot-detail.component';

describe('ChatbotsDetailComponent', () => {
  let component: ChatbotDetailComponent;
  let fixture: ComponentFixture<ChatbotDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChatbotDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
