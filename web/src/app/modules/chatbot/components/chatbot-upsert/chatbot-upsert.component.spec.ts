import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatbotUpsertComponent} from './chatbots-upsert.component';

describe('ChatbotsUpsertComponent', () => {
  let component: ChatbotUpsertComponent;
  let fixture: ComponentFixture<ChatbotUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChatbotUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
