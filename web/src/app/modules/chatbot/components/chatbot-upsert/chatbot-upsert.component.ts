import {
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatListOption,
  MatSelectionList,
  MatSelectionListChange,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {
  AppEnumsComponent,
  AppObjectManagerService,
  FormErrorStateMatcher
} from '../../../../shared';
import {GrpcApi} from '../../../../core/sdk'
import {ROUTER_LOADED} from '../../../../core/actions';
import {getErrorString} from '../../../../shared/values/error-string';
import {
  POSITIVE_INTEGER_REGEX,
  VARIABLE_REGEX
} from '../../../../shared/values/values';
import {ConfirmComponent} from 'app/shared';


@Component({
  selector: 'app-chatbot-upsert',
  templateUrl: './chatbot-upsert.component.html',
  styleUrls: ['./chatbot-upsert.component.scss'],
  providers: [{
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }]
})

/**
 * Chatbot 등록 수정 Component
 */
export class ChatbotUpsertComponent implements OnInit {
  // FormGroup 선언
  informationGroup: FormGroup;
  itfGroup: FormGroup;
  sttModelGroup: FormGroup;
  title: string;
  role: string;

  chatbot: any;
  chatbotName: string;
  chatbotTitle: string;
  sessionTimeoutStr: string;

  itfp = null;
  itfiList = [
    {
      name: 'none',
      ip: '',
      port: '',
      description: '',
      policy_name: ''
    }
  ];
  chatbotNameList = [];
  sttModelList = [];
  doneFlag = false;
  originalChatbot: any;

  itfpList = [];

  @ViewChild(MatSelectionList) sttModels: MatSelectionList;
  @ViewChildren('options') options: QueryList<MatListOption>;

  constructor(private store: Store<any>,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              public appEnum: AppEnumsComponent,
              private _formBuilder: FormBuilder,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi) {
    this.sessionTimeoutStr = '600';
    // [STEP] informationGroup 선언
    this.informationGroup = this._formBuilder.group({
      name: [undefined,
        [
          Validators.required,
          () => !new RegExp(VARIABLE_REGEX).test(this.chatbotName) ? {'variable': true} : null,
          () => this.getDuplicatedName() ? {'duplicated': true} : null
        ]
      ],
      title: [undefined,
        [
          Validators.required,
          () => !new RegExp(VARIABLE_REGEX).test(this.chatbotName) ? {'variable': true} : null,
        ]
      ],
      description: '',
      session_timeout: ['600',
        [
          Validators.required,
          () => !new RegExp(POSITIVE_INTEGER_REGEX).test(this.sessionTimeoutStr) ?
            {'isPositiveInteger': true} : null
        ]
      ],
      external_system_info: '',
      active: true,
    });

    // [STEP] itfGroup 선언
    this.itfGroup = this._formBuilder.group({
      intent_finder_policy: ['none', Validators.required],
      default_skill: [undefined, Validators.required],
    });

    this.sttModelGroup = this._formBuilder.group({
      stt_models: [
        []
      ]
    });
  }

  getDuplicatedName() {
    if (this.role === 'edit' && this.originalChatbot) {
      this.chatbotNameList.splice(this.chatbotNameList.indexOf(this.originalChatbot.name));
    }
    return this.chatbotNameList.find(chatbotName => chatbotName === this.chatbotName);
  }

  ngOnInit(): void {
    this.checkDuplicateChatbotName();

    // 1.objectManager에서 cb정보를 조회합니다
    this.chatbot = this.objectManager.get('cb');

    // /m2u/dialog-service/chatbots/default-chatbot/edit 로 접근할수 있는 방법은 두가지 입니다
    // 1. chatbots view에서 -> chatbots edit로 버튼을 클릭해서 이동한 경우
    // 2. 사용자가 직접 url에 /m2u/dialog-service/chatbots/default-chatbot/edit를 입력하여 들어온 경우
    // 2 번의 경우 objectManager에 cb가 없습니다
    // 이 경우 cb을 획득하기 위한 예외처리는 아래와 같습니다
    let promise = new Promise((resolve, reject) => {
      this.route.params.subscribe(par => {
        let tempRoute = this.route.toString();
        if (tempRoute.indexOf('new') > 0) {
          // console.log('#@ chatbots/new :', tempRoute);
          this.role = 'add';
        } else if (tempRoute.indexOf('edit') > 0) {
          // console.log('#@ /edit :' + tempRoute);
          this.role = 'edit';
          this.chatbotName = par['id']; // url에 명시된 chatbotName을 획득합니다
        } else {
          console.log('#@ There is no chatbot url');
          let message = 'Cannot find this chatbot url.';
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.backPage();
          reject();
        }
      });
      resolve();
    });
    promise.catch(() => {
      // 정상적으로 데이터를 갖고 오지 못한 경우
      let message = `Cannot find [${this.chatbotName}] this chatbot`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.backPage();
      throw new Error();
    });
    promise.then(() => {
      switch (this.role) {
        case 'add':
          this.title = 'Add Chatbot';
          break;
        case 'edit':
          this.getChatbotInfo().then(() => {
            delete this.chatbot.auth_provider;
            delete this.chatbot.detail;
            delete this.chatbot.input;
            delete this.chatbot.output;
            delete this.chatbot.use_invocation_trigger;
            delete this.chatbot.invocation_keywords;
            delete this.chatbot.launch_skill;
            delete this.chatbot.open_skills;

            this.originalChatbot = JSON.parse(JSON.stringify(this.chatbot));

            this.informationGroup.patchValue({
              name: this.chatbot.name,
              title: this.chatbot.title,
              description: this.chatbot.description,
              session_timeout: this.chatbot.session_timeout,
              external_system_info: this.chatbot.external_system_info,
              active: this.chatbot.active,
            });

            this.informationGroup.get('name').disable();

            this.itfGroup.patchValue({
              intent_finder_policy: this.chatbot.intent_finder_policy,
              default_skill: this.chatbot.default_skill,
            });

            this.getIntentFinderInstanceList().then((itfiList: any[]) => {
              let itfpSet = new Set();
              itfiList.map(itfi => {
                itfpSet.add(itfi.policy_name);
              });

              this.itfpList = ['none'];
              this.itfpList = this.itfpList.concat(Array.from(itfpSet));
              this.getIntentFinderPolicyInfo(this.chatbot.intent_finder_policy);
            });

                this.options.changes.subscribe((r) => {
                  this.options.toArray().forEach(option => {
                    this.sttModelGroup.get('stt_models').value.some(sttModel => {
                      if (option.value.model === sttModel.model && option.value.lang === sttModel.lang
                        && option.value.sample_rate === sttModel.sample_rate) {
                        option.selected = true;
                        this.cdr.detectChanges();
                        return true;
                      }
                    });
                  });
                });
                this.isDone();
              },
              err => {
                let msg = `Something went wrong. [${getErrorString(err)}]`;
                this.snackBar.open(msg, 'Confirm', {duration: 10000});
                this.store.dispatch({type: ROUTER_LOADED});
              }
            );
            this.title = 'Edit Chatbot';
            break;
          default:
            this.backPage();
            let message = 'Role is not properly assigned.';
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            return;
        }
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
    // this.sttModels.
    this.sttModels.selectionChange.subscribe((s: MatSelectionListChange) => {
      let sttModelList: any[] = this.sttModelGroup.get('stt_models').value;

      if (s.option.selected) {
        sttModelList.push(s.option.value);
      } else {
        sttModelList.splice(sttModelList.indexOf(s.option.value), 1);
      }
      this.cdr.detectChanges();
    });

  }

  /**
   * MatStepper의 단계가 변화될 때 이벤트를 감지한다.
   * 각각의 step에 맞게 grpc를 통하여 데이터를 불러온다.
   * 1 : IntentFinderInstance
   * 2 : SttModels
   * @param event
   */
  changeStep(event: any) {
    if (event.selectedIndex === 1) {
      // [STEP] Intent Finder Policy MatSteper
      this.getIntentFinderInstanceList().then((itfiList: any[]) => {
        let itfpSet = new Set();
        itfiList.map(itfi => {
          itfpSet.add(itfi.policy_name);
        });

        this.itfpList = ['none'];
        this.itfpList = this.itfpList.concat(Array.from(itfpSet));
      });
    } else if (event.selectedIndex === 2) {
      this.getSttModels();
    }
  }

  /**
   * chatbotName 의 중복 예외처리를 위하여 chatbotNameList에 chatbot List의 Name을 저장한다.
   */
  checkDuplicateChatbotName() {
    this.grpc.getChatbotAllList().subscribe(
      chatbots => {
        this.chatbotNameList = [];
        chatbots.chatbot_list.map(chatbot => this.chatbotNameList.push(chatbot.name));
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
  }

  /***
   * IntentFinderInstance를 클릭하였을 때, 해당 IntentFinderPolicy의 정보를 불러온다.
   */
  selectItfPolicy(itfp: any) {
    this.itfGroup.patchValue({
      intent_finder_policy: itfp
    });

    this.itfGroup.patchValue({
      default_skill: undefined
    });

    if (this.itfGroup.get('intent_finder_policy').value === 'none') {
      this.itfp = null;
    } else {
      this.getIntentFinderPolicyInfo(itfp);
    }
    this.isDone();
  }

  getIntentFinderInstanceList() {
    return new Promise((resolve, reject) => {
      this.grpc.getIntentFinderInstanceList().subscribe(
        res => {
          this.itfiList = res.itfi_list;
          resolve(this.itfiList);
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          console.error('err', err.message);
          reject();
        }
      )
    });
  }

  /**
   * Intent Finder Instance 클릭시, Intent Finder Policy 정보 저장
   */
  getIntentFinderPolicyInfo(policyName: string): void {
    if (policyName !== 'none') {
      this.grpc.getIntentFinderPolicyInfo(policyName).subscribe(res => {
        if (res) {
          this.itfp = res;
          this.settingUIStep();
        }
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
    }
  }

  /**
   * 기존 IntentFinderPolicy steps에 화면에서만 쓰는 변수를 추가 후 세팅하여,
   * 사용자에게 Step 과 Hint 모델정보를 보여준다.
   */
  settingUIStep() {
    this.itfp.steps.forEach((step, index) => {
      step['seq'] = index + 1;

      // UI에 Skill, Hint 여부 표시 하기 위하여 세팅
      if (step.run_style === 'IFC_SKILL_FOUND_RETURN'
        || step.run_style === 'IFC_SKILL_FOUND_COLLECT_HINTS') {
        step.stepRunStyle = 'S';
        step.is_hint = false;
      } else if (step.run_style === 'IFC_HINT') {
        step.stepRunStyle = 'H';
        step.is_hint = true;
      }

      // UI에 Step에 등록된 Model 정보 표시
      switch (step.test_rule) {
        case 'custom_script_mod':
          step.type = 'CLM_CLASSIFY_SCRIPT';
          step.stepType = `<Custom Script>`;
          break;
        case 'pcre_mod':
          step.type = 'CLM_PCRE';
          step.stepType = `<PCRE - ${step.pcre_mod.model}>`;
          break;
        case 'dnn_cl_mod':
          step.type = 'CLM_DNN_CLASSIFIER';
          step.stepType = `<DNN - ${step.dnn_cl_mod.model}>`;
          break;
        case 'hmd_mod':
          step.type = 'CLM_HMD';
          step.stepType = `<HMD - ${step.hmd_mod.model}>`;
          break;
        case 'final_rule':
          step.type = 'CLM_FINAL_RULE';
          step.stepType = `<Final rule>`;
          break;
      }
    });
  }

  /**
   * Name 입력시 Title에 Name과 동일한 값을 세팅해준다.
   * 처음 한번만 title 입력 칸에 setting 해준다.
   * @param {string} name
   */
  setTitle(name: string) {
    if (this.role === 'add' && this.informationGroup.get('title').value === undefined) {
      this.informationGroup.patchValue({
        title: name
      });
    }
    this.cdr.detectChanges();
  }

  /**
   *
   * @param {string} lang ==> {IntentFinderPolicy 의 Language}
   * @returns {string} => Image의 URL 주소
   */
  getImageURL(lang: string): string {
    return lang === 'ko_KR' ? '/assets/img/korean_05.png' : '/assets/img/english_05.png';
  }

  getSttModels() {
    let chain = new Promise((resolve, reject) =>
      this.grpc.getSupervisorServerGroupList().subscribe(
        result => {
          if (result.svd_svr_grp_list.length === 0) {
            resolve();
          } else {
            let stt = result.svd_svr_grp_list[0].svd_proc_list.find(process => process.name === 'brain-stt');
            resolve(stt.state !== 0);
          }
        },
        err => reject(err)));

    chain = chain.then(isBrainSttActive => new Promise((resolve, reject) =>
      this.grpc.getSTTModels().subscribe(result => {
          this.store.dispatch({type: ROUTER_LOADED});
          // STT model
          if (isBrainSttActive) {
            this.sttModelList = result.models.sort((a, b) => a.model > b.model)
          }
          resolve();
        },
        err => reject(err))));

    chain.catch(err => {
      let message = `Something went wrong. [${getErrorString(err)}]`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  isDone() {
    if (this.informationGroup.valid && this.itfGroup.valid) {
      this.doneFlag = true;
    } else {
      this.doneFlag = false;
    }
  }

  submit() {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.informationGroup.get('name').value}'?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (this.role === 'add') {
          this.onAdd();
        } else {
          let chatbot = {
            name: this.informationGroup.get('name').value,
            title: this.informationGroup.get('title').value,
            description: this.informationGroup.get('description').value,
            active: this.informationGroup.get('active').value,
            intent_finder_policy: this.itfGroup.get('intent_finder_policy').value,
            default_skill: this.itfGroup.get('default_skill').value,
            session_timeout: this.informationGroup.get('session_timeout').value,
            external_system_info: this.informationGroup.get('external_system_info').value,
            stt_models: this.sttModelGroup.get('stt_models').value
          };

          if (JSON.stringify(chatbot) === JSON.stringify(this.originalChatbot)) {
            this.snackBar.open('Values are not changed. Please make some modification(s) first.', 'Confirm', {duration: 2000});
            return false;
          } else {
            this.onEdit();
          }
        }
      }
    });
  }

  onAdd() {
    let param =
      Object.assign({}, this.informationGroup.getRawValue(), this.itfGroup.getRawValue()
        , this.sttModelGroup.getRawValue());
    param.session_timeout = Number(param.session_timeout);

    // Hazelcast에 Chatbot 정보 입력
    this.grpc.insertChatbotInfo(param).subscribe(
      res => {
        this.snackBar.open(`Chatbot '${param.name}' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to create a Chatbot. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    )
  }

  onEdit() {
    let param =
      Object.assign({}, this.informationGroup.getRawValue(), this.itfGroup.getRawValue()
        , this.sttModelGroup.getRawValue());

    if (typeof param.session_timeout === 'string') {
      param.session_timeout = Number(param.session_timeout);
    }

    this.grpc.updateChatbotInfo(param).subscribe(
      result => {
        this.snackBar.open(`Chatbot '${param.name}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to update a Chatbot. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    );
  }

  backPage: any = (): void => {
    this.objectManager.clean('cb');
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  /**
   * objectManager에 chatbot이 없는 경우 조회를 합니다
   * 없는 chatbot을 조회한 결과, res.name='' 로 결과가 전달되어
   * res.name==='' 로 비교하여 예외처리 추가
   */
  getChatbotInfo() {
    return new Promise((resolve, reject) => {
      this.grpc.getChatbotInfo(this.chatbotName).subscribe(res => {
        if (res) {
          this.chatbot = res;
          this.objectManager.set('cb', this.chatbot);
          if (this.chatbot.name === undefined || this.chatbot.name === '') {
            reject();
          } else {
            resolve();
          }
        } else {
          reject();
          return;
        }
      });
    }).catch(() => {
      this.snackBar.open('Cannot Find Chatbot Name', 'Confirm', {duration: 10000});
      this.backPage();
      throw new Error();
    });
  }
}
