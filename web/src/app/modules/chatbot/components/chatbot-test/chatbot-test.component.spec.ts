import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatbotTestComponent} from './chatbot-test.component';

describe('ChatbotTestComponent', () => {
  let component: ChatbotTestComponent;
  let fixture: ComponentFixture<ChatbotTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChatbotTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
