import * as Recorder from 'minds-recorderjs'
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  SimpleChange,
  ChangeDetectorRef,
  ViewEncapsulation
} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {
  ErrorStateMatcher,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {
  AppEnumsComponent,
  FormErrorStateMatcher,
  getErrorString
} from '../../../../../shared';
import {GrpcApi} from '../../../../../core/sdk';
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';
import {ChatbotTestService} from '../chatbot-test.service';
import {DragScrollComponent} from 'ngx-drag-scroll/lib';
import {register} from 'ts-node';

const SPEECH_ENCODING = 'LINEAR16';
const SPEECH_CHANNEL_NO = 1;
const SPEECH_SAMPLING_RATE = 16000;
const SPEECH_RECORD_BUFFER_LENGTH = 4096;
const SPEECH_MODEL = 'baseline';
const SPEECH_PLAY_BUFFER_SIZE = 1024 * 1024;

@Component({
  selector: 'app-chat-main',
  templateUrl: './chat-main.component.html',
  styleUrls: ['./chat-main.component.scss'],
  providers: [{
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }, ChatbotTestService],
  encapsulation: ViewEncapsulation.None
})

export class ChatMainComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @Input() chatbot: any;
  @Output() changeSpec = new EventEmitter<any>();
  @Output() changeTalk = new EventEmitter<any>();
  @ViewChild('perfectScrollbar') perfectScrollbar: PerfectScrollbarComponent;
  @ViewChild('nav', {read: DragScrollComponent}) ds: DragScrollComponent;

  session: any;
  startIndex = -1;
  isWaitingToChat = true;
  isDebug = false;
  id: any;
  pwd: any;
  token: any;
  lang: string;
  talks = [];
  clickedTalk = {index: -1, who: undefined};
  selectedOption: string;
  chatInput: HTMLInputElement;
  height: any;
  index = 0;
  idFormControl = new FormControl(undefined, [
    Validators.required
  ]);
  pwdFormControl = new FormControl(undefined, [
    Validators.required
  ]);
  tokenFormControl = new FormControl(undefined, [
    Validators.required
  ]);

  selectedSpecOption = 'v3';
  selectOptionDisabled = false;
  authToken = undefined;
  deviceId = '';
  nextJobTimer: number = undefined;

  cards = []; // list of cards
  cards_list: any;

  // Speech to Speech talk context
  sts: {
    wsUrl: string,
    isRecording: boolean,
    isMicOn: boolean,
    sentParam: boolean,
    voiceInput: HTMLButtonElement,
    websocket: WebSocket,
    recorder: any,
    audioContext: AudioContext,
    audioStream: MediaStream,
    audioSource: AudioBufferSourceNode,
    bufferScraper: any,
    sentSize: number,
    talkIndex: number,
    streamEnded: boolean,
    speechPlaySamplingRate: number,
    speechBuffer: Float32Array,
    speechBufferIndex: number,
    speechBufferPlayedTotal: number,
    speechPlayer: any
  } = {
    wsUrl: undefined,
    isRecording: false,
    isMicOn: false,
    sentParam: false,
    voiceInput: undefined,
    websocket: undefined,
    recorder: undefined,
    audioContext: undefined,
    audioStream: undefined,
    audioSource: undefined,
    bufferScraper: undefined,
    sentSize: 0,
    talkIndex: -1,
    streamEnded: false,
    speechPlaySamplingRate: SPEECH_SAMPLING_RATE,
    speechBuffer: undefined,
    speechBufferIndex: 0,
    speechBufferPlayedTotal: 0,
    speechPlayer: undefined
  };

  constructor(private snackBar: MatSnackBar,
              private appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private grpc: GrpcApi,
              private el: ElementRef,
              private cdr: ChangeDetectorRef,
              private testChatService: ChatbotTestService) {
    this.sts.wsUrl = this.testChatService.getSpeechToSpeechTalkUrl();
  }

  ngOnInit() {
    this.whenSpecOptionChanged();
  }

  ngOnChanges(changes: SimpleChanges) {
    const chatbot: SimpleChange = changes.chatbot;
    if (chatbot) {
      if (chatbot.previousValue !== chatbot.currentValue) {
        this.changeChatbot();
      }
    }
  }

  ngAfterViewInit() {
    this.chatInput = <HTMLInputElement>(document.getElementById('chatInput'));
    this.sts.voiceInput = <HTMLButtonElement>(document.getElementById('voiceInput'));
    this.chatInput.disabled = true;
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  changeChatbot() {
    this.chatClose();
    this.session = undefined;
    this.selectedOption = 'Id/Pwd';
    this.id = undefined;
    this.pwd = undefined;
    this.token = undefined;
    this.lang = this.chatbot.english_cnt > this.chatbot.korean_cnt ?
      this.appEnum.policyLangKeys[this.appEnum.policyLang.en_US] :
      this.appEnum.policyLangKeys[this.appEnum.policyLang.ko_KR];
    this.chatInput.disabled = true;
    this.talks.length = 0;
    this.isWaitingToChat = true;
    this.changeSpec.emit('v3');
    this.idFormControl.reset();
    this.pwdFormControl.reset();
    this.resetSts();
  }

  orderMeta(response) {
    if (response.meta !== undefined) {
      let unordered = response.meta;
      let ordered = {};
      Object.keys(unordered).sort().forEach(function (key) {
        ordered[key] = unordered[key];
      });
      response.meta = ordered;
    }
  }

  resetNextJob() {
    if (this.nextJobTimer !== undefined) {
      clearTimeout(this.nextJobTimer);
      this.nextJobTimer = undefined;
    }
  }

  registerNextJob(self: any, res: any) {
    self.resetNextJob();
    if (res.hasOwnProperty('nextJob')) {
      const nextJob = res.nextJob;
      if (nextJob.hasOwnProperty('event')) {
        self.nextJobTimer = setTimeout(self.eventToTextTalk,
          nextJob.raiseAfter,
          self,
          nextJob.event);
      } else {
        console.log('unhandled nextJob', nextJob);
      }
    }
  }

  eventToTextTalk(self: any, event: any) {
    console.log('eventToTextTalk', event);

    event['meta'] = {
      debug: true,
      'number': 1.0,
      text: 'true',
      obj: [
        'complex',
        {example: 2345}
      ]
    };

    let eventParam = {
      'payload': event,
      'device': {
        'id': self.deviceId,
        'type': 'WEB',
        'version': '0.1',
        'channel': 'ADMINWEB'
      },
      'location': {
        'latitude': 10.3,
        'longitude': 20.5,
        'location': 'mindslab'
      },
      'authToken': self.authToken
    };

    const chatTime = new Date();
    let index = self.talks.push({
      user: undefined,
      bot: {utter: '......', metadata: undefined},
      cardList: []
    }) - 1;
    let talk = self.talks[index];

    const enableChatInput = () => {
      self.chatInput.placeholder = 'Type Something';
      self.chatInput.disabled = false;
      self.chatInput.focus();
      self.clickTalk(index, 'bot');
      self.setScroll(index);
    };

    self.testChatService.eventToTextTalk(eventParam).subscribe(res => {
      console.log('eventToTextTalk response: ', res);
      if (res.hasOwnProperty('exception')) {
        const exception = res.exception;
        talk.bot = {
          utter: `errCode : ${exception.statusCode} \n message : ${exception.exMessage}`,
          metadata: exception,
          time: new Date(exception.thrownAt).toLocaleString('en-US', {
            hour: 'numeric', minute: 'numeric', hour12: true
          }),
          type: 'E'
        };
      } else {
        const payload = res.directive.payload;
        const response = payload.response;
        self.orderMeta(response);
        talk.bot = {
          utter: response.speech.utter.replace(new RegExp('\r?\n', 'g'), '<br>'),
          metadata: res,
          time: new Date(res.directive.beginAt).toLocaleString('en-US', {
            hour: 'numeric', minute: 'numeric', hour12: true
          }),
          type: 'S'
        };
        self.registerNextJob(self, payload);
      }
      enableChatInput();
      self.changeTalk.emit(self.talks);
    }, err => {
      talk.bot = {
        utter: `Error: ${getErrorString(JSON.stringify(err))}`,
        metadata: [{attr: 'error', value: err.code}, {
          attr: 'detail',
          value: err.detail
        }]
      };
      enableChatInput();
    });
  }

  chatStart() {
    let message;
    if (this.chatbot === undefined) {
      message = 'Please Select Chatbot.';
    } else {
      switch (this.selectedOption) {
        case 'NotAuth':
          break;
        case 'Id/Pwd':
          if (this.id === undefined) {
            message = 'Please Input the id.';
          } else if (this.isFormError(this.idFormControl)) {
            message = 'Please Input the id.';
          } else if (this.pwd === undefined) {
            message = 'Please Input the pwd.';
          } else if (this.isFormError(this.pwdFormControl)) {
            message = 'Please Input the id.';
          }
          break;
        case 'Token':
          if (this.token === undefined) {
            message = 'Please Input the token.';
          } else if (this.isFormError(this.tokenFormControl)) {
            message = 'Please Input the token.';
          }
          break;
      }
    }

    if (message) {
      this.snackBar.open(message, 'Confirm', {duration: 2000});
      return;
    }

    if (this.selectedSpecOption === 'v1') {
      let caller = {
        chatbot: this.chatbot.name,
        name: 'm2u admin text',
        accessFrom: 'WEB_BROWSER'
      };
      this.grpc.openDialogService(caller).subscribe(
        session => {
          this.session = session;
          this.isDebug = false;
          this.chatInput.disabled = false;
          this.isWaitingToChat = false;
          this.resetSts();
          this.clickTalk(-1, '');
          if (this.chatbot.detail.greeting && this.chatbot.detail.greeting.length > 0) {
            this.talks.push({
              user: {utter: undefined, metadata: []},
              bot: {utter: this.chatbot.detail.greeting, metadata: []},
              cardList: []
            });
            this.changeTalk.emit(this.talks);
            this.startIndex = 0;
            this.setScroll(this.startIndex);
          } else {
            this.startIndex = -1;
          }
        },
        err => {
          this.snackBar.open('Failed to open a Dialog.', 'Confirm', {duration: 10000});
        }
      );
    } else if (this.selectedSpecOption === 'v3') {

      let signInParam = {
        'userKey': this.id,
        'passPhrase': this.pwd
      };

      this.testChatService.signIn(signInParam).subscribe(
        res => {
          // MAP 정상 응답
          if (res.hasOwnProperty('directive')) {
            const directive = res['directive'];

            // 인증 성공
            if (directive.payload.hasOwnProperty('authSuccess')) {
              const authSuccess = directive.payload.authSuccess;
              this.authToken = authSuccess.authToken;

              let openParam = {
                'payload': {
                  'utter': 'UTTER1',
                  'lang': this.lang,
                  'chatbot': this.chatbot.name,
                  'meta': {
                    'debug': true
                  }
                },
                'device': {
                  'id': this.deviceId,
                  'type': 'WEB',
                  'version': '0.1',
                  'channel': 'ADMINWEB'
                },
                'location': {
                  'latitude': 10.3,
                  'longitude': 20.5,
                  'location': 'mindslab'
                },
                'authToken': this.authToken
              };
              this.session = {};

              openParam.device.id = 'WEB_' + this.randomString();
              this.deviceId = openParam.device.id;


              this.testChatService.open(openParam).subscribe(
                openres => {
                  let payload;
                  let response;
                  if (openres.directive !== undefined) {
                    payload = openres.directive.payload;
                    response = payload.response;
                  }
                  this.isDebug = false;
                  this.chatInput.disabled = false;
                  this.isWaitingToChat = false;
                  this.resetSts();
                  if (openres.hasOwnProperty('exception')) {
                    const exception = openres.exception;
                    this.talks.push({
                      bot: {
                        utter: `errCode : ${exception.statusCode} \n message : ${exception.exMessage}`,
                        metadata: exception,
                        time: new Date(exception.thrownAt).toLocaleString('en-US', {
                          hour: 'numeric',
                          minute: 'numeric',
                          hour12: true
                        }),
                        type: 'E'
                      }
                    });
                  } else {
                    this.orderMeta(openres.directive.payload.response);
                    let bot = {
                      utter: openres.directive.payload.response.speech.utter,
                      metadata: openres,
                      time: new Date(res.directive.beginAt).toLocaleString('en-US', {
                        hour: 'numeric',
                        minute: 'numeric',
                        hour12: true
                      }),
                      type: 'S'
                    };
                    this.cards.length = 0;
                    this.cards = [];
                    if (openres && openres.directive && payload && response && response.cards) {
                      this.cards_list = response.cards;
                      this.cards_list.forEach(a => {
                        let x: any;
                        x = a;
                        if (x.hasOwnProperty('select')) {
                          this.cards.push(x);
                        }
                        if (x.hasOwnProperty('linkList')) {
                          this.cards.push(x);
                        }
                        if (x.hasOwnProperty('link')) {
                          this.cards.push(x);
                        }
                      });
                    }
                    this.talks.push({
                      bot: bot,
                      cardList: this.cards
                    });
                    this.session['id'] = openres.directive.payload.response.meta['m2u.sessionId'];
                    this.registerNextJob(this, openres.directive.payload);
                  }
                  this.changeTalk.emit(this.talks);
                  this.startIndex = -1;
                  this.clickTalk(0, 'bot');
                },
                err => {
                  this.snackBar.open(`Failed to open a Dialog. [${getErrorString(err)}]`, 'Confirm', {duration: 10000});
                }
              )

              // 인증 실패
            } else if (directive.payload.hasOwnProperty('authFailure')) {
              const authFailure = directive.payload.authFailure;
              this.snackBar.open(`Failed to Login. [code:${authFailure.resCode}
              , message : ${authFailure.message}]`, 'Confirm', {duration: 10000});
            }

            // MAP 오류 응답
          } else if (res.hasOwnProperty('exception')) {
            const exception = res['exception'];
            this.snackBar.open(`Failed to Login. [code:${exception.statusCode}
            , message : ${exception.exMessage}]`, 'Confirm', {duration: 10000});
          }
        },
        err => {
          this.snackBar.open(`Failed to Login. [${getErrorString(err)}]`, 'Confirm', {duration: 10000});
        }
      )
    }
  }


  inputChat() {
    let promise;
    if (this.chatInput.value.length < 1) {
      return;
    }
    this.resetNextJob();
    let inMetadata = [
      {attr: 'in.sessionid', value: this.session.id},
      {attr: 'in.lang', value: 'kor'}
    ];
    if (this.isDebug) {
      inMetadata.push({attr: 'in.debug', value: 'true'});
    }

    let index;
    if (this.selectedSpecOption === 'v3') {
      const chatTime = new Date();
      index = this.talks.push({
        user: {
          utter: this.chatInput.value,
          metadata: '',
          time: `${chatTime.toLocaleString('en-US', {
            hour: 'numeric',
            minute: 'numeric',
            hour12: true
          })}`
        },
        bot: {utter: '......', metadata: undefined},
        cardList: []
      }) - 1;
    } else {
      index = this.talks.push({
        user: {
          utter: this.chatInput.value,
          metadata: inMetadata
        },
        bot: {utter: '......', metadata: undefined},
        cardList: []
      }) - 1;
    }

    let talk = this.talks[index];
    const enableChatInput = () => {
      this.chatInput.placeholder = 'Type Something';
      this.chatInput.disabled = false;
      this.chatInput.focus();
      this.clickTalk(index, 'bot');
      this.setScroll(index);
    };

    if (this.selectedSpecOption === 'v1') {
      this.grpc.sendSimpleTextTalk({utter: talk.user.utter}, talk.user.metadata).subscribe(
        result => {
          talk.bot = {
            utter: result.utter.length > 0 ? result.utter : '{ none }',
            metadata: result.$metadata
          };
          this.changeTalk.emit(this.talks);
          enableChatInput();
        },
        err => {
          talk.bot = {
            utter: `Error: ${getErrorString(err)}`,
            metadata: [
              {attr: 'error', value: err.code},
              {attr: 'detail', value: err.detail},
            ]
          };
          enableChatInput();
        }
      );
    } else if (this.selectedSpecOption === 'v3') {

      let textToTextTalkParam = {
        'payload': {
          'utter': talk.user.utter,
          'lang': this.lang,
          'meta': {
            debug: true,
            'number': 1.0,
            text: 'true',
            obj: [
              'complex',
              {example: 2345}
            ]
          }
        },
        'device': {
          'id': this.deviceId,
          'type': 'WEB',
          'version': '0.1',
          'channel': 'ADMINWEB'
        },
        'location': {
          'latitude': 10.3,
          'longitude': 20.5,
          'location': 'mindslab'
        },
        'authToken': this.authToken
      };

      promise = new Promise((resolve, reject) => {
        this.testChatService.textToTextTalk(textToTextTalkParam).subscribe(res => {
          this.cards.length = 0;
          if (res.hasOwnProperty('exception')) {
            const exception = res.exception;
            talk.user.metadata = textToTextTalkParam;
            talk.bot = {
              utter: `errCode : ${exception.statusCode} \n message : ${exception.exMessage}`,
              metadata: exception,
              time: new Date(exception.thrownAt).toLocaleString('en-US', {
                hour: 'numeric', minute: 'numeric', hour12: true
              }),
              type: 'E'
            };
          } else {
            const payload = res.directive.payload;
            const response = payload.response;
            this.orderMeta(response);
            talk.user.metadata = textToTextTalkParam;
            talk.bot = {
              utter: response.speech.utter.replace(new RegExp('\r?\n', 'g'), '<br>'),
              metadata: res,
              time: new Date(res.directive.beginAt).toLocaleString('en-US', {
                hour: 'numeric', minute: 'numeric', hour12: true
              }),
              type: 'S'
            };
            this.cards = [];
            if (res && res.directive && payload && response && response.cards) {
              this.cards_list = response.cards;
              this.cards.length = 0;
              this.cards_list.forEach(a => {
                let x: any;
                x = a;
                if (x.hasOwnProperty('select')) {
                  this.cards.push(x);
                }
                if (x.hasOwnProperty('linkList')) {
                  this.cards.push(x);
                }
                if (x.hasOwnProperty('link')) {
                  this.cards.push(x);
                }
              });
              resolve([index, this.cards]);
            }
            this.registerNextJob(this, payload);
          }
          enableChatInput();
          this.changeTalk.emit(this.talks);
        }, err => {
          talk.bot = {
            utter: `Error: ${getErrorString(JSON.stringify(err))}`,
            metadata: [{attr: 'error', value: err.code}, {
              attr: 'detail',
              value: err.detail
            }]
          };
          enableChatInput();
        });
      });
      promise.then((resList) => {
        this.talks[resList[0]].cardList = resList[1];
      });
    } else {
      let talkQuery = {
        query: {
          utter: this.chatInput.value,
          lang: this.lang
        },
        chatbot: this.chatbot.name,
        meta: {fields: {}}
      };
      talk.user.metadata.push({attr: 'in.devicetoken', value: this.session.id});
      talk.user.metadata.forEach(data => {
        talkQuery.meta.fields[data.attr] = {
          kind: 'string_value',
          string_value: data.value
        }
      });

      this.grpc.sendTalkAnalyze(talkQuery, talk.user.metadata).subscribe(
        result => {
          let metaFields = result.answer.meta.fields;
          let outMetadata = [];
          Object.keys(metaFields).forEach(key => {
            let field = metaFields[key];
            outMetadata.push({attr: key, value: field[field.kind]})
          });
          talk.bot = {
            utter: result.answer.utter.length > 0 ? result.answer.utter : '{ none }',
            metadata: outMetadata
          };
          this.changeTalk.emit(this.talks);
          enableChatInput();
        },
        err => {
          talk.bot = {
            utter: `Error: ${getErrorString(err)}`,
            metadata: [
              {attr: 'error', value: err.code},
              {attr: 'detail', value: err.detail},
            ]
          };
          enableChatInput();
        }
      );
    }

    this.chatInput.value = '';
    this.chatInput.placeholder = 'Waiting for reply...';
    this.chatInput.disabled = true;
    this.clickTalk(index, 'user');
    this.chatInput.focus();
    this.setScroll(index);
    this.cards.length = 0;
    this.cards_list = [];
  }


  setScroll(index) {
    setTimeout(() => {
      this.scrollToBottom(index);
    });
  }

  scrollToBottom(index): void {
    let chatText: any = document.querySelector(`#test-chat-contents${index}`);
    // let chatText: any = document.querySelector(`#test-chat-contents-left-text${index}`);
    // let metaDataText: any = document.querySelector(`#metadata${index}`);
    let chatHeight = chatText.offsetHeight;
    // let metaDataHeight = metaDataText.offsetHeight * 2;

    try {
      this.height += chatHeight;
      // this.panelHeight += metaDataHeight;
      this.perfectScrollbar.directiveRef.scrollToBottom(this.height);
      // this.infoPanel.panelScrollBar.directiveRef.scrollToBottom(this.panelHeight);
    } catch (err) {
      console.log('scrollHeight is null');
    }
  }

  clickTalk = (index: number, who: string) => {
    if (this.talks[this.clickedTalk.index] !== undefined) {
      if (this.talks[this.clickedTalk.index][this.clickedTalk.who] !== undefined) {
        this.talks[this.clickedTalk.index][this.clickedTalk.who].clicked = undefined;
      }
    }
    if (index > this.startIndex && this.talks[index] !== undefined) {
      this.talks[index][who].clicked = 'talk-clicked';
    }
    this.clickedTalk = {index: index, who: who};
    this.changeTalk.emit(this.talks);
  };

  chatClose() {
    if (this.session) {
      if (this.selectedSpecOption === 'v3') {
        this.resetNextJob();

        let closeParam = {
          'device': {
            'id': this.deviceId,
            'type': 'WEB',
            'version': '0.1',
            'channel': 'ADMINWEB'
          },
          'location': {
            'latitude': 10.3,
            'longitude': 20.5,
            'location': 'mindslab'
          },
          'authToken': this.authToken
        };

        this.testChatService.close(closeParam).subscribe(
          res => {
            this.selectedSpecOption = 'v3';
            this.selectedOption = 'Id/Pwd';
            this.selectOptionDisabled = true;
          },
          err => {
            let message = `while closing session(${this.authToken}). [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        )
      } else {
        let sessionId = this.session.id;
        this.grpc.closeDialogService({session_id: sessionId}).subscribe(
          SessionStat => {
          },
          err => {
            let message = `while closing session(${sessionId}). [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      }
    }
    this.selectedSpecOption = 'v3';
    this.selectedOption = 'Id/Pwd';
    this.selectOptionDisabled = true;
  }

  whenSpecOptionChanged() {
    if (this.selectedSpecOption === 'v1') {
      this.selectedOption = 'NotAuth';
      this.selectOptionDisabled = false;
    } else if (this.selectedSpecOption === 'v3') {
      this.selectedOption = 'Id/Pwd';
      this.selectOptionDisabled = true;
    }
    this.changeSpec.emit(this.selectedSpecOption);
  }


  randomString() {
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    const string_length = 15;
    let randomstring = '';
    for (let i = 0; i < string_length; i++) {
      let rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
  }

  checkEnterKeyDown(event, flag) {
    if (event.keyCode === 13) {
      if (flag === 'login') {
        this.chatStart();
      } else if (flag === 'chat') {
        this.inputChat();
        event.preventDefault();
      }
    }
  }

  resendUtter(utter) {
    this.chatInput.value = utter;
    this.inputChat();
  }

  restartSession() {
    this.resetNextJob();
    let closeParam = {
      'device': {
        'id': this.deviceId,
        'type': 'WEB',
        'version': '0.1',
        'channel': 'ADMINWEB'
      },
      'location': {'latitude': 10.3, 'longitude': 20.5, 'location': 'mindslab'},
      'authToken': this.authToken
    };

    this.testChatService.close(closeParam).subscribe(
      res => {
        let openParam = {
          'payload': {
            'utter': 'UTTER1',
            'lang': this.lang,
            'chatbot': this.chatbot.name,
            'meta': {
              'debug': true
            }
          },
          'device': {
            'id': this.deviceId,
            'type': 'WEB',
            'version': '0.1',
            'channel': 'ADMINWEB'
          },
          'location': {
            'latitude': 10.3,
            'longitude': 20.5,
            'location': 'mindslab'
          },
          'authToken': this.authToken
        };
        let info = {
          restartSessionInfo: {
            prevSessionId: this.session.id,
            newSessionId: '-',
          }
        };
        this.session = {};

        openParam.device.id = 'WEB_' + this.randomString();
        this.deviceId = openParam.device.id;
        this.testChatService.open(openParam).subscribe(
          openres => {
            let payload;
            let response;
            if (openres.directive !== undefined) {
              payload = openres.directive.payload;
              response = payload.response;
            }
            this.isDebug = false;
            this.chatInput.disabled = false;
            this.isWaitingToChat = false;
            this.resetSts();
            info.restartSessionInfo.newSessionId = openres.directive.payload.response.meta['m2u.sessionId'];
            let index = this.talks.push(info);
            this.chatInput.focus();
            this.setScroll(index - 1);
            let new_index;
            if (openres.hasOwnProperty('exception')) {
              const exception = openres.exception;
              new_index = this.talks.push({
                bot: {
                  utter: `errCode : ${exception.statusCode} \n message : ${exception.exMessage}`,
                  metadata: exception,
                  time: new Date(exception.thrownAt).toLocaleString('en-US', {
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: true
                  }),
                  type: 'E'
                }
              });
            } else {
              this.cards.length = 0;
              this.cards = [];
              if (openres && openres.directive && payload && response && response.cards) {
                this.cards_list = response.cards;
                this.cards.length = 0;
                this.cards_list.forEach(a => {
                  let x: any;
                  x = a;
                  if (x.hasOwnProperty('select')) {
                    this.cards.push(x);
                  }
                  if (x.hasOwnProperty('linkList')) {
                    this.cards.push(x);
                  }
                  if (x.hasOwnProperty('link')) {
                    this.cards.push(x);
                  }
                });
              }
              this.orderMeta(openres.directive.payload.response);
              new_index = this.talks.push({
                bot: {
                  utter: openres.directive.payload.response.speech.utter,
                  metadata: openres,
                  time: new Date(openres.directive.beginAt).toLocaleString('en-US', {
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: true
                  }),
                  type: 'S'
                },
                cardList: this.cards
              });
              this.session['id'] = openres.directive.payload.response.meta['m2u.sessionId'];
              this.registerNextJob(this, openres.directive.payload);
            }

            this.changeTalk.emit(this.talks);
            this.startIndex = -1;
            this.clickTalk(new_index - 1, 'bot');
            this.setScroll(new_index - 1);
          },
          err => {
            this.snackBar.open(`Failed to open a Dialog. [${getErrorString(err)}]`, 'Confirm', {duration: 10000});
          }
        )
      },
      err => {
        let message = `while closing session(${this.authToken}). [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    )
  }

  startRecording() {
    console.log('startRecording');
    this.resetNextJob();
    this.setupWebsocket();
  }

  stopRecording() {
    console.log('stopRecording');
    this.resetSts();
  }

  resetRecorder() {
    this.sts.isMicOn = false;
    if (!!this.sts.bufferScraper) {
      clearInterval(this.sts.bufferScraper);
      this.sts.bufferScraper = undefined;
    }
    if (!!this.sts.audioStream) {
      this.sts.audioStream.getAudioTracks()[0].stop();
      this.sts.audioStream = undefined;
    }
    if (!!this.sts.recorder) {
      this.sts.recorder.stop();
      this.sts.recorder.clear();
      this.sts.recorder = undefined;
    }
    if (this.sts.sentSize > 0) {
      console.log('resetRecorder Total sent bytes:', this.sts.sentSize);
      this.sts.sentSize = 0;
    }
  }

  resetSts() {
    console.log('resetSts');
    this.resetRecorder();
    if (!!this.sts.speechPlayer) {
      clearTimeout(this.sts.speechPlayer);
      this.sts.speechPlayer = undefined;
    }
    this.sts.speechBuffer = undefined;
    this.sts.speechBufferIndex = 0;
    this.sts.speechBufferPlayedTotal = 0;
    if (!!this.sts.audioSource) {
      this.sts.audioSource.stop(0);
      this.sts.audioSource = undefined;
    }
    this.sts.audioContext = undefined;
    if (!!this.sts.websocket) {
      this.sts.websocket.close(1000);
    }
    this.sts.websocket = undefined;
    this.sts.voiceInput.disabled = false;
    this.sts.isRecording = false;
    this.sts.isMicOn = false;
    this.sts.sentParam = false;
    this.sts.talkIndex = -1;
    this.sts.streamEnded = false;
  }

  isWebsocketReady(): boolean {
    return !!this.sts.websocket &&
      this.sts.websocket.readyState === WebSocket.OPEN;
  }

  getAudioBuffer = () => {
    this.sts.recorder.exportPCM((blob: Blob) => {
      if (!this.sts.sentParam) {
        this.sendSpeechParam();
        this.sts.sentParam = true;
      }
      if (!this.isWebsocketReady()) {
        this.snackBar.open(`Invalid Websocket State(${this.sts.websocket.readyState}).`,
          'Confirm', {duration: 10000});
        this.resetSts();
        return;
      }
      if (!this.sts.recorder) {
        console.log('recorder is not valid.');
        return;
      }

      this.sts.websocket.send(blob);
      this.sts.recorder.clear();
      this.sts.sentSize += blob.size;
      if (!this.sts.isMicOn && this.sts.sentSize > 1000) {
        this.sts.talkIndex = this.talks.push({
          user: {
            utter: '......',
            metadata: '',
            time: `${new Date().toLocaleString('en-US', {
              hour: 'numeric',
              minute: 'numeric',
              hour12: true
            })}`
          },
          bot: {utter: undefined, metadata: undefined},
          cardList: []
        }) - 1;
        this.sts.isMicOn = true;
      }
    });
  };

  setupWebsocket() {
    try {
      let ws = new WebSocket(this.sts.wsUrl);
      ws.binaryType = 'blob';
      ws.onopen = event => {
        console.log('open ', event);
        this.sts.websocket = ws;
        setTimeout(this.setupRecorder, 0);
      };
      ws.onclose = event => {
        console.log('close ', event);
        if (this.sts.isRecording) {
          this.resetSts();
          this.snackBar.open(`Websocket disconnected.`, 'Confirm', {duration: 10000});
        }
      };
      ws.onmessage = this.handleWsMessage;
      ws.onerror = reason => {
        this.resetSts();
        console.log('error', reason);
        this.snackBar.open(`Websocket error. [${getErrorString(reason)}]`,
          'Confirm', {duration: 10000});
      };
    } catch
      (err) {
      this.snackBar.open(`Failed to  is setup the websocket. [${getErrorString(err)}]`,
        'Confirm', {duration: 10000});
    }
  }

  setupRecorder = () => {
    navigator.mediaDevices.getUserMedia({audio: true})
    .then(stream => {
      let audioContext = new AudioContext();
      let input = audioContext.createMediaStreamSource(stream);
      console.log('Media stream successfully created');

      // Initialize the Recorder Library
      let config = {
        sampleRate: SPEECH_SAMPLING_RATE,
        bufferLen: SPEECH_RECORD_BUFFER_LENGTH,
        numChannels: SPEECH_CHANNEL_NO
      };
      let recorder = new Recorder(input, config);

      console.log('Recorder initialized');
      // Start recording !
      recorder.record();
      console.log('Recording...');

      this.sts.recorder = recorder;
      this.sts.audioContext = audioContext;
      this.sts.audioStream = stream;
      this.sts.isRecording = true;
      this.sts.bufferScraper = setInterval(this.getAudioBuffer, 50);
    })
    .catch(err => {
      this.snackBar.open(`Failed to start the recoder. [${getErrorString(err)}]`,
        'Confirm', {duration: 10000});
      this.resetSts();
    });
  };

  sendSpeechParam() {
    console.log('WebSocket Send,');
    let speechParam = {
      payload: {
        lang: this.lang,
        meta: {
          debug: true
        }
      },
      param: {
        speech_recognition_param: {
          encoding: SPEECH_ENCODING,
          sample_rate: SPEECH_SAMPLING_RATE,
          model: SPEECH_MODEL,
          single_utterance: true
        }
      },
      device: {
        id: this.deviceId,
        type: 'WEB',
        version: '0.1',
        channel: 'ADMINWEB',
        support: {
          support_render_text: true,
          support_render_card: false,
          support_speech_synthesizer: true,
          support_expect_speech: true,
          support_action: false
        }
      },
      location: {
        latitude: 10.3,
        longitude: 20.5,
        location: 'mindslab'
      },
      authToken: this.authToken,
      authInternal: 'm2u-auth-internal'
    };
    if (this.isWebsocketReady()) {
      this.sts.websocket.send(JSON.stringify(speechParam));
    } else {
      const message = this.sts.websocket ?
        `Invalid Websocket State(${this.sts.websocket.readyState})` :
        'Invalid Websocket';
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.resetSts();
    }
  }

  handleWsMessage = message => {
    if (message.type === 'message') {
      let data;
      try {
        data = JSON.parse(message.data);
      } catch (e) {
        console.log(`Exception occurred. message data: [${message.data}]`);
        console.log(e);
        this.resetSts();
        return;
      }

      if (data.hasOwnProperty('directive')) {
        let ifc = data.directive.interface;
        let payload = data.directive.payload;

        // 음성인식 메시지 수신 시
        if (ifc.interface === 'SpeechRecognizer') {
          if (ifc.operation === 'Recognizing') {
            // 음성인식 중 미완성 인식결과
            const recognizingUtter = payload.results[0].transcript.trim();
            console.log('Recognizing... \'%s\'', recognizingUtter, Date.now().toString());
            this.talks[this.sts.talkIndex].user.utter = recognizingUtter;
          } else if (ifc.operation === 'Recognized') {
            // 음성인식 완료
            const recognizedUtter = payload.results[0].transcript.trim();
            console.log('Recognized. \'%s\'', recognizedUtter, Date.now().toString());
            this.resetRecorder();
            this.sts.voiceInput.disabled = true;
            this.talks[this.sts.talkIndex].user.utter = recognizedUtter;
            this.talks[this.sts.talkIndex].user.metadata = data;
          } else {
            console.log('unhandled operation: [%s:%s]', ifc.interface, ifc.operation);
          }
        } else if (ifc.interface === 'View') {
          if (ifc.operation === 'RenderText') {
            // 단순 답변 수신
            const index = this.sts.talkIndex;
            const answer = payload.text;
            console.log('RenderText [%s]', answer);
            this.talks[index].bot.utter = answer;
            this.talks[index].bot.metadata = data;
            this.talks[index].bot.time = new Date(data.directive.beginAt)
            .toLocaleString('en-US', {
              hour: 'numeric', minute: 'numeric', hour12: true
            });
            this.talks[index].bot.type = 'S';
            this.clickTalk(index, 'bot');
            this.setScroll(index);
          } else if (ifc.operation === 'RenderHidden') {
            const index = this.sts.talkIndex;
            this.talks[index].bot.metadata.directive.payload['meta'] = data.directive.payload;
          } else {
            console.log('unhandled operation: [%s:%s]', ifc.interface, ifc.operation);
          }
        } else if (ifc.interface === 'SpeechSynthesizer') {
          if (ifc.operation === 'Play') {
            const param = data.directive.param.speechSynthesizerParam;
            this.sts.speechPlaySamplingRate = param.sampleRate;
            this.sts.speechBuffer = new Float32Array(SPEECH_PLAY_BUFFER_SIZE);
            console.log('Set Player Parameter:', param, payload);
            this.playSpeech();
          } else {
            console.log('unhandled operation: [%s:%s]', ifc.interface, ifc.operation);
          }
        } else if (ifc.interface === 'NextJob') {
          if (ifc.operation === 'Set') {
            const index = this.sts.talkIndex;
            this.talks[index].bot.metadata['nextJob'] = payload;
            this.registerNextJob(this, {nextJob: payload});
          } else if (ifc.operation === 'Reset') {
            this.resetNextJob();
          } else {
            console.log('unhandled operation: [%s:%s]', ifc.interface, ifc.operation);
          }
        } else {
          console.log('unhandled interface: ', ifc, '\n\tpayload: ', payload);
        }
      } else if (data.hasOwnProperty('bytes')) {
        // TTS Stream 수신 후 버퍼에 저장
        // console.log('bytes message: ', data.bytes.length. Date.now().toString());
        this.queueSpeechBuffer(data.bytes);
      } else if (data.hasOwnProperty('streamBreak')) {
        // TTS 중단 수신
        console.log('StreamBreak message: ', data);
        this.resetSts();
      } else if (data.hasOwnProperty('streamEnd')) {
        // TTS END 수신
        console.log('StreamEnd message: ', data);
        this.sts.streamEnded = true;
        if (!this.sts.speechPlayer) {
          this.resetSts();
        }
        // this.playSpeechAll();
      } else if (data.hasOwnProperty('exception')) {
        let index = this.sts.talkIndex;
        this.resetSts();
        const exception = data.exception;
        if (this.sts.talkIndex > -1) {
          let index = this.sts.talkIndex;
          this.talks[index].bot.utter = `errCode : ${exception.statusCode} \n message : ${exception.exMessage}`;
          this.talks[index].bot.metadata = exception;
          this.talks[index].bot.time = new Date(exception.thrownAt).toLocaleString('en-US',
            {hour: 'numeric', minute: 'numeric', hour12: true});
          this.talks[index].bot.type = 'E';
          this.clickTalk(index, 'bot');
          this.setScroll(index);
        } else {
          let index = this.talks.push({
            user: {
              utter: `errCode : ${exception.statusCode} \n message : ${exception.exMessage}`,
              metadata: exception,
              time: `${new Date().toLocaleString('en-US', {
                hour: 'numeric',
                minute: 'numeric',
                hour12: true
              })}`
            },
            bot: {utter: undefined, metadata: undefined},
            cardList: []
          }) - 1;
          this.clickTalk(index, 'user');
          this.setScroll(index);
        }
      } else {
        console.log('unhandled message: ', message);
      }
    } else {
      console.log('unhandled message: ', message);
    }
  };

  downloadSpeech() {
    const blob = new Blob([this.sts.speechBuffer], {type: 'application/octet-stream'});

    let a: any;
    a = document.createElement('a');
    a.href = window.URL.createObjectURL(blob);
    a.download = 'tts.bin';
    document.body.appendChild(a);
    a.style = 'display: none';
    a.click();
    a.remove();
  }

  queueSpeechBuffer(encoded: string) {
    const raw = window.atob(encoded);
    let length = raw.length;
    const uint8Array = new Uint8Array(length);
    for (let i = 0; i < length; i++) {
      uint8Array[i] = raw.charCodeAt(i);
    }

    const int16Array = new Int16Array(uint8Array.buffer);

    int16Array.forEach((value) => {
      const index = this.sts.speechBufferIndex++;
      this.sts.speechBuffer[index] = value / (value < 0 ? 32768 : 32767);
    });
  }

  playSpeech = () => {
    const sts = this.sts;
    const queuedLength = sts.speechBufferIndex;
    console.log('playSpeech queued:', queuedLength, 'stremEnded:', sts.streamEnded, Date.now().toString());

    if (queuedLength === 0) {
      if (sts.streamEnded) {
        console.log('playSpeech End Total:', this.sts.speechBufferPlayedTotal);
        this.resetSts();
      } else {
        sts.speechPlayer = setTimeout(this.playSpeech, sts.speechBufferIndex > 0 ? 50 : 500);
      }
      return;
    }

    console.log('playSpeech length:', this.sts.speechBufferIndex, this.sts.speechBuffer.length);

    if (queuedLength > 8192 || sts.streamEnded) {
      const buffer = sts.speechBuffer.slice(0, sts.speechBufferIndex);
      let audioBuffer = sts.audioContext.createBuffer(SPEECH_CHANNEL_NO,
        buffer.length,
        this.sts.speechPlaySamplingRate);
      audioBuffer.getChannelData(0).set(buffer);
      sts.audioSource = sts.audioContext.createBufferSource();
      sts.audioSource.buffer = audioBuffer;
      sts.audioSource.connect(sts.audioContext.destination);
      sts.audioSource.onended = () => {
        sts.speechBufferPlayedTotal += queuedLength;
        console.log('playSpeech Played:', this.sts.speechBufferPlayedTotal);
        sts.speechPlayer = setTimeout(this.playSpeech, 0);
      };
      this.sts.audioSource.start(0);
      sts.speechBuffer = new Float32Array(SPEECH_PLAY_BUFFER_SIZE);
      sts.speechBufferIndex = 0;
    } else {
      console.log('playSpeech buffering ', queuedLength);
      sts.speechPlayer = setTimeout(this.playSpeech, 10);
    }
  };

  playSpeechAll() {
    console.log('playSpeechAll', Date.now().toString());

    if (this.sts.speechBufferIndex === 0) {
      this.resetSts();
      return;
    }

    console.log('speech length:', this.sts.speechBufferIndex, this.sts.speechBuffer.length);
    let audioBuffer = this.sts.audioContext.createBuffer(SPEECH_CHANNEL_NO,
      this.sts.speechBuffer.length,
      SPEECH_SAMPLING_RATE);
    audioBuffer.getChannelData(0).set(this.sts.speechBuffer);
    this.sts.audioSource = this.sts.audioContext.createBufferSource();
    this.sts.audioSource.buffer = audioBuffer;
    this.sts.audioSource.connect(this.sts.audioContext.destination);
    this.sts.audioSource.start(0, 0, this.sts.speechBufferIndex);
    this.sts.audioSource.onended = () => this.resetSts();
  }

  ngOnDestroy() {
    this.chatClose();
  }

  goListPage(url: string) {
    window.open(url);
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }
}
