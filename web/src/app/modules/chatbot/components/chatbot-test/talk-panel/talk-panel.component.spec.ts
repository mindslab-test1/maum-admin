import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TalkPanelComponent} from './talk-panel.component';

describe('TalkPanelComponent', () => {
  let component: TalkPanelComponent;
  let fixture: ComponentFixture<TalkPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TalkPanelComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalkPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
