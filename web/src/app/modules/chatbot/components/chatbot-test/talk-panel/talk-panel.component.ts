import {
  Component,
  Input,
  OnInit,
  ViewChild,
  OnChanges,
  SimpleChanges,
  SimpleChange,
  ChangeDetectorRef,
  ViewEncapsulation
} from '@angular/core';
import {AppEnumsComponent, getErrorString} from '../../../../../shared';
import {ROUTER_LOADED} from '../../../../../core/actions';
import {GrpcApi} from '../../../../../core/sdk/services/custom/Grpc';
import {Store} from '@ngrx/store';
import {MatSnackBar} from '@angular/material';
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';
import {MatTableDataSource} from '@angular/material/table';
import {TableComponent} from '../../../../../shared';

@Component({
  selector: 'app-talk-panel',
  templateUrl: './talk-panel.component.html',
  styleUrls: ['./talk-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TalkPanelComponent implements OnInit, OnChanges {
  @Input() chatbot: any;
  @Input() spec: string;
  @Input() talks: any;
  @ViewChild('panelScrollBar') panelScrollBar: PerfectScrollbarComponent;

  table: any;
  selectedIndex = 0;
  clickMeta: any;
  clickedTalk: any;
  startIndex = -1;
  chatbotInfo;
  initFlag = true;
  response_json: {
    clicked: string;
    metadata: any;
    type: string;
    time: string;
    utter: string;
  };
  entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    '\'': '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };
  @ViewChild('tableComponent') daiTableComponent: TableComponent;
  dataSource: MatTableDataSource<any>;
  header;
  rows: any[] = [];

  constructor(private store: Store<any>,
              public appEnum: AppEnumsComponent,
              private grpc: GrpcApi,
              private cdr: ChangeDetectorRef,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.header = [
      {attr: 'name', name: 'Name', isSort: true},
      {attr: 'lang', name: 'Lang', onlyImage: this.getImage},
      {attr: 'da_name', name: 'Da', isSort: true},
    ];

    this.clickedTalk = {index: 0, who: 'bot'};
  }

  ngOnChanges(changes: SimpleChanges) {
    const chatbotChange: SimpleChange = changes.chatbot;
    if (chatbotChange) {
      if (chatbotChange.previousValue !== chatbotChange.currentValue) {
        this.setContent();
        this.initFlag = true;
      }
    }

    const talksChange: SimpleChange = changes.talks;
    console.log('length : ' + this.talks.length);
    if (talksChange) {
      if (talksChange.previousValue !== talksChange.currentValue) {
        this.talks = talksChange.currentValue;
        this.talks.forEach((item, index) => {
          if (item.hasOwnProperty('bot')) {
            if (item.bot.clicked === 'talk-clicked') {
              this.clickedTalk = {index: index, who: 'bot'};
            }
          }
          if (item.hasOwnProperty('user')) {
            if (item.user.clicked === 'talk-clicked') {
              this.clickedTalk = {index: index, who: 'user'};
            }
          }
        });
        if (this.talks.length > 0) {
          this.response_json = this.talks[this.clickedTalk.index][this.clickedTalk.who];
          if (this.response_json.metadata && this.response_json.metadata.response) {
            this.response_json.metadata.directive.payload.response.speech.speechUtter = String(
              this.response_json.metadata.directive.payload.response.speech.speechUtter).replace(
              /[&<>"'\/"]/g,
              s => this.entityMap[s]);
            this.response_json.metadata.directive.payload.response.speech.utter = String(
              this.response_json.metadata.directive.payload.response.speech.utter).replace(
              /[&<>"'\/"]/g,
              s => this.entityMap[s]);
          }
        }
      }
    }
    if (this.talks.length > 0 && this.spec === 'v3' && this.initFlag) {
      this.selectedIndex = 1;
      this.initFlag = false;
    }
    this.cdr.detectChanges();
  }

  getMetadataString(metatdata: any[]): string {
    let last = metatdata.length - 1;
    let meta = metatdata.reduce((t, data, i) => t.concat(`${data.attr}: ${data.value}${i < last ? '<br>' : ''}`), '');
    return meta.length > 0 ? meta : '{ none }';
  }

  setContent() {
    // getChatbotInfo를 하는 이유는 getChatbotAllList가 ChatbotDialogAgentInstanceInfo 리스트를 리턴하기때문
    this.grpc.getChatbotInfo(this.chatbot.name).subscribe(
      result => {
        this.chatbotInfo = result;
      }
    );
    this.grpc.getDialogAgentInstanceAllList().subscribe(
      result => {
        let dais = result.dai_list.filter(dai => dai.chatbot_name === this.chatbot.name);
        dais.sort((a, b) => a.name > b.name);
        this.rows = dais;
        this.selectedIndex = 0;
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  clickMetadata(index: number, who: string) {
    this.clickMeta = {index: index, who: who};
  }

  getImage: any = (row: any) => {
    if (row.lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  }
}
