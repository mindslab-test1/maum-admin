import 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';


const HEADERS = {headers: {'m2u-auth-internal': 'm2u-auth-internal'}};

@Injectable()
export class ChatbotTestService {
  apiUrl: string;
  wsUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.frontRestApiUrl;
    this.wsUrl = environment.wsUrl;
  }

  signIn(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/auth/signIn', param, HEADERS);
  }

  signOut(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/auth/signOut', param, HEADERS);
  }

  open(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/open', param, HEADERS);
  }

  close(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/close', param, HEADERS);
  }

  textToTextTalk(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/textToTextTalk', param, HEADERS);
  }

  eventToTextTalk(param: any): Observable<any> {
    return this.http.post(this.apiUrl + '/v3/dialog/eventToTextTalk', param, HEADERS);
  }

  getSpeechToSpeechTalkUrl(): string {
    return this.wsUrl + '/v3/ws/speechToSpeechTalk';
  }

  getSpeechToTextTalkUrl(): string {
    return this.wsUrl + '/v3/ws/speechToTextTalk';
  }
}
