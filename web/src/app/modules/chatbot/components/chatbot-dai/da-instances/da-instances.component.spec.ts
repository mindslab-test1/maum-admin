import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaInstancesComponent} from './da-instances.component';

describe('DaInstancesComponent', () => {
  let component: DaInstancesComponent;
  let fixture: ComponentFixture<DaInstancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaInstancesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
