import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSidenav, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';

import {DAI_DELETE, DAI_UPDATE_ALL, ROUTER_LOADED} from '../../../../../core/actions';
import {AuthService} from '../../../../../core/auth.service';
import {
  AppEnumsComponent,
  AppObjectManagerService,
  getErrorString,
  TableComponent
} from '../../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';
import {ConfirmComponent} from 'app/shared';


@Component({
  selector: 'app-dai-manage',
  templateUrl: './da-instances.component.html',
  styleUrls: ['./da-instances.component.scss'],
})

export class DaInstancesComponent implements OnInit, OnDestroy {
  page_title: any; // = 'Manage DA Instances';
  page_description: any; // = 'Add DA Instances and update information about DA Instances.';

  chatbotName: any;
  actions: any;
  toggle = false;


  // table
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('damsTemplate') damsTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  dataSource: MatTableDataSource<any>;
  header;
  rows: any[] = [];

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    let id = 'dai';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      }
    };

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {attr: 'name', name: 'Name', isSort: true, width: '30%'},
      {attr: 'skill_count', name: 'Skill Count', isSort: true, width: '10%'},
      {attr: 'lang', name: 'Lang', onlyImage: this.getImage, width: '10%'},
      {attr: 'da_name', name: 'Da', isSort: true, width: '20%'},
      {attr: 'dams', name: 'Dam', isSort: true, template: this.damsTemplate},
    ];

    // this.tableComponent.onRowClick = this.fillRowInfo;
    this.subscription.add(
      this.store.select('dais')
      .subscribe(dais => {
        if (dais) {
          dais.sort((a, b) => a.name > b.name);
          this.rows = dais;
          this.rows.forEach(row => row.isChecked = false);
          this.whenCheckChanged();
          /*
                      setTimeout(() => {
                        this.fillRowInfo(this.rows[0]);
                      }, this.rows.length > 0 ? 500 : 0);
          */
        }
      })
    );

    // #@  route.params로 chatbot 명을 획득합니다
    this.route.params.subscribe(par => {
      let chatbotName = par['id'];
      if (chatbotName === undefined || chatbotName === null || chatbotName.length < 1) {
        this.backPage();
        return;
      }
      this.chatbotName = chatbotName;

      this.grpc.getDialogAgentInstanceByChatbotName(this.chatbotName).subscribe(
        result => {
          result.dai_list.forEach(dai => {
            dai['skill_count'] = dai.skill_names.length;
          });
          let dais = result.dai_list;
          // dais.sort((a, b) => a.name > b.name);
          this.store.dispatch({type: DAI_UPDATE_ALL, payload: dais});
          this.store.dispatch({type: ROUTER_LOADED});
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.store.dispatch({type: ROUTER_LOADED});
        }
      );
    });
  }

  /** action callbacks **/
  add: any = () => {
    // let params = `role=add&chatbot=${encodeURI(this.chatbotName)}`;

    // #@ dai new로 이동합니다
    this.router.navigate(['./new'], {relativeTo: this.route});
  };

  edit: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }
    this.objectManager.set('dai', items[0]);
    // let params = `role=edit&chatbot=${encodeURI(this.chatbotName)}`;

    // #@ dai edit로 이동합니다
    this.router.navigate(['./edit'], {relativeTo: this.route});
  };

  delete: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${items.map(item => item.name)}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      let keyList = items.map(row => {
        return {name: row.dai_id};
      });

      this.grpc.deleteDialogAgentInstance(keyList).subscribe(
        res => {
          let deletedNames = [];
          let deletedItems = res.result_list.filter(entry => entry.result).map(entry => {
            deletedNames.push(items.find(item => item.dai_id === entry.name).name);
            return entry.name;
          });
          this.store.dispatch({type: DAI_DELETE, payload: deletedItems});
          this.snackBar.open(`DA Instance '${deletedNames}' Deleted.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Delete DA Instance. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  };

  /*
  fillRowInfo: any = (row: any) => {
    this.infoPanel.setContent(row);
    if (row === undefined) {
      this.sidenav.close();
      this.panelToggleText = 'Show Info Panel';
    } else {
      this.sidenav.open();
      this.panelToggleText = 'Hide Info Panel';
    }
  };
  */

  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.edit.disabled = true;
        this.actions.delete.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.edit.disabled = false;
        this.actions.delete.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.edit.disabled = true;
        this.actions.delete.disabled = false;
        break;
    }
  };

  getImage: any = (row: any) => {
    if (row.lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  };

  backPage: any = () => {
    this.router.navigate(['../chatbots'], {relativeTo: this.route});
  };

  getDamIcon(isActive) {
    if (isActive) {
      return 'cached';
    } else {
      return 'stop';
    }
  }

  getIconLang(row) {
    if (row.lang === 'kor') {
      return '<img src="/assets/img/korean_05.png">';
    } else if (row.lang === 'eng') {
      return '<img src="/assets/img/english_05.png">';
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
