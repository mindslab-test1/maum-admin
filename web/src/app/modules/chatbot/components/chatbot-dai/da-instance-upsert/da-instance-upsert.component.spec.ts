import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaInstanceUpsertComponent} from './da-instance-upsert.component';

describe('DaInstanceUpsertComponent', () => {
  let component: DaInstanceUpsertComponent;
  let fixture: ComponentFixture<DaInstanceUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaInstanceUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaInstanceUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
