import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSelectionList,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {GrpcApi} from '../../../../../core/sdk'
import {ROUTER_LOADED} from '../../../../../core/actions';
import {
  AppEnumsComponent,
  AppObjectManagerService,
  FormErrorStateMatcher,
  getErrorString
} from '../../../../../shared';
import {AlertComponent} from 'app/shared/components/dialog/alert.component';
import {ConfirmComponent} from 'app/shared';

const intType = '(^[0-9]*$)';

@Component({
  selector: 'app-dai-upsert',
  templateUrl: './da-instance-upsert.component.html',
  styleUrls: ['./da-instance-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class DaInstanceUpsertComponent implements OnInit {
  dialogTitle: string;
  role: string;
  chatbotName: string;
  chatbot: any;
  dai = {
    dai_id: undefined,
    chatbot_name: undefined,
    da_name: undefined,
    sc_name: undefined,
    skill_names: [],
    name: undefined,
    lang: undefined,
    description: undefined,
    params: {},
    dai_exec_info: []
  };
  daiName: any;
  org_dai: any;
  dams: any;

  name: any;
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  // typeFormControl = new FormControl('', [
  //   Validators.pattern(intType)
  // ]);

  skillList = [];

  das: any;
  selectedDa = undefined;
  runParams = [];

  chatbotSkillList = [];
  defaultDaiSkillList = [];
  allChecked = false;
  @ViewChild(MatSelectionList) skillSelectionList: MatSelectionList;

  constructor(private store: Store<any>,
              private route: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              public appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    // 1.objectManager에서 dai정보를 조회합니다
    this.dai = this.objectManager.get('dai');
    // url로 바로 접근한 경우 임시로 값을 넣어 줍니다
    if (this.dai === undefined) {
      this.setDefaultDaiInfo();
    } else {
      this.daiName = this.dai.name;
    }
    // /m2u/dialog-service/chatbots/dai/default-dai/edit 로 접근할수 있는 방법은 두가지 입니다
    // 1. chatbot detail에서 -> Manage DAI로 버튼을 클릭해서 이동한 경우
    // 2. 사용자가 직접 url에/m2u/dialog-service/dam/default-dam/edit를 입력하여 들어온 경우
    // 2 번의 경우 objectManager에 dam이 없습니다
    // 이 경우 dam을 획득하기 위한 예외처리는 아래와 같습니다
    let promise = new Promise((resolve, reject) => {
      this.route.params.subscribe(par => {
        let tempRoute = this.route.toString();
        this.chatbotName = par['id']; // url에 명시된 chatbot을 획득합니다
        if (tempRoute.indexOf('new') > 0) {
          // console.log('#@ dai/new :' + tempRoute);
          this.role = 'add';
          this.setDefaultDaiInfo(); // this.dai를 초기화 해줍니다(edit를 했다가 add를 하면 dai 정보가 남아 있음)
        } else if (tempRoute.indexOf('edit') > 0) {
          // console.log('#@ /edit :' + tempRoute);
          this.role = 'edit';
          this.daiName = par['id']; // url에 명시된 dai를 획득합니다
        } else {  // 잘못된 url 접근
          // console.log('#@ There is no dai url');
          let message = 'Failed, Cannot find this dai url.';
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.backPage();
          reject();
        }
      });
      if (this.role === 'edit' && this.dai.name === undefined) {
        // 사용자가 직접 버튼을 클릭하여 넘어왔을 경우
        // 사용자가 url로 edit를 접근했을 경우 grpc 호출을 통해 dam 정보를 받아온다
        this.getDialogAgentInstanceByChatbotName().then(() => resolve())
        .catch(() => reject());

      } else {
        resolve();  // resolve를 오출하여 promise를 완료한다
      }
    });
    promise.catch(() => {
      // 정상적으로 데이터를 갖고 오지 못한경우
      let message = `Cannot find [${this.daiName}] this dai`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
      this.backPage();
      return;
    });
    promise.then(() => {
      switch (this.role) {
        case 'add':
          this.dialogTitle = 'Add DA Instance to ' + this.chatbotName;
          break;
        case 'edit':
          let dai = this.objectManager.get('dai');
          if (dai === undefined) {
            this.store.dispatch({type: ROUTER_LOADED});
            this.backPage();
            return;
          }
          delete dai.skill_count;
          delete dai.checked;
          this.dai = dai;
          this.org_dai = JSON.parse(JSON.stringify(this.dai));
          this.dialogTitle = 'Edit DA Instance of ' + this.chatbotName;
          this.dai.skill_names.forEach(name => {
            this.defaultDaiSkillList.push(name);
          });
          this.store.dispatch({type: ROUTER_LOADED});
          break;
        default:
          this.store.dispatch({type: ROUTER_LOADED});
          this.backPage();
          // let message = 'Role is not properly assigned.';
          // this.snackBar.open(message, 'Confirm', {duration: 10000});
          break;
      }
      this.retrieveData();
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  retrieveData() {
    this.grpc.getDialogAgentManagerAllList().subscribe(
      result => {
        let dams = result.damwd_list;
        dams.forEach(dam => {
          dam.label = `${dam.dam_info.name}(${dam.dam_info.ip}:${dam.dam_info.port})`;
          dam.disabled = true;
          dam.checked = false;

          // dai edit 시 해당 dai에서 설정한 dam에 띄울 count 정보를 출력해주기 위한 처리.
          let info = (this.dai.dai_exec_info).filter(x => (x.dam_name === dam.dam_info.name));
          if (info.length > 0) { // dam name과  일치하는 정보가 존재할 경우
            dam.instances = info.length > 0 ? info['0'].cnt : undefined;
          } else { // dam name과  일치하는 정보가 존재하지 않을 경우
            dam.instances = undefined;
          }
        });
        dams.sort((a, b) => a.dam_info.name > b.dam_info.name);
        this.dams = dams;
        this.grpc.getDialogAgentAllList().subscribe(
          result2 => {
            result2.da_list.forEach(da => {
              da.da_info.viewValue = da.da_info.name + '(' + this.appEnum.runtimeEnvironment[da.da_info.runtime_env]
                + ':' + da.da_info.runtime_env_version + ')';
            });
            result2.da_list.sort((a, b) => {
              return a.da_info.name < b.da_info.name ? -1 : a.da_info.name > b.da_info.name ? 1 : 0;
            });

            this.das = result2.da_list;
            if (this.role === 'edit') {
              this.selectedDa = this.das.find(da => da.da_info.name === this.dai.da_name);
              this.onSelectedDaChange();
            }

            if (this.chatbotName) {
              this.grpc.getChatbotInfo(this.chatbotName).subscribe(
                chatbot => {
                  this.chatbot = chatbot;
                  // default skill 추가
                  this.chatbotSkillList.push(this.chatbot.default_skill);
                  if (this.chatbot.intent_finder_policy !== 'none') {
                    this.grpc.getIntentFinderPolicyInfo(this.chatbot.intent_finder_policy).subscribe(
                      itfpRes => {
                        itfpRes.steps.forEach(step => {
                          if (!step.is_hint) {
                            step.categories.forEach(category => {
                              if (this.chatbotSkillList.indexOf(category) === -1) {
                                this.chatbotSkillList.push(category);
                              }
                            })
                          }
                        });
                        this.chatbotSkillList.sort();

                        // 전체 선택된 경우일 경우
                        if (this.chatbotSkillList.length === this.defaultDaiSkillList.length) {
                          this.allChecked = true;
                        }
                      })
                  }
                },
                err => {
                  let message = `Something went wrong. [${getErrorString(err)}]`;
                  this.snackBar.open(message, 'Confirm', {duration: 10000});
                  this.store.dispatch({type: ROUTER_LOADED});
                }
              );
            } else {
              let message = `Something went wrong. Chatbot undefined`;
              this.snackBar.open(message, 'Confirm', {duration: 10000});
              this.store.dispatch({type: ROUTER_LOADED});
            }
          }, err => {
            let message = `Something went wrong. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            this.store.dispatch({type: ROUTER_LOADED});
          });
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });
  }

  onSelectedLangChange() {
    if (this.chatbot.intent_finder_policy !== 'none') {
      this.dai.skill_names = [];
      this.skillSelectionList.deselectAll();
      this.skillList = [];
    }
  }

  onSelectedDaChange() {
    this.dai.da_name = this.selectedDa.da_info.name;
    this.grpc.getRuntimeParameters({da_name: this.dai.da_name}).subscribe(
      paramList => {
        this.runParams = paramList.params.filter(list => (list.name !== '' && list.default_value !== '')
          && (list.name !== undefined && list.default_value !== undefined)
          && (list.name !== null && list.default_value !== null));
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      }
    );

    this.dams.forEach(dam => {
      let daai = this.selectedDa.daai_list.find(item => item.dam_name === dam.dam_info.name);
      if (daai) {
        dam.disabled = false;
        dam.checked = daai.active;
        if (dam.instances === undefined && dam.checked) {
          dam.instances = 1;
        }
      } else {
        dam.disabled = true;
        dam.checked = false;
        dam.instances = undefined;
      }
    });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    let resultName = [];
    this.runParams.forEach(param => {
      if (param.required) {
        if (param.default_value === 'undefined' || param.default_value === '') {
          resultName.push(param.name);
          // return false;
        }
      }
    });
    if (resultName.length > 0) {
      this.snackBar.open(`Please input '${resultName}' field(s).`, 'confirm', {duration: 2000});
    } else if (this.isFormError(this.nameFormControl) || this.dai.name === undefined) {
      this.snackBar.open('Please input the name.', 'confirm', {duration: 2000});
    } else if (this.dai.lang === undefined) {
      this.snackBar.open('Please select the language.', 'confirm', {duration: 2000});
    } else if (this.skillSelectionList.selectedOptions.selected.length === 0) {
      this.snackBar.open('Please select the skill.', 'confirm', {duration: 2000});
    } else if (this.dai.da_name === undefined) {
      this.snackBar.open('Please select the da.', 'confirm', {duration: 2000});
      // } else if (this.isFormError(this.typeFormControl)) {
      //   this.snackBar.open('Please input type integer.', 'confirm', {duration: 2000});
    } else {
      const ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = `Add '${this.dai.name}'?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          if (this.role === 'add') {
            this.onAdd();
          } else {
            this.onEdit();
          }
        }
      });
    }
  }

  aggregateDialogAgentInfo() {
    this.dai.chatbot_name = this.chatbot.name;
    this.dai.dai_exec_info = [];
    this.dai.skill_names = []; // 초기화
    this.skillSelectionList.selectedOptions.selected.forEach(selected => {
      this.dai.skill_names.push(selected.value);
    });
    this.dams.forEach(dam => {
      if (!dam.disabled) {
        this.dai.dai_exec_info.push({
          dai_id: this.role === 'edit' ? this.dai.dai_id : undefined,
          dam_name: dam.dam_info.name,
          active: dam.checked,
          cnt: dam.checked ? dam.instances : 0
        });
      }
    });
    this.dai.params = {};
    this.runParams.forEach(param => this.dai.params[param.name] = param.default_value);
  }

  onAdd(): void {
    delete this.dai['isChecked'];
    this.aggregateDialogAgentInfo();
    this.grpc.insertDialogAgentInstanceInfo(this.dai).subscribe(
      result => {
        this.snackBar.open(`DAI '${this.dai.name}' created.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to create a DAI. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    );
  }

  onEdit(): void {
    delete this.dai['isChecked'];
    this.aggregateDialogAgentInfo();
    this.grpc.updateDialogAgentInstanceInfo(this.dai).subscribe(
      result => {
        this.snackBar.open(`DAI '${this.dai.name}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let message = `Failed to update a DAI. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    );
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  backPage: any = () => {
    this.objectManager.clean('cb');
    let param = `chatbot=${encodeURI(this.chatbotName)}`;

    // #@ chatbots dai 페이지로 이동합니다
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  checkAll() {
    if (this.allChecked) {
      this.skillSelectionList.selectAll();
    } else {
      this.skillSelectionList.deselectAll()
    }
  };

  /**
   * url로 바로 접근한 경우 임시로 값을 넣어 줍니다
   * 해당 함수를 호출하지 않으면 html에서 [(ngModel)]="dai.lang" 접근시 오류가 발생합니다
   * Dai의 name이 undefined 이므로 이후 grpc를 통해 dai info를 받아 옵니다
   */
  setDefaultDaiInfo() {
    let tempDai = {
      dai_id: undefined,
      chatbot_name: undefined,
      da_name: undefined,
      sc_name: undefined,
      skill_names: [],
      name: undefined,
      lang: '',
      description: undefined,
      params: {},
      dai_exec_info: []
    };
    this.dai = tempDai;
  }

  getDialogAgentInstanceByChatbotName() {
    return new Promise((resolve, reject) => {
      this.grpc.getDialogAgentInstanceByChatbotName(this.chatbotName).subscribe(
        result => {
          if (result && result.dai_list) {
            result.dai_list.forEach(dai => {
              dai['skill_count'] = dai.skill_names.length;
            });
            let res: any;
            res = result.dai_list.filter(elem => elem.name === this.daiName);
            if (res) {
              this.objectManager.set('dai', res[0]);
              resolve();
            } else {
              reject();
            }
          } else {
            reject();
          }
        }, err => {
          reject();
        });
    }).catch(() => {
      this.backPage();
      return;
    });
  }
}
