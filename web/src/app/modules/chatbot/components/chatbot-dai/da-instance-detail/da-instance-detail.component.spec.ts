import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaInstanceDetailComponent} from './da-instance-detail.component';

describe('DaInstanceDetailComponent', () => {
  let component: DaInstanceDetailComponent;
  let fixture: ComponentFixture<DaInstanceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaInstanceDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaInstanceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
