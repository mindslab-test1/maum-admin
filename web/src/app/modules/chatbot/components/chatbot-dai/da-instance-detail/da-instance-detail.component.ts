import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';

import {Subscription} from 'rxjs';
import {ROUTER_LOADED} from '../../../../../core/actions';
import {AuthService} from '../../../../../core/auth.service';
import {AppObjectManagerService, TableComponent, getErrorString} from '../../../../../shared';
import {ConsoleUserApi} from '../../../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-dai-detail',
  templateUrl: './da-instance-detail.component.html',
  styleUrls: ['./da-instance-detail.component.scss'],
})

export class DaInstanceDetailComponent implements OnInit {
  dialogTitle: string;
  actions: any;
  // runtimeTable: any;
  row: any;
  header;
  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  subscription = new Subscription();

  @ViewChild('runtimeTableComponent') runtimeTableComponent: TableComponent;

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    this.dialogTitle = 'DA Instance: Weather_LB1';
    let id = 'dai';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'DELETE',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      // disable: {
      //   icon: 'disable',
      //   text: 'Disable',
      //   callback: this.disable,
      //   hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      // },
      stop: {
        icon: 'stop',
        text: 'Stop',
        callback: this.stop,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      },
      restart: {
        icon: 'play_arrow',
        text: 'Restart',
        callback: this.restart,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      }
    };
    this.header = [
      {attr: 'name', name: 'Name', sortable: true},
      {attr: 'type', name: 'Type', sortable: true},
      {attr: 'defaultValue', name: 'Default Value', sortable: true},
    ];
    this.rows = ['1', '2', '3', '4'];

    // // subscribe store
    // this.subscription.add(
    //   this.store.select('das')
    //     .subscribe(das => {
    //       if (das && das.length > 0) {
    //         this.runningTable.rows = das;
    //         setTimeout(() => {
    //         }, 500);
    //       }
    //     })
    // );
    //
    // //rest.함수명(바꿀예정).subscribe( 더미데이터 넣어서 작업 예정 )
    // this.rest.getAgents().subscribe(
    //   das => {
    //     das = [
    //       {
    //         name: 'stock',
    //         lang: 'Python',
    //         type: 'SDS',
    //         privacy: 'required',
    //         running: [
    //           {name: 'DAM1', active: true},
    //           {name: 'DAM2', active: false},
    //           {name: 'DAM3', active: true},
    //           {name: 'DAM4', active: true}
    //         ]
    //       },
    //       {
    //         name: 'weather',
    //         lang: 'Java',
    //         type: 'SDS',
    //         privacy: 'required',
    //         running: [
    //           {name: 'DAM1', active: false},
    //           {name: 'DAM2', active: false},
    //           {name: 'DAM3', active: false},
    //           {name: 'DAM4', active: true}
    //         ]
    //       },
    //       {
    //         name: 'stock2',
    //         lang: 'C',
    //         type: 'Test',
    //         privacy: 'required',
    //         running: [
    //           {name: 'DAM1', active: true},
    //           {name: 'DAM2', active: false},
    //           {name: 'DAM3', active: true},
    //           {name: 'DAM4', active: true}
    //         ]
    //       }];
    //     this.store.dispatch({type: DA_UPDATE_ALL, payload: das});
    //     this.store.dispatch({type: ROUTER_LOADED});
    //   }, err => {
    // let message = `Something went wrong. [${getErrorString(err)}]`;
    // this.snackBar.open(message, 'Confirm', {duration: 10000});
    //     this.store.dispatch({type: ROUTER_LOADED});
    //   });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  edit: any = () => {

  };

  delete: any = () => {
    // const ref = this.dialog.open(ConfirmComponent);
    //
    // ref.componentInstance.message = `Delete '${this.id}?`;
    // ref.afterClosed()
    //   .subscribe(confirmed => {
    //     if (!confirmed) {
    //       return;
    //     }
    //
    //     const item = {
    //       ip: this.ip,
    //       port: this.port
    //     };
    //
    //     this.rest.deleteDialogAgentManager(item).then((res) => {
    //       const body: string = res._body;
    //       if (body != null && body !== undefined && body.toLowerCase().indexOf('delete ok') !== -1) {
    //         this.store.dispatch({type: DAM_DELETE, payload: this.id});
    //         this.snackBar.open(`Dialog Agent Manager '${this.id}' deleted.`, 'Confirm', {duration: 3000});
    //         this.router.navigate(['../dam'], {relativeTo: this.route});
    //       } else {
    //         this.snackBar.open(`Failed to delete Dialog Agent Manager '${this.id}'`, 'Confirm', {duration: 10000});
    //       }
    //     });
    //   });
  };

  stop: any = () => {

  };

  restart: any = () => {

  };

  backPage() {
    this.objectManager.clean('dam');
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
