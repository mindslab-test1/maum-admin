import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';

import {CB_DELETE, CB_UPDATE_ALL, ROUTER_LOADED} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {AppObjectManagerService, getErrorString, TableComponent} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';
import {ConfirmComponent} from 'app/shared';


@Component({
  selector: 'app-chatbots',
  templateUrl: 'chatbots.component.html',
  styleUrls: ['./chatbots.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ChatbotsComponent implements OnInit, OnDestroy {
  page_title: any; // = 'Manage Chatbots';
  page_description: any; // = 'Add Chatbots and update information about Chatbots.';
  title: any = 'Chatbots';

  toggle = false;

  // toolbar
  actions: any;

  // 데이터
  dai: any;

  // table
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;

  workspace_id: any;

  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('daInstancesTemplate') daInstancesTemplate: TemplateRef<any>;
  @ViewChild('sessionCntTemplate') sessionCntTemplate: TemplateRef<any>;
  @ViewChild('intentFinderPolicyTemplate') intentFinderPolicyTemplate: TemplateRef<any>;
  // @ViewChild('sidenav') sidenav: MatSidenav;
  // @ViewChild('infoPanel') infoPanel: ChatbotsPanelComponent;
  dataSource: MatTableDataSource<any>;
  header;
  rows: any[] = [];

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    let id = 'chbt';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      test: {
        icon: '',
        text: 'Test',
        callback: this.test,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      }
    };

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {
        attr: 'name',
        name: 'Name',
        isSort: true,
        onClick: this.navigateChatbotDetailView,
        width: '17%'
      },
      {attr: 'title', name: 'Title', isSort: true},
      {
        attr: 'daInstances.length',
        name: '#DA Instances',
        width: '10%',
        template: this.daInstancesTemplate
      },
      {
        attr: 'session_cnt',
        name: 'Sessions',
        isSort: true,
        width: '10%',
        template: this.sessionCntTemplate
      },
      {
        attr: 'intent_finder_policy',
        name: 'Intent Finder Policy',
        isSort: true,
        template: this.intentFinderPolicyTemplate,
        width: '17%'
      },
      {attr: 'workspace_id', name: 'Workspace Id', isSort: true},
    ];
    // this.tableComponent.onRowClick = this.fillRowInfo;

    this.subscription.add(
      this.store.select('cbs')
      .subscribe(cbs => {
        if (cbs) {
          cbs.sort((a, b) => a.name > b.name);
          this.rows = cbs;
          this.whenCheckChanged();
          setTimeout(() => {
            // this.fillRowInfo(this.rows[0]);
          }, this.rows.length > 0 ? 500 : 0);
        }
      })
    );

    // 모든 chatbot list 조회(workspaces 무시)
    this.grpc.getChatbotAllList().subscribe(
      result => {
        result.chatbot_list.forEach(chatbot => {
          chatbot.daInstances = [];
          if (chatbot.korean_cnt > 0) {
            chatbot.daInstances.push({lang: 'kor', count: chatbot.korean_cnt});
          }
          if (chatbot.english_cnt > 0) {
            chatbot.daInstances.push({lang: 'eng', count: chatbot.english_cnt});
          }
        });
        this.store.dispatch({type: CB_UPDATE_ALL, payload: result.chatbot_list});
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });

    this.workspace_id = parseInt(localStorage.getItem('m2uWorkspaceId'), 10);

    // 모든 chatbot list 조회(workspace_id base 조회)
    /*this.grpc.getMyChatbotAllList(this.workspace_id).subscribe(
      result => {
        result.chatbot_list.forEach(chatbot => {
          chatbot.daInstances = [];
          if (chatbot.korean_cnt > 0) {
            chatbot.daInstances.push({lang: 'kor', count: chatbot.korean_cnt});
          }
          if (chatbot.english_cnt > 0) {
            chatbot.daInstances.push({lang: 'eng', count: chatbot.english_cnt});
          }
        });

        console.log('#@ result.chatbot_list :', result.chatbot_list);

        this.store.dispatch({type: CB_UPDATE_ALL, payload: result.chatbot_list});
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });*/

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    // this.router.navigateByUrl('m2u/dialog-service/chatbots-upsert#add');

    // new로 이동합니다
    this.router.navigate(['./new'], {relativeTo: this.route});
  };

  delete: any = () => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.name);
    if (names.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      let keyList = names.map(key => {
        return {name: key};
      });

      this.grpc.deleteChatbot(keyList).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.store.dispatch({type: CB_DELETE, payload: deletedItems});
          this.snackBar.open(`Chatbot '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Delete Chatbot. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  edit: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }
    this.objectManager.set('cb', items[0]);
    // edit로 이동합니다
    this.router.navigate(['./', items[0].name, 'edit'], {relativeTo: this.route});
  };

  test: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Test.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }

    // let params = 'chatbot=' + encodeURI(items[0].name);
    // this.router.navigateByUrl('m2u/dialog-service/testchat?' + params);

    // testchat detail로 이동합니다
    this.router.navigate(['./', items[0].name, 'test'], {relativeTo: this.route});
  };

  // togglePanel: any = () => {
  //   this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
  //   this.sidenav.toggle();
  // };

  // fillRowInfo: any = (row: any) => {
  //   this.infoPanel.setContent(row);
  //
  //   if (row === undefined) {
  //     this.sidenav.close();
  //     this.panelToggleText = 'Show Info Panel';
  //   } else {
  //     this.sidenav.open();
  //     this.panelToggleText = 'Hide Info Panel';
  //   }
  // };

  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        this.actions.test.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        this.actions.test.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        this.actions.test.disabled = true;
        break;
    }
  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  navigateChatbotDetailView: any = (row: any) => {
    this.objectManager.set('cb', row);
    // let params = 'chatbot=' + encodeURI(row.name);
    // this.router.navigateByUrl('m2u/dialog-service/chatbots-detail?' + params);

    this.router.navigate(['./', row.name], {relativeTo: this.route});
  };

  navigateIntentFinderPolicyView: any = (row: any) => {
    let tempItfp = '';
    if (row['intent_finder_policy'] !== '-') {
      let promise = new Promise((resolve, reject) => {
        this.grpc.getIntentFinderPolicyInfo(row.intent_finder_policy).subscribe(res => {
          if (res.name === undefined || res.lang === undefined) {
            reject();
          } else {
            tempItfp = res.name;
            this.objectManager.set('itfp', res);
            resolve();
          }
        });
      });
      promise.then(() => {
        // this.router.navigateByUrl('m2u/dialog-service/itfp-detail');
        // itfp detail로 이동합니다
        this.router.navigate(['../../intent-finder', tempItfp], {relativeTo: this.route});
      });
      promise.catch(() => {
        this.snackBar.open('The intent finder policy is empty.', 'Confirm', {duration: 3000});
      });
    }
  };

  getImage: any = (row: any) => {
    if (row.lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  };

  getImageUrl: any = (row: any) => {
    if (row.lang === 'ko_KR') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  };

  getNavigateClassName(row, attr) {
    if (attr === 'intent_finder_policy') {
      return (row.intent_finder_policy !== 'none' ? 'idCursor' : 'none');
    } else if (attr === 'intent_finder_policy') {
      return row.intent_finder_policy !== '-' ? 'idCursor' : 'none';
    }
  }
}
