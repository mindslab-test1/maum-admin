import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {Store} from '@ngrx/store';

import {AuthService} from '../../../../core/auth.service';
import {ConsoleUserApi} from '../../../../core/sdk';
import {DAI_UPDATE_ALL, ROUTER_LOADED} from '../../../../core/actions';
import {AppObjectManagerService, getErrorString, TableComponent} from '../../../../shared';
import {GrpcApi} from '../../../../core/sdk/services/custom/Grpc';
import {AppEnumsComponent} from 'app/shared';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-chatbot-panel',
  templateUrl: './chatbot-panel.component.html',
  styleUrls: ['./chatbot-panel.component.scss']
})
export class ChatbotPanelComponent implements OnInit {
  cbsInfo: any;
  table: any;
  actions: any;
  role: string;
  row: any;
  intentFinderPolicyInfo: any;
  @ViewChild('tableComponent') chatbotsPanelTableComponent: TableComponent;
  @ViewChild('damsTemplate') damsTemplate: TemplateRef<any>;
  dataSource: MatTableDataSource<any>;
  header;
  rows: any[] = [];

  constructor(private store: Store<any>,
              private router: Router,
              public appEnum: AppEnumsComponent,
              private snackBar: MatSnackBar,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    let id = 'chbt';
    let roles = this.consoleUserApi.getCachedCurrent().roles;
    this.actions = [
      {
        icon: '',
        text: 'Add DAI',
        callback: this.addDaInstance,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      {
        icon: '',
        text: 'Manage DAIs',
        callback: this.manageDaInstances,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.READ, roles)
      }
    ];

    this.header = [
      {attr: 'name', name: 'Name', isSort: true},
      {attr: 'lang', name: 'Lang', onlyImage: this.getImage},
      {attr: 'da_name', name: 'Da', isSort: true},
    ];
  }

  setContent(cbsInfo: any) {
    if (cbsInfo !== undefined) {
      this.grpc.getDialogAgentInstanceByChatbotName(cbsInfo.name).subscribe(
        result => {
          let dais = result.dai_list;
          dais.sort((a, b) => a.name > b.name);
          this.rows = dais;
          this.cbsInfo = cbsInfo;
          this.store.dispatch({type: DAI_UPDATE_ALL, payload: dais});
          this.store.dispatch({type: ROUTER_LOADED});
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.store.dispatch({type: ROUTER_LOADED});
        }
      );
      if (cbsInfo.intent_finder_policy === 'none') {
        this.intentFinderPolicyInfo = {
          name: 'none',
        };
      } else {
        this.grpc.getIntentFinderPolicyInfo(cbsInfo.intent_finder_policy).subscribe(res => {
            this.intentFinderPolicyInfo = res;
            this.setStepsLength(this.intentFinderPolicyInfo);
          },
          err => {
            let message = `Something went wrong. [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
            this.store.dispatch({type: ROUTER_LOADED});
          });
      }
    }
  }

  /**
   * Side Panel Open 전에, Steps의  ClaasifiredMethod 종류별로 Length를 세팅 해준다.
   * @param row {테이블에서 클릭한 IntentFinderPolicy json 객체}
   */
  setStepsLength(row: any) {
    // 세팅 시 새로운 변수 할당 및 초기화
    row['customLength'] = 0;
    row['pcreLength'] = 0;
    row['hmdLength'] = 0;
    row['dnnLength'] = 0;
    row['finalLength'] = 0;

    // ClassifiredMethod 종류별로 count를 + 해준다.
    row.steps.forEach(step => {
      switch (step.test_rule) {
        case 'custom_script_mod':
          row['customLength'] += 1;
          break;
        case 'pcre_mod':
          row['pcreLength'] += 1;
          break;
        case 'hmd_mod':
          row['hmdLength'] += 1;
          break;
        case 'dnn_cl_mod':
          row['dnnLength'] += 1;
          break;
        case 'final_rule':
          row['finalLength'] = 1;
          break;
      }
    });
  }

  getImage: any = (row: any) => {
    if (row) {
      if (row.lang === 'kor' || row.lang === 'ko_KR') {
        return '/assets/img/korean_05.png';
      } else if (row.lang === 'eng' || row.lang === 'en_US') {
        return '/assets/img/english_05.png';
      }
    }
  };

  addDaInstance: any = () => {
    // let params = `role=add&chatbot=${encodeURI(this.cbsInfo.name)}`;
    // this.router.navigateByUrl('m2u/dialog-service/chatbots-dainstance-upsert?' + params);
    console.log('#@ addDaInstance this.cbsInfo.name :', this.cbsInfo.name);
    // chatbots dai new로 이동합니다
    this.router.navigateByUrl('m2u/dialog-service/chatbots/' + this.cbsInfo.name + '/dai/new');
  };

  manageDaInstances: any = () => {
    // let params = `chatbot=${encodeURI(this.cbsInfo.name)}`;
    // this.router.navigateByUrl('m2u/dialog-service/chatbots-dainstances-manage?' + params);

    // dai manage로 이동합니다
    this.router.navigateByUrl('m2u/dialog-service/chatbots/' + this.cbsInfo.name + '/dai');
  };

  navigateDetailDomain: any = (row: any) => {
    this.row = row;
    this.router.navigateByUrl('m2u/dialog-service/chatbots-dainstance-detail');
  }
}
