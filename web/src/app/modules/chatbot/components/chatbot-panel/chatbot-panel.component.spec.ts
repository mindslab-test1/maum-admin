import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatbotPanelComponent} from './chatbots-panel.component';

describe('ChatbotsPanelComponent', () => {
  let component: ChatbotPanelComponent;
  let fixture: ComponentFixture<ChatbotPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChatbotPanelComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
