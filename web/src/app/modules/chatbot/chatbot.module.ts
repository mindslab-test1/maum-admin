import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatbotsComponent} from './components/chatbots/chatbots.component';
import {SharedModule} from '../../shared/shared.module';
import {ChatbotUpsertComponent} from './components/chatbot-upsert/chatbot-upsert.component';
import {ChatbotPanelComponent} from './components/chatbot-panel/chatbot-panel.component';
import {ChatbotDetailComponent} from './components/chatbot-detail/chatbot-detail.component';
import {DaInstancesComponent} from './components/chatbot-dai/da-instances/da-instances.component';
import {DaInstanceDetailComponent} from './components/chatbot-dai/da-instance-detail/da-instance-detail.component';
import {DaInstanceUpsertComponent} from './components/chatbot-dai/da-instance-upsert/da-instance-upsert.component';
import {ChatbotTestComponent} from './components/chatbot-test/chatbot-test.component';
import {ChatMainComponent} from './components/chatbot-test/chat-main/chat-main.component';
import {TalkPanelComponent} from './components/chatbot-test/talk-panel/talk-panel.component';
import {RouterModule} from '@angular/router';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {ChatbotRoutingModule} from './chatbot-routing.module';

@NgModule({
  declarations: [
    ChatbotsComponent,
    ChatbotUpsertComponent,
    ChatbotPanelComponent,
    ChatbotDetailComponent,
    DaInstancesComponent,
    DaInstanceDetailComponent,
    DaInstanceUpsertComponent,
    ChatbotTestComponent,
    ChatMainComponent,
    TalkPanelComponent
  ],
  imports: [
    CommonModule,
    ChatbotRoutingModule,
    SharedModule,
    RouterModule,
    MatSidenavModule,
    MatSelectModule,
    MatInputModule,
    MatListModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatRadioModule,
    MatChipsModule,
    MatIconModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule
  ],
})
export class ChatbotModule {
}
