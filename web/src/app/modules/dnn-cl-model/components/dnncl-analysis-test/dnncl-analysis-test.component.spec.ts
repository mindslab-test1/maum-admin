import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DnnclAnalysisTestComponent} from './dnncl-analysis-test.component';

describe('DnnclAnalysisTestComponent', () => {
  let component: DnnclAnalysisTestComponent;
  let fixture: ComponentFixture<DnnclAnalysisTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnnclAnalysisTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnnclAnalysisTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
