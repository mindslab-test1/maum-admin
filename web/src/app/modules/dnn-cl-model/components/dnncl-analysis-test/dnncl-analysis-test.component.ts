import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {
  ErrorStateMatcher,
  MatDialog,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {Subject} from 'rxjs';
import {AlertComponent, FormErrorStateMatcher, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {DnnTestEntity} from '../../entity/dnn-test/dnn-test.entity';
import {DnnTestService} from '../../services/dnn-test/dnn-test.service';

@Component({
  selector: 'app-ta-dnn-analysis-test',
  templateUrl: './dnncl-analysis-test.component.html',
  styleUrls: ['./dnncl-analysis-test.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None
})
export class DnnclAnalysisTestComponent implements OnInit, OnDestroy {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  private _onDestroy = new Subject<void>();

  selectedModel: DnnTestEntity = null;

  textData: string;
  workspaceId: string;
  error: string;
  loading = false;
  textFormControl = new FormControl('', [
    Validators.required,
  ]);
  submitButton = true;


  // table
  header = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence'},
    {attr: 'category', name: 'Category', isSort: true},
    {attr: 'probability', name: 'Probability', isSort: true},
  ];
  rows: DnnTestEntity[] = [];
  dataSource: MatTableDataSource<any>;
  param: DnnTestEntity;

  constructor(private storage: StorageBrowser,
              public matcher: FormErrorStateMatcher,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private dnnTestService: DnnTestService,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.selectedModel = new DnnTestEntity();
    this.selectedModel.name = '';
    this.route.params.subscribe(par => {
      let name = par['id'];
      if (!name || name === '') {
        this.back('Error. Check your model name in a url.');
      }
      this.selectedModel.workspaceId = this.workspaceId;
      this.dnnTestService.getModel(name, this.selectedModel.workspaceId).subscribe(res => {
        if (!res) {
          this.back('Error. Check your model name in a url.');
        } else {
          this.selectedModel.name = res.name;
        }
      });
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.selectedModel.name === '') {
      this.router.navigate(['..', '..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }

  isSubmit() {
    if (this.textData !== undefined && this.textData.length > 0) {
      this.submitButton = false;
    } else {
      this.submitButton = true;
    }
  }

  submit = () => {
    this.param = new DnnTestEntity();
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');
    this.param.name = this.selectedModel.name;
    this.param.textData = this.textData;

    if (!this.submitButton) {
      this.loading = true;
      this.dnnTestService.taDnnAnalyzeText(this.param).subscribe(
        res => {
          if (res) {
            this.rows = res;
            this.loading = false;
          }
        },
        err => {
          this.openAlertDialog('Failed', 'Server error. test failed.', 'error');
          this.loading = false;
        }
      );
    }
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
