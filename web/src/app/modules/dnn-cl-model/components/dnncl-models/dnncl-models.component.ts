import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ModelInsertService} from '../../services/model-insert/model-insert.service';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';
import {AlertComponent, ConfirmComponent, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {AppObjectManagerService} from '../../../../shared';

@Component({
  selector: 'app-ta-model-management',
  templateUrl: './dnncl-models.component.html',
  styleUrls: ['./dnncl-models.component.scss'],
  providers: [ModelInsertService],
  encapsulation: ViewEncapsulation.None,
})
export class DnnclModelsComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  actions: any;

  workspaceId: string;
  user: string;
  data: any[] = [];
  length: number;
  pageParam: MatPaginator;
  dataSource: MatTableDataSource<any>;
  deleteModel: any = [];
  header;

  sort: MatSort;

  constructor(private dialog: MatDialog,
              private modelService: ModelInsertService,
              private router: Router,
              private route: ActivatedRoute,
              private objectManager: AppObjectManagerService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private cdr: ChangeDetectorRef,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.header = [
      {attr: 'checkbox', name: '', checkbox: true, width: '5%'},
      {attr: 'no', name: 'No.', no: true, width: '5%'},
      {attr: 'name', name: 'Name', width: '15%', isSort: true, onClick: this.nameClick},
      {attr: 'manDesc', name: 'Description', isSort: true, width: '15%',},
      {attr: 'cnt', name: 'Changed Count', width: '10%', isSort: true, onClick: this.countClick},
      {attr: 'action', name: 'Action', width: '7%', isButton: true, buttonName: 'Edit'},
      {attr: 'dnnDicId', name: 'dnnDicId', hidden: true},
    ];

    this.actions = [
      {type: 'add', text: 'Add', icon: 'add_circle_outline', callback: this.createModel},
      {type: 'delete', text: 'Delete', icon: 'delete_circle_outline', callback: this.deleteModels}
    ];
    this.user = this.storage.get('user').id;
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.objectManager.clean('dnnData');
  }

  ngAfterViewInit() {
    this.getList();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  setSort(matSort?: MatSort) {
    this.sort = matSort;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  getpaginator(page?: MatPaginator) {
    this.pageParam = page;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  getList(pageIndex?) {
    let model: ModelManagementEntity = new ModelManagementEntity();
    this.cdr.detectChanges();
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    model.pageSize = this.pageParam.pageSize;
    model.pageIndex = this.pageParam.pageIndex;
    model.orderDirection = this.sort.direction === '' ||
    this.sort.direction === undefined ? 'desc' : this.sort.direction;
    model.orderProperty = this.sort.active === '' ||
    this.sort.active === undefined ? 'createdAt' : this.sort.active;
    model.workspaceId = this.storage.get('m2uWorkspaceId');
    model.dnnUse = true;

    this.modelService.getHmdOrDnnWithCommitHistory(model).subscribe(res => {
      if (res) {
        let result: any[] = res.map(x => {
          x.entity.cnt = x.cnt.toString();
          return x.entity;
        });
        this.data = result;
        this.length = result.length;
      } else {
        this.length = 0;
      }
      this.tableComponent.isCheckedAll = false;
    });
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  createModel = () => {
    this.router.navigate(['new'], {relativeTo: this.route});
  };

  editModel(data) {
    this.objectManager.set('dnnData', data);
    this.router.navigate([data.name, 'edit'], {relativeTo: this.route});
  }

  nameClick: any = (data: any) => {
    this.objectManager.set('dnnData', data);
    this.router.navigate([data.name], {relativeTo: this.route});
  };

  countClick: any = (data: any) => {
    this.router.navigate(['../../histories/dnn', data.name, data.dnnDicId], {relativeTo: this.route.parent});
  };

  deleteModels = () => {
    this.deleteModel.length = 0;
    this.data.forEach(items => {
      if (items.isChecked) {
        this.deleteModel.push({
          id: items.id,
          hmdDicId: items.hmdDicId,
          dnnDicId: items.dnnDicId
        });
      }
    });

    if (this.deleteModel.length === 0) {
      this.openAlertDialog('notice', 'Model has to be selected', 'notice');
      return null;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove models?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (this.deleteModel.length === this.tableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.modelService.deleteSelectedModels(this.deleteModel).subscribe(res => {
          if (res) {
            this.openAlertDialog('Success', 'Success Remove Model', 'success');
            this.getList();
          }
        }, () => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. remove models failed.', 'error');
        });
      }
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
