import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DnnclModelsComponent} from './dnncl-models.component';

describe('DnnclModelsComponent', () => {
  let component: DnnclModelsComponent;
  let fixture: ComponentFixture<DnnclModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnnclModelsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnnclModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
