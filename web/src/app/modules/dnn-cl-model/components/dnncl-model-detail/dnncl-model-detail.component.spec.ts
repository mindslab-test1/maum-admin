import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DnnclModelDetailComponent} from './dnncl-model-detail.component';

describe('DnnclModelDetailComponent', () => {
  let component: DnnclModelDetailComponent;
  let fixture: ComponentFixture<DnnclModelDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnnclModelDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnnclModelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
