import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {
  AlertComponent,
  AppObjectManagerService,
  CommitDialogComponent,
  ConfirmComponent,
  TableComponent, UploaderButtonComponent
} from 'app/shared';
import {TreeViewComponent} from 'app/shared/components/tree/tree.component';
import {DictionaryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-upsert-dialog.component';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {DictionaryCategoryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-category-upsert-dialog.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {DnnTrainingDataEntity} from '../../entity/dnn-training-data/dnn-training-data.entity';
import {DnnTrainingDataService} from '../../services/dnn-training-data/dnn-training-data.service';
import {DnnTrainingService} from '../../services/dnn-training/dnn-training.service';
import {ModelInsertService} from '../../services/model-insert/model-insert.service';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';


@Component({
  selector: 'app-dnn-model',
  templateUrl: './dnncl-model-detail.component.html',
  styleUrls: ['./dnncl-model-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DnnclModelDetailComponent implements OnInit, OnDestroy {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('appTree') appTree: TreeViewComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  @Output() selectFiles: EventEmitter<string[]> = new EventEmitter<string[]>();
  selectedDnnDic = new DnnTrainingDataEntity();
  dnnData: any;
  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();

  pageParam: MatPaginator;
  pageLength: number;

  currentCategory: any = null;
  actions: any[] = [];
  model_actions: any = {};
  editable = false;
  isReadOnly: any = 'readOnly';
  isNew: any = 'readOnly';
  inputData = null;
  searchSentence: string;

  dnnDicLineEntity = new DnnTrainingDataEntity;
  categories: any = null;

  header = [];
  isCreated = false;

  defaultHeader = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', input: false, width: '35%'},
    {attr: 'category', name: 'Category', width: '35%'}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', input: true, width: '35%'},
    {attr: 'category', name: 'Category', width: '35%'}
  ];
  dnnDicLineList: any[] = [];
  dataSource: MatTableDataSource<any>;

  workspaceId = '';
  user = '';

  infoGroup1: any;
  infoGroup2: any;

  categoryCallback = {
    add: (next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.dnnTrainingDataService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        workspaceId: this.workspaceId,
        dnnDicId: this.selectedDnnDic.dnnDicId,
        isNew: !this.isCreated,
        creatorId: this.user,
        updaterId: this.user
      };
    },
    edit: (node: any, next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.dnnTrainingDataService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        id: this.currentCategory.id,
        parentId: this.currentCategory.data.parentId,
        workspaceId: this.workspaceId,
        oldName: this.currentCategory.data.name,
        dnnDicId: this.selectedDnnDic.dnnDicId,
        isNew: !this.isCreated
      };
      ref.componentInstance.data = node;

      ref.afterClosed().subscribe(res => {
        if (res) {
          this.getDnnDicLineAllList();
        }
      });
    },
    remove: (node: DnnTrainingDataEntity, next: () => void) => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed()
      .subscribe(confirmed => {
        if (confirmed) {
          this.dnnTrainingDataService.deleteCategory(node).subscribe(
            res => {
              if (res.message === 'DELETE_SUCCESS') {
                this.openAlertDialog('Delete', `Category ${node.name} deleted.`, 'success');
                next();
              } else if (res.message === 'SENTENCE_EXISTENCE') {
                this.openAlertDialog('Failed', `There is a sentence in the category. Please delete the sentence.`, 'error');
              }
            },
            () => {
              this.openAlertDialog('Error', `Server Error. Category ${node.name} not deleted.`, 'error');
            }
          );
        }
      });
    },
    focus: (node: any) => {
      this.currentCategory = node;
      this.getDnnDicLineAllList(0);
    },
    blur: () => {
      this.currentCategory = null;
    },
  };

  constructor(private dnnTrainingDataService: DnnTrainingDataService,
              private dnnTrainingService: DnnTrainingService,
              private storage: StorageBrowser,
              private route: ActivatedRoute,
              private router: Router,
              private objectManager: AppObjectManagerService,
              private modelService: ModelInsertService,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actions = [
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      }, {
        type: 'train',
        text: 'Train',
        icon: 'school',
        callback: this.train,
        hidden: false
      },
      {
        type: 'save',
        text: 'Save',
        icon: 'save',
        callback: this.save,
        hidden: true
      },
    ];

    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.user = this.storage.get('user').id;
    this.header = this.defaultHeader;
    this.selectedDnnDic.id = '';
    this.selectedDnnDic.name = '';
    this.selectedDnnDic.manDesc = '';
    this.model_actions = {
      type: 'Create',
      text: 'Create model',
      icon: 'add_circle_outline',
      callback: this.createModelInfo,
      hidden: true
    };

    this.dnnData = this.objectManager.get('dnnData');
    this.route.params.subscribe(par => {
      this.objectManager.clean('dnnData');
      if (par.hasOwnProperty('id')) {
        this.isCreated = true;
        if (par['id'] !== '') {
          this.selectedDnnDic.name = par['id'];
          if (!this.dnnData) { // objectManager로 넘어온 정보 없는 경우
            let param: ModelManagementEntity = new ModelManagementEntity();
            param.hmdUse = false;
            param.dnnUse = true;
            param.workspaceId = this.workspaceId;
            param.name = this.selectedDnnDic.name;
            param.id = '';
            this.modelService.getHmdOrDnnModel(param).subscribe(res => {
              if (res) {
                this.dnnData = res;
                this.isCreated = true;
                this.model_actions.hidden = true;
                this.selectedDnnDic = res;
                this.selectedDnnDic.manDesc = this.dnnData.manDesc;
                this.getDnnCategoryAllList();
              } else {
                this.openAlertDialog('Error', `Error. Check a name of dnn model in a url`, 'error');
                this.back();
              }
            }, () => {
              this.openAlertDialog('Error', `Error. Server Error [getHmdOrDnnModel]`, 'error');
            });
          } else { // objectManager 로 정보 넘어온 경우
            this.selectedDnnDic.dnnDicId = this.dnnData.dnnDicId;
            this.selectedDnnDic.id = this.dnnData.id;
            this.selectedDnnDic.manDesc = this.dnnData.manDesc;
            this.getDnnCategoryAllList();
          }
          this.route.url.subscribe(url => {
            if (url.length > 1) {
              if (url[1].path === 'edit') {
                this.header = this.editHeader;
                this.updateToolbar();
              } else {
                this.openAlertDialog('Error', `Error. Check a url`, 'error');
                this.back();
              }
            }
          }, () => {
            this.openAlertDialog('Error', `Error. Server Error  [Init]`, 'error');
          });
        }
      } else {
        this.model_actions.hidden = false;
        this.isCreated = false;
        this.isReadOnly = false;
        this.isNew = false;
        this.store.dispatch({type: ROUTER_LOADED});
      }
    });

    this.infoGroup1 = new FormGroup({
      sentence: new FormControl('sentence'),
    });

    this.infoGroup2 = new FormGroup({
      name: new FormControl([this.selectedDnnDic.name, Validators.required]),
      manDesc: new FormControl('manDesc'),
    });
  }

  changeModelDesc = (param) => {
    if (this.isCreated) {
      this.editModelInfo(param);
    } else {
      this.selectedDnnDic.manDesc = param;
    }
  };

  changeModelName = (param) => {
    if (!this.isCreated) {
      this.selectedDnnDic.name = param;
    }
  };

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  back = () => {
    if (this.editable) {
      this.router.navigate(['..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  };

  editModelInfo = (desc: string) => {
    let modelManagementEntity: ModelManagementEntity = new ModelManagementEntity();
    modelManagementEntity.hmdUse = false;
    modelManagementEntity.dnnUse = true;
    modelManagementEntity.hmdDicId = '';
    modelManagementEntity.workspaceId = this.workspaceId;
    modelManagementEntity.updaterId = this.user;
    modelManagementEntity.manDesc = desc === '' ? this.selectedDnnDic.manDesc : desc;
    modelManagementEntity.name = null;
    modelManagementEntity.id = this.selectedDnnDic.id;
    modelManagementEntity.dnnDicId = this.selectedDnnDic.dnnDicId;
    this.modelService.updateHmdOrDnn(modelManagementEntity).subscribe(() => {
    }, () => {
      this.openAlertDialog('Error', `Change dnn model name.`, 'error');
    });
  };

  createModelInfo = () => {
    let modelManagementEntity: ModelManagementEntity = new ModelManagementEntity();
    modelManagementEntity.hmdUse = false;
    modelManagementEntity.dnnUse = true;
    modelManagementEntity.workspaceId = this.workspaceId;
    modelManagementEntity.creatorId = this.user;
    modelManagementEntity.updaterId = this.user;
    modelManagementEntity.manDesc = this.selectedDnnDic.manDesc;
    modelManagementEntity.name = this.selectedDnnDic.name;
    modelManagementEntity.dnnDicId = this.selectedDnnDic.dnnDicId;
    if (this.selectedDnnDic.name === '') {
      this.openAlertDialog('Error', `Input Dnn model name`, 'error');
    } else {
      this.isNew = 'readOnly';
      this.modelService.insertHmdOrDnn(modelManagementEntity).subscribe(res => {
        if (res.message === 'INSERT_DUPLICATED') {
          this.openAlertDialog('Duplicate', 'Model Name duplicated.', 'Error');
        } else {
          this.isCreated = true;
          this.model_actions.hidden = true;
          this.selectedDnnDic = res;
          this.dnnData = res;
          this.objectManager.set('dnn', this.selectedDnnDic);
          this.router.navigate(['..', this.selectedDnnDic.name, 'edit'], {relativeTo: this.route});
        }
      });
    }
  };

  getDnnCategoryAllList() {
    let promise = new Promise((resolve, reject) => {
      this.dnnTrainingDataService.getDnnCategoryAllList(this.workspaceId, this.selectedDnnDic.dnnDicId).subscribe(
        res => {
          if (res) {
            this.categories = res;
            resolve();
          }
        }, () => {
          this.openAlertDialog('Error', `Server error. Failed to fetch categories.`, 'error');
          reject();
        }
      );
    });

    promise.then(() => {
      this.getDnnDicLineAllList();
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  edit = () => {
    this.objectManager.set('dnn', this.selectedDnnDic);
    this.router.navigate(['edit'], {relativeTo: this.route});
  };

  testWithFiles = () => {
    this.router.navigate(['analysis'], {relativeTo: this.route});
  };

  testWithText = () => {
    this.router.navigate(['test'], {relativeTo: this.route});
  };

  addLine = (sentence = '') => {
    if (!this.currentCategory || !this.currentCategory['isLeaf']) {
      this.openAlertDialog('Failed', `Cannot add a line to non-leaf category.`, 'error');
      return false;
    }
    this.dnnDicLineEntity = new DnnTrainingDataEntity();
    this.dnnDicLineEntity.category = this.currentCategory.data.name;
    this.dnnDicLineEntity.sentence = sentence;
    this.dnnDicLineEntity.versionId = this.selectedDnnDic.dnnDicId;

    this.dnnTrainingDataService.insertLine(this.dnnDicLineEntity).subscribe(
      () => {
        this.dnnDicLineList.unshift({category: this.currentCategory.data.name, sentence: null});
        this.getDnnDicLineAllList().then(() => {
          this.appTree.currentNode.data.count += 1;
        });
      }, () => {
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  };

  setLeafChildrenCount(node, dnnDicLineList) {
    if (node.children.length > 0) {
      node.children.forEach(childChildren => {
        this.setLeafChildrenCount(childChildren, dnnDicLineList);
      });
    } else {
      let findNodeCount: number = dnnDicLineList.filter(dnnDicLine => node.name === dnnDicLine.category).length;
      if (findNodeCount > 0) {
        node.count -= findNodeCount;
      }
    }
  }

  deleteLine = () => {
    let dnnTrainingDataEntities: DnnTrainingDataEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (dnnTrainingDataEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (dnnTrainingDataEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.dnnTrainingDataService.deleteLines(dnnTrainingDataEntities).subscribe(
            res => {
              if (res) {
                this.getDnnDicLineAllList().then(() => {
                  this.appTree.nodes.forEach(node => {
                    this.setLeafChildrenCount(node, dnnTrainingDataEntities);
                  });
                  this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
                });
              }
            },
            () => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  deleteDupLine = () => {
    if (this.currentCategory === null) {
      this.openAlertDialog('Notice', `select category please.`, 'notice');
    } else {
      this.dnnTrainingDataService.getDuplicatesLines(this.selectedDnnDic.dnnDicId, this.currentCategory.data.name).subscribe(
        res => {
          if (res['resultCode'] === 'SUCCESS') {
            let resultMessage: string;
            res['dicLine'].forEach(item => {
              resultMessage += `
                <b>Sentence</b> : ${item.sentence}  <b>Count</b>: [${item.duplicateCount}]<br>
            `;
            });
            let ref = this.dialog.open(ConfirmComponent);
            ref.componentInstance.message = `
                There are ${res['dicLine'].length} kinds of duplicate sentences.<br>
                <br>${resultMessage}<br>
                Do you want to clear above duplicates?
          `;
            ref.afterClosed().subscribe(
              confirmed => {
                if (confirmed) {
                  this.dnnDicLineEntity = new DnnTrainingDataEntity();
                  this.dnnDicLineEntity.versionId = this.selectedDnnDic.dnnDicId;
                  this.dnnDicLineEntity.category = this.currentCategory.data.name;

                  this.dnnTrainingDataService.deleteDuplicatesLines(this.dnnDicLineEntity).subscribe(
                    result => {
                      if (result) {
                        let sum = 0;
                        res['dicLine'].forEach(dicLine => {
                          // 자기 자신을 제외한 dicLine 삭제 하기 때문에 -1을 해줌
                          sum += (dicLine.duplicateCount - 1);
                        });

                        this.getDnnDicLineAllList(0).then(() => {
                          this.appTree.currentNode.data.count -= sum;
                        });
                      }
                    }, () => {
                      this.openAlertDialog('Error', `Server Error. Can not deleted sentences.`, 'error');
                    }
                  )
                }
              });
          } else if (res['resultCode'] === 'NO_DUPLICATION') {
            this.openAlertDialog('No Duplication', `There are no duplicate sentence in this category.`, 'success');
          }
        },
        () => {
          this.openAlertDialog('Error', `Server Error. Can not deleted sentences.`, 'error');
        }
      )
    }
  };

  save = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDnnDic.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          let param = new DnnTrainingDataEntity();
          param.id = this.selectedDnnDic.dnnDicId;
          param.name = this.selectedDnnDic.name;
          param.workspaceId = this.selectedDnnDic.workspaceId;
          param.title = result.title;
          param.message = result.message;

          this.dnnTrainingDataService.commit(param).subscribe(
            () => {
              this.header = this.defaultHeader;
              this.router.navigate(['..'], {relativeTo: this.route});
            },
            () => {
              this.openAlertDialog('Failed', `Something wrong. Failed to commit.`, 'error');
            });
        }
      });
  };

  onUploadFile = () => {
    this.actions.forEach(action => action.disabled = true);
    this.selectFiles.emit([]);
    this.getDnnCategoryAllList();
  };

  updateToolbar = () => {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'train') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    this.isReadOnly = this.editable ? false : 'readOnly';
  };

  add = () => {
    let ref: any = this.dialog.open(DictionaryUpsertDialogComponent);
    ref.componentInstance.dicApi = this.dnnTrainingDataService;
    ref.componentInstance.workspaceId = this.workspaceId;
  };

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }
    if (this.dnnDicLineList.length > 0) {
      this.getDnnDicLineAllList();
    }
  };

  getDnnDicLineAllList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.dnnDicLineEntity = new DnnTrainingDataEntity();
    this.dnnDicLineEntity.versionId = this.selectedDnnDic.dnnDicId;
    if (this.currentCategory) {
      this.dnnDicLineEntity.category = this.currentCategory.data.name;
    }

    this.dnnDicLineEntity.sentence = this.searchSentence;
    this.dnnDicLineEntity.pageSize = this.pageParam.pageSize;
    this.dnnDicLineEntity.pageIndex = this.pageParam.pageIndex;

    return new Promise(resolve => {
      this.dnnTrainingDataService.getAllDicLines(this.dnnDicLineEntity).subscribe(
        res => {
          if (res) {
            this.dnnDicLineList = res['dicLineList'];
            this.lineTableComponent.isCheckedAll = false;
            this.pageLength = res['total'];
          }
          resolve();
        },
        () => {
          this.openAlertDialog('Error', `Server Error. Failed to fetch sentences.`, 'error');
        }
      )
    });
  }

  train = () => {
    this.router.navigate(['train'], {relativeTo: this.route});
  };

  changeSentence(data) {
    this.inputData = data;
    this.dnnDicLineEntity = new DnnTrainingDataEntity();
    this.dnnDicLineEntity.seq = this.inputData.seq;
    this.dnnDicLineEntity.versionId = this.selectedDnnDic.dnnDicId;
    this.dnnDicLineEntity.category = this.inputData.category;
    this.dnnDicLineEntity.sentence = this.inputData.sentence;

    this.dnnTrainingDataService.insertLine(this.dnnDicLineEntity).subscribe(
      () => {
      }, () => {
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };
}
