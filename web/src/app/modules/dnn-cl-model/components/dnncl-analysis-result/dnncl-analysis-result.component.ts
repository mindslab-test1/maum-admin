import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AlertComponent, ConfirmComponent, DownloadService, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {DnnResultService} from '../../services/dnn-result/dnn-result.service';
import {DnnResultEntity} from '../../entity/dnn-result/dnn-result.entity';

@Component({
  selector: 'app-dnncl-analysis-result',
  templateUrl: './dnncl-analysis-result.component.html',
  styleUrls: ['./dnncl-analysis-result.component.scss'],
})
export class DnnclAnalysisResultComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  modelName: string;
  workspaceId: string;
  inputSentence: string;
  inputCategory: string;

  actions: any;
  header = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'model', name: 'Model', width: '10%', isSort: true},
    {attr: 'fileGroup', name: 'File Group', width: '10%', isSort: true, hidden: true},
    {attr: 'sentence', name: 'Sentence'},
    {attr: 'category', name: 'Category', width: '10%', isSort: true},
    {attr: 'probability', name: 'Probability', isSort: true},
    {attr: 'createdAt', name: 'Created at', width: '10%', format: 'date', isSort: true},
  ];
  matSort: MatSort;
  dnnResults: DnnResultEntity[] = [];
  dataSource: MatTableDataSource<any>;
  pageLength: number;
  pageParam: MatPaginator;
  param: DnnResultEntity;

  constructor(private storage: StorageBrowser,
              private dnnResultService: DnnResultService,
              private downloadService: DownloadService,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      this.modelName = par['id'];
      if (!this.modelName || this.modelName === '') {
        this.back('Error. Check your model name in a url.');
      }
      this.workspaceId = this.storage.get('m2uWorkspaceId');
    });
    this.actions = [
      {
        type: 'download',
        text: 'Download',
        icon: 'file_download',
        callback: this.download,
        disabled: true,
      },
      {
        type: 'clearResults',
        text: 'Clear Results',
        icon: 'delete_sweep',
        callback: this.clear,
        disabled: true,
      },
    ];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.getAllDnnResults();
  }

  search(matSort?: MatSort) {
    this.matSort = matSort;

    if (this.dnnResults.length > 0) {
      this.getAllDnnResults();
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  download = () => {
    this.downloadService.downloadCsvFile(this.tableComponent.rows, this.header, 'result');
  };

  clear = () => {
    this.dialog.open(ConfirmComponent).afterClosed().subscribe(
      result => {
        if (!result) {
          return false;
        } else {
          if (this.dnnResults.length > 0) {
            this.dnnResultService.deleteByDnnResult(this.param).subscribe(
              res => {
                this.openAlertDialog('Delete', `Delete success. ${res['deleteCount']} deleted.`, 'success');
                this.getAllDnnResults();
              },
              err => {
                this.openAlertDialog('Failed', 'Error. delete failed.', 'error');
              });
          }
        }
      });
  };

  setPage(page?: MatPaginator) {
    this.pageParam = page;

    if (this.dnnResults.length > 0) {
      this.getAllDnnResults();
    }
  }

  getAllDnnResults(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.param = new DnnResultEntity();
    this.param.workspaceId = this.workspaceId;
    this.param.model = this.modelName;
    if (this.inputSentence === '') {
      this.param.sentence = null;
    } else {
      this.param.sentence = this.inputSentence;
    }
    this.param.category = this.inputCategory;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    this.param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.dnnResultService.getAllDnnResults(this.param).subscribe(
      res => {
        this.pageLength = res['totalElements'];
        this.dnnResults = res['content'];
        this.tableComponent.isCheckedAll = false;
      },
      err => {
        this.openAlertDialog('Failed', 'Error. failed fetch analysis result list', 'error');
      });
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.modelName === '') {
      this.router.navigate(['..', '..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
