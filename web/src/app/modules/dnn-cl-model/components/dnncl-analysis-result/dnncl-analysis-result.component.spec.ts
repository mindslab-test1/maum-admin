import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DnnclAnalysisResultComponent} from './dnncl-analysis-result.component';

describe('DnnclAnalysisResultComponent', () => {
  let component: DnnclAnalysisResultComponent;
  let fixture: ComponentFixture<DnnclAnalysisResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnnclAnalysisResultComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnnclAnalysisResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
