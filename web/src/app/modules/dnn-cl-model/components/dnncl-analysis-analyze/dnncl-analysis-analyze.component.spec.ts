import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DnnclAnalysisAnalyzeComponent} from './dnncl-analysis-analyze.component';

describe('DnnclAnalysisAnalyzeComponent', () => {
  let component: DnnclAnalysisAnalyzeComponent;
  let fixture: ComponentFixture<DnnclAnalysisAnalyzeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnnclAnalysisAnalyzeComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnnclAnalysisAnalyzeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
