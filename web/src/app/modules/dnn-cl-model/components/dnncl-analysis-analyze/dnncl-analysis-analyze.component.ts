import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AlertComponent, ConfirmComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {DnnResultService} from '../../services/dnn-result/dnn-result.service';
import {ModelInsertService} from '../../../version/model-insert.service';
import {DnnAnalyzeService} from '../../services/dnn-analyze/dnn-analyze.service';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';
import {DnnAnalyzeEntity} from '../../entity/dnn-analyze/dnn-analyze.entity';

interface ModelInfo {
  id: string;
  type: string;
  name: string;
  workspaceId: string;
  creatorId: string;
}

@Component({
  selector: 'app-ta-dnn-analyze',
  templateUrl: './dnncl-analysis-analyze.component.html',
  styleUrls: ['./dnncl-analysis-analyze.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ModelInsertService]
})
export class DnnclAnalysisAnalyzeComponent implements OnInit {
  model: ModelInfo = {
    id: '',
    type: 'dnn',
    name: '',
    workspaceId: '',
    creatorId: '',
  };

  fileList: string[] = [];
  isTest: boolean;
  workspaceId: string;

  constructor(private storage: StorageBrowser,
              private dnnResultService: DnnResultService,
              private router: Router,
              private modelService: ModelInsertService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private dnnAnalyzeService: DnnAnalyzeService,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.route.params.subscribe(par => {
      let name = par['id'];
      if (!name || name === '') {
        this.back('Error. Check your model name in a url.');
      }
      let param: ModelManagementEntity = new ModelManagementEntity();
      param.hmdUse = false;
      param.dnnUse = true;
      param.workspaceId = this.workspaceId;
      param.name = name;
      this.modelService.getHmdOrDnnModel(param).subscribe(res => {
        if (res && res.hasOwnProperty('dnnDicId') && res.dnnDicId !== '') {
          let id = res.dnnDicId;
          this.model.id = id;
          this.model.workspaceId = this.storage.get('m2uWorkspaceId');
          this.model.creatorId = this.storage.get('user').id;
          this.model.name = name;
          this.model.type = 'dnn';
        } else {
          this.back('Error. Check your model name in a url.');
        }
      })
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.model.name === '') {
      window.history.back();
    } else {
      // 목록으로 이동
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }

  gotoResult() {
    this.router.navigate(['..', 'analysis-result'], {relativeTo: this.route});
  }

  selectFiles = (event) => {
    this.fileList = event;
    this.checkButton();
  };

  checkButton = () => {
    if (this.fileList.length > 0) {
      this.isTest = true;
    } else {
      this.isTest = false;
    }
  }

  test = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to start DNN model testing?';

    ref.afterClosed().subscribe(result => {
      if (result) {
        let param = new DnnAnalyzeEntity();
        param.workspaceId = this.model.workspaceId;
        param.name = this.model.name;
        param.fileList = this.fileList;
        param.creatorId = this.model.creatorId;

        this.dnnAnalyzeService.startAnalyze(param).subscribe(
          res => {
            this.openAlertDialog('Analyze', `complete analyze.`, 'success');
          },
          err => {
            this.openAlertDialog('Failed', 'Error. Failed to Analyze', 'error');
          });
      }
    });
  };
}
