import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {DnnTrainingEntity} from '../../entity/dnn-training/dnn-training.entity';
import {DnnTrainingService} from '../../services/dnn-training/dnn-training.service';
import {ReplaySubject, Subject} from 'rxjs';
import {AlertComponent, ConfirmComponent, FormErrorStateMatcher} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';

@Component({
  selector: 'app-dnn-training',
  templateUrl: './dnncl-model-train.component.html',
  styleUrls: ['./dnncl-model-train.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})
export class DnnclModelTrainComponent implements OnInit, OnDestroy {

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredDnnDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  selectedDnnDic: DnnTrainingEntity = new DnnTrainingEntity;

  // action
  actions: any;
  node: number;
  repeat: number;
  isTrain: boolean;

  intervalHandler: any = null;

  models: DnnTrainingEntity[] = [];
  trainings: DnnTrainingEntity[] = [];

  nodeFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);

  repeatFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);
  timeout: number;
  workspaceId: string;

  constructor(public matcher: FormErrorStateMatcher,
              public dialog: MatDialog,
              private dnnTrainingService: DnnTrainingService,
              private route: ActivatedRoute,
              private router: Router,
              private storage: StorageBrowser,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actions = [
      {type: 'train', text: 'Train', icon: 'school', callback: this.train},
    ];
    this.workspaceId = this.storage.get('m2uWorkspaceId');

    this.route.params.subscribe(par => {
      if (par.hasOwnProperty('id')) {
        if (par['id'] !== '') {
          this.selectedDnnDic.name = par['id'];
          let param: ModelManagementEntity = new ModelManagementEntity();
          param.hmdUse = false;
          param.dnnUse = true;
          param.workspaceId = this.workspaceId;
          param.name = this.selectedDnnDic.name;
          this.dnnTrainingService.getDnnDicByName(this.workspaceId, this.selectedDnnDic.name).subscribe(res => {
            if (res) {
              this.selectedDnnDic = res;
              this.node = 200;
              this.repeat = 30;
              this.timeout = 5000;
              this.readyToTrain();
              this.initGetAllProgress();
            } else {
              this.openAlertDialog('Error', `Error. Check a name of dnn model in a url`, 'error');
              this.router.navigate(['..'], {relativeTo: this.route});
            }
          }, err => {
            this.openAlertDialog('Error', `Error. Server Error`, 'error');
          });
        }
      }
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  back = () => {
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.syncProgress();
    }, this.timeout);
  }

  train = () => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      let dnnTraining: DnnTrainingEntity = new DnnTrainingEntity();
      dnnTraining = this.selectedDnnDic;
      dnnTraining.node = this.node;
      dnnTraining.repeat = this.repeat;
      this.dnnTrainingService.open(dnnTraining).subscribe(
        res => {
          if (res) {
            if (this.intervalHandler) {
              clearInterval(this.intervalHandler);
              this.triggerInterval();
            }
            this.openAlertDialog('Train', 'Start dnn trained', 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', err.error.message, 'error');
        }
      )
    });
  };

  stopTraining = (key: string) => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      let dnnTrainingEntity: DnnTrainingEntity = new DnnTrainingEntity();
      dnnTrainingEntity.dnnKey = key;

      this.dnnTrainingService.stop(dnnTrainingEntity).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Stop', 'Training has been stopped.', 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', 'Server error. stop failed.', 'error');
        });
    });
  };

  deleteModel(key) {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }

      let dnnTrainingEntity: DnnTrainingEntity = new DnnTrainingEntity();
      dnnTrainingEntity.dnnKey = key;

      this.dnnTrainingService.deleteModel(dnnTrainingEntity).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Delete', 'Delete model success.', 'success');
            this.getDnnAllModels(dnnTrainingEntity);
          }
        },
        err => {
          this.openAlertDialog('Error', 'Server error. Training delete failed.', 'error');
        });
    });
  }

  readyToTrain() {
    if (this.node != null && this.repeat != null && this.selectedDnnDic != null
      && !this.matcher.isErrorState(this.nodeFormControl)
      && !this.matcher.isErrorState(this.repeatFormControl)) {
      this.isTrain = false;
    } else {
      this.isTrain = true;
    }
  }

  getClassButton() {
    return this.isTrain === true ? 'mlt-disabled' : 'mlt-not-disabled';
  }

  getDnnAllModels(dnnTrainingEntity: DnnTrainingEntity) {
    dnnTrainingEntity.workspaceId = this.storage.get('m2uWorkspaceId');
    dnnTrainingEntity.name = this.selectedDnnDic.name;
    this.dnnTrainingService.getModelsByName(dnnTrainingEntity).subscribe(
      res => {
        if (res) {
          if (this.models.length === 0) {
            this.models = res;
          } else if (this.models.length > res.length) {
            this.models.forEach((model, index) => {
              let data = res.find(item => item.dnnKey === model.dnnKey);
              if (!data) {
                this.models.splice(index, 1);
              }
            });
          } else if (this.models.length < res.length) {
            res.forEach(model => {
              let data = this.models.find(item => item.dnnKey === model.dnnKey);
              if (!data) {
                this.models.push(model);
              }
            });
          }
        }
      },
      err => {
        this.openAlertDialog('Error', 'Server error. failed to fetch dnn models.', 'error');
      });
  }

  initGetAllProgress() {

    let dnnTrainingEntity: DnnTrainingEntity = new DnnTrainingEntity();
    dnnTrainingEntity.workspaceId = this.storage.get('m2uWorkspaceId');

    this.dnnTrainingService.getAllProgress(dnnTrainingEntity).subscribe(
      res => {
        if (res) {
          this.trainings = res['dtList'];
        }
        this.getDnnAllModels(dnnTrainingEntity);
        this.triggerInterval();
      },
      err => {
        if (err.error.message === 'NOT_STARTED') {
          this.openAlertDialog('Error', 'Server error. dnn connect failed.', 'error');
        }
      });
  }

  syncProgress = () => {
    let dnnTrainingEntity: DnnTrainingEntity = new DnnTrainingEntity();
    dnnTrainingEntity.workspaceId = this.storage.get('m2uWorkspaceId');

    this.dnnTrainingService.getAllProgress(dnnTrainingEntity).subscribe(
      res => {
        if (res) {
          if (this.trainings.length === 0) {
            clearInterval(this.intervalHandler);
            this.timeout = 5000;
            this.triggerInterval();
            this.trainings = res['dtList'];
          } else {
            clearInterval(this.intervalHandler);
            this.timeout = 450;
            this.triggerInterval();
            if (this.trainings.length <= res['dtList'].length) {
              res['dtList'].forEach(dnnModel => {
                let training: DnnTrainingEntity = this.trainings.find(item => item.dnnKey === dnnModel.dnnKey);
                if (training) {
                  training['progress'] = dnnModel.progress;
                } else {
                  this.trainings.push(dnnModel);
                }
              });
            } else if (this.trainings.length > res['dtList'].length) {
              this.trainings.forEach((dnnModel, index) => {
                let training: DnnTrainingEntity = res['dtList'].find(item => item.dnnKey === dnnModel.dnnKey);
                if (training) {
                  training['progress'] = dnnModel['progress'];
                } else {
                  this.trainings.splice(index, 1);
                }
              });
            }
          }
        }
        this.getDnnAllModels(dnnTrainingEntity);
      },
      err => {
        if (err.error.message === 'NOT_STARTED') {
          clearInterval(this.intervalHandler);
          this.openAlertDialog('Error', 'Server error. dnn connect failed.', 'error');
        }
      });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
