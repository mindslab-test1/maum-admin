import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DnnclModelTrainComponent} from './dnncl-model-train.component';

describe('DnnclModelTrainComponent', () => {
  let component: DnnclModelTrainComponent;
  let fixture: ComponentFixture<DnnclModelTrainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DnnclModelTrainComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnnclModelTrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
