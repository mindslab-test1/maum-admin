import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DnnTrainingEntity} from '../../entity/dnn-training/dnn-training.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class DnnTrainingService {
  API_URL;
  WORK_SPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.WORK_SPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  getDnnDicAllList(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/training/getDnnDicAllList/', dnnTrainingEntity);
  }

  getDnnDictionaryId(name: string): Observable<any> {
    let dnnTrainingEntity: DnnTrainingEntity;
    dnnTrainingEntity.name = name;
    dnnTrainingEntity.workspaceId = this.WORK_SPACE_ID;
    return this.http.post(this.API_URL + '/ta/dnn/training/getDnnId/', dnnTrainingEntity);
  }

  getDnnDicByName(workspaceId: string, name: string): Observable<any> {
    let param: DnnTrainingEntity = new DnnTrainingEntity();
    param.workspaceId = workspaceId;
    param.name = name;
    return this.http.post(this.API_URL + '/ta/dnn/training/getDnnDicByName', param);
  }

  getAllModels(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/getAllModels/`, dnnTrainingEntity);
  }

  getModelsByName(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/getModelsByName/`, dnnTrainingEntity);
  }

  getAllProgress(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/getAllProgress/`, dnnTrainingEntity);
  }

  deleteModel(dnnTrainingEntity: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/deleteModel/`, dnnTrainingEntity);
  }

  open(dnnTraining: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/open/`, dnnTraining);
  }

  stop(dnnTraining: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/stop/`, dnnTraining);
  }

  deleteTraining(dnnTraining: DnnTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training/deleteTraining/`, dnnTraining);
  }
}
