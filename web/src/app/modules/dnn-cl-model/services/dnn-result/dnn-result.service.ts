import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DnnResultEntity} from '../../entity/dnn-result/dnn-result.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class DnnResultService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllDnnResults(dnnResultEntity: DnnResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/result/getAllDnnResults/`, dnnResultEntity);
  }

  deleteByDnnResult(dnnResultEntity: DnnResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/result/deleteByDnnResult/`, dnnResultEntity);
  }
}
