import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class ModelInsertService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  insertHmdOrDnn(insertData: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/insertHmdOrDnn', insertData);
  }

  getHmdOrDnn(model: ModelManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getHmdOrDnn', model);
  }

  deleteSelectedModels(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/deleteSelectedModels', models);
  }

  updateHmdOrDnn(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/updateHmdOrDnn', models);
  }

  getHmdOrDnnModel(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getHmdOrDnnModel', models);
  }

  getHmdOrDnnWithCommitHistory(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getHmdOrDnnWithCommitHistory', models);
  }
}
