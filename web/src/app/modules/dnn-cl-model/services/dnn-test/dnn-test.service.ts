import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {DnnTestEntity} from '../../entity/dnn-test/dnn-test.entity';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class DnnTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllModels(param: DnnTestEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/test/getAllModels/`, param);
  }

  getModel(name: string, workspaceId: string): Observable<any> {
    let param: DnnTestEntity = new DnnTestEntity;
    param.name = name;
    param.workspaceId = workspaceId;
    return this.http.post(`${this.API_URL}/ta/dnn/test/getModel/`, param);
  }

  taDnnAnalyzeText(param: DnnTestEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/test/taDnnAnalyzeText/`, param);
  }
}
