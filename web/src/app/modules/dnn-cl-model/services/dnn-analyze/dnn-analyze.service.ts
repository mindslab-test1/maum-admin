import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DnnAnalyzeEntity} from '../../entity/dnn-analyze/dnn-analyze.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class DnnAnalyzeService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllModels(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/analyze/getAllModels/`, param);
  }

  getFileGroups(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/analyze/getFileGroups/', param);
  }

  getFileList(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/analyze/getFileList/', param);
  }

  startAnalyze(param: DnnAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/dnn/analyze/startAnalyze', param);
  }
}
