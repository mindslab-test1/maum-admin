import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DnnTrainingDataEntity} from '../../entity/dnn-training-data/dnn-training-data.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class DnnTrainingDataService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getDnnDicAllList(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/ta/dnn/training-data/getDnnDicAllList/' + workspaceId);
  }

  getDnnCategoryAllList(workspaceId: string, dnnDicId: string): Observable<any> {
    return this.http.get(`${this.API_URL}/ta/dnn/training-data/getDnnCategoryAllList/${workspaceId}/dnnDicId/${dnnDicId}`);
  }

  insertCategory(category: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/insertCategory`, category);
  }

  updateCategory(category: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/updateCategory`, category);
  }

  deleteCategory(category: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/deleteCategory`, category);
  }

  hasLines(dnnTrainingDataEntity: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/hasLines/`, dnnTrainingDataEntity);
  }

  getAllDicLines(dnnTrainingDataEntity: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/getAllDicLines/`, dnnTrainingDataEntity);
  }

  getDuplicatesLines(versionId: string, category: string): Observable<any> {
    return this.http.get(`${this.API_URL}/ta/dnn/training-data/getDuplicatesLines/${versionId}/category/${category}`);
  }

  insertLine(dnnTrainingDataEntity: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/insertLine`, dnnTrainingDataEntity);
  }

  deleteLines(dnnTrainingDataEntities: DnnTrainingDataEntity[]): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/deleteLines`, dnnTrainingDataEntities);
  }

  deleteDuplicatesLines(dnnTrainingDataEntity: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/deleteDuplicatesLines`, dnnTrainingDataEntity);
  }

  commit(dnnTrainingDataEntity: DnnTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/dnn/training-data/commit`, dnnTrainingDataEntity);
  }
}
