import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DnnclModelsComponent} from './components/dnncl-models/dnncl-models.component';
import {DnnclModelDetailComponent} from './components/dnncl-model-detail/dnncl-model-detail.component';
import {DnnclModelTrainComponent} from './components/dnncl-model-train/dnncl-model-train.component';
import {DnnclAnalysisAnalyzeComponent} from './components/dnncl-analysis-analyze/dnncl-analysis-analyze.component';
import {DnnclAnalysisResultComponent} from './components/dnncl-analysis-result/dnncl-analysis-result.component';
import {DnnclAnalysisTestComponent} from './components/dnncl-analysis-test/dnncl-analysis-test.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        // sideNav를 선택하면 /dashboard 로 이동합니다
        path: '',
        component: DnnclModelsComponent,
        data: {hasNoSidebar: false}  // sidebar를 삭제 합니다
      },
      {
        path: 'new',
        component: DnnclModelDetailComponent,
      },
      {
        path: ':id/train',
        component: DnnclModelTrainComponent
      },
      {
        path: ':id/analysis',
        component: DnnclAnalysisAnalyzeComponent
      },
      {
        path: ':id/analysis-result',
        component: DnnclAnalysisResultComponent
      },
      {
        path: ':id/test',
        component: DnnclAnalysisTestComponent
      },
      {
        path: ':id/edit',
        component: DnnclModelDetailComponent
      },
      {
        path: ':id',
        component: DnnclModelDetailComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DnnClModelRoutingModule {
}
