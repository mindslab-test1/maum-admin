export class DnnTrainingEntity {

  // DnnDicModel and DnnDicTraining
  workspaceId: string;
  dnnKey: string;

  // DnnModel only use
  name: string;
  dnnBinary: string;

  // DnnDicEntity only use
  id: string;
  versions: string;
  applied: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;

  // not entity, only data transfer use
  node: number;
  repeat: number;

}
