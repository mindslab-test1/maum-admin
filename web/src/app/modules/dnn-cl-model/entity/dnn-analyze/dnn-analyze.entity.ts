import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class DnnAnalyzeEntity extends PageParameters {

  // common
  id: string;
  name: string;

  // only DnnModelEntity
  dnnKey: string;
  fileList: string[];
  workspaceId: string;
  creatorId: string;

  dnnBinary: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;

  // only FileGroup
  fileGroup: string;
  fileGroupId: string;
  purpose: string;

  // only File
  type: string;
  size: number;
  uploader: string;
  uploadedAt: Date;

}
