import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class DnnResultEntity extends PageParameters {

  id: string;
  workspaceId: string;
  model: string;
  fileGroup: string;
  sentence: string;
  category: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;

}
