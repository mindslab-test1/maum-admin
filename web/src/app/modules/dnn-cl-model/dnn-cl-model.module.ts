import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DnnClModelRoutingModule} from './dnn-cl-model-routing.module';
import {DnnclAnalysisAnalyzeComponent} from './components/dnncl-analysis-analyze/dnncl-analysis-analyze.component';
import {DnnclAnalysisResultComponent} from './components/dnncl-analysis-result/dnncl-analysis-result.component';
import {DnnclAnalysisTestComponent} from './components/dnncl-analysis-test/dnncl-analysis-test.component';
import {DnnclModelsComponent} from './components/dnncl-models/dnncl-models.component';
import {DnnclModelTrainComponent} from './components/dnncl-model-train/dnncl-model-train.component';
import {DnnclModelDetailComponent} from './components/dnncl-model-detail/dnncl-model-detail.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressBarModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {DnnTrainingDataService} from './services/dnn-training-data/dnn-training-data.service';
import {ModelInsertService} from './services/model-insert/model-insert.service';
import {DnnTrainingService} from './services/dnn-training/dnn-training.service';
import {DnnTestService} from './services/dnn-test/dnn-test.service';
import {DnnResultService} from './services/dnn-result/dnn-result.service';
import {DnnAnalyzeService} from './services/dnn-analyze/dnn-analyze.service';

@NgModule({
  declarations: [
    DnnclAnalysisAnalyzeComponent,
    DnnclAnalysisResultComponent,
    DnnclAnalysisTestComponent,
    DnnclModelsComponent,
    DnnclModelTrainComponent,
    DnnclModelDetailComponent,
  ],
  imports: [
    CommonModule,
    DnnClModelRoutingModule,
    MatToolbarModule,
    MatSelectModule,
    MatCardModule,
    SharedModule,
    MatInputModule,
    MatProgressBarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
  ],
  exports: [
    DnnclAnalysisAnalyzeComponent,
    DnnclAnalysisResultComponent,
    DnnclAnalysisTestComponent,
    DnnclModelsComponent,
    DnnclModelTrainComponent,
    DnnclModelDetailComponent,
  ],
  providers: [
    DnnTrainingDataService,
    DnnTrainingService,
    ModelInsertService,
    DnnTestService,
    DnnResultService,
    DnnAnalyzeService
  ]
})
export class DnnClModelModule {
}
