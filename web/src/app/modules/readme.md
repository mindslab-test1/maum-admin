## 이 디렉토리는
- 재사용 가능한 모듈들의 집합이다.
- 재사용하는 웹 응용프로그램은 maum.ai chatbot builder, mlt(machine learning tutor) 이다.
- maum.ai chatbot builder는 기존의 m2u admin + mlt 기능을 통합하되, workspace 개념으로 통폐합된다.
- mlt는 workspace 개념을 복원하여 재정비된다.


## what is workspace?
- 웤크스페이스는 개발자, 서비스 운영자를 위한 공간이다.
- 이 공간에서 개발자는 학습, 운영, 개발을 진행한다.
- 이 공간에서 운영자는 서비스를 모니터링하고 설계한다.
- 이 서비스는 하나의 서비스로 클라우드에서 운영되는 것을 목표로 한다.


## 하위 디렉토리의 구조
- chatbot 계열
  - chatbot
  - chatbot의 로그
  - chatbot의 로그 통계
  - intent finder
    - dnn cl
    - hmd
    - simple classifier
    - xdc
  - question-answering engine
    - nqa(natural questions)
    - bqa(basic qa)
  - dialog model
  - dialog model design
  - advanced
    - stt
    - process monitoring
    - dialog agent
    - dialog agent instance
  - administrator
  - profile for each users
