import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VersionMainComponent} from './version-main.component';

describe('VersionMainComponent', () => {
  let component: VersionMainComponent;
  let fixture: ComponentFixture<VersionMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VersionMainComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
