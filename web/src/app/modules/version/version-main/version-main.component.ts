import {ChangeDetectorRef, Component, Inject} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AlertComponent, DownloadService} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {ActivatedRoute, Router} from '@angular/router';
import {ROUTER_LOADED} from '../../../core/actions';
import {Store} from '@ngrx/store';
import {ModelInsertService} from '../model-insert.service';

interface ModelInfo {
  id: string;
  type: string;
  name: string;
  path: string;
  workspaceId: string;
}

enum ModelType {
  dnn = 0,
  hmd = 1,
  ner = 2,
  morph = 3,
  replacement = 5,
  xdc = 6
}

enum ModelPart {
  'dnncl-models' = 0,
  'hmd-models' = 1,
  'ner' = 2,
  'morps' = 3,
  'replacement' = 5,
  'xdc-models' = 6
}

enum ModelPath {
  'training-data' = 0,
  'dic' = 1
}

@Component({
  selector: 'app-ta-version',
  templateUrl: './version-main.component.html',
  styleUrls: ['./version-main.component.scss']
})
export class VersionMainComponent {
  model: ModelInfo = {
    id: '',
    type: toString(),
    name: '',
    path: '',
    workspaceId: ''
  };
  title = '';

  constructor(
    private modelService: ModelInsertService,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private downloadService: DownloadService,
    private cdr: ChangeDetectorRef,
    private store: Store<any>,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      this.model.id = par['id'];
      this.model.name = par['name'];
      this.model.type = par['type'];
      if (par['type'] in ModelType === false) {
        this.back('Error. Check a type of dictionary  in a url');
      }
      if (!this.model.name || this.model.name === '') {
        this.back('Error. Check your dictionary name in a url');
      }
      if (!this.model.id || this.model.id === '') {
        this.back('Error. Check your dictionary id in a url');
      }
    });
    this.model.workspaceId = this.storage.get('m2uWorkspaceId');
    this.store.dispatch({type: ROUTER_LOADED});
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.model.type in ModelType) {
      if (this.model.type === ModelType[2] || this.model.type === ModelType[3] || this.model.type === ModelType[5]) { // dictionary 경우
        this.router.navigate(['./..', 'dict-nlp3', ModelPart[ModelType[this.model.type]]], {relativeTo: this.route.parent});
      } else {
        this.router.navigate(['./..', ModelPart[ModelType[this.model.type]]], {relativeTo: this.route.parent});
      }
    } else {
      window.history.back();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
