import {GitComponent} from 'app/shared/components/git/git.component';
import {AlertComponent} from 'app/shared/components/dialog/alert.component';
import {DownloadService} from 'app/shared/services/download.service';
import {
  ChangeDetectorRef,
  Component,
  Input,
  SimpleChanges,
  ViewChild, ViewEncapsulation
} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';

interface ModelInfo {
  id: string;
  type: string;
  name: string;
  path: string;
  part: string;
  workspaceId: string;
}

enum ModelType {
  dnn = 0,
  hmd = 1,
  morph = 2,
  ner = 3,
  stt = 4,
  replacement = 5,
  xdc = 6
}

enum ModelPath {
  'training-data' = 0,
  'dic' = 1,
  'dictionary' = 2,
  'am/transcription' = 3 // todo 추후 stt url 체계변경되면 알맞게 변경필요.
}

enum ModelPart {
  ta = 0,
  dictionary = 1,
  stt = 2, // todo 추후 stt url 체계변경되면 알맞게 변경필요.
}

enum UiPath {
  'dnncl-models' = 0,
  'hmd-models' = 1,
  'dict-nlp3/morps' = 2,
  'dict-nlp3/ner' = 3,
  'stt' = 4, // todo 추후 stt url 체계변경되면 알맞게 변경필요.
  'dict-nlp3/replacement' = 5,
  'xdc-models' = 6
}

@Component({
  selector: 'app-version',
  templateUrl: './version-detail.component.html',
  styleUrls: ['./version-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class VersionDetailComponent {
  @Input() type: string;
  @Input() name: string;
  @Input() id: string;
  @Input() workspaceId: string;
  @ViewChild(GitComponent) private gitComp: GitComponent;

  model: ModelInfo = {
    id: '',
    type: '',
    name: '',
    path: '',
    part: '',
    workspaceId: ''
  };

  full_path = '';

  constructor(
    private downloadService: DownloadService,
    private cdr: ChangeDetectorRef,
    private store: Store<any>,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog) {
  }

  ngOnChanges(changes: SimpleChanges) {
    try {
      if (changes.hasOwnProperty('workspaceId') &&
        changes.workspaceId.currentValue !== '' &&
        changes.workspaceId.currentValue !== undefined) {
        this.model.workspaceId = changes.workspaceId.currentValue;
      }
      if (changes.hasOwnProperty('name') && changes.name.currentValue !== '' && changes.name.currentValue !== undefined) {
        this.model.name = changes.name.currentValue;
        if (this.name === '') {
          this.back('Error. Check your dictionary name in a url', this.model.type);
        }
      }
      if (changes.hasOwnProperty('id') && changes.id.currentValue !== '' && changes.id.currentValue !== undefined) {
        if (this.id === '') {
          this.back('Error. Check your dictionary name in a url', this.model.type);
        }
        this.model.id = changes.id.currentValue;
      }
      if (changes.hasOwnProperty('type') && changes.type.currentValue !== '' && changes.type.currentValue !== undefined) {
        this.model.type = changes.type.currentValue;
        if (this.type in ModelType === false) {
          this.back('Error. Check a type of dictionary  in a url');
        }
        if (this.model.type in ModelType) {
          if (this.model.type === ModelType[0]) { // dnn
            this.model.path = ModelPath[0];
          } else if (this.model.type === ModelType[1]) { // hmd
            this.model.path = ModelPath[1];
          } else if (this.model.type === ModelType[2] || this.model.type === ModelType[3]
          || this.model.type === ModelType[5]) { // dictionary
            this.model.path = ModelPath[2];
          } else if (this.model.type === ModelType[4]) { // stt
            this.model.path = ModelPath[3];
          } else if (this.model.type === ModelType[6]) { // xdc
            this.model.path = ModelPath[0];
          }
          // this.model.path = ModelPath[ModelType[this.model.type]];
          if (this.model.type === ModelType[0] || this.model.type === ModelType[1] || this.model.type === ModelType[6]) { // dnn, hmd, xdc
            this.model.part = ModelPart[0];
            this.full_path = '/' + this.model.part + '/' + this.model.type + '/' + this.model.path;
          } else if (this.model.type === ModelType[2] || this.model.type === ModelType[3]
            || this.model.type === ModelType[5]) { // dictionary
            this.model.part = ModelPart[1];
            this.full_path = '/' + this.model.part + '/' + this.model.type;
          } else if (this.model.type === ModelType[4]) { // stt
            this.model.part = ModelPart[2];
            this.full_path = '/' + this.model.part + '/' + this.model.type + '/' + this.model.path;
          }
        }
      }
    } catch (e) {
      this.openAlertDialog('Load Result', 'Something wrong!', 'error');
    }
  }

  back(msg = '', type = '', name = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.model.part === '') {
      window.history.back();
    } else {
      this.router.navigateByUrl('m2u-builder/' + UiPath[ModelType[this.model.type]]);
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}

