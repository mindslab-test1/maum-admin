import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {VersionMainComponent} from './version-main/version-main.component';

const routes: Routes = [
  {
    path: ':type/:name/:id',
    component: VersionMainComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VersionRoutingModule {
}
