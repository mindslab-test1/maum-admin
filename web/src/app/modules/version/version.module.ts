import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VersionRoutingModule} from './version-routing.module';
import {VersionMainComponent} from './version-main/version-main.component';
import {VersionDetailComponent} from './version-detail/version-detail.component';
import {SharedModule} from '../../shared/shared.module';
import {MatButtonModule, MatIconModule, MatToolbarModule} from '@angular/material';
import {ModelInsertService} from './model-insert.service';

@NgModule({
  declarations: [VersionMainComponent, VersionDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    VersionRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [VersionDetailComponent],
  providers: [ModelInsertService]
})
export class VersionModule {
}
