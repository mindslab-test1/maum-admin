import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PosTestComponent} from './pos-test.component';

describe('PosTestComponent', () => {
  let component: PosTestComponent;
  let fixture: ComponentFixture<PosTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PosTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
