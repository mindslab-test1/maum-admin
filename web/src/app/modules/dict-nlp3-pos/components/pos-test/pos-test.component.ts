import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {AlertComponent} from 'app/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {PosDictionaryService} from '../../services/pos-dictionaries/pos-dictionary.service';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {PosTestResultService} from '../../services/pos-test-result/pos-test-result.service';
import {PosDictionaryLineEntity} from "../../entity/pos-dictionary-line/pos-dictionary-line.entity";

enum PosDicType {
  MORP_CUSTOM = 0, // 사용자 사전
  MORP_EOJEOL = 1, // 어절 사전
  MORP_COMPOUND_WORD = 2, // 복합 명사 사전
  MORP_POST_PATTERN_STR = 3, // 후처리 패턴 String 사전
  // MORP_POST_PATTERN_MORP = 4, // 후처리 패턴 형태소 사전
}

@Component({
  selector: 'app-pos-test',
  templateUrl: './pos-test.component.html',
  styleUrls: ['./pos-test.component.scss']
})
export class PosTestComponent implements OnInit {
  test_dictionaries = [
    {
      name: 'pre custom dictionary',
      value: PosDicType.MORP_CUSTOM,
      checked: false,
      disabled: true
    },
    {
      name: 'post pattern string dictionary',
      value: PosDicType.MORP_POST_PATTERN_STR,
      checked: false, disabled: true
    },
    // {
    //   name: 'post pattern morph dictionary',
    //   value: PosDicType.MORP_POST_PATTERN_MORP,
    //   checked: false, disabled: true
    // },
    {
      name: 'compound dictionary',
      value: PosDicType.MORP_COMPOUND_WORD,
      checked: false,
      disabled: true
    },
  ];

  fileList: string[] = [];
  testStat = 0;
  checkedList: number[] = [];

  isTest: boolean;
  isReset: boolean;
  test_btn: HTMLElement;
  reset_btn: HTMLElement;

  dictionaryId = '';
  dictionaryType = '';
  dictionaryName = '';
  workspaceId = '';

  constructor(private posDictionaryService: PosDictionaryService,
              private testResultService: PosTestResultService,
              private route: ActivatedRoute,
              private store: Store<any>,
              private router: Router,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.isReset = false;
    this.isTest = false;
    this.changeBtnColor();

    this.load();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  load() {
    new Promise((resolve, reject) => {
      // 파일 사전 기본 정보 가져오기
      this.route.params.subscribe(par => {
        this.dictionaryType = 'morph';
        this.dictionaryName = par['id'];
        this.posDictionaryService.getDictionary(this.dictionaryName).subscribe(res => {
          if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
            this.dictionaryId = res.dictionary_id;
            this.dictionaryId = res.dictionary_id;
            this.workspaceId = this.storage.get('m2uWorkspaceId');
            resolve();
          } else {
            reject('Error. Something wrong!');
          }
        }, err => {
          reject('Error. Something wrong!');
        });
      }, err2 => reject('Error. Something wrong!'));
    }).then(() => {
      let req: PosDictionaryLineEntity = new PosDictionaryLineEntity();
      req.id = this.dictionaryId;
      this.test_dictionaries.forEach(x => {
        req.pageIndex = 1;
        req.pageSize = 1;
        req.versionId = this.dictionaryId;
        req.workspaceId = this.workspaceId;
        req.dicType = x.value;
        this.posDictionaryService.getDictionaryContentsWithPage(req).subscribe(res => {
          if (res.hasOwnProperty('dictionary_contents')) {
            if (res['dictionary_contents'].hasOwnProperty('content')) {
              x.disabled = false;
            }
          }
        });
      });
      this.checkButton();
    }).catch((msg: string) => {
      this.back(msg);
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  changeBtnColor = () => {
    this.test_btn = <HTMLElement>document.getElementById('test-btn');
    this.test_btn.style.backgroundColor = this.isTest ? '#52627b' : '#777777';
    this.reset_btn = <HTMLElement>document.getElementById('reset-btn');
    this.reset_btn.style.backgroundColor = this.isReset ? '#52627b' : '#777777';
  };

  test = () => {
    // 사전 적용 및 테스트 결과  보여주기
    new Promise((resolve, reject) => {
      let param = {
        'checkedList': this.checkedList,
        'fileList': this.fileList
      };
      this.isReset = false;
      this.isTest = false;
      this.changeBtnColor();
      this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
        resolve();
      }, err => {
        reject();
      });
    }).then(() => {
      let tmp = this.dictionaryId;
      this.dictionaryId = '';
      this.dictionaryId = tmp;
      this.testStat += 1;
      this.checkButton();
      this.openAlertDialog('Test', 'Test done.', 'success');
    }).catch(() => {
      this.checkButton();
      this.openAlertDialog('Test', 'Something wrong', 'error');
    });
  };

  reset = () => {
    let param = {
      'checkedList': this.checkedList,
      'fileList': []
    };
    this.isReset = false;
    this.isTest = false;
    this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
      this.openAlertDialog('Test', 'Reset done.', 'success');
    }, error1 => {
      this.openAlertDialog('Reset', 'Something wrong!', 'error');
    });
    this.checkButton();
  };

  back = (msg: string) => {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  clickCheckbox = (val) => {
    let target = this.test_dictionaries.filter(x => x.value === val)[0];
    target.checked = !target.checked;
    this.checkButton();
  };

  selectFiles = (event) => {
    this.fileList = event;
    this.checkButton();
  };

  checkButton = () => {
    this.checkedList = this.test_dictionaries
    .filter(x => x.checked === true && x.disabled === false)
    .map(e => e.value);
    if (this.checkedList.length > 0) {
      this.isReset = true;
      if (this.fileList.length > 0) {
        this.isTest = true;
      } else {
        this.isTest = false;
      }
    } else {
      this.isTest = false;
      this.isReset = false;
    }
    this.changeBtnColor();
  }
}
