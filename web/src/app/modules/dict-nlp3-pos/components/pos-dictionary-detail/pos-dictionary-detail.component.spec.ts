import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PosDictionaryDetailComponent} from './pos-dictionary-detail.component';

describe('PosDictionaryDetailComponent', () => {
  let component: PosDictionaryDetailComponent;
  let fixture: ComponentFixture<PosDictionaryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PosDictionaryDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosDictionaryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
