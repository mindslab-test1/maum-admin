import {
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {AlertComponent} from 'app/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {PosDictionaryService} from '../../services/pos-dictionaries/pos-dictionary.service';
import {PosTestResultService} from '../../services/pos-test-result/pos-test-result.service';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';

interface ResultInfo {
  id: string;
  dictionaryName: string;
  createdAt: Date;
  tester: string;
  workspaceId: string;
}

interface DicInfo {
  id: string;
  name: string;
  workspace: string;
  type: string;
}

@Component({
  selector: 'app-pos-test-result',
  templateUrl: './pos-test-result.component.html',
  styleUrls: ['./pos-test-result.component.scss']
})
export class PosTestResultComponent implements OnChanges {

  @Input() dictionaryId: string;
  @Input() dictionaryName: string;
  @Input() dictionaryType: string;
  @Input() testState: number;

  public gridOptions: GridOptions;
  public defaultColDef;

  actions: any;
  dictionary: DicInfo = {
    id: undefined,
    name: '',
    workspace: undefined,
    type: undefined
  };
  row_data: ResultInfo[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private posDictionaryService: PosDictionaryService,
              private testResultService: PosTestResultService,
              private store: Store<any>,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onRowSelected: this.onRowSelected,
      onCellClicked: (event) => {
        if (event.colDef.field === 'detail') {
          this.router.navigate(['../../../test-result/morph', event.data.dictionaryName, event.data.id], {relativeTo: this.route});
        }
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };

    this.actions = [{
      type: 'delete',
      text: 'Delete',
      icon: 'delete_sweep',
      callback: this.delete,
      disabled: true,
      hidden: false
    }];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnChanges(changes: SimpleChanges) {
    try {
      if (this.dictionaryId && this.dictionaryType.toUpperCase() === 'MORPH') {
        this.row_data = [];
        this.posDictionaryService.getDictionaryName(this.dictionaryId).subscribe(res => {
          if (res.hasOwnProperty('dictionary_name')) {
            this.dictionaryName = res.dictionary_name;
          }
          this.getData();
        });
      }
    } catch (e) {
      this.openAlertDialog('Load Result', 'Something wrong!', 'error');
    }
  }

  getData = () => {
    this.row_data = [];
    this.testResultService.getResult(this.dictionaryType, this.dictionaryId).subscribe(
      res => {
        if (res.hasOwnProperty('list')) {
          res.list.forEach(one => {
            let elem: ResultInfo = {
              id: undefined,
              dictionaryName: this.dictionaryName,
              createdAt: undefined,
              tester: undefined,
              workspaceId: undefined,
            };
            elem.id = one.id;
            elem.createdAt = one.createdAt;
            elem.tester = one.tester;
            elem.workspaceId = one.workspaceId;
            this.row_data.push(elem);
          });
          this.gridOptions.api.setRowData(this.row_data);
        }
      }, err => {
        this.openAlertDialog('Load Result', 'Something wrong!', 'error');
      }
    );
  };

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    if (selectedData.length > 0) {
      let ids: number[] = selectedData.map(i => i.id);
      this.testResultService.deleteResult(this.dictionaryType, ids).subscribe(res => {
        this.openAlertDialog('Delete', `Corpus file downloaded successfully.`, 'success');
        this.getData();
      }, err => {
        this.openAlertDialog('Delete', 'Something wrong.', 'error');
      });
    }
  };

  onRowSelected = (param) => {
    let deleteBtn = this.actions.filter(y => y.type === 'delete')[0];
    if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
      deleteBtn.disabled = false;
    } else {
      deleteBtn.disabled = true;
    }
  };

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 2,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 1,
      hide: true,
      editable: false,
    }, {
      headerName: 'Dictionary name',
      field: 'dictionaryName',
      width: 40,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Created date',
      field: 'createdAt',
      width: 40,
      cellRenderer: this.dateCellRenderer,
      editable: false
    }, {
      headerName: 'Detail',
      field: 'detail',
      width: 40,
      filter: 'agTextColumnFilter',
      cellRenderer: this.nameRenderer,
      editable: false
    },]
  }

  dateCellRenderer = (params) => {
    let date: Date = new Date(params.value);
    let result = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
    return result;
  };

  nameRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">view</sapn>';
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }
}
