import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PosTestResultComponent} from './pos-test-result.component';

describe('PosTestResultComponent', () => {
  let component: PosTestResultComponent;
  let fixture: ComponentFixture<PosTestResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PosTestResultComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosTestResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
