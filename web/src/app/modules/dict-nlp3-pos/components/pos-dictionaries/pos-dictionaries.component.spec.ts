import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PosDictionariesComponent} from './pos-dictionaries.component';

describe('PosDictionariesComponent', () => {
  let component: PosDictionariesComponent;
  let fixture: ComponentFixture<PosDictionariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PosDictionariesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosDictionariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
