import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class PosDictionaryLineEntity extends PageParameters {
  id: string;
  dicType: number;
  pattern: string;
  word: string;
  srcPos: string;
  destPos: string;
  lineIdx: number;
  compoundType: string;
  description: string;
  versionId: string;
  createdAt: Date;
  workspaceId: string;
}
