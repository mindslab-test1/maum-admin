import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class TestFileService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  // type에 따른 리스트 조회(ner, morph)
  getFileList(param: string): Observable<any> {
    let url: string = this.API_URL + '/dictionary/test/get-fileList/' + param + '/' + this.WORKSPACE_ID;
    return this.http.post(url, null);
  }

  // type에 따른 파일 추가(ner, morph)
  insertFile(param: string, data: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/test/upload-file/' + param + '/'
      + this.WORKSPACE_ID + '/' + this.ID, data);
  }

  // type에 따른 파일 삭제(ner, morph)
  deleteFiles(param: string, ids: number[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/test/delete-files/' + param + '/' + this.WORKSPACE_ID, ids);
  }
}
