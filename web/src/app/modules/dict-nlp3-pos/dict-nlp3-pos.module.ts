import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import {DictNlp3PosRoutingModule} from './dict-nlp3-pos-routing.module';
import {PosDictionariesComponent} from './components/pos-dictionaries/pos-dictionaries.component';
import {PosDictionaryDetailComponent} from './components/pos-dictionary-detail/pos-dictionary-detail.component';
import {PosTestComponent} from './components/pos-test/pos-test.component';
import {PosTestResultComponent} from './components/pos-test-result/pos-test-result.component';
import {TestFileComponent} from './components/test-file/test-file.component';
import {SharedModule} from '../../shared/shared.module';
import {AgGridModule} from 'ag-grid-angular';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatIconModule,
  MatToolbarModule
} from '@angular/material';
import {PosDictionaryService} from './services/pos-dictionaries/pos-dictionary.service';
import {TestFileService} from './services/test-file/test-file.service';
import {PosTestResultService} from './services/pos-test-result/pos-test-result.service';

@NgModule({
  declarations: [
    PosDictionariesComponent,
    PosDictionaryDetailComponent,
    PosTestComponent,
    PosTestResultComponent,
    TestFileComponent
  ],
  imports: [
    CommonModule,
    DictNlp3PosRoutingModule,
    SharedModule,
    AgGridModule.withComponents([]),
    MatChipsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCheckboxModule,
    MatCardModule
  ],
  exports: [
    PosDictionariesComponent,
    PosDictionaryDetailComponent,
    PosTestComponent,
    PosTestResultComponent,
    TestFileComponent
  ],
  providers: [
    PosDictionaryService,
    PosTestResultService,
    TestFileService
  ]
})
export class DictNlp3PosModule {
}
