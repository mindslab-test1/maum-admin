import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PosDictionariesComponent} from './components/pos-dictionaries/pos-dictionaries.component';
import {PosDictionaryDetailComponent} from './components/pos-dictionary-detail/pos-dictionary-detail.component';
import {PosTestComponent} from './components/pos-test/pos-test.component';
import {PosTestResultComponent} from './components/pos-test-result/pos-test-result.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: PosDictionariesComponent,
      },
      {
        path: ':id',
        component: PosDictionaryDetailComponent
      },
      {
        path: ':id/test',
        component: PosTestComponent
      },
      // {
      //   path: ':id/test-result',
      //   component: PosTestResultComponent
      // }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictNlp3PosRoutingModule {
}
