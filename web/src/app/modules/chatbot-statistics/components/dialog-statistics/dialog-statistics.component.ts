import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';
import {ExcelService, getErrorString, TableComponent} from '../../../../shared';
import {StatisticsApi} from '../../../../core/sdk';


import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-dialog-statistics',
  templateUrl: './dialog-statistics.component.html',
  styleUrls: ['./dialog-statistics.component.scss'],
})


export class DialogStatisticsComponent implements OnInit, OnDestroy {
  page_title: any; // = 'Dialog Agents';
  page_description: any; // = 'Add Dialog Agent Managers and update information about Dialog Agent Managers.';

  frequencies = ['Daily', 'Weekly', 'Monthly'];
  items = ['User', 'Session', 'Talk Count', 'Chatbot', 'Skill', 'Success/Fail'];

  connectorType: string;
  selectedItem: string;
  selectedFrequency: string;
  filterKeyword: FormControl;
  panelToggleText: string;
  row: any;
  header;
  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;

  constructor(private store: Store<any>,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private excelService: ExcelService,
              private statisticsApi: StatisticsApi) {
  }

  ngOnInit() {
    this.statisticsApi.assignConnector().subscribe(
      type => {
        this.connectorType = type;
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });

    // Search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  analyseChatbot() {
    let querys = {
      mysql: `select chatbot, count(chatbot) as 'accessed', sum(talkCount) as 'talkCount' from Session group by chatbot`,
      oracle: `select chatbot, count(chatbot) as 'accessed', sum(talkCount) as 'talkCount' from Session group by chatbot`,
      default: `select chatbot, count(chatbot) as 'accessed', sum(talkCount) as 'talkCount' from Session group by chatbot`,
    };
    let query = querys[this.connectorType] ? querys[this.connectorType] : querys.default;

    this.statisticsApi.executeQuery({Query: query}).subscribe(
      rs => {
        this.header = [
          {attr: 'chatbot', name: 'Chatbot', isSort: true, searchable: true},
          {attr: 'accessed', name: 'Accessed', isSort: true},
          {attr: 'talkCount', name: 'Talk Count', isSort: true},
        ];
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      });
  }

  analyseSkill() {
    let querys = {
      mysql: `select skill, count(skill) as 'count' from Talk group by skill`,
      default: `select clResult as 'result', count(clResult) as 'count' from Talk group by clResult`,
    };
    let query = querys[this.connectorType] ? querys[this.connectorType] : querys.default;

    this.statisticsApi.executeQuery({Query: query}).subscribe(
      rs => {
        this.header = [
          {attr: 'skill', name: 'Skill', isSort: true, searchable: true},
          {attr: 'count', name: 'Count', isSort: true},
        ];
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      });
  }

  analyseSuccessFail() {
    let querys = {
      mysql: `select clResult as 'result', count(clResult) as 'count' from Talk group by clResult`,
      default: `select clResult as 'result', count(clResult) as 'count' from Talk group by clResult`,
    };
    let query = querys[this.connectorType] ? querys[this.connectorType] : querys.default;

    this.statisticsApi.executeQuery({Query: query}).subscribe(
      rs => {
        rs.forEach(result => result.result = result.result === 1 ? 'Success' : 'Fail');
        this.header = [
          {attr: 'result', name: 'Success/Fail', isSort: true, searchable: true},
          {attr: 'count', name: 'Count', isSort: true},
        ];
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      });
  }


  doAnalyse() {
    console.log(this.selectedItem, this.selectedFrequency);

    if (this.selectedItem === undefined) {
      this.snackBar.open('Please select a item.', 'Confirm', {duration: 10000});
      return;
    }
    if (this.selectedFrequency === undefined) {
      this.snackBar.open('Please select a frequency.', 'Confirm', {duration: 10000});
      return;
    }

    if (this.selectedItem === 'Chatbot') {
      this.analyseChatbot();
    } else if (this.selectedItem === 'Skill') {
      this.analyseSkill();
    } else if (this.selectedItem === 'Success/Fail') {
      this.analyseSuccessFail();
    } else {
      this.snackBar.open('Underconstruction!', 'Confirm', {duration: 10000});
    }
  }

  download() {
    if (this.rows.length > 0) {
      let name = (this.selectedItem + '-' + this.selectedFrequency + '-').replace('/', '_');
      this.excelService.exportAsExcelFile(this.rows, name, true);
    } else {
      this.snackBar.open('No Data to download!', 'Confirm', {duration: 3000});
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
