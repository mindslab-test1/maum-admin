import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ChannelComponent} from './components/channel/channel.component';
import {DialogStatisticsComponent} from './components/dialog-statistics/dialog-statistics.component';
import {ResponseComponent} from './components/response/response.component';

const routes: Routes = [
  {
    path: 'channels',
    component: ChannelComponent,
  },
  {
    path: 'dialog-stat',
    component: DialogStatisticsComponent,
  },
  {
    path: 'response',
    component: ResponseComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatbotStatisticsRoutingModule {
}
