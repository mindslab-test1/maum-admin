import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';

import {ChatbotStatisticsRoutingModule} from './chatbot-statistics-routing.module';
import {DialogStatisticsComponent} from './components/dialog-statistics/dialog-statistics.component';
import {ChannelComponent} from './components/channel/channel.component';
import {EngineResultComponent} from './components/engine-result/engine-result.component';
import {ResponseComponent} from './components/response/response.component';
import {
  MatDatepickerModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatSidenavModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  declarations: [
    DialogStatisticsComponent,
    ChannelComponent,
    EngineResultComponent,
    ResponseComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ChatbotStatisticsRoutingModule,
    MatSidenavModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatTabsModule
  ]
})
export class ChatbotStatisticsModule {
}
