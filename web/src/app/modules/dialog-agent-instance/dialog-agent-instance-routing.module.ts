import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DaiSvcsComponent} from './components/dai-svcs/dai-svcs.component';
import {DaiSvcUpsertComponent} from './components/dai-svc-upsert/dai-svc-upsert.component';
import {DaiSvcDetailComponent} from './components/dai-svc-detail/dai-svc-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: DaiSvcsComponent,
      },
      {
        path: 'new',
        component: DaiSvcUpsertComponent,
      },
      {
        path: ':id/edit',
        component: DaiSvcUpsertComponent
      },
      {
        path: ':id',
        component: DaiSvcDetailComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DialogAgentInstanceRoutingModule {
}
