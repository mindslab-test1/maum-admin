import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';

import {ROUTER_LOADED, DAM_DELETE} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {AppObjectManagerService, AlertComponent, getErrorString} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {DaiSvcDetailTableComponent} from '../dai-svc-detail-table/dai-svc-detail-table.component';
import {ConfirmComponent} from 'app/shared';

@Component({
  selector: 'app-dai-svc-detail',
  templateUrl: './dai-svc-detail.component.html',
  styleUrls: ['./dai-svc-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DaiSvcDetailComponent implements OnInit, AfterViewInit {
  dam: any;
  actions: any;
  table: any;
  subscription = new Subscription();
  damName: any;

  @ViewChild('damDetailTableComponent') damDetailTableComponent: DaiSvcDetailTableComponent;

  constructor(private store: Store<any>,
              private objectManager: AppObjectManagerService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    this.dam = this.objectManager.get('dam');

    // url로 바로 접근한 경우 임시값 설정(이후 다시 조회)
    if (this.dam === undefined) {
      this.setDefalutDamInfo();
    }

    let id = 'dam';
    // #@ 2018.12.06 SDKToken이 사라지는 이슈 때문에 임시 주석처리
    let roles = this.consoleUserApi.getCachedCurrent().roles;
    // let roles = '{admin: true}';

    // 1.Edit 화면 상단 버튼 정의
    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      activate: {
        icon: this.dam.active ? 'stop' : 'play_arrow',
        text: this.dam.active ? 'Deactivate' : 'Activate',
        callback: this.activate,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      },
      restart: {
        icon: 'autorenew',
        text: 'Restart',
        callback: this.restart,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      },
      // disable: {
      //   icon: 'sync_disabled',
      //   text: 'Disable',
      //   callback: this.disable,
      //   hidden: true
      // }
    };

    // 2.Dialog Agents & DA Instances table 정의
    this.table = {
      header: [
        {attr: 'da_info.name', name: 'Dialog Agent', sortable: true, searchable: true, width: 200},
        {attr: 'daa_info.active', name: 'Active', sortable: true, asIcon: true, width: 80},
        // {attr: 'da_info.registeredAt', name: 'Registered At', sortable: true, searchable: true, width: 120},
        {
          attr: 'chatbot_name',
          name: 'Chatbot',
          inArray: 'chatbot_dai_list',
          searchable: true,
          width: 180
        },
        {attr: 'dai_name', name: 'DA Instances', inArray: 'chatbot_dai_list', width: 180},
        {attr: 'state', name: 'State', inArray: 'chatbot_dai_list', width: 80},
        {attr: 'summary', name: 'Summary', inArray: 'chatbot_dai_list', width: 120},
      ],
      rows: [],
    };

    // URL로 바로 접근한 경우
    let promise = new Promise((resolve, reject) => {
      if (this.dam.name === undefined) {
        // objectManager 로 넘어온 dam 객체가 없는 경우
        this.route.params.subscribe(par => {
          this.damName = par['id'];
          this.dam.name = this.damName;
        });
        this.getDialogAgentManagerInfo().then(() => resolve()).catch(() => reject());
      } else {
        resolve();
      }
    });
    promise.then(() => {  // promise의 결과가 정상인 경우
      // 해당 dam의 DAI 목록 조회
      this.grpc.getDialogAgentWithDialogAgentInstanceList(this.dam.name).subscribe(
        result => {
          this.table.rows = result.da_with_dai_info;
          this.store.dispatch({type: ROUTER_LOADED});
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.store.dispatch({type: ROUTER_LOADED});
        });
    });
    promise.catch(() => { // promise의 결과가 잘못된 경우
      let message = `Cannot find [${this.damName}] this damName`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.backPage();
      return;
    });
  }

  ngAfterViewInit(): void {
    this.store.dispatch({type: ROUTER_LOADED});
  }

  edit: any = () => {
    // this.router.navigateByUrl('m2u/dialog-service/dam-upsert#edit');
    // #@ dam edit 화면으로 이동합니다
    this.router.navigate(['m2u-builder', 'dialog-svcs', this.dam.name, 'edit']);
  };

  delete: any = () => {
    const ref = this.dialog.open(ConfirmComponent);

    ref.componentInstance.message = `Delete '${this.dam.name}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      this.grpc.deleteDialogAgentManager([{name: this.dam.name}]).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.backPage();
          this.snackBar.open(`Dialog Agent Manager '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Delete Dialog Agent Manager '${this.dam.name}'.\n[${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  activate: any = () => {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${this.dam.active ? 'Deactivate' : 'Activate'} '${this.dam.name}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      if (this.dam.active) {
        this.grpc.stopDialogAgentManager([{name: this.dam.name}]).subscribe(
          res => {
            this.dam.active = false;
            this.setActivateIcons();
            let updateItem = res.result_list.filter(item => item.result).map(item => item.name);
            this.snackBar.open(`Dialog Agent Manager(s) '${updateItem}' Deactivated.`, 'Confirm', {duration: 3000});
          },
          err => {
            let message = `Failed to Deactivate Dialog Agent Manager(s). [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      } else {
        this.grpc.startDialogAgentManager([{name: this.dam.name}]).subscribe(
          res => {
            this.dam.active = true;
            this.setActivateIcons();
            let updateItem = res.result_list.filter(item => item.result).map(item => item.name);
            this.snackBar.open(`Dialog Agent Manager(s) '${updateItem}' Activated.`, 'Confirm', {duration: 3000});
          },
          err => {
            let message = `Failed to Activate Dialog Agent Manager(s). [${getErrorString(err)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      }

    });
  };

  restart: any = () => {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${'Restart'} '${this.dam.name}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.grpc.restartDialogAgentManager([{name: this.dam.name}]).subscribe(
        res => {
          this.dam.active = true;
          this.setActivateIcons();
          let updateItem = res.result_list.filter(item => item.result).map(item => item.name);
          this.snackBar.open(`Dialog Agent Manager(s) '${updateItem}' Stopped.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Stop Dialog Agent Manager(s). [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  setActivateIcons() {
    if (this.dam.active) {
      this.actions.activate.text = 'Deactivate';
      this.actions.activate.icon = 'stop';
    } else {
      this.actions.activate.text = 'Activate';
      this.actions.activate.icon = 'play_arrow';
    }
  }

  getIcon() {
    if (this.dam.active) {
      return 'check_circle';
    } else {
      return 'block';
    }
  }

  backPage: any = () => {
    this.objectManager.clean('dam');
    // this.router.navigate(['../dam'], {relativeTo: this.route});

    // #@ 삭제가 완료 되면 이전으로 이동합니다
    this.router.navigate(['../'], {relativeTo: this.route});
  };

  /**
   * objectManager에 dam이 없는 경우 조회를 합니다
   * 없는 dam을 조회한 결과, res.name='' 로 결과가 전달되어
   * res.name==='' 로 비교하여 예외처리 추가
   */
  getDialogAgentManagerInfo() {
    return new Promise((resolve, reject) => {
      this.grpc.getDialogAgentManagerInfo(this.damName).subscribe(res => {
        if (res) {
          this.dam = res;
          this.objectManager.set('dam', this.dam);
          if (this.dam.name === '') { // 조회 결과가 비정상인 경우
            reject();
          }
          this.actions.activate.icon = this.dam.active ? 'stop' : 'play_arrow';
          this.actions.activate.text = this.dam.active ? 'Deactivate' : 'Activate';
          resolve();
        } else {
          this.backPage();
          reject();
          return;
        }
      }, err => {
        let message = 'Fail to get dam info';
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        reject();
      });
    });
  }

  /**
   * url로 바로 접근한 경우 임시로 값을 넣어 줍니다
   * 해당 함수를 호출하지 않으면 html에서 [(ngModel)]="dam.description" 접근시 오류가 발생합니다
   * Dam의 name이 undefined 이므로 이후 grpc를 통해 dam info를 받아 옵니다
   */
  setDefalutDamInfo() {
    let tempDam = {
      name: undefined,
      ip: '',
      port: '',
      description: '',
      active: false,
      default: false
    };
    this.dam = tempDam;
  }
}
