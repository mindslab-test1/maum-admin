import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaiSvcDetailComponent} from './dai-svc-detail.component';

describe('DaiSvcDetailComponent', () => {
  let component: DaiSvcDetailComponent;
  let fixture: ComponentFixture<DaiSvcDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaiSvcDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaiSvcDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
