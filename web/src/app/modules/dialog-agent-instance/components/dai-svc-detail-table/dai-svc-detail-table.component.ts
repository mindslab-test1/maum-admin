import {Component, Input, ViewChild, ViewEncapsulation} from '@angular/core';
import {PaginationControlsComponent, PaginatePipe, PaginationService} from 'ngx-pagination';
import {TableComponent} from 'app/shared';

@Component({
  selector: 'app-dialog-agent-manager-detail-table',
  templateUrl: './dai-svc-detail-table.component.html',
  styleUrls: ['./dai-svc-detail-table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DaiSvcDetailTableComponent extends TableComponent {

  // gih2yun: AOT 컴파일 오류 잡은 것임.
  sorter: { col: any, order: boolean } = null;
  tableId: string;
  paginationOff: number;
  fetching = false;

  // AOT

  getStateIcon(row, col, instance) {
    return instance.state === '0' ? 'cached' : 'block';
  }
}
