import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaiSvcDetailTableComponent} from './dai-svc-detail-table.component';

describe('DaiSvcDetailTableComponent', () => {
  let component: DaiSvcDetailTableComponent;
  let fixture: ComponentFixture<DaiSvcDetailTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaiSvcDetailTableComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaiSvcDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
