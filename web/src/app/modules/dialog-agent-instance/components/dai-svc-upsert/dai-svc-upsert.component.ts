import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {
  MatDialog,
  MatSnackBar,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {Store} from '@ngrx/store';

import {GrpcApi} from '../../../../core/sdk'
import {ROUTER_LOADED} from '../../../../core/actions';
import {
  ErrorDialogComponent,
  AppObjectManagerService,
  AlertComponent,
  FormErrorStateMatcher,
  getErrorString
} from '../../../../shared';
import {IP_REGEX, PORT_REGEX} from '../../../../shared/values/values'
import {ConfirmComponent} from 'app/shared';

@Component({
  selector: 'app-dai-svc-upsert',
  templateUrl: './dai-svc-upsert.component.html',
  styleUrls: ['./dai-svc-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class DaiSvcUpsertComponent implements OnInit {
  nameFormControl = new FormControl(undefined, [
    Validators.required
  ]);
  ipFormControl = new FormControl(undefined, [
    Validators.required,
    Validators.pattern(IP_REGEX)
  ]);
  portFormControl = new FormControl(undefined, [
    Validators.required,
    Validators.pattern(PORT_REGEX)
  ]);

  dialogTitle: string;
  dam = {
    name: undefined,
    ip: undefined,
    port: undefined,
    description: undefined,
    active: false,
    default: false
  };
  org_dam: any;
  role: string;
  submitButton: any;
  damName: any;

  constructor(private store: Store<any>,
              private grpc: GrpcApi,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              public snackBar: MatSnackBar,
              private objectManager: AppObjectManagerService,
              public formErrorStateMatcher: FormErrorStateMatcher) {
  }

  ngOnInit() {

    // 1.objectManager에서 dam정보를 조회합니다
    this.dam = this.objectManager.get('dam');
    // url로 바로 접근한 경우 임시로 값을 넣어 줍니다
    if (this.dam === undefined) {
      this.setDefalutDamInfo();
    }
    // /m2u/dialog-service/dam/default-dam/edit 로 접근할수 있는 방법은 두가지 입니다
    // 1. dam view에서 -> dam edit로 버튼을 클릭해서 이동한 경우
    // 2. 사용자가 직접 url에/m2u/dialog-service/dam/default-dam/edit를 입력하여 들어온 경우
    // 2 번의 경우 objectManager에 dam이 없습니다
    // 이 경우 dam을 획득하기 위한 예외처리는 아래와 같습니다
    let promise = new Promise((resolve, reject) => {
      this.route.params.subscribe(par => {
        let tempRoute = this.route.toString();
        if (tempRoute.indexOf('new') > 0) {
          console.log('#@ dam/new :' + tempRoute);
          this.role = 'add';
        } else if (tempRoute.indexOf('edit') > 0) {
          console.log('#@ /edit :' + tempRoute);
          this.role = 'edit';
          this.damName = par['id']; // url에 명시된 damName을 획득합니다
        } else {  // 잘못된 url 접근
          console.log('#@ There is no dam url');
          let message = `Cannot find this dam url.`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.backPage();
          reject();
        }
      });
      if (this.role === 'edit' && this.dam.name === undefined) {
        // 사용자가 직접 버튼을 클릭하여 넘어왔을 경우
        // 사용자가 url로 edit를 접근했을 경우 grpc 호출을 통해 dam 정보를 받아온다
        this.getDialogAgentManagerInfo().then(() => resolve()).catch(() => reject());
      } else {
        resolve();  // resolve를 오출하여 promise를 완료한다
      }
    });
    promise.catch(() => {
      // 정상적으로 데이터를 갖고 오지 못한경우
      let message = `Cannot find [${this.damName}] this dam`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
      this.backPage();
      return;
    });
    promise.then(() => {
      switch (this.role) {
        case 'add':
          this.submitButton = true;
          this.dam.port = '9907';
          this.dialogTitle = 'Add Dialog Agent Manager';
          break;
        case 'duplicate':
        case 'edit':
          let dam = this.objectManager.get('dam');
          if (dam === undefined) {
            this.backPage();
            return;
          }
          this.dam = dam;
          this.org_dam = JSON.parse(JSON.stringify(this.dam));
          if (this.role === 'edit') {
            this.dialogTitle = 'Edit Dialog Agent Manager';
          } else {
            this.dialogTitle = 'Duplicate Dialog Agent Manager';
          }
          break;
        default:
          this.backPage();
          // let message = 'Role is not properly assigned.';
          // this.snackBar.open(message, 'Confirm', {duration: 10000});
          return;
      }
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    if (this.role === 'edit') {
      if (Object.keys(this.dam).every(key => {
        return this.dam[key] === this.org_dam[key];
      })) {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Not Changed';
        ref.componentInstance.message = `Values are not changed.Please make some modification(s) first.`;
      } else {
        if (this.check()) {
          this.openDialog('edit');
        }
      }
    } else {
      if (this.role === 'duplicate' &&
        (this.dam.name === this.org_dam.name ||
          (this.dam.ip === this.org_dam.ip && this.dam.port === this.org_dam.port))) {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Duplicated';
        ref.componentInstance.message = `Please enter new name and ip:port.`;
      } else {
        if (this.check()) {
          this.openDialog('add');
        }
      }
    }
  }

  onAdd(): void {
    this.dam.port = parseInt(this.dam.port, 10);
    this.grpc.insertDialogAgentManagerInfo(this.dam).subscribe(
      res => {
        this.snackBar.open(`Dialog Agent Manager '${this.dam.name}' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        let message = getErrorString(err);
        if (message === `gRPC:6 ALREADY_EXISTS: IP, port duplicate(6)`) {
          ref.componentInstance.title = 'Duplicated';
          message = 'IP, Port Duplicated.'
        } else if (message === `gRPC:6 ALREADY_EXISTS: name duplicate(6)`) {
          ref.componentInstance.title = 'Duplicated';
          message = 'DAM Name Duplicated.'
        } else if (message === `gRPC:14 UNAVAILABLE: Unavailable DAM server(14)`) {
          ref.componentInstance.title = 'Unavailable';
          message = 'Please check if ' + this.dam.ip + ':' + this.dam.port + ' is available.'
        }
        console.log(message);
        ref.componentInstance.message = `Add Failed. ${message}`;
      });
  }

  onEdit(): void {
    this.dam.port = parseInt(this.dam.port, 10);
    this.grpc.updateDialogAgentManagerInfo(this.dam).subscribe(
      res => {
        this.snackBar.open(`Dialog Agent Manager '${this.dam.name}' updated.`, 'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to update Dialog Agent Manager '${this.dam.name}. [ ${getErrorString(err)}} ]`;
      });
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  openDialog(data) {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = data === 'add' ? `Add '${this.dam.name}?'` : `Save '${this.dam.name}?'`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  check() {
    if (this.isFormError(this.nameFormControl)) {
      this.submitButton = true;
    } else if (this.isFormError(this.ipFormControl)) {
      this.submitButton = true;
    } else if (this.isFormError(this.portFormControl)) {
      this.submitButton = true;
    } else if (this.dam.name === undefined || this.dam.ip === undefined || this.dam.port === undefined) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
      return true;
    }
  }

  backPage: any = () => {
    this.objectManager.clean('dam');
    // this.router.navigate(['../dam'], {relativeTo: this.route});

    // #@ dam 페이지로 이동합니다
    // this.router.navigate(['m2u/dialog-service/dam/'], {relativeTo: this.route});
    // 삭제가 완료 되면 이전으로 이동합니다
    this.router.navigate(['../'], {relativeTo: this.route});
  };

  /**
   * objectManager에 dam이 없는 경우 조회를 합니다
   * 없는 dam을 조회한 결과, res.name='' 로 결과가 전달되어
   * res.name==='' 로 비교하여 예외처리 추가
   */
  getDialogAgentManagerInfo() {
    return new Promise((resolve, reject) => {
      this.grpc.getDialogAgentManagerInfo(this.damName).subscribe(res => {
        if (res) {
          this.dam = res;
          this.objectManager.set('dam', this.dam);
          if (this.dam.name === '') { // 조회 결과가 비정상인 경우
            reject();
          }
          resolve();
        } else {
          this.backPage();
          reject();
          return;
        }
      }, err => {
        let message = 'Fail to get dam info';
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        reject();
      });
    });
  }

  /**
   * url로 바로 접근한 경우 임시로 값을 넣어 줍니다
   * 해당 함수를 호출하지 않으면 html에서 [(ngModel)]="dam.description" 접근시 오류가 발생합니다
   * Dam의 name이 undefined 이므로 이후 grpc를 통해 dam info를 받아 옵니다
   */
  setDefalutDamInfo() {
    let tempDam = {
      name: undefined,
      ip: '',
      port: '',
      description: '',
      active: false,
      default: false
    };
    this.dam = tempDam;
  }
}
