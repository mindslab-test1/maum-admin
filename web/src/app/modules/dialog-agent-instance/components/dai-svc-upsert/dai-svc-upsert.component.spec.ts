import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaiSvcUpsertComponent} from './dai-svc-upsert.component';

describe('DaiSvcUpsertComponent', () => {
  let component: DaiSvcUpsertComponent;
  let fixture: ComponentFixture<DaiSvcUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaiSvcUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaiSvcUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
