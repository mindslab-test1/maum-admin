import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {
  Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSnackBar, MatSidenav, MatPaginator} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED, DAM_UPDATE_ALL, DAM_DELETE} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {
  AppObjectManagerService,
  TableComponent,
  getErrorString
} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';
import {Http} from '@angular/http';


import {ConfirmComponent} from 'app/shared';

@Component({
  selector: 'app-dai-svcs',
  templateUrl: './dai-svcs.component.html',
  styleUrls: ['./dai-svcs.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DaiSvcsComponent implements OnInit, OnDestroy {
  page_title: any; // = 'Manage Dialog Agent Managers';
  page_description: any; // = 'Add Dialog Agent Managers and update information about Dialog Agent Managers.';
  title: any = 'Dialog Agent Manager';

  actions: any;
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;

  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  // @ViewChild('sidenav') sidenav: MatSidenav;
  // @ViewChild('infoPanel') infoPanel: DialogAgentManagerPanelComponent;

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header = [];

  constructor(private store: Store<any>,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi,
              private objectManager: AppObjectManagerService,
              private http: Http) {
  }

  ngOnInit() {
    let id = 'dam';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      }
    };

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {attr: 'name', name: 'Name', isSort: true, onClick: this.navigateDamDetailView},
      {attr: 'ip_port', name: 'IP:PORT', isSort: true},
      {attr: 'active', name: 'Active', isSort: true, asIcon: true},
      {attr: 'da_cnt', name: '# DA', isSort: true},
      {attr: 'dai_cnt', name: '# DA Instances', isSort: true},
      {attr: 'duration', name: 'Duration At', isSort: true},
    ];

    // this.tableComponent.onRowClick = this.fillRowInfo;
    // // subscribe store
    this.subscription.add(
      this.store.select('dams')
      .subscribe(dams => {
        if (dams) {
          // 초기 name 순으로 정렬
          dams.sort((a, b) => {
            return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
          });
          this.rows = dams;
          this.tableComponent.isCheckedAll = false;
          this.whenCheckChanged();
          // this.rows.forEach(row => row.checked = false);
          setTimeout(() => {
            // this.fillRowInfo(this.rows[0]);
          }, this.rows.length > 0 ? 500 : 0);
        }
      })
    );

    this.grpc.getDialogAgentManagerAllList().subscribe(
      result => {
        result.damwd_list.forEach(dam => {
          dam.ip_port = dam.dam_info.ip + ':' + dam.dam_info.port;
          dam.name = dam.dam_info.name;
          dam.active = dam.dam_info.active;
        });
        this.store.dispatch({type: DAM_UPDATE_ALL, payload: result.damwd_list});
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    let role;
    if (this.actions.add.text === 'Duplicate') {
      let item = this.rows.filter(row => row.isChecked)[0].dam_info;
      console.log('test : ', item);
      this.objectManager.set('dam', item);
      role = 'duplicate';

      // #@ edit로 이동합니다
      this.router.navigate([item.name, 'edit'],
        {relativeTo: this.activatedRoute});
    } else {
      this.objectManager.clean('dam');
      role = 'add';

      // #@ new로 이동합니다
      this.router.navigate(['new'],
        {relativeTo: this.activatedRoute});
    }
    // this.router.navigateByUrl('dialog-service/dam-upsert#' + role);
  };

  edit: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }
    this.objectManager.set('dam', items[0].dam_info);
    // this.router.navigateByUrl('dialog-service/dam-upsert#edit');

    // edit로 이동합니다 damName을 함께 전달합니다
    this.router.navigate([items[0].dam_info.name, 'edit'],
      {relativeTo: this.activatedRoute});
  };

  delete: any = () => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.dam_info.name);
    if (names.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      let keyList = names.map(key => {
        return {name: key};
      });
      this.grpc.deleteDialogAgentManager(keyList).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.store.dispatch({type: DAM_DELETE, payload: deletedItems});
          this.snackBar.open(`Dialog Agent Manager '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Delete Dialog Agent Manager '${names}'. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  // togglePanel: any = () => {
  //   this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
  //   this.sidenav.toggle();
  // };

  // fillRowInfo: any = (row: any) => {
  //   this.infoPanel.dam = row;
  //   if (row === undefined) {
  //     this.sidenav.close();
  //     this.panelToggleText = 'Show Info Panel';
  //   } else {
  //     this.sidenav.open();
  //     this.panelToggleText = 'Hide Info Panel';
  //   }
  // };

  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.add.text = 'Add';
        this.actions.add.icon = 'add_circle_outline';
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.add.text = 'Duplicate';
        this.actions.add.icon = 'control_point_duplicate';
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.add.text = 'Add';
        this.actions.add.icon = 'add_circle_outline';
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        break;
    }
  };

  navigateDamDetailView: any = (row: any) => {
    this.objectManager.set('dam', row.dam_info);
    // this.router.navigateByUrl('dialog-service/dam-detail');

    // #@ 해당 dam의 detail로 이동합니다
    this.router.navigate([row.dam_info.name],
      {relativeTo: this.activatedRoute});
  };


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
