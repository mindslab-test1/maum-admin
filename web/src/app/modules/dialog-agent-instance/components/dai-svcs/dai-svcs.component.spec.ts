import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DaiSvcsComponent} from './dai-svcs.component';

describe('DaiSvcsComponent', () => {
  let component: DaiSvcsComponent;
  let fixture: ComponentFixture<DaiSvcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DaiSvcsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaiSvcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
