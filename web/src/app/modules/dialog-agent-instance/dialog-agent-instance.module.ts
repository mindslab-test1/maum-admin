import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DialogAgentInstanceRoutingModule} from './dialog-agent-instance-routing.module';
import {DaiSvcsComponent} from './components/dai-svcs/dai-svcs.component';
import {DaiSvcDetailComponent} from './components/dai-svc-detail/dai-svc-detail.component';
import {DaiSvcUpsertComponent} from './components/dai-svc-upsert/dai-svc-upsert.component';
import {DaiSvcDetailTableComponent} from './components/dai-svc-detail-table/dai-svc-detail-table.component';
import {SharedModule} from '../../shared/shared.module';
import {
  MatButtonModule, MatCardModule, MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule
} from '@angular/material';

@NgModule({
  declarations: [
    DaiSvcsComponent,
    DaiSvcDetailComponent,
    DaiSvcUpsertComponent,
    DaiSvcDetailTableComponent],
  imports: [
    CommonModule,
    DialogAgentInstanceRoutingModule,
    SharedModule,
    MatInputModule,
    MatSidenavModule,
    MatSelectModule,
    MatListModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule
  ]
})
export class DialogAgentInstanceModule {
}
