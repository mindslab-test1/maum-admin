import {AfterViewInit, Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class TableComponent implements OnInit, AfterViewInit {

  @Input() headers: any[] = [];
  @Input() rows: any[] = [];
  @Input() dataSource: MatTableDataSource<any>;
  @Input() filterPlaceholder: any;
  @Input() isFilter: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isCheckedAll: boolean;

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.rows);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  getColumn(headers) {
    let arr: any[] = [];
    headers.filter(col => col.attr).forEach((col) => {
      arr.push(col.attr);
    });
    return arr;
  }

  onClickCheckAll() {
    /* 페이징 적용했을 때 사용하는 부분 */
    let pageIndex: number = this.paginator.pageIndex;
    let pageSize: number = this.paginator.pageSize;

    let start: number = pageIndex * pageSize;
    let end: number = (pageIndex + 1) * pageSize - 1;

    if (this.paginator.length <= end) {
      end = this.paginator.length - 1;
    }

    /*공통 적용부분*/
    for (let i = start; i < end + 1; i++) {
      this.rows[i].isChecked = this.isCheckedAll;
    }
  }

  colClick(row, col) {
    if (col.onClick) {
      col.onClick(row);
    }
  }

  getButtonClass(col) {
    return col.buttonColor === 'accent' ? 'accent-button' : 'primary-button';
  }
}
