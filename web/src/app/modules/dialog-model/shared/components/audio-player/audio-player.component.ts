import WaveSurfer from 'wavesurfer.js';
import TimelinePlugin from 'wavesurfer.js/dist/plugin/wavesurfer.timeline.min.js';

import {
  Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AudioPlayerComponent implements OnInit, OnChanges, OnDestroy {

  @Input() url: string = undefined;

  waveSurfer: any;
  volume: any;
  slider = {
    min: 0,
    max: 1,
    step: 0.1
  };

  ngOnInit() {
    this.waveSurfer = WaveSurfer.create({
      container: '#waveSurfer',
      waveColor: '#D2EDD4',
      progressColor: '#46B54D',
      plugins: [
        TimelinePlugin.create({
          wavesurfer: this.waveSurfer,
          container: '#waveSurfer-timeline',
        })
      ]
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.url.currentValue) {
      this.waveSurfer['volume'] = 0.5;
      this.waveSurfer.setVolume(0.5);
      this.setPlaybackRate(1.0);
      this.waveSurfer.load(this.url);
    }
  }

  setPlaybackRate(rate: number) {
    if (rate > 0 && rate < 3) {
      this.waveSurfer.setPlaybackRate(rate);
      this.waveSurfer.playbackRate = rate;
    }
  }

  setVolume(volume: number) {
    if (volume >= 0 && volume <= 1) {
      this.waveSurfer.setVolume(volume);
      this.waveSurfer.volume = volume;
    }
  }

  ngOnDestroy() {
    if (this.waveSurfer) {
      this.waveSurfer.destroy();
      this.waveSurfer = null;
    }
  }
}
