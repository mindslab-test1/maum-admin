import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-popup-error',
  templateUrl: './popup.error.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupErrorComponent implements OnInit {

  @Input() title: string = undefined;
  @Input() message: string = undefined;

  constructor() {
  }

  ngOnInit() {
  }
}
