import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-popup-script',
  templateUrl: './popup.script.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class PopupScriptComponent implements OnInit {
  public btnScript = '';

  constructor() {

  }

  ngOnInit() {

  }

  scriptOpen() {
    $('.popup-script-guide').css('display', 'inline-block');
    this.btnScript = 'primary';
  }
}
