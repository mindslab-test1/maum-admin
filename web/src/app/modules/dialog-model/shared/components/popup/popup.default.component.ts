import {Component, OnInit, ViewEncapsulation, Input, Inject} from '@angular/core';
import {MatDialog} from '@angular/material';
import {PopupTableComponent} from './popup.table.component';

@Component({
  selector: 'app-popup-default',
  templateUrl: './popup.default.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupDefaultComponent implements OnInit {
  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  selectModel() {
    this.dialog.open(PopupTableComponent, {
      maxWidth: '70%',
      maxHeight: '50%',
      minWidth: '40%',
    });
  }
}
