import {
  Component,
  EventEmitter,
  Injectable,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogModel, DialogService} from '../../../service/dialog.service';
import {PopupErrorComponent} from './popup.error.component';
import {Router} from '@angular/router';
import {EmitterService} from '../../../service/emitter.service';

@Component({
  selector: 'app-popup-delete',
  templateUrl: './popup.delete.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupDeleteComponent implements OnInit {

  dialogModel: DialogModel;

  @Input() domain: string = undefined;
  @Input() message: string = undefined;

  constructor(private router: Router,
              private dialogService: DialogService,
              private emitterService: EmitterService,
              public dialog: MatDialog) {
    this.dialogModel = JSON.parse(localStorage.getItem('selectedDialogModel')) as DialogModel;
  }

  ngOnInit() {

  }

  onDelete(domain: string) {
    if (domain === 'Model') {
      this.deleteModel();
    } else if (domain === 'Entity') {

    } else if (domain === 'Intent') {
      this.emitterService.emitter(domain, 'ok');
    }
  }

  deleteModel() {
    this.dialogService.deleteDialogModel(this.dialogModel.workspaceId, this.dialogModel.id)
    .subscribe(response => {
      if (response && response.statusCode === 200) {
        localStorage.setItem('currentWorkspaceId', '1');
        this.dialogService.getDialogModelList(localStorage.getItem('currentWorkspaceId'))
        .subscribe(mResponse => {
          if (mResponse && mResponse.statusCode === 200) {
            let dialogModelList = mResponse.data.modelInfo;
            if (!dialogModelList) {
              dialogModelList = [];
            }
            localStorage.setItem('dialogModelList', JSON.stringify(dialogModelList));
            localStorage.setItem('currentDialogModel', JSON.stringify(dialogModelList[0]));
          }
          this.dialog.closeAll();
          this.router.navigate(['mlt', 'sds', 'all-model']).catch();
        }, error => {
          this.dialog.closeAll();
          this.dialog.open(PopupErrorComponent, {
            minWidth: '45%',
            disableClose: true,
          });
        });
      }
    }, error => {
      this.dialog.closeAll();
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    });
  }
}
