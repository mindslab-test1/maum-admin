import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-popup-log',
  templateUrl: './popup.log.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupLogComponent {

  @Input() errorlog: string = undefined;

  constructor() {
  }

  downloadLog() {
    const BOM = '%EF%BB%BF';
    const regex = /<br\s*[\/]?>/gi;
    const dataUri = 'data:text/plain;charset=UTF-8,' + BOM + encodeURIComponent(this.errorlog.replace(regex, '\n'));

    const date = new Date();
    const year = date.getFullYear();
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();

    if (month.length === 1) {
      month = '0' + month;
    }
    if (day.length === 1) {
      day = '0' + day;
    }

    const exportFileDefaultName = 'ErrorLog_' + year + month + day + '.log';

    const linkElement = document.createElement('a');
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName);
    linkElement.click();
  }
}
