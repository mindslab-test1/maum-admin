import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-popup-warning',
  templateUrl: './popup.warning.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupWarningComponent implements OnInit {
  constructor() {

  }

  ngOnInit() {
  }
}
