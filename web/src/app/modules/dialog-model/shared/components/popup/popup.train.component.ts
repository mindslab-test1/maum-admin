import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {DialogModel, DialogService} from '../../../service/dialog.service';
import {PopupErrorComponent} from './popup.error.component';
import {MatDialog} from '@angular/material';
import {EmitterService} from '../../../service/emitter.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-popup-train',
  templateUrl: './popup.train.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupTrainComponent implements OnInit {
  modelId: string;
  dialogModel: DialogModel;

  @Input() domain: string = undefined;
  @Input() message: string = undefined;

  constructor(public dialog: MatDialog,
              private emitterService: EmitterService,
              public dialogService: DialogService) {

    this.dialogModel = JSON.parse(localStorage.getItem('currentDialogModel')) as DialogModel;
  }

  ngOnInit() {
    $('.train-text i').mouseenter(function () {
      $(event.currentTarget).next('div').css('display', 'block');
    }).mouseleave(function () {
      $(event.currentTarget).next('div').css('display', 'none');
    });
  }

  clickTrain() {
    const dialogModel: DialogModel = JSON.parse(localStorage.getItem('currentDialogModel')) as DialogModel;
    this.modelId = dialogModel.id;

    localStorage.removeItem('trainId');
    this.dialogService.startTraining(this.modelId).subscribe(result => {
      if (result && result.statusCode === 200) {
        if (result.data.hasOwnProperty('error')) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '45%',
            disableClose: true,
          });
          ref.componentInstance.title = '에러';
          ref.componentInstance.message = result.data.error;
        } else {
          localStorage.setItem('trainId', result.data.resultInfo.trainId);
          this.dialogModel.lastTrainKey = result.data.resultInfo.trainId;
          this.dialogService.updateDialogModelInfo(this.dialogModel).subscribe(res => {
            if (res && res.statusCode === 200) {
              localStorage.setItem('currentDialogModel', JSON.stringify(this.dialogModel));
              this.emitterService.emitter('training', 'ok');
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }
}
