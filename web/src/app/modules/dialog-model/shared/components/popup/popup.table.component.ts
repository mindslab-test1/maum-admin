import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialogRef} from '@angular/material';

export interface Element {
  name: string;
  id: string;
  created: string;
}

const ELEMENT_DATA: Element[] = [
  {name: 'Hydrogenaaaaadf', id: 'V', created: '0000-00-00 00:00'},
  {name: 'Helium', id: 'HE', created: '0000-00-00 00:00'},
  {name: 'Lithium', id: 'LI', created: '0000-00-00 00:00'},
  {name: 'Beryllium', id: 'BE', created: '0000-00-00 00:00'},
  {name: 'Boron', id: 'B', created: '0000-00-00 00:00'},
  {name: 'Carbon', id: 'C', created: '0000-00-00 00:00'},
  {name: 'Nitrogen', id: 'Q', created: '0000-00-00 00:00'},
  {name: 'Oxygen', id: 'O', created: '0000-00-00 00:00'},
  {name: 'Fluorine', id: 'F', created: '0000-00-00 00:00'},
  {name: 'Neon', id: 'NE', created: '0000-00-00 00:00'},
  {name: 'Sodium', id: 'NA', created: '0000-00-00 00:00'},
  {name: 'Magnesium', id: 'MS', created: '0000-00-00 00:00'},
  {name: 'Aluminum', id: 'ID', created: '0000-00-00 00:00'},
  {name: 'Silicon', id: 'AL', created: '0000-00-00 00:00'},
  {name: 'Phosphorus', id: 'S', created: '0000-00-00 00:00'},
  {name: 'Sulfur', id: 'P', created: '0000-00-00 00:00'},
  {name: 'Chlorine', id: 'Cl', created: '0000-00-00 00:00'},
  {name: 'Argon', id: 'Ar', created: '0000-00-00 00:00'},
  {name: 'Potassium', id: 'K', created: '0000-00-00 00:00'},
  {name: 'Calcium', id: 'Ca', created: '0000-00-00 00:00'},
];

@Component({
  selector: 'app-popup-table',
  templateUrl: './popup.table.component.html',
  styleUrls: ['./popup.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PopupTableComponent implements OnInit {
  displayedColumns = ['radio', 'name', 'id', 'created', 'setting', 'download', 'remove'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  selection = new SelectionModel<Element>(true, []);

  constructor(private router: Router,
              public dialogRef: MatDialogRef<PopupTableComponent>) {

  }

  ngOnInit() {

  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  createAdd() {
    this.dialogRef.close();
    this.router.navigate(['mlt', 'create']).catch();
  }

  modelSetting() {
    this.dialogRef.close();
    this.router.navigate(['mlt', 'setting']).catch();
  }
}
