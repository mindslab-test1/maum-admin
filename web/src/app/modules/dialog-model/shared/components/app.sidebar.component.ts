import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app.sidebar.component.html',
  styleUrls: ['./app.sidebar.component.scss']
})
export class AppSidebarComponent implements OnInit {

  @Input() sidebarChild: any[] = [];
  @Input() parentPath = '';
  @Input() dept = 0;
  @Input() currentPath = '';


  constructor(private router: Router) {

  }

  ngOnInit() {
  }

  click(nav, $event) {
    if (!nav.children) {
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(() => {
        this.router.navigate([this.parentPath, nav.path]).catch();
      });
    }
    $event.preventDefault();
  }
}
