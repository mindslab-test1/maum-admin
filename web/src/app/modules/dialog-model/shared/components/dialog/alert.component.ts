import {Component, OnInit, Input} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input() title: string = null;
  @Input() message: string = null;

  constructor(public dialogRef: MatDialogRef<any>) {
  }

  ngOnInit() {
  }

}
