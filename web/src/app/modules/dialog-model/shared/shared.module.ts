import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatTableModule,
  MatSortModule,
  MatCommonModule,
  MatSidenavModule,
  MatMenuModule,
  MatSnackBarModule,
  MatDialogModule,
  MatLineModule,
  MatToolbarModule,
  MatSelectModule,
  MatCardModule,
  MatRippleModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatButtonToggleModule,
  MatChipsModule,
  MatSliderModule,
  MatTreeModule,
} from '@angular/material';

import {FlexLayoutModule} from '@angular/flex-layout';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppSidebarComponent} from './components/app.sidebar.component';
import {AppDummyComponent} from './components/app.dummy.component';
import {ErrorDialogComponent} from './components/dialog/error-dialog.component';
import {FormErrorStateMatcher} from './values/form.error.state.matcher';
import {AlertComponent} from './components/dialog/alert.component';
import {TableComponent} from './components/table/table.component';
import {ClipboardService} from './services/clipboard.service';
import {AudioPlayerComponent} from './components/audio-player/audio-player.component';
import {TranscriptTextComponent} from './components/transcript-text/transcript.text.component';
import {
  PopupDefaultComponent,
  PopupTableComponent,
  PopupTrainComponent,
  PopupWarningComponent,
  PopupScriptComponent,
  PopupLogComponent,
  PopupDeleteComponent,
  PopupErrorComponent
} from './components/popup';

@NgModule({
  imports: [
    MatButtonModule, MatCheckboxModule, MatRadioModule, MatAutocompleteModule, MatIconModule,
    MatInputModule, MatListModule, MatTableModule, MatSortModule, MatCommonModule, MatSidenavModule, MatMenuModule,
    MatSnackBarModule, MatDialogModule, MatLineModule, MatToolbarModule, MatSelectModule, MatCardModule, MatRippleModule,
    MatPaginatorModule, MatSlideToggleModule, MatTabsModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule,
    MatProgressBarModule, MatProgressSpinnerModule, MatButtonToggleModule, MatChipsModule, MatSliderModule,
    FlexLayoutModule, CommonModule, FormsModule, HttpModule, ReactiveFormsModule, MatTreeModule
  ],
  declarations: [
    AppSidebarComponent, AppDummyComponent, ErrorDialogComponent, AlertComponent, TableComponent, AudioPlayerComponent,
    TranscriptTextComponent, PopupDefaultComponent, PopupTableComponent, PopupTrainComponent, PopupWarningComponent,
    PopupScriptComponent, PopupLogComponent, PopupDeleteComponent, PopupErrorComponent
  ],
  providers: [
    FormErrorStateMatcher, ClipboardService
  ],
  exports: [
    MatButtonModule, MatCheckboxModule, MatRadioModule, MatAutocompleteModule, MatIconModule,
    MatInputModule, MatListModule, MatTableModule, MatSortModule, MatCommonModule, MatSidenavModule, MatMenuModule,
    MatSnackBarModule, MatDialogModule, MatLineModule, MatToolbarModule, MatSelectModule, MatTreeModule, MatCardModule, MatRippleModule,
    MatPaginatorModule, MatSlideToggleModule, MatTabsModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule,
    MatProgressBarModule, MatProgressSpinnerModule, MatButtonToggleModule, MatChipsModule, MatSliderModule,
    FlexLayoutModule, CommonModule, FormsModule, HttpModule, ReactiveFormsModule,
    AppSidebarComponent, AppDummyComponent, ErrorDialogComponent, AlertComponent, TableComponent, AudioPlayerComponent,
    TranscriptTextComponent, PopupDefaultComponent, PopupTableComponent, PopupTrainComponent, PopupWarningComponent,
    PopupScriptComponent, PopupLogComponent, PopupDeleteComponent, PopupErrorComponent
  ],
  entryComponents: [
    ErrorDialogComponent, AlertComponent, PopupDefaultComponent, PopupTableComponent, PopupTrainComponent, PopupWarningComponent,
    PopupScriptComponent, PopupLogComponent, PopupDeleteComponent, PopupErrorComponent
  ]
})
export class SharedModuleSDS {
}


