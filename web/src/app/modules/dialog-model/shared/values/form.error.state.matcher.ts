import {FormGroupDirective, NgForm, FormControl} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

export class FormErrorStateMatcher implements ErrorStateMatcher {
  public isErrorState(control: FormControl): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }

  public isCategoryErrorState(control: FormControl): boolean {
    return !!(control.invalid);
  }
}
