import {Pipe, PipeTransform} from '@angular/core';
import {DialogUtterData} from '../service/dialog.service';

@Pipe({
  name: 'mappedUserSay'
})
export class DialogIntentSlotMappedUserSayPipe implements PipeTransform {
  transform(value: DialogUtterData[], _sayString: string): Array<{ id: string, text: string, isCorpus: boolean, slot: string }> {
    const ret: Array<{ id: string, text: string, isCorpus: boolean, slot: string }> = [];
    let userSayString = '' + _sayString;
    if (value && value.length > 0) {
      for (const utt of value) {
        const findIdx = userSayString.indexOf(utt.mappedValue);
        if (findIdx === 0) {
          ret.push({
            id: utt.id,
            text: userSayString.substring(0, utt.mappedValue.length),
            isCorpus: true,
            slot: utt.entityId
          });
          userSayString = userSayString.substring(utt.mappedValue.length);
        } else if (findIdx > 0) {
          ret.push({
            id: utt.id,
            text: userSayString.substring(0, findIdx),
            isCorpus: false,
            slot: ''
          });
          ret.push({
            id: utt.id,
            text: userSayString.substr(findIdx, utt.mappedValue.length),
            isCorpus: true,
            slot: utt.entityId
          });
          userSayString = userSayString.substring(findIdx + utt.mappedValue.length);
        } else {

        }
      }
    }
    if (userSayString.length > 0) {
      ret.push({
        id: '',
        text: userSayString,
        isCorpus: false,
        slot: ''
      });
    }
    return ret;
  }
}
