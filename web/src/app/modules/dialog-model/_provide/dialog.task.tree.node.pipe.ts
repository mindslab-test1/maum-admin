// import {Pipe, PipeTransform} from '@angular/core';
// import {TreeNode} from 'primeng/primeng';
// import {DialogSystemResponseUI} from '../service';

// export enum NODE_TYPE {
//   CONDITION = <any>'CONDITION',
//   UTTERANCE = <any>'UTTERANCE',
//   INTENTION = <any>'INTENTION',
// }
//
// const DEFAULT_START_EVENT_NODE = {
//   parent: undefined,
//   data: {
//     name: 'condition'
//   },
//   children: [
//     {
//       data: {
//         name: 'utterance'
//       }
//     }
//   ]
// };
// const DEFAULT_NORMAL_EVENT_NODE = {
//   parent: undefined,
//   data: {
//     name: 'intention'
//   },
//   children: [
//     {
//       data: {
//         name: 'condition'
//       },
//       children: [
//         {
//           data: {
//             name: 'utterance'
//           }
//         }
//       ]
//     }
//   ]
// };
//
// @Pipe({
//   name: 'taskTreeNode'
// })
// export class DialogTaskTreeNodePipe implements PipeTransform {
//   transform(value: DialogSystemResponseUI, _isStartEvent: boolean, _isGlobalTask: boolean): TreeNode[] {
//     const retArray = [];
//     let events = [];
//     let isStartEventType: boolean = _isStartEvent;
//     if (value) {
//       if (_isGlobalTask) {
//         events = value.taskStartEvent;
//         isStartEventType = true;
//       } else {
//         if (isStartEventType) {
//           events = value.taskStartEvent;
//         } else {
//           events = value.taskEvent;
//         }
//       }
//       if (isStartEventType) {
//         for (let i = 0; i < events.length; i++) {
//           const node = Object.assign({}, DEFAULT_START_EVENT_NODE);
//           Object.assign(node, {
//             data: {
//               name: 'condition' + i,
//               original: events[i],
//               isGlobal: _isGlobalTask,
//               type: NODE_TYPE.CONDITION
//             },
//             children: [
//               {
//                 data: {
//                   name: 'utterance' + i,
//                   original: events[i]['startEventUtter'],
//                   isGlobal: _isGlobalTask,
//                   type: NODE_TYPE.UTTERANCE
//                 }
//               }
//             ]
//           });
//           retArray.push(node);
//         }
//       } else {
//         for (let i = 0; i < events.length; i++) {
//           const node = Object.assign({}, DEFAULT_NORMAL_EVENT_NODE);
//           Object.assign(node, {
//             data: {
//               name: 'intention' + i,
//               original: events[i],
//               isGlobal: _isGlobalTask,
//               type: NODE_TYPE.INTENTION
//             },
//             children: [
//               {
//                 data: {
//                   name: 'condition' + events[i].index,
//                   original: events[i],
//                   isGlobal: _isGlobalTask,
//                   type: NODE_TYPE.CONDITION
//                 },
//                 children: [
//                   {
//                     data: {
//                       name: 'utterance' + events[i].index,
//                       original: events[i],
//                       isGlobal: _isGlobalTask,
//                       type: NODE_TYPE.UTTERANCE
//                     }
//                   }
//                 ]
//               }
//             ]
//           });
//           retArray.push(node);
//         }
//       }
//     }
//     return retArray;
//   }
// }
