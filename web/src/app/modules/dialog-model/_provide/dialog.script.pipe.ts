import {Pipe, PipeTransform} from '@angular/core';
import {DialogUtterData} from '../service/dialog.service';

@Pipe({
  name: 'scriptTransform'
})
export class DialogScriptPipe implements PipeTransform {
  transform(value: string): string {
    let ret: string;

    if (value === null || value === undefined || value.length === 0 || (typeof value) !== 'string') {
      return value;
    } else {
      // ret = value
      //   .replace(/Count/g, '<mark>Count</mark>')
      //   .replace(/Value/g, '<mark>Value</mark>')
      //   .replace(/ExistValue/g, '<mark>ExistValue</mark>')
      //   .replace(/HasValue/g, '<mark>HasValue</mark>')
      //   .replace(/IsDAType/g, '<mark>IsDAType</mark>')
      //   .replace(/IsDATypeAtPreviousUtter/g, '<mark>IsDATypeAtPreviousUtter</mark>')
      //   .replace(/IsDATypeAtUtterHistory/g, '<mark>IsDATypeAtUtterHistory</mark>')
      //   .replace(/ExistValueAtHistory/g, '<mark>ExistValueAtHistory</mark>');

      ret = value
      .replace(/\n$/g, '\n\n')
      .replace(/Count\b/g, '<mark>$&</mark>')
      .replace(/Value\b/g, '<mark>$&</mark>')
      .replace(/ExistValue\b/g, '<mark>$&</mark>')
      .replace(/HasValue\b/g, '<mark>$&</mark>')
      .replace(/IsDAType\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtUtterHistory\b/g, '<mark>$&</mark>')
      .replace(/ExistValueAtHistory\b/g, '<mark>$&</mark>');
    }

    return ret;
  }
}
