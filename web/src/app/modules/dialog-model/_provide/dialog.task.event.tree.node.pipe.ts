import {Pipe, PipeTransform} from '@angular/core';
import {TaskEvent} from '../service/dialog.service';
import {TreeNode} from 'primeng/primeng';


export enum NODE_TYPE_EVENT {
  CONDITION = <any>'CONDITION',
  UTTERANCE = <any>'UTTERANCE',
  INTENTION = <any>'INTENTION',
}

const DEFAULT_EVENT_NODE = {
  parent: undefined,
  data: {
    name: 'intention'
  },
  children: []
};

const DEFAULT_EVENT_CONDITION_NODE = {
  parent: undefined,
  data: {
    name: 'condition'
  },
  children: []
};

const DEFAULT_EVENT_UTTER_NODE = {
  data: {
    name: 'utterance'
  }
};

@Pipe({
  name: 'taskEventTreeNode'
})
export class DialogTaskEventTreeNodePipe implements PipeTransform {
  transform(value: TaskEvent[], _isGlobalTask: boolean): TreeNode[] {
    const retArray = [];
    if (value) {
      if (value.length > 0) {
        for (let i = 0; i < value.length; i++) {
          let parentNode = {};
          parentNode = Object.assign({}, DEFAULT_EVENT_NODE);
          Object.assign(parentNode, {
            data: {
              name: 'intention' + i,
              original: value[i],
              isGlobal: _isGlobalTask,
              type: NODE_TYPE_EVENT.INTENTION,
              index: i
            },
            children: []
          });
          const condition = value[i].eventCondition;
          if (condition && condition.length > 0) {
            for (let j = 0; j < condition.length; j++) {
              let child = {};
              child = Object.assign({}, DEFAULT_EVENT_CONDITION_NODE);
              child = Object.assign(child, {
                data: {
                  name: 'condition' + j,
                  original: condition[j],
                  isGlobal: _isGlobalTask,
                  type: NODE_TYPE_EVENT.CONDITION,
                  index: j
                },
                children: []
              });
              const utter = condition[j].eventUtter;
              if (utter && utter.length > 0) {
                for (let k = 0; k < utter.length; k++) {
                  let grandChild = {};
                  grandChild = Object.assign({}, DEFAULT_EVENT_UTTER_NODE);
                  grandChild = Object.assign(grandChild, {
                    data: {
                      name: 'utterance' + k,
                      original: utter[k],
                      isGlobal: _isGlobalTask,
                      type: NODE_TYPE_EVENT.UTTERANCE,
                      index: k
                    }
                  });
                  child['children'].push(grandChild);
                }
              }
              parentNode['children'].push(child);
            }
          }
          retArray.push(parentNode);
        }
      }
    }
    return retArray;
  }
}
