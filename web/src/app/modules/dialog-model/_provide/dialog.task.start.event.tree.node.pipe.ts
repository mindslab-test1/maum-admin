import {Pipe, PipeTransform} from '@angular/core';
import {TreeNode} from 'primeng/primeng';
import {TaskStartEvent} from '../service/dialog.service';

export enum NODE_TYPE_START_EVENT {
  CONDITION = <any>'CONDITION',
  UTTERANCE = <any>'UTTERANCE'
}

const DEFAULT_START_EVENT_NODE = {
  parent: undefined,
  data: {
    name: 'condition'
  },
  children: []
};

const DEFAULT_UTTER_NODE = {
  data: {
    name: 'utterance'
  }
};

@Pipe({
  name: 'taskStartEventTreeNode'
})
export class DialogTaskStartEventTreeNodePipe implements PipeTransform {
  transform(value: TaskStartEvent[], _isGlobalTask: boolean): TreeNode[] {
    const retArray = [];
    if (value) {
      if (value.length > 0) {
        for (let i = 0; i < value.length; i++) {
          let parentNode = {};
          parentNode = Object.assign({}, DEFAULT_START_EVENT_NODE);
          Object.assign(parentNode, {
            data: {
              name: 'condition' + i,
              original: value[i],
              isGlobal: _isGlobalTask,
              type: NODE_TYPE_START_EVENT.CONDITION,
              index: i
            },
            children: []
          });
          if (value[i].startEventUtter && value[i].startEventUtter.length > 0) {
            for (let j = 0; j < value[i].startEventUtter.length; j++) {
              let slot = Object.assign({}, DEFAULT_UTTER_NODE);
              slot = Object.assign(slot, {
                data: {
                  name: 'utterance' + j,
                  original: value[i].startEventUtter[j],
                  isGlobal: _isGlobalTask,
                  type: NODE_TYPE_START_EVENT.UTTERANCE,
                  index: j
                }
              });
              parentNode['children'].push(slot);
            }
          }
          retArray.push(parentNode);
        }
      }
    }
    return retArray;
  }
}
