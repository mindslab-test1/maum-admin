import {Pipe, PipeTransform} from '@angular/core';
import {TreeNode} from 'primeng/primeng';
import {DialogEntity} from '../service';
import * as _ from 'lodash';

const DEFAULT_ENTITY_NODE = {
  parent: undefined,
  data: {
    name: '',
    className: '',
    classSource: ''
  },
  children: []
};

const DEFAULT_SLOT_NODE = {
  data: {
    name: '',
    description: '',
    entityData: [],
    classSource: 'KB',
    entityType: 'STRING'
  }
};

@Pipe({
  name: 'entityTreeNode'
})
export class EntityTreeNodePipe implements PipeTransform {
  transform(value: DialogEntity[], expanded?: boolean): TreeNode[] {
    const ret = [];
    for (const entity of value) {
      const alreadyHasClass: boolean = (ret.filter((val) => (val.data.className === entity.className)).length > 0);
      let parentNode = {};
      if (alreadyHasClass === true) {
        parentNode = ret.filter((val) => (val.data.className === entity.className))[0];
      } else {
        parentNode = _.cloneDeep(DEFAULT_ENTITY_NODE);
        parentNode = Object.assign(parentNode, {
          data: {
            name: entity.className,
            className: entity.className,
            classSource: entity.classSource,
            sharedYn: entity.sharedYn,
            original: entity,
            type: 'CLASS_NODE'
          },
          children: []
        });
      }
      let slot = _.cloneDeep(DEFAULT_SLOT_NODE);
      slot = Object.assign(slot, {
        data: {
          name: entity.entityName,
          entityData: entity.entityData,
          entityType: entity.entityType,
          sharedYn: entity.sharedYn,
          original: entity,
          type: 'ENTITY_NODE'
        }
      });
      parentNode['children'].push(slot);
      if (expanded === true) {
        parentNode['expanded'] = true;
      }

      if (alreadyHasClass === false) {
        ret.push(parentNode);
      }
    }
    return ret;
  }
}
