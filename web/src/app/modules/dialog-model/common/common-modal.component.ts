import {AfterViewChecked, Component, ElementRef} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-common-modal',
  templateUrl: './common-modal.component.html'
})

export class CommonModalComponent implements AfterViewChecked {
  public dummy: any = null;

  constructor(private elementRef: ElementRef) {
  }

  ngAfterViewChecked() {
    this.popupCenter();
  }

  public modalOpen() {
    $(this.elementRef.nativeElement.querySelector('div')).addClass('modal-section');
  }

  public modalClose() {
    $(this.elementRef.nativeElement.querySelector('div')).removeClass('modal-section');
  }

  public popupCenter() {
    const modalHeight = $('.modal-section .common-modal-container').height() + 30;
    const modalMargin = -(modalHeight / 2);
    $('.common-modal-container').css({
      'top': '50%',
      'marginTop': modalMargin + 'px'
    });
    $('.modal-section').css('min-height', modalHeight);
  }

  public closeAllModals() {
    $('.common-modal-section').removeClass('modal-section');
  }
}
