import {finalize, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';


import {environment} from '../../../../environments/environment';
import {UUID} from 'angular2-uuid';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

// jsPlumb data models
export class NodeData {
  id: string;
  type: string;
  text: string;
  left: number;
  top: number;
  preLeft: number;
  preTop: number;
  w: number;
  h: number;
  task: DialogTask;

  constructor() {
    this.id = null;
    this.type = null;
    this.text = null;
    this.left = 0;
    this.top = 0;
    this.preLeft = 0;
    this.preTop = 0;
    this.w = 70;
    this.h = 50;
    this.task = new DialogTask();
  }
}

export class EdgeData {
  id: string;
  source: string;
  target: string;
  data: EdgeDataData;

  constructor() {
    this.source = null;
    this.source = null;
    this.data = new EdgeDataData();
  }
}

export class EdgeDataData {
  label: string;
  type: string;
  nextTask: DialogNextTask;

  constructor() {
    this.label = null;
    this.label = 'connection';
    this.nextTask = new DialogNextTask();
  }
}

// dialog entity models
export class DialogModel {
  id: string;
  modelName: string;
  description: string;
  status: string;
  tasks: DialogTask[];
  intents: DialogIntent[];
  entities: DialogEntity[];
  creatorId: string;
  updaterId: string;
  workspaceId: string;
  workspaceName: string;
  createdAt: string;
  updatedAt: string;
  lastTrainKey: string;

  constructor() {

    this.id = '';
    this.modelName = '';
    this.description = '';
    this.status = '';
    this.tasks = [];
    this.intents = [];
    this.entities = [];
    this.creatorId = '';
    this.updaterId = '';
    this.workspaceId = '';
    this.workspaceName = '';
    this.createdAt = '';
    this.updatedAt = '';
    this.lastTrainKey = '';
  }
}

export enum DialogEntitySlotSourceType {
  SYSTEM = <any>'SYSTEM',
  USER = <any>'USER',
  KB = <any>'KB',
  DIALOG_USER = <any>'DIALOG_USER',
  DIALOG_SYSTEM = <any>'DIALOG_SYSTEM'
}

export enum DialogEntitySlotType {
  INT = <any>'INT',
  FLOAT = <any>'FLOAT',
  STRING = <any>'STRING',
  TIME = <any>'TIME',
  DATE = <any>'DATE',
  EVENT = <any>'EVENT'
}

export enum DialogTaskType {
  ESSENTIAL = <any>'ESSENTIAL',
  OPTIONAL = <any>'OPTIONAL'
}

export class DialogEntityData {
  id: string;
  entityData: string;
  entityId: string;
  creatorId: string;
  entityDataSynonym: DialogEntityDataSynonym [];

  constructor() {
    this.id = '';
    this.entityData = '';
    this.entityId = '';
    this.creatorId = '';
    this.entityDataSynonym = [];
  }
}

export class DialogEntityDataSynonym {
  id: string;
  entityDataSynonym: string;
  creatorId: string;
  entityDataId: string;

  constructor() {
    this.id = '';
    this.entityDataSynonym = '';
    this.creatorId = '';
    this.entityDataId = '';
  }
}

export class DialogEntity {
  id: string;
  className: string;
  classSource: DialogEntitySlotSourceType;
  entityName: string;
  entitySource: string;
  entityType: DialogEntitySlotType;
  sharedYn: string;
  creatorId: string;
  updaterId: string;
  entityData: DialogEntityData[];
  createdAt: string;
  updatedAt: string;
  workspaceId: string;

  constructor() {
    this.className = 'new_domain';
    this.classSource = DialogEntitySlotSourceType.KB;
    this.entityName = 'new_entity';
    this.entitySource = '';
    this.entityType = DialogEntitySlotType.STRING;
    this.sharedYn = '';
    this.creatorId = '';
    this.updaterId = '';
    this.createdAt = '';
    this.updatedAt = '';
    this.workspaceId = '';
  }
}

export class DialogUtterData {
  id: string;
  creatorId: string;
  entityId: string;
  mappedValue: string;
  intentUtterId: string;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.entityId = '';
    this.mappedValue = '';
    this.intentUtterId = '';
  }
}

export class DialogUtter {
  id: string;
  creatorId: string;
  mappedUserSay: string;
  userSay: string;
  intentId: string;
  intentUtterData: DialogUtterData[];
  createdAt: string;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.mappedUserSay = '';
    this.userSay = '';
    this.intentId = '';
    this.intentUtterData = [];
  }
}

export class DialogIntent {
  id: string;
  intentName: string;
  sharedYn: string;
  creatorId: string;
  updaterId: string;
  workspaceId: string;
  intentUtter: DialogUtter[];
  importedYn: string;

  constructor() {
    this.id = '';
    this.intentName = '';
    this.sharedYn = '';
    this.creatorId = '';
    this.updaterId = '';
    this.workspaceId = '';
    this.intentUtter = [];
    this.importedYn = '';
  }
}

export class TaskStartEvent {
  id: string;
  creatorId: string;
  startEventAction: string;
  startEventCondition: string;
  taskId: string;
  startEventUtter: TaskStartEventUtter[];
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.startEventAction = '';
    this.startEventCondition = '';
    this.taskId = '';
    this.startEventUtter = [];
    this.index = -1;
  }
}

export class TaskStartEventUtter {
  id: string;
  creatorId: string;
  extraData: string;
  intentId: string;
  startEventId: string;
  startEventUtterData: TaskStartEventUtterData[];
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.extraData = '';
    this.intentId = '';
    this.startEventId = '';
    this.startEventUtterData = [];
    this.index = -1;
  }
}

export class TaskStartEventUtterData {
  id: string;
  creatorId: string;
  startEventUtterData: string;
  startEventUtterId: string;
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.startEventUtterData = '';
    this.startEventUtterId = '';
    this.index = -1;
  }
}

export class TaskEvent {
  id: string;
  creatorId: string;
  intentId: string;
  taskId: string;
  eventCondition: TaskEventCondition[];
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.intentId = '';
    this.taskId = '';
    this.eventCondition = [];
    this.index = -1;
  }
}

export class TaskEventCondition {
  id: string;
  creatorId: string;
  eventAction: string;
  eventCondition: string;
  extraData: string;
  eventId: string;
  eventUtter: TaskEventUtter[];
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.eventAction = '';
    this.eventCondition = '';
    this.extraData = '';
    this.eventId = '';
    this.eventUtter = [];
    this.index = -1;
  }
}

export class TaskEventUtter {
  id: string;
  creatorId: string;
  extraData: string;
  intentId: string;
  eventConditionId: string;
  eventUtterData: TaskEventUtterData[];
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.extraData = '';
    this.intentId = '';
    this.eventConditionId = '';
    this.eventUtterData = [];
    this.index = -1;
  }
}

export class TaskEventUtterData {
  id: string;
  creatorId: string;
  eventUtterData: string;
  eventUtterId: string;
  index: number;

  constructor() {
    this.id = '';
    this.creatorId = '';
    this.eventUtterData = '';
    this.eventUtterId = '';
    this.index = -1;
  }
}

export class DialogNextTask {
  id: string;
  controller: string;
  createdAt: string;
  creatorId: string;
  nextTaskCondition: string;
  nextTaskId: string;
  taskId: string;
}

export class DialogTask {
  id: string;
  taskName: string;
  taskGoal: string;
  taskType: DialogTaskType;
  resetSlot: string;
  creatorId: string;
  updaterId: string;
  workspaceId: string;
  modelId: string;
  startEvent: TaskStartEvent[];
  event: TaskEvent[];
  nextTask: DialogNextTask[];
  key: string;
  taskLocation: string;
  loc: string;

  constructor() {
    this.id = '';
    this.taskName = '';
    this.taskGoal = '';
    this.taskType = DialogTaskType.ESSENTIAL;
    this.resetSlot = 'no';
    this.creatorId = '';
    this.updaterId = '';
    this.workspaceId = '';
    this.modelId = '';
    this.startEvent = [];
    this.event = [];
    this.nextTask = [];
    this.key = '';
    this.taskLocation = '0 0';
    this.loc = '0 0';
  }
}

export const DefaultDialogTask: DialogTask = {
  id: '',
  taskName: '',
  taskGoal: '',
  taskType: DialogTaskType.ESSENTIAL,
  resetSlot: 'no',
  creatorId: '',
  updaterId: '',
  workspaceId: '',
  modelId: '',
  startEvent: [],
  event: [],
  nextTask: [],
  key: '',
  loc: '0 0',
  taskLocation: '0 0'
};

export const DefaultGlobalDialogTask: DialogTask = {
  id: 'GLOBAL_TASK',
  taskName: 'GLOBAL_TASK',
  taskGoal: null,
  taskType: DialogTaskType.ESSENTIAL,
  resetSlot: 'no',
  creatorId: '',
  updaterId: '',
  workspaceId: '',
  modelId: '',
  startEvent: [],
  event: [],
  nextTask: [],
  key: '',
  loc: '0 0',
  taskLocation: '0 0'
};

export class ScriptGuide {
  function: string;
  description: string;
}

@Injectable()
export class DialogService {
  @BlockUI() blockUI: NgBlockUI;
  private DEFAULT_REQUEST_OPTION: RequestOptions;
  private url: string;

  // private isCopy: boolean;

  constructor(private http: Http) {
    this.DEFAULT_REQUEST_OPTION = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json; charset=UTF-8'
      })
    });
    this.url = environment.maumAiApiUrl;
  }

  public getUUID(): string {
    return UUID.UUID();
  }

  public getDialogModelList(workspaceId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/list`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        if (response) {
          return response.json();
        } else {
          return null;
        }
      }));
  }


  public insertDialogModel(dialogModel: DialogModel): Observable<any> {
    this.blockUI.start();
    const body = {
      id: dialogModel.id,
      modelName: dialogModel.modelName,
      description: dialogModel.description,
      userId: dialogModel.creatorId
    };
    return this.http.post(`${this.url}/sds/model/workspace/${dialogModel.workspaceId}/model`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }


  public getDialogModel(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/allinfo`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public deleteDialogModel(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.delete(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelTask(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/list`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelTaskAll(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/allinfolist`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public deleteDialogModelTask(workspaceId: string, modelId: string, taskId: string): Observable<any> {
    this.blockUI.start();
    return this.http.delete(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelNextTaskList(workspaceId: string, modelId: string, task: DialogTask): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${task.id}/nexttask/list`,
      this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelIntents(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/list`,
      this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelIntentAll(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/allinfolist`,
      this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelSharedIntentAll(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/sharedintent/list`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelIntentUtter(workspaceId: string, modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${intent.id}/intentutter/list`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelIntentUtterData(workspaceId: string, modelId: string, utter: DialogUtter): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${utter.intentId}/intentutter/${utter.id}/intentutterdata/list`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelSharedEntityAll(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/sharedentity/list`,
      this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelEntityAll(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/allinfolist`,
      this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public getDialogModelEntities(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/list`, this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public updateDialogModelInfo(dialogModel: DialogModel): Observable<any> {
    this.blockUI.start();
    const body = {
      modelName: dialogModel.modelName,
      description: dialogModel.description,
      userId: dialogModel.creatorId,
      lastTrainKey: dialogModel.lastTrainKey
    };
    return this.http.put(`${this.url}/sds/model/workspace/${dialogModel.workspaceId}/model/${dialogModel.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTask(workspaceId: string, modelId: string, task: DialogTask): Observable<any> {
    this.blockUI.start();
    const body = {
      id: task.id,
      taskName: task.taskName,
      taskGoal: task.taskGoal,
      userId: task.creatorId,
      taskLocation: task.loc
    };
    return this.http.post(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public updateDialogModelTask(workspaceId: string, modelId: string, task: DialogTask): Observable<any> {
    this.blockUI.start();
    const body = {
      taskName: task.taskName,
      taskGoal: task.taskGoal,
      userId: task.creatorId,
      taskLocation: task.loc
    };
    return this.http.put(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${task.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  public insertDialogNextTask(workspaceId: string, modelId: string, data: DialogNextTask): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      nextTaskId: data.nextTaskId,
      nextTaskCondition: data.nextTaskCondition,
      userId: data.creatorId
    };
    return this.http.post(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/nexttask`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(
      map(response => {
        return response.json();
      }), finalize(() => {
        this.blockUI.stop();
      }),);
  }

  getScriptGuide(): Observable<ScriptGuide[]> {
    return this.http.get('./assets/script-guide.json').pipe(
      map(response => {
        return (response.json().scriptGuide) as ScriptGuide[];
      }));
  }

  public insertDialogModelIntent(modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    const body = {
      id: intent.id,
      intentName: intent.intentName,
      sharedYn: intent.sharedYn,
      userId: intent.creatorId
    };
    return this.http.post(`${this.url}/sds/model/workspace/${intent.workspaceId}/model/${modelId}/intent`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelIntent(modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    const body = {
      intentName: intent.intentName,
      sharedYn: intent.sharedYn,
      userId: intent.creatorId
    };
    return this.http.put(`${this.url}/sds/model/workspace/${intent.workspaceId}/model/${modelId}/intent/${intent.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public shareDialogModelIntent(modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: intent.creatorId
    };
    return this.http.put(`${this.url}/sds/model/workspace/${intent.workspaceId}/model/${modelId}/intent/${intent.id}/share`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelIntent(workspaceId: string, modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: intent.creatorId,
      isLinkDelete: (intent.sharedYn === 'Y')
    };
    return this.http.post(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${intent.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }


  public importDialogSharedIntent(workspaceId: string, modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: intent.creatorId
    };
    return this.http.put(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${intent.id}/import`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public linkDialogSharedIntent(workspaceId: string, modelId: string, intent: DialogIntent): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: intent.creatorId
    };
    return this.http.put(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${intent.id}/link`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelEntity(modelId: string, entity: DialogEntity): Observable<any> {
    this.blockUI.start();
    const body = {
      id: entity.id,
      className: entity.className,
      userId: entity.creatorId,
      entityName: entity.entityName,
      entityType: entity.entityType,
      sharedYn: entity.sharedYn
    };
    return this.http.post(`${this.url}/sds/model/workspace/${entity.workspaceId}/model/${modelId}/entity`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelEntity(modelId: string, entity: any): Observable<any> {
    this.blockUI.start();
    const body = {
      entityType: entity.entityType,
      sharedYn: entity.sharedYn,
      userId: entity.creatorId
    };
    if (entity.className) {
      body['className'] = entity.className;
    } else {
      body['entityName'] = entity.entityName;
    }
    return this.http.put(`${this.url}/sds/model/workspace/${entity.workspaceId}/model/${modelId}/entity/${entity.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelEntity(modelId: string, entity: DialogEntity): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: entity.creatorId,
      isLinkDelete: (entity.sharedYn === 'Y')
    };
    return this.http.post(`${this.url}/sds/model/workspace/${entity.workspaceId}/model/${modelId}/entity/${entity.id}`, JSON.stringify(body),
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelEntityClass(
    workspaceId: string, modelId: string, isLinkDelete: string[], entityIdList: string[]): Observable<any> {
    this.blockUI.start();
    const body = {
      isLinkDelete: isLinkDelete,
      entityIdList: entityIdList
    };
    return this.http.post(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/list`, JSON.stringify(body),
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelEntityDatas(workspaceId: string, modelId: string, entityId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/${entityId}/entitydata/list`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelEntityData(workspaceId: string, modelId: string, data: DialogEntityData): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      entityData: data.entityData,
      userId: data.creatorId
    };
    return this.http.post(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/${data.entityId}/entitydata`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelEntityData(workspaceId: string, modelId: string, data: DialogEntityData): Observable<any> {
    this.blockUI.start();
    const body = {
      entityData: data.entityData,
      userId: data.creatorId,
    };
    return this.http.put(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/${data.entityId}/entitydata/${data.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelEntityData(workspaceId: string, modelId: string, data: DialogEntityData): Observable<any> {
    this.blockUI.start();
    return this.http.delete(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/${data.entityId}/entitydata/${data.id}`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelEntityDataSynonym(workspaceId: string, modelId: string, data: DialogEntityData): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/${data.entityId}/entitydata/${data.id}/entitydatasynonym/list`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelEntityDataSynonym(
    workspaceId: string, modelId: string, entityId: string, data: DialogEntityDataSynonym): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      entityDataSynonym: data.entityDataSynonym,
      userId: data.creatorId
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entity/${entityId}/entitydata/${data.entityDataId}/entitydatasynonym`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelEntityDataSynonym(
    workspaceId: string, modelId: string, entityId: string, data: DialogEntityDataSynonym): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/entity/${entityId}/entitydata/${data.entityDataId}/entitydatasynonym/${data.id}`;
    return this.http.delete(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelIntentUtter(workspaceId: string, modelId: string, utter: DialogUtter): Observable<any> {
    this.blockUI.start();
    const body = {
      id: utter.id,
      userId: utter.creatorId,
      userSay: utter.userSay,
      mappedUserSay: utter.mappedUserSay
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${utter.intentId}/intentutter`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelIntentUtter(workspaceId: string, modelId: string, utter: DialogUtter): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: utter.creatorId,
      userSay: utter.userSay,
      mappedUserSay: utter.mappedUserSay
    };
    return this.http.put(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${utter.intentId}/intentutter/${utter.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelIntentUtter(workspaceId: string, modelId: string, utter: DialogUtter): Observable<any> {
    this.blockUI.start();
    return this.http.delete(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${utter.intentId}/intentutter/${utter.id}`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelIntentUtterData(workspaceId: string, modelId: string, intentId: string, data: DialogUtterData): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      userId: data.creatorId,
      entityId: data.entityId,
      mappedValue: data.mappedValue
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intent/${intentId}/intentutter/${data.intentUtterId}/intentutterdata`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelIntentUtterData(workspaceId: string, modelId: string, intentId: string, data: DialogUtterData): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/intent/${intentId}/intentutter/${data.intentUtterId}/intentutterdata/${data.id}`;
    return this.http.delete(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelIntentUtterData(workspaceId: string, modelId: string, intentId: string, data: DialogUtterData): Observable<any> {
    this.blockUI.start();
    const body = {
      userId: data.creatorId,
      entityId: data.entityId,
      mappedValue: data.mappedValue
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/intent/${intentId}/intentutter/${data.intentUtterId}/intentutterdata/${data.id}`;
    return this.http.put(url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTaskStartEvent(workspaceId: string, modelId: string, data: TaskStartEvent): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      startEventCondition: data.startEventCondition,
      startEventAction: data.startEventAction,
      userId: data.creatorId
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/startevent`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelTaskStartEvent(workspaceId: string, modelId: string, data: DialogTask): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.id}/startevent/list`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskStartEvent(workspaceId: string, modelId: string, data: TaskStartEvent): Observable<any> {
    this.blockUI.start();
    const body = {
      startEventCondition: data.startEventCondition,
      startEventAction: data.startEventAction,
      userId: data.creatorId
    };
    return this.http.put(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/startevent/${data.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskStartEvent(workspaceId: string, modelId: string, data: TaskStartEvent): Observable<any> {
    this.blockUI.start();
    return this.http.delete(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/startevent/${data.id}`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTaskStartEventUtter(
    workspaceId: string, modelId: string, taskId: string, data: TaskStartEventUtter): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      intentId: data.intentId,
      extraData: data.extraData,
      userId: data.creatorId
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/startevent/${data.startEventId}/starteventutter`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelTaskStartEventUtter(workspaceId: string, modelId: string, data: TaskStartEvent): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/startevent/${data.id}/starteventutter/list`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskStartEventUtter(
    workspaceId: string, modelId: string, taskId: string, data: TaskStartEventUtter): Observable<any> {
    this.blockUI.start();
    const body = {
      intentId: data.intentId,
      extraData: data.extraData,
      userId: data.creatorId
    };
    return this.http.put(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/startevent/${data.startEventId}/starteventutter/${data.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskStartEventUtter(
    workspaceId: string, modelId: string, taskId: string, data: TaskStartEventUtter): Observable<any> {
    this.blockUI.start();
    return this.http.delete(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/startevent/${data.startEventId}/starteventutter/${data.id}`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTaskStartEventUtterData(
    workspaceId: string, modelId: string, taskId: string, startEventId: string, data: TaskStartEventUtterData): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      startEventUtterData: data.startEventUtterData,
      userId: data.creatorId
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/task/${taskId}/startevent/${startEventId}/starteventutter/${data.startEventUtterId}/starteventutterdata`;
    return this.http.post(url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelTaskStartEventUtterData(
    workspaceId: string, modelId: string, taskId: string, data: TaskStartEventUtter): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/task/${taskId}/startevent/${data.startEventId}/starteventutter/${data.id}/starteventutterdata/list`;
    return this.http.get(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskStartEventUtterData(
    workspaceId: string, modelId: string, taskId: string, startEventId: string, data: TaskStartEventUtterData): Observable<any> {
    this.blockUI.start();
    const body = {
      startEventUtterData: data.startEventUtterData,
      userId: data.creatorId
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/task/${taskId}/startevent/${startEventId}/starteventutter/${data.startEventUtterId}/starteventutterdata/${data.id}`;
    return this.http.put(url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskStartEventUtterData(
    workspaceId: string, modelId: string, taskId: string, startEventId: string, data: TaskStartEventUtterData): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}`;
    url += `/task/${taskId}/startevent/${startEventId}/starteventutter/${data.startEventUtterId}/starteventutterdata/${data.id}`;
    return this.http.delete(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  //////////////////////////////////////////////////////////////////////////
  public insertDialogModelTaskEvent(workspaceId: string, modelId: string, data: TaskEvent): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      userId: data.creatorId
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/event`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelTaskEvent(workspaceId: string, modelId: string, data: DialogTask): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.id}/event/list`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskEvent(workspaceId: string, modelId: string, data: TaskEvent): Observable<any> {
    this.blockUI.start();
    const body = {
      intentId: data.intentId,
      userId: data.creatorId
    };
    return this.http.put(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/event/${data.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskEvent(workspaceId: string, modelId: string, data: TaskEvent): Observable<any> {
    this.blockUI.start();
    return this.http.delete(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.id}/event/${data.id}`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTaskEventCondition(
    workspaceId: string, modelId: string, taskId: string, data: TaskEventCondition): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      eventCondition: data.eventCondition,
      eventAction: data.eventAction,
      extraData: data.extraData,
      userId: data.creatorId
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/event/${data.eventId}/eventcondition`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  //
  public getDialogModelTaskEventCondition(workspaceId: string, modelId: string, data: TaskEvent): Observable<any> {
    this.blockUI.start();
    return this.http.get(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${data.taskId}/event/${data.id}/eventcondition/list`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskEventCondition(
    workspaceId: string, modelId: string, taskId: string, data: TaskEventCondition): Observable<any> {
    this.blockUI.start();
    const body = {
      eventCondition: data.eventCondition,
      eventAction: data.eventAction,
      extraData: data.extraData,
      userId: data.creatorId
    };
    return this.http.put(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/event/${data.eventId}/eventcondition/${data.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskEventCondition(
    workspaceId: string, modelId: string, taskId: string, data: TaskEventCondition): Observable<any> {
    this.blockUI.start();
    return this.http.delete(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/event/${data.eventId}/eventcondition/${data.id}`,
      this.DEFAULT_REQUEST_OPTION
    ).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTaskEventUtter(
    workspaceId: string, modelId: string, taskId: string, eventId: string, data: TaskEventUtter): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      extraData: data.extraData,
      intentId: data.intentId,
      userId: data.creatorId
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${data.eventConditionId}/eventutter`;
    return this.http.post(
      url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelTaskEventUtter(workspaceId: string, modelId: string, taskId: string, data: TaskEventCondition): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${data.eventId}/eventcondition/${data.id}/eventutter/list`;
    return this.http.get(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskEventUtter(
    workspaceId: string, modelId: string, taskId: string, eventId: string, data: TaskEventUtter): Observable<any> {
    this.blockUI.start();
    const body = {
      extraData: data.extraData,
      intentId: data.intentId,
      userId: data.creatorId
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${data.eventConditionId}/eventutter/${data.id}`;
    return this.http.put(url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskEventUtter(
    workspaceId: string, modelId: string, taskId: string, eventId: string, data: TaskEventUtter): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${data.eventConditionId}/eventutter/${data.id}`;
    return this.http.delete(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public insertDialogModelTaskEventUtterData(
    workspaceId: string, modelId: string, taskId: string,
    eventId: string, eventConditionId: string, data: TaskEventUtterData): Observable<any> {
    this.blockUI.start();
    const body = {
      id: data.id,
      eventUtterData: data.eventUtterData,
      userId: data.creatorId
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${eventConditionId}/eventutter/${data.eventUtterId}/eventutterdata`;
    return this.http.post(url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getDialogModelTaskEventUtterData(
    workspaceId: string, modelId: string, taskId: string, eventId: string, data: TaskEventUtter): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${data.eventConditionId}/eventutter/${data.id}/eventutterdata/list`;
    return this.http.get(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogModelTaskEventUtterData(
    workspaceId: string, modelId: string, taskId: string,
    eventId: string, eventConditionId: string, data: TaskEventUtterData): Observable<any> {
    this.blockUI.start();
    const body = {
      eventUtterData: data.eventUtterData,
      userId: data.creatorId
    };
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${eventConditionId}/eventutter/${data.eventUtterId}/eventutterdata/${data.id}`;
    return this.http.put(url, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogModelTaskEventUtterData(
    workspaceId: string, modelId: string, taskId: string,
    eventId: string, eventConditionId: string, data: TaskEventUtterData): Observable<any> {
    this.blockUI.start();
    let url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}`;
    url += `/event/${eventId}/eventcondition/${eventConditionId}/eventutter/${data.eventUtterId}/eventutterdata/${data.id}`;
    return this.http.delete(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateDialogNextTask(workspaceId: string, modelId: string, taskId: string, data: DialogNextTask): Observable<any> {
    this.blockUI.start();
    const body = {
      nextTaskId: data.nextTaskId,
      nextTaskCondition: data.nextTaskCondition,
      userId: data.creatorId
    };
    return this.http.put(
      `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/nexttask/${data.id}`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public deleteDialogNextTask(workspaceId: string, modelId: string, taskId: string, nextTaskId: string): Observable<any> {
    this.blockUI.start();
    const url = `${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/task/${taskId}/nexttask/${nextTaskId}`;
    return this.http.delete(url, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public copyDialogModel(data: DialogModel): Observable<any> {
    this.blockUI.start();
    const body = {
      sourceModelId: data.id,
      modelName: data.modelName,
      description: data.description,
      userId: data.creatorId
    };
    return this.http.post(
      `${this.url}/sds/model/workspace/${data.workspaceId}/copymodel`,
      JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public startTraining(modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/trainer/open/${modelId}`, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getTrainingProgress(trainKey: string): Observable<any> {
    return this.http.get(`${this.url}/sds/model/trainer/progress/${trainKey}`, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }));
  }

  public getTrainingProgressAll(): Observable<any> {
    return this.http.get(`${this.url}/sds/model/trainer/allprogress`, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }));
  }

  public getTrainingBinary(modelName: string, trainKey: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/trainer/getbinary/${modelName}/${trainKey}`, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public stopTraining(trainKey: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/trainer/stop/${trainKey}`, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public downloadDialogEntity(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/entitydownload`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response;
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public downloadDialogIntent(workspaceId: string, modelId: string): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/workspace/${workspaceId}/model/${modelId}/intentdownload`,
      this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response;
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public uploadDialogEntity(workspaceId: string, modelId: string, formData: FormData): Observable<any> {
    this.blockUI.start();
    const userId = localStorage.getItem('currentUserId');
    const header = new Headers({'enctype': 'multipart/form-data'});
    return this.http.post(`${this.url}/sds/model/user/${userId}/workspace/${workspaceId}/model/${modelId}/entityupload`,
      formData, {headers: header}).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public uploadDialogIntent(workspaceId: string, modelId: string, formData: FormData): Observable<any> {
    this.blockUI.start();
    const userId = localStorage.getItem('currentUserId');
    const header = new Headers({'enctype': 'multipart/form-data'});
    return this.http.post(`${this.url}/sds/model/user/${userId}/workspace/${workspaceId}/model/${modelId}/intentupload`,
      formData, {headers: header}).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public updateTalkResolverModel(data: DialogModel): Observable<any> {
    this.blockUI.start();
    const body = {
      modelId: data.id,
      modelName: data.modelName
    };
    return this.http.post(
      `${this.url}/sds/model/talk/resolver/updatemodel`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public linkTalkResolverModel(data: DialogModel): Observable<any> {
    this.blockUI.start();
    const body = {
      modelId: data.id,
      modelName: data.modelName
    };
    return this.http.post(
      `${this.url}/sds/model/talk/resolver/linkmodel`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public findTalkResolverModel(groupName: string): Observable<any> {
    this.blockUI.start();
    const body = {
      groupName: groupName
    };
    return this.http.post(
      `${this.url}/sds/model/talk/resolver/find`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public openTalkSdsOpen(host: string, port: number, modelId: string, modelName: string, sessionKey: number): Observable<any> {
    this.blockUI.start();
    const body = {
      host: host,
      port: port,
      modelId: modelId,
      modelName: modelName,
      sessionKey: sessionKey
    };
    return this.http.post(
      `${this.url}/sds/model/talk/sds/open`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public sendTalkSdsDialog(
    host: string, port: number, modelId: string, modelName: string, sessionKey: number, msg: string): Observable<any> {
    this.blockUI.start();
    const body = {
      host: host,
      port: port,
      modelId: modelId,
      modelName: modelName,
      sessionKey: sessionKey,
      utter: msg
    };
    return this.http.post(
      `${this.url}/sds/model/talk/sds/dialog`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public closeTalkSdsClose(
    host: string, port: number, modelId: string, modelName: string, sessionKey: number): Observable<any> {
    this.blockUI.start();
    const body = {
      host: host,
      port: port,
      modelId: modelId,
      modelName: modelName,
      sessionKey: sessionKey
    };
    return this.http.post(
      `${this.url}/sds/model/talk/sds/close`, JSON.stringify(body), this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      return response.json();
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }

  public getTalkErrorLog(): Observable<any> {
    this.blockUI.start();
    return this.http.get(`${this.url}/sds/model/talk/errlog`, this.DEFAULT_REQUEST_OPTION).pipe(map(response => {
      this.blockUI.stop();
      return response;
    }), finalize(() => {
      this.blockUI.stop();
    }),);
  }
}
