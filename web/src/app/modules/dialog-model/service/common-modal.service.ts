import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs';

export enum ModalType {
  DEFAULT = <any>'DEFAULT',
  DIALOG_MODEL_TRAINING_MODAL = <any>'DIALOG_MODEL_TRAINING_MODAL',
  ALERT_MESSAGE_MODAL = <any>'ALERT_MESSAGE'
}

export class CommonModalEvent {
  modalType: ModalType;
  modalTitle: string;
  modalTextMessage: string;
  modalHasCancelButton: boolean;
  isOpen: boolean;
  eventEmitter: EventEmitter<any>;
  modalExtraData: any;

  constructor() {
    this.modalType = ModalType.DEFAULT;
    this.modalTextMessage = '';
    this.modalTitle = 'Alert';
    this.isOpen = true;
    this.eventEmitter = null;
    this.modalExtraData = null;
  }
}

export class DialogModelTrainingEvent {
  indriThreshold: number;
  svmThreshold: number;
  isOk: boolean;

  constructor() {
    this.indriThreshold = 0.5;
    this.svmThreshold = 0;
    this.isOk = true;
  }
}

export class AlertModalEvent {
  isOk: boolean;

  constructor() {
    this.isOk = false;
  }
}

@Injectable()
export class CommonModalService {
  private modalHandlerObserver: any;
  public modalHandler: Observable<CommonModalEvent>;

  constructor() {
    this.modalHandlerObserver = null;
    this.modalHandler = Observable.create(observer => {
      this.modalHandlerObserver = observer;
    });
  }

  public openDialogModelTrainingModal(modelId: number, modelName: string) {
    const evt = new CommonModalEvent();
    evt.modalType = ModalType.DIALOG_MODEL_TRAINING_MODAL;
    evt.eventEmitter = null;
    evt.isOpen = true;
    evt.modalExtraData = {};
    evt.modalExtraData['modelId'] = modelId;
    evt.modalExtraData['modelName'] = modelName;
    this.modalHandlerObserver.next(evt);
  }

  public openAlertDialog(modalTitle: string, modalText: string, hasCancelbutton: boolean, eventEmitter?: EventEmitter<AlertModalEvent>) {
    const evt = new CommonModalEvent();
    evt.modalType = ModalType.ALERT_MESSAGE_MODAL;
    evt.eventEmitter = eventEmitter;
    evt.modalTitle = modalTitle;
    evt.modalTextMessage = modalText;
    evt.modalHasCancelButton = hasCancelbutton;
    evt.isOpen = true;
    this.modalHandlerObserver.next(evt);
  }

  public closeAlertDialog() {
    const evt = new CommonModalEvent();
    evt.modalType = ModalType.ALERT_MESSAGE_MODAL;
    evt.isOpen = false;
    this.modalHandlerObserver.next(evt);
  }

}
