import {Injectable, EventEmitter} from '@angular/core';

@Injectable()
export class EmitterService {
  public _emitters: { [channel: string]: EventEmitter<any> } = {};

  public get(channel: string): EventEmitter<any> {
    if (!this._emitters[channel]) {
      this._emitters[channel] = new EventEmitter();
    }
    return this._emitters[channel];
  }

  public emitter(channelName: string, data: any) {
    this.get(channelName).emit(data);
  }
}
