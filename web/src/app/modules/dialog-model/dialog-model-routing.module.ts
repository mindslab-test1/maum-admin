import {ModuleWithProviders, NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DialogModelsComponent} from './models/components/dialog-models.component';
import {DialogModelsNewComponent} from './models/components/dialog-models-new.component';
import {DialogModelsEditComponent} from './models/components/dialog-models-edit.component';
import {TaskComponent} from './task/components/task.component';
import {IntentComponent} from './intent/components/intent.component';
import {EntitiesComponent} from './entities/components/entities.component';
import {TestComponent} from './test/components/test.component';
import {ScriptGuideComponent} from './docs/components/script.guide.component';
import {Task2Component} from './task2/components/task2.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: DialogModelsComponent
      },
      {
        path: 'new',
        component: DialogModelsNewComponent
      },
      {
        path: ':id/edit',
        component: DialogModelsEditComponent
      },
      {
        path: ':modelId/task',
        component: TaskComponent
      },
      {
        path: ':modelId/task2',
        component: Task2Component
      },
      {
        path: ':modelId/intent',
        component: IntentComponent
      },
      {
        path: ':modelId/entity',
        component: EntitiesComponent
      },
      {
        path: ':modelId/test',
        component: TestComponent
      },
      {
        path: 'doc',
        component: ScriptGuideComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DialogModelRoutingModule {
}
