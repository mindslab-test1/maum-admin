import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Message} from 'primeng/primeng';
import {MatDialog} from '@angular/material';
import {DialogService} from '../../service/dialog.service';
import {PopupErrorComponent} from '../../shared/components/popup/popup.error.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';

@Component({
  selector: 'app-script-guide',
  templateUrl: './script.guide.component.html',
  styleUrls: ['./script.guide.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ScriptGuideComponent implements OnInit, OnDestroy {
  value: number;
  isTraining: boolean;
  visibleTraining: boolean;

  msgs: Message[] = [];
  trainingCheck: any;

  constructor(public dialog: MatDialog, private store: Store<any>,
              public dialogService: DialogService) {
    this.value = 0;
    this.isTraining = false;
    this.visibleTraining = false;

    this.trainingCheck = setInterval(() => {
      const trainId = localStorage.getItem('trainId');
      this.checkTraining(trainId);
    }, 10000);
  }

  ngOnInit() {
    this.store.dispatch({type: ROUTER_LOADED});

  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
  }

  checkTraining(trainId) {
    if (trainId !== null && trainId !== undefined) {
      this.visibleTraining = true;
      this.dialogService.getTrainingProgress(trainId).subscribe(result => {
        if (result && result.statusCode === 200) {
          if (result.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = result.data.error;
          } else {
            if (result.data.resultInfo.step === 'SDS_TRAIN_DONE') {
              this.value = 100;
              localStorage.removeItem('trainId');
              this.isTraining = false;
              this.msgs = [];
              this.msgs.push({
                severity: 'info',
                summary: 'Train Status',
                detail: result.data.resultInfo.step
              });
            } else {
              this.isTraining = true;
              if (result.data.resultInfo.value && result.data.resultInfo.maximum) {
                this.value = Math.round(Number(result.data.resultInfo.value) / Number(result.data.resultInfo.maximum) * 100);
              }
            }
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  stopTraining() {
    const trainId = localStorage.getItem('trainId');
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          if (response.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = response.data.error;
          } else {
            this.isTraining = true;
            this.value = 0;
            localStorage.removeItem('trainId');
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Train Status', detail: 'CANCELED'});
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }
}
