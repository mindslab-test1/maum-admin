import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Message, TreeNode, TreeTable} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import * as go from 'gojs';
import {GraphLinksModel} from 'gojs';
import {
  DefaultDialogTask,
  DefaultGlobalDialogTask,
  DialogModel,
  DialogService,
  DialogTask,
  ScriptGuide,
  TaskStartEvent,
  TaskEvent,
  TaskStartEventUtter,
  TaskEventCondition,
  TaskEventUtter,
  TaskEventUtterData,
  UtilService,
  EmitterService,
  DialogNextTask,
  TaskStartEventUtterData
} from '../../service';
import * as _ from 'lodash';
import {MatDialog} from '@angular/material';
import {PopupTrainComponent, PopupErrorComponent} from '../../shared/components/popup';

import {
  NODE_TYPE_EVENT,
  NODE_TYPE_START_EVENT
} from '../../_provide';
import * as $ from 'jquery';
import {NgBlockUI} from 'ng-block-ui';
import {MessageService} from 'primeng/components/common/messageservice';
import {CommonModalComponent} from '../../common/common-modal.component';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {jsPlumbUtil} from 'jsplumbtoolkit';

declare const Mark: any;

@Component({
  selector: 'app-task',
  templateUrl: 'task.component.html',
  styleUrls: ['./task.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TaskComponent implements OnInit, AfterViewInit, OnDestroy {

  user: any;
  workspace: any;

  public btnScript = '';
  private diagram: go.Diagram;
  @ViewChild('taskDialogWrapper')
  taskDialogWrapper: ElementRef;
  value: number;
  isTraining: boolean;
  visibleTraining: boolean;
  isScriptGuideOpen: boolean;

  public scriptInput: any = {
    key: '',
    script: ''
  };

  @ViewChild('scriptInputModal')
  scriptInputModal: CommonModalComponent;

  @ViewChild('scriptGuidePopup')
  scriptGuidePopup: CommonModalComponent;

  dialogModel: DialogModel;
  public modelId: string;
  public workspaceId: string;
  public isLoaded: boolean;
  public tasks: DialogTask[];

  public selectedTask: DialogTask = null;
  public globalTask: DialogTask = _.cloneDeep(DefaultGlobalDialogTask);

  private dialogTasksLinkInfo: {
    id: string,
    from: string,
    to: string,
    condition: string,
    controller: string,
    fromId: string,
    toId: string
  }[] = [];

  private _storedDialogTasks: DialogTask[] = [];
  private _storedGlobalTask: DialogTask = _.cloneDeep(DefaultGlobalDialogTask);

  public isNeedToUpdate: boolean;

  public selectedNode: TreeNode = null;
  public selectedLink: any = null;
  public scriptGuide: ScriptGuide[] = [];

  public intentListOption: {
    label: string,
    value: string
  }[];

  public RESPONSE_EVENT: any = {
    FIRST: 'first',
    SECOND: 'second'
  };

  @ViewChild('highlightDiv')
  highlightDiv: ElementRef;

  @ViewChild('writeScript')
  writeScript: ElementRef;

  @ViewChild('startEventTreeTable')
  startEventTreeTable: TreeTable;

  @ViewChild('eventTreeTable')
  eventTreeTable: TreeTable;

  @ViewChild('globalTreeTable')
  globalTreeTable: TreeTable;

  docsMarkInstance: any;

  msgs: Message[] = [];
  trainingCheck: any;

  constructor(private router: Router, private store: Store<any>,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private utilService: UtilService,
              private emitterService: EmitterService,
              public dialogService: DialogService) {

    this.workspace = {id: localStorage.getItem('m2uWorkspaceId')};
    this.user = JSON.parse(localStorage.getItem('user'));

    this.route.params.subscribe(params => {
      this.modelId = params.modelId !== ':modelId' ? params.modelId : null;
    });

    if (!this.modelId) {
      this.router.navigate(['m2u', 'dialog-models', 'dialog-models']).catch();
    } else {

      this.value = 0;
      this.isTraining = false;
      this.visibleTraining = false;
      this.isNeedToUpdate = false;
      this.isScriptGuideOpen = false;
      this.diagram = this.utilService.initGoJSDiagram();
      this.diagram.initialContentAlignment = go.Spot.Center;

      this.diagram.addDiagramListener('ChangedSelection', e => {
        const node = e.diagram.selection.first();
        if ((node !== null) && (node['isTreeLink'] !== true)) {
          this.selectedTask = node.data;
          this.selectedNode = undefined;
          this.selectedLink = null;
          this.closeEditSection();
        } else if ((node !== null) && (node['isTreeLink'] === true)) {
          this.selectedTask = null;
          this.selectedNode = undefined;
          this.selectedLink = node.data;
          const filtered = this.dialogTasksLinkInfo.filter(x => x.fromId === this.selectedLink.fromId && x.toId === this.selectedLink.toId);
          if (!this.selectedLink.hasOwnProperty('condition')) {
            Object.assign(this.selectedLink, {
              condition: (filtered.length > 0) ? filtered[0].condition : ''
            });
          }
          if (!this.selectedLink.hasOwnProperty('controller')) {
            Object.assign(this.selectedLink, {
              controller: (filtered.length > 0) ? filtered[0].controller : 'system'
            });
          }
          if (!this.selectedLink.hasOwnProperty('fromId')) {
            Object.assign(this.selectedLink, {
              fromId: (filtered.length > 0) ? filtered[0].fromId : ''
            });
          }
          if (!this.selectedLink.hasOwnProperty('toId')) {
            Object.assign(this.selectedLink, {
              toId: (filtered.length > 0) ? filtered[0].toId : ''
            });
          }
          if (!this.selectedLink.hasOwnProperty('id')) {
            Object.assign(this.selectedLink, {
              id: (filtered.length > 0) ? filtered[0].id : ''
            });
          } else {
            if (this.selectedLink.id === '' || this.selectedLink.id === undefined) {
              this.selectedLink.id = (filtered.length > 0) ? filtered[0].id : '';
            }
          }
          this.openScriptEditModal('Transition Condition', this.selectedLink.condition, false);
        } else {
          this.selectedTask = null;
          this.selectedNode = undefined;
          this.selectedLink = null;
        }

        this.diagram.requestUpdate();
      });

      this.diagram.addDiagramListener('LinkDrawn', e => {
        const node = e.diagram.selection.first();

        this.selectedTask = null;
        this.selectedNode = undefined;
        this.selectedLink = node.data;
        if (!this.selectedLink.hasOwnProperty('condition')) {
          Object.assign(this.selectedLink, {
            condition: ''
          });
        }
        if (!this.selectedLink.hasOwnProperty('controller')) {
          Object.assign(this.selectedLink, {
            controller: 'system'
          });
        }
        if (!this.selectedLink.hasOwnProperty('fromId')) {
          Object.assign(this.selectedLink, {
            fromId: node['fromNode'].data.id
          });
        }
        if (!this.selectedLink.hasOwnProperty('toId')) {
          Object.assign(this.selectedLink, {
            toId: node['toNode'].data.id
          });
        }
        if (!this.selectedLink.hasOwnProperty('id')) {
          Object.assign(this.selectedLink, {
            id: ''
          });
        }
        this.selectedLink.fromId = node['fromNode'].data.id;
        this.selectedLink.toId = node['toNode'].data.id;

        const taskIdx = this.tasks.findIndex(x => x.id === this.selectedLink.fromId);
        const nextTaskIdx = this.tasks.findIndex(x => x.id === this.selectedLink.toId);
        const nextTask = new DialogNextTask();
        nextTask.id = this.dialogService.getUUID();
        nextTask.controller = this.selectedLink.controller;
        nextTask.nextTaskCondition = this.selectedLink.condition;
        nextTask.creatorId = this.user.id;
        nextTask.nextTaskId = this.tasks[nextTaskIdx].id;
        nextTask.taskId = this.tasks[taskIdx].id;
        this.dialogService.insertDialogNextTask(this.workspace.id, this.modelId, nextTask).subscribe(response => {
          if (response && response.statusCode === 200) {
            const task = this.tasks.find(x => x.id === nextTask.taskId);
            if (task) {
              task.nextTask.push(nextTask);
            }
            this.createLinkInfoFromDialogTasks();
            this.selectedLink.id = nextTask.id;
            this.openScriptEditModal('Transition Condition', this.selectedLink.condition, false);
          }
        }, error => {
          this.errorMessage(error);
        });
      });

      this.diagram.addDiagramListener('BackgroundSingleClicked', e => {
        if ((this.selectedTask !== null) && (this.selectedTask.taskName === this.globalTask.taskName)) {
          this.selectedTask = null;
          this.selectedNode = undefined;
          this.selectedLink = null;
          this.closeEditSection();
        }
      });

      this.diagram.addModelChangedListener(e => {
        if (e['propertyName'] !== 'loc') {
          this.syncTaskDiagram();
        }
      });

      this.diagram.addDiagramListener('SelectionMoved', e => {
        this.saveTask();
      });

      this.diagram.addDiagramListener('SelectionDeleting', e => {
        this.removeTask();
      });

      this.isLoaded = false;

      this.dialogService.getDialogModel(this.workspace.id, this.modelId).subscribe(res => {
        if (res && res.statusCode === 200) {
          this.dialogModel = res.data.modelInfo;
        }
      }, error => {
        this.errorMessage(error);
      });

      if (this.dialogModel === null) {
        this.router.navigate(['m2u', 'dialog-models', 'dialog-models']).catch();
        return;
      }

      this.trainingCheck = setInterval(() => {
        const trainId = localStorage.getItem('trainId');
        this.checkTraining(trainId);
      }, 10000);
    }
  }

  ngOnInit() {
    this.selectAllTask();
    this.dialogService.getScriptGuide().subscribe((result: ScriptGuide[]) => {
      this.scriptGuide = this.scriptGuide.concat(result);
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.diagram.div = this.taskDialogWrapper.nativeElement;

    // this.docsMarkInstance = new Mark(this.highlightDiv.nativeElement);
    // this.docsMarkInstance.unmark({
    //   done: () => {
    //     this.docsMarkInstance.mark(['dolor', 'ipsum', 'IsDAType'], {
    //       separateWordSearch: true,
    //       diacritics: true,
    //       element: 'mark'
    //     });
    //   }
    // });
  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
  }

  checkTraining(trainId) {
    if (trainId !== null && trainId !== undefined) {
      this.visibleTraining = true;
      this.dialogService.getTrainingProgress(trainId).subscribe(result => {
        if (result && result.statusCode === 200) {
          if (result.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = result.data.error;
          } else {
            if (result.data.resultInfo.step === 'SDS_TRAIN_DONE') {
              this.value = 100;
              localStorage.removeItem('trainId');
              this.isTraining = false;
              this.msgs = [];
              this.msgs.push({
                severity: 'info',
                summary: 'Train Status',
                detail: result.data.resultInfo.step
              });
            } else {
              this.isTraining = true;
              if (result.data.resultInfo.value && result.data.resultInfo.maximum) {
                this.value = Math.round(Number(result.data.resultInfo.value) / Number(result.data.resultInfo.maximum) * 100);
              }
            }
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  stopTraining() {
    const trainId = localStorage.getItem('trainId');
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          if (response.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = response.data.error;
          } else {
            this.isTraining = true;
            this.value = 0;
            localStorage.removeItem('trainId');
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Train Status', detail: 'CANCELED'});
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  selectAllTask() {
    this.dialogService.getDialogModelTaskAll(this.workspace.id, this.modelId).subscribe(tResponse => {
      this.tasks = [];
      if (tResponse && tResponse.statusCode === 200) {
        this.tasks = tResponse.data.task;
        if (this.tasks.length > 0) {
          for (const task of this.tasks) {
            task.key = task.taskName;
            task.loc = task.taskLocation;
          }
        }
        this._storedDialogTasks = tResponse.data.task;
        const generalTask = this.tasks.filter((val) => (val.taskName !== 'GLOBAL_TASK'));
        this.createLinkInfoFromDialogTasks();
        this.diagram.model = new go.GraphLinksModel(generalTask, this.dialogTasksLinkInfo);

        const globalTasks = this.tasks.filter((val) => (val.taskName === 'GLOBAL_TASK'));
        if (globalTasks.length > 0) {
          this._storedGlobalTask = _.cloneDeep(globalTasks[0]);
          this.globalTask = _.cloneDeep(globalTasks[0]);
        } else {
          this._storedGlobalTask = _.cloneDeep(DefaultGlobalDialogTask);
          this.globalTask = _.cloneDeep(DefaultGlobalDialogTask);
        }

        const initTasks: DialogTask[] = [];
        const initGlobalTask: DialogTask = {
          id: this.globalTask.id,
          taskName: this.globalTask.taskName,
          taskGoal: this.globalTask.taskGoal,
          taskType: this.globalTask.taskType,
          resetSlot: this.globalTask.resetSlot,
          creatorId: this.globalTask.creatorId,
          updaterId: this.globalTask.updaterId,
          workspaceId: this.globalTask.workspaceId,
          modelId: this.globalTask.modelId,
          startEvent: this.globalTask.startEvent,
          event: this.globalTask.event,
          nextTask: this.globalTask.nextTask,
          key: this.globalTask.key,
          taskLocation: this.globalTask.taskLocation,
          loc: this.globalTask.taskLocation
        };
        initTasks.push(initGlobalTask);

        for (const nodeData of this.diagram.model.nodeDataArray) {
          const tempTask: DialogTask = {
            id: nodeData['id'],
            taskName: nodeData['taskName'],
            taskGoal: nodeData['taskGoal'],
            taskType: nodeData['taskType'],
            resetSlot: nodeData['resetSlot'],
            creatorId: nodeData['creatorId'],
            updaterId: nodeData['updaterId'],
            workspaceId: nodeData['workspaceId'],
            modelId: nodeData['modelId'],
            startEvent: nodeData['startEvent'],
            event: nodeData['event'],
            nextTask: nodeData['nextTask'],
            key: nodeData['key'],
            taskLocation: nodeData['loc'],
            loc: nodeData['loc']
          };
          initTasks.push(tempTask);
        }
        localStorage.setItem('init_tasks', JSON.stringify(initTasks));

        this.intentListOption = [];
        this.intentListOption.push({
          label: '',
          value: ''
        });
        this.dialogService.getDialogModelIntents(this.workspace.idId, this.modelId).subscribe(iResponse => {
          if (iResponse && iResponse.statusCode === 200) {
            const intents = iResponse.data.intentInfoList;
            if (intents.length > 0) {
              for (const intent of intents) {
                this.intentListOption.push({
                  label: intent.intentName,
                  value: intent.id
                });
              }
            }
            this.isLoaded = true;
            console.log('intentListOption :: ', this.intentListOption);
          }
        }, error => {
          const errorJson = error.json();
          if (errorJson.statusCode === 400) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = errorJson.statusMessage;
          } else {
            this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
          }
        });
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  @HostListener('window:resize')
  windowResized() {
    this.diagram.requestUpdate();
  }

  public createNewTask() {
    if (this.isLoaded) {
      if (this.checkModelCreator()) {
        const newTask = _.cloneDeep(DefaultDialogTask);
        let newName = 'New_Task';
        let founded = false;
        let retryCnt = 1;
        while (founded === false) {
          const f = this.diagram.model.nodeDataArray.filter((val) => (val['key'] === newName));
          if (f.length === 0) {
            founded = true;
          } else {
            newName = 'New_Task' + '_' + retryCnt;
            retryCnt++;
          }
        }
        newTask['key'] = newName;
        newTask['taskName'] = newName;
        newTask['id'] = jsPlumbUtil.uuid();
        newTask['creatorId'] = this.user.id;
        newTask['taskGoal'] = '';
        const viewportBounds = this.diagram.viewportBounds;
        const randomX = Math.random() * (0 - viewportBounds.x) + viewportBounds.x;
        const randomY = Math.random() * (0 - viewportBounds.y) + viewportBounds.y;
        newTask['loc'] = randomX + ' ' + randomY;
        newTask['workspaceId'] = this.workspace.id;
        newTask['modelId'] = this.modelId;
        this.addTask(newTask);
      }
    }
  }

  public addTask(newTask: DialogTask) {
    this.dialogService.insertDialogModelTask(this.workspace.id, this.modelId, newTask).subscribe(response => {
      if (response && response.statusCode === 200) {
        this.tasks = this.tasks.concat(newTask);
        this._storedDialogTasks.push(_.cloneDeep(newTask)); // jslee
        this.diagram.model.addNodeData(newTask);
        this.diagram.requestUpdate();
        this.diagram.selectCollection([this.diagram.findNodeForKey(newTask['taskName'])]);
        this.createLinkInfoFromDialogTasks();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  public saveTask() {
    if (this.isLoaded && this.selectedTask) {
      if (this.checkModelCreator()) {
        const dataArray = this.diagram.model.nodeDataArray as DialogTask[];
        const filtered = dataArray.filter(x => x.taskName === this.selectedTask.taskName && x.id !== this.selectedTask.id);
        if (filtered.length > 0) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '45%',
            disableClose: true,
          });
          ref.componentInstance.title = '경고';
          ref.componentInstance.message = '이미 다른 Task가 사용중입니다.';
          let count = this.diagram.model.undoManager.history.count;
          while (count >= 0) {
            this.diagram.model.undoManager.undo();
            count--;
          }
          this.selectedTask.taskName = this.diagram.selection.first().key.toString();
        } else {
          this.dialogService.updateDialogModelTask(this.workspace.id, this.modelId, this.selectedTask).subscribe(response => {
            if (response && response.statusCode === 200) {
              if (this.selectedTask.taskName === 'GLOBAL_TASK') {
                this._storedGlobalTask = _.cloneDeep(this.selectedTask);
                this.globalTask = _.cloneDeep(this.selectedTask);
              } else {
                this._storedDialogTasks.push(_.cloneDeep(this.selectedTask)); // jslee
                this.createLinkInfoFromDialogTasks();
              }
              this.diagram.model.commitTransaction();
            }
          }, error => {
            let count = this.diagram.model.undoManager.history.count;
            while (count >= 0) {
              this.diagram.model.undoManager.undo();
              count--;
            }
            this.selectedTask.taskName = this.diagram.selection.first().key.toString();
            this.errorMessage(error);
          });
        }
      }
    }
  }

  public removeTask() {
    if (this.checkModelCreator()) {
      if ((this.selectedTask !== null) && (this.selectedTask.taskName !== this.globalTask.taskName)) {
        this.dialogService.deleteDialogModelTask(this.workspace.id, this.modelId, this.selectedTask.id).subscribe(response => {
          if (response && response.statusCode === 200) {
            if (this.diagram.selection.first() !== null) {
              const linkModel = this.diagram.model as GraphLinksModel;
              const filtered = this.dialogTasksLinkInfo.filter(x => x.fromId === this.selectedTask.id || x.toId === this.selectedTask.id);
              for (const linkData of filtered) {
                const data = linkModel.linkDataArray.find(x => x['id'] === linkData.id);
                linkModel.removeLinkData(data);
              }
              this.diagram.model.removeNodeData(this.diagram.selection.first().data);
            }
            this.selectedTask = null;
            this.selectedNode = undefined;
            this.selectedLink = null;

            this.closeEditSection();
          }
        }, error => {
          this.errorMessage(error);
        });
      } else if ((this.selectedLink !== null) && (this.selectedLink !== undefined)) {
        const taskId = this.tasks.find(x => x.taskName === this.selectedLink.from).id;
        this.dialogService.deleteDialogNextTask(this.workspace.id, this.modelId, taskId, this.selectedLink.id).subscribe(res => {
          if (res && res.statusCode === 200) {
            if (this.diagram.selection.first() !== null) {
              const linkModel = this.diagram.model as GraphLinksModel;
              linkModel.removeLinkData(this.diagram.selection.first().data);
            }
            this.syncTaskDiagram();
            this.diagram.requestUpdate();
            this.selectedTask = null;
            this.selectedNode = undefined;
            this.selectedLink = null;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  public onTaskNameChanged() {
    const selectedNode = this.diagram.selection.first();
    if (selectedNode !== null) {
      this.diagram.model.setDataProperty(selectedNode.data, 'key', selectedNode.data.taskName);
    }
  }

  public addTaskResponse(isStartEvnet: boolean, isGlobalTask: boolean) {
    if (this.checkModelCreator()) {
      let newIdx = 0;
      if (isStartEvnet) {
        if (isGlobalTask) {
          console.log(this.selectedTask);
          if (this.globalTreeTable.value.length > 0) {
            const sortedResponse = this.globalTreeTable.value.sort((a, b) => {
              if (a.data.index > b.data.index) {
                return 1;
              } else if (a.data.index < b.data.index) {
                return -1;
              } else {
                return 0;
              }
            });
            const lastResponse = sortedResponse[sortedResponse.length - 1];
            newIdx = lastResponse.data.index + 1;
          } else {
            this.selectedTask.startEvent = [];
          }
        } else {
          if (this.startEventTreeTable.value.length > 0) {
            const sortedResponse = this.startEventTreeTable.value.sort((a, b) => {
              if (a.data.index > b.data.index) {
                return 1;
              } else if (a.data.index < b.data.index) {
                return -1;
              } else {
                return 0;
              }
            });
            const lastResponse = sortedResponse[sortedResponse.length - 1];
            newIdx = lastResponse.data.index + 1;
          } else {
            this.selectedTask.startEvent = [];
          }
        }
        if (this.selectedNode && this.selectedNode.data.type === 'UTTERANCE') {
          const startEventUtter = new TaskStartEventUtter();
          startEventUtter.id = this.dialogService.getUUID();
          startEventUtter.startEventId = this.selectedNode.data.original.startEventId;
          startEventUtter.startEventUtterData = [];
          startEventUtter.creatorId = this.user.id;
          startEventUtter.extraData = '';
          startEventUtter.intentId = '';
          this.dialogService.insertDialogModelTaskStartEventUtter(
            this.workspace.id, this.modelId, this.selectedTask.id, startEventUtter).subscribe(uResponse => {
            if (uResponse && uResponse.statusCode === 200) {
              const startEventId = this.selectedNode.data.original.startEventId;
              const idx = this.selectedTask.startEvent.findIndex(x => x.id === startEventId);
              this.selectedTask.startEvent[idx].startEventUtter.push(startEventUtter);
              const utter = this.createTreeNode(this.selectedTask.startEvent[idx].startEventUtter,
                startEventUtter, NODE_TYPE_START_EVENT.UTTERANCE.toString(), 'utterance', isGlobalTask);
              const utterNode = {
                parent: this.selectedNode.parent,
                data: utter.data,
                children: []
              };
              if (isGlobalTask) {
                this.globalTreeTable.value.find(x => x.data.original.id === startEventId).children.push(utterNode);
              } else {
                this.startEventTreeTable.value.find(x => x.data.original.id === startEventId).children.push(utterNode);
              }

              const startEventUtterData = new TaskStartEventUtterData();
              startEventUtterData.id = this.dialogService.getUUID();
              startEventUtterData.startEventUtterId = this.selectedNode.data.original.id;
              startEventUtterData.creatorId = this.user.id;
              startEventUtterData.startEventUtterData = '';
              this.dialogService.insertDialogModelTaskStartEventUtterData(
                this.workspace.id, this.modelId, this.selectedTask.id, startEventId, startEventUtterData).subscribe(res => {
                if (res && res.statusCode === 200) {
                  const startEventUtterIdx = this.selectedTask.startEvent[idx].startEventUtter.findIndex(x => x.id === startEventUtter.id);
                  this.selectedTask.startEvent[idx].startEventUtter[startEventUtterIdx].startEventUtterData.push(startEventUtterData);
                }
              }, error => {
                this.errorMessage(error);
              });
            }
          }, error => {
            this.errorMessage(error);
          });
        } else {
          const startEvent = new TaskStartEvent();
          startEvent.id = this.dialogService.getUUID();
          startEvent.creatorId = this.user.id;
          if (isGlobalTask) {
            startEvent.taskId = this.globalTask.id;
          } else {
            startEvent.taskId = this.selectedTask.id;
          }
          startEvent.startEventAction = '';
          startEvent.startEventCondition = '';
          startEvent.startEventUtter = [];
          startEvent.index = newIdx;
          this.dialogService.insertDialogModelTaskStartEvent(this.workspace.id, this.modelId, startEvent).subscribe(eResponse => {
            if (eResponse && eResponse.statusCode === 200) {
              const startEventUtter = new TaskStartEventUtter();
              startEventUtter.id = this.dialogService.getUUID();
              startEventUtter.startEventId = startEvent.id;
              startEventUtter.startEventUtterData = [];
              startEventUtter.creatorId = this.user.id;
              startEventUtter.extraData = '';
              startEventUtter.intentId = '';
              this.dialogService.insertDialogModelTaskStartEventUtter(
                this.workspace.id, this.modelId, this.selectedTask.id, startEventUtter).subscribe(uResponse => {
                if (uResponse && uResponse.statusCode === 200) {
                  const startEventUtterData = new TaskStartEventUtterData();
                  startEventUtterData.id = this.dialogService.getUUID();
                  startEventUtterData.startEventUtterId = startEventUtter.id;
                  startEventUtterData.creatorId = this.user.id;
                  startEventUtterData.startEventUtterData = '';
                  this.dialogService.insertDialogModelTaskStartEventUtterData(
                    this.workspace.id, this.modelId, this.selectedTask.id, startEvent.id, startEventUtterData).subscribe(res => {
                    if (res && res.statusCode === 200) {
                      startEventUtter.startEventUtterData.push(startEventUtterData);
                      startEvent.startEventUtter.push(startEventUtter);
                      this.selectedTask.startEvent.push(startEvent);
                      const utter = this.createTreeNode(startEvent.startEventUtter,
                        startEventUtter, NODE_TYPE_START_EVENT.UTTERANCE.toString(), 'utterance', isGlobalTask);
                      const condition = this.createTreeNode(this.selectedTask.startEvent,
                        startEvent, NODE_TYPE_START_EVENT.CONDITION.toString(), 'condition', isGlobalTask);
                      const conditionNode = {
                        parent: undefined,
                        data: condition.data,
                        children: []
                      };
                      const utterNode = {
                        parent: conditionNode,
                        data: utter.data,
                        children: []
                      };
                      conditionNode.children.push(utterNode);
                      if (isGlobalTask) {
                        this.globalTreeTable.value.push(conditionNode);
                      } else {
                        this.startEventTreeTable.value.push(conditionNode);
                      }
                    }
                  }, error => {
                    this.errorMessage(error);
                  });
                }
              }, error => {
                this.errorMessage(error);
              });
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      } else {
        if (this.eventTreeTable.value.length > 0) {
          const sortedResponse = this.eventTreeTable.value.sort((a, b) => {
            if (a.data.index > b.data.index) {
              return 1;
            } else if (a.data.index < b.data.index) {
              return -1;
            } else {
              return 0;
            }
          });
          const lastResponse = sortedResponse[sortedResponse.length - 1];
          newIdx = lastResponse.data.index + 1;
        } else {
          this.selectedTask.event = [];
        }
        if (!this.selectedNode || this.selectedNode.data.type === 'INTENTION') {
          const event = new TaskEvent();
          event.id = this.dialogService.getUUID();
          event.creatorId = this.user.id;
          event.taskId = this.selectedTask.id;
          event.eventCondition = [];
          event.intentId = '';
          event.index = newIdx;
          this.dialogService.insertDialogModelTaskEvent(this.workspace.id, this.modelId, event).subscribe(eRes => {
            if (eRes && eRes.statusCode === 200) {
              const eventCondition = new TaskEventCondition();
              eventCondition.id = this.dialogService.getUUID();
              eventCondition.eventUtter = [];
              eventCondition.creatorId = this.user.id;
              eventCondition.eventAction = '';
              eventCondition.eventCondition = '';
              eventCondition.extraData = '';
              eventCondition.eventId = event.id;
              this.dialogService.insertDialogModelTaskEventCondition(
                this.workspace.id, this.modelId, this.selectedTask.id, eventCondition).subscribe(cRes => {
                if (cRes && cRes.statusCode === 200) {
                  const eventUtter = new TaskEventUtter();
                  eventUtter.id = this.dialogService.getUUID();
                  eventUtter.eventConditionId = eventCondition.id;
                  eventUtter.eventUtterData = [];
                  eventUtter.creatorId = this.user.id;
                  eventUtter.extraData = '';
                  eventUtter.intentId = '';
                  this.dialogService.insertDialogModelTaskEventUtter(
                    this.workspace.id, this.modelId, this.selectedTask.id, event.id, eventUtter).subscribe(uRes => {
                    if (uRes && uRes.statusCode === 200) {
                      const eventUtterData = new TaskEventUtterData();
                      eventUtterData.id = this.dialogService.getUUID();
                      eventUtterData.eventUtterId = eventUtter.id;
                      eventUtterData.creatorId = this.user.id;
                      eventUtterData.eventUtterData = '';
                      this.dialogService.insertDialogModelTaskEventUtterData(
                        this.workspace.id, this.modelId, this.selectedTask.id,
                        event.id, eventCondition.id, eventUtterData).subscribe(res => {
                        if (res && res.statusCode === 200) {
                          eventUtter.eventUtterData.push(eventUtterData);
                          eventCondition.eventUtter.push(eventUtter);
                          event.eventCondition.push(eventCondition);
                          this.selectedTask.event.push(event);
                          const eventData = this.createTreeNode(this.selectedTask.event, event,
                            NODE_TYPE_EVENT.INTENTION.toString(), 'intention', isGlobalTask);
                          const eventNode = {
                            parent: undefined,
                            data: eventData.data,
                            children: []
                          };
                          const conditionData = this.createTreeNode(event.eventCondition, eventCondition,
                            NODE_TYPE_EVENT.CONDITION.toString(), 'condition', isGlobalTask);
                          const conditionNode = {
                            parent: eventNode,
                            data: conditionData.data,
                            children: []
                          };
                          const utterData = this.createTreeNode(eventCondition.eventUtter, eventUtter,
                            NODE_TYPE_EVENT.UTTERANCE.toString(), 'utterance', isGlobalTask);
                          const utterNode = {
                            parent: conditionNode,
                            data: utterData.data,
                            children: []
                          };
                          conditionNode.children.push(utterNode);
                          eventNode.children.push(conditionNode);
                          this.eventTreeTable.value.push(eventNode);
                        }
                      }, error => {
                        this.errorMessage(error);
                      });
                    }
                  }, error => {
                    this.errorMessage(error);
                  });
                }
              }, error => {
                this.errorMessage(error);
              });
            }
          }, error => {
            this.errorMessage(error);
          });
        } else {
          if (this.selectedNode.data.type === 'CONDITION') {
            const event = this.selectedNode.parent.data.original;
            const eventCondition = new TaskEventCondition();
            eventCondition.id = this.dialogService.getUUID();
            eventCondition.eventId = this.selectedNode.data.original.eventId;
            eventCondition.eventUtter = [];
            eventCondition.creatorId = this.user.id;
            eventCondition.extraData = '';
            eventCondition.eventCondition = '';
            eventCondition.eventAction = '';
            this.dialogService.insertDialogModelTaskEventCondition(
              this.workspace.id, this.modelId, this.selectedTask.id, eventCondition).subscribe(cRes => {
              if (cRes && cRes.statusCode === 200) {
                const eventUtter = new TaskEventUtter();
                eventUtter.id = this.dialogService.getUUID();
                eventUtter.eventConditionId = eventCondition.id;
                eventUtter.eventUtterData = [];
                eventUtter.creatorId = this.user.id;
                eventUtter.extraData = '';
                eventUtter.intentId = '';
                this.dialogService.insertDialogModelTaskEventUtter(
                  this.workspace.id, this.modelId, this.selectedTask.id, event.id, eventUtter).subscribe(uRes => {
                  if (uRes && uRes.statusCode === 200) {
                    const eventUtterData = new TaskEventUtterData();
                    eventUtterData.id = this.dialogService.getUUID();
                    eventUtterData.eventUtterId = eventUtter.id;
                    eventUtterData.creatorId = this.user.id;
                    eventUtterData.eventUtterData = '';
                    this.dialogService.insertDialogModelTaskEventUtterData(
                      this.workspace.id, this.modelId, this.selectedTask.id,
                      eventCondition.eventId, eventCondition.id, eventUtterData).subscribe(res => {
                      if (res && res.statusCode === 200) {
                        eventUtter.eventUtterData.push(eventUtterData);
                        eventCondition.eventUtter.push(eventUtter);
                        event.eventCondition.push(eventCondition);
                        const conditionData = this.createTreeNode(event.eventCondition, eventCondition,
                          NODE_TYPE_START_EVENT.CONDITION.toString(), 'condition', isGlobalTask);
                        const conditionNode = {
                          parent: this.selectedNode.parent,
                          data: conditionData.data,
                          children: []
                        };
                        const utterData = this.createTreeNode(eventCondition.eventUtter, eventUtter,
                          NODE_TYPE_START_EVENT.UTTERANCE.toString(), 'utterance', isGlobalTask);
                        const utterNode = {
                          parent: conditionNode,
                          data: utterData.data,
                          children: []
                        };
                        conditionNode.children.push(utterNode);
                        this.eventTreeTable.value.find(x => x.data.original.id === event.id).children.push(conditionNode);
                      }
                    }, error => {
                      this.errorMessage(error);
                    });
                  }
                }, error => {
                  this.errorMessage(error);
                });
              }
            }, error => {
              this.errorMessage(error);
            });
          } else {
            const event = this.selectedNode.parent.parent.data.original;
            const eventIdx = this.selectedNode.parent.parent.data.index;
            let eventCondition = new TaskEventCondition();
            eventCondition = this.selectedNode.parent.data.original;
            const eventUtter = new TaskEventUtter();
            eventUtter.id = this.dialogService.getUUID();
            eventUtter.eventConditionId = eventCondition.id;
            eventUtter.eventUtterData = [];
            eventUtter.creatorId = this.user.id;
            eventUtter.extraData = '';
            eventUtter.intentId = '';
            this.dialogService.insertDialogModelTaskEventUtter(
              this.workspace.id, this.modelId, this.selectedTask.id, event.id, eventUtter).subscribe(uRes => {
              if (uRes && uRes.statusCode === 200) {
                const eventUtterData = new TaskEventUtterData();
                eventUtterData.id = this.dialogService.getUUID();
                eventUtterData.eventUtterId = eventUtter.id;
                eventUtterData.creatorId = this.user.id;
                eventUtterData.eventUtterData = '';
                this.dialogService.insertDialogModelTaskEventUtterData(
                  this.workspace.id, this.modelId, this.selectedTask.id, event.id, eventCondition.id, eventUtterData).subscribe(res => {
                  if (res && res.statusCode === 200) {
                    eventUtter.eventUtterData.push(eventUtterData);
                    eventCondition.eventUtter = eventCondition.eventUtter.concat(eventUtter);
                    event.eventCondition = event.eventCondition.concat(eventCondition);
                    const utter = this.createTreeNode(eventCondition.eventUtter, eventUtter,
                      this.selectedNode.data.type, 'utterance', isGlobalTask);
                    const utterNode = {
                      parent: this.selectedNode.parent,
                      data: utter.data,
                      children: []
                    };
                    const conditionIdx =
                      this.eventTreeTable.value[eventIdx].children.findIndex(x => x.data.original.id === eventCondition.id);
                    this.eventTreeTable.value[eventIdx].children[conditionIdx].children.push(utterNode);
                  }
                }, error => {
                  this.errorMessage(error);
                });
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        }
      }
    }
  }

  public removeTaskResponse(isStartEvnet: boolean, isGlobalTask: boolean) {
    if (this.checkModelCreator()) {
      if (this.selectedNode && this.selectedNode !== null) {
        if (isStartEvnet) {
          if (this.selectedNode.data.type === 'CONDITION') {
            const startEvent = this.selectedNode.data.original;
            this.dialogService.deleteDialogModelTaskStartEvent(
              this.workspace.id, this.modelId, startEvent).subscribe(response => {
              if (response && response.statusCode === 200) {
                if (isGlobalTask) {
                  const conditionIdx = this.globalTreeTable.value.findIndex(x => x.data.original.id === startEvent.id);
                  this.globalTreeTable.value.splice(conditionIdx, 1);
                } else {
                  const conditionIdx = this.startEventTreeTable.value.findIndex(x => x.data.original.id === startEvent.id);
                  this.startEventTreeTable.value.splice(conditionIdx, 1);
                }
                this.selectedNode = undefined;
                this.closeEditSection();
              }
            }, error => {
              this.errorMessage(error);
            });
          } else {
            if (this.selectedNode.parent.children.length > 1) {
              const startEventUtter = this.selectedNode.data.original;
              const taskId = this.selectedNode.parent.data.original.taskId;
              this.dialogService.deleteDialogModelTaskStartEventUtter(
                this.workspace.id, this.modelId, taskId, startEventUtter).subscribe(response => {
                if (response && response.statusCode === 200) {
                  const startEventId = this.selectedNode.parent.data.original.id;
                  if (isGlobalTask) {
                    const conditionIdx = this.globalTreeTable.value.findIndex(x => x.data.original.id === startEventId);
                    const utteranceIdx = this.globalTreeTable.value[conditionIdx].children
                    .findIndex(x => x.data.original.id === this.selectedNode.data.original.id);
                    this.globalTreeTable.value[conditionIdx].children.splice(utteranceIdx, 1);
                  } else {
                    const conditionIdx = this.startEventTreeTable.value.findIndex(x => x.data.original.id === startEventId);
                    const utteranceIdx = this.startEventTreeTable.value[conditionIdx].children
                    .findIndex(x => x.data.original.id === this.selectedNode.data.original.id);
                    this.startEventTreeTable.value[conditionIdx].children.splice(utteranceIdx, 1);
                  }
                  this.selectedNode = this.selectedNode.parent;
                  this.closeEditSection();
                }
              }, error => {
                this.errorMessage(error);
              });
            } else {
              const startEvent = this.selectedNode.parent.data.original;
              this.dialogService.deleteDialogModelTaskStartEvent(
                this.workspace.id, this.modelId, startEvent).subscribe(response => {
                if (response && response.statusCode === 200) {
                  if (isGlobalTask) {
                    const idx = this.globalTreeTable.value.findIndex(x => x.data.original.id === startEvent.id);
                    this.globalTreeTable.value.splice(idx, 1);
                  } else {
                    const idx = this.startEventTreeTable.value.findIndex(x => x.data.original.id === startEvent.id);
                    this.startEventTreeTable.value.splice(idx, 1);
                  }
                  this.selectedNode = undefined;
                  this.closeEditSection();
                }
              }, error => {
                this.errorMessage(error);
              });
            }
          }
        } else {
          if (this.selectedNode.data.type === 'INTENTION') {
            const event = this.selectedNode.data.original;
            this.dialogService.deleteDialogModelTaskEvent(
              this.workspace.id, this.modelId, event).subscribe(response => {
              if (response && response.statusCode === 200) {
                const idx = this.eventTreeTable.value.findIndex(x => x.data.original.id === event.id);
                this.eventTreeTable.value.splice(idx, 1);
                this.selectedNode = undefined;
                this.closeEditSection();
              }
            }, error => {
              this.errorMessage(error);
            });
          } else {
            if (this.selectedNode.data.type === 'CONDITION') {
              const eventCondition = this.selectedNode.data.original;
              const event = this.selectedNode.parent.data.original;
              if (this.selectedNode.parent.children.length > 1) {
                this.dialogService.deleteDialogModelTaskEventCondition(
                  this.workspace.id, this.modelId, event.taskId, eventCondition).subscribe(res => {
                  if (res && res.statusCode === 200) {
                    const eventIdx = this.eventTreeTable.value.findIndex(x => x.data.original.id === event.id);
                    const conditionIdx =
                      this.eventTreeTable.value[eventIdx].children.findIndex(x => x.data.original.id === eventCondition.id);
                    this.eventTreeTable.value[eventIdx].children.splice(conditionIdx, 1);
                    this.selectedNode = this.selectedNode.parent;
                    this.closeEditSection();
                  }
                }, error => {
                  this.errorMessage(error);
                });
              } else {
                this.dialogService.deleteDialogModelTaskEvent(
                  this.workspace.id, this.modelId, event).subscribe(response => {
                  if (response && response.statusCode === 200) {
                    const eventIdx = this.eventTreeTable.value.findIndex(x => x.data.original.id === event.id);
                    this.eventTreeTable.value.splice(eventIdx, 1);
                    this.selectedNode = undefined;
                    this.closeEditSection();
                  }
                }, error => {
                  this.errorMessage(error);
                });
              }
            } else {
              const event = this.selectedNode.parent.parent.data.original;
              const condition = this.selectedNode.parent.data.original;
              const eventUtter = this.selectedNode.data.original;
              const taskId = this.selectedTask.id;
              if (this.selectedNode.parent.children.length > 1) {
                this.dialogService.deleteDialogModelTaskEventUtter(
                  this.workspace.id, this.modelId, taskId, event.id, eventUtter).subscribe(res => {
                  if (res && res.statusCode === 200) {
                    const eventIdx = this.eventTreeTable.value.findIndex(x => x.data.original.id === event.id);
                    const conditionIdx = this.eventTreeTable.value[eventIdx].children.findIndex(x => x.data.original.id === condition.id);
                    const utterIdx = this.eventTreeTable.value[eventIdx].children[conditionIdx].children
                    .findIndex(x => x.data.original.id === eventUtter.id);
                    this.eventTreeTable.value[eventIdx].children[conditionIdx].children.splice(utterIdx, 1);
                    this.selectedNode = this.selectedNode.parent;
                    this.closeEditSection();
                  }
                }, error => {
                  this.errorMessage(error);
                });
              } else {
                if (this.selectedNode.parent.parent.children.length > 1) {
                  this.dialogService.deleteDialogModelTaskEventCondition(
                    this.workspace.id, this.modelId, event.taskId, condition).subscribe(res => {
                    if (res && res.statusCode === 200) {
                      const eventIdx = this.eventTreeTable.value.findIndex(x => x.data.original.id === event.id);
                      const conditionIdx = this.eventTreeTable.value[eventIdx].children
                      .findIndex(x => x.data.original.id === condition.id);
                      this.eventTreeTable.value[eventIdx].children.splice(conditionIdx, 1);
                      this.selectedNode = this.selectedNode.parent;
                      this.closeEditSection();
                    }
                  }, error => {
                    this.errorMessage(error);
                  });
                } else {
                  this.dialogService.deleteDialogModelTaskEvent(
                    this.workspace.id, this.modelId, event).subscribe(response => {
                    if (response && response.statusCode === 200) {
                      const eventIdx = this.eventTreeTable.value.findIndex(x => x.data.original.id === event.id);
                      this.eventTreeTable.value.splice(eventIdx, 1);
                      this.selectedNode = undefined;
                      this.closeEditSection();
                    }
                  }, error => {
                    this.errorMessage(error);
                  });
                }
              }
            }
          }
        }
      }
    }
  }

  public addUtterance(isStartEvent) {
    if (this.checkModelCreator()) {
      if (isStartEvent) {
        const utterData = new TaskStartEventUtterData();
        utterData.id = this.dialogService.getUUID();
        utterData.startEventUtterId = this.selectedNode.data.original.id;
        utterData.creatorId = this.user.id;
        utterData.startEventUtterData = '';
        const startEventId = this.selectedNode.data.original.startEventId;
        this.dialogService.insertDialogModelTaskStartEventUtterData(
          this.workspace.id, this.modelId, this.selectedTask.id, startEventId, utterData).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedNode.data.original.startEventUtterData = this.selectedNode.data.original.startEventUtterData.concat(utterData);
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        const utterData = new TaskEventUtterData();
        utterData.id = this.dialogService.getUUID();
        utterData.eventUtterId = this.selectedNode.data.original.id;
        utterData.creatorId = this.user.id;
        utterData.eventUtterData = '';
        const eventId = this.selectedNode.parent.data.original.eventId;
        const eventConditionId = this.selectedNode.data.original.eventConditionId;
        this.dialogService.insertDialogModelTaskEventUtterData(
          this.workspace.id, this.modelId, this.selectedTask.id, eventId, eventConditionId, utterData).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedNode.data.original.eventUtterData = this.selectedNode.data.original.eventUtterData.concat(utterData);
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  public removeUtterance(idx, isStartEvent) {
    if (this.checkModelCreator()) {
      if (isStartEvent) {
        const startEventUtterData = this.selectedNode.data.original.startEventUtterData[idx];
        const startEventId = this.selectedNode.data.original.startEventId;
        this.dialogService.deleteDialogModelTaskStartEventUtterData(
          this.workspace.id, this.modelId, this.selectedTask.id, startEventId, startEventUtterData).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedNode.data.original.startEventUtterData.splice(idx, 1);
            this.selectedNode.data.original.startEventUtterData = this.selectedNode.data.original.startEventUtterData.concat();
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        const eventUtterData = this.selectedNode.data.original.eventUtterData[idx];
        const eventConditionId = this.selectedNode.data.original.eventConditionId;
        const eventId = this.selectedNode.parent.data.original.eventId;
        this.dialogService.deleteDialogModelTaskEventUtterData(
          this.workspace.id, this.modelId, this.selectedTask.id, eventId, eventConditionId, eventUtterData).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedNode.data.original.eventUtterData.splice(idx, 1);
            this.selectedNode.data.original.eventUtterData = this.selectedNode.data.original.eventUtterData.concat();
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  public utterChanged(idx, value, isStartEvent) {
    if (this.checkModelCreator()) {
      if (isStartEvent) {
        const startEventUtterData = this.selectedNode.data.original.startEventUtterData[idx];
        startEventUtterData.startEventUtterData = value;
        const startEventId = this.selectedNode.data.original.startEventId;
        this.dialogService.updateDialogModelTaskStartEventUtterData(
          this.workspace.id, this.modelId, this.selectedTask.id, startEventId, startEventUtterData).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedNode.data.original.startEventUtterData[idx].startEventUtterData = value;
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        const eventUtterData = this.selectedNode.data.original.eventUtterData[idx];
        eventUtterData.eventUtterData = value;
        const eventId = this.selectedNode.parent.data.original.eventId;
        const eventConditionId = this.selectedNode.data.original.eventConditionId;
        if (!eventConditionId) {
          console.log(this.selectedNode);
        }
        this.dialogService.updateDialogModelTaskEventUtterData(
          this.workspace.id, this.modelId, this.selectedTask.id, eventId, eventConditionId, eventUtterData).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedNode.data.original.eventUtterData[idx].eventUtterData = value;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  public intentChanged(isStartEvent) {
    this.extraDataChanged(isStartEvent);
  }

  public extraDataChanged(isStartEvent) {
    if (this.checkModelCreator()) {
      if (isStartEvent) {
        const startEventUtter = this.selectedNode.data.original;
        this.dialogService.updateDialogModelTaskStartEventUtter(
          this.workspace.id, this.modelId, this.selectedTask.id, startEventUtter).subscribe(res => {
          if (res && res.statusCode === 200) {

          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        if (this.selectedNode.data.type === 'CONDITION') {
          const eventCondition = this.selectedNode.data.original;
          this.dialogService.updateDialogModelTaskEventCondition(
            this.workspace.id, this.modelId, this.selectedTask.id, eventCondition).subscribe(res => {
            if (res && res.statusCode === 200) {

            }
          }, error => {
            this.errorMessage(error);
          });
        } else if (this.selectedNode.data.type === 'UTTERANCE') {
          const eventUtter = this.selectedNode.data.original;
          const eventId = this.selectedNode.parent.data.original.eventId;
          this.dialogService.updateDialogModelTaskEventUtter(
            this.workspace.id, this.modelId, this.selectedTask.id, eventId, eventUtter).subscribe(res => {
            if (res && res.statusCode === 200) {

            }
          }, error => {
            this.errorMessage(error);
          });
        } else if (this.selectedNode.data.type === 'INTENTION') {
          const event = this.selectedNode.data.original;
          this.dialogService.updateDialogModelTaskEvent(this.workspace.id, this.modelId, event).subscribe(res => {
            if (res && res.statusCode === 200) {

            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  public toggleGlobalTask() {
    if (this.selectedTask !== null) {
      if (this.selectedTask.taskName === this.globalTask.taskName) {
        this.selectedTask = null;
        this.selectedNode = undefined;
        this.selectedLink = null;
        this.closeEditSection();
        this.diagram.requestUpdate();
        return;
      } else {
        this.diagram.selectCollection([]);
      }
    }
    this.selectedTask = this.globalTask;
    this.selectedNode = undefined;
    this.selectedLink = null;
    this.closeEditSection();
    this.diagram.requestUpdate();
  }

  public clickTreeWrapperBackground(event) {
    if (event.target.classList.value === 'dialog-tree') {
      this.selectedNode = undefined;
      this.selectedLink = null;
      this.closeEditSection();
    }
  }

  public openIntentionEditSection() {
    $('.response-intention').css('display', 'block');
    $('.response-condition, .response-utterance').css('display', 'none');
  }

  public openConditionEditSection() {
    $('.response-condition').css('display', 'block');
    $('.response-utterance, .response-intention').css('display', 'none');
  }

  public openUtteranceEditSection() {
    $('.response-utterance').css('display', 'block');
    $('.response-condition, .response-intention').css('display', 'none');
  }

  public closeEditSection() {
    $('.response-utterance, .response-condition, .response-intention').css('display', 'none');
  }

  public onNodeSelected(event) {
    if (event.node.children !== null && event.node.children !== undefined &&
      event.node.children.length > 0) {
      event.node.expanded = true;
    }
    setTimeout(() => {
      if (event.node.data.type === NODE_TYPE_EVENT.INTENTION) {
        this.openIntentionEditSection();
      } else if (event.node.data.type === NODE_TYPE_EVENT.CONDITION) {
        this.openConditionEditSection();
      } else if (event.node.data.type === NODE_TYPE_EVENT.UTTERANCE) {
        this.openUtteranceEditSection();
      } else {
        this.closeEditSection();
      }
    }, 0);
  }

  public eventClick(event, type) {
    $(event.currentTarget).addClass('event_on').siblings().removeClass('event_on');
    $('.response-intention, .response-condition, .response-utterance').css('display', 'none');
    if (type === this.RESPONSE_EVENT.FIRST) {
      $('.event-first').css('display', 'block');
      $('.event-second').css('display', ' none');
    } else if (type === this.RESPONSE_EVENT.SECOND) {
      $('.event-second').css('display', 'block');
      $('.event-first').css('display', 'none');
    }
    this.selectedNode = undefined;
    this.selectedLink = null;
    this.closeEditSection();
  }

  public openScriptEditModal(key: string, script: string, isStartEvent: boolean) {
    $('.task-popup-default-wrap').css('display', 'table');
    this.scriptInput = {
      key: key,
      script: script,
      isStartEvent: isStartEvent
    };

    this.highlightDiv.nativeElement.innerHTML = this.applyHighlights(this.scriptInput.script);
  }

  public popupClose() {
    $('.task-popup-default-wrap').css('display', 'none');
  }

  public popupSave(script: string, key: string, isStartEvent: boolean) {
    $('.task-popup-default-wrap').css('display', 'none');
    if (key === 'Task Goal') {
      this.selectedTask.taskGoal = script;
      this.saveTask();
    } else if (key === 'Condition') {
      if (isStartEvent) {
        this.selectedNode.data.original.startEventCondition = script;
        this.dialogService.updateDialogModelTaskStartEvent(
          this.workspace.id, this.modelId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.startEvent[this.selectedNode.data.index].startEventCondition = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        this.selectedNode.data.original.eventCondition = script;
        const taskId = this.selectedNode.parent.data.original.taskId;
        this.dialogService.updateDialogModelTaskEventCondition(
          this.workspace.id, this.modelId, taskId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.event[this.selectedNode.parent.data.index].eventCondition[this.selectedNode.data.index].eventCondition
              = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    } else if (key === 'Action') {
      if (isStartEvent) {
        this.selectedNode.data.original.startEventAction = script;
        this.dialogService.updateDialogModelTaskStartEvent(
          this.workspace.id, this.modelId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.startEvent[this.selectedNode.data.index].startEventAction = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        this.selectedNode.data.original.eventAction = script;
        const taskId = this.selectedNode.parent.data.original.taskId;
        this.dialogService.updateDialogModelTaskEventCondition(
          this.workspace.id, this.modelId, taskId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.event[this.selectedNode.parent.data.index].eventCondition[this.selectedNode.data.index].eventAction
              = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    } else if (key === 'Transition Condition') {
      this.syncTaskDiagram();
      this.selectedLink.condition = script;
      const task = this.tasks.find(x => x.id === this.selectedLink.fromId);
      const nextTask = task.nextTask.find(x => x.id && x.taskId === this.selectedLink.fromId && x.nextTaskId === this.selectedLink.toId);
      nextTask.nextTaskCondition = script;
      this.dialogService.updateDialogNextTask(this.workspace.id, this.modelId, task.id, nextTask).subscribe(res => {
        if (res && res.statusCode === 200) {
          this.syncTaskDiagram();
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  public onMentionSelect(text: any) {
    return text.function;
  }

  public handleInput(text) {
    const highlightedText = this.applyHighlights(text);
    this.highlightDiv.nativeElement.innerHTML = highlightedText;
  }

  public handleScroll(event) {
    this.highlightDiv.nativeElement.scrollTop = event.currentTarget.scrollTop;
  }

  public applyHighlights(text) {
    if (!text || text === null || text === '') {
      return '';
    } else {
      return text
      .replace(/\n$/g, '\n\n')
      .replace(/Count\b/g, '<mark>$&</mark>')
      .replace(/ExistValueFromDialog\b/g, '<mark>$&</mark>')
      .replace(/ExistValueAtHistory\b/g, '<mark>$&</mark>')
      .replace(/ExistValue\b/g, '<mark>$&</mark>')
      .replace(/ExistSlotAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/ExistSlotAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/ExistSlotAtUtterHistory\b/g, '<mark>$&</mark>')
      .replace(/ExistClassAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/ExistClassAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/HasValueHistory\b/g, '<mark>$&</mark>')
      .replace(/HasValueAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/HasValueAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/HasValue\b/g, '<mark>$&</mark>')
      .replace(/IsDAType\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtUtterHistory\b/g, '<mark>$&</mark>')
      .replace(/isDATypeAtCurrentTask\b/g, '<mark>$&</mark>')
      .replace(/IsDA\b/g, '<mark>$&</mark>')
      .replace(/IsConfirmTrue\b/g, '<mark>$&</mark>')
      .replace(/Value\b/g, '<mark>$&</mark>');
    }
  }

  public trainOpen() {
    if (this.checkModelCreator()) {
      this.dialog.open(PopupTrainComponent, {
        maxHeight: '50%',
        width: '40%',
        disableClose: true,
      });
      this.emitterService.get('training').subscribe(result => {
        if (result === 'ok') {
          const trainId = localStorage.getItem('trainId');
          this.checkTraining(trainId);
          this.dialog.closeAll();
        }
      });
    }
  }

  public scriptToggle() {
    this.isScriptGuideOpen = !this.isScriptGuideOpen;
    if (this.isScriptGuideOpen) {
      $('.popup-container').css('width', '1010px');
      this.btnScript = 'primary';
    } else {
      $('.popup-container').css('width', '600px');
      this.btnScript = 'default';
    }
  }

  public createLinkInfoFromDialogTasks() {
    this.dialogTasksLinkInfo = [];
    if (this.tasks.length > 0) {
      for (const task of this.tasks) {
        if (task.nextTask && task.nextTask.length > 0) {
          task.nextTask.forEach(next => {
            if (next.id) {
              const link = {
                id: '',
                from: '',
                to: '',
                condition: '',
                controller: '',
                fromId: '',
                toId: ''
              };
              link.id = next.id;
              link.condition = next.nextTaskCondition;
              link.controller = next.controller;
              link.from = task.taskName;
              link.to = this.tasks.find(x => x.id === next.nextTaskId).taskName;
              link.fromId = next.taskId;
              link.toId = next.nextTaskId;
              this.dialogTasksLinkInfo.push(link);
            }
          });
        }
      }
    }
  }

  public syncTaskDiagram() {
    for (const task of this.diagram.model.nodeDataArray as DialogTask[]) {
      task.nextTask = [];
      const filtered = this.dialogTasksLinkInfo.filter((val) => (val.from === task.taskName));
      for (const next of filtered) {
        if (next.id) {
          task.nextTask.push({
            id: next.id,
            controller: next.controller,
            nextTaskCondition: next.condition,
            nextTaskId: next.toId,
            creatorId: task.creatorId,
            createdAt: '',
            taskId: next.fromId
          });
        }
      }
    }
  }

  createTreeNode(dataList: any[], data: any, type: string, name: string, isGlobalTask: boolean) {
    let idx = 0;
    if (dataList && dataList.length > 0) {
      idx = dataList.length - 1;
    }
    const slot = {
      data: {
        name: name + idx,
        original: data,
        isGlobal: isGlobalTask,
        type: type,
        index: idx
      }
    };
    return slot;
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }

  checkModelCreator() {
    console.log(this.dialogModel.creatorId, '::', this.user.id);
    if (this.dialogModel.creatorId + '' !== this.user.id + '') {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '경고';
      ref.componentInstance.message = '다른 사용자가 작성한 모델은 수정할 수 없습니다.';
      return false;
    } else {
      return true;
    }
  }
}
