import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {PopupTrainComponent, PopupErrorComponent} from '../../shared/components/popup';
import {Message, TreeNode, TreeTable} from 'primeng/primeng';
import {
  DialogEntity,
  DialogEntityData,
  DialogEntityDataSynonym,
  DialogEntitySlotType,
  DialogModel,
  DialogService, EmitterService
} from '../../service';
import * as _ from 'lodash';
import {PopupDeleteComponent} from '../../shared/components/popup/popup.delete.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';

@Component({
  selector: 'app-entities',
  templateUrl: './entities.component.html',
  styleUrls: ['./entities.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EntitiesComponent implements OnInit, OnDestroy {
  dialogModel: DialogModel;
  modelId: string;
  modelName: string;
  workspaceId: string;
  value: number;
  isTraining: boolean;
  visibleTraining: boolean;
  slotTypeOptions: {
    label: string,
    value: string
  }[];

  entityList: DialogEntity[] = [];
  sharedEntityList: DialogEntity[] = [];
  _storedEntityList: DialogEntity[] = [];
  _storedSharedEntityList: DialogEntity[] = [];
  selectedNode: TreeNode;
  selectedSharedNode: TreeNode;

  @ViewChild('entityTreeTable')
  entityTreeTable: TreeTable;

  @ViewChild('sharedEntityTreeTable')
  sharedEntityTreeTable: TreeTable;

  @ViewChild('upload')
  upload: any;

  msgs: Message[] = [];
  trainingCheck: any;

  isLoaded: boolean;
  onShared: boolean;

  constructor(public dialog: MatDialog, private store: Store<any>,
              private emitterService: EmitterService,
              private dialogService: DialogService) {
    this.value = 0;
    this.isTraining = false;
    this.visibleTraining = false;
    this.dialogModel = JSON.parse(localStorage.getItem('currentDialogModel')) as DialogModel;
    this.modelId = this.dialogModel.id;
    this.modelName = this.dialogModel.modelName;
    this.workspaceId = localStorage.getItem('currentWorkspaceId');
    this.onShared = false;
    this.slotTypeOptions = [];
    for (const key of Object.keys(DialogEntitySlotType)) {
      this.slotTypeOptions.push({
        label: key,
        value: key
      });
    }

    this.isLoaded = false;

    this.trainingCheck = setInterval(() => {
      const trainId = localStorage.getItem('trainId');
      this.checkTraining(trainId);
    }, 10000);
  }

  ngOnInit() {
    this.selectEntityList();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
    localStorage.removeItem('init_entities');
  }

  checkTraining(trainId) {
    if (trainId !== null && trainId !== undefined) {
      this.visibleTraining = true;
      this.dialogService.getTrainingProgress(trainId).subscribe(result => {
        if (result && result.statusCode === 200) {
          if (result.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = result.data.error;
          } else {
            if (result.data.resultInfo.step === 'SDS_TRAIN_DONE') {
              this.value = 100;
              localStorage.removeItem('trainId');
              this.isTraining = false;
              this.msgs = [];
              this.msgs.push({
                severity: 'info',
                summary: 'Train Status',
                detail: result.data.resultInfo.step
              });
            } else {
              this.isTraining = true;
              if (result.data.resultInfo.value && result.data.resultInfo.maximum) {
                this.value = Math.round(Number(result.data.resultInfo.value) / Number(result.data.resultInfo.maximum) * 100);
              }
            }
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  stopTraining() {
    const trainId = localStorage.getItem('trainId');
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          if (response.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = response.data.error;
          } else {
            this.isTraining = true;
            this.value = 0;
            localStorage.removeItem('trainId');
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Train Status', detail: 'CANCELED'});
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  selectEntityList() {
    this.selectEntityListAll();
    this.selectSharedEntityListAll();
  }

  selectSharedEntityListAll() {
    this.dialogService.getDialogModelSharedEntityAll(this.workspaceId, this.modelId).subscribe(response => {
      if (response && response.statusCode) {
        this.sharedEntityList = response.data.entity;
        this._storedSharedEntityList = response.data.entity;
        setTimeout(() => {
          if (this.selectedSharedNode) {
            const classNode = this.entityTreeTable.value.filter(
              x => x.data.original.className === this.selectedSharedNode.data.original.className)[0];
            if (classNode) {
              classNode.expanded = true;
              this.selectedSharedNode = classNode.children.filter(
                x => (x.data.original.id === this.selectedSharedNode.data.original.id))[0];
            } else {
              this.selectedSharedNode = undefined;
            }
          }
        }, 0);

        localStorage.setItem('init_entities', JSON.stringify(this.sharedEntityList));
        this.isLoaded = true;
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  selectEntityListAll() {
    this.dialogService.getDialogModelEntityAll(this.workspaceId, this.modelId).subscribe(response => {
      if (response && response.statusCode) {
        this.entityList = response.data.entity;
        this._storedEntityList = response.data.entity;
        setTimeout(() => {
          if (this.selectedNode) {
            const classNode = this.entityTreeTable.value.filter(
              x => x.data.original.className === this.selectedNode.data.original.className)[0];
            if (classNode) {
              classNode.expanded = true;
              this.selectedNode = classNode.children.filter(
                x => (x.data.original.id === this.selectedNode.data.original.id))[0];
            } else {
              this.selectedNode = undefined;
            }
          }
        }, 0);

        localStorage.setItem('init_entities', JSON.stringify(this.entityList));
        this.isLoaded = true;
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  onNodeSelect(event) {
    if (event.node.children !== null && event.node.children !== undefined && event.node.children.length > 0) {
      event.node.expanded = true;
    }
  }

  addEntity() {
    if (this.checkModelCreator()) {
      if (this.selectedNode) {
        if (this.selectedNode.data.original.sharedYn === 'Y' && !this.onShared) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '30%',
            disableClose: true,
          });
          ref.componentInstance.title = '안내';
          ref.componentInstance.message = '공유된 엔티티는 수정할 수 없습니다.';
        } else {
          const newEntity = new DialogEntity();
          let classNameFounded = false;
          let entityNameFounded = false;
          let retryCnt = 1;
          if (this.selectedNode.data.type === 'ENTITY_NODE') {
            newEntity.className = this.selectedNode.data.original.className;
            newEntity.classSource = this.selectedNode.data.original.classSource;
          } else {
            while (!classNameFounded) {
              const node = this.entityTreeTable.value.filter(x => x.data.original.className === newEntity.className);
              if (node.length > 0) {
                newEntity.className = 'new_domain_' + retryCnt;
                retryCnt++;
              } else {
                classNameFounded = true;
              }
            }
          }

          retryCnt = 1;
          while (!entityNameFounded) {
            let isExist = false;
            for (const classNode of this.entityTreeTable.value) {
              const filtered = classNode.children.filter(x => x.data.original.entityName === newEntity.entityName);
              if (filtered.length > 0) {
                isExist = true;
              }
            }
            if (isExist) {
              newEntity.entityName = 'new_entity_' + retryCnt;
              retryCnt++;
            } else {
              entityNameFounded = true;
            }
          }
          newEntity.id = this.dialogService.getUUID();
          newEntity.creatorId = localStorage.getItem('currentUserId');
          newEntity.workspaceId = this.workspaceId;
          newEntity.sharedYn = 'N';
          newEntity.entityType = DialogEntitySlotType.STRING;
          this.dialogService.insertDialogModelEntity(this.modelId, newEntity).subscribe(response => {
            if (response && response.statusCode === 200) {
              const newNode = this.createTreeNode(newEntity, this.selectedNode.data.type, true);
              if (this.selectedNode.data.type === 'CLASS_NODE') {
                this.entityTreeTable.value.push(newNode);
              } else {
                this.entityTreeTable.value.find(x => x.data.original.id === this.selectedNode.parent.data.original.id)
                .children.push(newNode);
              }
              this.selectedNode = newNode;
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      } else {
        if (this.onShared) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '30%',
            disableClose: true,
          });
          ref.componentInstance.title = '안내';
          ref.componentInstance.message = '공유된 엔티티는 수정할 수 없습니다.';
        } else {
          const newEntity = new DialogEntity();
          let founded = false;
          let retryCnt = 1;
          while (founded === false) {
            const f = this.entityList.filter((val) => (val.className === newEntity.className));
            if (f.length <= 0) {
              founded = true;
            } else {
              newEntity.className = 'new_domain_' + retryCnt;
              retryCnt++;
            }
          }

          founded = false;
          retryCnt = 1;
          while (founded === false) {
            const f = this.entityList.filter((val) => (val.entityName === newEntity.entityName));
            if (f.length <= 0) {
              founded = true;
            } else {
              newEntity.entityName = 'new_entity_' + retryCnt;
              retryCnt++;
            }
          }
          newEntity.id = this.dialogService.getUUID();
          newEntity.creatorId = localStorage.getItem('currentUserId');
          newEntity.workspaceId = this.workspaceId;
          newEntity.sharedYn = 'N';
          newEntity.entityType = DialogEntitySlotType.STRING;
          this.dialogService.insertDialogModelEntity(this.modelId, newEntity).subscribe(response => {
            if (response && response.statusCode === 200) {
              this.entityList.push(newEntity);
              this._storedEntityList = this.entityList;
              const newNode = this.createTreeNode(newEntity, 'CLASS_NODE', true);
              this.entityTreeTable.value.push(newNode);
              this.selectedNode = newNode;
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  removeEntity() {
    if (this.checkModelCreator()) {
      if (this.selectedNode) {
        if (!this.onShared) {
          if (this.selectedNode.data.original.sharedYn === 'Y') {
            const ref = this.dialog.open(PopupDeleteComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.domain = 'Entity';
            ref.componentInstance.message = '이 엔티티는 공유된 엔티티 입니다.<br/>삭제시 참조하는 모델에 영향을 끼칠수 있습니다.';
            this.emitterService.get('Entity').subscribe(result => {
              if (result === 'ok') {
                this.removeEntityConfirmed();
                this.dialog.closeAll();
              }
            });
          } else {
            this.removeEntityConfirmed();
          }
        } else {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '30%',
            disableClose: true,
          });
          ref.componentInstance.title = '안내';
          ref.componentInstance.message = '공유된 엔티티는 삭제할 수 없습니다.';
        }
      }
    }
  }

  removeEntityConfirmed() {
    let removeList = [];
    if (this.selectedNode.data.type === 'CLASS_NODE') {
      removeList = this.selectedNode.children;
      if (removeList.length > 0) {
        const isLinkDelete = [];
        const entityIdList = [];
        for (let i = 0; i < removeList.length; i++) {
          entityIdList.push(removeList[i].data.original.id);
          if (removeList[i].data.original.sharedYn === 'Y') {
            isLinkDelete.push('true');
          } else {
            isLinkDelete.push('false');
          }
        }
        if (isLinkDelete.length === entityIdList.length) {
          this.dialogService.deleteDialogModelEntityClass(
            this.workspaceId, this.modelId, isLinkDelete, entityIdList).subscribe(response => {
            if (response && response.statusCode === 200) {
              const classIdx = this.entityTreeTable.value.findIndex(x => x.data.original.id === this.selectedNode.data.original.id);
              if (classIdx >= 0) {
                this.entityTreeTable.value.splice(classIdx, 1);
                this.selectedNode = undefined;
              }
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    } else {
      this.dialogService.deleteDialogModelEntity(this.modelId, this.selectedNode.data.original).subscribe(response => {
        if (response && response.statusCode === 200) {
          const classIdx = this.entityTreeTable.value
          .findIndex(x => x.data.original.id === this.selectedNode.parent.data.original.id);
          const entityIdx = this.entityTreeTable.value[classIdx].children
          .findIndex(x => x.data.original.id === this.selectedNode.data.original.id);
          if (entityIdx >= 0) {
            this.entityTreeTable.value[classIdx].children.splice(entityIdx, 1);
            this.selectedNode = this.entityTreeTable.value[classIdx];
          }
          if (this.entityTreeTable.value[classIdx].children.length === 0) {
            this.entityTreeTable.value.splice(classIdx, 1);
            this.selectedNode = undefined;
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  addEntityData() {
    if (this.checkModelCreator()) {
      if (this.selectedNode && this.selectedNode.data.original.sharedYn === 'Y' && !this.onShared) {
        const ref = this.dialog.open(PopupErrorComponent, {
          minWidth: '30%',
          disableClose: true,
        });
        ref.componentInstance.title = '안내';
        ref.componentInstance.message = '공유된 엔티티는 수정할 수 없습니다.';
      } else {
        if (!this.onShared) {
          if (!this.selectedNode.data.original.entityData) {
            this.selectedNode.data.original.entityData = [];
          }
          const entityData = new DialogEntityData();
          entityData.id = this.dialogService.getUUID();
          entityData.creatorId = localStorage.getItem('currentUserId');
          entityData.entityId = this.selectedNode.data.original.id;
          entityData.entityDataSynonym = [];
          this.selectedNode.data.original.entityData.push(entityData);
        } else {
          if (!this.selectedSharedNode.data.original.entityData) {
            this.selectedSharedNode.data.original.entityData = [];
          }
          const entityData = new DialogEntityData();
          entityData.id = this.dialogService.getUUID();
          entityData.creatorId = localStorage.getItem('currentUserId');
          entityData.entityId = this.selectedSharedNode.data.original.id;
          entityData.entityDataSynonym = [];
          this.selectedSharedNode.data.original.entityData.push(entityData);
        }
      }
    }
  }

  removeEntityData(idx) {
    if (this.checkModelCreator()) {
      if (this.selectedNode && this.selectedNode.data.original.sharedYn === 'Y' && !this.onShared) {
        const ref = this.dialog.open(PopupErrorComponent, {
          minWidth: '30%',
          disableClose: true,
        });
        ref.componentInstance.title = '안내';
        ref.componentInstance.message = '공유된 엔티티는 수정할 수 없습니다.';
      } else {
        if (!this.onShared) {
          this.dialogService.deleteDialogModelEntityData(
            this.workspaceId, this.modelId, this.selectedNode.data.original.entityData[idx]).subscribe(response => {
            if (response && (response.statusCode === 200 || response.statusCode === 400)) {
              this.selectedNode.data.original.entityData.splice(idx, 1);
            }
          }, error => {
            this.errorMessage(error);
          });
        } else {
          this.dialogService.deleteDialogModelEntityData(
            this.workspaceId, this.modelId, this.selectedSharedNode.data.original.entityData[idx]).subscribe(response => {
            if (response && (response.statusCode === 200 || response.statusCode === 400)) {
              this.selectedSharedNode.data.original.entityData.splice(idx, 1);
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  saveEntity() {
    if (this.isLoaded) {
      if (this.checkModelCreator()) {
        if (!this.onShared) {
          const saveEntity = {
            id: this.selectedNode.data.original.id,
            entityType: this.selectedNode.data.original.entityType,
            creatorId: this.selectedNode.data.original.creatorId,
            sharedYn: this.selectedNode.data.sharedYn,
            workspaceId: this.workspaceId
          };
          if (this.selectedNode.data.entityType && this.selectedNode.data.entityType !== this.selectedNode.data.original.entityType) {
            saveEntity['entityType'] = this.selectedNode.data.original.entityType;
          } else {
            if (this.selectedNode.data.className) {
              saveEntity['className'] = this.selectedNode.data.className;
            } else {
              saveEntity['entityName'] = this.selectedNode.data.name;
            }
          }
          this.dialogService.updateDialogModelEntity(this.modelId, saveEntity).subscribe(response => {
            if (response && response.statusCode === 200) {
              this.selectEntityList();
            }
          }, error => {
            this.errorMessage(error);
          });
        } else {
          const saveEntity = {
            id: this.selectedSharedNode.data.original.id,
            entityType: this.selectedSharedNode.data.original.entityType,
            creatorId: this.selectedSharedNode.data.original.creatorId,
            sharedYn: this.selectedSharedNode.data.sharedYn,
            workspaceId: this.workspaceId
          };
          if (this.selectedSharedNode.data.entityType && this.selectedSharedNode.data.entityType
            !== this.selectedSharedNode.data.original.entityType) {
            saveEntity['entityType'] = this.selectedSharedNode.data.original.entityType;
          } else {
            if (this.selectedSharedNode.data.className) {
              saveEntity['className'] = this.selectedSharedNode.data.className;
            } else {
              saveEntity['entityName'] = this.selectedSharedNode.data.name;
            }
          }
          this.dialogService.updateDialogModelEntity(this.modelId, saveEntity).subscribe(response => {
            if (response && response.statusCode === 200) {
              this.selectEntityList();
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  saveEntityData(idx: number) {
    if (this.isLoaded) {
      if (this.checkModelCreator()) {
        if (!this.onShared) {
          const data = this.selectedNode.data.original.entityData[idx];
          if (data.createdAt) {
            this.dialogService.updateDialogModelEntityData(this.workspaceId, this.modelId, data).subscribe(response => {
              if (response && response.statusCode === 200) {

              }
            }, error => {
              this.errorMessage(error);
            });
          } else {
            this.dialogService.insertDialogModelEntityData(this.workspaceId, this.modelId, data).subscribe(response => {
              if (response && response.statusCode === 200) {
                this.selectedNode.data.original.entityData[idx].createdAt = new Date().toLocaleString();
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        } else {
          const data = this.selectedSharedNode.data.original.entityData[idx];
          if (data.createdAt) {
            this.dialogService.updateDialogModelEntityData(this.workspaceId, this.modelId, data).subscribe(response => {
              if (response && response.statusCode === 200) {

              }
            }, error => {
              this.errorMessage(error);
            });
          } else {
            this.dialogService.insertDialogModelEntityData(this.workspaceId, this.modelId, data).subscribe(response => {
              if (response && response.statusCode === 200) {
                this.selectedSharedNode.data.original.entityData[idx].createdAt = new Date().toLocaleString();
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        }
      }
    }
  }

  removeSynonym(entityData, idx) {
    if (this.checkModelCreator()) {
      this.dialogService.deleteDialogModelEntityDataSynonym(
        this.workspaceId, this.modelId, entityData.entityId, entityData.entityDataSynonym[idx]).subscribe(response => {
        if (response && response.statusCode === 200) {
          entityData.entityDataSynonym.splice(idx, 1);
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  synonymEnterEvent(event, idx) {
    if (this.checkModelCreator()) {
      if (event.keyCode === 13) {
        if (!this.onShared) {
          setTimeout(() => {
            const entityDataSynonym = new DialogEntityDataSynonym();
            entityDataSynonym.id = this.dialogService.getUUID();
            entityDataSynonym.entityDataSynonym = event.target.value;
            entityDataSynonym.entityDataId = this.selectedNode.data.original.entityData[idx].id;
            entityDataSynonym.creatorId = localStorage.getItem('currentUserId');
            this.dialogService.insertDialogModelEntityDataSynonym(
              this.workspaceId, this.modelId, this.selectedNode.data.original.entityData[idx].entityId, entityDataSynonym)
            .subscribe(response => {
              if (response && response.statusCode === 200) {
                if (!this.selectedNode.data.original.entityData[idx].entityDataSynonym) {
                  this.selectedNode.data.original.entityData[idx].entityDataSynonym = [];
                }
                this.selectedNode.data.original.entityData[idx].entityDataSynonym.push(entityDataSynonym);
                event.target.value = '';
              }
            }, error => {
              this.errorMessage(error);
            });
          }, 0);
        } else {
          setTimeout(() => {
            const entityDataSynonym = new DialogEntityDataSynonym();
            entityDataSynonym.id = this.dialogService.getUUID();
            entityDataSynonym.entityDataSynonym = event.target.value;
            entityDataSynonym.entityDataId = this.selectedSharedNode.data.original.entityData[idx].id;
            entityDataSynonym.creatorId = localStorage.getItem('currentUserId');
            this.dialogService.insertDialogModelEntityDataSynonym(
              this.workspaceId, this.modelId, this.selectedSharedNode.data.original.entityData[idx].entityId, entityDataSynonym)
            .subscribe(response => {
              if (response && response.statusCode === 200) {
                if (!this.selectedSharedNode.data.original.entityData[idx].entityDataSynonym) {
                  this.selectedSharedNode.data.original.entityData[idx].entityDataSynonym = [];
                }
                this.selectedSharedNode.data.original.entityData[idx].entityDataSynonym.push(entityDataSynonym);
                event.target.value = '';
              }
            }, error => {
              this.errorMessage(error);
            });
          }, 0);
        }
      }
    }
  }

  onEntityChange(target?: string) {
    if (!this.onShared) {
      if (this.selectedNode !== null && this.selectedNode !== undefined) {
        let filtered = [];
        if (target) {
          if (target === 'entityName') {
            filtered = this.entityList.filter(x => x.id !== this.selectedNode.data.original.id
              && x.entityName === this.selectedNode.data.name);
          }
        }
        if (filtered.length > 0) {
          if (target === 'entityName') {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '30%',
              disableClose: true,
            });
            ref.componentInstance.title = '경고';
            ref.componentInstance.message = 'entityName duplicated.';
            this.selectedNode.data.name = this.selectedNode.data.original.entityName;
          }
        } else {
          this.saveEntity();
        }
      }
    } else {
      if (this.selectedSharedNode !== null && this.selectedSharedNode !== undefined) {
        let filtered = [];
        if (target) {
          if (target === 'entityName') {
            filtered = this.entityList.filter(x => x.id !== this.selectedSharedNode.data.original.id
              && x.entityName === this.selectedSharedNode.data.name);
          }
        }
        if (filtered.length > 0) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '30%',
            disableClose: true,
          });
          ref.componentInstance.title = '경고';
          ref.componentInstance.message = 'entityName duplicated.';
          this.selectedSharedNode.data.name = this.selectedSharedNode.data.original.entityName;
        } else {
          this.saveEntity();
        }

      }
    }
  }

  entityDataChanged(idx: number) {
    if (!this.onShared) {
      if (this.selectedNode !== null && this.selectedNode !== undefined) {
        this.saveEntityData(idx);
      }
    } else {
      if (this.selectedSharedNode !== null && this.selectedSharedNode !== undefined) {
        this.saveEntityData(idx);
      }
    }
  }

  trainOpen() {
    if (this.checkModelCreator()) {
      this.dialog.open(PopupTrainComponent, {
        maxHeight: '50%',
        width: '40%',
        disableClose: true,
      });
      this.emitterService.get('training').subscribe(result => {
        if (result === 'ok') {
          const trainId = localStorage.getItem('trainId');
          this.checkTraining(trainId);
          this.dialog.closeAll();
        }
      });
    }
  }

  downloadEntities() {
    this.dialogService.downloadDialogEntity(this.workspaceId, this.modelId).subscribe(res => {
      if (res && res.status === 200) {
        if (res.headers.get('content-type') === 'text/tsv;charset=UTF-8') {
          const BOM = '%EF%BB%BF';
          const dataUri = 'data:text/tsv;charset=UTF-8,' + BOM + encodeURIComponent(res.text());

          const date = new Date();
          const year = date.getFullYear();
          let month = (date.getMonth() + 1).toString();
          let day = date.getDate().toString();

          if (month.length === 1) {
            month = '0' + month;
          }
          if (day.length === 1) {
            day = '0' + day;
          }

          const exportFileDefaultName = this.modelName + '_' + 'entities_' + year + month + day + '.tsv';

          const linkElement = document.createElement('a');
          linkElement.setAttribute('href', dataUri);
          linkElement.setAttribute('download', exportFileDefaultName);
          linkElement.click();
        }
      } else {
        console.log(res);
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  uploadEntities(event: any) {
    if (this.checkModelCreator()) {
      if (event.target.files.length > 0) {
        const file: File = event.target.files[0];
        const formData: FormData = new FormData();
        formData.append('sourceFile ', file, file.name);
        this.dialogService.uploadDialogEntity(this.workspaceId, this.modelId, formData).subscribe(res => {
          console.log(res);
          if (res && res.statusCode === 200) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '30%',
              disableClose: true,
            });
            ref.componentInstance.title = '업로드 완료';
            let message = 'Total Count : ' + res.data.totalRowCount;
            message += '  Success : ' + res.data.successCount;
            message += '  Fail : ' + res.data.failCount;
            if (Number(res.data.failCount) > 0) {
              message += '<p>Failed Data' + '</p>';
              if (res.data.failDataList.length > 0) {
                for (const failData of res.data.failDataList) {
                  message += '<p> - ' + failData + '</p>';
                }
              }
            }
            ref.componentInstance.message = message;
          } else {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '30%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = res.statusMessage;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  onToggleEntityTabs(event) {
    if (event.index === 0) {
      this.onShared = false;
      this.selectEntityListAll();
    } else {
      this.onShared = true;
      this.selectSharedEntityListAll();
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '30%',
        disableClose: true,
      });
      ref.componentInstance.title = '안내';
      ref.componentInstance.message = '이 엔티티는 공유된 인텐트의 엔티티입니다.<br/>수정시 참조하는 모델에 영향을 끼칠수 있습니다.';
    }
  }

  isSharedEntity() {
    if (this.selectedNode.data.sharedYn === 'Y') {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '30%',
        disableClose: true,
      });
      ref.componentInstance.title = '안내';
      ref.componentInstance.message = '공유된 엔티티는 수정할 수 없습니다.';
      return true;
    } else {
      return false;
    }
  }

  createTreeNode(data: any, node: string, expanded: boolean) {
    let classNode = {};
    const dataNode = {
      data: {
        name: data.entityName,
        entityData: data.entityData,
        entityType: data.entityType,
        sharedYn: data.sharedYn,
        original: data,
        type: 'ENTITY_NODE'
      }
    };
    if (node === 'CLASS_NODE') {
      classNode = {
        parent: undefined,
        data: {
          name: data.className,
          className: data.className,
          classSource: data.classSource,
          sharedYn: data.sharedYn,
          original: data,
          type: 'CLASS_NODE'
        },
        expanded: expanded,
        children: []
      };
      if (this.selectedNode) {
        dataNode['parent'] = this.selectedNode.parent;
      }
      classNode['children'].push(dataNode);
      return classNode;
    } else {
      return dataNode;
    }
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      this.dialog.closeAll();
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.closeAll();
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }

  checkModelCreator() {
    if (this.dialogModel.creatorId !== localStorage.getItem('currentUserId')) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '30%',
        disableClose: true,
      });
      ref.componentInstance.title = '경고';
      ref.componentInstance.message = '다른 사용자가 작성한 모델은 수정할 수 없습니다.';
      return false;
    } else {
      return true;
    }
  }
}
