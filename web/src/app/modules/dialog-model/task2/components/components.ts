import {Dialogs, Node} from 'jsplumbtoolkit';
import {BaseNodeComponent} from 'jsplumbtoolkit-angular';
import {Component} from '@angular/core';

function isNode(obj: any): obj is Node {
  return obj.objectType === 'Node';
}

// /**
//  * This is the base class for editable nodes in this demo. It extends `BaseNodeComponent`
//  */
export class BaseEditableNodeComponent extends BaseNodeComponent {

  /*  removeNode() {
      console.log('removeNode...');
      const obj = this.getNode();
      if (obj != null) {
        if (isNode(obj)) {
          Dialogs.show({
            id: 'dlgConfirm',
            title: '타스크 삭제',
            data: {
              msg: '[' + obj.data.text + ']를 삭제 하시겠습니까?'
            },
            onOK: () => {
              this.toolkit.removeNode(<Node>obj);
            }
          });
        }
      }
    }

    editNode() {
      console.log('editNode...');
      const obj = this.getNode();
      Dialogs.show({
        id: 'dlgText',
        data: obj.data,
        title: '타스크 타이틀 수정',
        onOK: (data: any) => {
          if (data.text && data.text.length > 2) {
            // if name is at least 2 chars long, update the underlying data and
            // update the UI.
            this.toolkit.updateNode(obj, data);
          }
        }
      });
    }*/

}


// ----------------- start node -------------------------------
@Component({
  templateUrl: '../templates/start.html',
  styleUrls: ['./task2.component.scss']
})
export class StartNodeComponent2 extends BaseEditableNodeComponent {
}

// ----------------- end node -------------------------------
@Component({
  templateUrl: '../templates/end.html',
  styleUrls: ['./task2.component.scss']
})
export class EndNodeComponent2 extends BaseEditableNodeComponent {
}

// ----------------- task node -------------------------------
@Component({
  templateUrl: '../templates/task.html',
  styleUrls: ['./task2.component.scss']
})
export class TaskNodeComponent2 extends BaseEditableNodeComponent {
}

