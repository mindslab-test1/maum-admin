import {
  AfterViewInit, ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Message, TreeNode, TreeTable} from 'primeng/primeng';
import {SelectItem} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import * as go from 'gojs';
import {GraphLinksModel} from 'gojs';
import {
  DefaultDialogTask,
  DefaultGlobalDialogTask,
  DialogModel,
  DialogService,
  DialogTask,
  ScriptGuide,
  TaskStartEvent,
  TaskEvent,
  TaskStartEventUtter,
  TaskEventCondition,
  TaskEventUtter,
  TaskEventUtterData,
  UtilService,
  EmitterService,
  DialogNextTask,
  TaskStartEventUtterData,
  NodeData,
  EdgeData, EdgeDataData,
} from '../../service';
import * as _ from 'lodash';
import {
  MatDialog,
  MatTreeFlatDataSource,
  MatTreeFlattener,
  MatTreeNestedDataSource
} from '@angular/material';
import {PopupTrainComponent, PopupErrorComponent} from '../../shared/components/popup';

import {
  NODE_TYPE_EVENT,
  NODE_TYPE_START_EVENT
} from '../../_provide';
import * as $ from 'jquery';
import {NgBlockUI} from 'ng-block-ui';
import {MessageService} from 'primeng/components/common/messageservice';
import {CommonModalComponent} from '../../common/common-modal.component';
import {
  Dialogs, DragEventCallbackOptions, Edge, Group, jsPlumbToolkit, jsPlumbUtil, Node, ObjectId, Port,
  Surface
} from 'jsplumbtoolkit';
import {
  EndNodeComponent2, StartNodeComponent2, TaskNodeComponent2
} from './components';
import {jsPlumbService, jsPlumbSurfaceComponent} from 'jsplumbtoolkit-angular';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';
import {FlatTreeControl, NestedTreeControl} from '@angular/cdk/tree';
import {start} from 'repl';

declare const Mark: any;

export enum TapType {
  STARTEVENT = <any>'STARTEVENT',
  EVENT = <any>'EVENT'
}

export enum TreeType {
  CONDITION = <any>'CONDITION',
  UTTERANCE = <any>'UTTERANCE',
  INTENTION = <any>'INTENTION'
}

@Component({
  selector: 'app-task',
  templateUrl: 'task2.component.html',
  styleUrls: ['./task2.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Task2Component implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(jsPlumbSurfaceComponent)
  surfaceComponent: jsPlumbSurfaceComponent;

  @ViewChild('scriptInputModal')
  scriptInputModal: CommonModalComponent;

  @ViewChild('scriptGuidePopup')
  scriptGuidePopup: CommonModalComponent;

  @ViewChild('highlightDiv')
  highlightDiv: ElementRef;

  // enum variable
  tapType = TapType;
  treeType = TreeType;

  // user variable
  user: any = {};
  workspace: any = {};

  // model variable
  modelId: string = null;
  dialogModel: DialogModel = new DialogModel();
  intentListOption: any[] = [];
  selectedIntent = '';
  test = [{label: 1, value: 1}, {label: 2, value: 2}];

  // training variable
  visibleTraining = false;
  trainingCheck: any;
  value: number;
  isTraining = false;
  msgs: Message[] = [];

  // script variable
  btnScript = '';
  scriptInput: any = {
    key: '',
    script: ''
  };
  isScriptGuideOpen = false;
  scriptGuide: ScriptGuide[] = [];

  // jsplumb variable
  toolkit: jsPlumbToolkit;
  toolkitId: string;
  toolkitParams = {};
  toolkitData = {};

  surface: Surface;
  surfaceId: string;

  selectedNode: NodeData = null;
  selectedEdge = null;

  view = {
    nodes: {
      selectable: {
        events: {
          tap: (params: any) => {
            this.selectedNode = <NodeData>params.node.data;
            console.log('task click', this.selectedNode);
            this.selectedTap = TapType.STARTEVENT;
            this.initTreeData();
          }
        }
      },
      'start': {
        parent: 'selectable',
        component: StartNodeComponent2
      },
      'end': {
        parent: 'selectable',
        component: EndNodeComponent2
      },
      'task': {
        parent: 'selectable',
        component: TaskNodeComponent2
      }
    },
    edges: {
      'default': {
        anchor: 'AutoDefault',
        endpoint: 'Blank',
        connector: ['Flowchart', {cornerRadius: 5}],
        paintStyle: {
          strokeWidth: 2,
          stroke: 'rgb(132, 172, 179)',
          outlineWidth: 3,
          outlineStroke: 'transparent'
        },
        hoverPaintStyle: {strokeWidth: 2, stroke: 'rgb(67,67,67)'}, // hover paint style for this edge type.
        events: {
          'dblclick': (params: any) => {
            return false;
          },
          'click': (params: any) => {
            const edge: EdgeData = <EdgeData>params.edge;
            const edgeDataData: EdgeDataData = <EdgeDataData>edge.data;
            const nextTask: DialogNextTask = <DialogNextTask>edgeDataData.nextTask;
            this.selectedEdge = params.edge;
            this.openScriptEditModal('Transition Condition', nextTask.nextTaskCondition, false);
          }
        },
        overlays: [
          ['Arrow', {location: 1, width: 10, length: 10}]
        ]
      },
      'connection': {
        parent: 'default',
        overlays: [
          [
            'Label', {
            label: '${label}',
            events: {
              click: (params: any) => {
                return false;
              }
            }
          }
          ]
        ]
      }
    },
    ports: {
      'start': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection'
      },
      'end': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection'
      },
      'task': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection'
      },
    }
  };

  renderParams = {
    layout: {
      type: 'Spring'
    },
    events: {
      edgeAdded: (params: any) => {
        if (params.addedByMouse) {

          console.log('edgeAdded ::', params);
          const nextTask = {
            id: this.dialogService.getUUID(),
            nextTaskId: params.targetId,
            nextTaskCondition: '',
            creatorId: this.user.id,
            taskId: params.sourceId
          };

          this.dialogService.insertDialogNextTask(this.workspace.id, this.modelId, <DialogNextTask>nextTask).subscribe(response => {
            if (response && response.statusCode === 200) {
              params.edge.data['nextTask'] = <DialogNextTask>nextTask;
              this.selectedEdge = params.edge;
              this.openScriptEditModal('Transition Condition', this.selectedEdge.data.nextTask.nextTaskCondition, false);
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    },
    consumeRightClick: false,
    dragOptions: {
      filter: '.jtk-draw-handle, .node-action, .node-action i',
      start: (params: DragEventCallbackOptions) => {
        // 현재 선택된 노드로 만들어 준다.
        this.selectedNode = params.el['jtk']['node']['data'];
        // 이동하기 전 위치값을 저장
        this.selectedNode.preLeft = Number(this.selectedNode.left);
        this.selectedNode.preTop = Number(this.selectedNode.top);
      },
      stop: (params: DragEventCallbackOptions) => {
        console.log('stop', this.selectedNode);
        const clone = <DialogTask>Object.assign({}, this.selectedNode.task);
        clone.loc = this.selectedNode.left + ' ' + this.selectedNode.top;


        this.dialogService.updateDialogModelTask(this.workspace.id, this.modelId, clone).subscribe(response => {
          if (response && response.statusCode === 200) {
            this.selectedNode.task = clone;
          }
        }, error => {
          this.errorMessage(error);

          // 위치 update 실패시 이전 위치값으로 node를 무빙 시켜줘야함(함수를 못찾겠음)
          /*this.selectedNode.left = Number(this.selectedNode.preLeft);
          this.selectedNode.top = Number(this.selectedNode.preTop);
          clone.loc = Number(this.selectedNode.preLeft) + ' ' + Number(this.selectedNode.preTop);
          this.selectedNode.task = clone;*/
        });
      }
    }
  };

  // tap event variable
  selectedTap = TapType.STARTEVENT;

  // tree event variable
  selectedTree: any = null;
  tree: any = [];
  treeControl = new FlatTreeControl<any>(
    node => node.level, node => node.expandable);
  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);
  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(private router: Router, private route: ActivatedRoute, public dialog: MatDialog,
              private utilService: UtilService, private emitterService: EmitterService, public dialogService: DialogService,
              private $jsplumb: jsPlumbService, private elementRef: ElementRef, private store: Store<any>,
              private cdr: ChangeDetectorRef) {

    this.route.params.subscribe(params => {
      this.modelId = params.modelId !== ':modelId' ? params.modelId : null;
    });

    if (!this.modelId) {
      this.router.navigate(['../..']).catch();
    }

    this.workspace = {id: localStorage.getItem('m2uWorkspaceId')};
    this.user = JSON.parse(localStorage.getItem('user'));

    this.toolkitId = 'task-flowchart';
    this.surfaceId = 'task-flowchartSurface';
    this.toolkit = this.$jsplumb.getToolkit(this.toolkitId, this.toolkitParams);
    this.toolkit.clear();
  }

  ngOnInit() {
    // 모델정보 load
    this.dialogService.getDialogModel(this.workspace.id, this.modelId).subscribe(res => {
      if (res && res.statusCode === 200) {
        this.dialogModel = res.data.modelInfo;
        console.log('dialogModel', this.dialogModel);
        this.toolkitData = this.convertModelToChartData(this.dialogModel);
        this.toolkit.load({data: this.toolkitData});
        const exportData = this.toolkit.exportData();
        console.log('exportData', exportData);
      }
    });

    // 스크립트 가이드 load
    this.dialogService.getScriptGuide().subscribe((result: ScriptGuide[]) => {
      this.scriptGuide = this.scriptGuide.concat(result);
    });


    // 학습 상태 확인
    this.trainingCheck = setInterval(() => {
      const trainId = localStorage.getItem('trainId');
      this.checkTraining(trainId);
    }, 10000);


    // model intent load
    this.intentListOption.push({label: '', value: ''});
    this.dialogService.getDialogModelIntents(this.workspace.id, this.modelId).subscribe(iResponse => {
      if (iResponse && iResponse.statusCode === 200) {
        const intents = iResponse.data.intentInfoList;
        if (intents.length > 0) {
          for (const intent of intents) {
            this.intentListOption.push({
              label: intent.intentName,
              value: intent.id
            });
          }
          console.log('intentListOption :: ', this.intentListOption);
        }
      }
    }, error => {
      const errorJson = error.json();
      if (errorJson.statusCode === 400) {
        const ref = this.dialog.open(PopupErrorComponent, {
          minWidth: '45%',
          disableClose: true,
        });
        ref.componentInstance.title = '에러';
        ref.componentInstance.message = errorJson.statusMessage;
      } else {
        this.dialog.open(PopupErrorComponent, {
          minWidth: '45%',
          disableClose: true,
        });
      }
    });

    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.surface = this.surfaceComponent.surface;
    this.toolkit = this.surface.getToolkit();
  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
  }

  // training function list
  checkTraining(trainId) {
    if (trainId !== null && trainId !== undefined) {
      this.visibleTraining = true;
      this.dialogService.getTrainingProgress(trainId).subscribe(result => {
        if (result && result.statusCode === 200) {
          if (result.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = result.data.error;
          } else {
            if (result.data.resultInfo.step === 'SDS_TRAIN_DONE') {
              this.value = 100;
              localStorage.removeItem('trainId');
              this.isTraining = false;
              this.msgs = [];
              this.msgs.push({
                severity: 'info',
                summary: 'Train Status',
                detail: result.data.resultInfo.step
              });
            } else {
              this.isTraining = true;
              if (result.data.resultInfo.value && result.data.resultInfo.maximum) {
                this.value = Math.round(Number(result.data.resultInfo.value) / Number(result.data.resultInfo.maximum) * 100);
              }
            }
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  stopTraining() {
    const trainId = localStorage.getItem('trainId');
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          if (response.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = response.data.error;
          } else {
            this.isTraining = true;
            this.value = 0;
            localStorage.removeItem('trainId');
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Train Status', detail: 'CANCELED'});
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  trainOpen() {
  }

  // task event function list
  createNewTask() {
    if (this.checkModelCreator()) {
      const newTask = _.cloneDeep(DefaultDialogTask);
      let newName = 'New_Task';
      let founded = false;
      let retryCnt = 1;
      const exportData = this.toolkit.exportData();

      while (founded === false) {
        const f = exportData.nodes.filter((val) => (val['text'] === newName));
        if (f.length === 0) {
          founded = true;
        } else {
          newName = 'New_Task' + '_' + retryCnt;
          retryCnt++;
        }
      }

      newTask['taskName'] = newName;
      newTask['id'] = this.dialogService.getUUID();
      newTask['creatorId'] = this.user.id;
      newTask['taskGoal'] = '';
      const randomX = Math.floor(Math.random() * 100) + 10;
      const randomY = Math.floor(Math.random() * 100) + 10;
      newTask['loc'] = randomX + ' ' + randomY;
      newTask['workspaceId'] = this.workspace.id;
      newTask['modelId'] = this.modelId;

      this.dialogService.insertDialogModelTask(this.workspace.id, this.modelId, newTask).subscribe((response) => {
        if (response && response.statusCode === 200) {
          console.log('newTask', newTask);
          const node = new NodeData();
          node.id = newTask.id;
          node.text = newTask.taskName;
          node.type = 'task';
          node.left = newTask.loc ? Number(newTask.loc.split(' ')[0]) : Math.floor(Math.random() * 100) + 10;
          node.top = newTask.loc ? Number(newTask.loc.split(' ')[1]) : Math.floor(Math.random() * 100) + 10;
          node.task = newTask;

          this.toolkit.addNode(node);

        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  removeTask() {
    if (this.checkModelCreator()) {
      this.dialogService.deleteDialogModelTask(this.workspace.id, this.modelId, this.selectedNode.id).subscribe(response => {
        if (response && response.statusCode === 200) {
          this.toolkit.removeNode(this.selectedNode.id);
          this.selectedNode = null;
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  saveTaskName() {
    // api 실패의 경우 data rollback을 위해 clone data 생성
    const taskName = String(this.selectedNode.text);
    const param = Object.assign({}, this.selectedNode.task);
    param.taskName = taskName;

    this.dialogService.updateDialogModelTask(this.workspace.id, this.modelId, param).subscribe(response => {
      if (response && response.statusCode === 200) {
        this.selectedNode.task = param;
        this.toolkit.update(this.toolkit.getNode(this.selectedNode.id), {text: taskName});
      }
    }, error => {
      this.errorMessage(error);
      this.toolkit.update(this.toolkit.getNode(this.selectedNode.id), {text: String(this.selectedNode.task.taskName)});
    });
  }

  addTaskResponse() {
    if (this.checkModelCreator()) {
      if (this.selectedTap === TapType.STARTEVENT && (this.selectedTree ? this.selectedTree.type === TreeType.CONDITION : true)) {
        const startEvent = this.generateTaskStartEvent();

        this.dialogService.insertDialogModelTaskStartEvent(this.workspace.id, this.modelId, startEvent).subscribe(eResponse => {
          if (eResponse && eResponse.statusCode === 200) {

            this.selectedNode.task.startEvent.push(startEvent);
            this.initTreeData();

            const startEventUtter = this.generateTaskStartEventUtter(startEvent.id);

            this.dialogService.insertDialogModelTaskStartEventUtter(
              this.workspace.id, this.modelId, this.selectedNode.id, startEventUtter).subscribe(uResponse => {
              if (uResponse && uResponse.statusCode === 200) {

                startEvent.startEventUtter.push(startEventUtter);
                this.initTreeData();
                const startEventUtterData = this.generateTaskStartEventUtterData(startEventUtter.id);

                this.dialogService.insertDialogModelTaskStartEventUtterData(
                  this.workspace.id, this.modelId, this.selectedNode.id, startEvent.id, startEventUtterData).subscribe(res => {
                  if (res && res.statusCode === 200) {
                    startEventUtter.startEventUtterData.push(startEventUtterData);
                    this.initTreeData();
                  }
                }, error => {
                  this.errorMessage(error);
                });
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        }, error => {
          this.errorMessage(error);
        });
      } else if (this.selectedTap === TapType.STARTEVENT && (this.selectedTree ? this.selectedTree.type === TreeType.UTTERANCE : false)) {
        console.log('add startEvent utteracne');

        const startEventUtter = this.generateTaskStartEventUtter(this.selectedTree.startEventId);

        this.dialogService.insertDialogModelTaskStartEventUtter(
          this.workspace.id, this.modelId, this.selectedNode.id, startEventUtter).subscribe(uResponse => {
          if (uResponse && uResponse.statusCode === 200) {

            const startEvent = this.findTreeDataForId(this.selectedTree.startEventId, this.tree);
            startEvent.startEventUtter.push(startEventUtter);
            this.initTreeData();
            const startEventUtterData = this.generateTaskStartEventUtterData(startEventUtter.id);

            this.dialogService.insertDialogModelTaskStartEventUtterData(
              this.workspace.id, this.modelId, this.selectedNode.id, startEventUtter.id, startEventUtterData).subscribe(res => {
              if (res && res.statusCode === 200) {
                startEventUtter.startEventUtterData.push(startEventUtterData);
                this.initTreeData();
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  removeTaskResponse() {
    if (this.checkModelCreator()) {
      if (this.selectedTree) {
        if (this.selectedTap === TapType.STARTEVENT) {
          if (this.selectedTree.type === TreeType.CONDITION) {

            const startEventUtter = new TaskStartEvent();
            startEventUtter.taskId = this.selectedTree.taskId;
            startEventUtter.id = this.selectedTree.id;

            this.dialogService.deleteDialogModelTaskStartEvent(
              this.workspace.id, this.modelId, startEventUtter).subscribe(response => {
              if (response && response.statusCode === 200) {
                this.selectedNode.task.startEvent.splice(this.selectedTree.index, 1);
                this.initTreeData();
              }
            }, error => {
              this.errorMessage(error);
            });
          } else if (this.selectedTree.type === TreeType.UTTERANCE) {
            console.log('--- startEvent tap, UTTERANCE');
          }
        } else if (this.selectedTap === TapType.EVENT) {
          console.log('--- event tap');
        }
      }
    }
  }

  generateTaskStartEvent() {
    const startEvent = new TaskStartEvent();
    startEvent.id = this.dialogService.getUUID();
    startEvent.taskId = this.selectedNode.id;
    startEvent.startEventCondition = '';
    startEvent.startEventAction = '';
    startEvent.startEventUtter = [];
    startEvent.creatorId = this.user.id;

    return startEvent;
  }

  generateTaskStartEventUtter(startEventId: string) {
    if (startEventId) {
      const startEventUtter = new TaskStartEventUtter();
      startEventUtter.id = this.dialogService.getUUID();
      startEventUtter.startEventId = startEventId;
      startEventUtter.intentId = '';
      startEventUtter.extraData = '';
      startEventUtter.startEventUtterData = [];
      startEventUtter.creatorId = this.user.id;
      return startEventUtter;
    } else {
      return null;
    }
  }

  generateTaskStartEventUtterData(startEventUtterId: string) {
    if (startEventUtterId) {
      const startEventUtterData = new TaskStartEventUtterData();
      startEventUtterData.id = this.dialogService.getUUID();
      startEventUtterData.startEventUtterId = startEventUtterId;
      startEventUtterData.startEventUtterData = '';
      startEventUtterData.creatorId = this.user.id;
      return startEventUtterData;
    } else {
      return null;
    }
  }

  // edge function list
  deleteEdge() {
    const nextTask = this.selectedEdge.data.nextTask;
    this.dialogService.deleteDialogNextTask(this.workspace.id, this.modelId, nextTask.taskId, nextTask.id)
    .subscribe(res => {
      if (res && res.statusCode === 200) {
        this.toolkit.removeEdge(this.selectedEdge);
        this.selectedEdge = null;
        this.popupClose();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  // tab event
  eventClick(tapType) {
    this.selectedTap = tapType;
    this.initTreeData();
    console.log('selectedTap..', this.selectedTap);
  }


  // tree function list
  initTreeData() {
    if (this.selectedNode) {
      this.selectedTree = null;
      this.tree = [];
      const treeData = [];

      if (this.selectedTap === TapType.STARTEVENT) {
        this.selectedNode.task.startEvent.forEach((item, index) => {
          item['index'] = index;
          item['alias'] = 'condition' + index;
          item['type'] = TreeType.CONDITION;
          item['children'] = item.startEventUtter;
          //item['data'] = Object.assign({}, item);

          item.startEventUtter.forEach((item2, index2) => {
            item2['index'] = index2;
            item2['alias'] = 'utterance' + index2;
            item2['type'] = TreeType.UTTERANCE;

            item2.startEventUtterData.forEach((item3, index3) => {
              item3['index'] = index3;
            });

            //item2['data'] = Object.assign({}, item2);
          });

          treeData.push(item);
        });

        this.tree = treeData;
        this.dataSource.data = this.tree;
        console.log('tree', this.dataSource.data);
      } else if (this.selectedTap === TapType.EVENT) {
        this.selectedNode.task.event.forEach((item, index) => {
          item['alias'] = 'intent' + index;
          item['data'] = item;
          item['children'] = item.eventCondition;

          item.eventCondition.forEach((item2, index2) => {
            item2['alias'] = 'condition' + index2;
            item2['data'] = item2;
            item2['children'] = item2.eventUtter;

            item2.eventUtter.forEach((item3, index3) => {
              item3['alias'] = 'utterance' + index3;
            });
          });

          treeData.push(item);
        });

        this.tree = treeData;
        this.dataSource.data = this.tree;
        console.log('tree', this.dataSource.data);
      }
    }
  }

  onTreeSelected(node) {
    console.log('onTreeSelected', node);
    this.selectedTree = node.node;
  }

  findTreeDataForId(id: string, child: any) {
    let root = [];
    let result = null;

    if (child) {
      root = child;

      root.some((item, index) => {
        if (id === item.id) {
          result = item;
          return true;
        } else {
          const childResult = this.findTreeDataForId(id, item['children']);
          if (childResult) {
            result = childResult;
            return true;
          }
        }
      });
    }

    return result;
  }

  transformer(node: any, level: number) {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.alias,
      level: level,
      node: node
    };
  }

  hasChild(number: number, node: any) {
    return node.expandable;
  }

  // tree data function list
  changeUtterance() {
    console.log('chageIntent....');
    if (this.checkModelCreator()) {
      if (this.selectedTap === TapType.STARTEVENT && (this.selectedTree ? this.selectedTree.type === TreeType.UTTERANCE : false)) {
        console.log('start event utternce');
        console.log(this.selectedTree, this.selectedIntent);

        const param = Object.assign({}, this.selectedTree);
        param['extraData'] = '';
        param['intentId'] = this.selectedIntent;

        this.dialogService.updateDialogModelTaskStartEventUtter(
          this.workspace.id, this.modelId, this.selectedNode.id, <TaskStartEventUtter>param).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTree = '';
            this.selectedTree = this.selectedIntent;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }

  }

  // script popup function list
  openScriptEditModal(key: string, script: string, isStartEvent: boolean) {
    $('.task-popup-default-wrap').css('display', 'table');
    this.scriptInput = {
      key: key,
      script: script,
      isStartEvent: isStartEvent
    };

    this.highlightDiv.nativeElement.innerHTML = this.applyHighlights(this.scriptInput.script);
  }


  // popup function list
  popupSave(script: string, key: string, isStartEvent: boolean) {
    if (this.checkModelCreator()) {
      $('.task-popup-default-wrap').css('display', 'none');

      if (key === 'Transition Condition') {
        console.log('key ::', this.selectedEdge);
        this.selectedEdge.data.nextTask.nextTaskCondition = script;
        this.dialogService.updateDialogNextTask(this.workspace.id, this.modelId, this.selectedEdge.source, this.selectedEdge.data.nextTask)
        .subscribe(res => {
          if (res && res.statusCode === 200) {
            return false;
          }
        }, error => {
          this.errorMessage(error);
        });
      } else if (key === 'Task Goal') {
        // api 실패의 경우 data rollback을 위해 clone data 생성
        const param = Object.assign({}, this.selectedNode.task);
        param.taskGoal = script;

        this.dialogService.updateDialogModelTask(this.workspace.id, this.modelId, param).subscribe(response => {
          if (response && response.statusCode === 200) {
            this.selectedNode.task.taskGoal = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      }

      if (this.selectedTap === TapType.STARTEVENT) {
        const clone = Object.assign({}, this.selectedTree);
        const param = new TaskStartEvent();
        param.id = clone.id;
        param.taskId = clone['taskId'];
        param.startEventCondition = (key === 'Condition' ? script : clone['startEventCondition']);
        param.startEventAction = (key === 'Action' ? script : clone['startEventAction']);
        param.creatorId = this.user.id;

        this.dialogService.updateDialogModelTaskStartEvent(
          this.workspace.id, this.modelId, param).subscribe(res => {
          if (res && res.statusCode === 200) {
            if (key === 'Condition') {
              this.selectedTree['startEventCondition'] = script;
            } else if (key === 'Action') {
              this.selectedTree['startEventAction'] = script;
            }
          }
        }, error => {
          this.errorMessage(error);
        });
      } else if (this.selectedTap === TapType.EVENT) {

      }

    }


    /*if (key === 'Task Goal') {
      this.selectedTask.taskGoal = script;
      this.saveTask();
    } else if (key === 'Condition') {
      if (isStartEvent) {
        this.selectedNode.data.original.startEventCondition = script;
        this.dialogService.updateDialogModelTaskStartEvent(
          this.workspace.id, this.modelId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.startEvent[this.selectedNode.data.index].startEventCondition = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        this.selectedNode.data.original.eventCondition = script;
        const taskId = this.selectedNode.parent.data.original.taskId;
        this.dialogService.updateDialogModelTaskEventCondition(
          this.workspace.id, this.modelId, taskId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.event[this.selectedNode.parent.data.index].eventCondition[this.selectedNode.data.index].eventCondition
              = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    } else if (key === 'Action') {
      if (isStartEvent) {
        this.selectedNode.data.original.startEventAction = script;
        this.dialogService.updateDialogModelTaskStartEvent(
          this.workspace.id, this.modelId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.startEvent[this.selectedNode.data.index].startEventAction = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        this.selectedNode.data.original.eventAction = script;
        const taskId = this.selectedNode.parent.data.original.taskId;
        this.dialogService.updateDialogModelTaskEventCondition(
          this.workspace.id, this.modelId, taskId, this.selectedNode.data.original).subscribe(res => {
          if (res && res.statusCode === 200) {
            this.selectedTask.event[this.selectedNode.parent.data.index].eventCondition[this.selectedNode.data.index].eventAction
              = script;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    } else if (key === 'Transition Condition') {
      this.syncTaskDiagram();
      this.selectedLink.condition = script;
      const task = this.tasks.find(x => x.id === this.selectedLink.fromId);
      const nextTask = task.nextTask.find(x => x.id && x.taskId === this.selectedLink.fromId && x.nextTaskId === this.selectedLink.toId);
      nextTask.nextTaskCondition = script;
      this.dialogService.updateDialogNextTask(this.workspace.id, this.modelId, task.id, nextTask).subscribe(res => {
        if (res && res.statusCode === 200) {
          this.syncTaskDiagram();
        }
      }, error => {
        this.errorMessage(error);
      });
    }*/
  }

  popupClose() {
    $('.task-popup-default-wrap').css('display', 'none');
    this.scriptInput = {
      key: '',
      script: '',
      isStartEvent: false
    }
  }

  scriptToggle() {
    this.isScriptGuideOpen = !this.isScriptGuideOpen;
    if (this.isScriptGuideOpen) {
      $('.popup-container').css('width', '1010px');
      this.btnScript = 'primary';
    } else {
      $('.popup-container').css('width', '600px');
      this.btnScript = 'default';
    }
  }

  onMentionSelect(text: any) {
    return text.function;
  }

  handleInput(text) {
    const highlightedText = this.applyHighlights(text);
    this.highlightDiv.nativeElement.innerHTML = highlightedText;
  }

  handleScroll(event) {
    this.highlightDiv.nativeElement.scrollTop = event.currentTarget.scrollTop;
  }

  applyHighlights(text) {
    if (!text || text === null || text === '') {
      return '';
    } else {
      return text
      .replace(/\n$/g, '\n\n')
      .replace(/Count\b/g, '<mark>$&</mark>')
      .replace(/ExistValueFromDialog\b/g, '<mark>$&</mark>')
      .replace(/ExistValueAtHistory\b/g, '<mark>$&</mark>')
      .replace(/ExistValue\b/g, '<mark>$&</mark>')
      .replace(/ExistSlotAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/ExistSlotAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/ExistSlotAtUtterHistory\b/g, '<mark>$&</mark>')
      .replace(/ExistClassAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/ExistClassAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/HasValueHistory\b/g, '<mark>$&</mark>')
      .replace(/HasValueAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/HasValueAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/HasValue\b/g, '<mark>$&</mark>')
      .replace(/IsDAType\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtCurrentUtter\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtPreviousUtter\b/g, '<mark>$&</mark>')
      .replace(/IsDATypeAtUtterHistory\b/g, '<mark>$&</mark>')
      .replace(/isDATypeAtCurrentTask\b/g, '<mark>$&</mark>')
      .replace(/IsDA\b/g, '<mark>$&</mark>')
      .replace(/IsConfirmTrue\b/g, '<mark>$&</mark>')
      .replace(/Value\b/g, '<mark>$&</mark>');
    }
  }


  // common function list
  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }

  checkModelCreator() {
    if (this.dialogModel.creatorId + '' !== this.user.id + '') {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '경고';
      ref.componentInstance.message = '다른 사용자가 작성한 모델은 수정할 수 없습니다.';
      return false;
    } else {
      return true;
    }
  }

  convertModelToChartData(model) {
    const chartData = {nodes: [], edges: []};

    model.task.forEach((dialogTask: DialogTask) => {
      const tempNode = new NodeData();
      const nextTask: DialogNextTask[] = <DialogNextTask[]>dialogTask.nextTask;

      tempNode.id = dialogTask.id;
      tempNode.type = 'task';
      tempNode.text = dialogTask.taskName;
      tempNode.left = dialogTask.taskLocation ? Number(dialogTask.taskLocation.split(' ')[0]) : Math.floor(Math.random() * 100) + 10;
      tempNode.top = dialogTask.taskLocation ? Number(dialogTask.taskLocation.split(' ')[1]) : Math.floor(Math.random() * 100) + 10;
      tempNode.task = dialogTask;
      chartData.nodes.push(tempNode);

      nextTask.forEach((next) => {
        const tempEdge = new EdgeData();

        tempEdge.id = next.id
        tempEdge.source = dialogTask.id;
        tempEdge.target = next.nextTaskId;
        tempEdge.data.type = 'connection';
        tempEdge.data.label = '라벨을 입력하세요';
        tempEdge.data.nextTask = next;
        chartData.edges.push(tempEdge);
      });
    });

    console.log('chartData', chartData);
    return chartData;
  }


}
