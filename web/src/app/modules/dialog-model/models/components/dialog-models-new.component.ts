import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
} from '@angular/core';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {DialogModel, DialogService, DialogTask, DialogTaskType} from '../../service/dialog.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {PopupErrorComponent} from '../../shared/components/popup/popup.error.component';

class ExtraParams {
  isCopyModel = false;
  modelId: string = null;
}

@Component({
  selector: 'app-dialog-models-new',
  templateUrl: './dialog-models-new.component.html',
  styleUrls: ['./dialog-models-new.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DialogModelsNewComponent implements OnInit {

  @ViewChild('upload') upload;

  extraParams: ExtraParams;
  workspace: any;
  user: any;

  dialogModel: DialogModel = new DialogModel();
  sourceDialogModel: DialogModel;

  uploadModelFile: any = null;
  uploadModelFileName = '';
  uploadDialogModel: DialogModel = null;
  jsonText: any;

  // AOT BUILD: 윤기현
  isCopyModel = false;

  constructor(private store: Store<any>, private router: Router, private route: ActivatedRoute,
              private dialogService: DialogService, public dialog: MatDialog) {

    this.workspace = {id: localStorage.getItem('m2uWorkspaceId')};
    this.user = JSON.parse(localStorage.getItem('user'));

    this.extraParams = new ExtraParams();
    this.route.queryParams.subscribe(query => {
      this.extraParams.isCopyModel = query['isCopyModel'] ? query['isCopyModel'] === 'true' ? true : false : false;
      this.extraParams.modelId = query['modelId'];
    });
  }

  ngOnInit() {
    console.log('onInit DialogModelsNewComponent...');

    if (!this.extraParams.isCopyModel) {
      this.dialogModel = new DialogModel();
      this.dialogModel.id = this.dialogService.getUUID();
      this.dialogModel.status = 'edit';
      this.dialogModel.creatorId = this.user.id;
      this.dialogModel.workspaceId = this.workspace.id;
      this.dialogModel.workspaceName = this.workspace.name;
      const greetTask: DialogTask = {
        id: 'Greet',
        taskName: 'Greet',
        taskGoal: null,
        taskType: DialogTaskType.ESSENTIAL,
        resetSlot: 'no',
        creatorId: this.dialogModel.creatorId,
        updaterId: this.dialogModel.creatorId,
        workspaceId: this.dialogModel.workspaceId,
        modelId: this.dialogModel.id,
        startEvent: [],
        event: [],
        nextTask: [],
        key: '',
        loc: '0 0',
        taskLocation: '0 0'
      };
      this.dialogModel.tasks.push(greetTask);
    } else {

      this.sourceDialogModel = null;

      this.dialogService.getDialogModel(this.workspace.id, this.extraParams.modelId).subscribe(res => {
        if (res && res.statusCode === 200) {
          this.sourceDialogModel = res.data.modelInfo;

          this.dialogModel.id = this.sourceDialogModel.id;
          this.dialogModel.modelName = this.sourceDialogModel.modelName + '_copy';
          this.dialogModel.description = this.sourceDialogModel.description + '_copy';
          this.dialogModel.status = 'edit';
          this.dialogModel.tasks = this.sourceDialogModel.tasks;
          this.dialogModel.intents = this.sourceDialogModel.intents;
          this.dialogModel.entities = this.sourceDialogModel.entities;
          this.dialogModel.creatorId = this.user.id;
          this.dialogModel.workspaceId = this.workspace.id;
          this.dialogModel.workspaceName = this.workspace.name;
        }
      }, error => {
        this.errorMessage(error);
      });
    }

    this.store.dispatch({type: ROUTER_LOADED});
  }

  btnBack() {
    this.router.navigate(['..'], {relativeTo: this.route}).catch();
  }

  btnCreate() {
    if (this.uploadModelFileName !== '') {
      const reader = new FileReader();
      reader.onload = () => {
        this.jsonText = reader.result;
        try {
          this.uploadDialogModel = JSON.parse(this.jsonText);

          this.dialogModel.tasks = this.uploadDialogModel.tasks;
          this.dialogModel.intents = this.uploadDialogModel.intents;
          this.dialogModel.entities = this.uploadDialogModel.entities;

          this.dialogService.insertDialogModel(this.dialogModel).subscribe(iResponse => {
            if (iResponse && iResponse.statusCode === 200) {
              this.dialogService.getDialogModelList(localStorage.getItem('currentWorkspaceId'))
              .subscribe(lResponse => {
                if (lResponse && lResponse.statusCode === 200) {
                  this.router.navigate(['..'], {relativeTo: this.route}).catch();
                }
              }, error => {
                this.errorMessage(error);
              });
            }
          }, error => {
            this.errorMessage(error);
          });
        } catch (e) {
          this.uploadDialogModel = null;
          this.uploadModelFileName = '';
          this.uploadModelFile = null;

          alert('Json Parsing Error.');
        }
      };

      reader.readAsText(this.uploadModelFile);
    } else {
      if (!this.extraParams.isCopyModel) {
        this.dialogService.insertDialogModel(this.dialogModel).subscribe(iResponse => {
          if (iResponse && iResponse.statusCode === 200) {
            this.dialogService.getDialogModelList(localStorage.getItem('currentWorkspaceId'))
            .subscribe(lResponse => {
              if (lResponse && lResponse.statusCode === 200) {
                this.router.navigate(['..'], {relativeTo: this.route}).catch();
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        }, error => {
          this.errorMessage(error);
        });
      } else {
        this.dialogService.copyDialogModel(this.dialogModel).subscribe(iResponse => {
          if (iResponse && iResponse.statusCode === 200) {
            this.dialogService.getDialogModelList(localStorage.getItem('currentWorkspaceId'))
            .subscribe(lResponse => {
              if (lResponse && lResponse.statusCode === 200) {
                this.router.navigate(['..'], {relativeTo: this.route}).catch();
              }
            }, error => {
              this.errorMessage(error);
            });
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }

  public uploadFile(event: any) {
    if (event.target.files.length === 0) {
      this.uploadModelFileName = '';
      this.uploadModelFile = null;
    } else {
      this.uploadModelFileName = event.target.files[0].name;
      this.uploadModelFile = event.target.files[0];
    }
  }

}
