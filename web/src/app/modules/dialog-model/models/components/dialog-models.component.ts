import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import * as $ from 'jquery';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogModel, DialogService} from '../../service/dialog.service';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material';
import {PopupErrorComponent} from 'app/modules/dialog-model/shared/components/popup';

interface TrainingProgress {
  started: string;
  elapsed: string;
  maximum: number;
  value: number;
  progress: number;
  step: string;
  modelName: string;
  modelId: string;
  key: string;
}

@Component({
  selector: 'app-dialog-models',
  templateUrl: './dialog-models.component.html',
  styleUrls: ['./dialog-models.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DialogModelsComponent implements OnInit {

  user: any;
  workspace: any;

  displayedColumns = ['name', 'id', 'status', 'create', 'setting', 'copy', 'download'];
  dataSource: any;

  dialogModelList: DialogModel[];
  value: number;
  trainingCheck: any;
  trainingProgressList: TrainingProgress[];

  constructor(private store: Store<any>, private router: Router, private dialogService: DialogService,
              public dialog: MatDialog, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    console.log('onInit DialogModelsComponent...');
    localStorage.setItem('m2uWorkspace', JSON.stringify({id: '1', name: 'temp'}));
    this.user = JSON.parse(localStorage.getItem('user'));
    this.workspace = {id: localStorage.getItem('m2uWorkspaceId')};
    this.getDialogModelList();
    this.getTrainingProgressAll();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
  }

  getDialogModelList() {
    this.dialogService.getDialogModelList(this.workspace.id)
    .subscribe(response => {
      if (response && response.statusCode === 200) {
        const modelInfoList = response.data.modelInfoList;
        // localStorage.setItem('dialogModelList', JSON.stringify(modelInfoList));
        this.dialogModelList = modelInfoList;
        this.dataSource = new MatTableDataSource(this.dialogModelList);
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  getTrainingProgressAll() {
    this.trainingCheck = setInterval(() => {
      this.dialogService.getTrainingProgressAll().subscribe(res => {
        this.trainingProgressList = [];
        if (res && res.statusCode === 200) {
          const progressList = res.data.resultInfo;
          if (progressList && progressList.length > 0) {
            for (const progress of progressList) {
              let modelName = '';
              let modelId = '';
              if (progress.model && progress.model.length > 0) {
                const idx = progress.model.lastIndexOf('_');
                modelName = progress.model.substr(0, idx);
                modelId = progress.model.substr(idx + 1, progress.model.length);
              }
              const data: TrainingProgress = {
                elapsed: progress.elapsed,
                started: progress.started,
                key: progress.key,
                maximum: progress.maximum,
                value: (progress.value) ? progress.value : progress.maximum,
                progress: (progress.value) ? Math.round(Number(progress.value) / Number(progress.maximum) * 100) : 100,
                step: progress.step,
                modelName: modelName,
                modelId: modelId
              };
              this.trainingProgressList.push(data);
            }
          }
        }
      });
    }, 1000000);
  }

  stopTraining(trainId) {
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          const idx = this.trainingProgressList.findIndex(x => x.key === trainId);
          this.trainingProgressList.splice(idx, 1);
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  applyFilterIntent(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  moveCreate() {
    const extra = {
      queryParams: {
        isCopyModel: false
      },
      preserveQueryParams: true,
      relativeTo: this.activatedRoute
    };
    this.router.navigate(['..', 'new'], extra).catch();
  }

  moveSetting($event, element) {
    $event.stopPropagation();
    if (parseInt(element.creatorId, 10) === this.user.id) {
      this.router.navigate([element.id, 'edit'], {relativeTo: this.activatedRoute}).catch();
    } else {
      alert('다른 사용자가 작성한 모델은 수정할 수 없습니다.');
    }
  }

  moveCopy($event, element) {
    $event.stopPropagation();
    if (element.id) {
      const extra = {
        queryParams: {
          isCopyModel: true,
          modelId: element.id
        },
        preserveQueryParams: false
      };

      this.router.navigate(['..', 'new'], extra).catch();
    }
  }

  downloadModel($event, element) {
    $event.stopPropagation();
    this.dialogService.getDialogModel(element.workspaceId, element.id).subscribe(response => {
      if (response && response.statusCode === 200) {
        const dataStr = JSON.stringify(response.data.modelInfo);
        const dataUri = 'data:application/json;charset=utf-8,' + encodeURIComponent(dataStr);

        const date = new Date();
        const year = date.getFullYear();
        let month = (date.getMonth() + 1).toString();
        let day = date.getDate().toString();

        if (month.length === 1) {
          month = '0' + month;
        }
        if (day.length === 1) {
          day = '0' + day;
        }
        const exportFileDefaultName = element.modelName + '_' + year + month + day + '.json';
        const linkElement = document.createElement('a');
        linkElement.setAttribute('href', dataUri);
        linkElement.setAttribute('download', exportFileDefaultName);
        linkElement.click();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  rowModel($event) {
    const modelIndex = $($event.currentTarget).index();
    const modelData = this.dataSource.data[modelIndex];

    if (modelData.id) {
      localStorage.setItem('currentDialogModel', JSON.stringify(modelData));
      this.router.navigate([modelData.id, 'task2'], {relativeTo: this.activatedRoute}).catch();
    }

    /*if (modelData.modelStatus === 'commit') {
      $('.model-commit-wrap').css('display', 'inline-block');
    } else if (modelData.modelStatus === 'edit') {
      $('.model-commit-wrap').css('display', 'none');
    }*/
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }

}
