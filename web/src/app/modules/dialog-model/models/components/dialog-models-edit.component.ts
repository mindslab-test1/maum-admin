import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogModel, DialogService} from '../../service/dialog.service';
import {MatDialog} from '@angular/material';
import {PopupErrorComponent} from '../../shared/components/popup/popup.error.component';

@Component({
  selector: 'app-dialog-models-edit',
  templateUrl: './dialog-models-edit.component.html',
  styleUrls: ['./dialog-models-new.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DialogModelsEditComponent implements OnInit {

  user: any;
  workspace: any;

  modelId: string;

  selectedDialogModel: DialogModel;

  constructor(private store: Store<any>, private router: Router, private route: ActivatedRoute,
              private dialogService: DialogService, public dialog: MatDialog) {

    this.workspace = {id: localStorage.getItem('m2uWorkspaceId')};
    this.user = JSON.parse(localStorage.getItem('user'));
    this.selectedDialogModel = new DialogModel();

    this.route.params.subscribe(params => {
      this.modelId = params.id;
    });

    this.dialogService.getDialogModel(this.workspace.id, this.modelId).subscribe(res => {
      if (res && res.statusCode === 200) {
        this.selectedDialogModel = res.data.modelInfo;
      }
    }, error => {
      this.errorMessage(error);
    });

  }

  ngOnInit() {
    console.log('onInit DialogModelsEditComponent...');
    this.store.dispatch({type: ROUTER_LOADED});
  }

  modelDelete() {
    this.dialogService.deleteDialogModel(this.selectedDialogModel.workspaceId, this.selectedDialogModel.id).subscribe(res => {
      if (res && res.statusCode === 200) {
        localStorage.removeItem('selectedDialogModel');
        this.router.navigate(['../..'], {relativeTo: this.route}).catch();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  modelSave() {
    this.dialogService.updateDialogModelInfo(this.selectedDialogModel)
    .subscribe(response => {
      if (response && response.statusCode === 200) {
        this.router.navigate(['../..'], {relativeTo: this.route}).catch();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  btnBack() {
    this.router.navigate(['../..'], {relativeTo: this.route}).catch();
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }

}
