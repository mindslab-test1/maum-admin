import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {
  DialogEntity, DialogIntent, DialogModel, DialogService,
  DialogTask, UtilService
} from '../../service';
import * as go from 'gojs';
import {Message} from 'primeng/primeng';

import {ChatMessage, MessageType, Type} from '../../chatbot/chatbot.models';
import * as _ from 'lodash';

import * as $ from 'jquery';
import {ChatBotComponent} from '../../chatbot/components/chatbot.component';
import {PopupErrorComponent} from '../../shared/components/popup/popup.error.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TestComponent implements OnInit, AfterViewInit, OnDestroy {
  private diagram: go.Diagram;
  public tasks: DialogTask[];
  intentDataSource;
  entitiesDataSource;
  displayedColumns = ['name', 'value'];
  currentTask: string;

  @ViewChild('dialogModelTaskContainer')
  dialogModelTaskContainer: ElementRef;

  testMessages: Array<ChatMessage>;

  @ViewChild('dialogTestChatbot')
  dialogTestChatbot: ChatBotComponent;

  @ViewChild('logContainer')
  logContainer: ElementRef;

  isLoaded: boolean;
  workspaceId: string;
  modelId: string;
  private dialogTasksLinkInfo: {
    id: string,
    from: string,
    to: string,
    condition: string,
    controller: string,
    fromId: string,
    toId: string
  }[] = [];

  dialogEntities: {
    name: string,
    value: string
  }[] = [];

  dialogIntents: {
    name: string,
    value: string
  }[] = [];

  value: number;
  isTraining: boolean;
  visibleTraining: boolean;
  dialogModel: DialogModel;
  msgs: Message[] = [];
  trainingCheck: any;
  isConversationOpened: boolean;

  host: string;
  port: number;
  sessionKey: number;
  isLogOn: boolean;
  errorLog: string;

  constructor(private router: Router, private store: Store<any>,
              public dialog: MatDialog,
              private utilService: UtilService,
              public dialogService: DialogService) {
    this.dialogModel = JSON.parse(localStorage.getItem('currentDialogModel')) as DialogModel;
    this.workspaceId = this.dialogModel.workspaceId;
    this.modelId = this.dialogModel.id;
    this.testMessages = [];
    this.testMessages.push(_.cloneDeep({
      type: Type.SERVER,
      msgType: MessageType.TEXT,
      message: 'Loading'
    }));
    this.value = 0;
    this.isTraining = false;
    this.visibleTraining = false;
    this.isConversationOpened = false;
    this.diagram = this.utilService.initGoJSDiagramReadOnly();
    this.isLoaded = false;
    this.isLogOn = false;
    this.errorLog = '';
  }

  ngOnInit() {
    this.selectAllTask();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.diagram.div = this.dialogModelTaskContainer.nativeElement;
    // this.openConversationTemp();
    this.openConversation();
  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
    this.closeConversation();
  }

  selectAllTask() {
    this.dialogService.getDialogModelTaskAll(this.workspaceId, this.modelId).subscribe(tResponse => {
      this.tasks = [];
      if (tResponse && tResponse.statusCode === 200) {
        this.tasks = tResponse.data.task;
        if (this.tasks.length > 0) {
          for (const task of this.tasks) {
            task.key = task.taskName;
            task.loc = task.taskLocation;
          }
        }
        const generalTask = this.tasks.filter((val) => (val.taskName !== 'GLOBAL_TASK'));
        this.createLinkInfoFromDialogTasks();

        this.diagram.model = new go.GraphLinksModel(generalTask, this.dialogTasksLinkInfo);
        if (this.currentTask) {
          this.taskDiagramFocusByKey(this.currentTask);
        }
        this.selectEntityListAll();
        this.selectIntentAll();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  selectEntityListAll() {
    this.dialogService.getDialogModelEntityAll(this.workspaceId, this.modelId).subscribe(response => {
      let entityList: DialogEntity[] = [];
      if (response && response.statusCode) {
        entityList = response.data.entity;
        if (entityList.length > 0) {
          for (const entity of entityList) {
            this.dialogEntities.push({
              name: entity.entityName,
              value: ''
            });
          }
          this.dialogEntities = this.dialogEntities.concat();
          this.entitiesDataSource = new MatTableDataSource(this.dialogEntities);
        }
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  selectIntentAll() {
    this.dialogService.getDialogModelIntentAll(this.workspaceId, this.modelId)
    .subscribe(iResponse => {
      let intentList: DialogIntent[] = [];
      if (iResponse && iResponse.statusCode === 200) {
        intentList = iResponse.data.intent;
        if (intentList.length > 0) {
          for (const intent of intentList) {
            this.dialogIntents.push({
              name: intent.intentName,
              value: ''
            });
          }
          this.dialogIntents = this.dialogIntents.concat();
          this.intentDataSource = new MatTableDataSource(this.dialogIntents);
        }
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  taskDiagramFocusByKey(key: string) {
    this.diagram.clearHighlighteds();
    this.diagram.highlight(this.diagram.findNodeForKey(key));
  }

  openConversation() {
    // console.log(this.dialogModel);
    if (this.dialogModel.lastTrainKey && this.dialogModel.lastTrainKey !== '') {
      this.dialogService.getTrainingBinary(this.dialogModel.modelName, this.dialogModel.lastTrainKey).subscribe(bRes => {
        // console.log('getTrainingBinary', bRes);
        if (bRes && bRes.statusCode === 200) {
          if (bRes.data.hasOwnProperty('error')) {
            this.replaceLastChatMessage();
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = bRes.data.error;
          } else {
            this.dialogService.updateTalkResolverModel(this.dialogModel).subscribe(uRes => {
              // console.log('updateTalkResolverModel', uRes);
              if (uRes && uRes.statusCode === 200) {
                if (uRes.result.hasOwnProperty('error')) {
                  this.replaceLastChatMessage();
                  const ref = this.dialog.open(PopupErrorComponent, {
                    minWidth: '45%',
                    disableClose: true,
                  });
                  ref.componentInstance.title = '에러';
                  ref.componentInstance.message = uRes.data.error;
                } else {
                  this.dialogService.linkTalkResolverModel(this.dialogModel).subscribe(lRes => {
                    // console.log('linkTalkResolverModel', lRes);
                    if (lRes && lRes.statusCode === 200) {
                      if (lRes.result.hasOwnProperty('error')) {
                        this.replaceLastChatMessage();
                        const ref = this.dialog.open(PopupErrorComponent, {
                          minWidth: '45%',
                          disableClose: true,
                        });
                        ref.componentInstance.title = '에러';
                        ref.componentInstance.message = lRes.data.error;
                      } else {
                        this.dialogService.findTalkResolverModel('sds_train').subscribe(fRes => {
                          // console.log('findTalkResolverModel', fRes);
                          if (fRes && fRes.statusCode === 200) {
                            if (fRes.result.hasOwnProperty('error')) {
                              this.replaceLastChatMessage();
                              const ref = this.dialog.open(PopupErrorComponent, {
                                minWidth: '45%',
                                disableClose: true,
                              });
                              ref.componentInstance.title = '에러';
                              ref.componentInstance.message = fRes.data.error;
                            } else {
                              const found = fRes.result.resultInfo;
                              this.host = found.serverAddress.split(':')[0];
                              this.port = Number(found.serverAddress.split(':')[1]);
                              this.sessionKey = Math.floor((Math.random() * 10000000000) + 1);
                              this.dialogService.openTalkSdsOpen(
                                this.host, this.port, this.dialogModel.id, this.dialogModel.modelName, this.sessionKey).subscribe(oRes => {
                                // console.log('openTalkSdsOpen', oRes);
                                if (oRes && oRes.statusCode === 200) {
                                  if (oRes.result.hasOwnProperty('error')) {
                                    this.replaceLastChatMessage();
                                    const ref = this.dialog.open(PopupErrorComponent, {
                                      minWidth: '45%',
                                      disableClose: true,
                                    });
                                    ref.componentInstance.title = '에러';
                                    ref.componentInstance.message = oRes.result.error;
                                  } else {
                                    if (oRes.result.resultInfo.sdsResponse.success) {
                                      this.isConversationOpened = true;
                                      if (oRes.result.resultInfo.sdsResponse.hasOwnProperty('currentTask')) {
                                        this.currentTask = oRes.result.resultInfo.sdsResponse.currentTask;
                                        this.taskDiagramFocusByKey(oRes.result.resultInfo.sdsResponse.currentTask);
                                      }
                                      if (oRes.result.resultInfo.sdsResponse.hasOwnProperty('response')) {
                                        this.replaceLastChatMessage();
                                        this.testMessages.push(_.cloneDeep({
                                          type: Type.SERVER,
                                          msgType: MessageType.TEXT,
                                          message: oRes.result.resultInfo.sdsResponse.response
                                        }));
                                      }
                                    }
                                  }
                                }
                              }, error => {
                                this.replaceLastChatMessage();
                                this.errorMessage(error);
                              });
                            }
                          }
                        }, error => {
                          this.replaceLastChatMessage();
                          this.errorMessage(error);
                        });
                      }
                    }
                  }, error => {
                    this.replaceLastChatMessage();
                    this.errorMessage(error);
                  });
                }
              }
            }, error => {
              this.replaceLastChatMessage();
              this.errorMessage(error);
            });
          }
        }
      }, error => {
        this.replaceLastChatMessage();
        this.errorMessage(error);
      });
    } else {
      this.replaceLastChatMessage();
      this.testMessages.push(_.cloneDeep({
        type: Type.SERVER,
        msgType: MessageType.TEXT,
        message: 'Training 된 모델이 아닙니다.'
      }));
      this.dialogTestChatbot.chattingScroll();
    }
  }

  closeConversation() {
    if (this.isConversationOpened) {
      this.dialogService.closeTalkSdsClose(
        this.host, this.port, this.dialogModel.id, this.dialogModel.modelName, this.sessionKey).subscribe(cRes => {
        if (cRes && cRes.statusCode === 200) {
          this.isConversationOpened = false;
          this.currentTask = undefined;
          this.host = undefined;
          this.port = undefined;
          this.sessionKey = undefined;
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  stopTraining() {
    const trainId = localStorage.getItem('trainId');
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          if (response.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = response.data.error;
          } else {
            this.isTraining = true;
            this.value = 0;
            localStorage.removeItem('trainId');
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Train Status', detail: 'CANCELED'});
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  willSendMessage(msg: string) {
    this.testMessages.push(_.cloneDeep({
      type: Type.CLIENT,
      msgType: MessageType.TEXT,
      message: msg
    }));
    this.testMessages.push(_.cloneDeep({
      type: Type.SERVER,
      msgType: MessageType.TEXT,
      message: ''
    }));
    this.dialogTestChatbot.chattingScroll();

    this.dialogService.sendTalkSdsDialog(
      this.host, this.port, this.dialogModel.id, this.dialogModel.modelName, this.sessionKey, msg).subscribe(dRes => {
      if (dRes && dRes.statusCode === 200) {
        if (dRes.result.hasOwnProperty('error')) {
          this.replaceLastChatMessage();
          this.testMessages.push(_.cloneDeep({
            type: Type.SERVER,
            msgType: MessageType.TEXT,
            message: dRes.result.error
          }));
          this.dialogTestChatbot.chattingScroll();
        } else {
          if (dRes.result.resultInfo.success) {
            if (dRes.result.resultInfo.hasOwnProperty('response')) {
              this.replaceLastChatMessage();
              this.testMessages.push(_.cloneDeep({
                type: Type.SERVER,
                msgType: MessageType.TEXT,
                message: dRes.result.resultInfo.response
              }));
              this.dialogTestChatbot.chattingScroll();
            }
            if (dRes.result.resultInfo.finished) {
              this.closeConversation();
            }
          }
        }
      }
    }, error => {
      this.errorMessage(error);
    }, () => {
      console.log('complete');
      this.getTalkErrorLog();
    });
  }

  moveTask() {
    this.router.navigate(['mlt', 'sds', 'task']).catch();
  }

  moveEntity() {
    this.router.navigate(['mlt', 'sds', 'entities']).catch();
  }

  moveIntent() {
    this.router.navigate(['mlt', 'sds', 'intent']).catch();
  }

  moveLog() {
    this.isLogOn = !this.isLogOn;
    if (this.isLogOn) {
      $('.info').css('display', 'none');
      $('.content-etc-log').css('display', 'inline-block');
      this.getTalkErrorLog();
    } else {
      $('.info').css('display', 'inline-block');
      $('.content-etc-log').css('display', 'none');
      this.selectAllTask();
    }
  }

  getTalkErrorLog() {
    this.dialogService.getTalkErrorLog().subscribe(res => {
      if (res.status === 200) {
        this.errorLog = res.text().replace(/\n/g, '<br/>');
      }
    }, error => {
      this.errorMessage(error);
    }, () => {
      if (this.isLogOn) {
        setTimeout(() => {
          this.logContainer.nativeElement.scrollTop = this.logContainer.nativeElement.scrollHeight;
        }, 0);
      }
    });
  }

  downloadLog() {
    const BOM = '%EF%BB%BF';
    const regex = /<br\s*[\/]?>/gi;
    const dataUri = 'data:text/plain;charset=UTF-8,' + BOM + encodeURIComponent(this.errorLog.replace(regex, '\n'));

    const date = new Date();
    const year = date.getFullYear();
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();

    if (month.length === 1) {
      month = '0' + month;
    }
    if (day.length === 1) {
      day = '0' + day;
    }

    const exportFileDefaultName = 'ErrorLog_' + year + month + day + '.log';

    const linkElement = document.createElement('a');
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName);
    linkElement.click();
  }

  public createLinkInfoFromDialogTasks() {
    this.dialogTasksLinkInfo = [];
    if (this.tasks.length > 0) {
      for (const task of this.tasks) {
        if (task.nextTask && task.nextTask.length > 0) {
          task.nextTask.forEach(next => {
            if (next.id) {
              const link = {
                id: '',
                from: '',
                to: '',
                condition: '',
                controller: '',
                fromId: '',
                toId: ''
              };
              link.id = next.id;
              link.condition = next.nextTaskCondition;
              link.controller = next.controller;
              link.from = task.taskName;
              link.to = this.tasks.find(x => x.id === next.nextTaskId).taskName;
              link.fromId = next.taskId;
              link.toId = next.nextTaskId;
              this.dialogTasksLinkInfo.push(link);
            }
          });
        }
      }
    }
  }

  replaceLastChatMessage() {
    if (this.testMessages.length > 0) {
      this.testMessages.splice(this.testMessages.length - 1, 1);
    }
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }
}
