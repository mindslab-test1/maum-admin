import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AgGridModule} from 'ag-grid-angular';
import {
  CheckboxModule,
  ContextMenuModule,
  DataTableModule,
  GrowlModule,
  TreeTableModule
} from 'primeng/primeng';

import {DialogModelRoutingModule} from './dialog-model-routing.module';

import {DialogModelsComponent} from './models/components/dialog-models.component';
import {DialogModelsNewComponent} from './models/components/dialog-models-new.component';
import {DialogModelsEditComponent} from './models/components/dialog-models-edit.component';
import {DialogService} from './service/dialog.service';
import {UtilService} from './service/util.service';
import {CommonModalService} from 'app/modules/dialog-model/service';
import {EmitterService} from './service/emitter.service';

import {DialogIntentSlotMappedUserSayPipe} from './_provide/dialog.user-intent.slot-mapped-user-say.pipe';
import {
  DialogTaskEventTreeNodePipe,
  DialogTaskStartEventTreeNodePipe,
  EntityTreeNodePipe,
} from 'app/modules/dialog-model/_provide';
import {DialogScriptPipe} from './_provide/dialog.script.pipe';

import {TaskComponent} from './task/components/task.component';
import {IntentComponent} from './intent/components/intent.component';
import {EntitiesComponent} from './entities/components/entities.component';
import {ChatBotComponent} from './chatbot/components/chatbot.component';

import {MentionModule} from 'angular-mentions/mention';
import {ProgressBarModule} from 'primeng/components/progressbar/progressbar';

import {SharedModuleSDS} from './shared/shared.module';
import {ChattingMetaComponent} from './chatbot/components/chatting.meta.component';
import {ChattingButtonComponent} from './chatbot/components/chatting.button.component';
import {ChattingClientComponent} from './chatbot/components/chatting.client.component';
import {ChattingDefaultComponent} from './chatbot/components/chatting.default.component';
import {CommonModalComponent} from './common/common-modal.component';
import {TestComponent} from './test/components/test.component';
import {ScriptGuideComponent} from './docs/components/script.guide.component';
import {jsPlumbToolkitModule} from 'jsplumbtoolkit-angular';
import {Task2Component} from './task2/components/task2.component';
import {Controls2Component} from './task2/components/controls.component';
import {
  EndNodeComponent2,
  StartNodeComponent2,
  TaskNodeComponent2
} from './task2/components/components';

@NgModule({
  imports: [
    AgGridModule.withComponents([]),
    CommonModule,
    FormsModule,
    HttpClientModule,
    // SharedModule,
    SharedModuleSDS,
    DialogModelRoutingModule,
    TreeTableModule,
    DataTableModule,
    CheckboxModule,
    ContextMenuModule,
    GrowlModule,
    ProgressBarModule,
    MentionModule,
    jsPlumbToolkitModule
  ],
  providers: [
    DialogService,
    UtilService,
    CommonModalService,
    DialogIntentSlotMappedUserSayPipe,
    DialogTaskStartEventTreeNodePipe,
    DialogTaskEventTreeNodePipe,
    EmitterService],
  declarations: [
    DialogModelsComponent,
    DialogModelsNewComponent,
    DialogModelsEditComponent,
    TaskComponent,
    Task2Component,
    IntentComponent,
    EntitiesComponent,
    TestComponent,
    ScriptGuideComponent,
    ChatBotComponent,
    ChattingDefaultComponent,
    ChattingClientComponent,
    ChattingButtonComponent,
    ChattingMetaComponent,
    StartNodeComponent2,
    EndNodeComponent2,
    TaskNodeComponent2,
    DialogTaskStartEventTreeNodePipe,
    DialogTaskEventTreeNodePipe,
    EntityTreeNodePipe,
    DialogIntentSlotMappedUserSayPipe,
    DialogScriptPipe,
    Controls2Component,
    CommonModalComponent],
  exports: [
    DialogModelsComponent,
    DialogModelsNewComponent,
    DialogModelsEditComponent,
    TaskComponent,
    Task2Component,
    IntentComponent,
    EntitiesComponent,
    TestComponent,
    ScriptGuideComponent,
    ChatBotComponent,
    ChattingDefaultComponent,
    ChattingClientComponent,
    ChattingButtonComponent,
    ChattingMetaComponent,
    Controls2Component,
    CommonModalComponent
  ],
  entryComponents: [
    StartNodeComponent2,
    EndNodeComponent2,
    TaskNodeComponent2
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DialogModelModule {
  private static HttpLoaderFactory: any;
}
