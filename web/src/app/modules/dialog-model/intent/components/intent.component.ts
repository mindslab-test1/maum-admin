import {AfterViewChecked, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatTabGroup} from '@angular/material';
import {
  DialogEntity, DialogIntent, DialogModel, DialogService, DialogUtter, EmitterService
} from '../../service';
import {DialogIntentSlotMappedUserSayPipe} from '../../_provide';
import {PopupErrorComponent, PopupTrainComponent} from '../../shared/components/popup';
import * as _ from 'lodash';
import {DataTable, Message} from 'primeng/primeng';
import {DialogUtterData} from '../../service';
import {PopupDeleteComponent} from '../../shared/components/popup';
import * as $ from 'jquery';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';

@Component({
  selector: 'app-intent',
  templateUrl: 'intent.component.html',
  styleUrls: ['./intent.component.scss']
})
export class IntentComponent implements AfterViewChecked, OnDestroy, OnInit {
  dialogModel: DialogModel;
  workspaceId: string;
  modelId: string;
  modelName: string;
  isLoaded: boolean;
  onShared: boolean;
  value: number;
  isTraining: boolean;
  visibleTraining: boolean;
  selectedIntent: DialogIntent;
  selectedUtterIndex: number;
  selectedIndex: number;

  @ViewChild('intentListDataTable')
  intentDataTable: DataTable;

  @ViewChild('sharedIntentListDataTable')
  sharedIntentDataTable: DataTable;

  @ViewChild('tabGroup')
  tabGroup: MatTabGroup;

  @ViewChild('intentUtterDataTable')
  intentUtterDataTable: DataTable;

  entityContextMenu: {
    label: string,
    value: string,
    items: [{
      label: string,
      value: string,
      command: any
    }]
  }[] = [];
  dialogEntityList: DialogEntity[] = [];
  dialogEntityDropdownOptions: {
    label: string,
    value: string
  }[] = [];


  intentList: DialogIntent[] = [];
  sharedIntentList: DialogIntent[] = [];

  searchOptions: {
    label: string,
    value: string
  }[];
  selectedSearchOption: string;
  selectedMappedValue: string;

  @ViewChild('upload')
  upload: any;

  msgs: Message[] = [];
  trainingCheck: any;
  paraphraseList: string[] = [];
  dummyParaphraseList: string[] = ['안녕1', '안녕2', '안녕3', '안녕4'];

  constructor(public dialog: MatDialog,
              private slotMappedUserSayPipe: DialogIntentSlotMappedUserSayPipe,
              private router: Router, private store: Store<any>,
              private emitterService: EmitterService,
              public dialogService: DialogService) {

    this.value = 0;
    this.isTraining = false;
    this.visibleTraining = false;
    this.selectedMappedValue = '';
    this.selectedUtterIndex = -1;
    this.isLoaded = false;
    this.onShared = false;
    this.selectedIndex = 0;

    this.dialogModel = JSON.parse(localStorage.getItem('currentDialogModel')) as DialogModel;
    this.modelId = this.dialogModel.id;
    this.modelName = this.dialogModel.modelName;
    this.workspaceId = localStorage.getItem('currentWorkspaceId');

    this.searchOptions = [
      {
        label: 'Intent',
        value: 'intentName'
      },
      {
        label: 'Related Task',
        value: 'relatedTask'
      }
    ];
    this.selectedSearchOption = 'name';

    this.trainingCheck = setInterval(() => {
      const trainId = localStorage.getItem('trainId');
      this.checkTraining(trainId);
    }, 10000);
  }

  ngOnInit() {
    this.selectIntentAll();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewChecked() {
    $('.ui-menuitem-link.ui-state-active').parents('.ui-menu-parent').children('.ui-menuitem-link').css({
      'background': '#4688E8',
      'opacity': 1
    });
  }

  ngOnDestroy() {
    clearInterval(this.trainingCheck);
    localStorage.removeItem('init_intents');
  }

  checkTraining(trainId) {
    if (trainId !== null && trainId !== undefined) {
      this.visibleTraining = true;
      this.dialogService.getTrainingProgress(trainId).subscribe(result => {
        if (result && result.statusCode === 200) {
          if (result.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = result.data.error;
          } else {
            if (result.data.resultInfo.step === 'SDS_TRAIN_DONE') {
              this.value = 100;
              localStorage.removeItem('trainId');
              this.isTraining = false;
              this.msgs = [];
              this.msgs.push({
                severity: 'info',
                summary: 'Train Status',
                detail: result.data.resultInfo.step
              });
            } else {
              this.isTraining = true;
              if (result.data.resultInfo.value && result.data.resultInfo.maximum) {
                this.value = Math.round(Number(result.data.resultInfo.value) / Number(result.data.resultInfo.maximum) * 100);
              }
            }
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  stopTraining() {
    const trainId = localStorage.getItem('trainId');
    if (trainId && trainId !== null) {
      this.dialogService.stopTraining(trainId).subscribe(response => {
        if (response && response.statusCode === 200) {
          if (response.data.hasOwnProperty('error')) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '45%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = response.data.error;
          } else {
            this.isTraining = true;
            this.value = 0;
            localStorage.removeItem('trainId');
            this.msgs = [];
            this.msgs.push({severity: 'info', summary: 'Train Status', detail: 'CANCELED'});
          }
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  selectIntentAll() {
    this.dialogService.getDialogModelIntentAll(this.workspaceId, this.modelId)
    .subscribe(iResponse => {
      this.intentList = [];
      if (iResponse && iResponse.statusCode === 200) {
        this.intentList = iResponse.data.intent;
        localStorage.setItem('init_intents', JSON.stringify(this.intentList));
        this.selectDialogModelEntity();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  selectSharedIntent() {
    this.dialogService.getDialogModelSharedIntentAll(this.workspaceId, this.modelId)
    .subscribe(iResponse => {
      this.sharedIntentList = [];
      if (iResponse && iResponse.statusCode === 200) {
        this.sharedIntentList = iResponse.data.intent;
        if (this.sharedIntentList.length > 0) {
          for (const intent of this.sharedIntentList) {
            if (this.intentList.find(x => x.id === intent.id)) {
              intent.importedYn = 'Y';
            } else {
              intent.importedYn = 'N';
            }
          }
        }
        this.selectDialogModelEntity();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  selectDialogModelEntity() {
    this.dialogService.getDialogModelEntities(this.workspaceId, this.modelId)
    .subscribe(eResponse => {
      this.dialogEntityList = [];
      if (eResponse && eResponse.statusCode === 200) {
        const entities = eResponse.data.entityInfoList;
        this.dialogEntityList = entities;
        if (entities && entities.length > 0) {
          this.entityContextMenu = [];
          this.dialogEntityDropdownOptions = [];
          for (const entity of entities) {
            const found = this.entityContextMenu.filter((val) => (val.label === entity.className));
            let classNode = null;
            if (found.length > 0) {
              classNode = found[0];
            } else {
              this.entityContextMenu.push({
                label: entity.className,
                value: entity.id,
                items: null
              });
              classNode = this.entityContextMenu[this.entityContextMenu.length - 1];
              classNode.items = [];
            }
            classNode.items.push({
              label: entity.entityName,
              value: entity.id,
              command: this.onClickContextMenu
            });
            this.dialogEntityDropdownOptions.push({
              label: entity.entityName,
              value: entity.id
            });
          }
        }
        this.isLoaded = true;
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  onSelectIntent(intent: DialogIntent) {
    this.selectedIntent = intent;
  }

  addIntent() {
    if (this.checkModelCreator()) {
      const defaultIntent = new DialogIntent();
      defaultIntent.id = this.dialogService.getUUID();
      defaultIntent.workspaceId = this.workspaceId;
      defaultIntent.sharedYn = 'N';
      defaultIntent.creatorId = localStorage.getItem('currentUserId');
      defaultIntent.updaterId = localStorage.getItem('currentUserId');
      defaultIntent.intentUtter = [];
      defaultIntent.intentName = this.checkIntentName('');
      this.dialogService.insertDialogModelIntent(this.modelId, defaultIntent).subscribe(response => {
        if (response && response.statusCode === 200) {
          this.intentList.push(defaultIntent);
          this.selectedIntent = this.intentList[this.intentList.length - 1];
          localStorage.setItem('init_intents', JSON.stringify(this.intentList));
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  onRemoveIntent(intent: DialogIntent, idx: number) {
    if (this.checkModelCreator()) {
      if (intent.sharedYn === 'Y' && this.onShared) {
        const ref = this.dialog.open(PopupDeleteComponent, {
          minWidth: '45%',
          disableClose: true,
        });
        ref.componentInstance.domain = 'Intent';
        ref.componentInstance.message = '이 인텐트는 공유된 인텐트 입니다.<br/>삭제시 참조하는 모델에 영향을 끼칠수 있습니다.';
        this.emitterService.get('Intent').subscribe(result => {
          if (result === 'ok') {
            this.removeIntent(intent, idx);
            this.dialog.closeAll();
          }
        });
      } else {
        this.removeIntent(intent, idx);
      }
    }
  }

  removeIntent(intent: DialogIntent, idx: number) {
    this.dialogService.deleteDialogModelIntent(this.workspaceId, this.modelId, intent).subscribe(response => {
      if (response && response.statusCode === 200) {
        this.intentList.splice(idx, 1);
        this.selectedIntent = null;
        this.selectedUtterIndex = -1;
        this.intentList = this.intentList.concat();
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  addUtter(paraphrase?: string) {
    if (this.checkModelCreator()) {
      if (this.selectedIntent) {
        if (this.selectedIntent.sharedYn === 'Y' && !this.onShared) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '30%',
            disableClose: true,
          });
          ref.componentInstance.title = '안내';
          ref.componentInstance.message = '공유된 인텐트는 수정할 수 없습니다.';
        } else {
          const intentUtter = {
            id: this.dialogService.getUUID(),
            creatorId: localStorage.getItem('currentUserId'),
            mappedUserSay: '',
            userSay: (paraphrase) ? paraphrase : '',
            intentId: this.selectedIntent.id,
            createdAt: '',
            intentUtterData: []
          };

          this.dialogService.insertDialogModelIntentUtter(this.workspaceId, this.modelId, intentUtter).subscribe(res => {
            if (res && res.statusCode === 200) {
              this.intentUtterDataTable.value.push(intentUtter);
              this.selectedIntent.intentUtter.push(intentUtter);
              if (paraphrase) {
                this.paraphraseList = this.paraphraseList.filter(x => x !== paraphrase);
              } else {
                this.selectedUtterIndex = this.selectedIntent.intentUtter.length - 1;
              }
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  onSelectUserSay(utter: DialogUtter, idx: number) {
    this.selectedUtterIndex = idx;
  }

  removeUtter(utter: DialogUtter, idx: number) {
    if (this.checkModelCreator()) {
      if (this.selectedIntent && this.selectedIntent.sharedYn === 'Y' && !this.onShared) {
        const ref = this.dialog.open(PopupErrorComponent, {
          minWidth: '30%',
          disableClose: true,
        });
        ref.componentInstance.title = '안내';
        ref.componentInstance.message = '공유된 인텐트는 수정할 수 없습니다.';
      } else {
        this.dialogService.deleteDialogModelIntentUtter(this.workspaceId, this.modelId, utter).subscribe(response => {
          if (response && response.statusCode === 200) {
            setTimeout(() => {
              this.selectedUtterIndex = -1;
              this.selectedIntent.intentUtter.splice(idx, 1);
              this.selectedIntent.intentUtter = this.selectedIntent.intentUtter.concat();
            }, 0);
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  onClickContextMenu = (event) => {
    const el = document.getElementById('userSayInput');
    this.addUtterMap(event.item.value, el['selectionStart'], el['selectionEnd']);
    el['selectionStart'] = 0;
    el['selectionEnd'] = 0;
  };

  entitySelected(event) {
    const el = document.getElementById('userSayInput');
    if (event.node.parent === undefined) {
      el['selectionStart'] = 0;
      el['selectionEnd'] = 0;
    } else {
      this.addUtterMap(event.node.data.original.id, el['selectionStart'], el['selectionEnd']);
      el['selectionStart'] = 0;
      el['selectionEnd'] = 0;
    }
  }

  onChangeUtterDataEntity(utter) {
    if (this.checkModelCreator()) {
      if (this.selectedIntent.intentUtter[this.selectedUtterIndex]) {
        this.dialogService.updateDialogModelIntentUtterData(
          this.workspaceId, this.modelId, this.selectedIntent.intentUtter[this.selectedUtterIndex].intentId, utter).subscribe(response => {
          if (response && response.statusCode === 200) {
            this.rebuildSlotMappedUserSay();
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  onSelectMappedValue(text) {
    this.selectedMappedValue = text;
    if (this.onShared) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '30%',
        disableClose: true,
      });
      ref.componentInstance.title = '안내';
      ref.componentInstance.message = '이 인텐트는 공유된 인텐트입니다.<br/>수정시 참조하는 모델에 영향을 끼칠수 있습니다.';
    }
  }

  removeMappedValue(event, dataId) {
    event.stopPropagation();
    if (this.checkModelCreator()) {
      if (this.selectedIntent && this.selectedIntent.sharedYn === 'Y' && !this.onShared) {
        const ref = this.dialog.open(PopupErrorComponent, {
          minWidth: '30%',
          disableClose: true,
        });
        ref.componentInstance.title = '안내';
        ref.componentInstance.message = '공유된 인텐트는 수정할 수 없습니다.';
      } else {
        setTimeout(() => {
          const utterData = this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData.find(x => x.id === dataId);
          this.dialogService.deleteDialogModelIntentUtterData(
            this.workspaceId, this.modelId, this.selectedIntent.id, utterData).subscribe(response => {
            if (response && response.statusCode === 200) {
              this.selectedMappedValue = null;
              this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData
                = this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData.filter((val) => (val.id !== dataId));
              this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData
                = this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData.concat();
              this.rebuildSlotMappedUserSay();
            }
          }, error => {
            this.errorMessage(error);
          });
        }, 0);
      }
    }
  }

  addUtterMap(entityId: string, startIdx: number, endIdx: number) {
    if (this.checkModelCreator()) {
      let isConflict = false;
      const userSay = this.selectedIntent.intentUtter[this.selectedUtterIndex].userSay;
      if (this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData) {
        for (const mapped of this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData) {
          const si = userSay.indexOf(mapped.mappedValue);
          const len = mapped.mappedValue.length;
          if ((startIdx >= si) && (startIdx < (si + len))) {
            isConflict = true;
            break;
          }
          if ((endIdx >= si) && (endIdx < (si + len))) {
            isConflict = true;
            break;
          }
        }
      } else {
        this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData = [];
      }

      if (isConflict === true) {
        console.log('isConfilict', isConflict);
      } else {
        const userUtter = this.selectedIntent.intentUtter[this.selectedUtterIndex];
        if (userUtter.userSay.substring(startIdx, endIdx).trim().length === 0) {
          console.log('isWhiteSpace');
        } else {
          const userUtterData = new DialogUtterData();
          userUtterData.id = this.dialogService.getUUID();
          userUtterData.creatorId = localStorage.getItem('currentUserId');
          userUtterData.entityId = entityId;
          userUtterData.intentUtterId = userUtter.id;
          userUtterData.mappedValue = userUtter.userSay.substring(startIdx, endIdx);
          this.dialogService.insertDialogModelIntentUtterData(
            this.workspaceId, this.modelId, this.selectedIntent.id, userUtterData).subscribe(response => {
            if (response && response.statusCode === 200) {
              userUtter.intentUtterData.push(userUtterData);
              const sorted = userUtter.intentUtterData.sort((a, b) => {
                const aIdx = userUtter.userSay.indexOf(a.mappedValue);
                const bIdx = userUtter.userSay.indexOf(b.mappedValue);
                return aIdx < bIdx ? -1 : aIdx > bIdx ? 1 : 0;
              });
              this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData = [].concat(sorted);
              this.rebuildSlotMappedUserSay();
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  private rebuildSlotMappedUserSay() {
    const mapInfos = this.slotMappedUserSayPipe.transform(
      this.selectedIntent.intentUtter[this.selectedUtterIndex].intentUtterData,
      this.selectedIntent.intentUtter[this.selectedUtterIndex].userSay);
    let mappedString = '';
    for (const info of mapInfos) {
      if (info.isCorpus === true) {
        mappedString = mappedString + `<${info.slot}=${info.text}>`;
      } else {
        mappedString = mappedString + info.text;
      }
    }
    let originalmMppedString = this.selectedIntent.intentUtter[this.selectedUtterIndex].mappedUserSay;
    this.selectedIntent.intentUtter[this.selectedUtterIndex].mappedUserSay = '' + mappedString;
    this.dialogService.updateDialogModelIntentUtter(
      this.workspaceId, this.modelId, this.selectedIntent.intentUtter[this.selectedUtterIndex]).subscribe(response => {
      if (response && response.statusCode === 200) {
        originalmMppedString = '';
      } else {
        this.selectedIntent.intentUtter[this.selectedUtterIndex].mappedUserSay = originalmMppedString;
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  public trainOpen() {
    if (this.checkModelCreator()) {
      this.dialog.open(PopupTrainComponent, {
        maxHeight: '50%',
        width: '40%',
        disableClose: true,
      });
      this.emitterService.get('training').subscribe(result => {
        if (result === 'ok') {
          const trainId = localStorage.getItem('trainId');
          this.checkTraining(trainId);
          this.dialog.closeAll();
        }
      });
    }
  }

  public saveIntent() {
    if (this.isLoaded) {
      if (this.checkModelCreator()) {
        const saveIntent: DialogIntent = {
          id: this.selectedIntent.id,
          intentName: this.checkIntentName(this.selectedIntent.intentName),
          sharedYn: this.selectedIntent.sharedYn,
          creatorId: this.selectedIntent.creatorId,
          updaterId: this.selectedIntent.updaterId,
          intentUtter: this.selectedIntent.intentUtter,
          workspaceId: this.selectedIntent.workspaceId,
          importedYn: this.selectedIntent.importedYn
        };

        this.dialogService.updateDialogModelIntent(this.modelId, saveIntent).subscribe(response => {
          if (response && response.statusCode === 200) {
            const idx = this.intentList.findIndex(x => x.id === saveIntent.id);
            this.intentList[idx].intentName = saveIntent.intentName;
            this.intentList[idx].sharedYn = saveIntent.sharedYn;
            this.intentList[idx].intentUtter = saveIntent.intentUtter;
            localStorage.setItem('init_intents', JSON.stringify(this.intentList));
          }
        }, error => {
          const initIntents = JSON.parse(localStorage.getItem('init_intents')) as DialogIntent[];
          const initIntent = initIntents.find(x => x.id === this.selectedIntent.id);
          if (this.selectedIntent.intentName !== initIntent.intentName) {
            this.selectedIntent.intentName = initIntent.intentName;
          }

          this.errorMessage(error);
        });
      }
    }
  }

  saveUtter() {
    if (this.checkModelCreator()) {
      if (this.selectedIntent) {
        if (this.selectedIntent.sharedYn === 'Y' && !this.onShared) {
          const ref = this.dialog.open(PopupErrorComponent, {
            minWidth: '30%',
            disableClose: true,
          });
          ref.componentInstance.title = '안내';
          ref.componentInstance.message = '공유된 인텐트는 수정할 수 없습니다.';
        } else {
          if (this.selectedIntent.intentUtter[this.selectedUtterIndex]) {
            this.dialogService.updateDialogModelIntentUtter(
              this.workspaceId, this.modelId, this.selectedIntent.intentUtter[this.selectedUtterIndex]).subscribe(response => {
              if (response && response.statusCode === 200) {

              }
            }, error => {
              this.errorMessage(error);
            });
            // if (this.selectedIntent.intentUtter[this.selectedUtterIndex].createdAt) {
            //   this.dialogService.updateDialogModelIntentUtter(
            //     this.workspaceId, this.modelId, this.selectedIntent.intentUtter[this.selectedUtterIndex]).subscribe(response => {
            //     if (response && response.statusCode === 200) {
            //
            //     }
            //   }, error => {
            //     this.errorMessage(error);
            //   });
            // } else {
            //   this.dialogService.insertDialogModelIntentUtter(
            //     this.workspaceId, this.modelId, this.selectedIntent.intentUtter[this.selectedUtterIndex]).subscribe(response => {
            //     if (response && response.statusCode === 200) {
            //       this.selectedIntent.intentUtter[this.selectedUtterIndex].createdAt = new Date().toString();
            //     }
            //   }, error => {
            //     this.errorMessage(error);
            //   });
            // }
          }
        }
      }
    }
  }

  public paraphraseOpen() {
    if (this.checkModelCreator()) {
      this.paraphraseList = this.dummyParaphraseList;
      // TODO paraphrase API
      $('.paraphrase-list-wrap').css('display', 'block');
      // $(event.currentTarget).css({
      //   'background': '#4688E8',
      //   'color': '#FFF',
      //   'border-radius': '50%'
      // }).parents('.users-says-input').css({
      //   'border-color': '#4688E8',
      //   'color': '#4688E8'
      // });
    }
  }

  public addParaphrase(paraphrase: string, event) {
    if (event) {
      this.addUtter(paraphrase);
    }
  }

  public onShareIntent(intent: DialogIntent, idx: number) {
    if (intent.sharedYn === 'N') {
      this.dialogService.shareDialogModelIntent(this.modelId, intent).subscribe(response => {
        if (response && response.statusCode === 200) {
          this.intentDataTable.value[idx].sharedYn = 'Y';
        }
      }, error => {
        this.errorMessage(error);
      });
    }
  }

  importIntent(intent: DialogIntent, idx: number) {
    if (this.checkModelCreator()) {
      if (this.onShared) {
        if (this.intentList.filter(x => x.id === intent.id).length === 0) {
          this.dialogService.importDialogSharedIntent(this.workspaceId, this.modelId, intent).subscribe(response => {
            if (response && response.statusCode === 200) {
              this.sharedIntentDataTable.value.find(x => x.id === intent.id).importedYn = 'Y';
              this.intentDataTable.value.push(intent);
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  linkIntent(intent: DialogIntent, idx: number) {
    if (this.checkModelCreator()) {
      if (this.onShared) {
        if (this.intentList.filter(x => x.id === intent.id).length === 0) {
          this.dialogService.linkDialogSharedIntent(this.workspaceId, this.modelId, intent).subscribe(response => {
            if (response && response.statusCode === 200) {
              this.intentDataTable.value.push(intent);
              this.msgs = [];
              this.msgs.push({
                severity: 'info',
                summary: 'Link Intent',
                detail: response.statusMessage
              });
            }
          }, error => {
            this.errorMessage(error);
          });
        }
      }
    }
  }

  public openTrainingDialog() {
    // this.commonModalService.openDialogModelTrainingModal(this.modelId, this.model.modelName);
  }

  downloadIntent() {
    this.dialogService.downloadDialogIntent(this.workspaceId, this.modelId).subscribe(res => {
      if (res && res.status === 200) {
        if (res.headers.get('content-type') === 'text/tsv;charset=UTF-8') {
          const BOM = '%EF%BB%BF';
          const dataUri = 'data:text/tsv;charset=UTF-8,' + BOM + encodeURIComponent(res.text());

          const date = new Date();
          const year = date.getFullYear();
          let month = (date.getMonth() + 1).toString();
          let day = date.getDate().toString();

          if (month.length === 1) {
            month = '0' + month;
          }
          if (day.length === 1) {
            day = '0' + day;
          }

          const exportFileDefaultName = this.modelName + '_' + 'intents_' + year + month + day + '.tsv';

          const linkElement = document.createElement('a');
          linkElement.setAttribute('href', dataUri);
          linkElement.setAttribute('download', exportFileDefaultName);
          linkElement.click();
        }
      } else {
        console.log(res);
      }
    }, error => {
      this.errorMessage(error);
    });
  }

  uploadIntents(event: any) {
    if (this.checkModelCreator()) {
      if (event.target.files.length > 0) {
        const file: File = event.target.files[0];
        const formData: FormData = new FormData();
        formData.append('sourceFile ', file, file.name);
        this.dialogService.uploadDialogIntent(this.workspaceId, this.modelId, formData).subscribe(res => {
          if (res && res.statusCode === 200) {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '30%',
              disableClose: true,
            });
            ref.componentInstance.title = '업로드 완료';
            let message = 'Total Count : ' + res.data.totalRowCount + '<br/>';
            message += '  Success : ' + res.data.successCount + '<br/>';
            message += '  Fail : ' + res.data.failCount + '<br/>';
            if (Number(res.data.failCount) > 0) {
              message += 'Failed Data' + '<br/>';
              if (res.data.failDataList.length > 0) {
                for (const failData of res.data.failDataList) {
                  message += ' - ' + failData + '<br/>';
                }
              }
            }
            ref.componentInstance.message = message;
          } else {
            const ref = this.dialog.open(PopupErrorComponent, {
              minWidth: '30%',
              disableClose: true,
            });
            ref.componentInstance.title = '에러';
            ref.componentInstance.message = res.statusMessage;
          }
        }, error => {
          this.errorMessage(error);
        });
      }
    }
  }

  backToList() {
    this.selectedIntent = null;
    this.selectedUtterIndex = -1;
    setTimeout(() => {
      if (!this.onShared) {
        this.tabGroup.selectedIndex = 0;
      } else {
        this.tabGroup.selectedIndex = 1;
      }
    }, 0);
  }

  onToggleIntentTabs(event) {
    if (event.index === 0) {
      this.selectedIndex = event.index;
      this.onShared = false;
      this.selectIntentAll();
    } else {
      this.selectedIndex = event.index;
      this.onShared = true;
      this.selectSharedIntent();
    }
  }

  checkModelCreator() {
    if (this.dialogModel.creatorId !== localStorage.getItem('currentUserId')) {
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '경고';
      ref.componentInstance.message = '다른 사용자가 작성한 모델은 수정할 수 없습니다.';
      return false;
    } else {
      return true;
    }
  }

  checkIntentName(intentName: string) {
    if (intentName === '') {
      intentName = 'intent_' + new Date().getTime();
    }
    if (this.intentList.filter(x => x.intentName === intentName).length > 1) {
      intentName = intentName + '_' + new Date().getTime();
    }
    return intentName;
  }

  errorMessage(error) {
    const errorJson = error.json();
    if (errorJson.statusCode === 400) {
      this.dialog.closeAll();
      const ref = this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
      ref.componentInstance.title = '에러';
      ref.componentInstance.message = errorJson.statusMessage;
    } else {
      this.dialog.closeAll();
      this.dialog.open(PopupErrorComponent, {
        minWidth: '45%',
        disableClose: true,
      });
    }
  }
}
