import {Component, Input} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-chatting-button',
  templateUrl: './chatting.button.component.html',
  styleUrls: ['./chatting.button.component.scss']
})

export class ChattingButtonComponent {
  @Input() message = '';

  constructor() {
  }

  public sendButtonMessage($event) {
  }

  private chattingScroll() {
    setTimeout(() => {
      const chatDiv = $('.chatbot-contents-wrap');
      chatDiv.scrollTop(chatDiv.prop('scrollHeight'));
    }, 0);
  }
}
