import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-chatting-meta',
  templateUrl: './chatting.meta.component.html',
  styleUrls: ['./chatting.meta.component.scss']
})

export class ChattingMetaComponent {
  @Input() message = '';

  constructor() {
  }
}
