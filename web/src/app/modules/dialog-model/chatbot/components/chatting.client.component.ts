import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-chatting-client',
  templateUrl: './chatting.client.component.html',
  styleUrls: ['./chatting.default.component.scss']
})

export class ChattingClientComponent {

  @Input() message: string;

  constructor() {
  }
}
