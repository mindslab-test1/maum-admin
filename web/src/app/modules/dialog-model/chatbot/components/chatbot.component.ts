import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ChatMessage, Type, MessageType} from '../chatbot.models';
import * as $ from 'jquery';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})

export class ChatBotComponent {
  public footer_input = '';
  @Input() messageQueue: ChatMessage[] = [];
  @Output() sendMessageHandler = new EventEmitter<string>();
  @Input() isConversationOpened: boolean;
  public Type = Type;
  public MessageType = MessageType;

  constructor() {
    this.isConversationOpened = false;
    this.chattingScroll();
  }

  public resize(event) {
    if (event === null || event.keyCode === 13) {
      this.sendMessage();
    } else {
      const chatbotContent = $('.chatbot-contents-wrap');
      const browser = navigator.userAgent.toLowerCase();
      $(event.currentTarget).height(1).height($(event.currentTarget).prop('scrollHeight') + 2);
      if ((navigator.appName === 'Netscape' && navigator.userAgent.search('Trident') !== -1) || (browser.indexOf('msie') !== -1)) {
        $('textarea.chatting-footer-input:empty').css('height', '18px');
      }
    }
  }

  public sendMessage() {
    if (this.isConversationOpened) {
      if (this.sendMessageHandler !== null && this.footer_input !== '' && this.footer_input !== '\n') {
        if (this.messageQueue.length === 0 || this.messageQueue[this.messageQueue.length - 1].message !== '') {
          this.sendMessageHandler.emit(this.footer_input);
        }
      }
    }
    this.footer_input = '';
    $('textarea.chatting-footer-input:empty').css('height', '18px');
  }

  public chattingScroll() {
    setTimeout(() => {
      const chatDiv = $('.chatbot-contents-wrap');
      chatDiv.scrollTop(chatDiv.prop('scrollHeight'));
    }, 0);
  }
}
