import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-chatting-default',
  templateUrl: './chatting.default.component.html',
  styleUrls: ['./chatting.default.component.scss']
})

export class ChattingDefaultComponent {

  @Input() message = '';

  constructor() {
  }
}
