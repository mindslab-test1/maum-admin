export interface ChatMessage {
  type: string;
  msgType: string;
  message: string;
}

export const Type = {
  SERVER: 'server',
  CLIENT: 'client'
};

export const MessageType = {
  TEXT: 'text',
  IMAGE: 'image',
  VIDEO: 'video',
  BUTTON: 'button',
  META: 'meta'
};

export interface CreateSession {
  id: string;
  chatbot: string;
  user_key: string;
  accessFrom: string;
  contexts: {
    fields: any
  };
  start_time: string;
  duration: {
    seconds: string;
    nanos: number;
  };
  human_assisted: boolean;
  intent_finder: string;
}

export interface ChatBotResponse {
  at: string;
  error: boolean;
  meta: any;
  receive: boolean;
  utter: string;
}

export interface ChatBotInfo {
  name: string;
  title: string;
  description: string;
  version: string;
  howto: string;
  output: string;
  input: string;
  skill: {
    name: string;
    lang: string;
    description: string;
    version: string;
    skill: string;
    properties: any;
    key: string;
  }[];
  detail: {
    auth: boolean;
    auth_provider: string;
    greeting: string;
    advertising: string;
    icons: any;
    bg_color: string;
    bg_image: string;
    tags: any[];
    categories: any[];
    created_at: string;
    modified_at: string;
    vendor: string;
  };
}
