import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SimpleClassifierRoutingModule} from './simple-classifier-routing.module';
import {SimpleClassifiersComponent} from './components/simple-classifiers/simple-classifiers.component';
import {SimpleClassifierUpsertComponent} from './components/simple-classifier-upsert/simple-classifier-upsert.component';
import {SimpleClassifierDetailComponent} from './components/simple-classifier-detail/simple-classifier-detail.component';
import {SimpleClassifierPanelComponent} from './components/simple-classifier-panel/simple-classifier-panel.component';
import {SharedModule} from '../../shared/shared.module';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';

@NgModule({
  declarations: [
    SimpleClassifiersComponent,
    SimpleClassifierUpsertComponent,
    SimpleClassifierPanelComponent,
    SimpleClassifierDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    SimpleClassifierRoutingModule,
    MatInputModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatIconModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatCardModule,
    MatExpansionModule,
    MatTabsModule,
    MatButtonModule,
    MatCheckboxModule,

  ]
})
export class SimpleClassifierModule {
}
