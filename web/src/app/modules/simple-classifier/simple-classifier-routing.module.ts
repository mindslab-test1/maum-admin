import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SimpleClassifiersComponent} from './components/simple-classifiers/simple-classifiers.component';
import {SimpleClassifierUpsertComponent} from './components/simple-classifier-upsert/simple-classifier-upsert.component';
import {SimpleClassifierDetailComponent} from './components/simple-classifier-detail/simple-classifier-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: SimpleClassifiersComponent,
      },
      {
        path: 'new',
        component: SimpleClassifierUpsertComponent
      },
      {
        path: ':id',
        component: SimpleClassifierDetailComponent
      },
      {
        path: ':id/edit',
        component: SimpleClassifierUpsertComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimpleClassifierRoutingModule {
}
