import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SimpleClassifiersComponent} from './simple-classifiers.component';

describe('SimpleClassifiersComponent', () => {
  let component: SimpleClassifiersComponent;
  let fixture: ComponentFixture<SimpleClassifiersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimpleClassifiersComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleClassifiersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
