import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';

import {Subscription} from 'rxjs';
import {ROUTER_LOADED, SC_DELETE} from '../../../../core/actions';
import {AuthService} from '../../../../core';
import {
  AppObjectManagerService,
  AlertComponent,
  AppEnumsComponent,
  getErrorString
} from 'app/shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk/services/custom';
import {ConfirmComponent} from 'app/shared';

@Component({
  selector: 'app-simple-classifier-detail',
  templateUrl: './simple-classifier-detail.component.html',
  styleUrls: ['./simple-classifier-detail.component.scss'],
})

export class SimpleClassifierDetailComponent implements OnInit {
  sc: any;
  title: any;
  actions: any;
  subscription = new Subscription();
  scName: any;

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    this.sc = this.objectManager.get('sc');

    let promise = new Promise((resolve, reject) => {
      if (this.sc === undefined) {
        // objectManager로 넘어온 sc 객체가 없는 경우
        this.route.params.subscribe(par => {
          this.scName = par['id'];
        });
        this.getSimpleClassifierInfo().then(() => resolve()).catch(() => reject());
      } else {
        // sc view에서 detail로 정상접근한 경우
        resolve();
      }
    });
    promise.catch(() => {
      let message = `Cannot find [${this.scName}] this scName`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.backPage();
      return;
    });
    promise.then(() => {
      let id = 'sc';
      let roles = this.consoleUserApi.getCachedCurrent().roles;

      this.actions = {
        edit: {
          icon: 'edit',
          text: 'Edit',
          callback: this.edit,
          hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
        },
        delete: {
          icon: 'delete',
          text: 'Delete',
          callback: this.delete,
          hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
        }
      };

      this.title = this.sc.name + ':' + this.appEnum.langCode[this.sc.lang];
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  edit: any = () => {
    // #@ sc 수정화면으로 이동합니다
    this.router.navigate(['edit'], {relativeTo: this.route});
  };

  delete: any = () => {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${this.sc.name}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      this.grpc.deleteSimpleClassifier([{name: this.sc.name}]).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.snackBar.open(`Simple Classifier '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
          this.backPage();
        },
        err => {
          let message = `Failed to Delete Simple Classifier. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  backPage: any = () => {
    this.objectManager.clean('sc');
    // this.router.navigate(['../sc'], {relativeTo: this.route});
    // #@ 이전페이지로 이동합니다
    this.router.navigate(['../'], {relativeTo: this.route});
  };

  getSimpleClassifierInfo() {
    return new Promise((resolve, reject) => {
      this.grpc.getSimpleClassifierInfo(this.scName).subscribe(res => {
        if (res) {
          this.sc = res;
          this.objectManager.set('sc', this.sc);
          if (this.sc.name === '') {  // 조회 결과가 비정상인 경우
            reject();
          }
          resolve();
        } else {
          this.backPage();
          reject();
          return;
        }
      }, err => {
        let message = 'Fail to get sc info';
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        reject();
      });
    });
  }
}

