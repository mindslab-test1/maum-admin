import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SimpleClassifierDetailComponent} from './simple-classifer-detail.component';

describe('SimpleClassiferDetailComponent', () => {
  let component: SimpleClassifierDetailComponent;
  let fixture: ComponentFixture<SimpleClassifierDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimpleClassifierDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleClassifierDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
