import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED} from '../../../../core/actions';
import {
  AlertComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  FormErrorStateMatcher,
  getErrorString,
  ErrorDialogComponent
} from '../../../../shared';
import {GrpcApi} from '../../../../core/sdk';
import {forkJoin} from 'rxjs';
import {DragulaService} from 'ng2-dragula';
import {ConfirmComponent} from 'app/shared';

// type matchType = {
//   words: string[], range: number[], skill: string, regex: string
// }

export interface MatchType {
  words: string[];
  range: number[];
  skill: string;
  regex: string;
}

@Component({
  selector: 'app-simple-classifier-upsert',
  templateUrl: './simple-classifier-upsert.component.html',
  styleUrls: ['./simple-classifier-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class SimpleClassifierUpsertComponent implements OnInit {
  dialogTitle: string;
  role: any;
  sc = {
    name: undefined,
    lang: undefined,
    description: undefined,
    skill_cnt: undefined,
    reg_cnt: undefined,
    skill_list: [],
    pos_tagging: false,
    named_entity: false,

  };
  scList = [];
  org_sc: any;

  selectedSkill: any;
  newSkill: any;
  newRegExp: any;
  skill_description: any;
  submitButton: any;

  nluSentence: string;
  posResult: string;
  nerResult: string;
  posNerResult: string;
  scName: any;

  oriSentList: string[] = [];
  sentences: string[] = [];
  matchSet: MatchType[] = [];

  nameFormControl = new FormControl('', [
    Validators.required,
    () => !!this.sc.name && this.scList.find(sc => sc === this.sc.name)
      ? {'duplicated': true} : null
  ]);

  skillFormControl = new FormControl(undefined, [
    Validators.required
  ]);

  regExFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private store: Store<any>,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              public appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private dragulaService: DragulaService,
              private cdr: ChangeDetectorRef,
              private grpc: GrpcApi) {
    const bag: any = this.dragulaService.find('evented-bag');
    if (bag !== undefined) {
      this.dragulaService.destroy('evented-bag');
      this.dragulaService.setOptions('evented-bag', {revertOnSpill: true});
    }
  }

  ngOnInit() {
    // 1.objectManager에서 sc정보를 조회합니다
    let tesmpSc = this.objectManager.get('sc');

    // /m2u/dialog-service/sc/default-sc/edit 로 접근할수 있는 방법은 두가지 입니다
    // 1. sc view에서 -> sc edit로 버튼을 클릭해서 이동한 경우
    // 2. 사용자가 직접 url에/m2u/dialog-service/sc/default-sc/edit를 입력하여 들어온 경우
    // 2 번의 경우 objectManager에 sc이 없습니다
    // 이 경우 sc을 획득하기 위한 예외처리는 아래와 같습니다
    let promise = new Promise((resolve, reject) => {
      this.route.params.subscribe(par => {
        let tempRoute = this.route.toString();
        if (tempRoute.indexOf('new') > 0) {
          console.log('#@ new :' + tempRoute);
          this.role = 'add';
        } else if (tempRoute.indexOf('edit')) {
          console.log('#@ edit :' + tempRoute);
          this.role = 'edit';
          this.scName = par['id'];
        } else {  // 잘못된 url 접근
          console.log('#@ There is no sc url');
          let message = 'Cannot find this sc url.';
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.backPage();
          reject();
        }
      });
      if (this.role === 'edit' && tesmpSc === undefined) {
        // 사용자가 직접 버튼을 클릭하여 넘어왔을 경우
        // 사용자가 url로 edit를 접근했을 경우 grpc 호출을 통해 dam 정보를 받아온다
        this.getSimpleClassifierInfo().then(() => resolve()).catch(() => reject());
      } else {
        resolve();  // resolve를 오출하여 promise를 완료한다
      }
    });
    promise.catch(() => {
      // 정상적으로 데이터를 갖고 오지 못한경우
      let message = `Cannot find [${this.scName}] this sc`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.backPage();
      return;
    });
    promise.then(() => {
      switch (this.role) {
        case 'add':
          this.submitButton = true;
          if (tesmpSc !== undefined) {
            this.sc = tesmpSc;
            this.check();
          }
          this.dialogTitle = 'Add Simple Classifier';
          this.sc.lang = this.appEnum.langCodeKeys[0];
          break;
        case 'edit':
          let sc = this.objectManager.get('sc');
          if (sc === undefined) {
            this.backPage();
            return;
          }
          delete sc.skill_cnt;
          delete sc.reg_cnt;
          delete sc.checked;
          this.sc = sc;
          this.org_sc = JSON.parse(JSON.stringify(this.sc));
          this.dialogTitle = 'Edit Simple Classifier';
          break;
        default:
          this.backPage();
          // let message = 'Role is not properly assigned.';
          // this.snackBar.open(message, 'Confirm', {duration: 10000});
          return;
      }
      this.grpc.getSimpleClassifierAllList().subscribe(
        scs => {
          this.scList = [];
          scs.sc_list.map(tempSc => this.scList.push(tempSc.name));
          this.store.dispatch({type: ROUTER_LOADED});
        },
        err => {
          let message = `Something went wrong. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
          this.store.dispatch({type: ROUTER_LOADED});
        });
    });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    if (this.check()) {
      delete this.sc.skill_cnt;
      delete this.sc.reg_cnt;
      if (this.role === 'edit') {
        if (!this.isModified()) {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Not Changed.';
          ref.componentInstance.message = `Values are not changed. Please make some modification(s) first.`;
        } else {
          this.openDialog('edit');
        }
      } else {
        this.openDialog('add');
      }
    }
  }

  isModified() {
    return JSON.stringify(this.sc) !== JSON.stringify(this.org_sc);
  }

  onAdd(): void {
    this.grpc.insertSimpleClassifierInfo(this.sc).subscribe(
      res => {
        this.snackBar.open(`Simple Classifier '${this.sc.name}' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed.';
        ref.componentInstance.message = `Failed to create a Simple Classifier. [${getErrorString(err)})]`;
      }
    );
  }

  onEdit(): void {
    delete this.sc['isChecked'];
    this.grpc.updateSimpleClassifierInfo(this.sc).subscribe(
      res => {
        this.snackBar.open(`Simple Classifier '${this.sc.name}' updated.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed.';
        ref.componentInstance.message = `Failed to update the Simple Classifier. [${getErrorString(err)})]`;
      }
    );
  }

  addSkill() {
    if (this.isFormError(this.skillFormControl) || this.newSkill === undefined || this.newSkill === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the Skill.`;
    } else if (this.sc.skill_list.find(item => this.newSkill === item.name)) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Duplicated';
      ref.componentInstance.message = `Duplicate are Skills.`;
    } else {
      this.sc.skill_list.push({
        name: this.newSkill,
        regex: [],
        skill_description: this.skill_description
      });
      this.newSkill = '';
      this.check();
    }
  }

  inputAddSkill(event) {
    if (event.keyCode === 13) { // enter
      this.addSkill();
      event.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
    }
  }

  deleteSkill(index) {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete ${this.sc.skill_list[index].name} ?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.sc.skill_list.splice(index, 1);
        this.selectedSkill = undefined;
        this.check();
      }
    });
  }

  addRegEx() {
    if (this.isFormError(this.regExFormControl) || this.newRegExp === undefined || this.newRegExp === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the regEx`;
    } else if (this.selectedSkill.regex.find(item => this.newRegExp === item)) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Duplicate are regEx.`;
    } else {
      this.selectedSkill.regex.push(this.newRegExp);
      this.newRegExp = '';
      this.check();
    }
  }

  inputAddRegEx(event) {
    if (event.keyCode === 13) {
      this.addRegEx();
      event.preventDefault();
    }
  }

  editRegEx(index) {
    let changeRegExps: HTMLInputElement = <HTMLInputElement>(document.getElementsByName('regex-edit')[index]);
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Edit RegEx ${changeRegExps.value} ?`;
    ref.afterClosed().subscribe(result => {
        if (result) {
          this.selectedSkill.regex.splice(index, 1, changeRegExps.value);
        } else {
          changeRegExps.disabled = true;
          changeRegExps.value = this.selectedSkill.regex[index];
        }
      }
    );
  }

  setEditableRegEx(index) {
    let changeRegExps: HTMLInputElement = <HTMLInputElement>(document.getElementsByName('regex-edit')[index]);
    changeRegExps.disabled = false;
  }

  deleteRegEx(index) {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete ${this.selectedSkill.regex[index]} ?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.selectedSkill.regex.splice(index, 1);
        this.check();
      }
    });
  }

  getButtonName() {
    return this.role === 'add' ? 'Add' : 'Save';
  }

  check() {
    if (this.isFormError(this.nameFormControl) || this.sc.name === undefined) {
      this.submitButton = true;
    } else if (this.sc.lang === undefined) {
      this.submitButton = true;
    } else if (this.sc.skill_list.length === 0) {
      this.submitButton = true;
    } else if (this.sc.skill_list.some(skill => skill.regex.length === 0)) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
      return true;
    }
  }

  openDialog(data) {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.sc.name}'?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  backPage: any = () => {
    this.objectManager.clean('sc');
    // 이전 detail 화면으로 이동합니다
    if (this.role === 'edit') {
      this.router.navigate(['../..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  };

  /**
   * Nlu Tagging Result Sentent Input에 입력 후 엔터시 getNluResult호출하도록 하는 함수
   * @param event
   */
  inputAddSentence(event: any): void {
    if (event.keyCode === 13) { // enter
      this.getNluResult();
      event.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
    }
  }

  /**
   * NLP3에 analyze, pos,ner,pos&ner TaggedList Grpc 요청하는 함수
   */
  getNluResult(): void {
    let inputTextParam = {
      text: this.nluSentence,
      lang: 'kor',
      split_sentence: true,
      use_tokenizer: false,
      level: 1,
      use_space: false,
      keyword_frequency_level: 0
    };
    this.sentences = [];
    this.oriSentList[0] = (this.nluSentence ? this.nluSentence : '');
    this.grpc.analyze(inputTextParam).subscribe(
      analyzeResult => {
        delete analyzeResult.$metadata;
        this.posResult = 'Loading.....';
        this.nerResult = 'Loading.....';
        this.posNerResult = 'Loading.....';
        forkJoin(
          this.grpc.getPosTaggedList(analyzeResult),
          this.grpc.getNerTaggedList(analyzeResult),
          this.grpc.getPosNerTaggedList(analyzeResult)
        ).subscribe(([posResult, nerResult, posNerResult]) => {
          this.posResult = posResult.result ? posResult.result[0] : '';
          this.nerResult = nerResult.result ? nerResult.result[0] : '';
          this.posNerResult = posNerResult.result ? posNerResult.result[0] : '';
          this.sentences.push(this.posResult, this.nerResult, this.posNerResult);
        }, ([posError, nerError, posNerError]) => {
          if (posError) {
            this.posResult = 'PosTagging Error';
          }
          if (nerError) {
            this.nerResult = 'NerTagging Error';
          }
          if (posNerError) {
            this.posNerResult = 'PosNerTagging Error';
          }
        })
      },
      analyzeErr => {
        this.posResult = 'Analyze Error';
        this.nerResult = 'Analyze Error';
        this.posNerResult = 'Analyze Error';
      }
    )
  }

  getSimpleClassifierInfo() {
    return new Promise((resolve, reject) => {
      this.grpc.getSimpleClassifierInfo(this.scName).subscribe(res => {
        if (res) {
          this.sc = res;
          this.objectManager.set('sc', this.sc);
          if (this.sc.name === '' || this.sc.name === undefined) {  // 조회 결과가 비정상인 경우
            reject();
          }
          resolve();
        } else {
          this.backPage();
          reject();
        }
      }, err => {
        let message = 'Fail to get sc info';
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        throw new Error();
      });
    });
  }

  testRegEx(index) {
    let regex = this.selectedSkill.regex[index];
    let regexObj = new RegExp(regex);
    this.matchSet = [];
    console.log(this.sentences);
    for (let s in this.sentences) {
      new Promise((resolve) => {
        let sent: string = this.sentences[s];
        let matchResult: MatchType = {
          words: [], range: [-1, -1], skill: '', regex: ''
        };
        let res = regexObj.exec(sent);
        for (let v in res) {
          if (v === '0') {
            matchResult.skill = this.selectedSkill.name;
            matchResult.regex = this.selectedSkill.regex[index];
            matchResult.range = [res['index'], res[0].length + res['index'] - 1];
            matchResult.words.push(res[v]);
          } else if (/\d/.test(v)) {
            matchResult.words.push(res[v]);
          }
        }
        resolve([matchResult, sent, s]);
      }).then((rsList: any[]) => {
        console.log(rsList);
        if (rsList) {
          let res: MatchType = rsList[0];
          let line: string = rsList[1];
          let indexNum = rsList[2];
          let start: number = res.range[0];
          let end: number = res.range[1];
          if (start === -1 || end === -1) { // matching 된 단어가 없을 때
            switch (indexNum) {
              case '0':
                this.posResult = '<span>' + line + '</span>';
                break;
              case '1':
                this.nerResult = '<span>' + line + '</span>';
                break;
              case '2':
                this.posNerResult = '<span>' + line + '</span>';
                break;
              default:
                break;
            }
          } else {
            let matchInfo = {
              skill: 'category : [' + res.skill + ']\r\n', match: '', group: []
            };
            for (let i in res.words) {
              if (i === '0') {
                matchInfo.match = 'match  : [' + res.words[0] + ']\r\n';
              } else {
                matchInfo.group.push('group#' + i + ' : [' + res.words[i] + ']\r\n');
              }
            }
            let match: string = matchInfo.skill + matchInfo.match + '\r\n';
            for (let i in matchInfo.group) {
              match += matchInfo.group[i];
            }
            let tagStr: string = '<span>' + line.substring(0, start) + '</span>';
            tagStr += '<span class="match-word" data-toggle="tooltip" data-html="true" ';
            tagStr += 'data-placement="bottom" title="' + match + '">' + line.substring(start, end + 1) + '</span>';
            tagStr += '<span>' + line.substring(end + 1) + '</span>';
            switch (indexNum) {
              case '0':
                this.posResult = tagStr;
                break;
              case '1':
                this.nerResult = tagStr;
                break;
              case '2':
                this.posNerResult = tagStr;
                break;
              default:
                break;
            }
          }
          this.cdr.detectChanges();
        }
      });
    }
  }
}
