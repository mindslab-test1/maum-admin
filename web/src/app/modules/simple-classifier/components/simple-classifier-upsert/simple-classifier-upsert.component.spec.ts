import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SimpleClassifierUpsertComponent} from './simple-classifer-upsert.component';

describe('SimpleClassiferUpsertComponent', () => {
  let component: SimpleClassifierUpsertComponent;
  let fixture: ComponentFixture<SimpleClassifierUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimpleClassifierUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleClassifierUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
