import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SimpleClassifierPanelComponent} from './simple-classifer-panel.component';

describe('SimpleClassiferPanelComponent', () => {
  let component: SimpleClassifierPanelComponent;
  let fixture: ComponentFixture<SimpleClassifierPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimpleClassifierPanelComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleClassifierPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
