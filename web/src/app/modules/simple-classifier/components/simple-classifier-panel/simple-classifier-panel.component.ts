import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-simple-classifier-panel',
  templateUrl: './simple-classifier-panel.component.html',
  styleUrls: ['./simple-classifier-panel.component.scss']
})
export class SimpleClassifierPanelComponent implements OnInit {
  scInfo: any;

  getImage(lang) {
    return lang === 'kor' ? '/assets/img/korean_05.png' : '/assets/img/english_05.png';
  }

  constructor() {
  }

  ngOnInit() {
  }

}
