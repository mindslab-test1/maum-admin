import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DialogAgentsComponent} from './components/dialog-agents/dialog-agents.component';
import {DialogAgentUpsertComponent} from './components/dialog-agent-upsert/dialog-agent-upsert.component';
import {DialogAgentDetailComponent} from './components/dialog-agent-detail/dialog-agent-detail.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: DialogAgentsComponent,
    },
    {
      path: 'new',
      component: DialogAgentUpsertComponent,
    },
    {
      path: ':id',
      component: DialogAgentDetailComponent,
    },
    {
      path: ':id/edit',
      component: DialogAgentUpsertComponent
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DialogAgentRoutingModule {
}
