import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DialogAgentRoutingModule} from './dialog-agent-routing.module';
import {DialogAgentsComponent} from './components/dialog-agents/dialog-agents.component';
import {DialogAgentDetailComponent} from './components/dialog-agent-detail/dialog-agent-detail.component';
import {DialogAgentUpsertComponent} from './components/dialog-agent-upsert/dialog-agent-upsert.component';
import {DialogAgentDetailTableComponent} from './components/dialog-agent-detail-table/dialog-agent-detail-table.component';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [
    DialogAgentsComponent,
    DialogAgentDetailComponent,
    DialogAgentUpsertComponent,
    DialogAgentDetailTableComponent],
  imports: [
    CommonModule,
    SharedModule,
    DialogAgentRoutingModule,
    MatSidenavModule,
    MatCardModule,
    MatCheckboxModule,
    MatListModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class DialogAgentModule {
}
