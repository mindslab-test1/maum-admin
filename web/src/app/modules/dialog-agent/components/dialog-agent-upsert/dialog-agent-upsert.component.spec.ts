import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DialogAgentUpsertComponent} from './dialog-agent-upsert.component';

describe('DialogAgentUpsertComponent', () => {
  let component: DialogAgentUpsertComponent;
  let fixture: ComponentFixture<DialogAgentUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAgentUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgentUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
