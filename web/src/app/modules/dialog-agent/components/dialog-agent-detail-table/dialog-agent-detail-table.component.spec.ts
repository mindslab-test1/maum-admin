import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DialogAgentDetailTableComponent} from './dialog-agent-detail-table.component';

describe('DialogAgentDetailTableComponent', () => {
  let component: DialogAgentDetailTableComponent;
  let fixture: ComponentFixture<DialogAgentDetailTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAgentDetailTableComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgentDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
