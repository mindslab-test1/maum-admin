import {Component, Input, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';
import {TableComponent} from 'app/shared';

@Component({
  selector: 'app-dialog-agent-detail-table',
  templateUrl: './dialog-agent-detail-table.component.html',
  styleUrls: ['../../../../shared/components/table/table.component.scss', './dialog-agent-detail-table.component.scss']
})
export class DialogAgentDetailTableComponent extends TableComponent {
}
