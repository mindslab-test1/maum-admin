import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSnackBar, MatDialog} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';

import {ROUTER_LOADED, DAI_UPDATE_ALL} from '../../../../core/actions';
import {AuthService} from '../../../../core';
import {
  AppObjectManagerService,
  AlertComponent,
  AppEnumsComponent,
  getErrorString
} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {ConfirmComponent} from 'app/shared';


@Component({
  selector: 'app-dialog-agent-detail',
  templateUrl: './dialog-agent-detail.component.html',
  styleUrls: ['./dialog-agent-detail.component.scss']
})

export class DialogAgentDetailComponent implements OnInit {
  title = '';
  da: any;
  daName: any;
  actions: any;
  daDamTable: any;
  daChatbotTable: any;
  runtimeTable: any;
  subscription = new Subscription();

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              public appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi,
              private location: Location,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.runtimeTable = {
      header: [
        {attr: 'name', name: 'Name', sortable: true},
        {attr: 'type', name: 'Type', sortable: true},
        {attr: 'desc', name: 'Description', sortable: true},
        {attr: 'default_value', name: 'Default Value', sortable: true},
        {attr: 'required', name: 'Required', sortable: true},
      ],
      rows: [],
    };

    this.daDamTable = {
      header: [
        {attr: 'dam_info.name', name: 'Name', sortable: true},
        {attr: 'dam_info.active', name: 'Active', sortable: true, asIcon: true},
        {attr: 'dam_info.port', name: 'Port'},
      ],
      rows: [],
    };

    this.daChatbotTable = {
      header: [
        {attr: 'chatbot_name', name: 'Chatbot', sortable: true},
        {attr: 'dai_name', name: 'DA Instance'},
        {attr: 'damState', name: 'DAM'}
        // {attr: 'state', name: 'State', asIcon: true},
        // {attr: 'action', name: 'Action'}
      ],
      rows: [],
    };


    let daObj = this.objectManager.get('da');
    // if (daObj === undefined) {
    //   this.backPage();
    //   return;
    // }
    // #@ daName을 설정합니다
    if (daObj === undefined) {
      this.route.params.subscribe(par => {
        this.daName = par['id'];
      });
    } else {
      this.da = daObj.da_info;
      this.daName = this.da.name;
    }
    if (this.daName === undefined) {
      this.backPage();
      return;
    }

    // this.da = daObj.da_info;
    this.title = `DA`;

    let id = 'da';
    let roles = this.consoleUserApi.getCachedCurrent().roles;
    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'DELETE',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      }
      // {
      //   icon: 'sync_disabled',
      //   text: 'Disable',
      //   callback: this.disable,
      //   hidden: AuthService.isNotAllowed(id, AuthService.ROLE.EXECUTE, roles)
      // }
    };

    let chain = new Promise((resolve, reject) =>
      this.grpc.getDialogAgentActivationInfoList(this.daName).subscribe(
        daDam => {
          console.log('#@ daDam :', daDam);
          this.daDamTable.rows = daDam.damwd_list;
          console.log('#@ this.daDamTable.rows :', this.daDamTable.rows);
          resolve();
        },
        err => reject(err))
    );

    // chain = chain.then(() => new Promise((resolve, reject) =>
    //   this.grpc.getDialogAgentInfo(this.daName).subscribe(
    //     da_with_dai_info => resolve(da_with_dai_info.chatbot_dai_list),
    //     err => reject(err)
    //   )));

    chain = chain.then(() => new Promise((resolve, reject) =>
      this.grpc.getDialogAgentInfo(this.daName).subscribe(
        da_with_dai_info => {
          if (da_with_dai_info === undefined) {
            this.snackBar.open('Cannot Find DA.', 'Confirm', {duration: 3000});
            this.backPage();
            return;
          }
          if (this.da === undefined || this.da.lang === undefined) {
            this.da = da_with_dai_info.da_info;
            console.log('#@ da_with_dai_info.da_info :', da_with_dai_info.da_info);
            console.log('#@ 111 this.da :', this.da);
            this.da.lang = `${this.appEnum.runtimeEnvironment[this.da.runtime_env]}`;
          }
          console.log('#@ 222 this.da :', this.da);
          this.title = `DA:${this.da.name}(${this.da.lang})`;
          this.cdr.detectChanges();
          resolve(da_with_dai_info.chatbot_dai_list)
        },
        err => reject(err)
      )));


    chain = chain.then((chatbot_dai_list: any[]) => new Promise((resolve, reject) =>
      this.grpc.getDialogAgentInstanceAllList().subscribe(
        result => {
          chatbot_dai_list.forEach(chatbot_dai => {
            let dais = result.dai_list.filter(dai => (dai.chatbot_name === chatbot_dai.chatbot_name)
              && (dai.name === chatbot_dai.dai_name));
            chatbot_dai.damState = [];
            dais[0].dai_exec_info.forEach(dai => {
              chatbot_dai.damState.push({
                damName: dai.dam_name,
                damActive: dai.active,
                damCnt: dai.cnt
              });
              this.store.dispatch({type: DAI_UPDATE_ALL, payload: dais});
            });
          });
          this.daChatbotTable.rows = chatbot_dai_list;
          resolve();
        },
        err => reject(err))));

    chain = chain.then((chatbot_dai_list: any[]) => new Promise((resolve, reject) => {
      // this.daChatbotTable.rows = chatbot_dai_list;
      this.grpc.getRuntimeParameters({da_name: this.da.name}).subscribe(
        result => {
          result.params.forEach(param => {
            if (param.type === 'DATA_TYPE_AUTH') {
              param.default_value = '******';
            }
            param.type = this.appEnum.dataType[param.type];
          });
          this.runtimeTable.rows = result.params;
          this.store.dispatch({type: ROUTER_LOADED});
          resolve();
        }, err => reject(err))
    }));

    chain.catch(err => {
      this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  edit: any = () => {
    this.objectManager.set('dam', this.da);
    // this.router.navigateByUrl('m2u/dialog-service/da-upsert#edit');
    // #@ da edit 화면으로 이동합니다
    this.router.navigate(['m2u-builder', 'dialog-agents', this.daName, 'edit']);
  };

  delete: any = () => {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${this.da.name}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      this.grpc.deleteDialogAgent([{name: this.da.name}]).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.backPage();
          this.snackBar.open(`Dialog Agent '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
        },
        err => {
          this.snackBar.open(getErrorString(err, `Failed to Delete Dialog Agent '${this.da.name}'.`)
            , 'Confirm', {duration: 10000});
        }
      );
    });
  };

  backPage: any = () => {
    this.objectManager.clean('da');
    // this.location.back();
    // #@ da 리스트 화면으로 이동합니다
    this.router.navigateByUrl('m2u-builder/dialog-agents');
  }
}
