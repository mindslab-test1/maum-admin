import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DialogAgentDetailComponent} from './dialog-agent-detail.component';

describe('DialogAgentDetailComponent', () => {
  let component: DialogAgentDetailComponent;
  let fixture: ComponentFixture<DialogAgentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAgentDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
