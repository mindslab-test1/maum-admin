import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DialogAgentsComponent} from './dialog-agents.component';

describe('DialogAgentsComponent', () => {
  let component: DialogAgentsComponent;
  let fixture: ComponentFixture<DialogAgentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAgentsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
