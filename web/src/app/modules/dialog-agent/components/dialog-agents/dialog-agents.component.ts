import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {
  Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSnackBar, MatSidenav, MatPaginator} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED, DA_UPDATE_ALL, DA_DELETE} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {AppObjectManagerService, AppEnumsComponent, getErrorString} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';


import {ConfirmComponent, TableComponent} from 'app/shared';

@Component({
  selector: 'app-dialog-agents',
  templateUrl: './dialog-agents.component.html',
  styleUrls: ['./dialog-agents.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DialogAgentsComponent implements OnInit, OnDestroy {
  page_title: any; // = 'Dialog Agents';
  page_description: any; // = 'Add Dialog Agent Managers and update information about Dialog Agent Managers.';
  title: any = 'Dialog Agents';

  body: any;
  actions: any;
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  row: any;
  headers;
  rows: any[] = [];

  subscription = new Subscription();
  pageLength: number;
  paginator: MatPaginator;

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  // @ViewChild('sidenav') sidenav: MatSidenav;
  // @ViewChild('infoPanel') infoPanel: DialogAgentsPanelComponent;
  @ViewChild('runningTemplate') runningTemplate: TemplateRef<any>;
  dataSource: MatTableDataSource<any>;

  // dialogRef: MatDialogRef<CommonDialogComponent>;

  constructor(private store: Store<any>,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private grpc: GrpcApi,
              private appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    let id = 'da';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      }
    };
    this.headers = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {
        attr: 'temp_name',
        name: 'Name',
        isSort: true,
        onClick: this.navigateDaDetailView,
        width: '30%'
      },
      {attr: 'temp_lang', name: 'Type', isSort: true, width: '20%'},
      {attr: 'temp_spec', name: 'Spec', isSort: true, width: '20%'},
      {attr: 'dam_list', name: 'Running', isSort: false, template: this.runningTemplate},
    ];

    // this.tableComponent.onRowClick = this.fillRowInfo;

    // subscribe store
    this.subscription.add(
      this.store.select('das')
      .subscribe(das => {
        if (das) {
          das.sort((a, b) => a.da_info.name > b.da_info.name);
          das.forEach(da => {
            da.temp_name = da.da_info.name;
            da.temp_lang = da.da_info.lang;
            da.temp_spec = da.da_info.da_prod_spec_enum;
            da.daai_list = da.daai_list.filter(daai => daai.active);
            da.daai_list.sort((a, b) => a.dam_name > b.dam_name);
          });
          // 초기 name 순으로 정렬
          das.sort((a, b) => {
            return a.da_info.name < b.da_info.name ? -1 : a.da_info.name > b.da_info.name ? 1 : 0;
          });
          this.rows = das;
          this.dataSource = new MatTableDataSource(this.rows);
          this.tableComponent.isCheckedAll = false;
          this.whenCheckChanged();
          setTimeout(() => {
            // this.fillRowInfo(this.rows[0]);
          }, this.rows.length > 0 ? 500 : 0);
        }
      })
    );

    this.grpc.getDialogAgentAllList().subscribe(
      result => {
        result.da_list.forEach(da => {
          let da_info = da.da_info;
          da.da_info.lang = `${this.appEnum.runtimeEnvironment[da_info.runtime_env]}`; // :${da_info.runtime_env_version}`;
          da.da_info.da_prod_spec_enum = this.getDialogAgentSpec(da.da_info.da_prod_spec);
        });
        console.log('#@ result.da_list :', result.da_list.length);
        this.pageLength = result.da_list.length;
        this.store.dispatch({type: DA_UPDATE_ALL, payload: result.da_list});
        this.store.dispatch({type: ROUTER_LOADED});
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged())
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    this.objectManager.clean('da');
    // this.router.navigateByUrl('m2u/dialog-service/da-upsert#add');
    // #@ da 신규생성 페이지로 이동합니다
    this.router.navigate(['new'], {relativeTo: this.activatedRoute});
  };

  edit: any = (data) => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }
    delete items[0].isChecked;
    delete items[0].temp_lang;
    delete items[0].temp_name;
    delete items[0].temp_spec;
    this.objectManager.set('da', items[0]);
    // this.router.navigateByUrl('m2u/dialog-service/da-upsert#edit');
    // #@ da 수정페이지로 이동합니다
    this.router.navigate([items[0].da_info.name, 'edit'],
      {relativeTo: this.activatedRoute});
  };

  delete: any = () => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.da_info.name);
    if (names.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      let keyList = names.map(key => {
        return {name: key};
      });
      this.grpc.deleteDialogAgent(keyList).subscribe(
        res => {
          let deletedItems = res.result_list.filter(item => item.result).map(item => item.name);
          this.store.dispatch({type: DA_DELETE, payload: deletedItems});
          this.snackBar.open(`Dialog Agent '${deletedItems}' Deleted.`, 'Confirm', {duration: 3000});
        },
        err => {
          let message = `Failed to Delete Dialog Agent '${names}'. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  // togglePanel: any = () => {
  //   this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
  //   this.sidenav.toggle();
  // }
  //
  // fillRowInfo: any = (row: any) => {
  //   this.infoPanel.daInfo = row;
  //   if (row === undefined) {
  //     this.sidenav.close();
  //     this.panelToggleText = 'Show Info Panel';
  //   } else {
  //     this.sidenav.open();
  //     this.panelToggleText = 'Hide Info Panel';
  //   }
  // }

  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        break;
    }
  };

  navigateDaDetailView: any = (row: any) => {
    delete row.temp_lang;
    delete row.temp_name;
    delete row.temp_spec;
    this.objectManager.set('da', row);
    // this.router.navigateByUrl('m2u/dialog-service/da-detail');
    // #@ da detail 페이지로 이동합니다
    this.router.navigate([row.da_info.name],
      {relativeTo: this.activatedRoute});
  };

  getDialogAgentSpec: any = (val, row): string => {
    let spec = this.appEnum.dialogAgentSpecKeys.findIndex(key => key === val) + 1;
    return 'V' + spec;
  };

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getCustomIcon(active, positive_icon, negative_icon) {
    return active ?
      typeof positive_icon === 'string' ? positive_icon : 'check_circle' :
      typeof negative_icon === 'string' ? negative_icon : 'block';
  }

  getIcon(active) {
    return this.getCustomIcon(active, null, null);
  }
}
