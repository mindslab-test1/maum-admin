import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NlpTestComponent} from './nlp-test.component';

describe('NlpTestComponent', () => {
  let component: NlpTestComponent;
  let fixture: ComponentFixture<NlpTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NlpTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NlpTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
