import {Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {NlpTestService} from '../services/nlp-test.service';
import {NlpTestEntity} from '../entity/nlp-test.entity';
import {
  AlertComponent, AppEnumsComponent, DownloadService,
  FormErrorStateMatcher
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-ta-tools-nlp-test',
  templateUrl: './nlp-test.component.html',
  styleUrls: ['./nlp-test.component.scss'],
  providers: [{
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }],
  encapsulation: ViewEncapsulation.None
})

/**
 * NLP Test Component
 */
export class NlpTestComponent implements OnInit {

  @ViewChild('testResultContainer') testResultContainer: ElementRef;
  context: string = null;
  isSubmit: boolean;
  result: string = null;
  textFormControl = new FormControl('', [
    Validators.required,
  ]);

  korNlps = [
    {langCode: this.appEnum.langCode.kor, nlpName: 'nlp1'},
    {langCode: this.appEnum.langCode.kor, nlpName: 'nlp2'},
    {langCode: this.appEnum.langCode.kor, nlpName: 'nlp3'}
  ];

  engNlps = [
    {langCode: this.appEnum.langCode.eng, nlpName: 'nlp2'}
  ];

  otherLevels = [
    {levelName: 'All', levelValue: 'NLP_ANALYSIS_ALL'},
    {levelName: 'Morpheme', levelValue: 'NLP_ANALYSIS_MORPHEME'},
    {levelName: 'Named Entity', levelValue: 'NLP_ANALYSIS_NAMED_ENTITY'},
  ];

  nlp3Levels = [
    {levelName: 'All', levelValue: 'NLP_ANALYSIS_ALL'},
    {levelName: 'Morpheme', levelValue: 'NLP_ANALYSIS_MORPHEME'},
    {levelName: 'Named Entity', levelValue: 'NLP_ANALYSIS_NAMED_ENTITY'},
    {levelName: 'Word Sense Disambiguation', levelValue: 'NLP_ANALYSIS_WORD_SENSE_DISAMBIGUATION'},
    {levelName: 'Dependency Parser', levelValue: 'NLP_ANALYSIS_DEPENDENCY_PARSER'},
    {levelName: 'Semantic Role Labeling', levelValue: 'NLP_ANALYSIS_SEMANTIC_ROLE_LABELING'},
    {levelName: 'Zero Anaphora', levelValue: 'NLP_ANALYSIS_ZERO_ANAPHORA'},
  ];

  splitSentences = [
    {view: 'Use', isSplit: true},
    {view: 'Not use', isSplit: false}
  ];

  selectedNlp = {langCode: null, nlpName: null};
  selectedLevel = null;
  selectedSps = null;

  workspaceId: string = null;
  fileReader: any;

  constructor(public matcher: FormErrorStateMatcher,
              private dialog: MatDialog,
              private nlpTestService: NlpTestService,
              private downloadService: DownloadService,
              public appEnum: AppEnumsComponent,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.isSubmit = true;
    /*
        let storageVar = this.storage.get('m2uWorkspaceId');
        this.workspaceId = storageVar ? storageVar.toString() : '1';
        let langCode = this.storage.get('workspace').filter(item => item.id === this.workspaceId)[0].langCode;
        // kor 일 때는 default 로 Nlp3 선택, eng 일 때는 default 로 nlp2 선택
    */
    let langCode = this.appEnum.langCode.kor;
    if (langCode === this.appEnum.langCode.kor) {
      this.selectedNlp = this.korNlps[2];
    } else if (langCode === this.appEnum.langCode.eng) {
      this.selectedNlp = this.engNlps[0];
    }

    this.selectedSps = this.splitSentences[0];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * NLP 선택이 변경될 때 UI에서 세팅한 값들을 초기화 한다.
   */
  changeNlp() {
    this.selectedLevel = null;
    this.context = '';
    this.result = null;
    this.isValid();
  }

  /**
   * 파일로 복수개의 문장을 업로드 하여, 화면에 보여준다.
   */
  fileUpload(): void {
    // get file
    let file = (<HTMLInputElement>document.getElementById('file')).files[0];

    if (!file) {
      (<HTMLInputElement>document.getElementById('file')).value = '';
      this.openAlertDialog('Failed', `File upload failed.`, 'error');
    } else if (!file.type.match('text.*')) {
      (<HTMLInputElement>document.getElementById('file')).value = '';
      this.openAlertDialog('Failed', `File type miss match. Only use text file.`, 'error');
    } else {
      // 1. fileReader 객체 생성
      this.fileReader = new FileReader();

      // 2. 파일을 읽는다.
      this.fileReader.readAsText(file);

      // 3. 읽을 파일의 데이터를 this.readText에 저장한다.
      this.fileReader.onload = () => {
        this.context = this.fileReader.result;
        this.isValid();
        (<HTMLInputElement>document.getElementById('file')).value = '';
      };
    }
  }

  /**
   * Analyze 결과로 얻은 document data를 파일로 다운한다.
   */
  fileDownload() {
    this.downloadService.downloadTxtFile(JSON.stringify(this.result), this.selectedNlp.nlpName);
  }

  /**
   * NLP Analyze
   */
  analyze() {
    let param: NlpTestEntity = new NlpTestEntity();
    // param = this.selectedNlp;
    param.workspaceId = this.storage.get('m2uWorkspaceId');
    param.nlpName = this.selectedNlp.nlpName;
    param.context = this.context;
    param.level = this.selectedLevel.levelValue;
    param.isSplit = this.selectedSps.isSplit;

    this.nlpTestService.getNLPTest(param).subscribe(
      res => {
        this.result = res.sentences;
        this.testResultContainer.nativeElement.scrollTop = 0;
      }, err => {
        this.openAlertDialog('Error', `Server error. Nlp analyze failed`, 'error');
      });
  }

  /**
   * analyze 할 수 있는 조건이 되는지 체크한다.
   */
  isValid() {
    if ((this.context !== null) && !this.matcher.isErrorState(this.textFormControl)
      && this.selectedLevel !== null) {
      if (this.context.trim() !== '') {
        this.isSubmit = false;
      }
    } else {
      this.isSubmit = true;
    }
  }

  /**
   * 메세지를 보여주기위한 Dialog Component를 연다.
   * @param title => 에러 메세지의 타이틀
   * @param message => 에러 메세지
   * @param status => Error, Confirm, Notice 상태 값을 가진다. 상태값에 따라 Dialog색상이 달라진다.
   */
  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
