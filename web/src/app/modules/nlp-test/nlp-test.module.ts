import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NlpTestComponent} from './components/nlp-test.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {NlpTestRoutingModule} from './nlp-test-routing.module';
import {NlpTestService} from './services/nlp-test.service';

@NgModule({
  declarations: [NlpTestComponent],
  imports: [
    CommonModule,
    NlpTestRoutingModule,
    SharedModule,
    MatSelectModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule
  ],
  providers: [
    NlpTestService
  ]
})
export class NlpTestModule {
}
