import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NlpTestComponent} from './components/nlp-test.component';

const routes: Routes = [
  {
    path: '',
    component: NlpTestComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NlpTestRoutingModule {
}
