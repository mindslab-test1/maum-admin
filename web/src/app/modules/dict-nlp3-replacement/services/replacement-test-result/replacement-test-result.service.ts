import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class ReplacementTestResultService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  // 테스트
  test(type: string, id: string, params: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/apply-and-test/' + this.WORKSPACE_ID
      + '/' + id + '/' + this.ID, params);
  }

  // 테스트 결과 가져오기
  getResult(type: string, dictionaryId: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/test/get-result/replacement/' + this.WORKSPACE_ID
      + '/' + dictionaryId, null);
  }

  getResultDetail(type: string, testId: number): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/test/get-resultDetail/replacement/' + this.WORKSPACE_ID
      + '/' + testId, null);
  }

  deleteResult(type: string, ids: number[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/test/delete-result/replacement/' + this.WORKSPACE_ID, ids);
  }
}
