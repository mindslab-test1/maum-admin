import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {ReplacementDictionaryLineEntity} from '../../entity/replacement-dictionary-line/replacement-dictionary-line.entity';
import {ReplacementDictionaryEntity} from '../../entity/replacement-dictionary/replacement-dictionary.entity';
import {environment} from 'environments/environment';
import {NerDictionaryEntity} from '../../../dict-nlp3-ner/entity/ner-dictionaries/ner-dictionary.entity';

interface DictionaryData {
  type: number,
  name: string
}

@Injectable()
export class ReplacementDictionaryService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  // replacement 사전 관련
  getDictionaryList(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionaryList', null);
  }

  getDictionaryName(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionaryName', id);
  }

  updateDictionary(entity: ReplacementDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/update-dictionary', entity);
  }

  getDictionaryContentsWithPage(param: ReplacementDictionaryLineEntity): Observable<any> {
    let req = {
      'id': param.versionId,
      'pageIndex': param.pageIndex,
      'workspaceId': this.WORKSPACE_ID,
      'pageSize': param.pageSize
    }
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionaryContentsWithPage', param);
  }

  deleteDictionaryContentsWithIdx(param: ReplacementDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/delete-dictionaryContentsWithIdx', param);
  }

  insertDictionaryLineWithIdx(param: ReplacementDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/insert-dictionaryLineWithIdx', param);
  }

  deleteDictionaries(idList: string[]): Observable<any> {
    let entities: ReplacementDictionaryEntity[] = [];
    for (let param of idList) {
      let entity: ReplacementDictionaryEntity = new ReplacementDictionaryEntity();
      entity.id = param;
      entity.name = '';
      entities.push(entity);
    }
    return this.http.post(this.API_URL + '/dictionary/replacement/delete-dictionaries', entities);
  }

  insertDic(param: any): Observable<any> {
    let entity: ReplacementDictionaryEntity = new ReplacementDictionaryEntity();
    entity.workspaceId = param.workspaceId;
    entity.creatorId = this.ID;
    entity.name = param.name;
    entity.description = param.description;
    return this.http.post(this.API_URL + '/dictionary/replacement/insert-dictionary', entity);
  }

  getDictionary(name: String): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionary', name);
  }

  getDictionaryTypeList(): Observable<any> {
    return this.http.get(this.API_URL + '/dictionary/replacement/get-dictionaryTypeList');
  }

  // ner 사전 contents로 존재하는 타입 리스트 가져오기
  getDictionaryContentsTypes(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionaryContents-type/' + this.WORKSPACE_ID
      + '/' + id, null);
  }

  // replacement 사전 contents 관련
  getDictionaryContents(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionaryContents', param);
  }

  deleteDictionaryContents(replacementEntities: ReplacementDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/delete-dictionaryContents', replacementEntities);
  }

  deleteDictionaryContentsAll(versionId: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/delete-dictionaryContentsAll', versionId);
  }

  insertDictionaryLines(replacementEntities: ReplacementDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/insert-dictionaryContents', replacementEntities);
  }

  insertDictionaryLine(replacementEntity: ReplacementDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/insert-dictionaryLine', replacementEntity);
  }

  updateDictionaryLine(replacementEntity: ReplacementDictionaryLineEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/update-dictionaryLine', replacementEntity);
  }

  // git
  commitDictionary(replacementEntity: ReplacementDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/commit-dictionary', replacementEntity);
  }

  // Download
  downloadContents(data: ReplacementDictionaryLineEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/download-dictionary', data);
  }

  // Upload
  uploadContents(data: String[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/upload-dictionary', data);
  }

  // 테스트
  test(id: string, params: any): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/apply-and-test/' + this.WORKSPACE_ID
      + '/' + id + '/' + this.ID, params);
  }

  getContentsFromGit(replacementEntity: ReplacementDictionaryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/get-dictionary-from-git', replacementEntity);
  }

  // 특정 사전 제거
  deleteDictionary(id: string): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/replacement/delete-dictionary', id);
  }

  // 사전 적용
  applyDictionary(id: string): Observable<any> {
    return this.http.get(this.API_URL + '/dictionary/replacement/apply-dictionary/' + this.WORKSPACE_ID
      + '/' + id);
  }

  getRepDictionaryResource(vid: string, wid: string, uid: string): Observable<any> {
    let params: String[];
    params = [vid, wid, uid];
    return this.http.post(this.API_URL + '/dictionary/replacement/get-nlp-resource', params);
  }
}
