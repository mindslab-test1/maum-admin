import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReplacementDictionariesComponent} from './components/replacement-dictionaries/replacement-dictionaries.component';
import {ReplacementDictionaryDetailComponent} from './components/replacement-dictionary-detail/replacement-dictionary-detail.component';
import {ReplacementTestComponent} from './components/replacement-test/replacement-test.component';
import {ReplacementTestResultComponent} from './components/replacement-test-result/replacement-test-result.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ReplacementDictionariesComponent,
      },
      {
        path: ':id',
        component: ReplacementDictionaryDetailComponent
      },
      {
        path: ':id/test',
        component: ReplacementTestComponent
      },
      // {
      //   path: ':id/test-result',
      //   component: ReplacementTestResultComponent
      // }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictNlp3ReplacementRoutingModule {
}
