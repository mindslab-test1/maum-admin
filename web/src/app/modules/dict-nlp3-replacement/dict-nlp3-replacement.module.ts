import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


import {DictNlp3ReplacementRoutingModule} from './dict-nlp3-replacement-routing.module';
import {ReplacementDictionariesComponent} from './components/replacement-dictionaries/replacement-dictionaries.component';
import {ReplacementDictionaryDetailComponent} from './components/replacement-dictionary-detail/replacement-dictionary-detail.component';
import {ReplacementTestComponent} from './components/replacement-test/replacement-test.component';
import {ReplacementTestResultComponent} from './components/replacement-test-result/replacement-test-result.component';
import {TestFileComponent} from './components/test-file/test-file.component';
import {SharedModule} from '../../shared/shared.module';
import {AgGridModule} from 'ag-grid-angular';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatIconModule,
  MatToolbarModule
} from '@angular/material';
import {ReplacementDictionaryService} from './services/replacement-dictionaries/replacement-dictionary.service';
import {TestFileService} from './services/test-file/test-file.service';
import {ReplacementTestResultService} from './services/replacement-test-result/replacement-test-result.service';

@NgModule({
  declarations: [
    ReplacementDictionariesComponent,
    ReplacementDictionaryDetailComponent,
    ReplacementTestComponent,
    ReplacementTestResultComponent,
    TestFileComponent
  ],
  imports: [
    CommonModule,
    DictNlp3ReplacementRoutingModule,
    SharedModule,
    AgGridModule.withComponents([]),
    MatChipsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCheckboxModule,
    MatCardModule
  ],
  exports: [
    ReplacementDictionariesComponent,
    ReplacementDictionaryDetailComponent,
    ReplacementTestComponent,
    ReplacementTestResultComponent,
    TestFileComponent
  ],
  providers: [
    ReplacementDictionaryService,
    ReplacementTestResultService,
    TestFileService
  ]
})
export class DictNlp3ReplacementModule {
}
