import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplacementTestResultComponent} from './replacement-test-result.component';

describe('ReplacementTestResultComponent', () => {
  let component: ReplacementTestResultComponent;
  let fixture: ComponentFixture<ReplacementTestResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplacementTestResultComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacementTestResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
