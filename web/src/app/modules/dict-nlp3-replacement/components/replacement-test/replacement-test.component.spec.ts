import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplacementTestComponent} from './replacement-test.component';

describe('ReplacementTestComponent', () => {
  let component: ReplacementTestComponent;
  let fixture: ComponentFixture<ReplacementTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplacementTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacementTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
