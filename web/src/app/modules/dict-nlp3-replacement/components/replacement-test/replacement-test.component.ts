import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {AlertComponent} from 'app/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {ReplacementDictionaryService} from '../../services/replacement-dictionaries/replacement-dictionary.service';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {ReplacementTestResultService} from '../../services/replacement-test-result/replacement-test-result.service';

@Component({
  selector: 'app-replacement-test',
  templateUrl: './replacement-test.component.html',
  styleUrls: ['./replacement-test.component.scss']
})
export class ReplacementTestComponent implements OnInit {
  fileList: string[] = [];
  testStat = 0;
  checkedList: number[] = [];

  isTest: boolean;
  isReset: boolean;
  test_btn: HTMLElement;
  reset_btn: HTMLElement;

  dictionaryId = '';
  dictionaryType = '';
  dictionaryName = '';
  workspaceId = '';

  constructor(private replacementDictionaryService: ReplacementDictionaryService,
              private testResultService: ReplacementTestResultService,
              private route: ActivatedRoute,
              private store: Store<any>,
              private router: Router,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.isReset = false;
    this.isTest = false;

    this.load();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  load() {
    new Promise((resolve, reject) => {
      // 파일 사전 기본 정보 가져오기
      this.route.params.subscribe(par => {
        this.dictionaryType = 'replacement';
        this.dictionaryName = par['id'];
        this.replacementDictionaryService.getDictionary(this.dictionaryName).subscribe(res => {
          if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
            this.dictionaryId = res.dictionary_id;
            this.dictionaryId = res.dictionary_id;
            this.workspaceId = this.storage.get('m2uWorkspaceId');
            resolve();
          } else {
            reject('Error. Something wrong!');
          }
        }, err => {
          reject('Error. Something wrong!');
        });
      }, err2 => reject('Error. Something wrong!'));
    }).then(() => {
      this.replacementDictionaryService.getDictionaryContentsTypes(this.dictionaryId).subscribe(res => {
        if (res.hasOwnProperty('list')) {
          let typeList: number[] = res.list;
          // this.test_dictionaries.forEach(x => {
          //   if (typeList.includes(x.value)) {
          //     x.disabled = false;
          //   }
          // });
        }
      });
      this.checkButton();
    }).catch((msg: string) => {
      this.back(msg);
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  test = () => {
    // 사전 적용 및 테스트 결과  보여주기
    new Promise((resolve, reject) => {
      let param = {
        'checkedList': this.checkedList,
        'fileList': this.fileList
      };
      this.isReset = false;
      this.isTest = false;
      this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
        resolve();
      }, err => {
        reject();
      });
    }).then(() => {
      let tmp = this.dictionaryId;
      this.dictionaryId = '';
      this.dictionaryId = tmp;
      this.testStat += 1;
      this.checkButton();
      this.openAlertDialog('Test', 'Test done.', 'success');
    }).catch(() => {
      this.checkButton();
      this.openAlertDialog('Test', 'Something wrong', 'error');
    });
  };

  reset = () => {
    let param = {
      'checkedList': this.checkedList,
      'fileList': []
    };
    this.isReset = false;
    this.isTest = false;
    this.testResultService.test(this.dictionaryType, this.dictionaryId, param).subscribe(res => {
      this.openAlertDialog('Test', 'Reset done.', 'success');
    }, error1 => {
      this.openAlertDialog('Reset', 'Something wrong!', 'error');
    });
    this.checkButton();
  };

  back = (msg: string) => {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  // clickCheckbox = (val) => {
  //   let target = this.test_dictionaries.filter(x => x.value === val)[0];
  //   target.checked = !target.checked;
  //   this.checkButton();
  // };

  selectFiles = (event) => {
    this.fileList = event;
    this.checkButton();
  };

  checkButton = () => {
    if (this.checkedList.length > 0) {
      this.isReset = true;
      if (this.fileList.length > 0) {
        this.isTest = true;
      } else {
        this.isTest = false;
      }
    } else {
      this.isTest = false;
      this.isReset = false;
    }
  }
}
