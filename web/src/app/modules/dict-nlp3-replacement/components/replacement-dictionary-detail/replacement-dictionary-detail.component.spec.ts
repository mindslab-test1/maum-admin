import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplacementDictionaryDetailComponent} from './replacement-dictionary-detail.component';

describe('ReplacementDictionaryDetailComponent', () => {
  let component: ReplacementDictionaryDetailComponent;
  let fixture: ComponentFixture<ReplacementDictionaryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplacementDictionaryDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacementDictionaryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
