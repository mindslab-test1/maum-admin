import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {AlertComponent, CommitDialogComponent, DownloadService} from 'app/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {ReplacementDictionaryLineEntity} from '../../entity/replacement-dictionary-line/replacement-dictionary-line.entity';
import {ReplacementDictionaryService} from '../../services/replacement-dictionaries/replacement-dictionary.service';
import {ReplacementDictionaryEntity} from '../../entity/replacement-dictionary/replacement-dictionary.entity';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {DatePipe} from '@angular/common';
import {NerDictionaryLineEntity} from '../../../dict-nlp3-ner/entity/ner-dictionary-line/ner-dictionary-line.entity';

interface DicInfo {
  id: string;
  name: string;
  description: string;
  appliedYn: boolean;
  workspaceId: string;
}

interface Line {
  id: string;
  type: number;
  srcStr: string;
  destStr: string;
  useYn: boolean;
  versionId: string;
  description: string;
  creatorId: string;
  createdAt: Date;
}

interface IDatasource {
  getRows(params: IGetRowsParams): void;

  destroy?(): void;
}

interface IGetRowsParams {
  startRow: number;
  endRow: number;
  sortModel: any,
  filterModel: any;
  context: any;

  successCallback(rowsThisBlock: any[], lastRow?: number): void;

  failCallback(): void;
}

type dic_contents = Line[];

@Component({
  selector: 'app-replacement-dictionary-contents',
  templateUrl: './replacement-dictionary-detail.component.html',
  styleUrls: ['./replacement-dictionary-detail.component.scss']
})
export class ReplacementDictionaryDetailComponent {
  public gridOptions: GridOptions;
  public defaultColDef;

  actions: any[] = [];
  editable = false;

  dictionary: DicInfo = {id: '', name: '', description: '', appliedYn: false, workspaceId: ''};
  dictionary_rows: dic_contents = [];
  dictionary_types: string[] = [];
  selected_click_data: HTMLElement = undefined;
  replacement_entities: ReplacementDictionaryLineEntity[] = [];
  workspaceId: string;
  versionId: string = undefined;
  reader: any;
  current_user_id: string;
  current_user_name: string;
  mactions: any = {};
  bMigration = false;

  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      let req: ReplacementDictionaryLineEntity = new ReplacementDictionaryLineEntity();
      if (this.gridOptions.api) {
        req.pageIndex = this.gridOptions.api.paginationGetCurrentPage();
        req.pageSize = this.gridOptions.api.paginationGetPageSize();
      } else {
        req.pageIndex = 1;
        req.pageSize = 100;
      }
      req.id = this.dictionary.id;
      req.versionId = this.dictionary.id;
      req.creatorId = this.current_user_id;
      this.replacementDictionaryService.getDictionaryContentsWithPage(req).subscribe(res => {
        if (res.hasOwnProperty('dictionary_contents')) {
          if (res['dictionary_contents'].hasOwnProperty('content')) {
            this.entitiesToData(res['dictionary_contents']['content']);
            this.replacement_entities = res.dictionary_contents.content;
            this.dictionary_rows = res.dictionary_contents.content;
            this.dictionary_rows.forEach(row => {
              if (row['createUserEntity']) {
                row['creatorId_label'] = row['createUserEntity']['username'];
                row['createdAt_label'] = this.datepipe.transform(row.createdAt, 'yyyy-MM-dd HH:mm:ss');
              }
            });
            params.successCallback(this.dictionary_rows, res['dictionary_contents_size']);
          } else {
            this.dictionary_rows = [];
            params.successCallback(this.dictionary_rows, 0);
          }
          if (res.hasOwnProperty('use_init')) {
            this.mactions['hidden'] = false;
            this.mactions['disabled'] = false;
            this.bMigration = true;
          }
        }
      });
    }
  }
  constructor(private replacementDictionaryService: ReplacementDictionaryService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private downloadService: DownloadService,
              private router: Router,
              private store: Store<any>,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              public  datepipe: DatePipe) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      this.dictionary.name = par['id'];
      if (!this.dictionary.name || this.dictionary.name === '') {
        this.back('Error. Check your dictionary name in a url');
      }
    });
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.dictionary_rows,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      pagination: true,
      rowModelType: 'infinite',
      cacheBlockSize: 100,
      paginationPageSize: 100,
      defaultColDef: this.defaultColDef,
      onRowSelected: this.onRowSelected,
      singleClickEdit: true,
      onCellValueChanged: (event) => {
        this.updateDictionaryData(event.data);
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };

    this.actions = [{
      type: 'edit',
      text: 'Edit',
      icon: 'edit_circle_outline',
      callback: this.edit,
      disabled: false,
      hidden: false
    }, {
      type: 'addLine',
      text: 'Add',
      icon: 'playlist_add',
      callback: this.addLine,
      disabled: false,
      hidden: true
    }, {
      type: 'deleteLine',
      text: 'Delete',
      icon: 'delete_sweep',
      disabled: true,
      callback: this.deleteLine,
      hidden: true
    }, {
      type: 'commit',
      text: 'Commit',
      icon: 'save',
      disabled: false,
      callback: this.commit,
      hidden: true
    }, {
      type: 'cancel',
      text: 'Cancel',
      icon: 'cancel',
      disabled: false,
      callback: this.cancel,
      hidden: true
    }, {
      type: 'download',
      text: 'Download',
      icon: 'file_download',
      disabled: false,
      callback: this.download,
      hidden: false
    }, {
      type: 'upload',
      text: 'Upload',
      icon: 'file_upload',
      disabled: false,
      callback: this.upload,
      hidden: true,
    }];

    this.mactions = {
      type: 'NLP',
      text: 'NLP dictionary migration',
      icon: 'system_update',
      disabled: false,
      action: this.getNLP,
      hidden: true,
    };


    this.load();
    this.current_user_id = this.storage.get('user').id;
    this.current_user_name = this.storage.get('user').username;
    this.store.dispatch({type: ROUTER_LOADED});
  }

  back(msg: string) {
    this.gridOptions.api.stopEditing();
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    this.router.navigate(['..'], {relativeTo: this.route});
  }

  load() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    let pro = new Promise((resolve, reject) => {
      this.replacementDictionaryService.getDictionary(this.dictionary.name).subscribe(res => {
        if (res.hasOwnProperty('dictionary_id') && res.dictionary_id !== '') {
          console.log('###', res.dictionary_id);
          this.dictionary.id = res.dictionary_id;
          this.gridOptions.columnApi.setColumnVisible('no', false);
          resolve();
        } else {
          reject('Error. Check your dictionary name in a url');
        }
      });
    });
    pro.then(() => {
      this.getDictionaryContents().then();
      this.getDictionaryTypeList();
    }).catch((msg: string) => {
      this.back(msg);
    });
  }

  onRowSelected = (param) => {
    let deleteBtn = this.actions.filter(y => y.type === 'deleteLine')[0];
    if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
      deleteBtn.disabled = false;
    } else {
      deleteBtn.disabled = true;
    }
  };

  getDictionaryContents = (): Promise<any> => {
    return new Promise(resolve => {
      try {
        this.gridOptions.api.setDatasource(this.dataSource);
      } catch (e) {
        this.back('Something wrong!');
      }
      resolve();
    });

    /*return new Promise(resolve => {
      let param = {
        'workspaceId': this.workspaceId,
        'id': this.dictionary.id
      };
      try {
        this.replacementDictionaryService.getDictionaryContents(param).subscribe(res => {
          if (res.hasOwnProperty('dictionary_contents')) {
            this.entitiesToData(res['dictionary_contents']);
            this.replacement_entities = res.dictionary_contents;
            this.dictionary_rows = res.dictionary_contents;
            this.dictionary_rows.forEach(row => {
              if (row['createUserEntity']) {
                row['creatorId_label'] = row['createUserEntity']['username'];
                row['createdAt_label'] = this.datepipe.transform(row.createdAt, 'yyyy-MM-dd HH:mm:ss');
              }
            });
          }
          if (res.hasOwnProperty('use_init')) {
            this.mactions['hidden'] = false;
            this.mactions['disabled'] = false;
            this.bMigration = true;
          }
          this.gridOptions.api.setRowData(this.dictionary_rows);
        });
      } catch (e) {
        this.back('Something wrong!');
      }
      resolve();
    });*/
  };

  getDictionaryTypeList = () => {
    try {
      this.replacementDictionaryService.getDictionaryTypeList().subscribe(res => {
        if (res) {
          this.gridOptions.api.getColumnDef('type').cellEditorParams = { values: res};
        }
      });
    } catch (e) {
      console.error('Fail to get dictionary types. ', e);
    }
  };

  entityToData = (entity: ReplacementDictionaryLineEntity): Line => {
    let one_line: Line = {
      id: undefined,
      type: undefined,
      srcStr: undefined,
      destStr: undefined,
      useYn: undefined,
      versionId: undefined,
      description: undefined,
      creatorId: undefined,
      createdAt: undefined
    };
    one_line.id = entity.id;
    one_line.type = entity.type;
    one_line.srcStr = entity.srcStr;
    one_line.destStr = entity.destStr;
    one_line.useYn = entity.useYn;
    one_line.versionId = entity.versionId;
    one_line.description = entity.description;
    one_line.creatorId = entity.creatorId;
    one_line.createdAt = entity.createdAt;
    return one_line;
  };

  entitiesToData = (entities: ReplacementDictionaryLineEntity[]) => {
    this.dictionary_rows = [];
    entities.map(x => {
      let one_line: Line = this.entityToData(x);
      this.dictionary_rows.push(one_line);
    });
  };

  dataToEntity = (data: Line, b_id = false): ReplacementDictionaryLineEntity => {
    let entity: ReplacementDictionaryLineEntity = new ReplacementDictionaryLineEntity();
    entity.type = data.type;
    entity.srcStr = data.srcStr;
    entity.destStr = data.destStr;
    entity.description = data.description;
    if (!b_id) {
      entity.id = data.id;
      entity.versionId = this.dictionary.id;
    }
    return entity;
  };

  dataToEntities = (data: dic_contents): ReplacementDictionaryLineEntity[] => {
    let entities: ReplacementDictionaryLineEntity[] = [];
    data.map(x => {
      let entity: ReplacementDictionaryLineEntity = this.dataToEntity(x);
      entities.push(entity);
    });
    return entities;
  };

  updateDictionaryData(data) {
    try {
      let param: ReplacementDictionaryLineEntity = this.dataToEntity(data);
      this.replacementDictionaryService.updateDictionaryLine(param).subscribe(res => {
        if (res) {
          if (res['success'] === 'duplicated') {
            this.back('Before text is duplicated!')
          }
        }
      });
    } catch (e) {
      this.back('Something wrong!');
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  updateToolbar = () => {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'download') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    if (this.editable) {
      this.gridOptions.columnApi.setColumnVisible('no', true);
    } else {
      this.gridOptions.api.stopEditing();
      this.gridOptions.columnApi.setColumnVisible('no', false);
      this.getDictionaryContents().then();
    }
    this.gridOptions.api.getColumnDef('srcStr').editable = !this.gridOptions.api.getColumnDef('srcStr').editable;
    this.gridOptions.api.getColumnDef('destStr').editable = !this.gridOptions.api.getColumnDef('destStr').editable;
    this.gridOptions.api.getColumnDef('type').editable = !this.gridOptions.api.getColumnDef('type').editable;
    this.gridOptions.api.getColumnDef('useYn').editable = !this.gridOptions.api.getColumnDef('useYn').editable;
    this.gridOptions.api.getColumnDef('description').editable = !this.gridOptions.api.getColumnDef('description').editable;
  };

  edit = () => {
    this.actions[0]['disabled'] = true;
    new Promise((resolve, reject) => {
      if (this.replacement_entities.length > 0) {
        try {
          this.replacement_entities.forEach(entity => {
            entity.versionId = this.dictionary.id;
          });
          this.replacementDictionaryService.insertDictionaryLines(this.replacement_entities).subscribe(
            res1 => {
              this.replacement_entities = [];
              resolve();
            });
        } catch (e) {
          reject();
        }
      } else {
        resolve();
      }
    }).then(() => {
      this.getDictionaryContents().then(() => {
        this.updateToolbar();
        this.actions[0]['disabled'] = false;
      });
    }).catch((e) => {
      this.back('Something wrong!');
    });
  };

  cancel = () => {
    this.updateToolbar();
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  download = () => {
    let params: ReplacementDictionaryLineEntity[] = this.dataToEntities(this.dictionary_rows);
    this.replacementDictionaryService.downloadContents(params).subscribe((res) => {
      if (res.hasOwnProperty('dictionary_contents')) {
        this.downloadService.downloadJsonFile(res.dictionary_contents, this.dictionary.name.toString());
        this.openAlertDialog('Download', `Corpus file downloaded successfully.`, 'success');
      } else {
        this.openAlertDialog('Failed', `Fail to download Corpus file.`, 'error');
      }
    }, (err) => {
      this.openAlertDialog('Failed', `Fail to download Corpus file.`, 'error');
    } );
  };

  upload = () => {
    let elem: HTMLInputElement = document.getElementById('selectFile') as HTMLInputElement;
    elem.click();
  };

  /*File Upload 시 호출되는 함수*/
  getData = (param) => {
    let file = param.target.files[0];
    if (file.type.match(/text\/plain/) || file.type.match(/\/json/)) {
      this.reader = new FileReader();
      this.reader.onload = (e: any) => {
        try {
          this.replacementDictionaryService.deleteDictionaryContentsAll(this.dictionary.id).subscribe(
            res => {
              this.replacementDictionaryService.uploadContents([this.reader.result, this.dictionary.id,
                this.workspaceId, this.current_user_id]).subscribe(res2 => {
                if (res2.hasOwnProperty('dictionary_contents')) {
                  this.dictionary_rows = [];
                  this.dictionary_rows = res2.dictionary_contents;
                  this.dictionary_rows.forEach(row => {
                    if (row['createUserEntity']) {
                      row['creatorId_label'] = row['createUserEntity']['username'];
                    } else {
                      row['creatorId_label'] = this.current_user_name;
                    }
                    row['createdAt_label'] = this.datepipe.transform(row.createdAt, 'yyyy-MM-dd HH:mm:ss');
                  });
                  this.gridOptions.api.setRowData(this.dictionary_rows);
                }
              });
            }
          );
        } catch (ex) {
          this.openAlertDialog('Failed', `Check your corpus file data.`, 'error');
          return;
        }
      };
      this.reader.readAsText(file);
    } else {
      this.openAlertDialog('Failed', `Check your corpus file format.`, 'error');
    }
  };

  addLine = () => {
    let param: ReplacementDictionaryLineEntity = new ReplacementDictionaryLineEntity();
    param.versionId = this.dictionary.id;
    param.creatorId = this.current_user_id;
    this.gridOptions.api.stopEditing();
    let selectedData: any = this.gridOptions.api.getSelectedNodes();
    let idx = 1;
    if (selectedData.length > 0) {
      idx = (+selectedData[selectedData.length - 1].rowIndex) + 1;
    } else {
      if (this.gridOptions.api.paginationGetRowCount()) {
        idx = this.gridOptions.api.paginationGetRowCount();
      }
    }
    param.lineIdx = idx + 1;
    this.replacementDictionaryService.insertDictionaryLineWithIdx(param).subscribe(
      res => {
        if (res.message === 'NO_VALID_DICTIONARY_TYPE') {
          this.openAlertDialog('Add Line', 'Please Add DICTIONARY TYPE first.', 'error');
        }
        if (res && res.dictionary_line) {
          if (res.dictionary_line['createUserEntity']) {
            res.dictionary_line['creatorId_label'] = res.dictionary_line['createUserEntity']['username'];
          } else {
            res.dictionary_line['creatorId_label'] = this.current_user_name;
          }
          res.dictionary_line['createdAt_label'] =
            this.datepipe.transform(res.dictionary_line.createdAt, 'yyyy-MM-dd HH:mm:ss');
          let val: NerDictionaryLineEntity = res.dictionary_line;
          this.gridOptions.api.updateRowData({addIndex: idx, add: [res.dictionary_line]});
          // this.gridOptions.api.updateRowData({add: [res.dictionary_line]});
          this.gridOptions.api.deselectAll();
          this.dictionary_rows.push(res.dictionary_line);
        }
      }, err => {
        this.openAlertDialog('Add Line', 'Something wrong.', 'error');
      }
    );
  };

  deleteLine = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    return new Promise((resolve, reject) => {
      this.replacementDictionaryService.deleteDictionaryContentsWithIdx(selectedData).subscribe(
        res => {
          resolve();
        }, err => {
          reject();
        }
      );
    }).then(() => {
      this.gridOptions.api.deselectAll();
      this.gridOptions.api.setDatasource(this.dataSource);
    });
  };

  commit = () => {
    this.gridOptions.api.stopEditing();
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.dictionary.name} updated`;
    new Promise((resolve, reject) => {
      ref.afterClosed().subscribe(
        result => {
          if (result) {
            let param: ReplacementDictionaryEntity = new ReplacementDictionaryEntity();
            param.name = this.dictionary.name;
            param.id = this.dictionary.id;
            param.version = this.dictionary.id;
            param.creatorId = this.current_user_id;
            param.workspaceId = this.workspaceId;
            this.replacementDictionaryService.commitDictionary(param).subscribe(
              res => {
                if (res.hasOwnProperty('entities')) {
                  this.dictionary_rows = res.entities;
                  resolve();
                } else {
                  reject('No data to commit.');
                }
              },
              err => {
                reject('Something wrong.');
              });
          }
        });
    }).then(() => {
      this.gridOptions.api.setRowData(this.dictionary_rows);
      this.updateToolbar();
    }).catch((msg: string) => {
      this.openAlertDialog('Delete Lines', msg, 'error');
    });
  };

  getNLP = () => {
    let data;
    let res = {'dictionary_contents': []};
    this.replacementDictionaryService.getRepDictionaryResource(this.dictionary.id, this.workspaceId, this.current_user_id).subscribe(
      dic_res => {
        if (dic_res.hasOwnProperty('dictionary_contents')) {
          this.openAlertDialog('Migration', 'Migration done.', 'success');
          this.gridOptions.api.setDatasource(this.dataSource);
        }
      }, err => {
          this.openAlertDialog('Migration', 'Something wrong', 'error');
      }
    );
    this.store.dispatch({type: ROUTER_LOADED});
  };

  createColumnDefs() {
    return [{
      headerName: '',
      field: 'no',
      width: 75,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    }, {
      headerName: 'AFTER',
      field: 'destStr',
      width: 200,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'BEFORE',
      field: 'srcStr',
      width: 200,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Dictionary Type',
      field: 'type',
      width: 100,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: false,
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {values: []}
    }, {
      headerName: 'Use',
      field: 'useYn',
      width: 100,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: false,
      cellEditor: 'agSelectCellEditor',
      cellEditorParams: {values: ['Y', 'N']}
    }, {
      headerName: 'Creator',
      field: 'creatorId_label',
      width: 100,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Created At',
      field: 'createdAt_label',
      width: 150,
      sort: 'desc',
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Description',
      field: 'description',
      width: 450,
      filter: 'agTextColumnFilter',
      editable: false,
    }
    ];
  }

  nlpTest = () => {
    // this.router.navigate(['test'], {relativeTo: this.route});
    this.router.navigateByUrl('m2u-builder/nlp-test');
  }

  nlpApply = () => {
    this.replacementDictionaryService.applyDictionary(this.dictionary.id).subscribe(
      res => {
        if (res) {
          this.openAlertDialog('Apply', 'Apply done.', 'success');
        } else {
          this.workspaceId = this.storage.get('m2uWorkspaceId');
        }
      }, () => {
        this.openAlertDialog('Test', 'Something wrong', 'error');
      });

  }
}
