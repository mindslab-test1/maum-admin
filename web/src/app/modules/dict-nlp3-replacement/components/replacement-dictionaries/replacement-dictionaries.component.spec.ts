import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReplacementDictionariesComponent} from './replacement-dictionaries.component';

describe('ReplacementDictionariesComponent', () => {
  let component: ReplacementDictionariesComponent;
  let fixture: ComponentFixture<ReplacementDictionariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReplacementDictionariesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacementDictionariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
