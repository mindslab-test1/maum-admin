import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {
  StorageBrowser
} from 'app/shared/storage/storage.browser';
import {DictionaryDictionaryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-dictionary-upsert-dialog.component';
import {AlertComponent} from 'app/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {ReplacementDictionaryService} from '../../services/replacement-dictionaries/replacement-dictionary.service';
import {ReplacementDictionaryEntity} from '../../entity/replacement-dictionary/replacement-dictionary.entity';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

interface DicInfo {
  id: string;
  name: string;
  changeCnt: number;
  creatorId: string;
  creatorId_label: string;
  workspaceId: string;
  description: string;
  appliedYn: string;
}

type dic_list = DicInfo[];

@Component({
  selector: 'app-replacement-dictionary',
  templateUrl: './replacement-dictionaries.component.html',
  styleUrls: ['./replacement-dictionaries.component.scss']
})
export class ReplacementDictionariesComponent {
  public gridOptions: GridOptions;
  public defaultColDef;
  rowData: dic_list = [];
  workspaceId: string;

  dictionary: string[] = [];
  actions: any[] = [];

  constructor(private replacementDictionaryService: ReplacementDictionaryService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private router: Router,
              private route: ActivatedRoute,
              private store: Store<any>,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.dictionaryTableOption(),
      rowData: this.rowData,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onRowSelected: this.onRowSelected,
      singleClickEdit: true,
      onCellClicked: (event) => {
        if (event.colDef.field === 'name') {
          this.router.navigate([event.data.name], {relativeTo: this.route});
        } else if (event.colDef.field === 'changeCnt') {
          this.router.navigate(['../../../histories/replacement', event.data.name, event.data.id], {relativeTo: this.route.parent});
        }
      },
      onCellValueChanged: (change_event) => {
        this.updateDesc(change_event.data);
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };
    this.actions = [{
      type: 'Add',
      text: 'Add',
      icon: 'add_circle_outline',
      callback: this.add,
      disabled: false,
      hidden: false
    }, {
      type: 'Edit',
      text: 'Edit',
      icon: 'edit_circle_outline',
      callback: this.edit,
      disabled: true,
      hidden: false
    }, {
      type: 'Delete',
      text: 'Delete',
      icon: 'remove_circle_outline',
      callback: this.delete,
      disabled: true,
      hidden: false
    },
    ];
    this.changeDictionary();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  load = () => {
    this.gridOptions.api.setRowData(this.rowData);
    this.workspaceId = this.storage.get('m2uWorkspaceId');
  };

  changeDictionary() {
    this.replacementDictionaryService.getDictionaryList().subscribe(
      res => {
        if (res) {
          this.rowData = [];
          res.forEach(elem => {
            let x = elem.entity;
            let dict: DicInfo = {
              id: undefined,
              name: undefined,
              changeCnt: undefined,
              description: undefined,
              creatorId: undefined,
              creatorId_label: undefined,
              workspaceId: undefined,
              appliedYn: undefined
            };
            dict.id = x.id;
            dict.name = x.name;
            dict.creatorId = x.creatorId;
            dict.creatorId_label = x['createUserEntity']['username'];
            dict.changeCnt = elem.cnt;
            dict.description = x.description;
            dict.workspaceId = x.workspaceId;
            dict.appliedYn = x.appliedYn;
            this.rowData.push(dict);
          });
          this.load();
        } else {
          this.workspaceId = this.storage.get('m2uWorkspaceId');
        }
      }, () => {
        this.back('Something wrong.');
      });
  }

  dictionaryTableOption = () => {
    let column_list = [{
      headerName: '',
      field: 'no',
      width: 25,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Dictionary id',
      field: 'id',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    }, {
      headerName: 'Dictionary name',
      field: 'name',
      width: 150,
      filter: 'agTextColumnFilter',
      editable: false,
      cellRenderer: this.nameRenderer,
      cellEditorSelector: function (params) {
        if (params.data.type === 'name') {
          return {

            component: 'numericCellEditor'
          };
        }
      }
    }, {
      headerName: 'Creator',
      field: 'creatorId_label',
      width: 100,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
    //   headerName: 'Updater',
    //   field: 'updaterId',
    //   width: 100,
    //   filter: 'agTextColumnFilter',
    //   editable: false,
    // }, {
      headerName: 'Change count',
      field: 'changeCnt',
      width: 100,
      filter: 'agTextColumnFilter',
      cellRenderer: this.cntRenderer,
      editable: false,
    }, {
      headerName: 'Applied',
      field: 'appliedYn',
      width: 70,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Description',
      field: 'description',
      width: 350,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'WorkspaceId',
      field: 'workspaceId',
      width: 1,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true
    }];
    return column_list;
  };

  updateDesc = (data) => {
    let param: ReplacementDictionaryEntity = new ReplacementDictionaryEntity();
    param.id = data.id;
    param.description = data.description;
    this.replacementDictionaryService.updateDictionary(param).subscribe();
  };

  nameRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">' + params.value + '</sapn>';
  };

  cntRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">' + params.value + '</sapn>';
  };

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  add = () => {
    let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent);
    ref.componentInstance.title = 'Add a replacement dictionary';
    ref.componentInstance.service = this.replacementDictionaryService;
    ref.componentInstance.meta = {
      workspaceId: this.storage.get('m2uWorkspaceId'),
      type: 'replacement',
      updaterId: this.storage.get('user').id
    };
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.changeDictionary();
      }
    });
  };

  edit = () => {
    let selectedData = this.gridOptions.api.getSelectedRows()[0];
    let ref: any = this.dialog.open(DictionaryDictionaryUpsertDialogComponent);
    ref.componentInstance.title = 'Edit the replacement dictionary';
    ref.componentInstance.service = this.replacementDictionaryService;
    ref.componentInstance.data = {
      name: selectedData.name,
      description: selectedData.description
    };
    ref.componentInstance.meta = {
      id: selectedData.id,
      workspaceId: this.storage.get('m2uWorkspaceId'),
      type: 'replacement',
      updaterId: this.storage.get('user').id
    };
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.changeDictionary();
      }
    });
  };

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    let idList: string[] = selectedData.map(v => v.id);
    this.replacementDictionaryService.deleteDictionaries(idList).subscribe(res => {
      this.openAlertDialog('Delete', 'Delete dictionary completely', 'success');
      this.changeDictionary();
    }, err => {
      this.openAlertDialog('Delete', 'Something wrong.', 'error');
    });
  };

  onRowSelected = (param) => {
    let editBtn = this.actions.filter(row => row.type === 'Edit')[0];
    let deleteBtn = this.actions.filter(row => row.type === 'Delete')[0];
    if (param.node) {
      let selectedRowCnt = param.node.rowModel.selectionController.getSelectedNodes().length;
      if (selectedRowCnt == 0) {
        editBtn.disabled = true;
        deleteBtn.disabled = true;
      } else if (selectedRowCnt === 1) {
        editBtn.disabled = false;
        deleteBtn.disabled = false;
      } else if(selectedRowCnt > 1) {
        editBtn.disabled = true;
        deleteBtn.disabled = false;
      }
    } else {
      deleteBtn.disabled = true;
    }
  };

  back(msg: string) {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    this.router.navigate(['..'], {relativeTo: this.route});
  }


}
