import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class ReplacementDictionaryLineEntity extends PageParameters {
  id: string;
  type: number;
  srcStr: string;
  destStr: string;
  useYn: boolean;
  versionId: string;
  description: string;
  createdAt: Date;
  creatorId: string;
  lineIdx: number;
}
