import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class ReplacementDictionaryEntity extends PageParameters {
  id: string;
  name: string;
  creatorId: string;
  workspaceId: string;
  changeCnt: number;
  version: string;
  description: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;
}
