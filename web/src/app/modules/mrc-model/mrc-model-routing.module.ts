import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PassageListComponent} from './components/passage-list/passage-list.component';
import {PassageUpsertComponent} from './components/passage-upsert/passage-upsert.component';
import {PassageDetailComponent} from './components/passage-detail/passage-detail.component';
import {QuestionAnswerListComponent} from './components/question-answer-list/question-answer-list.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: PassageListComponent,
    },
    {
      path: 'qas',
      component: QuestionAnswerListComponent,
    },
    // {
    //   path: 'new',
    //   component: PassageUpsertComponent,
    // },
    {
      path: ':id',
      component: PassageDetailComponent,
    },
    {
      path: ':id/edit',
      component: PassageUpsertComponent,
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MrcModelRoutingModule {
}
