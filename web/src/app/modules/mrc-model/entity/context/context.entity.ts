import {QuestionEntity} from 'app/modules/mrc-model/entity/question/question.entity';


export class ContextEntity {

  id: string;
  workspaceId: string;
  passageId: string;
  context: string;
  createAt: string;
  creatorId: string;

  constructor() {

  }
}
