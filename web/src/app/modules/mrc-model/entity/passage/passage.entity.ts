import {PageParameters} from 'app/shared/entities/pageParameters.entity';
import {QuestionEntity} from 'app/modules/mrc-model/entity/question/question.entity';

export class PassageEntity extends PageParameters {

  id: string;
  workspaceId: string;
  majorHeading: string;
  minorHeading: string;
  link: string;
  passage: string;
  creatorId: string;
  createdAt: string;
  updaterId: string;
  updatedAt: string;
  questionEntities: QuestionEntity[]

  headings: string;
  questionCnt: string;

  searchHeading: string;

  constructor() {
    super();
  }
}
