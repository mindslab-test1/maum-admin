export class AnswerEntity {

  id: string;
  answer: string;
  answerStart: number;
  answerEnd: number;
  questionId: string;
  workspaceId: string;

  constructor() {
    this.answer = '';
  }
}
