import {AnswerEntity} from 'app/modules/mrc-model/entity/answer/answer.entity';

export class QuestionEntity {

  id: string;
  workspaceId: string;
  passageId: string;
  question: string;
  isAnchor: boolean;
  answerEntity: AnswerEntity;

  updated: boolean;

  constructor() {
    this.answerEntity = new AnswerEntity();
    this.updated = false;
  }
}
