import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';
import {PassageEntity} from '../entity/passage/passage.entity';

@Injectable()
export class PassageService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getPassageList(param: PassageEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/list', param);
  }

  getPassage(id: string): Observable<any> {
    return this.http.get(this.API_URL + '/mrc/passage/id/' + id);
  }

  addPassage(passageEntity: PassageEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/add', passageEntity);
  }

  editPassage(passageEntity: PassageEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/edit', passageEntity);
  }

  removePassageList(passageEntities: PassageEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/remove/list', passageEntities);
  }

  removePassageListBySkillId(skillId: number): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/passage/remove/all', skillId);
  }

  // analyzeNlp(passageEntities: any): Observable<any> {
  //   return this.http.post(this.API_URL + '/mrc/passage/nlp-analysis', passageEntities);
  // }
  //
  // analyzeNlpAll(skillId: number): Observable<any> {
  //   return this.http.get(this.API_URL + '/mrc/passage/nlp-analysis/all/' + skillId);
  // }

  // removeQuestion(deleteData: any): Observable<any> {
  //   return this.http.post(this.API_URL + '/mrc/training-data/question/remove', deleteData);
  // }

  // processApply(contextId: string): Observable<any> {
  //   return this.http.get(this.API_URL + '/mrc/training-data/preprocess/' + contextId);
  // }

}
