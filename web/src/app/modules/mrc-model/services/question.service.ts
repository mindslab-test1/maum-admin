import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';
import {QuestionEntity} from '../entity/question/question.entity';
import {QuestionAnswerDTO} from '../dto/question-answer.dto';

@Injectable()
export class QuestionService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getQuestionAnswerList(questionAnswer: QuestionAnswerDTO): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/question/list', questionAnswer);
  }

  removeQuestion(questionEntity: QuestionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/mrc/question/remove', questionEntity);
  }
}
