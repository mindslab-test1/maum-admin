import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PassageEntity} from '../../entity/passage/passage.entity';
import {PassageService} from '../../services/passage.service';
import {QuestionService} from '../../services/question.service';
import {
  AlertComponent,
  ConfirmComponent,
  FormErrorStateMatcher,
  TableComponent
} from '../../../../shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {QuestionEntity} from '../../entity/question/question.entity';
import {ConsoleUserApi} from '../../../../core/sdk/services/custom';
import {AnswerEntity} from '../../entity/answer/answer.entity';

@Component({
  selector: 'app-mrc-passage-upsert',
  templateUrl: './passage-upsert.component.html',
  styleUrls: ['./passage-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class PassageUpsertComponent implements OnInit {
  // default set
  workspaceId: string;
  userId: string;
  invalidFlag = true;

  // qa upsert
  passageEntity: PassageEntity = new PassageEntity();
  questionEntity: QuestionEntity = new QuestionEntity();
  questionEntities: QuestionEntity[] = [];

  questionFormControl = new FormControl('', [
    Validators.required,
  ]);
  answerFormControl = new FormControl('', [
    Validators.required,
  ]);

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'question', name: 'Question', width: '80%'},
    {attr: 'action', name: '', type: 'text', isButton: true, buttonName: 'Delete'},
  ];

  constructor(private consoleUserApi: ConsoleUserApi,
              private passageService: PassageService,
              private questionService: QuestionService,
              private _formBuilder: FormBuilder,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(par => {
      this.passageEntity.id = par['id'];
    });
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.userId = this.consoleUserApi.getCachedCurrent().id;

    this.cdr.detectChanges();
    this.getPassage().then(() => {
      this.checkValid();
    })
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  getPassage() {
    return new Promise((resolve) => {
      this.passageEntity.workspaceId = this.workspaceId;
      this.passageService.getPassage(this.passageEntity.id).subscribe(res => {
        this.passageEntity = res;
        this.passageEntity.passage = this.passageEntity.passage
        .replace(/\r?\n/g, '<br>')
        .replace(/  +/g, s => ' ' + '&nbsp;'.repeat(s.length - 1));
        this.questionEntities = this.passageEntity.questionEntities;
        resolve();
      }, err => {
        this.openAlertDialog('Error', 'Failed to get passage.', 'error');
      })
    })
  }

  isDuplicated(questionEntities: QuestionEntity[], questionEntity: QuestionEntity): boolean {
    let question = questionEntities.find(q => q.question === questionEntity.question);

    if (!question || !question.answerEntity || !question.answerEntity.answer) {
      return false;
    }

    return question.answerEntity.answer === questionEntity.answerEntity.answer;
  }

  isLongEnough(question: string): boolean {
    return question.length >= 10;
  }

  isAnswerInPassageOrMinorHeading(answer: string): boolean {
    let escaped = answer
    .replace(/\r?\n/g, '<br>')
    .replace(/  +/g, s => ' ' + '&nbsp;'.repeat(s.length - 1));
    return this.passageEntity.passage.indexOf(escaped) >= 0
        || this.passageEntity.minorHeading === escaped;
  }

  addPassageQuestion() {
    this.questionEntity.question = this.questionEntity.question.trim().replace(/\s\s+/g, ' ');
    this.questionEntity.answerEntity.answer = this.questionEntity.answerEntity.answer.trim();

    if (!this.questionEntity.question) {
      this.questionFormControl.setErrors({'required': true});
    } else if (!this.isLongEnough(this.questionEntity.question)) {
      this.questionFormControl.setErrors({'too-short': true});
    } else if (!this.questionEntity.answerEntity.answer) {
      this.answerFormControl.setErrors({'required': true});
    } else if (!this.isAnswerInPassageOrMinorHeading(this.questionEntity.answerEntity.answer)) {
      this.answerFormControl.setErrors({'in-passage': true});
    } else if (this.isDuplicated(this.questionEntities, this.questionEntity)) {
      this.answerFormControl.setErrors({'duplicate': true});
    } else {
      let question = this.questionEntities.find(q => q.question === this.questionEntity.question);
      if (question) {
        // 질문의 답변 갱신
        if (!question.answerEntity) {
          question.answerEntity = new AnswerEntity();
        }
        question.answerEntity.answer = this.questionEntity.answerEntity.answer;
        question.updated = true;
      } else {
        // 신규 질문 등록
        this.questionEntity.passageId = this.passageEntity.id;
        this.questionEntity.workspaceId = this.workspaceId;
        this.questionEntity.answerEntity.workspaceId = this.workspaceId;
        this.questionEntity.updated = true;
        this.questionEntities.push(this.questionEntity);
      }
      this.questionEntity = new QuestionEntity();
      this.checkValid();
      this.questionFormControl.reset();
      this.answerFormControl.reset();
    }
  }

  upsertPassageQuestion() {
    if (!this.questionEntities.length) {
      this.openAlertDialog('Notice', `No Question.`, 'notice');
      return;
    }

    let questions = [];
    this.questionEntities.forEach(question => {
      if (question.updated) {
        delete question.updated;
        questions.push(question);
      }
    });

    if (!questions.length) {
      this.openAlertDialog('Notice', `No Updated Question.`, 'notice');
      return;
    }

    let passageEntity: PassageEntity = new PassageEntity();
    passageEntity.updaterId = this.userId;
    passageEntity.workspaceId = this.workspaceId;
    passageEntity.id = this.passageEntity.id;
    passageEntity.questionEntities = questions;
    this.passageService.editPassage(passageEntity).subscribe(res => {
          this.openAlertDialog('Success', `Save Success.`, 'success');
          this.goDetail();
        },
        err => {
          this.openAlertDialog('Failed', err.error, 'error');
          // this.router.navigated = false
          // this.router.navigate([this.router.url]);
        })
  }

  deletePassageQuestion = (question) => {
    let isNew = !question.id;
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.title = isNew ? 'Confirm' : 'WARNING!';
    ref.componentInstance.message = isNew ?
        `'${question.question}'<br>Continue to delete the question<br> from the list?` :
        `'${question.question}'<br>The question will be <strong>DELETED</strong> from `
        + `<strong>the DB</strong>!!!<br>Continue?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      if (isNew) {
        this.questionEntities.splice(this.questionEntities.findIndex(q => q.question === question.question), 1);
        this.checkValid();
      } else {
        this.questionService.removeQuestion(question).subscribe(res => {
              this.questionEntities.splice(this.questionEntities.findIndex(q => q.question === question.question), 1);
              this.checkValid();
              this.openAlertDialog('Success', `Delete Success.`, 'success');
            },
            err => {
              this.openAlertDialog('Failed', err.error, 'error');
            })
      }
    });
  }

  checkValid() {
    this.invalidFlag = this.passageEntity.headings === undefined
        || this.passageEntity.headings.trim() === ''
        || this.passageEntity.passage === undefined
        || this.passageEntity.passage.trim() === '';
  }

  getErrorMessage(formControl: FormControl) {
    if (formControl === this.questionFormControl) {
      return formControl.hasError('required') ? 'Please enter question.' :
          formControl.hasError('too-short') ? 'Question should be at least 10 letters long.'
              : null;
    } else if (formControl === this.answerFormControl) {
      for (const key of Object.keys(formControl.errors)) {
        if (formControl.errors[key]) {
          switch (key) {
            case 'required':
              return 'Please enter answer.';
            case 'in-passage':
              return 'Answer must be in passage or minor-heading.';
            case 'duplicate':
              return 'Answer is duplicated.';
            default:
              console.log(`Unhandled error [${key}].`);
              break;
          }
        }
      }
      return null;
    }
  }

  goDetail() {
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  goQAs() {
    this.router.navigate(['../../', 'qas'], {relativeTo: this.activatedRoute});
  }

  goList() {
    this.router.navigate(['../../'], {relativeTo: this.activatedRoute});
  }
}
