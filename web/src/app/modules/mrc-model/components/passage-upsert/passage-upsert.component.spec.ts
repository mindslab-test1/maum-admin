import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PassageUpsertComponent} from './passage-upsert.component';

describe('PassageUpsertComponent', () => {
  let component: PassageUpsertComponent;
  let fixture: ComponentFixture<PassageUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PassageUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassageUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
