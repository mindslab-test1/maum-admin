import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PassageDetailComponent} from './passage-detail.component';

describe('PassageDetailComponent', () => {
  let component: PassageDetailComponent;
  let fixture: ComponentFixture<PassageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PassageDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
