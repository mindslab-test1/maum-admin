import {ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PassageEntity} from '../../entity/passage/passage.entity';
import {PassageService} from '../../services/passage.service';
import {PassageQuestionEntity} from '../../entity/passage-question/passage-question.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {AlertComponent, FormErrorStateMatcher, TableComponent} from 'app/shared';
import {QuestionEntity} from "../../entity/question/question.entity";

@Component({
  selector: 'app-mrc-passage-detail',
  templateUrl: './passage-detail.component.html',
  styleUrls: ['./passage-detail.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class PassageDetailComponent implements OnInit {
  // default set
  flag: string;
  workspaceId: string;
  invalidFlag = true;

  // qa upsert
  passageEntity: PassageEntity = new PassageEntity();
  passageQuestionEntity: PassageQuestionEntity = new PassageQuestionEntity();
  passageQuestionEntities: PassageQuestionEntity[] = [];

  questionFormControl = new FormControl('', [
    Validators.required,
  ]);

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  dataSource: MatTableDataSource<any>;
  header = [
    {attr: 'question', name: 'Question', width: '50%'},
    {attr: 'questionMorph', name: 'Question Morph'},
  ];

  hidePassageMorphFlag = true;
  hideWordListFlag = true;

  constructor(private passageService: PassageService,
              private _formBuilder: FormBuilder,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(par => {
      this.passageEntity.id = par['id'];
    });
    this.workspaceId = this.storage.get('m2uWorkspaceId');

    this.flag = 'Edit';
    this.cdr.detectChanges();
    this.getPassage().then(() => {
      this.checkValid();
    })
    // this.cdr.detectChanges();
  }

  getPassage() {
    return new Promise((resolve) => {
      this.passageEntity.workspaceId = this.workspaceId;
      this.passageService.getPassage(this.passageEntity.id).subscribe(res => {
        this.passageEntity = res;
        this.passageEntity.passage = this.passageEntity.passage
        .replace(/\r?\n/g, '<br>')
        .replace(/  +/g, s => ' ' + '&nbsp;'.repeat(s.length - 1));
        for (const question of this.passageEntity.questionEntities) {
          if (question.answerEntity && question.answerEntity.answer) {
            question.answerEntity.answer = question.answerEntity.answer
            .replace(/\r?\n/g, '<br>')
            .replace(/  +/g, s => ' ' + '&nbsp;'.repeat(s.length - 1));
          }
        }
        resolve();
      }, err => {
        this.openAlertDialog('Error', 'Failed to get passage.', 'error');
      })
    })
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  addPassage() {
    this.passageEntity.workspaceId = this.workspaceId;
    // this.passageEntity.questionEntities = this.questionEntities;
    this.passageService.addPassage(this.passageEntity).subscribe(res => {
      this.openAlertDialog('Success', `Save Success.`, 'success');
      this.router.navigate(['..', 'new/'], {relativeTo: this.activatedRoute});
    })
  }

  addPassageQuestion() {
    if (this.passageQuestionEntity.question === undefined || this.passageQuestionEntity.question.trim() === '') {
      this.questionFormControl.setErrors({'required': true});
    } else if (this.passageQuestionEntities.findIndex(q => q.question === this.passageQuestionEntity.question) !== -1) {
      this.questionFormControl.setErrors({'duplicate': true});
    } else {
      this.passageQuestionEntity.workspaceId = this.workspaceId;
      this.passageQuestionEntities.push(this.passageQuestionEntity);
      this.passageQuestionEntity = new PassageQuestionEntity();
      this.checkValid();
      this.questionFormControl.reset();
    }
  }

  editPassage() {
    // this.passageEntity.questionEntities = this.questionEntities;
    this.passageService.editPassage(this.passageEntity).subscribe(res => {
      this.openAlertDialog('Success', `Save Success.`, 'success');
      this.router.navigate(['..', this.passageEntity.id, 'edit'], {relativeTo: this.activatedRoute});
    })
  }

  deletePassageQuestion = (row) => {
    if (this.flag === 'Edit') {
    }
    this.passageQuestionEntities.splice(this.passageQuestionEntities.findIndex(q => q.question === row.question), 1);
    this.checkValid();
  };

  checkValid() {
    this.invalidFlag = this.passageEntity.headings === undefined
        || this.passageEntity.headings.trim() === ''
        || this.passageEntity.passage === undefined
        || this.passageEntity.passage.trim() === '';
  }

  getErrorMessage() {
    return this.questionFormControl.hasError('required') ? 'Please enter question' :
        this.questionFormControl.hasError('duplicate') ? 'Question already exist' :
            '';
  }

  save() {
    if (this.flag === 'Add') {
      this.addPassage();
    } else if (this.flag === 'Edit') {
      this.editPassage();
    }
  }

  changeHidePassageMorphFlag() {
    if (this.hidePassageMorphFlag) {
      this.hidePassageMorphFlag = false;
    } else if (!this.hidePassageMorphFlag) {
      this.hidePassageMorphFlag = true;
    }
  }

  changeHideWordListFlag() {
    if (this.hideWordListFlag) {
      this.hideWordListFlag = false;
    } else if (!this.hideWordListFlag) {
      this.hideWordListFlag = true;
    }
  }

  goEdit() {
    this.router.navigate(['../', this.passageEntity.id, 'edit'], {relativeTo: this.activatedRoute});
  }

  goQAs() {
    this.router.navigate(['../', 'qas'], {relativeTo: this.activatedRoute});
  }

  goList() {
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

}
