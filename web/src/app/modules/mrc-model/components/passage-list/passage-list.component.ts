import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {PassageService} from '../../services/passage.service';
import {PassageEntity} from '../../entity/passage/passage.entity';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {
  AlertComponent, ConfirmComponent, FormErrorStateMatcher, TableComponent,
  UploaderButtonComponent
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {environment} from 'environments/environment';

@Component({
  selector: 'app-mrc-passage-list',
  templateUrl: './passage-list.component.html',
  styleUrls: ['./passage-list.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class PassageListComponent implements OnInit, AfterViewInit, OnDestroy {
  passageActionsArray: any[];
  workspaceId: string;

  pageParam: MatPaginator;
  pageLength: number;
  searchPassage = '';
  searchQuestion = '';
  searchHeading1 = '';
  searchHeading2 = '';
  summary = '';

  dataSource: MatTableDataSource<any>;
  passageParam: PassageEntity;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('passageTemplate') passageTemplate: TemplateRef<any>; /////////////////////////////////
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  @ViewChild('merger') merger: UploaderButtonComponent;
  header = [];
  passageList: any[] = [];

  private _onDestroy = new Subject<void>();

  constructor(private passageService: PassageService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.passageActionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.addPassage,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removePassage,
        params: 'remove'
      },
      {
        type: 'edit',
        text: 'Remove All',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removePassageAll,
        params: 'remove'
      },
    ];
    this.header = [
      // {attr: 'checkbox', name: 'Checkbox', checkbox: true},
      // {attr: 'no', name: 'No', no: false},
      {attr: 'headings', name: 'Headings', isSort: false, width: '20%'},
      {
        attr: 'passage',
        name: 'Passage',
        width: '70%',
        isSort: false,
        template: this.passageTemplate
      }, ///////////////////////////////////
      {attr: 'questionCnt', name: 'Question Count', width: '10%', isSort: false},
      {
        attr: 'action',
        name: 'Action',
        type: 'text',
        isButton: true,
        buttonName: 'Edit',
        width: '7%'
      },
    ];
  }

  ngAfterViewInit() {
    this.passageParam = new PassageEntity();
    let passageListInfo = this.storage.get('MrcPassageListInfo');
    if (passageListInfo && this.workspaceId === passageListInfo.workspaceId) {
      this.pageParam.pageIndex = passageListInfo.pageIndex;
      this.pageParam.pageSize = passageListInfo.pageSize;
      this.searchHeading1 = passageListInfo.searchHeading1;
      this.searchHeading2 = passageListInfo.searchHeading2;
      this.searchPassage = passageListInfo.searchPassage;
    } else {
      this.pageParam.pageIndex = 0;
      this.pageParam.pageSize = 10;
    }
    this.activatedRoute.queryParams.subscribe(
        query => {
          this.getNextPage();
          this.store.dispatch({type: ROUTER_LOADED});
        });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  searchKeydown(event) {
    if (event.keyCode === 13) {
      this.onClickSearchButton(0);
    }
  }

  /**
   * Search Button 정의
   * @param  pageIndex
   */
  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  onClickResetButton() {
    this.searchPassage = '';
    this.searchHeading1 = '';
    this.searchHeading2 = '';
    this.pageParam.pageIndex = 0;
    this.getNextPage();
  }

  search(matSort?: MatSort) {
    if (matSort) {
      if (!this.passageParam) {
        return;
      }
      // this.passageParam.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'asc' : matSort.direction;
      // this.passageParam.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    this.cdr.detectChanges();

    this.passageParam.pageIndex = this.pageParam.pageIndex;
    this.passageParam.pageSize = this.pageParam.pageSize;
    this.passageParam.workspaceId = this.workspaceId;
    this.passageParam.majorHeading = this.searchHeading1;
    this.passageParam.searchHeading = this.searchHeading2;
    this.passageParam.passage = this.searchPassage;

    this.passageService.getPassageList(this.passageParam).subscribe(res => {
          if (res) {
            this.summary = res.summary;
            this.passageList = res.list.content;
            this.pageLength = res.list.totalElements;
            this.dataSource = new MatTableDataSource(this.passageList);
            this.lineTableComponent.isCheckedAll = false;
            this.storage.set('MrcPassageListInfo', {
              'pageIndex': this.pageParam.pageIndex,
              'pageSize': this.pageParam.pageSize,
              'workspaceId': this.workspaceId,
              'searchHeading1': this.searchHeading1,
              'searchHeading2': this.searchHeading2,
              'searchPassage': this.searchPassage
            });
          }
        },
        err => {
          this.openAlertDialog('Failed', err.error, 'error');
        });
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.passageList.length > 0) {
      this.getNextPage();
    }
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  addPassage = ($event) => {
    this.router.navigate(['new'], {relativeTo: this.activatedRoute});
  };

  editPassage = (row) => {
    this.router.navigate([row.id, 'edit'], {relativeTo: this.activatedRoute});
  };

  detailPassage = (row) => {
    this.router.navigate([row.id], {relativeTo: this.activatedRoute});
  };

  removePassage = () => {
    let toBeRemoved: PassageEntity[] = [];
    this.passageList.forEach(passage => {
      if (passage.isChecked) {
        toBeRemoved.push(passage);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please Select Data to Removed', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Data?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.lineTableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.lineTableComponent.isCheckedAll = false;
        this.passageService.removePassageList(toBeRemoved).subscribe(
            res => {
              this.openAlertDialog('Success', `Success Deleted.`, 'success');
              this.getNextPage();
            },
            err => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
            });
      }
    });
  };

  removePassageAll = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.title = 'WARNING!';
    ref.componentInstance.message = 'Do you want to remove All Data?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;

        this.pageParam.pageIndex = 0;

        this.lineTableComponent.isCheckedAll = false;

        // this.passageService.removePassageListBySkillId(this.selectedSkill).subscribe(
        //   res => {
        //     this.openAlertDialog('Success', `Success Deleted.`, 'success');
        //     this.getNextPage();
        //   },
        //   err => {
        //     this.pageParam.pageIndex = originalPageIndex;
        //     this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
        //   });
      }
    });
  };

  // 다운로드 함수
  downloadFile = (type: string) => {
    let url = environment.maumAiApiUrl + '/mrc/passage/' + this.workspaceId;
    if (type === 'passage') {
      url += '/download-passage-set';
    } else if (type === 'learning') {
      url += '/download-learning-set';
    } else if (type === 'pqa-xlsx') {
      url += '/download-pqa-xlsx';
    } else {
      console.log('Unknown download type: ' + type);
      return;
    }

    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  confirmRisk(what: string) {
    this[what === 'merge' ? 'merger' : 'uploader'].preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.title = 'WARNING!';
      ref.componentInstance.message = ((type) => {
        switch (type) {
          case 'merge':
            return 'If you merge a file,<br> Some data may get defective.<br>Continue?';
          case 'upload':
          default:
            return 'If you upload a file,<br> All data will be overwritten with data of the file.<br>Continue?';
        }
      })(what);
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

  confirmUpload() {
    this.confirmRisk('upload');
  }

  confirmMerge() {
    this.confirmRisk('merge');
  }

  goQAs() {
    this.router.navigate(['qas'], {relativeTo: this.activatedRoute});
  }
}
