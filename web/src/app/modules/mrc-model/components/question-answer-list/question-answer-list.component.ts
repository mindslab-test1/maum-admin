import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  ErrorStateMatcher,
  MatButtonToggleGroup,
  MatDialog,
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {QuestionService} from '../../services/question.service';
import {QuestionAnswerDTO} from '../../dto/question-answer.dto';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {
  AlertComponent, FormErrorStateMatcher, TableComponent,
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-mrc-question-answer-list',
  templateUrl: './question-answer-list.component.html',
  styleUrls: ['./question-answer-list.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class QuestionAnswerListComponent implements OnInit, AfterViewInit, OnDestroy {
  workspaceId: string;

  pageParam: MatPaginator;
  pageLength: number;
  searchQA = '';
  searchHeading1 = '';
  searchHeading2 = '';
  summary = '';
  tableKind = '';

  dataSource: MatTableDataSource<any>;
  questionAnswerParam: QuestionAnswerDTO;
  @ViewChild(MatButtonToggleGroup) tableKindToggle: MatButtonToggleGroup;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('questionTemplate') questionTemplate: TemplateRef<any>;
  header = [];
  passageList: any[] = [];

  private _onDestroy = new Subject<void>();

  constructor(private questionService: QuestionService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private dialog: MatDialog,
              private cdr: ChangeDetectorRef,
              public matcher: FormErrorStateMatcher,
              private storage: StorageBrowser,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.header = [
      // {attr: 'no', name: 'No', no: false},
      {attr: 'headings', name: 'Headings', isSort: false, width: '20%'},
      {
        attr: 'question',
        name: 'Question',
        width: '20%',
        isSort: false,
        template: this.questionTemplate
      }, ///////////////////////////////////
      {attr: 'answer', name: 'Answer', width: '60%', isSort: false},
      {
        attr: 'action',
        name: 'Action',
        type: 'text',
        isButton: true,
        buttonName: 'Edit',
        width: '7%'
      },
    ];
  }

  ngAfterViewInit() {
    this.questionAnswerParam = new QuestionAnswerDTO();
    let qasInfo = this.storage.get('MrcQAsInfo');
    if (qasInfo && this.workspaceId === qasInfo.workspaceId) {
      this.tableKind = qasInfo.tableKind;
      this.pageParam.pageIndex = qasInfo.pageIndex;
      this.pageParam.pageSize = qasInfo.pageSize;
      this.searchHeading1 = qasInfo.searchHeading1;
      this.searchHeading2 = qasInfo.searchHeading2;
      this.searchQA = qasInfo.searchQA;
    } else {
      this.tableKind = 'qas';
      this.pageParam.pageIndex = 0;
      this.pageParam.pageSize = 10;
    }
    this.activatedRoute.queryParams.subscribe(
        query => {
          this.getNextPage();
          this.tableKindToggle.value = this.tableKind;
          this.store.dispatch({type: ROUTER_LOADED});
        });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  searchKeydown(event) {
    if (event.keyCode === 13) {
      this.onClickSearchButton(0);
    }
  }

  /**
   * Search Button 정의
   * @param  pageIndex
   */
  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  onClickResetButton() {
    this.resetTo(this.tableKind);
    this.getNextPage();
  }

  resetTo(kind: string) {
    this.tableKind = kind;
    this.pageParam.pageIndex = 0;
    this.searchQA = '';
    this.searchHeading1 = '';
    this.searchHeading2 = '';
  }

  setParams() {
    this.questionAnswerParam.pageIndex = this.pageParam.pageIndex;
    this.questionAnswerParam.pageSize = this.pageParam.pageSize;
    this.questionAnswerParam.workspaceId = this.workspaceId;
    this.questionAnswerParam.majorHeading = this.searchHeading1;
    this.questionAnswerParam.searchHeading = this.searchHeading2;
    this.questionAnswerParam.searchQA = this.searchQA;
    this.questionAnswerParam.doValidation = this.tableKind === 'invalid-answers';
  }

  setResponse(res) {
    this.summary = res.summary;
    this.passageList = res.list.content;
    this.pageLength = res.list.totalElements;
    this.dataSource = new MatTableDataSource(this.passageList);
    this.lineTableComponent.isCheckedAll = false;
  }

  storeMrcQAsInfo() {
    this.storage.set('MrcQAsInfo', {
      'tableKind': this.tableKind,
      'pageIndex': this.pageParam.pageIndex,
      'pageSize': this.pageParam.pageSize,
      'workspaceId': this.workspaceId,
      'searchHeading1': this.searchHeading1,
      'searchHeading2': this.searchHeading2,
      'searchQA': this.searchQA
    });
  }

  search(matSort?: MatSort) {
    if (matSort) {
      if (!this.questionAnswerParam) {
        return;
      }
      // this.questionAnswerParam.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'asc' : matSort.direction;
      // this.questionAnswerParam.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    this.cdr.detectChanges();
    this.setParams();
    this.questionService.getQuestionAnswerList(this.questionAnswerParam).subscribe(res => {
          if (res) {
            this.setResponse(res);
            this.storeMrcQAsInfo();
          }
        },
        err => {
          this.openAlertDialog('Failed', err.error, 'error');
        });
  }

  changeTableKind = (tableKind) => {
    if (this.tableKind !== tableKind) {
      this.resetTo(tableKind);
      this.getNextPage();
    }
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.passageList.length > 0) {
      this.getNextPage();
    }
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  editPassage = (row) => {
    this.router.navigate(['../', row.passageId, 'edit'], {relativeTo: this.activatedRoute});
  };

  detailPassage = (row) => {
    this.router.navigate(['../', row.passageId], {relativeTo: this.activatedRoute});
  };

  goList() {
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }
}
