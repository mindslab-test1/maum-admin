import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MrcModelRoutingModule} from './mrc-model-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {PassageDetailComponent} from './components/passage-detail/passage-detail.component';
import {PassageListComponent} from './components/passage-list/passage-list.component';
import {PassageUpsertComponent} from './components/passage-upsert/passage-upsert.component';
import {QuestionAnswerListComponent} from './components/question-answer-list/question-answer-list.component';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressBarModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {PassageService} from './services/passage.service';
import {QuestionService} from './services/question.service';

@NgModule({
  declarations: [
    PassageDetailComponent,
    PassageListComponent,
    PassageUpsertComponent,
    QuestionAnswerListComponent
  ],
  imports: [
    CommonModule,
    MrcModelRoutingModule,
    MatCardModule,
    SharedModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatInputModule,
    MatListModule
  ],
  providers: [
    PassageService,
    QuestionService
  ]
})

export class MrcModelModule {
}
