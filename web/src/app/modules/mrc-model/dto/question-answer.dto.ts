import {PageParameters} from '../../../shared/entities/pageParameters.entity';

export class QuestionAnswerDTO extends PageParameters {

  questionId: string;
  workspaceId: string;
  passageId: string;
  question: string;
  isAnchor: boolean;
  answer: string;

  headings: string;

  searchQA: string
  majorHeading: string;
  searchHeading: string;

  doValidation: boolean;

  constructor() {
    super();
    this.doValidation = false;
  }
}
