import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProcessMonitorRoutingModule} from './process-monitor-routing.module';
import {ServersComponent} from './components/servers/servers.component';
import {SharedModule} from '../../shared/shared.module';
import {ProcessesComponent} from './components/processes/processes.component';
import {ProcessDetailComponent} from './components/process-detail/process-detail.component';
import {MatIconModule, MatSidenavModule, MatTabsModule} from '@angular/material';

@NgModule({
  declarations: [ServersComponent, ProcessesComponent, ProcessDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ProcessMonitorRoutingModule,
    SharedModule,
    MatTabsModule,
    MatSidenavModule,
    MatIconModule
  ]
})
export class ProcessMonitorModule {
}
