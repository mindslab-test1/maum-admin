import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {TableComponent} from 'app/shared';


@Component({
  selector: 'app-process-total',
  templateUrl: './process-detail.component.html',
  styleUrls: ['./process-detail.component.scss']
})

/**
 * ProcessTotal Component
 */
export class ProcessDetailComponent implements OnInit {

  tabObj = {
    tabLabel: undefined,
    tabName: undefined
  };

  newSvdgList = [];

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header: any[] = [];
  filterKeyword: string;

  @Output() tabClick = new EventEmitter();
  @ViewChild('tableComponent') tableComponent: TableComponent;

  constructor() {
  }

  ngOnInit() {
    this.header = [
      {attr: 'no', name: 'No', isSort: true},
      {attr: 'host', name: 'Host', isSort: true},
      {attr: 'ip', name: 'IP', isSort: true},
      {attr: 'module', name: 'Module', isSort: true},
      {attr: 'state', name: 'State', isSort: true},
      {attr: 'startAt', name: 'Start Time', isSort: true},
      {attr: 'endAt', name: 'End Time', isSort: true},
      {attr: 'errLogFile', name: 'Error Log File', isSort: true},
    ];
  }

  /**
   * 탭 클릭시, 탭의 정보를 다른 component로 넘겨준다.
   * @param event
   */
  onTabClick(event) {
    if (event.tab) {
      this.tabObj.tabLabel = event.tab.textLabel;
    }
    this.tabClick.emit();
  }
}
