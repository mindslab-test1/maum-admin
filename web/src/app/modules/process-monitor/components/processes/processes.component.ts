import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';
import {GrpcApi} from 'app/core/sdk';
import {ProcessDetailComponent} from '../process-detail/process-detail.component';
import {MatDialog} from '@angular/material';
import {ErrorDialogComponent} from '../../../../shared';


@Component({
  selector: 'app-view-processes',
  templateUrl: './processes.component.html',
  styleUrls: ['./processes.component.scss'],
})

/**
 * Prcoeeses View Component
 */
export class ProcessesComponent implements OnInit {
  flag = true;
  Handler: any = null;

  svdg = [
    {
      group: 'group1',
      value:
        [
          {
            process: 'hzc',
            detail: {
              cnt: 0,
              name: 'm2u-hzc',
              error: false,
              svd_proc_list: []
            }
          },
          {
            process: 'pool',
            detail: {
              cnt: 0,
              name: 'm2u-pool',
              error: false,
              svd_proc_list: []
            }
          }
        ],
      names: ['IMDG', 'POOL']
    },
    {
      group: 'group2',
      value:
        [
          {
            process: 'map',
            detail: {
              cnt: 0,
              name: 'm2u-map',
              error: false,
              svd_proc_list: []
            }
          },
          {
            process: 'rest',
            detail: {
              cnt: 0,
              name: 'm2u-rest',
              error: false,
              svd_proc_list: []
            }
          },
          {
            process: 'stt',
            detail: {
              cnt: 0,
              name: 'brain-stt',
              error: false,
              svd_proc_list: []
            }
          },
          {
            process: 'tts',
            detail: {
              cnt: 0,
              name: 'm2u-tts',
              error: false,
              svd_proc_list: []
            }
          },
          {
            process: 'auth',
            detail: {
              cnt: 0,
              name: 'm2u-mapauth',
              error: false,
              svd_proc_list: []
            }
          },
          {
            process: 'ir',
            detail: {
              cnt: 0,
              name: 'm2u-mapauth',
              error: false,
              svd_proc_list: []
            }
          }
        ],
      names: ['MAP', 'REST', 'STT', 'TTS', 'AUTH', 'IR']
    },
    {
      group: 'group3',
      value: [
        {
          process: 'router',
          detail: {
            cnt: 0,
            name: 'm2u-router',
            error: false,
            svd_proc_list: []
          }
        },
        {
          process: 'front',
          detail: {
            cnt: 0,
            name: 'm2u-front',
            error: false,
            svd_proc_list: []
          }
        }
      ],
      names: ['ROUTER', 'FRONT']
    },
    {
      group: 'group4',
      value: [
        {
          process: 'itfm',
          detail: {
            cnt: 0,
            name: 'm2u-itfm',
            error: false,
            svd_proc_list: []
          }
        }
      ],
      names: ['ITFM']
    },
    {
      group: 'group5',
      value: [
        {
          process: 'dam',
          detail: {
            cnt: 0,
            name: 'm2u-dam',
            error: false,
            svd_proc_list: []
          }
        }
      ],
      names: ['DAM']
    },
    {
      group: 'group6',
      value: [
        {
          process: 'm2u-sds',
          detail: {
            cnt: 0,
            name: 'm2u-sds',
            error: false,
            svd_proc_list: []
          }
        }
      ],
      names: ['SDS']
    },
    {
      group: 'group7',
      value: [
        {
          process: 'admin',
          detail: {
            cnt: 0,
            name: 'm2u-svcadm',
            error: false,
            svd_proc_list: []
          }
        },
        {
          process: 'admrest',
          detail: {
            cnt: 0,
            name: 'm2u-admrest',
            error: false,
            svd_proc_list: []
          },
        },
        {
          process: 'logger',
          detail: {
            cnt: 0,
            name: 'm2u-logger',
            error: false,
            svd_proc_list: []
          }
        }
      ],
      names: ['SVC-ADMIN', 'ADMIN-REST', 'LOGGER']
    }
  ];

  errTable = [];
  svdList = [];

  @ViewChild('totalProcess') private totalProcess: ProcessDetailComponent;

  constructor(private store: Store<any>,
              private grpc: GrpcApi,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getSupervisorServerGroupList();
    this.totalProcess.tabObj = {
      tabLabel: 'FRONT',
      tabName: 'm2u-front'
    };
  }

  /**
   * server의 Process정보를 조회해온 후, Group별로 테이블에 데이터를 넣어준다.
   */
  getSupervisorServerGroupList() {
    this.grpc.getSupervisorServerGroupList().subscribe(
      result => {
        if (this.flag) {
          this.flag = false;
          this.svdList = result.svd_svr_grp_list;
          this.store.dispatch({type: ROUTER_LOADED});
        }

        this.errTable = [];

        // set 'svdg' using result
        this.setNewProcessList(this.svdg, result);
        this.totalProcess.newSvdgList = this.svdg;

        // set 'thisProcess' to selected process
        let thisProcess = this.svdg
        .find(item =>
          item.group === this.totalProcess.tabObj.tabLabel);

        setTimeout(() => {
          if (thisProcess) {
            this.totalProcess.rows =
              this.errTable.filter(row => row.group === this.totalProcess.tabObj.tabLabel);
            this.cdr.detectChanges();
          }
        }, 0);
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = 'Something went wrong. [ ' + err.message + '(' + err.code + ') ]';
        clearInterval(this.Handler);
      }
    );
  }

  /**
   * uptime 시간 값만 가져오기 위한 함수
   * @param description => pid 값과, uptime 정보를 가지고 있다.
   * @returns {string} => uptime 시간 값만 따로 반환한다.
   */
  calcUptime(description): string {
    let uptimeStr = 'uptime ';
    let uptime = '-';
    let index = description.indexOf(uptimeStr);
    if (index !== -1) {
      index += uptimeStr.length;
      uptime = description.substring(index);
    }
    return uptime;
  }

  /**
   * this.svdg 와 Error 테이블에 데이터를 세팅한다.
   * @param totalProcess => {this.svdg} => {Group별 Process 목록}
   * @param result => grpc.getSupervisorServerGroupList() 의 response 값
   */
  setNewProcessList(totalProcess, result) {
    totalProcess.forEach(group => {
      group.value.forEach(process => {
        process.cnt = 0;
        process.error = false;
        process.svd_proc_list = [];

        result.svd_svr_grp_list.forEach(svdGroup => {
          let serverInfo = {
            grp_name: undefined,
            host: undefined,
            ip: undefined,
            port: undefined
          };

          let processInfo: {
            description: any,
            exitstatus: any,
            group: any,
            logfile: any,
            name: any,
            alias: any,
            now: any,
            pid: any,
            spawnerr: any,
            start: any,
            state: any,
            stderr_logfile: any,
            stdout_logfile: any,
            stop: any,
            uptime: any
          };

          // set processInfo
          processInfo = svdGroup.svd_proc_list.find(
            item => item.name === process.detail.name
          );

          if (processInfo) {
            // set serverInfo
            serverInfo.grp_name = svdGroup.grpc_name;
            serverInfo.host = svdGroup.host;
            serverInfo.ip = svdGroup.ip;
            serverInfo.port = svdGroup.port;

            // set uptime
            processInfo.uptime = this.calcUptime(processInfo.description);
            process.svd_proc_list.push({
              serverInfo: serverInfo,
              processInfo: processInfo
            });

            if (processInfo.state !== 20) {
              process.error = true;
              this.insertErrTable(process, group);
            }
            process.cnt++;
          }
        });
      });

    });
  }

  /**
   * ErrorTable에 값을 세팅하는 함수
   * @param {number} no => 접속한 서버의 고유 seq 값
   * @param {string} moduleName => module 이름
   * @param process => 프로세스 정보
   * @param group => Group 정보
   * @returns {any} => error 테이블의 row 에 들어갈 데이터 반환
   */
  errArgConstructor(no: number, moduleName: string, process: any, group: any): any {
    let errArg: any = {
      no: undefined,
      module: undefined,
      state: undefined,
      startAt: undefined,
      endAt: undefined,
      errLog: undefined,
      ip: undefined,
      host: undefined,
      group: undefined
    };
    errArg.no = no;
    errArg.module = moduleName;
    errArg.state = this.setStateOfErr(process.processInfo.state);
    errArg.startAt = process.processInfo.description;
    errArg.endAt = '...';
    errArg.errLogFile = process.processInfo.stderr_logfile;
    errArg.ip = process.serverInfo.ip;
    errArg.host = process.serverInfo.host;
    errArg.group = group.group;
    return errArg;
  };

  /**
   * ErrorCode 숫자에 따라, 문자 데이터 반환
   * @param {number} errCode => ErrorCode number 형태
   * @returns {string} => number 값에 해당하는 문자열 반환
   */
  setStateOfErr(errCode: number): string {
    switch (errCode) {
      case 0 :
        return 'STOPPED';
      case 10 :
        return 'STARTING';
      case 20 :
        return 'RUNNING'; // not using
      case 30 :
        return 'BACKOFF';
      case 40 :
        return 'STOPPING';
      case 100 :
        return 'EXITED';
      case 200 :
        return 'FATAL';
      case 1000 :
        return 'UNKNOWN';
      default:
        return '';
    }
  }

  /**
   * Error테이블의 데이터를 추가하는 함수
   * @param theProcess => 하나의 process 객체
   * @param group => process가 속해있는 groups 객체
   */
  insertErrTable(theProcess: any, group: any): void {
    let no = 0;
    theProcess.svd_proc_list.forEach(
      process => {
        no++;
        if (process && process.processInfo.state !== 20) {
          let errArg =
            this.errArgConstructor(no, theProcess.process, process, group);

          // check if there is same errArg in 'errTable'
          let ifSameErr = this.errTable.find(err => (
            err.ip === errArg.ip &&
            err.host === errArg.host &&
            err.module === errArg.module)
          );
          if (!ifSameErr) {
            this.errTable.push(errArg);
          }
        }
      }
    );
  }
}
