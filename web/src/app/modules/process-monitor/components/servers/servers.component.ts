import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {Store} from '@ngrx/store';
import {GrpcApi} from '../../../../core/sdk/services/custom/Grpc';
import {ROUTER_LOADED} from '../../../../core/actions';
import {ErrorDialogComponent, TableComponent} from '../../../../shared';
import {getErrorString} from '../../../../shared/values/error-string';
import {FormControl, FormBuilder} from '@angular/forms';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-view-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss']
})
/**
 * Servers View Component
 */
export class ServersComponent implements OnInit {

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header: any[] = [];
  subscription = new Subscription();
  filterKeyword: FormControl;

  @ViewChild('tableComponent') tableComponent: TableComponent;

  constructor(private store: Store<any>,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private fb: FormBuilder,
              private grpc: GrpcApi) {
  }

  ngOnInit() {
    this.getSupervisorServerGroupList();
    this.header = [
      {attr: 'host', name: 'Host', isSort: true},
      {attr: 'ip', name: 'IP', isSort: true},
    ];

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );

    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * Grpc로 현재 접속중인 서버의 정보를 가져온다.
   */
  getSupervisorServerGroupList() {
    this.grpc.getSupervisorServerGroupList().subscribe(
      res => {
        this.rows = res.svd_svr_grp_list;
        this.cdr.detectChanges();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Something went wrong. [${getErrorString(err)}]`;
      });
  }


}
