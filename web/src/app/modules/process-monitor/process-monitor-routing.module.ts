import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ServersComponent} from './components/servers/servers.component';
import {AuthService} from '../../core';
import {ProcessesComponent} from './components/processes/processes.component';

const routes: Routes = [
  {
    // sideNav를 선택하면 /monitoring 로 이동합니다
    // /monitoring/server 으로 redirectTo 를 해줍니다
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'server',
      },
      { // START server
        path: 'server',
        component: ServersComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ,
            ]
          },
          nav: {
            name: 'Servers',
            comment: 'Server 목록 보기',
            icon: 'computer',
          }
        }
      },
      { // START process
        path: 'process',
        component: ProcessesComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ,
            ]
          },
          nav: {
            name: 'Processes',
            comment: 'Process 목록 보기',
            icon: 'view_module',
          },
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessMonitorRoutingModule {
}
