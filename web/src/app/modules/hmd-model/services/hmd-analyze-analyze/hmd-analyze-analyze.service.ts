import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HmdAnalyzeEntity} from '../../entity/hmd-analyze/hmd-analyze.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class HmdAnalyzeService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  startAnalyze(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/startAnalyze', hmdAnalyzeEntity);
  }

  getFileGroupList(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/getFileGroupList', hmdAnalyzeEntity);
  }

  getFileList(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/getFileList', hmdAnalyzeEntity);
  }

  getHMDDicList(hmdAnalyzeEntity: HmdAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/analyze/getHMDDicList', hmdAnalyzeEntity);
  }
}
