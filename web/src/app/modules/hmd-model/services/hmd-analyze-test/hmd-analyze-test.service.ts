import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HmdAnalyzeTestEntity} from '../../entity/hmd-analyze-test/hmd-analyze-test.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class HmdAnalyzeTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getHMDDicList(hmdAnalyzeTestEntity: HmdAnalyzeTestEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/test/getHMDDicList', hmdAnalyzeTestEntity);
  }

  getModel(name: string, workspaceId: string): Observable<any> {
    let param: HmdAnalyzeTestEntity = new HmdAnalyzeTestEntity;
    param.name = name;
    param.workspaceId = workspaceId;
    return this.http.post(this.API_URL + '/ta/hmd/test/getModel/', param);
  }

  getHMDAnalyzeTestResultList(hmdAnalyzeTestEntity: HmdAnalyzeTestEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/hmd/test/getHMDAnalyzeTestResultList', hmdAnalyzeTestEntity);
  }
}
