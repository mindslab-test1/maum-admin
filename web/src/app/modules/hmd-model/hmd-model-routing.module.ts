import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HmdModelsComponent} from './components/hmd-models/hmd-models.component';
import {HmdModelDetailComponent} from './components/hmd-detail/hmd-model-detail.component';
import {HmdAnalysisTestComponent} from './components/hmd-analysis-test/hmd-analysis-test.component';
import {HmdAnalysisAnalyzeComponent} from './components/hmd-analysis-analyze/hmd-analysis-analyze.component';
import {HmdAnalysisAnalyzeResultComponent} from './components/hmd-analysis-analyze-result/hmd-analysis-analyze-result.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        // sideNav를 선택하면 /dashboard 로 이동합니다
        path: '',
        component: HmdModelsComponent,
      },
      {
        // sideNav를 선택하면 /dashboard 로 이동합니다
        path: 'new',
        component: HmdModelDetailComponent,
      },
      {
        path: ':id/test',
        component: HmdAnalysisTestComponent,
      },
      {
        path: ':id/analysis-result',
        component: HmdAnalysisAnalyzeResultComponent,
      },
      {
        path: ':id/analysis',
        component: HmdAnalysisAnalyzeComponent,
      },
      {
        path: ':id/edit',
        component: HmdModelDetailComponent,
      },
      {
        path: ':id',
        component: HmdModelDetailComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HmdModelRoutingModule {
}
