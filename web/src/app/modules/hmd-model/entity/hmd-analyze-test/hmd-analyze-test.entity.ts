import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class HmdAnalyzeTestEntity extends PageParameters {

  id: string;

  // Workspace
  workspaceId: string;

  // FileGroup
  // HMDResult
  fileGroup: string;

  // HMDDic
  name: string;
  hmdKey: string;

  // File
  // for analyze HMD Test
  textData: string;
}
