import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class HmdAnalyzeResultEntity extends PageParameters {

  id: string;

  workspaceId: string;

  // HMDResult
  dictionary: string;
  model: string;
  fileGroup: string;
  sentence: string;
  category: string;
  rule: string;

  constructor() {
    super();
    this.dictionary = '';
    this.model = '';
    this.fileGroup = '';
    this.sentence = '';
    this.category = '';
    this.rule = '';
  }


}
