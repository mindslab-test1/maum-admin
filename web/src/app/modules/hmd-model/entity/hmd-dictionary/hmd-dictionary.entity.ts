import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class HmdDictionaryEntity extends PageParameters {

  id: string;

  // HMDDic
  applied: string;

  // HMDCategory
  hmdDicId: string;
  parCatId: string;
  name: string;
  createAt: string;
  updateAt: string;
  creatorId: string;
  updaterId: string;
  count: number;

  // HMDDicLine
  workspaceId: string;
  seq: number;
  sentence: string;
  category: string;
  versionId: string;


  // 화면에서 쓰이는 변수
  rule: string;
  duplicateCount: number;

  // update 구분에 사용되는 변수
  oldName: string;
  updateState: string;
  title: string;
  message: string;

  createdAt: Date;
  updatedAt: Date;
  hmdUse: boolean;
  dnnUse: boolean;
  dnnDicId: string;
  manDesc: string;

}
