import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  AlertComponent,
  ConfirmComponent,
  DownloadService,
  TableComponent
} from '../../../../shared';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {HmdAnalyzeResultService} from '../../services/hmd-analyze-result/hmd-analyze-result.service';
import {ROUTER_LOADED} from '../../../../core/actions';
import {HmdAnalyzeResultEntity} from '../../entity/hmd-analyze-result/hmd-analyze-result.entity';

@Component({
  selector: 'app-hmd-analysis-analyze-result',
  templateUrl: './hmd-analysis-analyze-result.component.html',
  styleUrls: ['./hmd-analysis-analyze-result.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HmdAnalysisAnalyzeResultComponent implements OnInit {
  @ViewChild('tableComponent') tableComponent: TableComponent;

  modelName: string;
  workspaceId: string;
  inputSentence: string;
  inputCategory: string;

  actions: any;
  header = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'model', name: 'Model', width: '10%', isSort: true},
    {attr: 'fileGroup', name: 'File Group', width: '10%', isSort: true, hidden: true},
    {attr: 'sentence', name: 'Sentence'},
    {attr: 'category', name: 'Category', width: '10%', isSort: true},
    {attr: 'probability', name: 'Probability', isSort: true},
    {attr: 'createdAt', name: 'Created at', width: '10%', format: 'date', isSort: true},
  ];
  matSort: MatSort;
  hmdResults: HmdAnalyzeResultEntity[] = [];
  dataSource: MatTableDataSource<any>;
  pageLength: number;
  pageParam: MatPaginator;
  param: HmdAnalyzeResultEntity;

  constructor(private storage: StorageBrowser,
              private hmdResultService: HmdAnalyzeResultService,
              private downloadService: DownloadService,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      this.modelName = par['id'];
      if (!this.modelName || this.modelName === '') {
        this.back('Error. Check your model name in a url.');
      }
      this.workspaceId = this.storage.get('m2uWorkspaceId');
    });
    this.actions = [
      {
        type: 'download',
        text: 'Download',
        icon: 'file_download',
        callback: this.download,
        disabled: true,
      },
      {
        type: 'clearResults',
        text: 'Clear Results',
        icon: 'delete_sweep',
        callback: this.clear,
        disabled: true,
      },
    ];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.getAllHmdResults();
  }

  search(matSort?: MatSort) {
    this.matSort = matSort;

    if (this.hmdResults.length > 0) {
      this.getAllHmdResults();
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  download = () => {
    this.downloadService.downloadCsvFile(this.tableComponent.rows, this.header, 'result');
  };

  clear = () => {
    this.dialog.open(ConfirmComponent).afterClosed().subscribe(
      result => {
        if (!result) {
          return false;
        } else {
          if (this.hmdResults.length > 0) {
            this.hmdResultService.deleteByHmdAnalyzeResultEntity(this.param).subscribe(
              res => {
                this.openAlertDialog('Delete', `Delete success. ${res['deleteCount']} deleted.`, 'success');
                this.getAllHmdResults();
              },
              () => {
                this.openAlertDialog('Failed', 'Error. delete failed.', 'error');
              });
          }
        }
      });
  };

  setPage(page?: MatPaginator) {
    this.pageParam = page;

    if (this.hmdResults.length > 0) {
      this.getAllHmdResults();
    }
  }

  getAllHmdResults(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.param = new HmdAnalyzeResultEntity();
    this.param.workspaceId = this.workspaceId;
    this.param.model = this.modelName;
    if (this.inputSentence === '') {
      this.param.sentence = null;
    } else {
      this.param.sentence = this.inputSentence;
    }
    this.param.category = this.inputCategory;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    this.param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.hmdResultService.getHmdAnalyzeResult(this.param).subscribe(
      res => {
        this.pageLength = res['totalElements'];
        this.hmdResults = res['content'];
        this.tableComponent.isCheckedAll = false;
      },
      () => {
        this.openAlertDialog('Failed', 'Error. failed fetch analysis result list', 'error');
      });
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.modelName === '') {
      this.router.navigate(['..', '..', '..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..', 'analysis'], {relativeTo: this.route});
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
