import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HmdAnalysisAnalyzeResultComponent} from './hmd-analysis-analyze-result.component';

describe('HmdAnalysisAnalyzeResultComponent', () => {
  let component: HmdAnalysisAnalyzeResultComponent;
  let fixture: ComponentFixture<HmdAnalysisAnalyzeResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HmdAnalysisAnalyzeResultComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HmdAnalysisAnalyzeResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
