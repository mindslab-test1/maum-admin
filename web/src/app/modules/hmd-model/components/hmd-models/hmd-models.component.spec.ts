import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HmdModelsComponent} from './hmd-models.component';

describe('HmdModelsComponent', () => {
  let component: HmdModelsComponent;
  let fixture: ComponentFixture<HmdModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HmdModelsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HmdModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
