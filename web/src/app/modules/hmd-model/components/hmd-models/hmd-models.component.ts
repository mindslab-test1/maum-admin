import {
  Component, OnInit, OnDestroy, ViewChild, TemplateRef, ChangeDetectorRef, Inject,
  AfterViewInit, ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {MatDialog, MatSnackBar, MatPaginator, MatSort} from '@angular/material';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';
import {
  AppObjectManagerService,
  TableComponent
} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {AlertComponent, ConfirmComponent} from 'app/shared';
import {ModelManagementEntity} from '../../../dnn-cl-model/entity/model-management/model-management.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {ModelInsertService} from '../../../dnn-cl-model/services/model-insert/model-insert.service';

@Component({
  selector: 'app-hmd-dictionary-view',
  templateUrl: './hmd-models.component.html',
  styleUrls: ['./hmd-models.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ModelInsertService],
})

export class HmdModelsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;

  page_title: any; // = 'HMD Managers';
  page_description: any; // = 'Add HMD and update information about HMD.';

  inputName: string;
  inputDescription: string;
  deleteModel: any[] = [];

  actions: any;
  table: any;

  panelToggleText: string;
  workspaceId: string;

  // subscription = new Subscription();

  // filterKeyword: FormControl;

  pageParam: MatPaginator;
  sort: MatSort;
  data: any[] = [];
  length: number;

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header = [];

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private grpc: GrpcApi,
              private consoleUserApi: ConsoleUserApi,
              private objectManager: AppObjectManagerService,
              private cdr: ChangeDetectorRef,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private modelService: ModelInsertService) {
  }

  ngOnInit() {
    // TODO:// 권한 관련 내용인듯
    // let id = 'hmd';
    // let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = [
      {type: 'add', text: 'Add', icon: 'add_circle_outline', callback: this.add},
      {type: 'delete', text: 'Delete', icon: 'delete_circle_outline', callback: this.delete},
    ];

    this.workspaceId = this.storage.get('m2uWorkspaceId');

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true, width: '5%'},
      {attr: 'no', name: 'No.', no: true, width: '5%'},
      {attr: 'name', name: 'Name', isSort: true, onClick: this.navigateHmdDetailView, width: '15%'},
      {attr: 'manDesc', name: 'Description', isSort: true, width: '15%'},
      {attr: 'cnt', name: 'Changed Count', width: '10%', isSort: true, onClick: this.countClick},
      {attr: 'action', name: 'Action', width: '7%', isButton: true, buttonName: 'Edit'},
      {attr: 'hmdDicId', name: 'hmdDicId', hidden: true},
    ];

  }

  ngAfterViewInit() {
    this.getList();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  editModel(data) {
    this.objectManager.set('hmd', data);
    this.router.navigate([data.name, 'edit'], {relativeTo: this.route});
  }

  countClick: any = (data: any) => {
    this.router.navigate(['../../histories/hmd', data.name, data.hmdDicId], {relativeTo: this.route.parent});
  };

  setSort(matSort?: MatSort) {
    this.sort = matSort;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  getpaginator(page?: MatPaginator) {
    this.pageParam = page;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  getList(pageIndex?) {
    let model: ModelManagementEntity = new ModelManagementEntity();
    this.cdr.detectChanges();
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    model.pageSize = this.pageParam.pageSize;
    model.pageIndex = this.pageParam.pageIndex;
    model.orderDirection = this.sort.direction === '' ||
    this.sort.direction === undefined ? 'desc' : this.sort.direction;
    model.orderProperty = this.sort.active === '' ||
    this.sort.active === undefined ? 'createdAt' : this.sort.active;
    model.name = this.inputName;
    model.manDesc = this.inputDescription;
    model.workspaceId = this.storage.get('m2uWorkspaceId');
    model.hmdUse = true;

    this.modelService.getHmdOrDnnWithCommitHistory(model).subscribe(res => {
      if (res) {
        let result: any[] = res.map(x => {
          x.entity.cnt = x.cnt.toString();
          return x.entity;
        });
        this.data = result;
        this.length = result.length;
      } else {
        this.length = 0;
      }
      this.tableComponent.isCheckedAll = false;
      /*this.data = res.content;
      this.tableComponent.isCheckedAll = false;
      this.length = res.totalElements;*/
    });
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  /** action callbacks **/
    // list에서 add 버튼 클릭시
  add: any = () => {
    this.objectManager.clean('hmd');
    this.router.navigate(['new'], {relativeTo: this.route});
  };

  // list에서 delete 버튼 클릭시
  delete: any = () => {
    this.deleteModel.length = 0;
    this.data.forEach(items => {
      if (items.isChecked) {
        this.deleteModel.push({
          id: items.id,
          hmdDicId: items.hmdDicId
        });
      }
    });

    if (this.deleteModel.length === 0) {
      this.openAlertDialog('notice', 'Model has to be selected', 'notice');
      return null;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove models?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (this.deleteModel.length === this.tableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.modelService.deleteSelectedModels(this.deleteModel).subscribe(res => {
          if (res) {
            this.openAlertDialog('Success', 'Success Remove Model', 'success');
            this.getList();
          }
        }, () => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. remove models failed.', 'error');
        });
      }
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }


  // 상세페이지 이동
  navigateHmdDetailView: any = (row: any) => {
    this.objectManager.set('hmdData', row);
    // #@ 해당 hmd의 detail로 이동
    this.router.navigate([row.name], {relativeTo: this.route});
  };


  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }
}
