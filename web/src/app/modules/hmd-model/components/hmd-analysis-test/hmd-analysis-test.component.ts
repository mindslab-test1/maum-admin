import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {HmdAnalyzeTestService} from '../../services/hmd-analyze-test/hmd-analyze-test.service';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSort,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {AlertComponent, TableComponent} from '../../../../shared';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {HmdAnalyzeTestEntity} from '../../entity/hmd-analyze-test/hmd-analyze-test.entity';
import {ROUTER_LOADED} from '../../../../core/actions';
import {ModelInsertService} from '../../../dnn-cl-model/services/model-insert/model-insert.service';

@Component({
  selector: 'app-hmd-analysis-test',
  templateUrl: './hmd-analysis-test.component.html',
  styleUrls: ['./hmd-analysis-test.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [HmdAnalyzeTestService, ModelInsertService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }],

})
export class HmdAnalysisTestComponent implements OnInit {
  @ViewChild('tableComponent') tableComponent: TableComponent;
  // select
  workspace: any;
  workspaceId: any;
  loading = false;
  textData: any;
  length: number;
  dataSource: MatTableDataSource<any>;

  // table
  testResultData: any[] = [];
  header = [
    {attr: 'no', name: 'No', no: true, width: '2%'},
    {attr: 'sentence', name: 'Sentence', width: '60%'},
    {attr: 'rule', name: 'Rule', width: '30%'},
    {attr: 'category', name: 'Category', isSort: true, width: '8%'}
  ];

  selectedModel: HmdAnalyzeTestEntity = null;

  constructor(private hmdAnalyzeTestService: HmdAnalyzeTestService,
              private dialog: MatDialog,
              private modelService: ModelInsertService,
              private storage: StorageBrowser,
              private router: Router,
              private route: ActivatedRoute,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.selectedModel = new HmdAnalyzeTestEntity();
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.route.params.subscribe(par => {
      let name = par['id'];
      if (!name || name === '') {
        this.back('Error. Check your model name in a url.');
      }
      this.selectedModel.workspaceId = this.storage.get('m2uWorkspaceId');
      this.selectedModel.name = name;
      this.hmdAnalyzeTestService.getModel(name, this.selectedModel.workspaceId).subscribe(res => {
        if (!res) {
          this.back('Error. Check your model name in a url.');
        } else {
          this.selectedModel.name = res.name;
        }
      });
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  submit() {
    this.loading = true;
    this.getHMDAnalyzeTestResultList();
    this.loading = false;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  getHMDAnalyzeTestResultList() {
    this.testResultData.length = 0;
    this.selectedModel.textData = this.textData;
    this.hmdAnalyzeTestService.getHMDAnalyzeTestResultList(this.selectedModel).subscribe(result => {
        if (result) {
          this.openAlertDialog('Analyze', `Classified Sentence: ${result.success}<br> Not classified Sentence: ${result.fail}`, 'success');
          this.length = result['totalElement'];
          result.result.forEach((value) => {
            this.testResultData.push({
              id: value['id'],
              sentence: value['sentence'],
              rule: value['rule'],
              category: value['category'],
            })
          });
        } else {
          return false;
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Error. Failed Test', 'error');
      }
    );
  }

  orderByHeader(sort?: MatSort) {
    if (this.tableComponent.dataSource === undefined) {
      return 0;
    }
    const data = this.tableComponent.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.testResultData = data;
      return;
    }

    this.testResultData = data.sort((a, b) => {
      let isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'category':
          return this.compare(a.category, b.category, isAsc);
        default:
          return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.selectedModel.name === '') {
      this.router.navigate(['..', '..'], {relativeTo: this.route});
    } else {
      // 목록으로 이동
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }
}
