import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HmdAnalysisTestComponent} from './hmd-analysis-test.component';

describe('HmdAnalysisTestComponent', () => {
  let component: HmdAnalysisTestComponent;
  let fixture: ComponentFixture<HmdAnalysisTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HmdAnalysisTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HmdAnalysisTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
