import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {HmdAnalyzeResultService} from '../../services/hmd-analyze-result/hmd-analyze-result.service';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {ActivatedRoute, Router} from '@angular/router';
import {ModelInsertService} from '../../../dnn-cl-model/services/model-insert/model-insert.service';
import {MatDialog} from '@angular/material';
import {HmdAnalyzeService} from '../../services/hmd-analyze-analyze/hmd-analyze-analyze.service';
import {Store} from '@ngrx/store';
import {ModelManagementEntity} from '../../../dnn-cl-model/entity/model-management/model-management.entity';
import {ROUTER_LOADED} from '../../../../core/actions';
import {AlertComponent, ConfirmComponent} from '../../../../shared';
import {HmdAnalyzeEntity} from '../../entity/hmd-analyze/hmd-analyze.entity';

interface ModelInfo {
  id: string;
  type: string;
  name: string;
  workspaceId: string;
  creatorId: string;
}


@Component({
  selector: 'app-hmd-analysis-analyze',
  templateUrl: './hmd-analysis-analyze.component.html',
  styleUrls: ['./hmd-analysis-analyze.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ModelInsertService]
})
export class HmdAnalysisAnalyzeComponent implements OnInit {
  model: ModelInfo = {
    id: '',
    type: 'hmd',
    name: '',
    workspaceId: '',
    creatorId: '',
  };

  fileList: string[] = [];
  isTest: boolean;

  constructor(private storage: StorageBrowser,
              private hmdResultService: HmdAnalyzeResultService,
              private router: Router,
              private modelService: ModelInsertService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private hmdAnalyzeService: HmdAnalyzeService,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.route.params.subscribe(par => {
      let name = par['id'];
      if (!name || name === '') {
        this.back('Error. Check your model name in a url.');
      }
      let param: ModelManagementEntity = new ModelManagementEntity();
      param.hmdUse = true;
      param.dnnUse = false;
      param.workspaceId = this.storage.get('m2uWorkspaceId');
      param.name = name;
      this.modelService.getHmdOrDnnModel(param).subscribe(res => {
        if (res && res.hasOwnProperty('hmdDicId') && res.hmdDicId !== '') {
          let id = res.hmdDicId;
          this.model.id = id;
          this.model.workspaceId = this.storage.get('m2uWorkspaceId');
          this.model.creatorId = this.storage.get('user').id;
          this.model.name = name;
          this.model.type = 'hmd';
        } else {
          this.back('Error. Check your model name in a url.');
        }
      })
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.model.name === '') {
      this.router.navigate(['..', '..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }

  gotoResult() {
    this.router.navigate(['..', 'analysis-result'], {relativeTo: this.route});
  }

  selectFiles = (event) => {
    this.fileList = event;
    this.checkButton();
  };

  checkButton = () => {
    if (this.fileList.length > 0) {
      this.isTest = true;
    } else {
      this.isTest = false;
    }
  }

  test = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to start HMD model testing?';

    ref.afterClosed().subscribe(result => {
      if (result) {
        let param = new HmdAnalyzeEntity();
        param.workspaceId = this.model.workspaceId;
        param.dicName = this.model.name;
        param.fileList = this.fileList;
        param.creatorId = this.model.creatorId;

        this.hmdAnalyzeService.startAnalyze(param).subscribe(
          res => {
            this.openAlertDialog('Analyze', `complete analyze.`, 'success');
          },
          err => {
            this.openAlertDialog('Failed', 'Error. Failed to Analyze', 'error');
          });
      }
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
