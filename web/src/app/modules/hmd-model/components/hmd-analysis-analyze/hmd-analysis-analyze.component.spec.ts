import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HmdAnalysisAnalyzeComponent} from './hmd-analysis-analyze.component';

describe('HmdAnalysisAnalyzeComponent', () => {
  let component: HmdAnalysisAnalyzeComponent;
  let fixture: ComponentFixture<HmdAnalysisAnalyzeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HmdAnalysisAnalyzeComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HmdAnalysisAnalyzeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
