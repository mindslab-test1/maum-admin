import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HmdModelDetailComponent} from './hmd-model-detail.component';

describe('HmdModelDetailComponent', () => {
  let component: HmdModelDetailComponent;
  let fixture: ComponentFixture<HmdModelDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HmdModelDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HmdModelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
