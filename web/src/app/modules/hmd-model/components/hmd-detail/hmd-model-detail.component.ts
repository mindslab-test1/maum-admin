import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatPaginator,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material';
import {HmdDictionaryEntity} from '../../entity/hmd-dictionary/hmd-dictionary.entity';
import {HmdDictionaryService} from '../../services/hmd-dictionary/hmd-dictionary.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TreeViewComponent} from 'app/shared/components/tree/tree.component';
import {GitComponent} from 'app/shared/components/git/git.component';
import {
  AlertComponent,
  CommitDialogComponent,
  ConfirmComponent,
  TableComponent,
  AppObjectManagerService, UploaderButtonComponent
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {DictionaryCategoryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-category-upsert-dialog.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {ModelInsertService} from '../../services/model-insert/model-insert.service';

@Component({
  selector: 'app-hmd-detail',
  templateUrl: './hmd-model-detail.component.html',
  styleUrls: ['./hmd-model-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [HmdDictionaryService, ModelInsertService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher,
  }],
})
export class HmdModelDetailComponent implements OnInit {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('appTree') appTree: TreeViewComponent;
  @ViewChild('gitComponent') git: GitComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;

  selectSearchFormControl: FormControl = new FormControl();

  title = '';
  hmd: any;
  workspaceId: any;
  selectedDictionary = new HmdDictionaryEntity();
  actions: any[] = [];
  ruleActions: any[] = [];
  editable = false;
  tipsVisible = false;
  currentCategory: any = null;
  pageParam: MatPaginator;
  pageLength: number;
  inputData = null;
  searchRule = null;
  isCreated = false;
  model_actions: any = {};
  isNew: any = 'readOnly';
  isReadOnly: any = 'readOnly';
  infoGroup: any;
  hmdData: any;
  user = '';

  header = [];

  defaultHeader = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'category', name: 'Category', width: '15%'},
    {attr: 'rule', name: 'Rule', input: false, width: '55%'}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'category', name: 'Category', width: '15%'},
    {attr: 'rule', name: 'Rule', input: true, width: '55%'}
  ];

  hmdDictionaryLineList: any[] = [];
  data: any[] = [];
  dataSource: MatTableDataSource<any>;

  param: HmdDictionaryEntity = new HmdDictionaryEntity();
  categories: any = null;

  categoryCallback = {
    add: (next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.hmdDictionaryService;
      ref.componentInstance.next = next;

      ref.componentInstance.meta = {
        workspaceId: this.storage.get('m2uWorkspaceId'),
        hmdDicId: this.selectedDictionary.hmdDicId
      };
    },
    edit: (node: any, next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.hmdDictionaryService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        id: this.currentCategory.id,
        parentId: this.currentCategory.data.parentId,
        workspaceId: this.storage.get('m2uWorkspaceId'),
        oldName: this.currentCategory.data.name,
        hmdDicId: this.selectedDictionary.hmdDicId
      };
      ref.componentInstance.data = node;
      ref.afterClosed().subscribe(res => {
        if (res) {
          this.getHmdDictionaryLineAllList().then();
        }
      });
    },
    remove: (node: HmdDictionaryEntity, next: () => void) => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed()
      .subscribe(confirmed => {
        if (confirmed) {
          this.hmdDictionaryService.deleteCategory(node).subscribe(
            res => {
              if (res.message === 'DELETE_SUCCESS') {
                this.openAlertDialog('Delete', `Category ${node.name} deleted.`, 'success');
                next();
              } else if (res.message === 'RULE_EXISTENCE') {
                this.openAlertDialog('Failed', `There is a rule in the category. Please delete the rule.`, 'error');
              }
            },
            err => {
              console.error('error', err);
              this.openAlertDialog('Error', `Server Error. Category ${node.name} not deleted.`, 'error');
            }
          );
        }
      });
    },
    focus: (node: any) => {
      this.currentCategory = node;
      this.getHmdDictionaryLineAllList(0).then();
    },
    blur: () => {
      this.currentCategory = null;
    },
  };

  constructor(private hmdDictionaryService: HmdDictionaryService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private objectManager: AppObjectManagerService,
              private dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private modelService: ModelInsertService,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actions = [
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      },
      {
        type: 'delete',
        text: 'Delete',
        icon: 'delete',
        callback: this.delete,
        hidden: false
      },
      {
        type: 'apply',
        text: 'Apply',
        icon: 'cloud_upload',
        callback: this.apply,
        hidden: false
      },
      {
        type: 'save',
        text: 'Save',
        icon: 'save',
        callback: this.save,
        hidden: true
      },
    ];

    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.user = this.storage.get('user').id;
    this.header = this.defaultHeader;
    this.selectedDictionary.name = '';
    this.selectedDictionary.manDesc = '';

    // rule button
    this.ruleActions = [
      {
        type: 'addLine',
        text: 'Add',
        // icon: 'playlist_add',
        callback: this.addLine,
        hidden: false
      },
      {
        type: 'deleteLine',
        text: 'Remove',
        // icon: 'delete_sweep',
        callback: this.deleteLine,
        hidden: false
      }
    ];

    this.model_actions = {
      type: 'Create',
      text: 'Create model',
      icon: 'edit_circle_outline',
      callback: this.createModelInfo,
      hidden: true
    };

    this.hmdData = this.objectManager.get('hmdData');
    // url에 없는 name으로 조회시 처리 로직
    this.route.params.subscribe(par => {
      this.objectManager.clean('hmdData');
      if (par.hasOwnProperty('id')) {
        this.isCreated = true;
        if (par['id'] !== '') {
          this.selectedDictionary.name = par['id'];
          if (!this.hmdData) {
            let param: HmdDictionaryEntity = new HmdDictionaryEntity();
            param.hmdUse = true;
            param.workspaceId = this.workspaceId;
            param.name = this.selectedDictionary.name;

            this.modelService.getHmdOrDnnModel(param).subscribe(res => {
              if (res) {
                this.hmdData = res;
                this.isCreated = true;
                this.model_actions.hidden = true;
                this.selectedDictionary = res;
                this.selectedDictionary.manDesc = this.hmdData.manDesc;
                this.getHmdCategoryAllList();
              } else {
                this.openAlertDialog('Error', `Error. Check a name of hmd model in a url`, 'error');
                this.backPage();
              }
            }, () => {
              this.openAlertDialog('Error', `Error. Server Error`, 'error');
              this.backPage();
            });
          } else { // objectManager 로 정보 넘어온 경우
            this.selectedDictionary.hmdDicId = this.hmdData.hmdDicId;
            this.selectedDictionary.id = this.hmdData.id;
            this.selectedDictionary.manDesc = this.hmdData.manDesc;
            this.getHmdCategoryAllList();
          }

          this.route.url.subscribe(url => {
            if (url.length > 1) {
              if (url[1].path === 'edit') {
                this.header = this.editHeader;
                this.updateToolbar();
              } else {
                this.openAlertDialog('Error', `Error. Check a url`, 'error');
                this.backPage();
              }
            }
          }, () => {
            this.openAlertDialog('Error', `Error. Server Error`, 'error');
          });
        }
      } else {
        this.model_actions.hidden = false;
        this.isCreated = false;
        this.isReadOnly = false;
        this.isNew = false;
        this.store.dispatch({type: ROUTER_LOADED});
      }
    });

    this.infoGroup = new FormGroup({
      name: new FormControl('name', [Validators.required]),
      manDesc: new FormControl('manDesc'),
    });
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  changeRule(data) {
    this.inputData = data;
    this.param = new HmdDictionaryEntity();
    this.param.seq = this.inputData.seq;
    this.param.versionId = this.selectedDictionary.hmdDicId;
    this.param.category = this.inputData.category;
    this.param.rule = this.inputData.rule;

    this.hmdDictionaryService.insertLine(this.param).subscribe(
      res => {
        if (res) {
        } else {
          return false;
        }
      }, err => {
        console.error('error', err);
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  // edit 모드
  edit = () => {
    this.objectManager.set('hmd', this.selectedDictionary);
    this.router.navigate(['edit'], {relativeTo: this.route});
  };

  // detail 화면에서 삭제처리
  delete = () => {
    let dataInfo = [];
    dataInfo.push({
      id: this.selectedDictionary.id,
      hmdDicId: this.selectedDictionary.hmdDicId/*,
      workspaceId: this.workspace*/
    });

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove models?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.modelService.deleteSelectedModels(dataInfo).subscribe(res => {
          if (res) {
            this.openAlertDialog('Success', 'Success Remove Model', 'success');
            this.router.navigate(['..'], {relativeTo: this.route});
          }
        }, () => {
          this.openAlertDialog('Error', 'Server error. remove models failed.', 'error');
        });
      }
    });
  };


  apply = () => {
    if (this.lineTableComponent.rows.length !== 0) {
      this.param = new HmdDictionaryEntity();
      this.param.workspaceId = this.workspaceId;
      this.param.name = this.selectedDictionary.name;
      this.param.id = this.selectedDictionary.hmdDicId;

      this.param.applied = this.git.commitList[0].hashKey;
      this.param.message = this.git.commitList[0].message;

      this.hmdDictionaryService.hmdApply(this.param).subscribe(res => {
          if (res.hasOwnProperty('message') && res.message === 'SUCCESS') {
            this.openAlertDialog('Apply', `complete apply.`, 'success');
          } else {
            this.openAlertDialog('Failed', `Failed to apply.`, 'error');
            return false;
          }
        }, err => {
          console.error('error', err);
          this.openAlertDialog('Error', `Server error. Failed to apply.`, 'error');
        }
      )
    } else {
      this.openAlertDialog('Failed', `Please add rule.`, 'error');
    }
  };

  // hmd name value setting
  changeModelName = (param) => {
    if (!this.isCreated) {
      this.selectedDictionary.name = param;
    }
  };

  // hmd description value setting
  changeModelDesc = (param) => {
    if (this.isCreated) {
      this.editModelInfo(param);
    } else {
      this.selectedDictionary.manDesc = param;
    }
  };


  editModelInfo = (desc: string) => {
    let hmdDictionaryEntity: HmdDictionaryEntity = new HmdDictionaryEntity;
    hmdDictionaryEntity.hmdUse = true;
    hmdDictionaryEntity.dnnUse = false;
    hmdDictionaryEntity.dnnDicId = '';
    hmdDictionaryEntity.workspaceId = this.workspaceId;
    hmdDictionaryEntity.updaterId = this.user;
    hmdDictionaryEntity.manDesc = desc === '' ? this.selectedDictionary.manDesc : desc;
    hmdDictionaryEntity.name = null;
    hmdDictionaryEntity.id = this.selectedDictionary.id;
    hmdDictionaryEntity.hmdDicId = this.selectedDictionary.hmdDicId;

    this.modelService.updateHmdOrDnn(hmdDictionaryEntity).subscribe(() => {
    }, () => {
      this.openAlertDialog('Error', `Change HMD model name.`, 'error');
    });
  };

  // 등록 화면에서 사용
  createModelInfo = () => {
    let hmdDictionaryEntity: HmdDictionaryEntity = new HmdDictionaryEntity();
    hmdDictionaryEntity.hmdUse = true;
    hmdDictionaryEntity.workspaceId = this.workspaceId;
    hmdDictionaryEntity.creatorId = this.selectedDictionary.id;
    hmdDictionaryEntity.updaterId = this.selectedDictionary.id;
    hmdDictionaryEntity.manDesc = this.selectedDictionary.manDesc;
    hmdDictionaryEntity.name = this.selectedDictionary.name;
    hmdDictionaryEntity.hmdDicId = this.selectedDictionary.hmdDicId;

    if (this.selectedDictionary.name === '') {
      this.openAlertDialog('Error', `Input HMD model name`, 'error');
    } else {
      this.isNew = 'readOnly';
      this.modelService.insertHmdOrDnn(hmdDictionaryEntity).subscribe(res => {
        if (res.message === 'INSERT_DUPLICATED') {
          this.openAlertDialog('Duplicate', 'Model Name duplicated.', 'Error');
        } else {
          this.isCreated = true;
          this.model_actions.hidden = true;
          this.selectedDictionary = res;
          this.hmdData = res;
          this.objectManager.set('hmd', this.selectedDictionary);
          this.router.navigate(['..', this.selectedDictionary.name, 'edit'], {relativeTo: this.route});
        }
      });
    }
  };


  getHmdCategoryAllList() {
    let promise = new Promise((resolve, reject) => {
      this.param = new HmdDictionaryEntity();
      this.param.id = this.selectedDictionary.hmdDicId;
      this.param.workspaceId = this.workspaceId;

      this.hmdDictionaryService.getHmdCategoryAllList(this.param).subscribe(
        res => {
          if (res) {
            this.categories = res;
            resolve();
          } else {
            return false;
          }
        }, err => {
          this.openAlertDialog('Error', `Server error. Failed to fetch categories.`, 'error');
          reject();
        }
      );
    });

    promise.then(() => {
      this.getHmdDictionaryLineAllList();
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  addLine = (rule = '') => {
    if (!this.currentCategory || !this.currentCategory['isLeaf']) {
      this.openAlertDialog('Failed', `Cannot add a line to non-leaf category.`, 'error');
      return false;
    }

    let temp: HmdDictionaryEntity = new HmdDictionaryEntity();
    temp.category = this.currentCategory.data.name;
    temp.rule = rule;
    temp.versionId = this.selectedDictionary.hmdDicId;

    this.hmdDictionaryService.insertLine(temp).subscribe(
      res => {
        if (res) {
          this.hmdDictionaryLineList.unshift({
            category: this.currentCategory.data.name,
            rule: null
          });
          this.getHmdDictionaryLineAllList().then(() => {
            this.appTree.currentNode.data.count += 1;
          });
        } else {
          return false;
        }
      }, err => {
        console.error('error', err);
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  };

  setLeafChildrenCount(node, hmdDicLineList) {
    if (node.children.length > 0) {
      node.children.forEach(childChildren => {
        this.setLeafChildrenCount(childChildren, hmdDicLineList);
      });
    } else {
      let findNodeCount: number = hmdDicLineList.filter(hmdDicLine => node.name === hmdDicLine.category).length;
      if (findNodeCount > 0) {
        node.count -= findNodeCount;
      }
    }
  }

  deleteLine = () => {
    let hmdDicLineList: HmdDictionaryEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (hmdDicLineList.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (hmdDicLineList.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.hmdDictionaryService.deleteLines(hmdDicLineList).subscribe(
            res => {
              if (res) {
                this.getHmdDictionaryLineAllList().then(() => {
                  this.appTree.nodes.forEach(node => {
                    this.setLeafChildrenCount(node, hmdDicLineList);
                  });
                  this.openAlertDialog('Delete', `The selected rule has been deleted.`, 'success');
                });
              } else {
                return false;
              }
            },
            err => {
              console.error('error', err);
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected rules.`, 'error');
            }
          );
        }
      });
    }
  };

  save = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedDictionary.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          this.param = new HmdDictionaryEntity();
          this.param.id = this.selectedDictionary.hmdDicId;
          this.param.name = this.selectedDictionary.name;
          this.param.workspaceId = this.workspaceId;
          this.param.title = result.title;
          this.param.message = result.message;

          this.hmdDictionaryService.commit(this.param).subscribe(
            () => {
              this.header = this.defaultHeader;
              this.router.navigate(['..'], {relativeTo: this.route});
            },
            err => {
              console.error('error', err);
            });
        }
      });
  };


  onUploadFile = () => {
    this.getHmdCategoryAllList();
  };

  updateToolbar() {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'apply') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    this.isReadOnly = this.editable ? false : 'readOnly';
  }

  testWithFiles() {
    this.router.navigate(['./analysis'], {relativeTo: this.route});
  };

  testWithText() {
    this.router.navigate(['./test'], {relativeTo: this.route});
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.hmdDictionaryLineList.length > 0) {
      this.getHmdDictionaryLineAllList();
    }

  }

  getHmdDictionaryLineAllList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.param = new HmdDictionaryEntity();
    this.param.versionId = this.selectedDictionary.hmdDicId;
    if (this.currentCategory) {
      this.param.category = this.currentCategory.data.name;
    }
    this.param.rule = this.searchRule;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;

    return new Promise(resolve => {
      this.hmdDictionaryService.getAllDicLines(this.param).subscribe(
        res => {
          if (res) {
            this.lineTableComponent.isCheckedAll = false;
            this.hmdDictionaryLineList = res['dicLineList'];
            this.pageLength = res['total'];
          } else {
            return false;
          }
          resolve();
        },
        err => {
          console.error('error', err);
          this.openAlertDialog('Error', `Server Error. Failed to fetch rules.`, 'error');
        }
      )
    });
  }


  backPage() {
    this.objectManager.clean('hmd');
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  hmdTipEnter() {
    this.tipsVisible = true;
  }

  hmdTipLeave() {
    this.tipsVisible = false;
  }

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };
}
