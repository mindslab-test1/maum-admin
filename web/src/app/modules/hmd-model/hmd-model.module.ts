import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HmdModelRoutingModule} from './hmd-model-routing.module';
import {HmdModelsComponent} from './components/hmd-models/hmd-models.component';
import {HmdModelDetailComponent} from './components/hmd-detail/hmd-model-detail.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {HmdAnalysisTestComponent} from './components/hmd-analysis-test/hmd-analysis-test.component';
import {ModelInsertService} from './services/model-insert/model-insert.service';
import {HmdAnalyzeTestService} from './services/hmd-analyze-test/hmd-analyze-test.service';
import {HmdAnalyzeResultService} from './services/hmd-analyze-result/hmd-analyze-result.service';
import {HmdAnalyzeService} from './services/hmd-analyze-analyze/hmd-analyze-analyze.service';
import {HmdDictionaryService} from './services/hmd-dictionary/hmd-dictionary.service';
import {HmdAnalysisAnalyzeResultComponent} from './components/hmd-analysis-analyze-result/hmd-analysis-analyze-result.component';
import {HmdAnalysisAnalyzeComponent} from './components/hmd-analysis-analyze/hmd-analysis-analyze.component';

@NgModule({
  declarations: [
    HmdModelsComponent,
    HmdModelDetailComponent,
    HmdAnalysisAnalyzeResultComponent,
    HmdAnalysisAnalyzeComponent,
    HmdAnalysisTestComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HmdModelRoutingModule,
    MatCardModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
  ],
  exports: [
    HmdModelsComponent,
    HmdModelDetailComponent,
    HmdAnalysisAnalyzeResultComponent,
    HmdAnalysisAnalyzeComponent,
    HmdAnalysisTestComponent
  ],
  providers: [
    ModelInsertService,
    HmdAnalyzeTestService,
    HmdAnalyzeResultService,
    HmdAnalyzeService,
    HmdDictionaryService
  ]
})
export class HmdModelModule {
}
