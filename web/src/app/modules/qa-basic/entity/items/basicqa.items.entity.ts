import {BasicQASkillEntity} from './basicqa.skill.entity';
import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class BasicQAItemsEntity extends PageParameters {

  id?: number;
  question?: string;
  answer?: string;
  useYn?: string;
  mainYn?: string;
  weight?: number;
  src?: string;
  basicQASkillEntity?: BasicQASkillEntity;
  skillId?: number;
  convertedUseYn?: string;

  channel?: string;
  category?: string;
  subCategory?: string;
  subSubCategory?: string;
}
