import {BasicQAParaphraseRelEntity} from './basicqa.paraphrase-rel.entity';
import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class BasicQAParaphraseMainWordEntity extends PageParameters {

  id?: number;
  word?: string;
  wordType?: string;
  useYn?: string;
  createAt?: Date;
  createId?: string;
  basicQAParaphraseRelEntities?: BasicQAParaphraseRelEntity[];
  basicQAParaphraseWordList?: string[];
  convertedUseYn?: string;
  convertedCreateAt?: Date;
  searchMainWord?: string;
  searchWords?: string;
}
