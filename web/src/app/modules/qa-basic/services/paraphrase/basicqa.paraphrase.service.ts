import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {BasicQAParaphraseWordEntity} from '../../entity/paraphrase/basicqa.paraphrase-word.entity';
import {BasicQAParaphraseMainWordEntity} from '../../entity/paraphrase/basicqa.paraphrase-main-word.entity';
import {BasicQAParaphraseRelEntity} from '../../entity/paraphrase/basicqa.paraphrase-rel.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class BasicQAParaphraseService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  checkWordExist(wordList: String[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/checkMainWordExist', wordList);
  }

  getMainWords(basicQAParaphraseMainWordEntity: BasicQAParaphraseMainWordEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/getMainWords', basicQAParaphraseMainWordEntity);
  }

  addMainWord(basicQAParaphraseMainWordEntity: BasicQAParaphraseMainWordEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/addMainWord', basicQAParaphraseMainWordEntity);
  }

  addWords(basicQAParaphraseWordEntities: BasicQAParaphraseWordEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/addWords', basicQAParaphraseWordEntities);
  }

  updateMainWord(basicQAParaphraseMainWordEntity: BasicQAParaphraseMainWordEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/updateMainWord', basicQAParaphraseMainWordEntity);
  }

  deleteMainWords(basicQAParaphraseMainWordEntities: BasicQAParaphraseMainWordEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/deleteMainWords', basicQAParaphraseMainWordEntities);
  }

  deleteRels(basicQAParaphraseRelEntities: BasicQAParaphraseRelEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/paraphrase/deleteRels', basicQAParaphraseRelEntities);
  }
}

