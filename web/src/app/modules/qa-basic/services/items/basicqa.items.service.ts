import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BasicQAItemsEntity} from '../../entity/items/basicqa.items.entity';
import {BasicQASkillEntity} from '../../entity/items/basicqa.skill.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class BasicQAService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getItems(basicQAItemEntity: BasicQAItemsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/item/getItems', basicQAItemEntity);
  }

  addItem(basicQAItemEntity: BasicQAItemsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/item/add', basicQAItemEntity);
  }

  updateItem(basicQAItemEntity: BasicQAItemsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/item/update', basicQAItemEntity);
  }

  deleteItems(basicQAItemEntities: BasicQAItemsEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/item/delete', basicQAItemEntities);
  }

  deleteItemBySkillId(skillId: number): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/item/delete/all', skillId);
  }

  getSkillList(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/skill/list');
  }

  getSkillByName(name: string): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/skill/name/' + name);
  }

  getSkillById(id: number): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/skill/id/' + id);
  }

  addSkill(basicQASkillEntity: BasicQASkillEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/skill/add', basicQASkillEntity);
  }

  updateSkill(basicQASkillEntity: BasicQASkillEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/skill/update', basicQASkillEntity);
  }

  deleteSkills(basicQASkillEntities: BasicQASkillEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/skill/delete/list', basicQASkillEntities);
  }


  /**
   * BasicQA 그룹코드로 전체 카테고리 조회
   *   {Observable<any>}
   */
  getCategory(): Observable<any> {
    return this.http.get(this.API_URL + '/basicqa/category/getCategoryAllList');
  }
}

