import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BasicQAQuestionEntity} from '../../entity/question/basicqa.question.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class BasicQAQuestionService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  question(param: BasicQAQuestionEntity): Observable<any> {
    return this.http.post(this.API_URL + '/basicqa/question/question', param);
  }
}

