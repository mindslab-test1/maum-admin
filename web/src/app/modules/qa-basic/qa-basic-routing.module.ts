import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ItemsComponent} from './components/items/items.component';
import {ScheduleComponent} from './components/schedule/schedule.component';
import {QuestionComponent} from './components/question/question.component';
import {ParaphraseComponent} from './components/paraphrase/paraphrase.component';
import {CategoryComponent} from './components/category/category.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'items',
      },
      {
        path: 'items',
        component: ItemsComponent,
        data: {
          nav: {
            name: 'Items',
            comment: '',
            icon: 'library_books',
          }
        }
      },
      {
        path: 'schedule',
        component: ScheduleComponent,
        data: {
          nav: {
            name: 'Schedule',
            comment: '',
            icon: 'today',
          }
        }
      },
      {
        path: 'question',
        component: QuestionComponent,
        data: {
          nav: {
            name: 'Question',
            comment: '',
            icon: 'question_answer',
          }
        }
      },
      {
        path: 'paraphrase',
        component: ParaphraseComponent,
        data: {
          nav: {
            name: 'Paraphrase',
            comment: '',
            icon: 'library_books',
          }
        }
      },
      {
        path: 'category',
        component: CategoryComponent,
        data: {
          nav: {
            name: 'Category',
            comment: 'bqa category',
            icon: 'category',
          }
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QaBasicRoutingModule {
}
