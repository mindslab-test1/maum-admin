import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {QaBasicRoutingModule} from './qa-basic-routing.module';
import {CategoryComponent} from './components/category/category.component';
import {ItemsComponent} from './components/items/items.component';
import {ParaphraseComponent} from './components/paraphrase/paraphrase.component';
import {QuestionComponent} from './components/question/question.component';
import {ScheduleComponent} from './components/schedule/schedule.component';
import {ItemsUpsertDialogComponent} from './components/items/items-upsert-dialog.component';
import {SkillUpsertDialogComponent} from './components/items/skill-upsert-dialog.component';
import {ParaphraseUpsertDialogComponent} from './components/paraphrase/paraphrase-upsert-dialog.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressBarModule,
  MatRadioModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {BasicQAScheduleService} from './services/schedule/basicqa.schedule.service';
import {BasicQAService} from './services/items/basicqa.items.service';
import {BasicQAQuestionService} from './services/question/basicqa.question.service';
import {BasicQAParaphraseService} from './services/paraphrase/basicqa.paraphrase.service';
import {BasicQACategoryService} from './services/category/basicqa.category.service';

@NgModule({
  declarations: [
    CategoryComponent,
    ItemsComponent,
    ParaphraseComponent,
    QuestionComponent,
    ScheduleComponent,
    ItemsUpsertDialogComponent,
    SkillUpsertDialogComponent,
    ParaphraseComponent,
    ParaphraseUpsertDialogComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    QaBasicRoutingModule,
    MatToolbarModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatRadioModule,
  ],
  exports: [
    ItemsComponent,
    ItemsUpsertDialogComponent,
    SkillUpsertDialogComponent,
    ParaphraseComponent,
    ParaphraseUpsertDialogComponent
  ],
  providers: [
    BasicQAScheduleService,
    BasicQAService,
    BasicQAQuestionService,
    BasicQAParaphraseService,
    BasicQACategoryService
  ],
  entryComponents: [
    SkillUpsertDialogComponent,
    ItemsUpsertDialogComponent,
    ParaphraseUpsertDialogComponent
  ]
})
export class QaBasicModule {
}
