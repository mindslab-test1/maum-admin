import {
  AfterViewInit,
  Component,
  Inject, OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {BasicQAService} from '../../services/items/basicqa.items.service';
import {DatePipe} from '@angular/common';
import {BasicQAItemsEntity} from '../../entity/items/basicqa.items.entity';
import {ItemsUpsertDialogComponent} from './items-upsert-dialog.component';
import {SkillUpsertDialogComponent} from './skill-upsert-dialog.component';
import {BasicQASkillEntity} from '../../entity/items/basicqa.skill.entity';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {
  AlertComponent, AppEnumsComponent, ConfirmComponent, TableComponent,
  UploaderButtonComponent
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';


@Component({
  selector: 'app-basicqa-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [BasicQAService, DatePipe]
})

export class ItemsComponent implements OnInit, OnDestroy, AfterViewInit {

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredBasicQASkillList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  // 그룹코드로 전체 Category 정보 획득 후 담는 변수
  contextCate = [];
  selectedCategory1 = {children: []};  // 채널
  selectedCategory2 = {children: []};  // 대분류
  selectedCategory3 = {children: []};  // 중분류
  selectedCategory4 = {children: []};  // 대분류
  tempCate = {children: []};

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  actionsArray: any[];
  skillActionsArray: any[];
  pageParam: MatPaginator;
  pageLength: number;
  searchQuestion: string;
  searchAnswer: string;
  searchSrc: string;
  useYn;
  mainYn;
  param = new BasicQAItemsEntity();

  header = [
    {attr: 'checkbox', name: 'CheckBox', type: 'text', checkbox: true, input: false},
    {attr: 'no', name: 'No.', no: true},
    {
      attr: 'basicQACategoryEntity.catName',
      name: 'Channel',
      type: 'text',
      checkbox: false,
      input: false
    },
    {
      attr: 'basicQACategoryEntity2.catName',
      name: 'Category',
      type: 'text',
      checkbox: false,
      input: false
    },
    {
      attr: 'basicQACategoryEntity3.catName',
      name: 'SubCategory1',
      type: 'text',
      checkbox: false,
      input: false
    },
    {
      attr: 'basicQACategoryEntity4.catName',
      name: 'SubCategory2',
      type: 'text',
      checkbox: false,
      input: false
    },
    {attr: 'basicQASkillEntity.name', name: 'Skill', type: 'text', checkbox: false, input: false},
    {attr: 'question', name: 'Question', type: 'text', checkbox: false, input: false},
    {attr: 'answer', name: 'Answer', type: 'text', checkbox: false, input: false},
    {attr: 'src', name: 'Src', type: 'text', checkbox: false, input: false, isSort: true},
    {
      attr: 'useYn',
      name: 'Use',
      type: 'text',
      checkbox: false,
      input: false,
      width: '7%',
      isSort: true
    },
    {
      attr: 'mainYn',
      name: 'Main',
      type: 'text',
      checkbox: false,
      input: false,
      width: '7%',
      isSort: true
    },
    {
      attr: 'weight',
      name: 'Weight',
      type: 'text',
      checkbox: false,
      input: false,
      width: '5%',
      isSort: true
    },
    {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '7%'},
  ];
  basicQAItemList: any[] = [];
  dataSource: MatTableDataSource<any>;

  basicQASkillList;
  selectedSkill: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private basicQAService: BasicQAService,
              public appEnum: AppEnumsComponent,
              private dialog: MatDialog,
              private datePipe: DatePipe,
              private store: Store<any>) {
  }

  ngOnInit() {
    // 전체 Category 정보 조회
    this.getCategory();

    this.selectedSkill = 0;
    this.actionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popUpsertDialog,
        params: 'add'
      },
      {
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeItems,
        type: 'delete'
      },
      {
        type: 'edit',
        text: 'Remove All',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeItemsAll,
        params: 'remove'
      },
    ];
    this.skillActionsArray = [
      {
        type: 'add',
        text: 'Add Skill',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popSkillUpsertDialog,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Edit Skill',
        icon: 'remove_circle_outline',
        hidden: true,
        disabled: true,
        callback: this.popSkillUpsertDialog,
        params: 'edit'
      },
    ];
    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterSkill();
    });
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.getSkillList();

    // router load 완료
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterSkill() {
    if (!this.basicQASkillList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredBasicQASkillList.next(this.basicQASkillList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredBasicQASkillList.next(
      this.basicQASkillList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }


  /**
   * Search Button 정의
   * @param  pageIndex
   */
  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  search(matSort?: MatSort) {
    if (matSort) {
      this.param.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'desc' : matSort.direction;
      this.param.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
      this.getNextPage();
    }
  }

  /**
   * BSQA에서 Skill이 선택되면 호출되는 함수
   * 해당 Skill로 등록된 MLT_BSQA_ITEM 테이블을 조회한다
   * @param event
   * @returns boolean
   */
  getNextPage(event?) {
    if (this.selectedSkill === 0) {
      this.skillActionsArray[1].hidden = true;
      this.skillActionsArray[1].disabled = true;
      return false;
    }
    this.skillActionsArray[1].hidden = false;
    this.skillActionsArray[1].disabled = false;

    this.param.question = this.searchQuestion;
    this.param.answer = this.searchAnswer;

    this.param.src = this.searchSrc;
    this.param.useYn = this.useYn;
    this.param.mainYn = this.mainYn;

    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.skillId = this.selectedSkill;

    // getItems로 전달할 param 정보를 설정한다
    this.param.channel = this.selectedCategory1['id'];
    this.param.category = this.selectedCategory2['id'];
    this.param.subCategory = this.selectedCategory3['id'];
    this.param.subSubCategory = this.selectedCategory4['id'];

    this.basicQAService.getItems(this.param).subscribe(res => {
      if (res) {
        this.basicQAItemList.length = 0;
        res.content.map(val => {
          val.createAt = this.datePipe.transform(val.createAt, 'yyyy-MM-dd hh: mm');
          if (val.weight === 1) {
            val.weight = '1.0';
          } else if (val.weight === 0) {
            val.weight = '0.0';
          }
        });
        this.basicQAItemList = res.content;
        this.pageLength = res.totalElements;
        this.dataSource = new MatTableDataSource(this.basicQAItemList);
        this.lineTableComponent.isCheckedAll = false;
      }
    });
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.basicQAItemList.length > 0) {
      this.getNextPage();
    }
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  removeItems = () => {
    let toBeRemoved = [];
    this.basicQAItemList.forEach(basicQAItem => {
      if (basicQAItem.isChecked) {
        toBeRemoved.push(basicQAItem);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please Select Items to Removed', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.lineTableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.lineTableComponent.isCheckedAll = false;
        this.basicQAService.deleteItems(toBeRemoved).subscribe(
          res => {
            this.openAlertDialog('Success', `Success Deleted.`, 'success');
            this.getNextPage();
          },
          err => {
            this.pageParam.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
          });
      }
    });
  };

  removeItemsAll = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove All Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        this.pageParam.pageIndex = 0;
        this.lineTableComponent.isCheckedAll = false;
        this.basicQAService.deleteItemBySkillId(this.selectedSkill).subscribe(
          res => {
            this.openAlertDialog('Success', `Success Deleted.`, 'success');
            this.getNextPage();
          },
          err => {
            this.pageParam.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', 'Server error. items delete failed.', 'error');
          });
      }
    });
  };

  popUpsertDialog = (type, row?) => {
    let paramData = {};
    paramData['selectedSkillId'] = this.selectedSkill;
    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = row;
    } else {
      return false;
    }

    paramData['service'] = this.basicQAService;
    paramData['entity'] = new BasicQAItemsEntity();

    // add, edit 창을 open 한다
    let dialogRef = this.dialog.open(ItemsUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getNextPage();
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  // 업로드 이후 호출되는 함수
  onUploadFile = () => {
    this.getNextPage();
  };

  // 다운로드 함수
  downloadFile = () => {
    let url = this.storage.get('mltApiUrl') + '/basicqa/item/' + this.selectedSkill + '/download-file';
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

  getSkillList() {
    this.basicQAService.getSkillList().subscribe(res => {
      this.basicQASkillList = res;
      this.filteredBasicQASkillList.next(this.basicQASkillList.slice());
    })
  }

  popSkillUpsertDialog = (type, row?) => {
    let paramData = {};

    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.selectedSkill;
    } else {
      return false;
    }

    paramData['service'] = this.basicQAService;
    paramData['entity'] = new BasicQASkillEntity();

    let dialogRef = this.dialog.open(SkillUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSkillList();
      if (result === 'delete') {
        this.selectedSkill = 0;
        this.skillActionsArray[1].hidden = true;
        this.skillActionsArray[1].disabled = true;
      }

    });
  };

  /**
   * 이미 선택된 값 초기화
   * @param valStr
   * @param dept
   */
  clearSelectOption(valStr, dept) {
    // 최대 길이 설정
    let maxLength = 0;
    if (valStr === 'selectedDataType') {
      maxLength = 2;
    } else if (valStr === 'selectedCategory') {
      maxLength = 4;
    }

    for (let i = (dept + 1); i <= maxLength; i++) {
      this[valStr + i] = this.tempCate;
      // eval('this.' + valStr + i + ' = this.tempCate');
    }
  }

  /**
   * 그룹코드를 이용하여 BasicQA 의 전체 Category 조회
   */
  getCategory() {
    this.basicQAService.getCategory().subscribe(res => {
        if (res) {
          this.contextCate = res.channel;
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get CategoryData', 'error');
        console.log('error', err);
      }
    );
  }
}
