import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {BasicQAItemsEntity} from '../../entity/items/basicqa.items.entity';
import {BasicQAService} from '../../services/items/basicqa.items.service';
import {AlertComponent, ConfirmComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';


@Component({
  selector: 'app-item-upsert-dialog',
  templateUrl: 'items-upsert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class ItemsUpsertDialogComponent implements OnInit {

  param: any;
  service: BasicQAService;
  role: string;

  name: string;
  item: BasicQAItemsEntity = new BasicQAItemsEntity();
  questionValidator = new FormControl('', [Validators.required]);
  answerValidator = new FormControl('', [Validators.required]);
  srcValidator = new FormControl('', [Validators.required]);
  weightValidator = new FormControl('', [Validators.required, Validators.min(0), Validators.max(1.0)
    , Validators.pattern('[0-1]+(\\.[0-9]?)?')]);
  useYnValidator = new FormControl('', [Validators.required]);
  mainYnValidator = new FormControl('', [Validators.required]);
  disabled = true;
  data: any;

  contextCate = []; // 전체 Category 정보
  selectedCategory1 = {children: []};  // 채널
  selectedCategory2 = {children: []};  // 대분류
  selectedCategory3 = {children: []};  // 중분류
  selectedCategory4 = {children: []};  // 소분류
  tempCate = {children: []};

  constructor(public dialogRef: MatDialogRef<ItemsUpsertDialogComponent>,
              private basicQAService: BasicQAService,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.role = this.data.role;
    this.item = this.data.row;
    this.item.skillId = this.data.selectedSkillId;
    this.service = this.data['service'];
    if (this.role === 'add') {
      this.item.useYn = 'Y';
      this.item.mainYn = 'N';
      this.item.weight = 1.0;
    }
    this.getCategory();
  }

  addItem = () => {
    // DB insert 시 category id로 insert 해야한다
    this.item.channel = this.selectedCategory1['id'];
    this.item.category = this.selectedCategory2['id'];
    this.item.subCategory = this.selectedCategory3['id'];
    this.item.subSubCategory = this.selectedCategory4['id'];

    this.service.addItem(this.item).subscribe(res => {
        if (res) {
          this.dialogRef.close(true);
          this.openAlertDialog('Success', 'Success Add Item', 'success');
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Add Item', 'error');
        console.log('error', err);
      }
    );
  };

  updateItem = () => {
    if (!this.data['row']['id']) {
      this.openAlertDialog('Error', 'Failed Edit Item', 'error');
      this.dialogRef.close();
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        // DB update 시 category id로 update 해야한다
        this.item.channel = this.selectedCategory1['id'];
        this.item.category = this.selectedCategory2['id'];
        this.item.subCategory = this.selectedCategory3['id'];
        this.item.subSubCategory = this.selectedCategory4['id'];

        this.service.updateItem(this.item).subscribe(res => {
            if (res) {
              this.dialogRef.close(true);
            }
          },
          err => {
            this.openAlertDialog('Error', 'Failed Edit Item', 'error');
            console.log('error', err);
          }
        );
      }
    });
  };

  removeItem = () => {
    if (!this.data['row']['id']) {
      this.openAlertDialog('Error', 'Failed Remove Item', 'error');
      this.dialogRef.close();
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let list = [];
        list.push(this.item);
        this.service.deleteItems(list).subscribe(res => {
            if (res) {
              this.dialogRef.close(true);
            }
          },
          err => {
            this.openAlertDialog('Error', 'Failed Remove Item', 'error');
            console.log('error', err);
          }
        );
      }
    })
  };

  checkInvalid($event?) {
    if ('INVALID' === this.questionValidator.status) {
      this.disabled = true;
      return false;
    } else if (('VALID' === this.questionValidator.status)
      && ('VALID' === this.answerValidator.status) && ('VALID' === this.srcValidator.status)
      && ('VALID' === this.weightValidator.status) && ('VALID' === this.useYnValidator.status)
      && ('VALID' === this.mainYnValidator.status)) {
      this.disabled = false;
      return true;
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * 이미 선택된 값 초기화
   * @param valStr
   * @param dept
   */
  clearSelectOption(valStr, dept) {
    // 최대 길이 설정
    let maxLength = 0;
    if (valStr === 'selectedDataType') {
      maxLength = 2;
    } else if (valStr === 'selectedCategory2') {
      maxLength = 4;
    }

    for (let i = (dept + 1); i <= maxLength; i++) {
      this[valStr + i] = this.tempCate;
      // eval('this.' + valStr + i + ' = this.tempCate');
    }

    // Category 정보가 수정되면 edit 버튼 활성화
    this.checkInvalid();
  }

  /**
   * 그룹코드를 이용하여 BasicQA 의 전체 Category 조회
   */
  getCategory() {
    this.basicQAService.getCategory().subscribe(res => {
        if (res) {
          this.contextCate = res.channel;

          if (this.role === 'edit') {
            // edit인 경우 contextCate(전체 Cate정보)에서 전달 받은 row정보(선택된 row)의 id값을 비교한다 (item은 data.row 정보)
            // item 정보가 없는 경우에 대한 Exception 예외 처리
            if (this.item.channel !== null) {
              this.selectedCategory1 = this.contextCate.filter(item => item.id === this.item.channel)[0];
            }
            if (this.item.category !== null) {
              this.selectedCategory2 = this.selectedCategory1.children.filter(item => item.id === this.item.category)[0];
            }
            if (this.item.subCategory !== null) {
              this.selectedCategory3 = this.selectedCategory2.children.filter(item => item.id === this.item.subCategory)[0];
            }
            if (this.item.subSubCategory !== null) {
              this.selectedCategory4 = this.selectedCategory3.children.filter(item => item.id === this.item.subSubCategory)[0];
            }
          }
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get CategoryData', 'error');
        console.log('error', err);
      }
    );
  }
}
