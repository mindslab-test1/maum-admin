import {AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {BasicQAService} from '../../services/items/basicqa.items.service';
import {DatePipe} from '@angular/common';
import {BasicQAScheduleService} from '../../services/schedule/basicqa.schedule.service';
import {MatDialog} from '@angular/material';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AlertComponent} from 'app/shared';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-basicqa-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [BasicQAService, DatePipe]

})
export class ScheduleComponent implements OnInit, OnDestroy, AfterViewInit {
  indexing;
  history;
  progressStatus = false;
  intervalHandler: any;
  timeout;
  disabled = false;

  basicQASkillList;
  selectedSkill = 0;
  clean = false;
  valid = true;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredBasicQASkillList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private basicQAScheduleService: BasicQAScheduleService,
              private datePipe: DatePipe,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterSkill();
    });
    this.getSkillList();
    this.timeout = 5000;
    this.getIndexingStatus();
  }

  ngAfterViewInit() {
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterSkill() {
    if (!this.basicQASkillList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredBasicQASkillList.next(this.basicQASkillList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredBasicQASkillList.next(
      this.basicQASkillList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.getIndexingStatus()
    }, this.timeout);
  }

  fullIndexing() {
    this.basicQAScheduleService.fullIndexing(this.selectedSkill, this.clean).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        };
      } else {
        this.disabled = false;
        clearInterval(this.intervalHandler);
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA Indexing failed.', 'error');
    })
  }

  incrementalIndexing() {
    this.basicQAScheduleService.incrementalIndexing(this.selectedSkill, this.clean).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA Indexing failed.', 'error');
    })
  }

  abortIndexing() {
    this.basicQAScheduleService.abortIndexing().subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA Indexing failed.', 'error');
    })
  }

  getIndexingStatus() {
    this.basicQAScheduleService.getIndexingStatus().subscribe(res => {
      this.progressStatus = res.status;
      clearInterval(this.intervalHandler);
      if (res.status) {
        this.disabled = true;
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.history = res.latestIndexingHistory;
        if (this.history != null) {
          this.history.progress = res.progress;
          let date = new Date(this.history.createAt);
          this.history.createAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          if (this.history.stopYn) {
            date = new Date(this.history.updateAt);
            this.history.updateAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          }
        }
        this.disabled = false;
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. BasicQA Indexing failed.', 'error');
    })
  }

  getSkillList() {
    this.basicQAScheduleService.getSkillList().subscribe(res => {
      this.basicQASkillList = res;
      this.filteredBasicQASkillList.next(this.basicQASkillList.slice());
    })
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  checkValid() {
    if (this.selectedSkill !== undefined && this.clean !== undefined) {
      this.valid = true;
    } else {
      this.valid = false;
    }
  }
}
