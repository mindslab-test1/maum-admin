import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {BasicQACategoryService} from '../../services/category/basicqa.category.service';
import {BasicQACategoryEntity} from '../../entity/category/basicqa.category.entity';
import {AlertComponent, ConfirmComponent, FormErrorStateMatcher} from 'app/shared';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';


// Component 선언
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})

export class CategoryComponent implements OnInit, AfterViewInit {
  tipsVisible = false;
  sourceList: string[] = [];
  selectedSource: string;

  treeData: BasicQACategoryEntity[] = [];

  inputBasicQACategory: string[] = [];
  // BasicQA Category List 객체 선언
  basicQACategoryList: BasicQACategoryEntity[][] = [[], [], [], []];

  selectedBasicQACategory: BasicQACategoryEntity[] = [];

  beforeClickData: HTMLInputElement[] = [];
  currentClickData: HTMLInputElement[] = [];

  // 채널
  channelFormControl = new FormControl('', [
    Validators.required,
  ]);

  // 대분류
  categoryFormControl = new FormControl('', [
    Validators.required
  ]);

  // 중분류
  subCategoryFormControl = new FormControl('', [
    Validators.required
  ]);

  // 소분류
  subSubCategoryFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private basicQACategoryService: BasicQACategoryService,
              private dialog: MatDialog,
              public matcher: FormErrorStateMatcher,
              private store: Store<any>) {
  }

  ngOnInit() {
    // this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.getAllBasicQACategory().then(() => {
      this.basicQACategoryList[0] = this.treeData[this.selectedSource];
    });
  }

  ngAfterViewInit(): void {
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * BasicQA Category 전체 조회
   * @returns Promise<any>
   */
  getAllBasicQACategory() {
    return new Promise(resolve => {
      this.basicQACategoryService.getBasicQACategoryAllList().subscribe(
        res => {
          if (res) {
            Object.keys(res).forEach(value => {
              this.sourceList.push(value);
            });
            this.treeData = res;
          }
          this.selectedSource = this.sourceList[0];
          resolve();
        },
        err => {
          this.openAlertDialog('Failed', 'Error. Categories not imported.', 'error');
        }
      )
    });
  }

  /**
   * BasicQA Category 조회
   * @param step
   * @param category
   * @param index
   */
  getBasicQACategory(step, category, index) {
    this.initBasicQACategoryList(step);
    this.selectedBasicQACategory[step - 1] = category;
    if (category.children !== 0) {
      // 자식이 있는 경우 List에 저장 (자식 Category 출력을 위해서)
      this.basicQACategoryList[step] = category.children;
    }
    this.isSelected(step, index);
  }

  isSelected(step, index) {
    this.inputBasicQACategory[step - 1] = this.selectedBasicQACategory[step - 1].catName;
    this.beforeClickData[step - 1] = this.currentClickData[step - 1];
    if (this.beforeClickData[step - 1] != null) {
      this.beforeClickData[step - 1].style.backgroundColor = '#ffffff';
    }
    this.currentClickData[step - 1] = <HTMLInputElement>document.getElementsByName(`category${step}`)[index];
    this.currentClickData[step - 1].style.backgroundColor = '#c1d9ff';  // 선택한 Category 배경색 변경
    for (let i = step; i < 4; i++) {
      this.inputBasicQACategory[i] = null;
    }
  }

  /**
   * 해당 Category의 자식 유무 (자식이 없는 경우 true)
   * @param category
   * @returns boolean
   */
  isDelete(category) {
    return category.children.length === 0;
  }

  /**
   * Alert 출력
   * @param title
   * @param message
   * @param status
   */
  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * 해당 Category 명 Edit 함수
   * @param step
   * @param type
   * @param category
   */
  openConfirmDialog(step, type, category) {
    if (type === 'edit' && this.getErrorState(step)) {
      this.openAlertDialog('Failed', 'Please Input Category', 'error');
      return;
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = type === 'edit' ? `Do you want to Edit
    ${this.selectedBasicQACategory[step - 1].catName} -> ${this.inputBasicQACategory[step - 1]} ?`
        : `Do you want to Delete ${category.catName}?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          if (type === 'delete') {
            this.deleteBasicQACategory(step, category);
          } else if (type === 'edit') {
            this.editCategory(step);
          }
        }
      });
    }
  }

  /**
   * BasicQA Category List 초기화
   * @param step
   */
  initBasicQACategoryList(step) {
    for (let i = step; i < 4; i++) {
      this.basicQACategoryList[i] = null;
      this.selectedBasicQACategory[i] = null;
      if (this.currentClickData[i] != null) {
        this.currentClickData[i].style.backgroundColor = '#ffffff';
      }
    }
  }

  /**
   * 선택된 Category refresh 기능 (하위까지 포함)
   * @param step
   */
  refresh(step) {
    for (let i = step - 1; i < 4; i++) {
      this.selectedBasicQACategory[i] = null;
      this.inputBasicQACategory[i] = null;
      if (this.currentClickData[i] != null) {
        this.currentClickData[i].style.backgroundColor = '#ffffff';
      }
    }

    if (step !== 4) {
      this.initBasicQACategoryList(step);
    }
  }

  changeSource() {
    this.refresh(1);
    this.initBasicQACategoryList(0);
    this.basicQACategoryList[0] = this.treeData[this.selectedSource];
  }

  getErrorState(step) {
    if (this.inputBasicQACategory[step - 1] === null) {
      return true;
    } else if (step === 1 && this.matcher.isErrorState(this.channelFormControl)) {
      return true;
    } else if (step === 2 && this.matcher.isErrorState(this.categoryFormControl)) {
      return true;
    } else if (step === 3 && this.matcher.isErrorState(this.subCategoryFormControl)) {
      return true;
    } else if (step === 4 && this.matcher.isErrorState(this.subSubCategoryFormControl)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * BasicQA Category 추가
   * @param step
   */
  addBasicQACategory(step) {
    if (this.getErrorState(step)) {
      this.openAlertDialog('Failed', 'Please Input Category', 'error');
    } else {
      let basicQACategory: BasicQACategoryEntity = new BasicQACategoryEntity();
      basicQACategory.catName = this.inputBasicQACategory[step - 1].trim();

      if (step === 1) {
        basicQACategory.catPath = this.inputBasicQACategory[step - 1].trim();
        basicQACategory.parCatId = this.selectedSource;
      } else {
        basicQACategory.catPath = `${this.selectedBasicQACategory[step - 2].catPath}>${this.inputBasicQACategory[step - 1].trim()}`;
        basicQACategory.parCatId = this.selectedBasicQACategory[step - 2].id;
      }
      this.basicQACategoryService.insertBasicQACategory(basicQACategory).subscribe(
        res => {
          // insert에 성공한 경우
          if (res.message === 'INSERT_SUCESS') {
            this.basicQACategoryList[step - 1].push(res.category);
            this.inputBasicQACategory[step - 1] = null;
            this.openAlertDialog('Success', `${basicQACategory.catName} Insert Success.`, 'success');
          } else if (res.message === 'INSERT_DUPLICATED') {
            this.openAlertDialog('Duplicated', `${basicQACategory.catName} Duplicated.`, 'error');
          }
        },
        err => {
          this.openAlertDialog('Failed', `Error. ${basicQACategory.catName} insert Failed.`, 'error');
        }
      );
    }
  }

  /**
   * BasicQA Category 수정
   * @param step
   */
  editCategory(step) {
    let basicQACategory: BasicQACategoryEntity = new BasicQACategoryEntity();
    basicQACategory = JSON.parse(JSON.stringify(this.selectedBasicQACategory[step - 1]));
    basicQACategory.catName = this.inputBasicQACategory[step - 1].trim();
    basicQACategory['groupCodeName'] = this.selectedSource;
    let categoryTempList: string[] = [];

    if (step === 1) {
      basicQACategory.catPath = this.inputBasicQACategory[step - 1].trim();
    } else {
      for (let i = step - 1; i < 4; i++) {
        if (this.selectedBasicQACategory[i]) {
          let oldPaths: string[] = this.selectedBasicQACategory[i].catPath.split('>');
          oldPaths.splice(oldPaths.indexOf(this.selectedBasicQACategory[step - 1].catName), 1, this.inputBasicQACategory[step - 1].trim());
          let newPath: string = oldPaths.join('>');
          categoryTempList[i] = newPath;
        }
      }
      basicQACategory.catPath = categoryTempList[step - 1];
    }

    this.basicQACategoryService.updateBasicQACategory(basicQACategory).subscribe(
      res => {
        // update에 성공한 경우
        if (res.message === 'UPDATE_SUCESS') {
          let changeCategory = this.basicQACategoryList[step - 1].find(key => key.id === res.category.id);
          changeCategory.catName = res.category.catName;
          changeCategory.catPath = res.category.catPath;
          this.selectedBasicQACategory[step - 1] = changeCategory;
          this.selectedBasicQACategory.forEach((result, index) => {
            if (categoryTempList[index]) {
              result.catPath = categoryTempList[index];
            }
          });
          this.inputBasicQACategory[step - 1] = null;
          this.openAlertDialog('Success', `${basicQACategory.catName} Edit Success.`, 'success');
        } else if (res.message === 'UPDATE_DUPLICATED') {
          this.openAlertDialog('Duplicated', `${basicQACategory.catName} Duplicated`, 'error');
        }
      },
      err => {
        this.openAlertDialog('Failed', `Error. ${basicQACategory.catName} Update Failed.`, 'error');
      }
    );
  }

  /**
   * BasicQA Category 삭제
   * @param step
   * @param category
   */
  deleteBasicQACategory(step, category) {
    this.basicQACategoryService.deleteBasicQACategory(category).subscribe(
      res => {
        // delete에 성공한 경우
        if (res.message === 'DELETE_SUCESS') {
          this.basicQACategoryList[step - 1]
          .splice(this.basicQACategoryList[step - 1].indexOf(category), 1);
          this.inputBasicQACategory[step - 1] = null;
          this.selectedBasicQACategory[step - 1] = null;
          this.openAlertDialog('Success', `${category.catName} Delete Success.`, 'success');
        }
      },
      err => {
        this.openAlertDialog('Failed', `Error. ${category.catName} delete failed.`, 'error');
      }
    );
  }
}
