import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {BasicQAService} from '../../services/items/basicqa.items.service';
import {DatePipe} from '@angular/common';
import {ParaphraseUpsertDialogComponent} from './paraphrase-upsert-dialog.component';
import {BasicQAParaphraseService} from '../../services/paraphrase/basicqa.paraphrase.service';
import {BasicQAParaphraseMainWordEntity} from '../../entity/paraphrase/basicqa.paraphrase-main-word.entity';
import {
  AlertComponent, AppEnumsComponent, ConfirmComponent, TableComponent,
  UploaderButtonComponent
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-basicqa-paraphrase',
  templateUrl: './paraphrase.component.html',
  styleUrls: ['./paraphrase.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [BasicQAService, DatePipe]

})
export class ParaphraseComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  actionsArray: any[];
  pageParam: MatPaginator;
  pageLength: number;
  searchWords: string;
  searchMainWord: string;
  useYn;

  header = [
    {attr: 'checkbox', name: 'CheckBox', type: 'text', checkbox: true, input: false},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'word', name: 'MainWord', type: 'text', checkbox: false, input: false, isSort: true},
    {attr: 'basicQAParaphraseWordList', name: 'Words', type: 'text', checkbox: false, input: false},
    {
      attr: 'useYn',
      name: 'Use',
      type: 'text',
      checkbox: false,
      input: false,
      width: '7%',
      isSort: true
    },
    {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '7%'},
  ];
  basicQAMainWordList: any[] = [];
  dataSource: MatTableDataSource<any>;
  param = new BasicQAParaphraseMainWordEntity();
  init = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private basicQAParaphraseService: BasicQAParaphraseService,
              public appEnum: AppEnumsComponent,
              private dialog: MatDialog,
              private datePipe: DatePipe,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popUpsertDialog,
        params: 'add'
      },
      {
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeItems,
        type: 'delete'
      },
    ];
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.getNextPage();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }


  search(matSort?: MatSort) {
    if (matSort) {
      this.param.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'desc' : matSort.direction;
      this.param.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
    }
    if (this.init) {
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    if (!this.init) {
      this.init = true;
    }

    this.param.searchWords = this.searchWords;
    this.param.searchMainWord = this.searchMainWord;
    this.param.useYn = this.useYn;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.basicQAParaphraseService.getMainWords(this.param).subscribe(res => {
      if (res) {
        this.tableComponent.isCheckedAll = false;
        this.basicQAMainWordList.length = 0;
        res.content.map(val => {
          val.convertedCreateAt = this.datePipe.transform(val.createAt, 'yyyy-MM-dd hh: mm');
          val.basicQAParaphraseWordList = '';
          val.basicQAParaphraseRelEntities.map(basicQAParaphraseRelEntity => {
            if (val.basicQAParaphraseWordList === '') {
              val.basicQAParaphraseWordList = basicQAParaphraseRelEntity.basicQAParaphraseWordEntity.word;
            } else {
              val.basicQAParaphraseWordList += ',' + basicQAParaphraseRelEntity.basicQAParaphraseWordEntity.word;
            }
          })
        });

        this.basicQAMainWordList = res.content;
        this.pageLength = res.totalElements;
        this.dataSource = new MatTableDataSource(this.basicQAMainWordList);
      }
    });
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.init) {
      this.getNextPage();
    }
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  removeItems = () => {
    let toBeRemoved = [];
    this.basicQAMainWordList.forEach(basicQAMainWordEntity => {
      if (basicQAMainWordEntity.isChecked) {
        toBeRemoved.push(basicQAMainWordEntity);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please Select Words to Removed', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Word?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.tableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.basicQAParaphraseService.deleteMainWords(toBeRemoved).subscribe(
          res => {
            this.getNextPage();
          },
          err => {
            this.openAlertDialog('Error', 'Server error. delete failed paraphase words', 'error');
            this.pageParam.pageIndex = originalPageIndex;
          });
      }
    });
  };

  popUpsertDialog = (type, row?) => {
    let paramData = {};

    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = row;
    } else {
      return false;
    }

    paramData['service'] = this.basicQAParaphraseService;
    paramData['entity'] = new BasicQAParaphraseMainWordEntity();

    let dialogRef = this.dialog.open(ParaphraseUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getNextPage();
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  onUploadFile = () => {
    this.getNextPage();
  };

  downloadFile = () => {
    let url = this.storage.get('mltApiUrl') + '/basicqa/paraphrase/download-file';
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };
}
