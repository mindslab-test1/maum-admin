import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {BasicQAQuestionService} from '../../services/question/basicqa.question.service';
import {BasicQAService} from '../../services/items/basicqa.items.service';
import {BasicQASkillEntity} from '../../entity/items/basicqa.skill.entity';
import {BasicQAQuestionEntity} from '../../entity/question/basicqa.question.entity';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AlertComponent} from 'app/shared';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-basicqa-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
})

export class QuestionComponent implements OnInit, OnDestroy, AfterViewInit {
  loading = false;

  skillList: BasicQASkillEntity[] = [];
  selectedSkill: BasicQASkillEntity;
  nTop: number;
  nTopFormControl: FormControl;
  textData: string;

  contextCate = [];
  selectedCategory1 = {children: []};
  selectedCategory2 = {children: []};
  selectedCategory3 = {children: []};
  selectedCategory4 = {children: []};
  tempCate = {children: []};

  result: any;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredSkillList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private basicQAQuestionService: BasicQAQuestionService,
              private basicQAService: BasicQAService,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  /**
   *  화면 초기화
   */
  ngOnInit() {
    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterSkill();
    });
    this.getSkillList();
    this.nTop = 3;
    this.nTopFormControl = new FormControl(3, [
      Validators.required,
      () => (this.nTop <= 0) ? {negativeInteger: true} : null
    ]);
  }

  ngAfterViewInit(): void {
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private filterSkill() {
    if (!this.skillList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredSkillList.next(this.skillList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredSkillList.next(
      this.skillList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }


  getSkillList() {
    this.basicQAService.getSkillList().subscribe(res => {
        this.skillList = res;
        this.filteredSkillList.next(this.skillList.slice());

      },
      err => {
        this.openAlertDialog('Error', 'Skill list not fetched.', 'error');
      }
    );
  }

  changeSkill() {
    this.getCategory().then(
      () => {
        this.initData();
      },
      (err) => {
        console.log('error', err);
        this.openAlertDialog('Failed', 'Failed Get CategoryData', 'error');
      });
  }

  getCategory() {
    return new Promise((resolve, reject) => {
      this.basicQAService.getCategory().subscribe(res => {
          if (res) {
            this.contextCate = res.channel;
            resolve();
          }
        },
        err => {
          reject(err);
        }
      );
    });
  }

  initData() {
    this.textData = null;
    this.result = null;
    this.nTop = 3;
    this.selectedCategory1 = null;
    this.selectedCategory2 = null;
    this.selectedCategory3 = null;
    this.selectedCategory4 = null;
  }

  question() {
    let param: BasicQAQuestionEntity = new BasicQAQuestionEntity();
    param.skillId = this.selectedSkill.id;
    param.nTop = this.nTop;
    param.question = this.textData;

    if (this.selectedCategory1) {
      param.channel = this.selectedCategory1['id'];
    }
    if (this.selectedCategory2) {
      param.category = this.selectedCategory2['id'];
    }
    if (this.selectedCategory3) {
      param.subCategory = this.selectedCategory3['id'];
    }
    if (this.selectedCategory4) {
      param.subSubCategory = this.selectedCategory4['id'];
    }
    this.loading = true;
    this.basicQAQuestionService.question(param).subscribe(
      res => {
        this.result = res;
        this.loading = false;
      },
      err => {
        console.error('err : ', err);
        this.openAlertDialog('Error', 'server error. Question failed.', 'error');
        this.loading = false;
      }
    )
  }

  clearSelectOption(valStr, dept) {
    // 최대 길이 설정
    let maxLength = 0;

    if (valStr === 'selectedCategory') {
      maxLength = 4;
    }

    for (let i = (dept + 1); i <= maxLength; i++) {
      this[valStr + i] = this.tempCate;
      // eval('this.' + valStr + i + ' = this.tempCate');
    }
  }

  clearChannel() {
    this.selectedCategory1 = null;
    this.selectedCategory2 = null;
    this.selectedCategory3 = null;
    this.selectedCategory4 = null;
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
