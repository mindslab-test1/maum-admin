import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Dav3SessionsComponent} from './components/dav3-sessions/dav3-sessions.component';
import {Dav3ChatLogsComponent} from './components/dav3-chat-logs/dav3-chat-logs.component';
import {Dav1SessionsComponent} from './components/dav1-sessions/dav1-sessions.component';
import {Dav1ChatLogsComponent} from './components/dav1-chat-logs/dav1-chat-logs.component';

const routes: Routes = [
  {
    path: 'dav3',
    component: Dav3SessionsComponent,
    children: [
      {
        path: ':sessionid',
        component: Dav3ChatLogsComponent
      }
    ]
  },
  {
    path: 'dav1',
    component: Dav1SessionsComponent,
    children: [
      {
        path: ':sessionid',
        component: Dav1ChatLogsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatbotLogRoutingModule {
}
