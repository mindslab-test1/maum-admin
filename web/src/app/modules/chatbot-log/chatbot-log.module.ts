import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ChatbotLogRoutingModule} from './chatbot-log-routing.module';
import {Dav1ChatLogsComponent} from './components/dav1-chat-logs/dav1-chat-logs.component';
import {Dav3ChatLogsComponent} from './components/dav3-chat-logs/dav3-chat-logs.component';
import {
  MatDatepickerModule,
  MatIconModule,
  MatSelectModule,
  MatSidenavModule
} from '@angular/material';
import {Dav3SessionsComponent} from './components/dav3-sessions/dav3-sessions.component';
import {Dav1SessionsComponent} from './components/dav1-sessions/dav1-sessions.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [Dav1ChatLogsComponent, Dav3ChatLogsComponent, Dav3SessionsComponent, Dav1SessionsComponent],
  imports: [
    CommonModule,
    ChatbotLogRoutingModule,
    MatSidenavModule,
    SharedModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule
  ]
})
export class ChatbotLogModule {
}
