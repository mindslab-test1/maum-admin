import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Dav3SessionsComponent} from './dav3-sessions.component';

describe('Dav3SessionsComponent', () => {
  let component: Dav3SessionsComponent;
  let fixture: ComponentFixture<Dav3SessionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Dav3SessionsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dav3SessionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
