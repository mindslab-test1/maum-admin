import {Component, OnInit, OnDestroy, ViewChild, TemplateRef} from '@angular/core';
import {Store} from '@ngrx/store';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {MatSnackBar, MatSidenav} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';

import {StatisticsApi} from '../../../../core/sdk';
import {ROUTER_LOADED} from '../../../../core/actions';
import {AppObjectManagerService, TableComponent, getErrorString} from '../../../../shared';
import {ExcelService} from '../../../../shared';
import {AuthService} from '../../../../core/auth.service';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-chat-log-v3-list',
  templateUrl: './dav3-chat-logs.component.html',
  styleUrls: ['./dav3-chat-logs.component.scss']
})

export class Dav3ChatLogsComponent implements OnInit {
  chatSession: any;
  panelToggleText: string;
  filterKeyword: FormControl;
  row: any;
  header;
  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('chatLogListTemplate') chatLogListTemplate: TemplateRef<any>;

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private objectManager: AppObjectManagerService,
              private snackBar: MatSnackBar,
              private excelService: ExcelService,
              private statisticsApi: StatisticsApi) {
  }

  ngOnInit() {
    let chatSessionObj = this.objectManager.get('chatSessionV3');
    this.chatSession = chatSessionObj;
    console.log('#@ this.chatSession :', this.chatSession);

    if (chatSessionObj === undefined) {
      this.backPage();
      return;
    }
    this.header = [
      {attr: 'SEQ', name: 'Seq', template: this.chatLogListTemplate},
      {attr: 'SKILL', name: 'Skill'},
      {attr: 'INTEXT', name: 'Question', styleTextLimit: true},
      {attr: 'OUTTEXT', name: 'Answer', styleTextLimit: true},
      {attr: 'STARTTIME', name: 'StartTime'},
      {attr: 'ENDTIME', name: 'EndTime'},
    ];

    setTimeout(() => {
      this.getChatList();
      this.store.dispatch({type: ROUTER_LOADED});
    }, 0);
  };

  getChatList: any = () => {
    this.statisticsApi.executeChatV3ListQuery({sessionId: this.chatSession.ID})
    .subscribe(
      resultSet => {
        this.rows = resultSet;
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      }
    );
  };

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  };

  backPage: any = () => {
    this.objectManager.clean('chatSessionV3');

    // fromSessionDetailPage boolean을 true로 저장
    let sessionList_info = this.objectManager.get('sessionListV3_info');
    if (sessionList_info) {
      sessionList_info.fromSessionDetailPage = true;
    }
    this.router.navigate(['../view-session-v3-list'], {relativeTo: this.route});
  };
}
