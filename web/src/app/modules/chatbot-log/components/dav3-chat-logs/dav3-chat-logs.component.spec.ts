import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Dav3ChatLogsComponent} from './dav3-chat-logs.component';

describe('Dav3ChatLogsComponent', () => {
  let component: Dav3ChatLogsComponent;
  let fixture: ComponentFixture<Dav3ChatLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Dav3ChatLogsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dav3ChatLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
