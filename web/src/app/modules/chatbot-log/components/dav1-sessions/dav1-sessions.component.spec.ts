import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Dav1SessionsComponent} from './dav1-sessions.component';

describe('Dav1SessionsComponent', () => {
  let component: Dav1SessionsComponent;
  let fixture: ComponentFixture<Dav1SessionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Dav1SessionsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dav1SessionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
