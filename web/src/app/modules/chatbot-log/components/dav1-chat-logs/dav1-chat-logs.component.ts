import {Component, OnInit, OnDestroy, ViewChild, TemplateRef} from '@angular/core';
import {Store} from '@ngrx/store';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {MatSnackBar, MatSidenav} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';

import {AppObjectManagerService, TableComponent, getErrorString} from '../../../../shared';
import {MatTableDataSource} from '@angular/material/table';
import {ROUTER_LOADED} from '../../../../core/actions';
import {StatisticsApi} from '../../../../core/sdk/services/custom';

@Component({
  selector: 'app-chat-log-list',
  templateUrl: './dav1-chat-logs.component.html',
  styleUrls: ['./dav1-chat-logs.component.scss']
})

export class Dav1ChatLogsComponent implements OnInit {
  chatSession: any;
  panelToggleText: string;
  // table = {header: [], rows: [], paginationOff: false};
  filterKeyword: FormControl;
  subscription = new Subscription();
  row: any;
  header;
  rows: any[] = [];
  dataSource: MatTableDataSource<any>;

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private objectManager: AppObjectManagerService,
              private snackBar: MatSnackBar,
              private statisticsApi: StatisticsApi) {
  }

  ngOnInit() {
    let chatSessionObj = this.objectManager.get('chatSession');
    this.chatSession = chatSessionObj;

    if (chatSessionObj === undefined) {
      this.backPage();
      return;
    }
    this.header = [
      {attr: 'SEQ', name: 'Seq'},
      {attr: 'SKILL', name: 'Skill'},
      {attr: 'INTEXT', name: 'Question', styleTextLimit: true},
      {attr: 'OUTTEXT', name: 'Answer', styleTextLimit: true},
      {attr: 'STARTAT', name: 'StartAt'},
      {attr: 'ENDAT', name: 'EndAt'},
    ];
    setTimeout(() => {
      this.getChatList();
      this.store.dispatch({type: ROUTER_LOADED});
    }, 0);
  };

  getChatList: any = () => {
    this.statisticsApi.executeChatListQuery({sessionId: this.chatSession.ID})
    .subscribe(
      resultSet => {
        this.rows = resultSet;
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
      }
    );
  };


  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  };

  backPage: any = () => {
    this.objectManager.clean('chatSession');

    // fromSessionDetailPage boolean을 true로 저장
    let sessionList_info = this.objectManager.get('sessionList_info');
    if (sessionList_info) {
      sessionList_info.fromSessionDetailPage = true;
    }
    this.router.navigate(['../view-session-list'], {relativeTo: this.route});
  };
}
