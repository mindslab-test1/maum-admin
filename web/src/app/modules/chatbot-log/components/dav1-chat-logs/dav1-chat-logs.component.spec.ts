import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Dav1ChatLogsComponent} from './dav1-chat-logs.component';

describe('Dav1ChatLogsComponent', () => {
  let component: Dav1ChatLogsComponent;
  let fixture: ComponentFixture<Dav1ChatLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Dav1ChatLogsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dav1ChatLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
