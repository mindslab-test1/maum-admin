import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DictTestResultDetailRoutingModule} from './dict-test-result-detail-routing.module';
import {TestResultDetailComponent} from './components/test-result-detail/dict-test-result-detail.component';
import {SharedModule} from '../../shared/shared.module';
import {AgGridModule} from 'ag-grid-angular';
import {MatButtonModule, MatIconModule, MatToolbarModule} from '@angular/material';
import {TestResultDetailService} from './services/test-result-detail/test-result-detail.service';
import {TestResultService} from './services/test-result/test-result.service';

@NgModule({
  declarations: [TestResultDetailComponent],
  imports: [
    CommonModule,
    DictTestResultDetailRoutingModule,
    SharedModule,
    AgGridModule.withComponents([]),
    MatToolbarModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [
    TestResultDetailComponent
  ],
  providers: [
    TestResultService,
    TestResultDetailService
  ]
})
export class DictTestResultDetailModule {
}
