import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TestResultDetailComponent} from './components/test-result-detail/dict-test-result-detail.component';

const routes: Routes = [
  {
    path: ':type/:name/:testId',
    component: TestResultDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictTestResultDetailRoutingModule {
}
