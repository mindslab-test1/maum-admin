import {Component, Inject, OnInit} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {AlertComponent} from 'app/shared';
import {ActivatedRoute, Router} from '@angular/router';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {TestResultDetailService} from '../../services/test-result-detail/test-result-detail.service';

interface ResultDetail {
  sentence: string;
  nes: string;
  morphs: string;
  replacements: string;
}

enum DicType {
  NER = 0, // 개체명 사전
  MORPH = 1, // 형태소 사전
  REPLACEMENT = 2, // 치환 사전
}

@Component({
  selector: 'app-test-result-detail',
  templateUrl: './dict-test-result-detail.component.html',
  styleUrls: ['./dict-test-result-detail.component.scss']
})
export class TestResultDetailComponent implements OnInit {
  public gridOptions: GridOptions;
  public defaultColDef;

  dictionaryName = '';
  dictionaryType = '';
  testId = 0;
  dictionaryId = '';
  row_data: ResultDetail[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private testResultDetailService: TestResultDetailService,
              private store: Store<any>,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      defaultColDef: this.defaultColDef,
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };
    this.route.params.subscribe(par => {
      this.dictionaryName = par['name'];
      this.dictionaryType = par['type'];
      this.testId = parseInt(par['testId'], 10);

      if (this.dictionaryType.toUpperCase() in DicType && this.dictionaryName !== '' && this.testId > 0) {
        this.getData();
      } else {
        this.back('Check your url');
      }
    }, err => {
      this.openAlertDialog('Load', 'Something wrong.', 'error');
      window.history.back();
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 1,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
      hide: true
    }, {
      headerName: 'Sentence',
      field: 'sentence',
      width: 100,
      editable: false
    }, {
      headerName: 'Morphs',
      field: 'morphs',
      width: 50,
      editable: false
    }, {
      headerName: 'NES',
      field: 'nes',
      width: 50,
      editable: false
    }
    ];
  }

  getData() {
    this.row_data = [];
    this.testResultDetailService.getResultDetail(this.dictionaryType, this.testId).subscribe(
      res => {
        if (res.hasOwnProperty('detail')) {
          let contents: ResultDetail[] = JSON.parse(res.detail.testResult);
          contents.forEach(one => {
            let elem: ResultDetail = {
              sentence: '',
              nes: '',
              morphs: '',
              replacements: ''
            };
            elem.sentence = one.sentence;
            elem.nes = one.nes;
            elem.morphs = one.morphs;
            this.row_data.push(elem);
          });
          if (DicType[this.dictionaryType.toUpperCase()] === DicType.NER) {
            this.gridOptions.columnApi.setColumnVisible('morphs', false);
            this.gridOptions.columnApi.setColumnVisible('nes', true);
          } else if (DicType[this.dictionaryType.toUpperCase()] === DicType.MORPH) {
            this.gridOptions.columnApi.setColumnVisible('nes', false);
            this.gridOptions.columnApi.setColumnVisible('morphs', true);
          }
          this.gridOptions.api.setRowData(this.row_data);
        }
      }, err => {
        this.openAlertDialog('Load Result', 'Something wrong!', 'error');
      }
    );
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
  }

  back = (msg: string, history = false) => {
    if (msg !== '') {
      this.openAlertDialog('Error', msg, 'error');
    }
    if (this.dictionaryName !== '' && this.dictionaryType.toUpperCase() === DicType[0]) {
      this.router.navigate(['../../../../ner', this.dictionaryName, 'test'], {relativeTo: this.route});
    } else if (this.dictionaryName !== '' && this.dictionaryType.toUpperCase() === DicType[1]) {
      this.router.navigate(['../../../../morps', this.dictionaryName, 'test'], {relativeTo: this.route});
    } else {
      window.history.back();
    }
  }
}
