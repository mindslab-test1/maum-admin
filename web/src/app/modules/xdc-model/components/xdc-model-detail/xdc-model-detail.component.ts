import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {
  AlertComponent,
  AppObjectManagerService,
  CommitDialogComponent,
  ConfirmComponent,
  TableComponent, UploaderButtonComponent
} from 'app/shared';
import {TreeViewComponent} from 'app/shared/components/tree/tree.component';
import {DictionaryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-upsert-dialog.component';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {DictionaryCategoryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-category-upsert-dialog.component';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {ModelInsertService} from '../../services/model-insert/model-insert.service';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';
import {XdcTrainingDataEntity} from '../../entity/xdc-training-data/xdc-training-data.entity';
import {XdcTrainingDataService} from '../../services/xdc-training-data/xdc-training-data.service';
import {XdcTrainingService} from '../../services/xdc-training/xdc-training.service';


@Component({
  selector: 'app-xdc-model',
  templateUrl: './xdc-model-detail.component.html',
  styleUrls: ['./xdc-model-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class XdcModelDetailComponent implements OnInit, OnDestroy {

  @ViewChild('lineTableComponent') lineTableComponent: TableComponent;
  @ViewChild('appTree') appTree: TreeViewComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  @Output() selectFiles: EventEmitter<string[]> = new EventEmitter<string[]>();

  selectedXdcDic = new XdcTrainingDataEntity();
  xdcData: any;
  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();

  pageParam: MatPaginator;
  pageLength: number;

  currentCategory: any = null;
  actions: any[] = [];
  model_actions: any = {};
  editable = false;
  isReadOnly: any = 'readOnly';
  isNew: any = 'readOnly';
  inputData = null;
  searchSentence: string;

  xdcDicLineEntity = new XdcTrainingDataEntity();
  categories: any = null;

  header = [];
  isCreated = false;

  defaultHeader = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', input: false, width: '35%'},
    {attr: 'category', name: 'Category', width: '35%'}
  ];

  editHeader = [
    {attr: 'checkbox', name: 'checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', input: true, width: '35%'},
    {attr: 'category', name: 'Category', width: '35%'}
  ];
  xdcDicLineList: any[] = [];
  dataSource: MatTableDataSource<any>;

  workspaceId = '';
  user = '';

  infoGroup1: any;
  infoGroup2: any;

  categoryCallback = {
    add: (next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.xdcTrainingDataService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        workspaceId: this.workspaceId,
        xdcDicId: this.selectedXdcDic.xdcDicId,
        isNew: !this.isCreated,
        creatorId: this.user,
        updaterId: this.user
      };
    },
    edit: (node: any, next: (node: any) => void) => {
      let ref: any = this.dialog.open(DictionaryCategoryUpsertDialogComponent);
      ref.componentInstance.service = this.xdcTrainingDataService;
      ref.componentInstance.next = next;
      ref.componentInstance.meta = {
        id: this.currentCategory.id,
        parentId: this.currentCategory.data.parentId,
        workspaceId: this.workspaceId,
        oldName: this.currentCategory.data.name,
        xdcDicId: this.selectedXdcDic.xdcDicId,
        isNew: !this.isCreated
      };
      ref.componentInstance.data = node;

      ref.afterClosed().subscribe(res => {
        if (res) {
          this.getXdcDicLineAllList();
        }
      });
    },
    remove: (node: XdcTrainingDataEntity, next: () => void) => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed()
      .subscribe(confirmed => {
        if (confirmed) {
          this.xdcTrainingDataService.deleteCategory(node).subscribe(
            res => {
              if (res.message === 'DELETE_SUCCESS') {
                this.openAlertDialog('Delete', `Category ${node.name} deleted.`, 'success');
                next();
              } else if (res.message === 'SENTENCE_EXISTENCE') {
                this.openAlertDialog('Failed', `There is a sentence in the category. Please delete the sentence.`, 'error');
              }
            },
            () => {
              this.openAlertDialog('Error', `Server Error. Category ${node.name} not deleted.`, 'error');
            }
          );
        }
      });
    },
    focus: (node: any) => {
      this.currentCategory = node;
      this.getXdcDicLineAllList(0);
    },
    blur: () => {
      this.currentCategory = null;
    },
  };

  constructor(private xdcTrainingDataService: XdcTrainingDataService,
              private xdcTrainingService: XdcTrainingService,
              private storage: StorageBrowser,
              private route: ActivatedRoute,
              private router: Router,
              private objectManager: AppObjectManagerService,
              private modelService: ModelInsertService,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actions = [
      {
        type: 'edit',
        text: 'Edit',
        icon: 'edit_circle_outline',
        callback: this.edit,
        hidden: false
      }, {
        type: 'train',
        text: 'Train',
        icon: 'school',
        callback: this.train,
        hidden: false
      },
      {
        type: 'save',
        text: 'Save',
        icon: 'save',
        callback: this.save,
        hidden: true
      },
    ];

    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.user = this.storage.get('user').id;
    this.header = this.defaultHeader;
    this.selectedXdcDic.name = '';
    this.selectedXdcDic.manDesc = '';
    this.model_actions = {
      type: 'Create',
      text: 'Create model',
      icon: 'add_circle_outline',
      callback: this.createModelInfo,
      hidden: true
    };

    this.xdcData = this.objectManager.get('xdcData');
    this.route.params.subscribe(par => {
      this.objectManager.clean('xdcData');
      if (par.hasOwnProperty('id')) {
        this.isCreated = true;
        if (par['id'] !== '') {
          this.selectedXdcDic.name = par['id'];
          if (!this.xdcData) { // objectManager로 넘어온 정보 없는 경우
            let param: ModelManagementEntity = new ModelManagementEntity();
            param.hmdUse = false;
            param.dnnUse = false;
            param.xdcUse = true;
            param.workspaceId = this.workspaceId;
            param.name = this.selectedXdcDic.name;
            param.id = '';
            this.modelService.getTaModel(param).subscribe(res => {
              if (res) {
                this.xdcData = res;
                this.isCreated = true;
                this.model_actions.hidden = true;
                this.selectedXdcDic = res;
                this.selectedXdcDic.manDesc = this.xdcData.manDesc;
                this.getXdcCategoryAllList();
              } else {
                this.openAlertDialog('Error', `Error. Check a name of xdc model in a url`, 'error');
                this.back();
              }
            }, () => {
              this.openAlertDialog('Error', `Error. Server Error [getXdcModel]`, 'error');
            });
          } else { // objectManager 로 정보 넘어온 경우
            this.selectedXdcDic.xdcDicId = this.xdcData.xdcDicId;
            this.selectedXdcDic.id = this.xdcData.id;
            this.selectedXdcDic.manDesc = this.xdcData.manDesc;
            this.getXdcCategoryAllList();
          }
          this.route.url.subscribe(url => {
            if (url.length > 1) {
              if (url[1].path === 'edit') {
                this.header = this.editHeader;
                this.updateToolbar();
              } else {
                this.openAlertDialog('Error', `Error. Check a url`, 'error');
                this.back();
              }
            }
          }, () => {
            this.openAlertDialog('Error', `Error. Server Error  [Init]`, 'error');
          });
        }
      } else {
        this.model_actions.hidden = false;
        this.isCreated = false;
        this.isReadOnly = false;
        this.isNew = false;
        this.store.dispatch({type: ROUTER_LOADED});
      }
    });

    this.infoGroup1 = new FormGroup({
      sentence: new FormControl('sentence'),
    });

    this.infoGroup2 = new FormGroup({
      name: new FormControl([this.selectedXdcDic.name, Validators.required]),
      manDesc: new FormControl('manDesc'),
    });
  }

  changeModelDesc = (param) => {
    if (this.isCreated) {
      this.editModelInfo(param);
    } else {
      this.selectedXdcDic.manDesc = param;
    }
  };

  changeModelName = (param) => {
    if (!this.isCreated) {
      this.selectedXdcDic.name = param;
    }
  };

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  back = () => {
    if (this.editable) {
      this.router.navigate(['..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  };

  editModelInfo = (desc: string) => {
    let modelManagementEntity: ModelManagementEntity = new ModelManagementEntity();
    modelManagementEntity.hmdUse = false;
    modelManagementEntity.dnnUse = false;
    modelManagementEntity.xdcUse = true;
    modelManagementEntity.hmdDicId = '';
    modelManagementEntity.dnnDicId = '';
    modelManagementEntity.workspaceId = this.workspaceId;
    modelManagementEntity.updaterId = this.user;
    modelManagementEntity.manDesc = desc === '' ? this.selectedXdcDic.manDesc : desc;
    modelManagementEntity.name = null;
    modelManagementEntity.id = this.selectedXdcDic.id;
    modelManagementEntity.xdcDicId = this.selectedXdcDic.xdcDicId;
    this.modelService.updateTaModel(modelManagementEntity).subscribe(() => {
    }, () => {
      this.openAlertDialog('Error', `Change xdc model name.`, 'error');
    });
  };

  createModelInfo = () => {
    let modelManagementEntity: ModelManagementEntity = new ModelManagementEntity();
    modelManagementEntity.hmdUse = false;
    modelManagementEntity.dnnUse = false;
    modelManagementEntity.xdcUse = true;
    modelManagementEntity.workspaceId = this.workspaceId;
    modelManagementEntity.creatorId = this.user;
    modelManagementEntity.updaterId = this.user;
    modelManagementEntity.manDesc = this.selectedXdcDic.manDesc;
    modelManagementEntity.name = this.selectedXdcDic.name;
    modelManagementEntity.xdcDicId = this.selectedXdcDic.xdcDicId;
    if (this.selectedXdcDic.name === '') {
      this.openAlertDialog('Error', `Input XDC model name`, 'error');
    } else {
      this.isNew = 'readOnly';
      this.modelService.insertTaModel(modelManagementEntity).subscribe(res => {
        this.isCreated = true;
        this.model_actions.hidden = true;
        this.selectedXdcDic = res;
        this.xdcData = res;
        this.objectManager.set('xdc', this.selectedXdcDic);
        this.router.navigate(['..', this.selectedXdcDic.name, 'edit'], {relativeTo: this.route});
      });
    }
  };

  getXdcCategoryAllList() {
    let promise = new Promise((resolve, reject) => {
      this.xdcTrainingDataService.getXdcCategoryAllList(this.workspaceId, this.selectedXdcDic.xdcDicId).subscribe(
        res => {
          if (res) {
            this.categories = res;
            resolve();
          }
        }, () => {
          this.openAlertDialog('Error', `Server error. Failed to fetch categories.`, 'error');
          reject();
        }
      );
    });

    promise.then(() => {
      this.getXdcDicLineAllList();
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  edit = () => {
    this.objectManager.set('xdc', this.selectedXdcDic);
    this.router.navigate(['edit'], {relativeTo: this.route});
  };

  testWithFiles = () => {
    this.router.navigate(['analysis'], {relativeTo: this.route});
  };

  testWithText = () => {
    this.router.navigate(['test'], {relativeTo: this.route});
  };

  addLine = (sentence = ' ') => {
    if (!this.currentCategory || !this.currentCategory['isLeaf']) {
      this.openAlertDialog('Failed', `Cannot add a line to non-leaf category.`, 'error');
      return false;
    }
    this.xdcDicLineEntity = new XdcTrainingDataEntity();
    this.xdcDicLineEntity.category = this.currentCategory.data.name;
    this.xdcDicLineEntity.sentence = sentence;
    this.xdcDicLineEntity.versionId = this.selectedXdcDic.xdcDicId;

    this.xdcTrainingDataService.insertLine(this.xdcDicLineEntity).subscribe(
      () => {
        this.xdcDicLineList.unshift({category: this.currentCategory.data.name, sentence: null});
        this.getXdcDicLineAllList().then(() => {
          this.appTree.currentNode.data.count += 1;
        });
      }, () => {
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  };

  setLeafChildrenCount(node, xdcDicLineList) {
    if (node.children.length > 0) {
      node.children.forEach(childChildren => {
        this.setLeafChildrenCount(childChildren, xdcDicLineList);
      });
    } else {
      let findNodeCount: number = xdcDicLineList.filter(xdcDicLine => node.name === xdcDicLine.category).length;
      if (findNodeCount > 0) {
        node.count -= findNodeCount;
      }
    }
  }

  deleteLine = () => {
    let xdcTrainingDataEntities: XdcTrainingDataEntity[] = this.lineTableComponent.rows.filter(item => item.isChecked);

    if (xdcTrainingDataEntities.length === 0) {
      this.openAlertDialog('Error', `Please select the checkbox.`, 'error');
    } else {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'Do you want delete ?';
      ref.afterClosed().subscribe(confirmed => {
        if (confirmed) {
          let originalPageIndex: number = this.pageParam.pageIndex;
          if (xdcTrainingDataEntities.length === this.lineTableComponent.rows.length) {
            if (this.pageParam.pageIndex === 0) {
              this.pageParam.pageIndex = 0;
            } else {
              this.pageParam.pageIndex -= 1;
            }
          }

          this.xdcTrainingDataService.deleteLines(xdcTrainingDataEntities).subscribe(
            res => {
              if (res) {
                this.getXdcDicLineAllList().then(() => {
                  this.appTree.nodes.forEach(node => {
                    this.setLeafChildrenCount(node, xdcTrainingDataEntities);
                  });
                  this.openAlertDialog('Delete', `The selected sentence has been deleted.`, 'success');
                });
              }
            },
            () => {
              this.pageParam.pageIndex = originalPageIndex;
              this.openAlertDialog('Error', `Server Error. Can not delete selected sentences.`, 'error');
            }
          );
        }
      });
    }
  };

  deleteDupLine = () => {
    if (this.currentCategory === null) {
      this.openAlertDialog('Notice', `select category please.`, 'notice');
    } else {
      this.xdcTrainingDataService.getDuplicatesLines(this.selectedXdcDic.xdcDicId, this.currentCategory.data.name).subscribe(
        res => {
          if (res['resultCode'] === 'SUCCESS') {
            let resultMessage: string;
            res['dicLine'].forEach(item => {
              resultMessage += `
                <b>Sentence</b> : ${item.sentence}  <b>Count</b>: [${item.duplicateCount}]<br>
            `;
            });
            let ref = this.dialog.open(ConfirmComponent);
            ref.componentInstance.message = `
                There are ${res['dicLine'].length} kinds of duplicate sentences.<br>
                <br>${resultMessage}<br>
                Do you want to clear above duplicates?
          `;
            ref.afterClosed().subscribe(
              confirmed => {
                if (confirmed) {
                  this.xdcDicLineEntity = new XdcTrainingDataEntity();
                  this.xdcDicLineEntity.versionId = this.selectedXdcDic.xdcDicId;
                  this.xdcDicLineEntity.category = this.currentCategory.data.name;

                  this.xdcTrainingDataService.deleteDuplicatesLines(this.xdcDicLineEntity).subscribe(
                    result => {
                      if (result) {
                        let sum = 0;
                        res['dicLine'].forEach(dicLine => {
                          // 자기 자신을 제외한 dicLine 삭제 하기 때문에 -1을 해줌
                          sum += (dicLine.duplicateCount - 1);
                        });

                        this.getXdcDicLineAllList(0).then(() => {
                          this.appTree.currentNode.data.count -= sum;
                        });
                      }
                    }, () => {
                      this.openAlertDialog('Error', `Server Error. Can not deleted sentences.`, 'error');
                    }
                  )
                }
              });
          } else if (res['resultCode'] === 'NO_DUPLICATION') {
            this.openAlertDialog('No Duplication', `There are no duplicate sentence in this category.`, 'success');
          }
        },
        () => {
          this.openAlertDialog('Error', `Server Error. Can not deleted sentences.`, 'error');
        }
      )
    }
  };

  save = () => {
    let ref = this.dialog.open(CommitDialogComponent);
    ref.componentInstance.title = `${this.selectedXdcDic.name} updated`;

    ref.afterClosed().subscribe(
      result => {
        if (result) {
          let param = new XdcTrainingDataEntity();
          param.id = this.selectedXdcDic.xdcDicId;
          param.name = this.selectedXdcDic.name;
          param.workspaceId = this.selectedXdcDic.workspaceId;
          param.title = result.title;
          param.message = result.message;

          this.xdcTrainingDataService.commit(param).subscribe(
            () => {
              this.header = this.defaultHeader;
              this.router.navigate(['..'], {relativeTo: this.route});
            },
            () => {
              this.openAlertDialog('Failed', `Something wrong. Failed to commit.`, 'error');
            });
        }
      });
  };

  onUploadFile = () => {
    this.actions.forEach(action => action.disabled = true);
    this.selectFiles.emit([]);
    this.getXdcCategoryAllList();
  };

  updateToolbar = () => {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'train') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    this.isReadOnly = this.editable ? false : 'readOnly';
  };

  add = () => {
    let ref: any = this.dialog.open(DictionaryUpsertDialogComponent);
    ref.componentInstance.dicApi = this.xdcTrainingDataService;
    ref.componentInstance.workspaceId = this.workspaceId;
  };

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }
    if (this.xdcDicLineList.length > 0) {
      this.getXdcDicLineAllList();
    }
  };

  getXdcDicLineAllList(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }

    this.xdcDicLineEntity = new XdcTrainingDataEntity();
    this.xdcDicLineEntity.versionId = this.selectedXdcDic.xdcDicId;
    if (this.currentCategory) {
      this.xdcDicLineEntity.category = this.currentCategory.data.name;
    }

    this.xdcDicLineEntity.sentence = this.searchSentence;
    this.xdcDicLineEntity.pageSize = this.pageParam.pageSize;
    this.xdcDicLineEntity.pageIndex = this.pageParam.pageIndex;

    return new Promise(resolve => {
      this.xdcTrainingDataService.getAllDicLines(this.xdcDicLineEntity).subscribe(
        res => {
          res.dicLineList.forEach(data => {
            if (data.sentence === ' ') {
              data.sentence = '';
            }
          })
          if (res) {
            this.xdcDicLineList = res['dicLineList'];
            this.lineTableComponent.isCheckedAll = false;
            this.pageLength = res['total'];
          }
          resolve();
        },
        () => {
          this.openAlertDialog('Error', `Server Error. Failed to fetch sentences.`, 'error');
        }
      )
    });
  }

  train = () => {
    this.router.navigate(['train'], {relativeTo: this.route});
  };

  changeSentence(data) {
    this.inputData = data;
    this.xdcDicLineEntity = new XdcTrainingDataEntity();
    this.xdcDicLineEntity.seq = this.inputData.seq;
    this.xdcDicLineEntity.versionId = this.selectedXdcDic.xdcDicId;
    this.xdcDicLineEntity.category = this.inputData.category;
    this.xdcDicLineEntity.sentence = this.inputData.sentence;

    this.xdcTrainingDataService.insertLine(this.xdcDicLineEntity).subscribe(
      () => {
      }, () => {
        this.openAlertDialog('Error', `ServerError. Unable to add.`, 'error');
      });
  }

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
