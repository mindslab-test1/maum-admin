import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {XdcModelDetailComponent} from './xdc-model-detail.component';

describe('XdcModelDetailComponent', () => {
  let component: XdcModelDetailComponent;
  let fixture: ComponentFixture<XdcModelDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XdcModelDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XdcModelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
