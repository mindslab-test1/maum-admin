import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {
  ErrorStateMatcher,
  MatDialog,
  MatTableDataSource,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {Subject} from 'rxjs';
import {AlertComponent, FormErrorStateMatcher, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {XdcTestEntity} from '../../entity/xdc-test/xdc-test.entity';
import {XdcTestService} from '../../services/xdc-test/xdc-test.service';
import {XdcResultEntity} from '../../entity/xdc-result/xdc-result.entity';

@Component({
  selector: 'app-ta-xdc-analysis-test',
  templateUrl: './xdc-analysis-test.component.html',
  styleUrls: ['./xdc-analysis-test.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None
})
export class XdcAnalysisTestComponent implements OnInit, OnDestroy {

  @ViewChild('tableComponent') tableComponent: TableComponent;

  private _onDestroy = new Subject<void>();

  selectedModel: XdcTestEntity = null;

  textData: string;
  workspaceId: string;
  error: string;
  loading = false;
  textFormControl = new FormControl('', [
    Validators.required,
  ]);
  submitButton = true;

  // table
  header = [
    {attr: 'no', name: 'No.', no: true, isSort: true, width: '10%'},
    {attr: 'textData', name: 'Text Data', width: '40%'},
    {attr: 'category', name: 'Category', isSort: true, width: '25%'},
    {attr: 'probability', name: 'Probability', isSort: true, width: '25%'},
  ];
  rows: XdcResultEntity[] = [];
  dataSource: MatTableDataSource<any>;
  param: XdcTestEntity;
  testResult: XdcResultEntity[] = [];

  constructor(private storage: StorageBrowser,
              public matcher: FormErrorStateMatcher,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private xdcTestService: XdcTestService,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.selectedModel = new XdcTestEntity();
    this.selectedModel.name = '';
    this.route.params.subscribe(par => {
      let name = par['id'];
      if (!name || name === '') {
        this.back('Error. Check your model name in a url.');
      }
      this.selectedModel.name = name;
      this.selectedModel.workspaceId = this.workspaceId;
      this.xdcTestService.getModel(this.selectedModel.name , this.selectedModel.workspaceId).subscribe(res => {
        if (!res) {
          this.back('Error. Check your model name in a url.');
        } else {
          this.selectedModel.name = res.name;
          this.selectedModel.xdcKey = res.xdcKey;
        }
      });
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.selectedModel.name === '') {
      this.router.navigate(['..', '..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }

  isSubmit() {
    if (this.textData !== undefined && this.textData.length > 0) {
      this.submitButton = false;
    } else {
      this.submitButton = true;
    }
  }

  submit = () => {
    this.param = new XdcTestEntity();
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');
    this.param.name = this.selectedModel.name;
    this.param.textData = this.textData;
    this.param.creatorId = this.storage.get('user').id;
    this.param.xdcKey = this.selectedModel.xdcKey;

    if (!this.submitButton) {
      this.loading = true;

      this.xdcTestService.taXdcAnalyzeText(this.param).subscribe(
        res => {
          let test_result: XdcResultEntity[] = [];
          if (res && res.xdc_result) {
            let xdc_result: XdcResultEntity[] = res.xdc_result;
            this.testResult = res.xdc_result;
            let cnt = 1;
            for (let elem of this.testResult) {
              let entity: XdcResultEntity = new XdcResultEntity();
              entity.no = cnt;
              cnt = cnt + 1;
              entity.category = elem.category;
              entity.textData = elem.textData;
              entity.probability = elem.probability;
              test_result.push(entity);
            }

            this.rows = test_result;
            this.loading = false;
          }
        },
        err => {
          this.openAlertDialog('Failed', 'Server error. test failed.', 'error');
          this.loading = false;
        }
      );
    }
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
