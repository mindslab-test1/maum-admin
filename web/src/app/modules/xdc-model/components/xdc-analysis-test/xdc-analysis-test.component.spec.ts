import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {XdcAnalysisTestComponent} from './xdc-analysis-test.component';

describe('XdcAnalysisTestComponent', () => {
  let component: XdcAnalysisTestComponent;
  let fixture: ComponentFixture<XdcAnalysisTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XdcAnalysisTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XdcAnalysisTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
