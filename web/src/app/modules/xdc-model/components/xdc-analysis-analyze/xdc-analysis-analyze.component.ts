import {ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AlertComponent, ConfirmComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {ModelInsertService} from '../../services/model-insert/model-insert.service';
import {XdcAnalyzeService} from '../../services/xdc-analyze/xdc-analyze.service';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';
import {XdcAnalyzeEntity} from '../../entity/xdc-analyze/xdc-analyze.entity';
import {XdcResultService} from '../../services/xdc-result/xdc-result.service';

interface ModelInfo {
  id: string;
  type: string;
  name: string;
  workspaceId: string;
  creatorId: string;
}

@Component({
  selector: 'app-ta-xdc-analyze',
  templateUrl: './xdc-analysis-analyze.component.html',
  styleUrls: ['./xdc-analysis-analyze.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ModelInsertService]
})
export class XdcAnalysisAnalyzeComponent implements OnInit {
  model: ModelInfo = {
    id: '',
    type: 'xdc',
    name: '',
    workspaceId: '',
    creatorId: '',
  };
  fileList: string[] = [];
  isTest: boolean;
  workspaceId: string;

  constructor(private storage: StorageBrowser,
              private xdcResultService: XdcResultService,
              private router: Router,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private xdcAnalyzeService: XdcAnalyzeService,
              public cdr: ChangeDetectorRef,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.route.params.subscribe(par => {
      let name = par['id'];
      if (!name || name === '') {
        this.back('Error. Check your model name in a url.');
      }
      let param: ModelManagementEntity = new ModelManagementEntity();
      param.workspaceId = this.workspaceId;
      param.name = name;
      this.xdcAnalyzeService.getModel(this.workspaceId, name).subscribe(res => {
        if (!res) {
          this.back('Error. Check your model name in a url.');
        } else {
          this.model.name = res.name;
          this.model.workspaceId = res.workspaceId;
          this.model.id = res.xdcKey;
          this.model.creatorId = this.storage.get('user').id;
        }
      })
    });
    this.store.dispatch({type: ROUTER_LOADED});
    this.cdr.detectChanges();
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.model.name === '') {
      window.history.back();
    } else {
      // 목록으로 이동
      this.router.navigate(['..'], {relativeTo: this.route});
    }
  }

  gotoResult() {
    this.router.navigate(['..', 'analysis-result'], {relativeTo: this.route});
  }

  selectFiles = (event) => {
    this.fileList = event;
    this.checkButton();
  };

  checkButton = () => {
    if (this.fileList.length > 0) {
      this.isTest = true;
    } else {
      this.isTest = false;
    }
  };

  test = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to start XDC model testing?';

    ref.afterClosed().subscribe(result => {
      if (result) {
        let param = new XdcAnalyzeEntity();
        param.workspaceId = this.model.workspaceId;
        param.name = this.model.name;
        param.fileList = this.fileList;
        param.creatorId = this.model.creatorId;
        param.xdcKey = this.model.id;

        this.xdcAnalyzeService.startAnalyze(param).subscribe(
          res => {
            this.openAlertDialog('Analyze', `complete analyze.`, 'success');
          },
          err => {
            this.openAlertDialog('Failed', 'Error. Failed to Analyze', 'error');
          });
      }
    });
  };
}
