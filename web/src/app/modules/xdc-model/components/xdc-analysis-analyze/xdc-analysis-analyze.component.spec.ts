import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {XdcAnalysisAnalyzeComponent} from './xdc-analysis-analyze.component';

describe('XdcclAnalysisAnalyzeComponent', () => {
  let component: XdcAnalysisAnalyzeComponent;
  let fixture: ComponentFixture<XdcAnalysisAnalyzeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XdcAnalysisAnalyzeComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XdcAnalysisAnalyzeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
