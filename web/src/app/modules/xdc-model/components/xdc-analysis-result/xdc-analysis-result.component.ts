import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {AlertComponent, ConfirmComponent, DownloadService, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {XdcResultService} from '../../services/xdc-result/xdc-result.service';
import {XdcResultEntity} from '../../entity/xdc-result/xdc-result.entity';
import {XdcWordEntity} from '../../entity/xdc-result/xdc-word.entity';
import {XdcWordResultEntity} from '../../entity/xdc-result/xdc-word-result.entity';

@Component({
  selector: 'app-xdc-analysis-result',
  templateUrl: './xdc-analysis-result.component.html',
  styleUrls: ['./xdc-analysis-result.component.scss'],
})
export class XdcAnalysisResultComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('tableWordComponent') tableWordComponent: TableComponent;

  modelName: string;
  workspaceId: string;
  inputSentence: string;
  inputCategory: string;

  actions: any;
  header = [];
  wordHeader = [];

  matSort: MatSort;
  matWordSort: MatSort;
  xdcResults: XdcResultEntity[] = [];
  wordResults: XdcWordEntity [] = [];
  dataSource: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  pageLength: number;
  wordPageLength: number;
  pageParam: MatPaginator;
  wordPageParam: MatPaginator;
  param: XdcResultEntity;
  wordParam: XdcWordResultEntity;
  sentenceId = '';

  constructor(private storage: StorageBrowser,
              private xdcResultService: XdcResultService,
              private downloadService: DownloadService,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.header = [
      {attr: 'no', name: 'No.', width: '5%', no: true},
      {attr: 'textData', name: 'Sentence', onClick: this.getWords},
      {attr: 'category', name: 'Category', width: '10%', isSort: true},
      {attr: 'probability', name: 'Probability', width: '20%', isSort: true},
      {attr: 'createdAt', name: 'Created at', width: '10%', format: 'date', isSort: true},
    ];
    this.wordHeader = [
      {attr: 'no', name: 'No.', width: '5%', no: true},
      {attr: 'word', name: 'Word', width: '55%'},
      {attr: 'weight', name: 'Weight', width: '40%', isSort: true},
    ];
    this.route.params.subscribe(par => {
      this.modelName = par['id'];
      if (!this.modelName || this.modelName === '') {
        this.back('Error. Check your model name in a url.');
      }
      this.workspaceId = this.storage.get('m2uWorkspaceId');
    });
    this.actions = [
      {
        type: 'download',
        text: 'Download',
        icon: 'file_download',
        callback: this.download,
        disabled: true,
      },
      {
        type: 'clearResults',
        text: 'Clear Results',
        icon: 'delete_sweep',
        callback: this.clear,
        disabled: true,
      },
    ];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngAfterViewInit() {
    this.getAllXdcResults();
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.wordParam.pageSize = 10;
    this.wordParam.pageIndex = 0;
  }

  search(matSort?: MatSort) {
    this.matSort = matSort;

    if (this.xdcResults.length > 0) {
      this.getAllXdcResults();
    }
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  download = () => {
    this.downloadService.downloadCsvFile(this.tableComponent.rows, this.header, 'result');
  };

  clear = () => {
    this.dialog.open(ConfirmComponent).afterClosed().subscribe(
      result => {
        if (!result) {
          return false;
        } else {
          if (this.xdcResults.length > 0) {
            this.xdcResultService.deleteByXdcResult(this.param).subscribe(
              res => {
                this.openAlertDialog('Delete', `Delete success. ${res['deleteCount']} deleted.`, 'success');
                this.getAllXdcResults();
                this.getAllXdcWordResults();
              },
              err => {
                this.openAlertDialog('Failed', 'Error. delete failed.', 'error');
              });
          }
        }
      });
  };

  setPage(page?: MatPaginator) {
    this.pageParam = page;

    if (this.xdcResults.length > 0) {
      this.getAllXdcResults();
    }
  }

  getAllXdcResults = (pageIndex?) => {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.param = new XdcResultEntity();
    this.param.workspaceId = this.workspaceId;
    this.param.model = this.modelName;
    if (this.inputSentence === '') {
      this.param.textData = null;
    } else {
      this.param.textData = this.inputSentence;
    }
    this.param.category = this.inputCategory;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.orderDirection = this.matSort.direction === '' || this.matSort.direction === undefined ? 'desc' : this.matSort.direction;
    this.param.orderProperty = this.matSort.active === '' || this.matSort.active === undefined ? 'createdAt' : this.matSort.active;

    this.xdcResultService.getAllXdcResults(this.param).subscribe(
      res => {
        this.pageLength = res['totalElements'];
        this.xdcResults = res['content'];
        this.tableComponent.isCheckedAll = false;
      },
      err => {
        this.openAlertDialog('Failed', 'Error. failed fetch analysis result list', 'error');
      });
  };

  getAllXdcWordResults = (pageIndex?) => {
    if (pageIndex !== undefined) {
      this.wordPageParam.pageIndex = pageIndex;
    }
    if (this.sentenceId !== '') {
      this.wordParam = new XdcWordResultEntity();
      this.wordParam.workspaceId = this.workspaceId;
      this.wordParam.sentenceId = this.sentenceId;
      this.wordParam.pageSize = this.wordPageParam.pageSize;
      this.wordParam.pageIndex = this.wordPageParam.pageIndex;
      this.wordParam.orderDirection = this.matWordSort.direction === '' || this.matWordSort.direction === undefined ?
        'desc' : this.matWordSort.direction;
      this.wordParam.orderProperty = this.matWordSort.active === '' || this.matWordSort.active === undefined ?
        'weight' : this.matWordSort.active;
      this.xdcResultService.getWordsWithPage(this.wordParam).subscribe(
        res => {
          if (res.hasOwnProperty('content') === undefined) {
            this.openAlertDialog('Failed', 'Cannot get word results', 'error');
          } else {
            this.wordPageLength = res['totalElements'];
            this.wordResults = res['content'];
          }
        },
        err => {
          this.openAlertDialog('Failed', 'Error. failed to get word results', 'error');
        });
    }

  };

  getWords: any = (data: any) => {

    if (this.xdcResults.length > data.no - 1) {
      this.sentenceId = this.xdcResults[data.no - 1].xdcSentences[0].id;
    }
    if (this.sentenceId !== '') {
      this.wordParam = new XdcWordResultEntity();
      this.wordParam.workspaceId = this.workspaceId;
      this.wordParam.sentenceId = this.sentenceId;
      this.wordParam.pageSize = this.wordPageParam.pageSize;
      this.wordParam.pageIndex = this.wordPageParam.pageIndex;
      this.wordParam.orderDirection = this.matWordSort.direction === '' || this.matWordSort.direction === undefined ?
        'desc' : this.matWordSort.direction;
      this.wordParam.orderProperty = this.matWordSort.active === '' || this.matWordSort.active === undefined ?
        'weight' : this.matWordSort.active;
      this.xdcResultService.getWordsWithPage(this.wordParam).subscribe(
        res => {
          this.wordPageLength = res['totalElements'];
          this.wordResults = res['content'];
        },
        err => {
          this.openAlertDialog('Failed', 'Error. failed fetch analysis word result list', 'error');
        });
    }
  };

  setWordPage(page?: MatPaginator) {
    this.wordPageParam = page;

    if (this.wordResults.length > 0) {
      this.getAllXdcWordResults();
    }
  }

  searchWord(matSort?: MatSort) {
    this.matWordSort = matSort;

    if (this.wordResults.length > 0) {
      this.getAllXdcWordResults();
    }
  }

  back(msg = '') {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    if (this.modelName === '') {
      this.router.navigate(['..', '..'], {relativeTo: this.route});
    } else {
      this.router.navigate(['..', 'analysis'], {relativeTo: this.route});
    }
  }


  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
