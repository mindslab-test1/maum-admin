import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {XdcAnalysisResultComponent} from './xdccl-analysis-result.component';

describe('XdcclAnalysisResultComponent', () => {
  let component: XdcAnalysisResultComponent;
  let fixture: ComponentFixture<XdcAnalysisResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XdcAnalysisResultComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XdcAnalysisResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
