import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {XdcModelsComponent} from './xdc-models.component';

describe('XdcclModelsComponent', () => {
  let component: XdcModelsComponent;
  let fixture: ComponentFixture<XdcModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XdcModelsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XdcModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
