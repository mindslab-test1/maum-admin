import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {XdcModelTrainComponent} from './xdc-model-train.component';

describe('XdcModelTrainComponent', () => {
  let component: XdcModelTrainComponent;
  let fixture: ComponentFixture<XdcModelTrainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [XdcModelTrainComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XdcModelTrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
