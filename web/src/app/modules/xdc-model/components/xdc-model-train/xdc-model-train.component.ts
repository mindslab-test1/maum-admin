import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatDialog, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {XdcTrainingEntity} from '../../entity/xdc-training/xdc-training.entity';
import {XdcTrainingService} from '../../services/xdc-training/xdc-training.service';
import {ReplaySubject, Subject} from 'rxjs';
import {AlertComponent, ConfirmComponent, FormErrorStateMatcher} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {ActivatedRoute, Router} from '@angular/router';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';

@Component({
  selector: 'app-xdc-training',
  templateUrl: './xdc-model-train.component.html',
  styleUrls: ['./xdc-model-train.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  encapsulation: ViewEncapsulation.None,
})
export class XdcModelTrainComponent implements OnInit, OnDestroy {

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredXdcDicList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  selectedXdcDic: XdcTrainingEntity = new XdcTrainingEntity;

  // action
  actions: any;
  epoch: number;
  max_batch_sz: number;
  train_max_sz: number;
  test_max_sz: number;
  isTrain: boolean;

  intervalHandler: any = null;

  models: XdcTrainingEntity[] = [];
  trainings: XdcTrainingEntity[] = [];

  maxBatchSzFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);

  trainMaxSzFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);

  testMaxSzFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);

  epochFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(/[0-9]+/)
  ]);
  timeout: number;
  workspaceId: string;

  constructor(public matcher: FormErrorStateMatcher,
              public dialog: MatDialog,
              private xdcTrainingService: XdcTrainingService,
              private route: ActivatedRoute,
              private router: Router,
              private storage: StorageBrowser,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actions = [
      {type: 'train', text: 'Train', icon: 'school', callback: this.train},
    ];
    this.workspaceId = this.storage.get('m2uWorkspaceId');

    this.route.params.subscribe(par => {
      if (par.hasOwnProperty('id')) {
        if (par['id'] !== '') {
          this.selectedXdcDic.name = par['id'];
          let param: ModelManagementEntity = new ModelManagementEntity();
          param.hmdUse = false;
          param.xdcUse = true;
          param.workspaceId = this.workspaceId;
          param.name = this.selectedXdcDic.name;
          this.xdcTrainingService.getXdcDicByName(this.workspaceId, this.selectedXdcDic.name).subscribe(res => {
            if (res) {
              this.selectedXdcDic = res;
              this.epoch = 300;
              this.max_batch_sz = 2;
              this.train_max_sz = 1500000;
              this.test_max_sz = 3000000;
              this.timeout = 50000;
              this.readyToTrain();
              this.initGetAllProgress();
            } else {
              this.openAlertDialog('Error', `Error. Check a name of xdc model in a url`, 'error');
              this.router.navigate(['..'], {relativeTo: this.route});
            }
          }, err => {
            this.openAlertDialog('Error', `Error. Server Error`, 'error');
          });
        }
      }
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  back = () => {
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.syncProgress();
    }, this.timeout);
  }

  train = () => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      let xdcTraining: XdcTrainingEntity = new XdcTrainingEntity();
      xdcTraining = this.selectedXdcDic;
      xdcTraining.epoch = this.epoch;
      xdcTraining.maxBatchSz = this.max_batch_sz;
      xdcTraining.trainMaxSz = this.train_max_sz;
      xdcTraining.testMaxSz = this.test_max_sz;
      this.xdcTrainingService.open(xdcTraining).subscribe(
        res => {
          if (res) {
            if (this.intervalHandler) {
              clearInterval(this.intervalHandler);
              this.triggerInterval();
            }
            this.openAlertDialog('Train', 'Start xdc trained', 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', err.error.message, 'error');
        }
      )
    });
  };

  stopTraining = (key: string) => {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }
      let xdcTrainingEntity: XdcTrainingEntity = new XdcTrainingEntity();
      xdcTrainingEntity.xdcKey = key;

      this.xdcTrainingService.stop(xdcTrainingEntity).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Stop', 'Training has been stopped.', 'success');
          }
        },
        err => {
          this.openAlertDialog('Error', 'Server error. stop failed.', 'error');
        });
    });
  };

  deleteModel(key) {
    this.dialog.open(ConfirmComponent)
    .afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return false;
      }

      let xdcTrainingEntity: XdcTrainingEntity = new XdcTrainingEntity();
      xdcTrainingEntity.xdcKey = key;

      this.xdcTrainingService.deleteModel(xdcTrainingEntity).subscribe(
        res => {
          if (res) {
            this.openAlertDialog('Delete', 'Delete model success.', 'success');
            this.getXdcAllModels(xdcTrainingEntity);
          }
        },
        err => {
          this.openAlertDialog('Error', 'Server error. Training delete failed.', 'error');
        });
    });
  }

  readyToTrain() {
    if (this.epoch != null && this.max_batch_sz != null && this.train_max_sz != null && this.test_max_sz != null
      && this.selectedXdcDic != null
      && !this.matcher.isErrorState(this.maxBatchSzFormControl)
      && !this.matcher.isErrorState(this.trainMaxSzFormControl)
      && !this.matcher.isErrorState(this.testMaxSzFormControl)
      && !this.matcher.isErrorState(this.epochFormControl)) {
      this.isTrain = false;
    } else {
      this.isTrain = true;
    }
  }

  getClassButton() {
    return this.isTrain === true ? 'mlt-disabled' : 'mlt-not-disabled';
  }

  getXdcAllModels(xdcTrainingEntity: XdcTrainingEntity) {
    xdcTrainingEntity.workspaceId = this.storage.get('m2uWorkspaceId');
    xdcTrainingEntity.name = this.selectedXdcDic.name;
    this.xdcTrainingService.getModelsByName(xdcTrainingEntity).subscribe(
      res => {
        if (res) {
          if (this.models.length === 0) {
            this.models = res;
          } else if (this.models.length > res.length) {
            this.models.forEach((model, index) => {
              let data = res.find(item => item.xdcKey === model.xdcKey);
              if (!data) {
                this.models.splice(index, 1);
              }
            });
          } else if (this.models.length < res.length) {
            res.forEach(model => {
              let data = this.models.find(item => item.xdcKey === model.xdcKey);
              if (!data) {
                this.models.push(model);
              }
            });
          }
        }
      },
      err => {
        this.openAlertDialog('Error', 'Server error. failed to fetch xdc models.', 'error');
      });
  }

  initGetAllProgress() {

    let xdcTrainingEntity: XdcTrainingEntity = new XdcTrainingEntity();
    xdcTrainingEntity.workspaceId = this.storage.get('m2uWorkspaceId');

    this.xdcTrainingService.getAllProgress(xdcTrainingEntity).subscribe(
      res => {
        if (res) {
          this.trainings = res['dtList'];
        }
        this.getXdcAllModels(xdcTrainingEntity);
        this.triggerInterval();
      },
      err => {
        if (err.error.message === 'NOT_STARTED') {
          this.openAlertDialog('Error', 'Server error. xdc connect failed.', 'error');
        }
      });
  }

  syncProgress = () => {
    let xdcTrainingEntity: XdcTrainingEntity = new XdcTrainingEntity();
    xdcTrainingEntity.workspaceId = this.storage.get('m2uWorkspaceId');

    this.xdcTrainingService.getAllProgress(xdcTrainingEntity).subscribe(
      res => {
        if (res) {
          if (this.trainings.length === 0) {
            clearInterval(this.intervalHandler);
            this.timeout = 5000;
            this.triggerInterval();
            this.trainings = res['dtList'];
          } else {
            clearInterval(this.intervalHandler);
            this.timeout = 450;
            this.triggerInterval();
            if (this.trainings.length <= res['dtList'].length) {
              res['dtList'].forEach(xdcModel => {
                let training: XdcTrainingEntity = this.trainings.find(item => item.xdcKey === xdcModel.xdcKey);
                if (training) {
                  training['progress'] = xdcModel.progress;
                } else {
                  this.trainings.push(xdcModel);
                }
              });
            } else if (this.trainings.length > res['dtList'].length) {
              this.trainings.forEach((xdcModel, index) => {
                let training: XdcTrainingEntity = res['dtList'].find(item => item.xdcKey === xdcModel.xdcKey);
                if (training) {
                  training['progress'] = xdcModel['progress'];
                } else {
                  this.trainings.splice(index, 1);
                }
              });
            }
          }
        }
        this.getXdcAllModels(xdcTrainingEntity);
      },
      err => {
        if (err.error.message === 'NOT_STARTED') {
          clearInterval(this.intervalHandler);
          this.openAlertDialog('Error', 'Server error. xdc connect failed.', 'error');
        }
      });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
