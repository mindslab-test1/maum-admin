import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {XdcModelRoutingModule} from './xdc-model-routing.module';

import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressBarModule,
  MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {XdcModelsComponent} from './components/xdc-models/xdc-models.component';
import {ModelInsertService} from './services/model-insert/model-insert.service';
import {XdcModelDetailComponent} from './components/xdc-model-detail/xdc-model-detail.component';
import {XdcTrainingDataService} from './services/xdc-training-data/xdc-training-data.service';
import {XdcTrainingService} from './services/xdc-training/xdc-training.service';
import {XdcModelTrainComponent} from './components/xdc-model-train/xdc-model-train.component';
import {XdcAnalysisTestComponent} from './components/xdc-analysis-test/xdc-analysis-test.component';
import {XdcTestService} from './services/xdc-test/xdc-test.service';
import {XdcAnalysisAnalyzeComponent} from './components/xdc-analysis-analyze/xdc-analysis-analyze.component';
import {XdcAnalyzeService} from './services/xdc-analyze/xdc-analyze.service';
import {XdcResultService} from './services/xdc-result/xdc-result.service';
import {XdcAnalysisResultComponent} from './components/xdc-analysis-result/xdc-analysis-result.component';

@NgModule({
  declarations: [
    XdcModelsComponent,
    XdcModelDetailComponent,
    XdcModelTrainComponent,
    XdcAnalysisTestComponent,
    XdcAnalysisAnalyzeComponent,
    XdcAnalysisResultComponent
  ],
  imports: [
    CommonModule,
    XdcModelRoutingModule,
    MatToolbarModule,
    MatSelectModule,
    MatCardModule,
    SharedModule,
    MatInputModule,
    MatProgressBarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
  ],
  exports: [
    XdcModelsComponent,
    XdcModelDetailComponent,
    XdcModelTrainComponent,
    XdcAnalysisTestComponent,
    XdcAnalysisAnalyzeComponent,
    XdcAnalysisResultComponent
  ],
  providers: [
    ModelInsertService,
    XdcTrainingService,
    XdcTrainingDataService,
    XdcTestService,
    XdcAnalyzeService,
    XdcResultService,
  ]
})
export class XdcModelModule {
}
