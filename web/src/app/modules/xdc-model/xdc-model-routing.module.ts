import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {XdcModelsComponent} from './components/xdc-models/xdc-models.component';
import {XdcModelDetailComponent} from './components/xdc-model-detail/xdc-model-detail.component';
import {XdcModelTrainComponent} from './components/xdc-model-train/xdc-model-train.component';
import {XdcAnalysisTestComponent} from './components/xdc-analysis-test/xdc-analysis-test.component';
import {XdcAnalysisAnalyzeComponent} from './components/xdc-analysis-analyze/xdc-analysis-analyze.component';
import {XdcAnalysisResultComponent} from './components/xdc-analysis-result/xdc-analysis-result.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        // sideNav를 선택하면 /dashboard 로 이동합니다
        path: '',
        component: XdcModelsComponent,
        data: {hasNoSidebar: false}  // sidebar를 삭제 합니다
      },
      {
        path: 'new',
        component: XdcModelDetailComponent,
      },
      {
        path: ':id/train',
        component: XdcModelTrainComponent
      },
      {
        path: ':id/edit',
        component: XdcModelDetailComponent
      },
      {
        path: ':id/test',
        component: XdcAnalysisTestComponent
      },
      {
        path: ':id/analysis-result',
        component: XdcAnalysisResultComponent
      },
      {
        path: ':id/analysis',
        component: XdcAnalysisAnalyzeComponent
      },
      {
        path: ':id',
        component: XdcModelDetailComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XdcModelRoutingModule {
}
