import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {XdcResultEntity} from '../../entity/xdc-result/xdc-result.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';
import {XdcWordResultEntity} from '../../entity/xdc-result/xdc-word-result.entity';

@Injectable()
export class XdcResultService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllXdcResults(xdcResultEntity: XdcResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/result/getAllXdcResults/`, xdcResultEntity);
  }

  deleteByXdcResult(xdcResultEntity: XdcResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/result/deleteByXdcResult/`, xdcResultEntity);
  }

  getWordsWithPage(xdcWordResultEntity: XdcWordResultEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/result/getWordsWithPage/`, xdcWordResultEntity);
  }
}
