import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ModelManagementEntity} from '../../entity/model-management/model-management.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class ModelInsertService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  insertTaModel(insertData: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/insertTaModel', insertData);
  }

  getXdc(model: ModelManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getXdc', model);
  }

  deleteSelectedModels(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/deleteSelectedModels', models);
  }

  updateTaModel(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/updateTaModel', models);
  }

  getTaModel(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getTaModel', models);
  }

  getXdcWithCommitHistory(models: any): Observable<any> {
    return this.http.post(this.API_URL + '/ta/getTaModelWithCommitHistory', models);
  }
}
