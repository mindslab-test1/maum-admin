import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';
import {XdcTrainingDataEntity} from '../../entity/xdc-training-data/xdc-training-data.entity';

@Injectable()
export class XdcTrainingDataService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getdcDicAllList(workspaceId: string): Observable<any> {
    return this.http.get(this.API_URL + '/ta/xdc/training-data/getXdcDicAllList/' + workspaceId);
  }

  getXdcCategoryAllList(workspaceId: string, xdcDicId: string): Observable<any> {
    return this.http.get(`${this.API_URL}/ta/xdc/training-data/getXdcCategoryAllList/${workspaceId}/xdcDicId/${xdcDicId}`);
  }

  insertCategory(category: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/insertCategory`, category);
  }

  updateCategory(category: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/updateCategory`, category);
  }

  deleteCategory(category: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/deleteCategory`, category);
  }

  hasLines(xdcTrainingDataEntity: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/hasLines/`, xdcTrainingDataEntity);
  }

  getAllDicLines(xdcTrainingDataEntity: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/getAllDicLines/`, xdcTrainingDataEntity);
  }

  getDuplicatesLines(versionId: string, category: string): Observable<any> {
    return this.http.get(`${this.API_URL}/ta/xdc/training-data/getDuplicatesLines/${versionId}/category/${category}`);
  }

  insertLine(xdcTrainingDataEntity: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/insertLine`, xdcTrainingDataEntity);
  }

  deleteLines(xdcTrainingDataEntities: XdcTrainingDataEntity[]): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/deleteLines`, xdcTrainingDataEntities);
  }

  deleteDuplicatesLines(xdcTrainingDataEntity: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/deleteDuplicatesLines`, xdcTrainingDataEntity);
  }

  commit(xdcTrainingDataEntity: XdcTrainingDataEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training-data/commit`, xdcTrainingDataEntity);
  }
}
