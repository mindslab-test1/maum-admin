import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {XdcTestEntity} from '../../entity/xdc-test/xdc-test.entity';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class XdcTestService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllModels(param: XdcTestEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/test/getAllModels/`, param);
  }

  getModel(name: string, workspaceId: string): Observable<any> {
    let param: XdcTestEntity = new XdcTestEntity;
    param.name = name;
    param.workspaceId = workspaceId;
    return this.http.post(`${this.API_URL}/ta/xdc/test/getModel/`, param);
  }

  taXdcAnalyzeText(param: XdcTestEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/test/taXdcAnalyzeText/`, param);
  }
}
