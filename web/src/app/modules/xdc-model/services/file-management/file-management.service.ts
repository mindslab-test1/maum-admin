import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {FileManagementEntity} from '../../entity/file-management/file-management.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class FileManagementService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getFileGroups(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/getFileGroups', param);
  }

  insertGroup(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/insertGroup', param);
  }

  updateGroup(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/updateGroup', param);
  }

  deleteGroup(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/deleteGroup', param);
  }

  getFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/getFileList', param);
  }

  getGroupFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/getGroupFileList', param);
  }

  deleteFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/deleteFiles', param);
  }

  includeFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/include-files', param);
  }

  excludeFiles(param: FileManagementEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/file-manage/exclude-files', param);
  }
}
