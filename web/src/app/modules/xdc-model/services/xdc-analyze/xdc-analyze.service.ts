import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {XdcAnalyzeEntity} from '../../entity/xdc-analyze/xdc-analyze.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';
import {XdcTestEntity} from '../../entity/xdc-test/xdc-test.entity';

@Injectable()
export class XdcAnalyzeService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getModel(workspaceId: string, name: string): Observable<any> {
    let param: XdcTestEntity = new XdcTestEntity;
    param.name = name;
    param.workspaceId = workspaceId;
    return this.http.post(`${this.API_URL}/ta/xdc/analyze/getModel/`, param);
  }

  startAnalyze(param: XdcAnalyzeEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/xdc/analyze/startAnalyze', param);
  }
}
