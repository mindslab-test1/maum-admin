import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from '../../../../../environments/environment';
import {XdcTrainingEntity} from '../../entity/xdc-training/xdc-training.entity';

@Injectable()
export class XdcTrainingService {
  API_URL;
  WORK_SPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.WORK_SPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  getXdcDicAllList(xdcTrainingEntity: XdcTrainingEntity): Observable<any> {
    return this.http.post(this.API_URL + '/ta/xdc/training/getXdcDicAllList/', xdcTrainingEntity);
  }

  getXdcDictionaryId(name: string): Observable<any> {
    let xdcTrainingEntity: XdcTrainingEntity;
    xdcTrainingEntity.name = name;
    xdcTrainingEntity.workspaceId = this.WORK_SPACE_ID;
    return this.http.post(this.API_URL + '/ta/xdc/training/getXdcId/', xdcTrainingEntity);
  }

  getXdcDicByName(workspaceId: string, name: string): Observable<any> {
    let param: XdcTrainingEntity = new XdcTrainingEntity();
    param.workspaceId = workspaceId;
    param.name = name;
    return this.http.post(this.API_URL + '/ta/xdc/training/getXdcDicByName', param);
  }

  getAllModels(xdcTrainingEntity: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/getAllModels/`, xdcTrainingEntity);
  }

  getModelsByName(xdcTrainingEntity: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/getModelsByName/`, xdcTrainingEntity);
  }

  getAllProgress(xdcTrainingEntity: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/getAllProgress/`, xdcTrainingEntity);
  }

  deleteModel(xdcTrainingEntity: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/deleteModel/`, xdcTrainingEntity);
  }

  open(xdcTraining: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/open/`, xdcTraining);
  }

  stop(xdcTraining: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/stop/`, xdcTraining);
  }

  deleteTraining(xdcTraining: XdcTrainingEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/ta/xdc/training/deleteTraining/`, xdcTraining);
  }
}
