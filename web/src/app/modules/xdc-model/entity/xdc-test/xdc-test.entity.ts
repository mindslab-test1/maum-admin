import {XdcWordResultEntity} from '../xdc-result/xdc-word-result.entity';

export class XdcTestEntity {

  // xdcModelEntity
  xdcKey: string;
  workspaceId: string;
  name: string;
  xdcBinary: string;
  creatorId: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;

  // grpc parameter
  probability: number;
  textData: string;
  category: string;
  xdcWordResults: XdcWordResultEntity[];
}
