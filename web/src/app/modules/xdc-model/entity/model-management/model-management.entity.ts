import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class ModelManagementEntity extends PageParameters {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;
  dnnUse: boolean;
  hmdUse: boolean;
  xdcUse: boolean;
  manDesc: string;
  name: string;
  workspaceId: string;

  dnnDicId: string;
  hmdDicId: string;
  xdcDicId: string;
}
