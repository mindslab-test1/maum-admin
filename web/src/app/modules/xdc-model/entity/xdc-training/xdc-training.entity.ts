export class XdcTrainingEntity {

  // XdcDicModel and XdcDicTraining
  workspaceId: string;
  xdcKey: string;

  name: string;
  xdcBinary: string;

  // XdcDicEntity only use
  id: string;
  versions: string;
  applied: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;

  // not entity, only data transfer use
  epoch: number;
  maxBatchSz: number;
  trainMaxSz: number;
  testMaxSz: number;

}
