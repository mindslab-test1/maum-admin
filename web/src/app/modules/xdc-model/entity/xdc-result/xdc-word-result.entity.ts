import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class XdcWordResultEntity extends PageParameters {

  id: string;
  model: string;
  sentence: string;
  createdAt: Date;
  creatorId: string;
  weight: string;

  workspaceId: string;
  sentenceId: string;
}
