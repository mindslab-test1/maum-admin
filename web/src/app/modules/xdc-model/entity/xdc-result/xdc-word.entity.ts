import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class XdcWordEntity extends PageParameters {

  no: number;
  id: string;
  word: string;
  start: number;
  end: number;
  weight: string;

}
