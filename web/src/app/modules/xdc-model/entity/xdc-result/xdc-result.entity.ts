import {PageParameters} from 'app/shared/entities/pageParameters.entity';
import {XdcWordResultEntity} from './xdc-word-result.entity';

export class XdcResultEntity extends PageParameters {

  no: number;
  id: string;
  workspaceId: string;
  model: string;
  fileGroup: string;
  textData: string;
  category: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;
  probability: number;
  xdcSentences: XdcWordResultEntity[];
}
