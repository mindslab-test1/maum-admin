import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class XdcAnalyzeEntity extends PageParameters {

  // common
  id: string;
  name: string;

  // only XdcModelEntity
  xdcKey: string;
  fileList: string[];
  workspaceId: string;
  creatorId: string;

  xdcBinary: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;

  // only FileGroup
  fileGroup: string;
  fileGroupId: string;
  purpose: string;

  // only File
  type: string;
  size: number;
  uploader: string;
  uploadedAt: Date;

}
