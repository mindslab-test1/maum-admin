import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class XdcTrainingDataEntity extends PageParameters {

  // common
  id: string;
  workspaceId: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  creatorId: string;
  updaterId: string;
  manDesc: string;

  // XdcCategoryEntity
  xdcDicId: string;
  parentId: string;
  count: number;

  // XdcDicEntity
  versions: string;
  applied: string;


  // XdcDicLineEntity
  seq: number;
  sentence: string;
  category: string;
  versionId: string;

  // XdcCategoryEntity not entity, only data transfer use
  oldName: string;
  updateState: string;

  // XdcDicLineEntity not entity, only data transfer use
  duplicateCount: number;

  title: string;
  message: string;

}
