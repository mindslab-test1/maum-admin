import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ParaphrasesComponent} from './paraphrases.component';

describe('ParaphrasesComponent', () => {
  let component: ParaphrasesComponent;
  let fixture: ComponentFixture<ParaphrasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ParaphrasesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParaphrasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
