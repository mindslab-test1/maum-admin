import {AfterViewInit, Component, TemplateRef, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {QueryParaphraseService} from '../../services/query-paraphrase/query-paraphrase.service';
import {QueryParaphraseEntity} from '../../entity/query-paraphrase/query-paraphrase.entity';
import {Router} from '@angular/router';
import {AlertComponent, AppEnumsComponent, ConfirmComponent, TableComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-query-paraphrase',
  templateUrl: './paraphrases.component.html',
  styleUrls: ['./paraphrases.component.scss'],
})

export class ParaphrasesComponent implements AfterViewInit {
  @ViewChild('paraphraseTableComponent') paraphraseTableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  searchKeyword: string;

  paraphraseDataSource: MatTableDataSource<any>;
  paraphrases: any[] = [];
  paraphrasePage: MatPaginator;
  paraphraseSort: MatSort;
  paraphrasePageTotalCount: number;
  removeFlag = true;

  header = [];

  constructor(private storage: StorageBrowser,
              private queryParaphraseService: QueryParaphraseService,
              private dialog: MatDialog,
              private appEnum: AppEnumsComponent,
              private router: Router,
              private store: Store<any>) {

  }

  ngOnInit() {
    this.header = [
      {attr: 'checkbox', name: 'Checkbox', checkbox: true},
      {attr: 'no', name: 'No.', no: true},
      {attr: 'title', name: 'Title', isSort: true, width: '20%', onClick: this.goDetail},
      {attr: 'currentStep', name: 'CurrentStep', width: '10%', isSort: true},
      {attr: 'allStep', name: 'AllStep', width: '45%', isSort: true},
      {attr: 'creatorId', name: 'Uploader', width: '10%', isSort: true},
      {attr: 'createdAt', name: 'UploadedAt', width: '15%', format: 'date', isSort: true}
    ];
  }

  ngAfterViewInit() {
    this.getParaphrases();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * Paraphrases 데이터를 가져온다.
   * @param number pageIndex
   */
  getParaphrases(pageIndex?: number) {
    let param = new QueryParaphraseEntity();
    param.workspaceId = this.storage.get('m2uWorkspaceId');
    param.pageSize = this.paraphrasePage.pageSize;
    param.pageIndex = this.paraphrasePage.pageIndex;
    param.orderDirection = this.paraphraseSort.direction === '' ||
    this.paraphraseSort.direction === undefined ? 'desc' : this.paraphraseSort.direction;
    param.orderProperty = this.paraphraseSort.active === '' ||
    this.paraphraseSort.active === undefined ? 'createdAt' : this.paraphraseSort.active;
    param.title = this.searchKeyword;

    if (pageIndex !== undefined) {
      this.paraphrasePage.pageIndex = pageIndex;
    }

    this.queryParaphraseService.getParaphrases(param).subscribe(
      res => {
        this.paraphrases = res['content'];
        this.paraphrases.forEach(paraphrase => {
          paraphrase.currentStep = this.appEnum.paraphraseStepHtml[paraphrase.currentStep];
          if (paraphrase.allStep !== null) {
            let strArr: string[] = paraphrase.allStep.split(',');
            let resultStr: string[] = [];
            strArr.forEach(str => {
              resultStr.push(this.appEnum.paraphraseStepHtml[str]);
            });
            paraphrase.allStep = resultStr.join('->');
          }
        });
        this.paraphrasePageTotalCount = res['totalElements'];
        this.paraphraseTableComponent.isCheckedAll = false;
        this.isChecked();
      },
      err => {
        this.openAlertDialog('Error', `Server error. paraphrases fetch failed.`, 'error');
      }
    )
  }

  /**
   * table에서 체크박스를 클릭한 적이 있는지 알 수 있는 함수
   * @param check => 체크 되었는지 안되었는지 유무(매개변수가 있다면 체크가 된 것이다.)
   */
  isChecked(check?) {
    if (check) {
      this.removeFlag = false;
    } else {
      this.removeFlag = true;
    }
  }

  removeParaphrase() {
    let param: QueryParaphraseEntity[] =
      this.paraphraseTableComponent.rows.filter(row => row.isChecked);

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        let originalPageIndex: number = this.paraphrasePage.pageIndex;
        if (param.length === this.paraphraseTableComponent.rows.length) {
          if (this.paraphrasePage.pageIndex === 0) {
            this.paraphrasePage.pageIndex = 0;
          } else {
            this.paraphrasePage.pageIndex -= 1;
          }
        }

        this.queryParaphraseService.deleteParaphrases(param).subscribe(
          res => {
            this.getParaphrases();
            this.openAlertDialog('Remove', `Remove paraphrase [${param.length}] success.`, 'success');
          },
          err => {
            this.paraphrasePage.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', `Server error. remove paraphrases failed.`, 'error');
          }
        )
      }
    });
  }

  /**
   * sort 세팅
   * @param MatSort sort
   */
  setSortParaphrase(sort?: MatSort) {
    if (sort) {
      this.paraphraseSort = sort;
    }

    if (this.paraphrases.length > 0) {
      this.getParaphrases();
    }
  }

  /**
   * 페이지 세팅
   * @param MatPaginator page
   */
  setParaphrasePage(page?: MatPaginator) {
    if (page) {
      this.paraphrasePage = page;
    }

    if (this.paraphrases.length > 0) {
      this.getParaphrases();
    }
  }

  /**
   * detail 화면으로 이동(param: id)
   * @param row => 클릭한 테이블의 row
   */
  goDetail: any = (row: any) => {
    let id = 'id';
    let param = `${encodeURI(row.id)}`;
    this.router.navigate(['m2u-builder', 'paraphrase', 'step'], {queryParams: { id: param} });
  };

  /**
   * 메세지를 보여주기위한 Dialog Component를 연다.
   * @param title => 에러 메세지의 타이틀
   * @param message => 에러 메세지
   * @param status => Error, Confirm, Notice 상태 값을 가진다. 상태값에 따라 Dialog색상이 달라진다.
   */
  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
