import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {SynonymDicEntity} from '../../entity/synonym-dic/synonym-dic.entity';
import {SynonymDicRelEntity} from '../../entity/synonym-dic/synonym-dic-rel.entity';
import {AlertComponent, AppEnumsComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';


@Component({
  selector: 'app-paraphrase-upsert-dialog',
  templateUrl: 'synonym-dic-upsert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class SynonymDicUpsertDialogComponent implements OnInit {

  param: any;
  service: any;
  role: string;

  name: string;
  synonymDic: SynonymDicEntity = new SynonymDicEntity();
  representationWordValidator = new FormControl('', [Validators.required]);
  synonymWordValidator = new FormControl('');
  disabled = true;
  data: any;
  synonymWord = '';
  originRepresentationWord;

  constructor(public dialogRef: MatDialogRef<SynonymDicUpsertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              public appEnum: AppEnumsComponent,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.role = this.data.role;
    this.synonymDic = this.data.row;
    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.originRepresentationWord = this.synonymDic.representation;
      this.disabled = false;
    } else {
      if (this.synonymDic.synonymDicRelEntities === undefined) {
        this.synonymDic.synonymDicRelEntities = [];
      }
    }

  }

  add() {
    this.checkExist(this.synonymDic.representation, 'representationWord').then(result => {
      if (result) {
        this.synonymDic.workspaceId = this.storage.get('m2uWorkspaceId');
        this.service.addSynonymDic(this.synonymDic).subscribe(res => {
          this.openAlertDialog('Success', 'Add Success', 'success');
          this.dialogRef.close(true);
        }, err => {
          this.openAlertDialog('Error', 'Failed to add', 'error');
          console.error('error', err);
        })
      }
    });
  }

  addSynonym(): void {
    if (this.synonymWord !== '') {
      this.checkExist(this.synonymWord, 'synonymWord').then(result => {
        if (result) {
          // INSERT
          let toBeInsertedSynonymRelEntity = new SynonymDicRelEntity();
          toBeInsertedSynonymRelEntity.synonymWord = this.synonymWord;
          toBeInsertedSynonymRelEntity.workspaceId = this.storage.get('m2uWorkspaceId');
          if (this.synonymDic.synonymDicRelEntities === undefined) {
            this.synonymDic.synonymDicRelEntities = [];
          }
          this.synonymDic.synonymDicRelEntities.push(toBeInsertedSynonymRelEntity);
          this.synonymWord = ''; // init
        }
        this.checkInvalid();
      })
    }
  }

  update() {
    this.checkExist(this.synonymDic.representation, 'representationWord').then(result => {
      if (this.originRepresentationWord === this.synonymDic.representation || result) {
        this.service.editSynonymDic(this.synonymDic).subscribe(res => {
          this.openAlertDialog('Success', 'Update Success', 'success');
          this.dialogRef.close(true);
        }, err => {
          this.openAlertDialog('Error', 'Failed to update', 'error');
          console.error('error', err);
        })
      }
    });
  }

  remove() {
    let tempArray = [];
    tempArray.push(this.synonymDic);
    this.service.removeSynonymDic(tempArray).subscribe(res => {
      this.openAlertDialog('Success', 'Remove Success', 'success');
      this.dialogRef.close(true);
    }, err => {
      this.openAlertDialog('Error', 'Failed to remove', 'error');
      console.error('error', err);
    })
  }

  removeRel(entity: SynonymDicRelEntity): void {
    if (entity.representationId !== undefined) { // 이미 DB에 등록되어있는 Rel들
      if ((this.synonymDic.toBeDeletedSynonymDicRelEntities === null) || (this.synonymDic.toBeDeletedSynonymDicRelEntities === undefined)) {
        this.synonymDic.toBeDeletedSynonymDicRelEntities = [];
      }
      this.synonymDic.toBeDeletedSynonymDicRelEntities.push(entity);
    }
    this.synonymDic.synonymDicRelEntities.splice(this.synonymDic.synonymDicRelEntities.indexOf(entity), 1);
  }

  checkExist(word: string, type: string) {
    return new Promise(resolve => {
      if (this.synonymDic.synonymDicRelEntities === undefined || this.synonymDic.synonymDicRelEntities === null) {
        this.synonymDic.synonymDicRelEntities = [];
      }
      if (this.synonymDic.toBeDeletedSynonymDicRelEntities === undefined || this.synonymDic.toBeDeletedSynonymDicRelEntities === null) {
        this.synonymDic.toBeDeletedSynonymDicRelEntities = [];
      }
      let valid = true;
      this.synonymDic.synonymDicRelEntities.forEach(entity => { // Rel 들과 중복 비교
        if (entity.synonymWord === word) {
          valid = false;
        }
      });
      if ((this.synonymDic.representation === word) && (type === 'synonymWord')) { // representation word 와 중복 비교
        valid = false;
      }

      if (valid) {
        // DB 값과 중복 비교
        let tmpArray = [];
        tmpArray.push(word);
        this.service.checkWordExist(tmpArray, this.storage.get('m2uWorkspaceId')).subscribe(res => {
          if (res.result) {
            resolve(true);
          } else {
            let valid2 = false;
            this.synonymDic.toBeDeletedSynonymDicRelEntities.forEach(entity => {
              if (entity.synonymWord === word) {
                valid2 = true;
              }
            });
            if (valid2) {
              resolve(true);
            } else {
              if (type === 'synonymWord') {
                this.synonymWordValidator.setErrors({
                  'duplicate': true
                });
              } else {
                this.representationWordValidator.setErrors({
                  'duplicate': true
                });
              }
              resolve(false);
            }
          }
        }, err => {
          resolve(false);
        })
      } else {
        if (type === 'synonymWord') {
          this.synonymWordValidator.setErrors({
            'duplicate': true
          });
        } else {
          this.representationWordValidator.setErrors({
            'duplicate': true
          });
        }
        resolve(false);
      }
    });
  }

  checkInvalid($event?) {
    if ('INVALID' === this.representationWordValidator.status) {
      this.disabled = true;
      return false;
    } else if ('VALID' === this.representationWordValidator.status) {
      this.disabled = false;
      return true;
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
