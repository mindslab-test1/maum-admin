import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';

import {SynonymDicService} from '../../services/synonym-dic/synonym-dic.service';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SynonymDicEntity} from '../../entity/synonym-dic/synonym-dic.entity';
import {SynonymDicUpsertDialogComponent} from './synonym-dic-upsert-dialog.component';
import {
  AlertComponent, ConfirmComponent, TableComponent,
  UploaderButtonComponent
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {environment} from 'environments/environment';


@Component({
  selector: 'app-paraphrase-synonym-dic',
  templateUrl: './synonym-dic.component.html',
  styleUrls: ['./synonym-dic.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [SynonymDicService]

})
export class SynonymDicComponent implements OnInit, AfterViewInit {

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  header = [
    {attr: 'checkbox', name: 'CheckBox', type: 'text', checkbox: true, input: false},
    {attr: 'no', name: 'No.', no: true},
    {
      attr: 'representation',
      name: 'RepresentativeWord',
      type: 'text',
      checkbox: false,
      input: false,
      isSort: true
    },
    {attr: 'synonymWordList', name: 'Synonym', type: 'text', checkbox: false, input: false},
    {attr: 'action', name: 'Action', type: 'text', isButton: true, buttonName: 'Edit', width: '7%'},
  ];
  actionsArray: any[];
  dataSource: MatTableDataSource<any>;
  synonymDicList = [];
  param = new SynonymDicEntity();
  init = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageParam: MatPaginator;
  pageLength = 10;
  workspaceId;
  searchRepresentativeWord: string;
  searchSynonym: string;

  constructor(private synonymDicService: SynonymDicService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.actionsArray = [
      {
        type: 'add',
        text: 'Add',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popUpsertDialog,
        params: 'add'
      },
      {
        type: 'delete',
        text: 'Remove',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeDics
      },
      {
        type: 'delete',
        text: 'Remove All',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeDicsAll
      },
    ];
    this.workspaceId = this.storage.get('m2uWorkspaceId');
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.getNextPage();

    this.store.dispatch({type: ROUTER_LOADED});
  }

  onClickSearchButton(pageIndex?) {
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    this.getNextPage();
  }

  search(matSort?: MatSort) {
    if (matSort) {
      this.param.orderDirection = matSort.direction === '' || matSort.direction === undefined ? 'desc' : matSort.direction;
      this.param.orderProperty = matSort.active === '' || matSort.active === undefined ? 'id' : matSort.active;
    }
    if (this.init) {
      this.getNextPage();
    }
  }

  getNextPage(event?) {
    if (!this.init) {
      this.init = true;
    }

    this.param.searchRepresentativeWord = this.searchRepresentativeWord;
    this.param.searchSynonym = this.searchSynonym;
    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.workspaceId = this.workspaceId;
    this.synonymDicService.getSynonymDicListByWorkspaceId(this.param).subscribe(res => {
      res.content.map(val => {
        val.synonymWordList = '';
        val.synonymDicRelEntities.map(entity => {
          if (val.synonymWordList === '') {
            val.synonymWordList = entity.synonymWord;
          } else {
            val.synonymWordList += ',' + entity.synonymWord;
          }
        })
      });

      this.synonymDicList = res.content;
      this.pageLength = res.totalElements;
      this.dataSource = new MatTableDataSource(this.synonymDicList);
    })
  }

  setPage(page?: MatPaginator) {
    this.pageParam = page;
    if (this.init) {
      this.getNextPage();
    }
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  popUpsertDialog = (type?, row?) => {
    let paramData = {};
    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = row;
    } else {
      return false;
    }

    paramData['service'] = this.synonymDicService;
    paramData['entity'] = new SynonymDicEntity();

    let dialogRef = this.dialog.open(SynonymDicUpsertDialogComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getNextPage();
    });
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  removeDics = () => {
    let toBeRemoved = [];
    this.synonymDicList.forEach(dic => {
      if (dic.isChecked) {
        toBeRemoved.push(dic);
      }
    });

    if (toBeRemoved.length === 0) {
      this.openAlertDialog('Error', 'Please select to remove', 'error');
      return false;
    }

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        if (toBeRemoved.length === this.tableComponent.rows.length) {
          if (this.pageParam.pageIndex === 0) {
            this.pageParam.pageIndex = 0;
          } else {
            this.pageParam.pageIndex -= 1;
          }
        }

        this.tableComponent.isCheckedAll = false;
        this.synonymDicService.removeSynonymDics(toBeRemoved).subscribe(res => {
          this.openAlertDialog('Success', 'Remove Success', 'success');
          this.getNextPage();
        }, err => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. failed remove synonym dictionary.', 'error');
        })
      }
    });
  };

  removeDicsAll = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove All Item?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let originalPageIndex: number = this.pageParam.pageIndex;
        this.pageParam.pageIndex = 0;
        this.tableComponent.isCheckedAll = false;
        this.synonymDicService.removeSynonymDicsByWorkspaceId(this.workspaceId).subscribe(res => {
          this.openAlertDialog('Success', 'Remove Success', 'success');
          this.getNextPage();
        }, err => {
          this.pageParam.pageIndex = originalPageIndex;
          this.openAlertDialog('Error', 'Server error. failed remove ending post positions.', 'error');
        });
      }
    });
  };

  onUploadFile = () => {
    this.getNextPage();
  };

  downloadFile = () => {
    let url = environment.maumAiApiUrl + '/paraphrase/synonymDic/downloadFile/' + this.workspaceId;
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
  };

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };
}
