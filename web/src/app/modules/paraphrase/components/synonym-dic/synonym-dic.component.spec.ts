import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SynonymDicComponent} from './synonym-dic.component';

describe('SynonymDicComponent', () => {
  let component: SynonymDicComponent;
  let fixture: ComponentFixture<SynonymDicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SynonymDicComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymDicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
