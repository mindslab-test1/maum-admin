import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SynonymDicUpsertDialogComponent} from './synonym-dic-upsert-dialog.component';

describe('SynonymDicUpsertDialogComponent', () => {
  let component: SynonymDicUpsertDialogComponent;
  let fixture: ComponentFixture<SynonymDicUpsertDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SynonymDicUpsertDialogComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymDicUpsertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
