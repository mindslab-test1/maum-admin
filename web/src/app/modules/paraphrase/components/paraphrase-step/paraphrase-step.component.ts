import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatStepper, MatTableDataSource} from '@angular/material';
import {QueryParaphraseStepService} from '../../services/query-paraphrase-step/query-paraphrase-step.service';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QueryParaphraseStepEntity} from '../../entity/query-paraphrase-step/query-paraphrase-step.entity';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {
  AlertComponent, AppEnumsComponent, ConfirmComponent, DownloadService,
  TableComponent
} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';


@Component({
  selector: 'app-query-paraphrase-step',
  templateUrl: './paraphrase-step.component.html',
  styleUrls: ['./paraphrase-step.component.scss'],
})

/**
 * Query ParaphraseStep 화면
 */
export class ParaphraseStepComponent implements OnInit, AfterViewInit {
  steps: any[] = [{step: this.appEnum.paraphraseStep.READY, confirm: false, label: 'Ready'}];
  userClickStep: any[] = [];
  stepIdx = 0;

  isHidden = true;
  hiddenFormGroup: FormGroup;

  inversion = false;
  wordEmbedding = false;
  synonym = false;
  ending = false;

  readySearchKeyword: string;
  prevSearchKeyword: string;
  currentSearchKeyword: string;

  readyDataSource: MatTableDataSource<any>;
  prevDataSource: MatTableDataSource<any>;
  currentDataSource: MatTableDataSource<any>;

  readys: QueryParaphraseStepEntity[] = [];
  prevs: any[] = [];
  currents: any[] = [];

  queryParaphraseId: string = null;

  readyPage: MatPaginator;
  prevPage: MatPaginator;
  currentPage: MatPaginator;

  readySort: MatSort;
  prevSort: MatSort;
  currentSort: MatSort;

  readyPageTotalCount: number;
  prevPageTotalCount: number;
  currentPageTotalCount: number;

  removeReadyFlag = true;
  removeCurrentFlag = true;
  fileReader: any;

  readyHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'sentence', name: 'Sentence', width: '90%', isSort: true},
  ];

  prevHeader = [
    {attr: 'no', name: 'No.', no: true},
    {attr: 'orgSentence', name: 'Original Sentence', width: '45%', isSort: true},
    {attr: 'sentence', name: 'Sentence', width: '45%', isSort: true},
  ];

  currentHeader = [
    {attr: 'checkbox', name: 'Checkbox', checkbox: true, width: '5%'},
    {attr: 'no', name: 'No.', no: true},
    {attr: 'orgSentence', name: 'Original Sentence', width: '45%', isSort: true},
    {attr: 'sentence', name: 'Sentence', width: '45%', isSort: true},
  ];
  firstParaphrase = true;

  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('readyTableComponent') readyTableComponent: TableComponent;
  @ViewChild('prevTableComponent') prevTableComponent: TableComponent;
  @ViewChild('currentTableComponent') currentTableComponent: TableComponent;

  constructor(private storage: StorageBrowser,
              private dialog: MatDialog,
              private route: ActivatedRoute,
              private _formBuilder: FormBuilder,
              private queryParaphraseStepService: QueryParaphraseStepService,
              private appEnum: AppEnumsComponent,
              private cdr: ChangeDetectorRef,
              private downloadService: DownloadService,
              private store: Store<any>) {
  }

  /** Returns a FormArray with the name 'formArray'. */
  get formArray(): AbstractControl | null {
    return this.hiddenFormGroup.get('formArray');
  }


  /**
   * parameter값으로 id가 있을 경우 작성중이였던 step 단계로 이동
   * id가 없을 경우 ready화면에서 file upload부터 진행한다.
   */
  ngOnInit() {
    this.hiddenFormGroup = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
          hiddenForm: ['', Validators.required]
        })
      ])
    });

    this.route.queryParams.subscribe(
      query => {
        this.queryParaphraseId = query['id'];
        if (this.queryParaphraseId) {
          let param: QueryParaphraseStepEntity = new QueryParaphraseStepEntity();
          param.workspaceId = this.storage.get('m2uWorkspaceId');
          param.id = this.queryParaphraseId;

          this.queryParaphraseStepService.getParaphrase(param).subscribe(
            res => {
              if (res['currentStep'] === this.appEnum.paraphraseStep.READY) {
                this.changeStep();
              } else {
                this.firstParaphrase = true;
                let allStep: string[] = res['allStep'].split(',');

                if (res['currentStep'] === this.appEnum.paraphraseStep.COMPLETE) {
                  allStep.splice(allStep.indexOf(this.appEnum.paraphraseStep.COMPLETE), 1);
                  this.stepIdx = allStep.length - 1;
                } else {
                  this.stepIdx = allStep.indexOf(res['currentStep']);
                }
                allStep.forEach(step => {
                  switch (step) {
                    case this.appEnum.paraphraseStep.INVERSION:
                      this.inversion = true;
                      break;
                    case this.appEnum.paraphraseStep.WORD_EMBEDDING:
                      this.wordEmbedding = true;
                      break;
                    case this.appEnum.paraphraseStep.SYNONYM:
                      this.synonym = true;
                      break;
                    case this.appEnum.paraphraseStep.ENDING_POST_POSITION:
                      this.ending = true;
                      break;
                  }
                });

                this.setSteps();
                this.steps.some((step, index) => {
                  if (this.steps.length - 1 !== index) {
                    if (step.step === res['currentStep']) {
                      this.stepper.next();
                      this.cdr.detectChanges();
                      return true;
                    }

                    (this.hiddenFormGroup.get('formArray') as FormArray)
                      .controls[index].patchValue({hiddenForm: 'next'});
                    step.confirm = true;
                    this.stepper.next();
                    this.cdr.detectChanges();
                  }
                });

                if (res['currentStep'] === this.appEnum.paraphraseStep.COMPLETE) {
                  this.steps[this.steps.length - 1].confirm = true;
                }
                this.stepper.next();
                this.cdr.detectChanges();
              }
            },
            err => {
              this.openAlertDialog('Failed', 'Server error. failed fetch to query paraphrase.', 'error');
            }
          )
        } else {
          // this.processing = true;
        }
      }
    );
  }

  ngAfterViewInit(): void {
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * text/plain 형식의 파일을 업로드 하는 함수
   */
  fileUpload(): void {
    // get file
    let file = (<HTMLInputElement>document.getElementById('file')).files[0];

    if (!file) {
      this.openAlertDialog('Failed', `File upload failed.`, 'error');
    } else if (!file.type.match('text.*')) {
      this.openAlertDialog('Failed', `File type miss match. Only use text file.`, 'error');
    } else {
      // 1. fileReader 객체 생성
      this.fileReader = new FileReader();

      // 2. 파일을 읽는다.
      this.fileReader.readAsText(file);

      // 3. 읽을 파일의 데이터를 this.readText에 저장한다.
      this.fileReader.onload = () => {
        if (this.fileReader.result !== null || this.fileReader.result.trim() === '') {
          let param = new QueryParaphraseStepEntity();
          param.sentence = this.fileReader.result;
          param.workspaceId = this.storage.get('m2uWorkspaceId');
          param.stepCd = this.appEnum.paraphraseStep.READY;
          param.pageIndex = this.readyPage.pageIndex;
          param.pageSize = this.readyPage.pageSize;
          param.title = file.name;
          // 한 번 이라도 파일 업로드를 하면 queryParaphraseId를 다음 파라미터에 추가
          // 파일업로드시 기존에 데이터를 삭제 하기 위한 조건 값
          if (this.queryParaphraseId) {
            param.queryParaphraseId = this.queryParaphraseId;
          }
          this.queryParaphraseStepService.fileUpload(param).subscribe(
            res => {
              this.readys = res['content'];
              this.queryParaphraseId = this.readys[0].queryParaphraseId;
              this.readyPageTotalCount = res['totalElements'];
              (<HTMLInputElement>document.getElementById('file')).value = '';
            },
            err => {
              (<HTMLInputElement>document.getElementById('file')).value = '';
              this.openAlertDialog('Error', `Server error. file upload failed.`, 'error');
            });
        } else {
          (<HTMLInputElement>document.getElementById('file')).value = '';
          this.openAlertDialog('Failed', `Please enter a sentence.`, 'error');
        }
      };
    }
  }

  /**
   * csv 파일 다운로드
   * @param type => prev, current 테이블
   */
  fileDownload(type) {
    let contents: QueryParaphraseStepEntity[] = [];
    let param = new QueryParaphraseStepEntity();
    let headers = null;
    if (type === 'prev') {
      headers = this.prevHeader.filter(item => !item.no);
      param.stepCd = this.steps[this.stepIdx - 1].step;
    } else if (type === 'current') {
      headers = this.currentHeader.filter(item => !item.checkbox && !item.no);
      param.stepCd = this.steps[this.stepIdx].step;
    }
    param.queryParaphraseId = this.queryParaphraseId;
    param.workspaceId = this.storage.get('m2uWorkspaceId');

    this.queryParaphraseStepService.getAllSentences(param).subscribe(
      res => {
        if (res) {
          contents = res;
          this.downloadService.downloadCsvFile(contents, headers, 'paraphrase');
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Server error. failed fetch to sentences.', 'error');
      }
    );
  }

  /**
   * sentence paraphrase grpc call
   * @returns Promise<any> => grpc call 이후 결과
   */
  paraphrase() {
    let param = new QueryParaphraseStepEntity();
    param.workspaceId = this.storage.get('m2uWorkspaceId');
    param.queryParaphraseId = this.queryParaphraseId;
    param.prevStepCd = this.steps[this.stepIdx].step;
    param.stepCd = this.steps[this.stepIdx + 1].step;
    param.firstParaphrase = this.firstParaphrase;

    if (this.firstParaphrase) {
      param.allStep = `${this.steps.map(item => item.step).join()},${this.appEnum.paraphraseStep.COMPLETE}`;
    }

    return new Promise(resolve => {
      this.queryParaphraseStepService.paraphrase(param).subscribe(
        res => {
          this.firstParaphrase = false;
          resolve();
        },
        err => {
          this.openAlertDialog('Error', `Server error. paraphrase failed.`, 'error');
        }
      )
    });
  }


  /**
   * ReadyTable에 리스트를 불러온다.(StepCd가 R일 경우)
   * @param number pageIndex => 검색 후 pageIndex를 초기화 하기 위한 매개변수
   */
  getReadyList(pageIndex?: number) {
    // 검색시 pageIndex 초기화
    if (pageIndex !== undefined) {
      this.readyPage.pageIndex = pageIndex;
    }

    this.getSentences(this.readyPage, this.readySort,
      this.steps[this.stepIdx].step, this.readySearchKeyword).subscribe(
      res => {
        this.readys = res['content'];
        this.readyPageTotalCount = res['totalElements'];
        this.readyTableComponent.isCheckedAll = false;
        this.isReadyChecked();
      },
      err => {
        this.openAlertDialog('Error', `Server error. get sentences failed.`, 'error');
      }
    );
  }

  /**
   * PrevTable에 Paraphrase 하기 전 리스트를 불러온다.
   * @param number pageIndex => 검색 후 pageIndex를 초기화 하기 위한 매개변수
   */
  getPrevList(pageIndex?: number) {
    // 검색시 pageIndex 초기화
    if (pageIndex !== undefined) {
      this.prevPage.pageIndex = pageIndex;
    }

    this.getSentences(this.prevPage, this.prevSort,
      this.steps[this.stepIdx - 1].step, this.prevSearchKeyword).subscribe(
      res => {
        this.prevs = res['content'];
        this.prevPageTotalCount = res['totalElements'];
        this.prevTableComponent.isCheckedAll = false;
      },
      err => {
        this.openAlertDialog('Error', `Server error. get sentences failed.`, 'error');
      }
    );

  }

  /**
   * CurrentTable 에 Paraphrase 한 Sentence 리스트를 불러온다.
   * @param number pageIndex => 검색 후 pageIndex를 초기화 하기 위한 매개변수
   */
  getCurrentList(pageIndex?: number) {
    // 검색시 pageIndex 초기화
    if (pageIndex !== undefined) {
      this.currentPage.pageIndex = pageIndex;
    }

    this.getSentences(this.currentPage, this.currentSort,
      this.steps[this.stepIdx].step, this.currentSearchKeyword).subscribe(
      res => {
        this.currents = res['content'];
        this.currentPageTotalCount = res['totalElements'];
        this.currentTableComponent.isCheckedAll = false;
        this.isCurrentChecked();
      },
      err => {
        this.openAlertDialog('Error', `Server error. get sentences failed.`, 'error');
      }
    );
  }

  /**
   * ReadyTable에서 체크박스를 클릭한 적이 있는지 알 수 있는 함수
   * @param check => 체크 되었는지 안되었는지 유무(매개변수가 있다면 체크가 된 것이다.)
   */
  isReadyChecked(check?) {
    if (check) {
      this.removeReadyFlag = false;
    } else {
      this.removeReadyFlag = true;
    }
  }

  /**
   * CurrentTable에서 체크박스를 클릭한 적이 있는지 알 수 있는 함수
   * @param check => 체크 되었는지 안되었는지 유무(매개변수가 있다면 체크가 된 것이다.)
   */
  isCurrentChecked(check?) {
    if (check) {
      this.removeCurrentFlag = false;
    } else {
      this.removeCurrentFlag = true;
    }
  }

  /**
   * ReadyTable에서 Sentence를 삭제하는 함수
   */
  removeReadySentence() {
    let param: QueryParaphraseStepEntity[] =
      this.readyTableComponent.rows.filter(row => row.isChecked);

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        let originalPageIndex: number = this.readyPage.pageIndex;
        if (param.length === this.readyTableComponent.rows.length) {
          if (this.readyPage.pageIndex === 0) {
            this.readyPage.pageIndex = 0;
          } else {
            this.readyPage.pageIndex -= 1;
          }
        }

        this.queryParaphraseStepService.deleteSentences(param).subscribe(
          res => {
            this.getReadyList();
          },
          err => {
            this.readyPage.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', `Server error. remove sentences failed.`, 'error');
          }
        )
      }
    });
  }

  /**
   * CurrentTable에서 Sentence를 삭제 하는 함수
   */
  removeCurrentSentence() {
    let param: QueryParaphraseStepEntity[] =
      this.currentTableComponent.rows.filter(row => row.isChecked);

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want delete ?';
    ref.afterClosed().subscribe(confirmed => {
      if (confirmed) {
        let originalPageIndex: number = this.currentPage.pageIndex;
        if (param.length === this.currentTableComponent.rows.length) {
          if (this.currentPage.pageIndex === 0) {
            this.currentPage.pageIndex = 0;
          } else {
            this.currentPage.pageIndex -= 1;
          }
        }

        this.queryParaphraseStepService.deleteSentences(param).subscribe(
          res => {
            this.getCurrentList();
          },
          err => {
            this.currentPage.pageIndex = originalPageIndex;
            this.openAlertDialog('Error', `Server error. remove sentences failed.`, 'error');
          }
        )
      }
    });
  }

  /**
   * ReadyTable sort
   * @param MatSort sort
   */
  setSortReady(sort?: MatSort) {
    if (sort) {
      this.readySort = sort;
    }

    if (this.readys.length > 0) {
      this.getReadyList();
    }
  }

  /**
   * PrevTable sort
   * @param MatSort sort
   */
  setSortPrev(sort?: MatSort) {
    if (sort) {
      this.prevSort = sort;
    }

    if (this.prevs.length > 0) {
      this.getPrevList();
    }
  }

  /**
   * CurrentTable sort
   * @param MatSort sort
   */
  setSortCurrent(sort?: MatSort) {
    if (sort) {
      this.currentSort = sort;
    }

    if (this.currents.length > 0) {
      this.getCurrentList();
    }
  }

  /**
   * ReadyTable Pagination
   * @param MatPaginator page
   */
  setReadyPage(page?: MatPaginator) {
    if (page) {
      this.readyPage = page;
    }

    if (this.readys.length > 0) {
      this.getReadyList();
    }
  }

  /**
   * PrevTable Pagination
   * @param MatPaginator page
   */
  setPrevPage(page?: MatPaginator) {
    if (page) {
      this.prevPage = page;
    }

    if (this.prevs.length > 0) {
      this.getPrevList();
    }
  }

  /**
   * CurrentTable Pagination
   * @param MatPaginator page
   */
  setCurrentPage(page?: MatPaginator) {
    if (page) {
      this.currentPage = page;
    }

    if (this.currents.length > 0) {
      this.getCurrentList();
    }
  }

  /**
   * 하나의 함수에서, 3개의 테이블의 로직을 처리 하기위하여 매개변수를 많이 받는다.
   * @param MatPaginator pageParam => paging 처리를 위한 매개변수
   * @param MatSort sortParam => sort 처리를 위한 매개변수
   * @param string stepCd => 스텝 단계를 나타내는 매개변수
   * @param string searchKeyword => 검색을 할때 쓰는 매개변수
   * @returns Observable<any[]>
   */
  getSentences(pageParam: MatPaginator, sortParam: MatSort, stepCd: string, searchKeyword: string): Observable<any> {
    let param = new QueryParaphraseStepEntity();
    param.pageSize = pageParam.pageSize;
    param.pageIndex = pageParam.pageIndex;
    param.workspaceId = this.storage.get('m2uWorkspaceId');
    param.queryParaphraseId = this.queryParaphraseId;
    param.stepCd = stepCd;
    param.searchKeyword = searchKeyword;
    param.orderDirection = sortParam.direction === '' ||
    sortParam.direction === undefined ? 'desc' : sortParam.direction;
    param.orderProperty = sortParam.active === '' ||
    sortParam.active === undefined ? 'createdAt' : sortParam.active;

    return this.queryParaphraseStepService.getSentences(param);
  }

  /**
   * Stepper 클릭시 리스트를 불러온다.
   * Current Table 에는 Paraphrase 결과를 가져온다.
   * @param event => MatStepper
   */
  changeStep(event?) {
    if (event) {
      this.stepIdx = event.selectedIndex;
    }

    // 레이아웃 체크 및 이벤트 발생
    if (this.stepIdx === 0) {
      this.isHidden = true;
      this.getReadyList();
      this.readyPage.pageIndex = 0;
    } else {
      this.isHidden = false;
      this.prevPage.pageIndex = 0;
      this.getPrevList();
      this.currentPage.pageIndex = 0;
      this.getCurrentList();
    }
  }

  /**
   * confirm 버튼 클릭시 수행 (마지막 step만 빼고 활성화)
   */
  confirm() {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Do you want to next step? <br>
    ${this.steps[this.stepIdx].label} -> ${this.steps[this.stepIdx + 1].label} paraphrase.`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.paraphrase().then(() => {
          (this.hiddenFormGroup.get('formArray') as FormArray)
            .controls[this.stepIdx].patchValue({hiddenForm: 'next'});
          this.steps[this.stepIdx].confirm = true;
          this.stepper.next();
          this.stepIdx = this.stepper.selectedIndex;
        });
      }
    });
  }

  /**
   * complete 버튼 클릭시 수행(마지막 step만 활성화)
   */
  complete() {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Do you want complete? <br> The ${this.steps[this.stepIdx].label} is not editable.`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        let param: QueryParaphraseStepEntity = new QueryParaphraseStepEntity();
        param.currentStep = this.appEnum.paraphraseStep.COMPLETE;
        param.id = this.queryParaphraseId;
        param.workspaceId = this.storage.get('m2uWorkspaceId');

        this.queryParaphraseStepService.complete(param).subscribe(
          res => {
            this.steps[this.stepIdx].confirm = true;
            this.openAlertDialog('Success', `Paraphrase step complete.`, 'success');
          },
          err => {
            this.openAlertDialog('Error', `Server error. complete failed.`, 'error');
          });
      }
    });
  }

  /**
   * checkbox 클릭 이벤트
   * 매 클릭 이벤트 발생시 steps객체 초기화 생성
   */
  setSteps() {
    this.steps = [{step: this.appEnum.paraphraseStep.READY, confirm: false, label: 'Ready'}];
    this.userClickStep = [];
    this.hiddenFormGroup = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
          hiddenForm: ['', Validators.required]
        })
      ])
    });

    if (this.inversion) {
      this.steps.push({
        step: this.appEnum.paraphraseStep.INVERSION,
        confirm: false,
        label: 'Inversion'
      });
      this.userClickStep.push(this.appEnum.paraphraseStep.INVERSION);
      (this.hiddenFormGroup.get('formArray') as FormArray).push(this._formBuilder.group({
        hiddenForm: ['', Validators.required]
      }));
    }

    if (this.wordEmbedding) {
      this.steps.push({
        step: this.appEnum.paraphraseStep.WORD_EMBEDDING,
        confirm: false,
        label: 'Word Embedding'
      });
      this.userClickStep.push(this.appEnum.paraphraseStep.WORD_EMBEDDING);
      (this.hiddenFormGroup.get('formArray') as FormArray).push(this._formBuilder.group({
        hiddenForm: ['', Validators.required]
      }));
    }

    if (this.synonym) {
      this.steps.push({
        step: this.appEnum.paraphraseStep.SYNONYM,
        confirm: false,
        label: 'Synonym'
      });
      this.userClickStep.push(this.appEnum.paraphraseStep.SYNONYM);
      (this.hiddenFormGroup.get('formArray') as FormArray).push(this._formBuilder.group({
        hiddenForm: ['', Validators.required]
      }));
    }

    if (this.ending) {
      this.steps.push({
        step: this.appEnum.paraphraseStep.ENDING_POST_POSITION,
        confirm: false,
        label: 'Ending Post Position'
      });
      this.userClickStep.push(this.appEnum.paraphraseStep.ENDING_POST_POSITION);
      (this.hiddenFormGroup.get('formArray') as FormArray).push(this._formBuilder.group({
        hiddenForm: ['', Validators.required]
      }));
    }
  }

  /**
   * 메세지를 보여주기위한 Dialog Component를 연다.
   * @param title => 에러 메세지의 타이틀
   * @param message => 에러 메세지
   * @param status => Error, Confirm, Notice 상태 값을 가진다. 상태값에 따라 Dialog색상이 달라진다.
   */
  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
