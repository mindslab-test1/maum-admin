import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ParaphraseStepComponent} from './paraphrase-step.component';

describe('ParaphraseStepComponent', () => {
  let component: ParaphraseStepComponent;
  let fixture: ComponentFixture<ParaphraseStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ParaphraseStepComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParaphraseStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
