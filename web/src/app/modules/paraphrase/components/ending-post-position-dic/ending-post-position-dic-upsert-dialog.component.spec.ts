import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EndingPostPositionDicUpsertDialogComponent} from './ending-post-position-dic-upsert-dialog.component';

describe('EndingPostPositionDicUpsertDialogComponent', () => {
  let component: EndingPostPositionDicUpsertDialogComponent;
  let fixture: ComponentFixture<EndingPostPositionDicUpsertDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EndingPostPositionDicUpsertDialogComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndingPostPositionDicUpsertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
