import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EndingPostPositionDicComponent} from './ending-post-position-dic.component';

describe('EndingPostPositionDicComponent', () => {
  let component: EndingPostPositionDicComponent;
  let fixture: ComponentFixture<EndingPostPositionDicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EndingPostPositionDicComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndingPostPositionDicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
