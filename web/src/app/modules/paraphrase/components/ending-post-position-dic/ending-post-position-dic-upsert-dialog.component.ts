import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {EndingPostPositionDicRelEntity} from '../../entity/ending-post-position-dic/ending-post-position-dic-rel.entity';
import {EndingPostPositionDicEntity} from '../../entity/ending-post-position-dic/ending-post-position-dic.entity';
import {AlertComponent, AppEnumsComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';


@Component({
  selector: 'app-paraphrase-ending-upsert-dialog',
  templateUrl: 'ending-post-position-dic-upsert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class EndingPostPositionDicUpsertDialogComponent implements OnInit {

  param: any;
  service: any;
  role: string;

  name: string;
  endingPostPositionDic: EndingPostPositionDicEntity = new EndingPostPositionDicEntity();
  representationWordValidator = new FormControl('', [Validators.required]);
  wordValidator = new FormControl('');
  typeValidator = new FormControl('', [Validators.required]);
  disabled = true;
  data: any;
  word = '';
  originRepresentationWord;

  constructor(public dialogRef: MatDialogRef<EndingPostPositionDicUpsertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              public appEnum: AppEnumsComponent,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.role = this.data.role;
    this.endingPostPositionDic = this.data.row;
    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.originRepresentationWord = this.endingPostPositionDic.representation;
      this.disabled = false;
    } else {
      if (this.endingPostPositionDic.endingPostPositionDicRelEntities === undefined) {
        this.endingPostPositionDic.endingPostPositionDicRelEntities = [];
      }
    }

  }

  add() {
    this.checkExist(this.endingPostPositionDic.representation, 'representationWord').then(result => {
      if (result) {
        this.endingPostPositionDic.workspaceId = this.storage.get('m2uWorkspaceId');
        this.service.addEndingPostPositionDic(this.endingPostPositionDic).subscribe(res => {
          this.openAlertDialog('Success', 'Add Success', 'success');
          this.dialogRef.close(true);
        }, err => {
          this.openAlertDialog('Error', 'Failed to add', 'error');
          console.error('error', err);
        })
      }
    });
  }

  addWord(): void {
    if (this.word !== '') {
      this.checkExist(this.word, 'word').then(result => {
        if (result) {
          // INSERT
          let toBeInsertedSynonymRelEntity = new EndingPostPositionDicRelEntity();
          toBeInsertedSynonymRelEntity.word = this.word;
          toBeInsertedSynonymRelEntity.workspaceId = this.storage.get('m2uWorkspaceId');
          if (this.endingPostPositionDic.endingPostPositionDicRelEntities === undefined) {
            this.endingPostPositionDic.endingPostPositionDicRelEntities = [];
          }
          this.endingPostPositionDic.endingPostPositionDicRelEntities.push(toBeInsertedSynonymRelEntity);
          this.word = ''; // init
        }
        this.checkInvalid();
      })
    }
  }

  update() {
    this.checkExist(this.endingPostPositionDic.representation, 'representationWord').then(result => {
      if (this.originRepresentationWord === this.endingPostPositionDic.representation || result) {
        this.service.editEndingPostPositionDic(this.endingPostPositionDic).subscribe(res => {
          this.openAlertDialog('Success', 'Update Success', 'success');
          this.dialogRef.close(true);
        }, err => {
          this.openAlertDialog('Error', 'Failed to update', 'error');
          console.error('error', err);
        })
      }
    });
  }

  remove() {
    let tempArray = [];
    tempArray.push(this.endingPostPositionDic);
    this.service.removeEndingPostPositionDics(tempArray).subscribe(res => {
      this.openAlertDialog('Success', 'Remove Success', 'success');
      this.dialogRef.close(true);
    }, err => {
      this.openAlertDialog('Error', 'Failed to remove', 'error');
      console.error('error', err);
    })
  }

  removeRel(entity: EndingPostPositionDicRelEntity): void {
    if (entity.representationId !== undefined) { // 이미 DB에 등록되어있는 Rel들
      if ((this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities === null) ||
        (this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities === undefined)) {
        this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities = [];
      }
      this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities.push(entity);
    }
    this.endingPostPositionDic.endingPostPositionDicRelEntities.splice(
      this.endingPostPositionDic.endingPostPositionDicRelEntities.indexOf(entity), 1);
  }

  checkExist(word: string, type: string) {
    return new Promise(resolve => {
      if (this.endingPostPositionDic.endingPostPositionDicRelEntities === undefined ||
        this.endingPostPositionDic.endingPostPositionDicRelEntities === null) {
        this.endingPostPositionDic.endingPostPositionDicRelEntities = [];
      }
      if (this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities === undefined ||
        this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities === null) {
        this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities = [];
      }
      let valid = true;
      this.endingPostPositionDic.endingPostPositionDicRelEntities.forEach(entity => {
        // Rel 들과 중복 비교
        if (entity.word === word) {
          valid = false;
        }
      });
      if ((this.endingPostPositionDic.representation === word) && (type === 'word')) {
        // representation word 와 중복 비교
        valid = false;
      }

      if (valid) {
        // DB 값과 중복 비교
        let tmpArray = [];
        tmpArray.push(word);
        this.service.checkWordExist(tmpArray, this.storage.get('m2uWorkspaceId')).subscribe(res => {
          if (res.result) {
            resolve(true);
          } else {
            let valid2 = false;
            this.endingPostPositionDic.toBeDeletedEndingPostPositionDicRelEntities
            .forEach(entity => {
              if (entity.word === word) {
                valid2 = true;
              }
            });
            if (valid2) {
              resolve(true);
            } else {
              if (type === 'word') {
                this.wordValidator.setErrors({
                  'duplicate': true
                });
              } else {
                this.representationWordValidator.setErrors({
                  'duplicate': true
                });
              }
              resolve(false);
            }
          }
        }, err => {
          resolve(false);
        })
      } else {
        if (type === 'word') {
          this.wordValidator.setErrors({
            'duplicate': true
          });
        } else {
          this.representationWordValidator.setErrors({
            'duplicate': true
          });
        }
        resolve(false);
      }
    });
  }

  checkInvalid($event?) {
    if ('INVALID' === this.representationWordValidator.status) {
      this.disabled = true;
      return false;
    } else if (
      ('VALID' === this.representationWordValidator.status) &&
      ('VALID' === this.typeValidator.status) &&
      (this.endingPostPositionDic.endingPostPositionDicRelEntities.length !== 0)) {
      this.disabled = false;
      return true;
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
