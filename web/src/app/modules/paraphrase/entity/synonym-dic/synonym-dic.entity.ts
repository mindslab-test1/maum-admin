import {SynonymDicRelEntity} from './synonym-dic-rel.entity';
import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class SynonymDicEntity extends PageParameters {

  id: string;
  workspaceId: string;
  representation: string;
  synonymWord: string;
  createAt: Date;
  createId?: string;
  synonymDicRelEntities: SynonymDicRelEntity[];
  toBeDeletedSynonymDicRelEntities: SynonymDicRelEntity[];
  searchRepresentativeWord?: string;
  searchSynonym?: string;
}
