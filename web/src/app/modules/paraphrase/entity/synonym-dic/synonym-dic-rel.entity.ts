import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class SynonymDicRelEntity extends PageParameters {

  id: string;
  workspaceId: string;
  representationId: string;
  synonymWord: string;
  createAt: Date;
  createId?: string;
}
