import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class EndingPostPositionDicRelEntity extends PageParameters {

  id: string;
  workspaceId: string;
  representationId: string;
  word: string;
  createdAt: Date;
  creatorId?: string;
}
