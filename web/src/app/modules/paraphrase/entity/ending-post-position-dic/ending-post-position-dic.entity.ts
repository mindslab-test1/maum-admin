import {EndingPostPositionDicRelEntity} from './ending-post-position-dic-rel.entity';
import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class EndingPostPositionDicEntity extends PageParameters {

  id?: string;
  workspaceId?: string;
  representation?: string;
  word?: string;
  type?: string;
  createdAt?: Date;
  creatorId?: string;
  endingPostPositionDicRelEntities?: EndingPostPositionDicRelEntity[];
  toBeDeletedEndingPostPositionDicRelEntities?: EndingPostPositionDicRelEntity[];
  searchRepresentativeWord?: string;
  searchWord?: string;
}
