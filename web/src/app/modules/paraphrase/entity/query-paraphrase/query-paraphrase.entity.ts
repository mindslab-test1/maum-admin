import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class QueryParaphraseEntity extends PageParameters {
  id: string;
  workspaceId: string;
  title: string;
  currentStep: string;
  allStep: string;
  creatorId: string;
  createdAt: Date;
}
