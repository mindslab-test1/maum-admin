import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ParaphrasesComponent} from './components/paraphrases/paraphrases.component';
import {ParaphraseStepComponent} from './components/paraphrase-step/paraphrase-step.component';
import {SynonymDicComponent} from './components/synonym-dic/synonym-dic.component';
import {EndingPostPositionDicComponent} from './components/ending-post-position-dic/ending-post-position-dic.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: ParaphrasesComponent,
        data: {
          nav: {
            name: 'Query Paraphrase',
            comment: 'Query Paraphrase',
            icon: 'list',
          }
        },
      },
      {
        path: 'step',
        component: ParaphraseStepComponent,
        data: {
          nav: {
            name: 'Query Paraphrase Step',
            comment: 'Query Paraphrase Step',
            icon: 'call_split',
          }
        },
      },
      {
        path: 'synonym',
        component: SynonymDicComponent,
        data: {
          nav: {
            name: 'Synonym',
            comment: 'synonym dic',
            icon: 'library_books',
          }
        },
      },
      {
        path: 'ending-post-position',
        component: EndingPostPositionDicComponent,
        data: {
          nav: {
            name: 'Ending PostPosition',
            comment: 'synonym dic',
            icon: 'library_books',
          }
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParaphraseRoutingModule {
}
