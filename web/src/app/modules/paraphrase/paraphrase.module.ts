import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ParaphraseRoutingModule} from './paraphrase-routing.module';
import {EndingPostPositionDicComponent} from './components/ending-post-position-dic/ending-post-position-dic.component';
import {EndingPostPositionDicUpsertDialogComponent} from './components/ending-post-position-dic/ending-post-position-dic-upsert-dialog.component';
import {SynonymDicComponent} from './components/synonym-dic/synonym-dic.component';
import {SynonymDicUpsertDialogComponent} from './components/synonym-dic/synonym-dic-upsert-dialog.component';
import {ParaphrasesComponent} from './components/paraphrases/paraphrases.component';
import {ParaphraseStepComponent} from './components/paraphrase-step/paraphrase-step.component';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatStepperModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {QueryParaphraseService} from './services/query-paraphrase/query-paraphrase.service';
import {QueryParaphraseStepService} from './services/query-paraphrase-step/query-paraphrase-step.service';
import {NqaCategoryUpsertComponent} from '../qa-natural-questions/components/nqa-category-upsert/nqa-category-upsert.component';
import {NqaChannelUpsertComponent} from '../qa-natural-questions/components/nqa-channel-upsert/nqa-channel-upsert.component';

@NgModule({
  declarations: [
    EndingPostPositionDicComponent,
    EndingPostPositionDicUpsertDialogComponent,
    SynonymDicComponent,
    SynonymDicUpsertDialogComponent,
    ParaphrasesComponent,
    ParaphraseStepComponent],
  imports: [
    CommonModule,
    ParaphraseRoutingModule,
    MatToolbarModule,
    MatStepperModule,
    MatFormFieldModule,
    MatCardModule,
    SharedModule,
    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatRadioModule,
    MatCheckboxModule
  ],
  providers: [
    QueryParaphraseService,
    QueryParaphraseStepService
  ],
  entryComponents: [
    SynonymDicUpsertDialogComponent,
    EndingPostPositionDicUpsertDialogComponent
  ]
})
export class ParaphraseModule {
}
