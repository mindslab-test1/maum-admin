import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {QueryParaphraseEntity} from '../../entity/query-paraphrase/query-paraphrase.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class QueryParaphraseService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getParaphrases(param: QueryParaphraseEntity): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qp/get-paraphrases/`, param)
  }

  deleteParaphrases(param: QueryParaphraseEntity[]): Observable<any> {
    return this.http.post(`${this.API_URL}/paraphrase/qp/delete-paraphrases/`, param)
  }

}
