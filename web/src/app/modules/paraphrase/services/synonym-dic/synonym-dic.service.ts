import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SynonymDicEntity} from '../../entity/synonym-dic/synonym-dic.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class SynonymDicService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  checkWordExist(wordList: string[], workspaceId: string): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/synonymDic/checkWordExist/' + workspaceId, wordList);
  }

  getSynonymDicListByWorkspaceId(synonymDicEntity: SynonymDicEntity): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/synonymDic/getSynonymDicListByWorkspaceId', synonymDicEntity);
  }

  addSynonymDic(synonymDicEntity: SynonymDicEntity): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/synonymDic/addSynonymDic', synonymDicEntity);
  }

  editSynonymDic(synonymDicEntity: SynonymDicEntity): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/synonymDic/editSynonymDic', synonymDicEntity);
  }

  removeSynonymDics(synonymDicEntities: SynonymDicEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/synonymDic/removeSynonymDics', synonymDicEntities);
  }

  removeSynonymDicsByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/synonymDic/removeSynonymDics/all', workspaceId);
  }
}

