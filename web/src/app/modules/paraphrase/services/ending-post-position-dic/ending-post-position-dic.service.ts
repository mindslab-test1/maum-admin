import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EndingPostPositionDicEntity} from '../../entity/ending-post-position-dic/ending-post-position-dic.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class EndingPostPositionDicService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  checkWordExist(wordList: string[], workspaceId: string): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/endingPostPositionDic/checkWordExist/' + workspaceId, wordList);
  }

  getEndingPostPositionDicListByWorkspaceId(endingPostPositionDicEntity: EndingPostPositionDicEntity): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/endingPostPositionDic/getEndingPostPositionDicListByWorkspaceId',
      endingPostPositionDicEntity);
  }

  addEndingPostPositionDic(endingPostPositionDicEntity: EndingPostPositionDicEntity): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/endingPostPositionDic/addEndingPostPositionDic',
      endingPostPositionDicEntity);
  }

  editEndingPostPositionDic(endingPostPositionDicEntity: EndingPostPositionDicEntity): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/endingPostPositionDic/editEndingPostPositionDic',
      endingPostPositionDicEntity);
  }

  removeEndingPostPositionDics(endingPostPositionDicEntities: EndingPostPositionDicEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/endingPostPositionDic/removeEndingPostPositionDics',
      endingPostPositionDicEntities);
  }

  removeEndingPostPositionDicsByWorkspaceId(workspaceId: string): Observable<any> {
    return this.http.post(this.API_URL + '/paraphrase/endingPostPositionDic/removeEndingPostPositionDics/all',
      workspaceId);
  }
}

