import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class NerCorpusTagEntity extends PageParameters {
  id: number;
  tag: string;
  word: string;
  createdAt: Date;
}
