import {PageParameters} from 'app/shared/entities/pageParameters.entity'

export class NerCorpusEntity extends PageParameters {
  id: number;
  sentence: string;
  createdAt: Date;
}
