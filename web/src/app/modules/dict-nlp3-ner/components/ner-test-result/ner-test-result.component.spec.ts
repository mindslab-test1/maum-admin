import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NerTestResultComponent} from './ner-test-result.component';

describe('NerTestResultComponent', () => {
  let component: NerTestResultComponent;
  let fixture: ComponentFixture<NerTestResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerTestResultComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerTestResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
