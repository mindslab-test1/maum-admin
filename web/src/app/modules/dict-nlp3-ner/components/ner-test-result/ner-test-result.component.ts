import {
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {NerDictionaryService} from '../../services/ner-dictionaries/ner-dictionary.service';
import {NerTestResultService} from '../../services/ner-test-result/ner-test-result.service';

import {MatDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {ROUTER_LOADED} from '../../../../core/actions';
import {AlertComponent, DateFormatPipe} from '../../../../shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';

interface ResultInfo {
  id: string;
  dictionaryName: string;
  createdAt: string;
  tester: string;
  workspaceId: string;
}

interface DicInfo {
  id: string;
  name: string;
  workspace: string;
  type: string;
}

enum DicType {
  NER = 0, // 개체명 사전
  MORPH = 1, // 형태소 사전
}

@Component({
  selector: 'app-ner-test-result',
  templateUrl: './ner-test-result.component.html',
  styleUrls: ['./ner-test-result.component.scss']
})
export class NerTestResultComponent implements OnChanges {

  @Input() dictionaryId: string;
  @Input() dictionaryName: string;
  @Input() dictionaryType: string;
  @Input() testState: number;

  public gridOptions: GridOptions;
  public defaultColDef;

  actions: any;
  type: DicType;
  dictionary: DicInfo = {
    id: undefined,
    name: '',
    workspace: undefined,
    type: undefined
  };
  row_data: ResultInfo[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private nerDictionaryService: NerDictionaryService,
              private testResultService: NerTestResultService,
              private store: Store<any>,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onRowSelected: this.onRowSelected,
      onCellClicked: (event) => {
        if (event.colDef.field === 'detail') {
          this.router.navigate(['../../../test-result/ner', event.data.dictionaryName, event.data.id], {relativeTo: this.route});
        }
      },
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      }
    };

    this.actions = [{
      type: 'delete',
      text: 'Delete',
      icon: 'delete_sweep',
      callback: this.delete,
      disabled: true,
      hidden: false
    }];
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnChanges(changes: SimpleChanges) {
    try {
      if (this.dictionaryId && this.dictionaryType.toUpperCase() in DicType) {
        this.row_data = [];
        if (DicType[this.dictionaryType.toUpperCase()] === DicType.NER) {
          this.nerDictionaryService.getDictionaryName(this.dictionaryId).subscribe(res => {
            if (res.hasOwnProperty('dictionary_name')) {
              this.dictionaryName = res.dictionary_name;
            }
            this.getData();
          });
        }
      }
    } catch (e) {
      this.openAlertDialog('Load Result', 'Something wrong!', 'error');
    }
  }

  getData = () => {
    this.row_data = [];
    this.testResultService.getResult(this.dictionaryType, this.dictionaryId).subscribe(
      res => {
        if (res.hasOwnProperty('list')) {
          res.list.forEach(one => {
            let elem: ResultInfo = {
              id: undefined,
              dictionaryName: this.dictionaryName,
              createdAt: undefined,
              tester: undefined,
              workspaceId: undefined,
            };
            elem.id = one.id;
            elem.tester = one.tester;
            elem.workspaceId = one.workspaceId;
            let dateFormatPipe = new DateFormatPipe();
            elem.createdAt = dateFormatPipe.transform(one.createdAt);
            this.row_data.push(elem);
          });
          this.gridOptions.api.setRowData(this.row_data);
        }
      }, err => {
        this.openAlertDialog('Load Result', 'Something wrong!', 'error');
      }
    );
  };

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    if (selectedData.length > 0) {
      let ids: number[] = selectedData.map(i => i.id);
      this.testResultService.deleteResult(this.dictionaryType, ids).subscribe(res => {
        this.openAlertDialog('Delete', `Corpus file downloaded successfully.`, 'success');
        this.getData();
      }, err => {
        this.openAlertDialog('Delete', 'Something wrong.', 'error');
      });
    }
  };

  onRowSelected = (param) => {
    let deleteBtn = this.actions.filter(y => y.type === 'delete')[0];
    if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
      deleteBtn.disabled = false;
    } else {
      deleteBtn.disabled = true;
    }
  };

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 2,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 1,
      hide: true,
      editable: false,
    }, {
      headerName: 'Dictionary name',
      field: 'dictionaryName',
      width: 40,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'Created date',
      field: 'createdAt',
      width: 40,
      editable: false
    }, {
      headerName: 'Detail',
      field: 'detail',
      width: 40,
      filter: 'agTextColumnFilter',
      cellRenderer: this.nameRenderer,
      editable: false
    }]
  }

  nameRenderer = (params) => {
    return params.value = '<span style="color: #E57373;">view</sapn>';
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }
}
