import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NerTestComponent} from './ner-test.component';

describe('NerTestComponent', () => {
  let component: NerTestComponent;
  let fixture: ComponentFixture<NerTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
