import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NerCorpusComponent} from './ner-corpus.component';

describe('NerCorpusComponent', () => {
  let component: NerCorpusComponent;
  let fixture: ComponentFixture<NerCorpusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerCorpusComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerCorpusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
