import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NerCorpusTagComponent} from './ner-corpus-tags.component';

describe('NerCorpusTagComponent', () => {
  let component: NerCorpusTagComponent;
  let fixture: ComponentFixture<NerCorpusTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerCorpusTagComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerCorpusTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
