import {Component, Inject} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {MatDialog} from '@angular/material';
import {AlertComponent, DownloadService} from 'app/shared';
import {NerCorpusTagService} from '../../services/ner-corpus-tags/ner-corpus-tag.service';
import {NerCorpusTagEntity} from '../../entity/ner-corpus-tags/ner-corpus-tag.entity';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

interface Item {
  tag: string,
  word: string
}

type tag = Item[];

@Component({
  selector: 'app-ner-corpus-tag',
  templateUrl: './ner-corpus-tags.component.html',
  styleUrls: ['./ner-corpus-tags.component.scss']
})
export class NerCorpusTagComponent {
  public gridApi;
  public gridColumnApi;
  public gridOptions: GridOptions;
  public defaultColDef;
  row_data: tag = [];
  actions: any[] = [];
  editable = false;
  title = 'app';
  pre_row_data: tag = [];
  reader: any;

  constructor(private nerCorpusTagService: NerCorpusTagService,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private downloadService: DownloadService,
              private store: Store<any>,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onRowSelected: this.onRowSelected,
      onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
      }
    };
    this.actions = [{
      type: 'edit',
      text: 'Edit',
      icon: 'edit_circle_outline',
      callback: this.edit,
      hidden: false
    }, {
      type: 'add',
      text: 'Add',
      icon: 'playlist_add',
      disabled: false,
      callback: this.addLine,
      hidden: true
    }, {
      type: 'cancel',
      text: 'Cancel',
      icon: 'cancel',
      disabled: false,
      callback: this.cancel,
      hidden: true
    }, {
      type: 'delete',
      text: 'Delete',
      icon: 'delete_sweep',
      disabled: true,
      callback: this.deleteLine,
      hidden: true
    }, {
      type: 'save', text: 'Save', icon: 'save', disabled: false, callback: this.save, hidden: true
    },
      {
        type: 'download',
        text: 'Download',
        icon: 'file_download',
        disabled: false,
        callback: this.download,
        hidden: false
      }, {
        type: 'upload',
        text: 'Upload',
        icon: 'file_upload',
        disabled: false,
        callback: this.upload,
        hidden: true,
      },];

    this.load();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  onRowSelected = (param) => {
    let deleteBtn = this.actions.filter(y => y.type === 'delete')[0];
    if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
      deleteBtn.disabled = false;
    } else {
      deleteBtn.disabled = true;
    }

  };

  load = () => {
    this.nerCorpusTagService.getCorpusTags().subscribe(
      res => {
        this.row_data = [];
        if (res.hasOwnProperty('tag_list')) {
          res.tag_list.forEach(x => {
            this.row_data.push({tag: x.tag, word: x.word});
          });
        }
        this.gridOptions.api.setRowData(this.row_data);
        this.gridOptions.columnApi.setColumnVisible('no', false);
      }, err => {
        this.openAlertDialog('Loading', `Something wrong!`, 'error');
      }
    );
  };

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  updateToolbar = () => {
    this.actions.forEach(action => {
      if (action.type === 'edit' || action.type === 'download') {
        action.hidden = !this.editable;
      } else {
        action.hidden = this.editable;
      }
    });
    this.editable = !this.editable;
    if (this.editable) {
      this.gridOptions.columnApi.setColumnVisible('no', true);
    } else {
      this.gridOptions.api.stopEditing();
      this.gridOptions.columnApi.setColumnVisible('no', false);
    }
  };

  edit = () => {
    this.pre_row_data = this.row_data;
    this.gridOptions.api.getColumnDef('tag').editable = true;
    this.gridOptions.api.getColumnDef('word').editable = true;
    this.updateToolbar();
  };

  cancel = () => {
    this.row_data = this.pre_row_data;
    this.gridOptions.api.setRowData(this.row_data);
    this.updateToolbar();
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  download = () => {
    let res: any = this.row_data;
    this.downloadService.downloadJsonFile(JSON.stringify(res), 'tags_for_ner_test');
    this.openAlertDialog('Download', `tag file downloaded successfully.`, 'success');
  };

  upload = () => {
    let elem: HTMLInputElement = document.getElementById('selectFile') as HTMLInputElement;
    elem.click();
  };

  getdata = (param) => {
    let file = param.target.files[0];

    const target: DataTransfer = <DataTransfer>(param.target);
    if (file.type.match(/\/json/)) {
      this.reader = new FileReader();
      this.reader.onload = (e: any) => {
        try {
          let load_data: tag = JSON.parse(this.reader.result);
          this.gridOptions.api.setRowData(load_data);
          this.openAlertDialog('Upload', `Your corpus file uploaded successfully.`, 'success');
        } catch (ex) {
          this.openAlertDialog('Upload', `Check your corpus file data.`, 'error');
          return;
        }
      };
      this.reader.readAsText(file);
    } else {
      this.openAlertDialog('Upload', `Check your corpus file format.`, 'error');
    }
  };

  addLine = () => {
    let newItem = {
      tag: '',
      word: ''
    };
    let res = this.gridOptions.api.updateRowData({add: [newItem]});
  };

  deleteLine = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    let res = this.gridOptions.api.updateRowData({remove: selectedData});
  };

  save = () => {
    this.gridOptions.api.stopEditing();
    let params: NerCorpusTagEntity[] = [];
    let tmp: tag = [];
    this.gridOptions.api.forEachNode(function (node) {
      let param: NerCorpusTagEntity = new NerCorpusTagEntity();
      param = node.data;
      params.push(param);
      tmp.push({tag: param.tag, word: param.word});
    });

    return new Promise((res2, rej2) => {
      this.nerCorpusTagService.deleteTags().subscribe(
        res => {
          res2(params);
        }, err => {
          rej2();
        }
      );
    }).then((par: NerCorpusTagEntity[]) => {
      this.gridOptions.api.getColumnDef('tag').editable = false;
      this.gridOptions.api.getColumnDef('word').editable = false;
      this.nerCorpusTagService.insertTags(par).subscribe(
        res => {
          this.row_data = tmp;
          this.updateToolbar();
          this.openAlertDialog('Save', `Data saved completely.`, 'success');
        }, err => {
          throw err;
        }
      );
    }).catch(() => {
      this.openAlertDialog('Save', 'Something wrong!', 'error');
    });
  };

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 35,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Tag',
      field: 'tag',
      width: 400,
      filter: 'agTextColumnFilter',
      editable: false,
      cellEditorSelector: function (params) {
        if (params.data.type === 'tag') {
          return {
            component: 'agLargeTextCellEditor'
          };
        }
      }
    }, {
      headerName: 'Word',
      field: 'word',
      width: 400,
      filter: 'agTextColumnFilter',
      editable: false,
      cellEditorSelector: function (params) {
        if (params.data.type === 'word') {
          return {
            component: 'agLargeTextCellEditor'
          };
        }
      }
    }];
  }

  back() {
    window.history.back();
  }
}
