import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NerDictionariesComponent} from './ner-dictionaries.component';

describe('NerDictionariesComponent', () => {
  let component: NerDictionariesComponent;
  let fixture: ComponentFixture<NerDictionariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerDictionariesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerDictionariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
