import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NerDictionaryDetailComponent} from './ner-dictionary-detail.component';

describe('NerDictionaryDetailComponent', () => {
  let component: NerDictionaryDetailComponent;
  let fixture: ComponentFixture<NerDictionaryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NerDictionaryDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NerDictionaryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
