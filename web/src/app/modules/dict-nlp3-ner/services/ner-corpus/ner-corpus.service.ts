import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {NerCorpusEntity} from '../../entity/ner-corpus/ner-corpus.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class NerCorpusService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  insertLines(corpusEntities: NerCorpusEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/corpus/insert-corpusList', corpusEntities);
  }

  deleteLines(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/corpus/delete-corpusList', null);
  }

  getCorpus(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/corpus/get-corpusList', null);
  }

  // NLU 태깅 결과
  testCorpus(sentences: string[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/corpus/test', sentences);
  }

  replaceCorpus(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/corpus/replaceList', null);
  }
}
