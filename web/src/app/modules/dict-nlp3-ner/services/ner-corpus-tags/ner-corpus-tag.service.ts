import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {NerCorpusTagEntity} from '../../entity/ner-corpus-tags/ner-corpus-tag.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class NerCorpusTagService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  insertTags(tagEntities: NerCorpusTagEntity[]): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/tag/insert-tagList', tagEntities);
  }

  deleteTags(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/tag/delete-tagList', null);
  }

  getCorpusTags(): Observable<any> {
    return this.http.post(this.API_URL + '/dictionary/ner/tag/get-tagList', null);
  }
}
