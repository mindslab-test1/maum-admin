import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DictNlp3NerRoutingModule} from './dict-nlp3-ner-routing.module';
import {NerDictionariesComponent} from './components/ner-dictionaries/ner-dictionaries.component';
import {NerDictionaryDetailComponent} from './components/ner-dictionary-detail/ner-dictionary-detail.component';
import {NerTestComponent} from './components/ner-test/ner-test.component';
import {NerCorpusTagComponent} from './components/ner-corpus-tags/ner-corpus-tags.component';
import {NerCorpusComponent} from './components/ner-corpus/ner-corpus.component';
import {NerTestResultComponent} from './components/ner-test-result/ner-test-result.component';
import {AgGridModule} from 'ag-grid-angular';
import {SharedModule} from '../../shared/shared.module';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatIconModule,
  MatListModule,
  MatToolbarModule
} from '@angular/material';
import {NerDictionaryService} from './services/ner-dictionaries/ner-dictionary.service';
import {NerCorpusService} from './services/ner-corpus/ner-corpus.service';
import {NerCorpusTagService} from './services/ner-corpus-tags/ner-corpus-tag.service';
import {NerTestResultService} from './services/ner-test-result/ner-test-result.service';

@NgModule({
  declarations: [
    NerDictionariesComponent,
    NerDictionaryDetailComponent,
    NerTestComponent,
    NerTestResultComponent,
    NerCorpusTagComponent,
    NerCorpusComponent,
  ],
  imports: [
    CommonModule,
    DictNlp3NerRoutingModule,
    AgGridModule.withComponents([]),
    SharedModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatChipsModule,
    MatCardModule,
    MatCheckboxModule,
    MatListModule,
  ],
  exports: [
    NerDictionariesComponent,
    NerDictionaryDetailComponent,
    NerTestResultComponent,
    NerTestComponent,
    NerCorpusTagComponent,
    NerCorpusComponent,
  ],
  providers: [
    NerDictionaryService,
    NerTestResultService,
    NerCorpusService,
    NerCorpusTagService,
  ]
})
export class DictNlp3NerModule {
}
