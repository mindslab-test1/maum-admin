import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NerDictionariesComponent} from './components/ner-dictionaries/ner-dictionaries.component';
import {NerCorpusComponent} from './components/ner-corpus/ner-corpus.component';
import {NerDictionaryDetailComponent} from './components/ner-dictionary-detail/ner-dictionary-detail.component';
import {NerTestComponent} from './components/ner-test/ner-test.component';
import {NerCorpusTagComponent} from './components/ner-corpus-tags/ner-corpus-tags.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: NerDictionariesComponent,
      },
      {
        path: 'corpus',
        component: NerCorpusComponent
      },
      {
        path: 'tags',
        component: NerCorpusTagComponent
      },
      {
        path: ':id/test',
        component: NerTestComponent
      },
      {
        path: ':id',
        component: NerDictionaryDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictNlp3NerRoutingModule {
}
