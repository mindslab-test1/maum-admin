import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {jsPlumbToolkitModule} from 'jsplumbtoolkit-angular';
import {jsPlumbToolkitDragDropModule} from 'jsplumbtoolkit-angular-drop';
import {SharedModule} from '../../shared/shared.module';
import {DialogModelDesignRoutingModule} from './dialog-model-design-routing.module';
import {ControlsComponent} from './design/components/controls';
import {
  StartNodeComponent,
  EndNodeComponent,
  FromNodeComponent,
  ToNodeComponent,
  TaskNodeComponent,
  GotoNodeComponent,
} from './design/components/components';
import {DialogDesignsDesignComponent} from './design/components/dialog-designs-design.component';
import {DefaultSkillListComponent} from './design/components/default-skill-list.component';
import {DialogModelModule} from '../dialog-model/dialog-model.module';
import {DialogDesignAddSkillUpsertComponent} from './design/components/dialog-design-add-skill-upsert.component';
import {MoveSkillConfirmComponent} from './design/components/move-skill-confirm.component';
import {MsgCodeDialogDesignsDesignComponent} from './design/components/msg-code-dialog-designs-design.component';
import {WebEditorComponent} from '../../shared/components/web-editor/web-editor.component';
import {ScenarioSimulatorComponent} from '../../shared/components/scenario-simulator/scenario-simulator.component';


export function createTranslateLoader(http: HttpClient) {
  // 다국어 파일의 확장자와 경로를 지정
  return new TranslateHttpLoader(http, './assets/i18n/dialog-model-design/', '.json');
}

@NgModule({
  declarations: [
    ControlsComponent,
    StartNodeComponent,
    EndNodeComponent,
    FromNodeComponent,
    ToNodeComponent,
    TaskNodeComponent,
    GotoNodeComponent,
    DialogDesignsDesignComponent,
    DefaultSkillListComponent,
    DialogDesignAddSkillUpsertComponent,
    MoveSkillConfirmComponent,
    MsgCodeDialogDesignsDesignComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    jsPlumbToolkitModule,
    jsPlumbToolkitDragDropModule,
    SharedModule,
    DialogModelDesignRoutingModule,
    DialogModelModule,
  ],
  entryComponents: [
    StartNodeComponent,
    EndNodeComponent,
    FromNodeComponent,
    ToNodeComponent,
    TaskNodeComponent,
    GotoNodeComponent,
    DialogDesignAddSkillUpsertComponent,
    MoveSkillConfirmComponent,
    WebEditorComponent,
    ScenarioSimulatorComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DialogModelDesignModule {
}
