import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DialogDesignsDesignComponent} from './design/components/dialog-designs-design.component';
import {MsgCodeDialogDesignsDesignComponent} from './design/components/msg-code-dialog-designs-design.component';

const routes: Routes = [
  {
    path: '',
    children: [
      // {
      //   path: 'new',
      //   component: DialogDesignsNewComponent
      // },
      {
        // path: ':id/edit',
        path: 'edit',
        component: DialogDesignsDesignComponent
      },
      {
        path: 'msg',
        component: MsgCodeDialogDesignsDesignComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DialogModelDesignRoutingModule {
}
