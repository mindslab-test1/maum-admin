import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class DialogDesignEntity extends PageParameters {

  id: string;

  creatorId: string;
  createdAt: Date;
  updaterId: string;
  updatedAt: Date;
  workspaceId: string;

  skillId: string;
  skillName: string;
  topicSentence: string;
  status: string;
  description: string;

  group1: string;
  group2: string;
  group3: string;
  group4: string;

  searchOption: string;

  metaYn: string;
  linkYn: string;

  suggestSkills: string;
  nextSkills: string;
  importSkills: string;

  jsonData: string;

  ids: string[] = [];
  subSkillType: string;
}
