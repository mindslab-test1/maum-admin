import {
  ChangeDetectorRef,
  Component,
  ElementRef, EventEmitter,
  Inject, Input, Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {DialogDesignEntity} from '../entity/dialog-design.entity';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {
  MatDialog,
  MatPaginator,
  MatSidenav,
  MatSort,
  MatTableDataSource
} from '@angular/material';
import {DialogDesignService} from '../services/dialog-design.service';
import {
  AlertComponent,
  AppObjectManagerService, ConfirmComponent,
  TableComponent
} from 'app/shared';
import {DialogDesignAddSkillUpsertComponent} from './dialog-design-add-skill-upsert.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-d-skill-list-module',
  templateUrl: './default-skill-list.component.html',
  styleUrls: ['./default-skill-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DefaultSkillListComponent {

  @ViewChild('skillNameTemplate') skillNameTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('designLoader') designLoader: ElementRef;
  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('tableComponent') subTableComponent: TableComponent;

  @Input() systemCodeList: any[] = [];
  @Input() defaultNodes;
  @Input() selectedSkill = [];
  @Output() outputSelectedSkillInfo = new EventEmitter<string>();
  @Output() outputImportMetaSkill = new EventEmitter<string[]>();
  @Output() outputMoveSkill = new EventEmitter<string[]>();

  actions: any;
  subscription = new Subscription();
  filterKeyword: FormControl;

  workspaceId: string;

  skills = [];
  metaYn = false;
  importSkillList: string[] = [];
  showSkillList: any[] = [];

  selectedTab: string;

  dataSource: MatTableDataSource<any>;
  dataSourcePart: MatTableDataSource<any>;

  rows: any[] = [];
  sort: MatSort;
  pageParam: MatPaginator;
  param = new DialogDesignEntity();
  headerAll: any[];
  lengthAll = 0;
  generalTabLabels = [];
  metaTabLabels = [];
  orderDirection = 'desc';
  orderProperty = 'updatedAt';

  headerPart: any[] = [];
  lengthPart = 0;
  subSort: MatSort;
  subPageParam: MatPaginator;
  subOrderDirection = 'desc';
  subOrderProperty = 'skillId';

  searchOptions = ['18', 'skillName'];
  searchText = '';
  searchButton;

  optionList = [];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private translate: TranslateService,
              public  datepipe: DatePipe,
              private dialogDesignService: DialogDesignService,
              private cdrf: ChangeDetectorRef,
              private elementRef: ElementRef,
              private objectManager: AppObjectManagerService,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private store: Store<any>,
              private fb: FormBuilder) {
    // 현재 사용 언어 설정
    translate.use('ko');
  }

  ngOnInit() {
    // this.getFileGroups();

    this.translate.get(['TAB.SUGGEST_SKILLS', 'TAB.PREV_SKILLS', 'TAB.NEXT_SKILLS', 'TAB.IMPORT_SKILLS', 'TAB.EXPORT_SKILLS',
      'TABLE.ID', 'TABLE.NAME', 'TABLE.STATUS', 'TABLE.GROUP1', 'TABLE.GROUP2', 'TABLE.GROUP3', 'TABLE.GROUP4',
      'TABLE.CREATE_AT', 'TABLE.CREATE_NAME', 'TABLE.UPDATED_AT', 'TABLE.UPDATED_NAME', 'TABLE.IMPORT',
      'TABLE.LINK_YN', 'LABEL.SEARCH']).subscribe(trans => {
      this.generalTabLabels = [trans['TAB.SUGGEST_SKILLS'], trans['TAB.PREV_SKILLS'],
        trans['TAB.NEXT_SKILLS']];
      this.metaTabLabels = [trans['TAB.EXPORT_SKILLS'], trans['TAB.SUGGEST_SKILLS']];

      this.headerAll = [
        {attr: 'checkbox', name: 'checkbox', checkbox: true, width: '50px'},
        {attr: 'skillId', name: trans['TABLE.ID'], isSort: true, width: '100px'},
        {
          attr: 'skillName',
          name: trans['TABLE.NAME'],
          isSort: true,
          onClick: this.confirmNavigateSkill,
          width: '200px'
        },
        {attr: 'status_label', name: trans['TABLE.STATUS'], width: '100px'},
        {attr: 'updatedAt', name: trans['TABLE.UPDATED_AT'], width: '150px', isSort: true},
        {attr: 'updaterId_label', name: trans['TABLE.UPDATED_NAME'], width: '150px'},
        {attr: 'group1_label', name: trans['TABLE.GROUP1'], width: '150px'},
        {attr: 'group2', name: trans['TABLE.GROUP2'], width: '150px', isSort: true},
        {attr: 'group3', name: trans['TABLE.GROUP3'], width: '150px', isSort: true},
        {attr: 'group4', name: trans['TABLE.GROUP4'], width: '150px', isSort: true},
        {attr: 'linkYn', name: trans['TABLE.LINK_YN'], width: '100px', isSort: true},
        {attr: 'createdAt', name: trans['TABLE.CREATE_AT'], width: '150px', isSort: true},
        {attr: 'creatorId_label', name: trans['TABLE.CREATE_NAME'], width: '150px'},
        // {attr: 'import', name: trans['TABLE.IMPORT'], width: '100px', asIcon: true, onClick: this.importMetaSkill},
      ];

      this.headerPart = [
        {attr: 'skillId', name: trans['TABLE.ID'], isSort: true, width: '100px'},
        {
          attr: 'skillName',
          name: trans['TABLE.NAME'],
          isSort: true,
          onClick: this.confirmNavigateSkill,
          width: '200px'
        },
        {attr: 'status_label', name: trans['TABLE.STATUS'], width: '100px'},
        {attr: 'group1_label', name: trans['TABLE.GROUP1'], width: '150px'},
        {attr: 'group2', name: trans['TABLE.GROUP2'], width: '150px', isSort: true},
        {attr: 'group3', name: trans['TABLE.GROUP3'], width: '150px', isSort: true},
        {attr: 'group4', name: trans['TABLE.GROUP4'], width: '150px', isSort: true}
      ];

      this.optionList = [ // option2
        {'id': 0, 'name': 'skillName', 'label': trans['TABLE.NAME']},
        {'id': 1, 'name': 'skillId', 'label': trans['TABLE.ID']},
        {'id': 2, 'name': 'group2', 'label': trans['TABLE.GROUP2']},
        {'id': 3, 'name': 'group3', 'label': trans['TABLE.GROUP3']},
        {'id': 4, 'name': 'group4', 'label': trans['TABLE.GROUP4']},
        {'id': 5, 'name': 'status', 'label': trans['TABLE.STATUS']},
        {'id': 6, 'name': 'updaterId', 'label': trans['TABLE.UPDATED_NAME']}
      ];

      this.searchButton = {
        type: 'search',
        text: trans['LABEL.SEARCH'],
        icon: 'archive',
        callback: this.keyupFunc,
        hidden: false,
      };
    });

    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.orderProperty = this.storage.get('scenarioOrderProperty') ? this.storage.get('scenarioOrderProperty') : this.orderProperty;
    this.orderDirection = this.storage.get('scenarioOrderDirection') ? this.storage.get('scenarioOrderDirection') : this.orderDirection;
    this.subOrderProperty = this.storage.get('scenarioOrderProperty') ? this.storage.get('scenarioOrderProperty') : this.subOrderProperty;
    this.subOrderDirection = this.storage.get('scenarioOrderDirection') ? this.storage.get('scenarioOrderDirection') : this.subOrderDirection;
    this.filterKeyword = this.fb.control('');
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.subPageParam.pageSize = 10;
    this.subPageParam.pageIndex = 0;
    if (this.systemCodeList.length > 0) {
      this.searchOptions[0] = this.systemCodeList['group1Code'][0];
      console.log('this.searchOptions[0]', this.searchOptions[0]);
    }

    if (this.selectedSkill.length <= 0) {
      this.selectedSkill = undefined;
    }

    this.getSkills();
    this.cdrf.detectChanges();
    // router load 완료
    // this.store.dispatch({type: ROUTER_LOADED});
  }

  setSort(matSort?: MatSort) {
    this.sort = matSort;

    if (this.skills.length > 0) {
      this.getSkills();
    }
  }

  setSubSort(matSort?: MatSort) {
    console.log('matSort: ', matSort);
    this.subSort = matSort;

    if (this.skills.length > 0) {
      this.showSkillInfo().then();
    }
  }

  setOption(e, optionIdx) {
    console.log('set option: ', e, optionIdx);
    if (e.value !== undefined) {
      if (optionIdx === '0') {
        this.searchOptions[optionIdx] = e.value;
      } else if (optionIdx === '1') {
        this.searchOptions[optionIdx] = this.changeIdToName(this.optionList, e.value);
      }
    } else if (e.value === undefined) {
      this.searchOptions[optionIdx] = '';
    }
    console.log('changed search option: ', this.searchOptions);
  }

  keyupFunc = () => {
    console.log('keyup searchText: ', this.searchText);
    this.getSkills(this.pageParam.pageIndex);
  }

  getPaginator(page?: MatPaginator) {
    this.pageParam = page;

    if (this.skills.length > 0) {
      this.getSkills();
    }
  }

  getSubPaginator(page?: MatPaginator) {
    this.subPageParam = page;

    if (this.showSkillList.length > 0) {
      this.showSkillInfo().then();
    }
  }

  changeIdToName(datas?, id?) {
    let name = '';
    if (datas) {
      datas.forEach(data => {
        if (data['id'] === id) {
          name = data['name'];
        }
      });
    }
    return name;
  }

  changeNameToId(datas?, name?) {
    let id = '';
    if (datas) {
      datas.forEach(data => {
        if (data['name'] === name) {
          id = data['id'];
        }
      });
    }
    return id;
  }

  changeValueToLabel = (skill): Promise<any> => {
    return new Promise((resolve) => {
      {
        if (this.systemCodeList) {
          skill['status_label'] = this.changeIdToName(this.systemCodeList['statusCode'], skill['status']);
          skill['group1_label'] = this.changeIdToName(this.systemCodeList['group1Code'], skill['group1']);
        }
        skill['creatorId_label'] = skill['createUserEntity']['username'];
        skill['updaterId_label'] = skill['updateUserEntity']['username'];
        skill['createdAt'] = this.datepipe.transform(skill['createdAt'], 'yyyy-MM-dd HH:mm:ss');
        skill['updatedAt'] = this.datepipe.transform(skill['updatedAt'], 'yyyy-MM-dd HH:mm:ss');
      }
      resolve();
    });
  }

  getSkills = (pageIndex?) => {
    this.param = new DialogDesignEntity();
    this.param.workspaceId = this.workspaceId;
    if (pageIndex !== undefined) {
      this.pageParam.pageIndex = pageIndex;
    }
    // 검색 조건
    this.param['group1'] = this.searchOptions[0];
    this.param[this.searchOptions[1]] = this.searchText.toUpperCase();

    this.param.pageSize = this.pageParam.pageSize;
    this.param.pageIndex = this.pageParam.pageIndex;
    // 테이블이 초기화 되어도 변수로 이전 상태 값을 유지할 수 있도록 한다.
    this.param.orderDirection = this.sort.direction === '' ||
    this.sort.direction === undefined ? this.orderDirection : this.sort.direction;
    this.param.orderProperty = this.sort.active === '' ||
    this.sort.active === undefined ? this.orderProperty : this.sort.active;

    // 테이블 order 상태 업데이트
    this.orderDirection = this.param.orderDirection;
    this.orderProperty = this.param.orderProperty;

    console.log('getSkills this.selectedSkill: ', this.selectedSkill);
    this.dialogDesignService.getSkillLists(this.param).subscribe(res => {
        console.log('getSkills result: ', res['content']);
        if (res['content'].length > 0) {
          this.skills = [];
          res['content'].forEach(content => {
            this.skills.push(content);
          });
          this.skills.forEach(skill => {
            this.changeValueToLabel(skill).then();
          });

          // 선택된 스킬이 없을 경우
          if (this.selectedSkill === undefined || this.selectedSkill.length === 0) {
            this.selectedSkill = this.skills[0];
            this.metaCheck(this.skills[0]['metaYn']);
            this.getSkill(this.skills[0]['id']);
            // this.checkMetaData(this.skills[0]);
          } else {
            this.showSkillInfo().then();
          }

          this.lengthAll = res['totalElements'];
          this.dataSource = new MatTableDataSource(this.skills);
        } else if (res['content'].length <= 0 || res['totalElements'] === 0) {
          if (this.searchText.length !== 0) {
            this.skills = [];
            this.showSkillList = [];
            this.lengthAll = 0;
          } else {
            this.skills = [];
            this.selectedSkill = [];
            this.showSkillList = [];
            this.lengthAll = 0;
            this.outputSelectedSkillInfo.emit();
          }
        }
      },
      err => {
        this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_GET_SKILL_LIST', 'error');
      }
    );
  };

  updateSkill(param: DialogDesignEntity, msg?: string) {
    this.dialogDesignService.updateSkill(param).subscribe(res => {
        if (res) {
          let message = msg ? msg : 'MSG.COMPLETE_SAVE_SKILL';
          this.getSkills();
          this.openAlertDialog('TITLE.SUCCESS', message, 'success');
        }
      },
      err => {
        this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_UPDATE_SKILL', 'error');
        return false;
      }
    );
    return true;
  }

  metaCheck(metaYn: string) {
    if (metaYn === 'Y') {
      this.metaYn = true;
      this.selectedTab = this.metaTabLabels[0];
    } else if (metaYn === 'N') {
      this.metaYn = false;
      if (this.selectedTab) {
        if (this.selectedTab.length === 0) {
          this.selectedTab = this.generalTabLabels[0];
        }
      }
    }
    this.showSkillInfo().then();
  }

  importMetaSkill: any = (row: any) => {
    console.log('importMetaSkill: ', row);
    if (row.valueOf().metaYn === 'N') {
      this.openAlertDialog('TITLE.ALERT', 'MSG.ALERT_IMPORT_GENERAL_SKILL', 'notice');
    } else {
      if (this.selectedSkill['metaYn'] === 'Y') {
        this.openAlertDialog('TITLE.ALERT', 'MSG.ALERT_IMPORT_META_SKILL', 'notice');
      } else if (this.selectedSkill['metaYn'] === 'N') {
        if (this.selectedSkill['importSkills'] && this.selectedSkill['importSkills'].includes(row['id'])) {
          this.openAlertDialog('TITLE.ALERT', 'MSG.ALERT_IMPORT_DUPLICATION_SKILL', 'notice');
        } else {
          this.outputImportMetaSkill.emit(row);
          this.openAlertDialog('TITLE.SUCCESS', 'MSG.COMPLETE_IMPORT_SKILL', 'success');
        }
      }
    }
  };

  // 임포트된 스킬 업데이트 체크
  checkMetaData(skillInfo: string[]) {
    console.log('checkMetaData: ', skillInfo);
    if (skillInfo['importSkills']) {
      let change = false;
      const selectedJson = JSON.parse(skillInfo['jsonData']);
      if (selectedJson) {
        let tmpNodeJson = [];
        let tmpEdgeJson = [];
        let totalNodeJson = [];
        let totalEdgeJson = [];

        // meta skill의 id별로 각각 노드, 엣지 데이터 추출
        this.importSkillList.forEach(importSkill => {
          selectedJson['nodes'].forEach(node => {
            if (node['skillId'] === importSkill['id']) {
              tmpNodeJson.push(node);
            }
          });
          totalNodeJson[importSkill['id']] = tmpNodeJson;
          tmpNodeJson = [];

          selectedJson['edges'].forEach(edge => {
            if (edge['data']['skillId'] === importSkill['id']) {
              tmpEdgeJson.push(edge);
            }
          });
          totalEdgeJson[importSkill['id']] = tmpEdgeJson;
          tmpEdgeJson = []
        });

        // 다를수밖에 없는 값들 덮어쓰기 (위치값, 노드 크기 등)
        this.importSkillList.forEach(importSkill => {
          let importSkillJson = JSON.parse(importSkill['jsonData']);

          totalNodeJson[importSkill['id']].forEach(node => {
            let originNodes = importSkillJson['nodes'];
            originNodes.forEach(originNode => {
              if (originNode['id'] === node['id']) {
                originNode['left'] = node['left'];
                originNode['top'] = node['top'];
                originNode['w'] = node['w'];
                originNode['h'] = node['h'];
                originNode['skillId'] = node['skillId'];
              }
            });
          });

          totalEdgeJson[importSkill['id']].forEach(edge => {
            let originEdges = importSkillJson['edges'];
            originEdges.forEach(originEdge => {
              if (originEdge['data']['id'] === edge['data']['id']) {
                originEdge['data']['skillId'] = edge['data']['skillId'];
              }
            });
          });

          // 비교해서 다르면 general skill에서 해당 메타 스킬 아이디를 가지고 있는 애들 삭제
          let compareNodes = (JSON.stringify(importSkillJson['nodes']) === JSON.stringify(totalNodeJson[importSkill['id']]));
          let compareEdges = (JSON.stringify(importSkillJson['nodes']) === JSON.stringify(totalNodeJson[importSkill['id']]));
          if (!compareNodes || !compareEdges) {
            change = true;
            let filteredNodes = [];
            let filteredEdges = [];

            for (let nodeIdx in selectedJson['nodes']) {
              if (selectedJson['nodes'][nodeIdx]['skillId'] !== importSkill['id']) {
                filteredNodes.push(selectedJson['nodes'][nodeIdx]);
              }
            }
            // 메타스킬의 노드 정보 다시 넣어주기
            selectedJson['nodes'] = filteredNodes;
            importSkillJson['nodes'].forEach(node => {
              node['skillId'] = importSkill['id'];
              selectedJson['nodes'][selectedJson.nodes.length] = node;
            });

            for (let edgeIdx in selectedJson['edges']) {
              if (selectedJson['edges'][edgeIdx]['data']['skillId'] !== importSkill['id']) {
                if (JSON.stringify(selectedJson.nodes).includes(selectedJson['edges'][edgeIdx]['source']) &&
                    JSON.stringify(selectedJson.nodes).includes(selectedJson['edges'][edgeIdx]['target'])) {
                  filteredEdges.push(selectedJson['edges'][edgeIdx]);
                }
              }
            }
            // 메타스킬의 엣지 정보 다시 넣어주기
            selectedJson['edges'] = filteredEdges;
            importSkillJson['edges'].forEach(edge => {
              edge['data']['skillId'] = importSkill['id'];
              selectedJson['edges'][selectedJson.edges.length] = edge;
            });
          }
        });
      }

      if (change) {
        skillInfo['jsonData'] = JSON.stringify(selectedJson, null, 2);
        this.openAlertDialog('TITLE.CAUTION', 'MSG.ALERT_IMPORT_SKILL_CHANGED', 'notice');
      }
      this.outputSelectedSkillInfo.emit(skillInfo['jsonData']);
    } else {
      this.outputSelectedSkillInfo.emit(skillInfo['jsonData']);
    }
  }

  editModel() {
    this.objectManager.set('dialog-design', this.tableComponent.rows.filter(row => row.isChecked));
    console.log('objectManager: ', this.objectManager);
  }

  confirmNavigateSkill: any = (row: any) => {
    this.outputMoveSkill.emit(row);
  }

  navigateSkillDetailView: any = (row: any) => {
    this.selectedSkill = [];
    this.showSkillList = [];
    this.importSkillList = [];
    this.lengthPart = 0;

    this.getSkill(row['id']);
  };

  getSkill(id) {
    this.param = new DialogDesignEntity();
    this.param.id = id;

    this.dialogDesignService.getSkill(this.param).subscribe(res => {
        if (res) {
          this.selectedSkill = res;
          this.metaCheck(this.selectedSkill['metaYn']);
          let selectedSkillJson = JSON.parse(this.selectedSkill['jsonData']);
          for (let col in this.selectedSkill) {
            if (col !== 'jsonData') {
              selectedSkillJson[col] = this.selectedSkill[col];
            }
          }
          this.selectedSkill['jsonData'] = JSON.stringify(selectedSkillJson, null, 2);
          this.outputSelectedSkillInfo.emit(this.selectedSkill['jsonData']);

          // if (this.selectedSkill['importSkills']) {
          //   this.getImportSkills(false).then(resolve => {
          //     if (resolve && this.selectedSkill['jsonData']) {
          //       // this.checkMetaData(this.selectedSkill);
          //     }
          //   });
          // } else {
          //   this.outputSelectedSkillInfo.emit(this.selectedSkill['jsonData']);
          // }
        }
      },
      err => {
        let title = 'TITLE.ERROR';
        let message = 'MSG.ERROR_GET_SKILL';

        this.translate.get([title, message]).subscribe(trans => {
          let ref = this.dialog.open(AlertComponent);
          ref.componentInstance.header = 'error';
          ref.componentInstance.title = trans[title];
          ref.componentInstance.message = trans[message];
        });
      });
  }

  setTabName(event) {
    console.log('event: ', event);
    if (event.tab) {
      this.showSkillList = [];
      this.lengthPart = 0;
      this.selectedTab = event.tab.textLabel;
      this.showSkillInfo().then();
    }
  }

  showSkillInfo(): Promise<Boolean> {
    return new Promise((resolve) => {
      this.translate.get(['TAB.SUGGEST_SKILLS', 'TAB.PREV_SKILLS', 'TAB.NEXT_SKILLS', 'TAB.IMPORT_SKILLS', 'TAB.EXPORT_SKILLS']).subscribe(trans => {
        if (this.selectedTab === trans['TAB.SUGGEST_SKILLS'] && this.selectedSkill['suggestSkills']) {
          if (this.selectedSkill['suggestSkills'].length > 0) {
            this.getSubSkills('suggest');
          }
        } else if (this.selectedTab === trans['TAB.IMPORT_SKILLS'] && this.selectedSkill['importSkills'].length > 0) {
          this.getImportSkills(true).then();
        } else if (this.selectedTab === trans['TAB.NEXT_SKILLS'] && this.selectedSkill['nextSkills'].length > 0) {
          this.getSubSkills('next');
        } else if (this.selectedTab === trans['TAB.EXPORT_SKILLS']) {
          this.getParentsSkills('export').then();
        } else if (this.selectedTab === trans['TAB.PREV_SKILLS']) {
          this.getParentsSkills('prev').then();
        }
      });
      resolve();
      // if (this.selectedSkill['importSkills']) {
      //   this.getImportSkills(false).then(() => {
      //       resolve();
      //     }
      //   );
      // } else {
      //   resolve();
      // }
    });
  }

  getSubSkills = (subSkillType: string) => {
    let result = true;
    let callApiResult = [];
    this.param = new DialogDesignEntity();
    this.param.workspaceId = this.workspaceId;
    this.param.suggestSkills = this.selectedSkill['suggestSkills'];
    this.param.nextSkills = this.selectedSkill['nextSkills'];
    this.param.skillId = this.selectedSkill['skillId'];
    this.param.subSkillType = subSkillType;

    this.param.pageSize = this.subPageParam.pageSize;
    this.param.pageIndex = this.subPageParam.pageIndex;
    // 테이블이 초기화 되어도 변수로 이전 상태 값을 유지할 수 있도록 한다.
    this.param.orderDirection = this.subSort.direction === '' ||
    this.subSort.direction === undefined ? this.subOrderDirection : this.subSort.direction;
    this.param.orderProperty = this.subSort.active === '' ||
    this.subSort.active === undefined ? this.subOrderProperty : this.subSort.active;

    console.log('getsubskill param: ', this.param);
    this.dialogDesignService.getSubSkills(this.param).subscribe(res => {
        if (res) {
          console.log('subskill res: ', res);
          this.showSkillList = [];
          callApiResult = res.content;
          callApiResult.forEach(skill => {
            this.changeValueToLabel(skill).then(() => {
              this.showSkillList.push(skill);
            });
          });
          this.lengthPart = res.totalElements;
          this.dataSourcePart = new MatTableDataSource(this.showSkillList);
        }
      },
      err => {
        result = false;
        this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_GET_SKILL_LIST', 'error');
      }
    );
    return result;
  };

  getImportSkills = (option: boolean): Promise<any> => {
    return new Promise((resolve) => {
      let result = true;
      this.param = new DialogDesignEntity();
      this.param.workspaceId = this.workspaceId;
      this.param.skillId = this.selectedSkill['skillId'];
      this.param.importSkills = this.selectedSkill['importSkills'];

      this.dialogDesignService.getImportSkills(this.param).subscribe(res => {
          if (res) {
            this.importSkillList = [];
            for (let importSkill of res) {
              if (importSkill) {
                this.changeValueToLabel(importSkill).then(() => {
                  this.importSkillList.push(importSkill);
                });
              }
            }
            // option이 true일때만 화면에 뿌려주기
            // false이면 리스트 가져오기만
            if (option) {
              this.showSkillList = res;
              this.lengthPart = res.length;
            }
          }
          resolve(result);
        },
        err => {
          result = false;
          this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_GET_SKILL_LIST', 'error');
          resolve(false);
        }
      );
    });
  };

  getParentsSkills = (parentsSkillType: string): Promise<any> => {
    return new Promise((resolve) => {
      this.showSkillList = [];

      if (parentsSkillType === 'prev') {
        for (let skill of this.skills) {
          let skillList = skill['nextSkills'];
          if (skillList) {
            if (skillList.includes(this.selectedSkill['skillId'])) {
              this.changeValueToLabel(skill).then(() => {
                this.showSkillList.push(skill);
              });
            }
          }
        }
      } else if (parentsSkillType === 'export') {
        for (let skill of this.skills) {
          let skillList = skill['importSkills'];
          if (skillList) {
            if (skillList.includes(this.selectedSkill['id'])) {
              this.showSkillList.push(skill);
            }
          }
        }
      }
      resolve(true);
    });
  };

  addSkill(): void {
    const dialogRef = this.dialog.open(DialogDesignAddSkillUpsertComponent, {
      width: '250px',
      height: '220px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('addSkill afterClosed', result);
      if (result) {
        this.selectedSkill = result;
        this.getSkills();
        let newJsonData = JSON.parse(result['jsonData']);
        newJsonData['id'] = result['id'];
        this.outputSelectedSkillInfo.emit(JSON.stringify(newJsonData, null, 2));
      }
    });
  }

  deleteSkill(): void {
    this.param = new DialogDesignEntity();
    this.param.workspaceId = this.workspaceId;

    if (this.objectManager.get('dialog-design')) {
      let dialogDesign = this.objectManager.get('dialog-design');
      for (let key in dialogDesign) {
        if (key) {
          this.param.ids.push(dialogDesign[key]['id']);
          this.lengthAll--;
        }
      }
    }
    console.log('this.param.ids: ', this.param.ids);

    this.translate.get(['MSG.CONFIRM_DELETE_SKILL']).subscribe(trans => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = trans['MSG.CONFIRM_DELETE_SKILL'];
      ref.afterClosed()
        .subscribe(result => {
          if (result) {
            this.dialogDesignService.deleteCheckedSkill(this.param).subscribe(() => {
                this.param.ids.forEach(item => {
                  if (this.selectedSkill !== undefined) {
                    if (item === this.selectedSkill['id']) {
                      this.selectedSkill = undefined;
                      this.showSkillList = [];
                      this.lengthPart = 0;
                    }
                  }
                });
                this.getSkills();
              },
              err => {
                this.openAlertDialog('TITLE.ERROR', 'ERROR_DELETE_SKILL', 'error');
              }
            );
          }
        });
    });
  };

  openAlertDialog(title, message, status) {
    this.translate.get([title, message]).subscribe(trans => {
      let ref = this.dialog.open(AlertComponent);
      ref.componentInstance.header = status;
      ref.componentInstance.title = trans[title];
      ref.componentInstance.message = trans[message];
    });
  }
}
