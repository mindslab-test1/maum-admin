import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Inject,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Store} from '@ngrx/store';
import {Dialogs, DrawingTools, jsPlumb, jsPlumbToolkit, jsPlumbUtil, Surface} from 'jsplumbtoolkit';
import {
  jsPlumbService,
  jsPlumbSurfaceComponent
} from 'jsplumbtoolkit-angular';

import {
  BaseEditableNodeComponent,
  StartNodeComponent,
  EndNodeComponent,
  FromNodeComponent,
  ToNodeComponent,
  TaskNodeComponent,
  GotoNodeComponent,
} from './components';
import {ROUTER_LOADED} from '../../../../core/actions';
import {
  AlertComponent,
  DownloadService,
  getErrorString,
  KorTypeToEngPipe,
} from '../../../../shared';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {DialogDesignEntity} from '../entity/dialog-design.entity';
import {DefaultSkillListComponent} from './default-skill-list.component';
import {DialogDesignService} from '../services/dialog-design.service';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-d-scenario-outline',
  templateUrl: './dialog-designs-design.component.html',
  styleUrls: ['./dialog-designs-design.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DialogDesignsDesignComponent {

  @ViewChild('designLoader') designLoader: ElementRef;
  @ViewChild(jsPlumbSurfaceComponent) surfaceComponent: jsPlumbSurfaceComponent;
  @ViewChild('upsertDialogName') upsertDialogName: ElementRef;
  @ViewChild('patternInput') patternInput: ElementRef;
  @ViewChild(DefaultSkillListComponent) defaultSkillComponent: DefaultSkillListComponent;

  korTypeToEng;

  actions: any[] = [];
  save: any;
  scenarioName: String;
  uploadId: string;

  toolkit: jsPlumbToolkit;
  toolkitId: string;
  toolkitParams = {};

  surface: Surface;
  surfaceId: string;

  nodeNames = ['start', 'task', 'end', 'sds', 'goto'];
  nodeTypes: any[] = [];

  tableTopic: any = [];
  systemCodeList: string[] = [];
  edgeTypeList = [
    {'value': 'text', 'label': 'Text', 'color': 'rgb(51, 204, 51)'},
    {'value': 'button', 'label': 'Button', 'color': 'rgb(51, 153, 255)'},
    {'value': 'transaction', 'label': 'Transaction', 'color': 'rgb(187, 153, 255)'}
    ];
  baseEditableNodeComponent = new BaseEditableNodeComponent();

  param = new DialogDesignEntity();

  workspaceId: string;
  userId: string;
  selectedSkillInfo = [];
  selectedSkillName: string;
  topicSentence: string;
  showEditSpace = false;

  cursor = 'default';
  width = 400;
  x = 0;
  grabber = false;

  removeNodeButton = document.createElement('button');
  removeEdgeButton = document.createElement('button');

  view = {
    nodes: {
      selectable: {
        events: {
          dblclick: (params: any) => {
            console.log('task-node double-clicked', params);
            this.appendTaskNode('U', params.node);
          },
          click: (params: any) => {
            console.log('task-node mousedown', params);
              if (params['node']['data'] !== this.currentNodeData) {
                this.toggleSelectionNode(params.node);
              }
              this.showEditSpace = true;
          },
        },
      },
      'start': {
        parent: 'selectable',
        component: StartNodeComponent,
      },
      'end': {
        parent: 'selectable',
        component: EndNodeComponent,
      },
      'from': {
        // parent: 'selectable',
        component: FromNodeComponent,
        events: {
          click: (params: any) => {
            console.log('from-node clicked', params);
            this.toggleSelectionNode(params.node);
          },
          dblclick: (params: any) => {
            console.log('from-node double-clicked', params);
            this.designLoader.nativeElement.click();
          },
        },
      },
      'to': {
        // parent: 'selectable',
        component: ToNodeComponent,
        events: {
          dblclick: (params: any) => {
            console.log('to-node double-clicked', params);
            this.designLoader.nativeElement.click();
          },
          click: (params: any) => {
            console.log('to-node clicked', params);
            this.toggleSelectionNode(params.node);
          },
        },
      },
      'task': {
        parent: 'selectable',
        component: TaskNodeComponent,
      },
      'goto': {
        parent: 'selectable',
        component: GotoNodeComponent,
      },
    },
    edges: {
      'default': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        connector: ['Bezier', {curviness: 70}],
        paintStyle: {
          strokeWidth: 3,
          stroke: 'rgb(132, 172, 179)',
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'}, // hover paint style for this edge type.
        overlays: [
          ['Arrow', {location: 1, width: 11, length: 11}],
        ],
        events: {
          click: (params: any) => {
            console.log('process edge click', params);
            this.selectionEdge(params.edge);
          }
        },
      },
      'connection': {
        parent: 'default',
        overlays: [
          [
            'Label', {
            label: '${label}',
            cssClass: 'aLabel',
            events: {
              dblclick: (params: any) => {
                console.log('connection edge click', params);
                this.editEdge(params.edge, 'U');
              },
            },
          },
          ],
        ],
      },
      'text': {
        parent: 'connection',
        paintStyle: {
          strokeWidth: 3,
          stroke: this.edgeTypeList[0]['color'],
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'} // hover paint style for this edge type.
      },
      'button': {
        parent: 'connection',
        paintStyle: {
          strokeWidth: 3,
          stroke: this.edgeTypeList[1]['color'],
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'} // hover paint style for this edge type.
      },
      'transaction': {
        parent: 'connection',
        paintStyle: {
          strokeWidth: 3,
          stroke: this.edgeTypeList[2]['color'],
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'} // hover paint style for this edge type.
      }
    },
    ports: {
      'start': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'end': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'from': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'to': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'task': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'goto': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
    },
  };

  renderParams = {
    layout: {
      type: 'Spring',
    },
    events: {
      canvasClick: (params: any) => {
        console.log('canvasClick', params);
        this.showEditSpace = false;
        this.toolkit.clearSelection();
        this.surface.zoomToFit({fill: 0.9, padding: 10});
      },
      edgeAdded: (params: any) => {
        if (params.addedByMouse) {
          console.log('params.addedByMouse', params);
          // this.editLabel(params.edge);
          if (!params.source.data.skillId || !params.target.data.skillId) {
            this.editEdge(params.edge, 'A');
          } else {
            this.toolkit.removeEdge(params.edge.data.id);
            Dialogs.show({
              id: 'dlgMessage',
              title: '엣지 등록',
              data: {
                msg: '임포트된 스킬의 노드간에는 엣지를 생성할 수 없습니다.',
              },
            });
          }
        }
      },
    },
    consumeRightClick: false,
    dragOptions: {
      filter: '.jtk-draw-handle, .node-action, .node-action i',
    },
  };

  selectType: String;
  currentNodeData: any;
  currentEdgeData: any;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private store: Store<any>, private $jsplumb: jsPlumbService,
              private translate: TranslateService,
              private dialog: MatDialog,
              private dialogDesignService: DialogDesignService,
              private elementRef: ElementRef,
              private downloadService: DownloadService) {
    // 현재 사용 언어 설정
    translate.use('ko');

    const dialogs = {
      dialogs: {
        dlgRadio: [
          '<form>' +
          ' <input type="radio" name="edge_radio" jtk-focus jtk-att="radio" value="' + this.edgeTypeList[0]['value'] + '" jtk-commit="true"/>' + this.edgeTypeList[0]['label'] +
          ' <input type="radio" name="edge_radio" jtk-focus jtk-att="radio" value="' + this.edgeTypeList[1]['value'] + '" jtk-commit="true"/>' + this.edgeTypeList[1]['label'] +
          ' <input type="radio" name="edge_radio" jtk-focus jtk-att="radio" value="' + this.edgeTypeList[2]['value'] + '" jtk-commit="true"/>' + this.edgeTypeList[2]['label'] +
          '</form>' +
          '<br/><br/>' +
          '<input type="text" size="50" jtk-focus jtk-att="text" value="${text}" jtk-commit="true"/>',
          'Select Type',
          true],
        dlgText: [
          '<input type="text" size="50" jtk-focus jtk-att="text" value="${text}" jtk-commit="true"/>',
          'Enter Text',
          true],
        dlgConfirm: ['${msg}?', 'Please Confirm', true],
        dlgMessage: ['${msg}', 'Message', false],
      },
    };

    Dialogs.initialize(dialogs);
    this.toolkitId = 'flowchart';
    this.surfaceId = 'flowchartSurface';
    this.toolkit = this.$jsplumb.getToolkit(this.toolkitId); // , this.toolkitParams);
  }

  ngOnInit() {
    console.log('ngOnInit toolkitId :: ', this.toolkitId);
    this.getSystemCode().then(() => {
      this.korTypeToEng = new KorTypeToEngPipe();
      this.translate.get(['LABEL.ADD_NODE', 'LABEL.SAVE', 'LABEL.IMPORT', 'LABEL.EXPORT', 'LABEL.SIMULATION',
        'TABLE.ID', 'TABLE.NAME', 'TABLE.TOPIC', 'TABLE.SUGGEST', 'TABLE.STATUS', 'TABLE.GROUP1', 'TABLE.GROUP2',
        'TABLE.GROUP3', 'TABLE.GROUP4', 'TABLE.STATUS']).subscribe(trans => {
        this.actions = [
          {
            type: 'add',
            text: trans['LABEL.ADD_NODE'],
            icon: 'add_circle_outline',
            callback: this.appendTaskNode,
            hidden: false,
          },
          {
            type: 'load',
            text: trans['LABEL.IMPORT'],
            icon: 'archive',
            // callback: () => this.designLoader.nativeElement.click(),
            hidden: true,
          },
          {
            type: 'export',
            text: trans['LABEL.EXPORT'],
            icon: 'unarchive',
            // callback: this.exportJson,
            hidden: true,
          },
          {
            type: 'simulation',
            text: trans['LABEL.SIMULATION'],
            icon: 'archive',
            // callback: this.commit,
            hidden: true,
          },
        ];

        this.save = {
          type: 'save',
          text: trans['LABEL.SAVE'],
          icon: 'archive',
          callback: this.commit,
          hidden: false,
        };

        this.tableTopic = [{attr: 'skillName', label : trans['TABLE.NAME'], type: 'input', data: null},
          {attr: 'skillId', label : trans['TABLE.ID'], type: 'input', data: null},
          {attr: 'topicSentence', label : trans['TABLE.TOPIC'], type: 'input', data: null},
          {attr: 'suggestSkills', label : trans['TABLE.SUGGEST'], type: 'input_identifier', data: null},
          {attr: 'group1', label : trans['TABLE.GROUP1'], type: 'select', data: {options: this.systemCodeList['group1Code']}},
          {attr: 'group2', label : trans['TABLE.GROUP2'], type: 'input', data: null},
          {attr: 'group3', label : trans['TABLE.GROUP3'], type: 'input', data: null},
          {attr: 'group4', label : trans['TABLE.GROUP4'], type: 'input', data: null},
          {attr: 'status', label : trans['TABLE.STATUS'], type: 'select', data: {options: this.systemCodeList['statusCode']}}];
      });
    });

    this.store.dispatch({type: ROUTER_LOADED});
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.userId = this.storage.get('user').id;

    while (this.nodeNames.length) {
      let name = this.nodeNames.reverse().pop();
      this.nodeTypes.push({
        type: name,
        label: name,
        w: 80,
        h: 25,
      });
    }
  }

  ngAfterViewInit() {
    this.surface = this.surfaceComponent.surface;

    const drawingTools = new DrawingTools({
      renderer: this.surface,
    });

    this.translate.get(['LABEL.DELETE']).subscribe(trans => {
      this.removeNodeButton.innerHTML = trans['LABEL.DELETE'];
      this.removeNodeButton.setAttribute('remove-task-node', 'true');
      this.removeNodeButton.className = 'dialogsButtonClass clearButton';
      this.removeNodeButton.addEventListener('click', this.removeTaskNode, true);

      this.removeEdgeButton.innerHTML = trans['LABEL.DELETE'];
      this.removeEdgeButton.setAttribute('remove-task-edge', 'true');
      this.removeEdgeButton.className = 'dialogsButtonClass clearButton';
    });
  }

  // 드래그하여 패널 width 조절
  @HostListener('document:mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (!this.grabber) {
      return;
    }
    this.resizer(event.clientX - this.x);
    this.x = event.clientX;
  }

  @HostListener('document:mouseup', ['$event'])
  onMouseUp(event: MouseEvent) {
    this.grabber = false;
  }

  resizer(offsetX: number) {
    this.width += offsetX;
  }

  @HostListener('document:mousedown', ['$event'])
  onMouseDown(event: MouseEvent) {
    if (event.clientX > (this.width + 36) && event.clientX < (this.width + 76)) {
      this.grabber = true;
      this.x = event.clientX;
    }
  }

  @HostListener('document:mouseover', ['$event'])
  onMouseOver(event: MouseEvent) {
    if (event.clientX > (this.width + 36) && event.clientX < (this.width + 76)) {
      this.cursor = 'ew-resize';
    } else {
      this.cursor = 'default';
    }
  }

  getSystemCode(): Promise <boolean> {
    return new Promise ((resolve => {
      this.dialogDesignService.getSystemCode().subscribe(res => {
          if (res) {
            console.log('systemCode: ', res);
            this.systemCodeList = res;
            this.defaultSkillComponent.systemCodeList = this.systemCodeList;
            this.baseEditableNodeComponent.setNodeCodeList(this.systemCodeList['nodeCode']);
            // this.baseEditableNodeComponent.nodeCodeList = this.systemCodeList['nodeCode'];
          }
          resolve(true);
        },
        err => {
          resolve(false);
        }
      );
    }));
  }

  getToolkit(): jsPlumbToolkit {
    return this.toolkit;
  }

  getNativeElement(component: any) {
    return (component.nativeElement || component._nativeElement || component.location.nativeElement).childNodes[1];
  }

  panMode() {
    console.log('panMode');
    this.surface.setMode('pan');
    this.modeChange();
  }

  selectMode() {
    console.log('selectMode');
    this.surface.setMode('select');
    this.modeChange();
  }

  zoomToFit() {
    this.surface.getToolkit().clearSelection();
    this.surface.zoomToFit();
  }

  modeChange() {
    this.surface.bind('modeChanged', (mode: String) => {
      const controls = this.getNativeElement(this.elementRef);
      jsPlumb.removeClass(controls.querySelectorAll('[mode]'), 'selected-mode');
      jsPlumb.addClass(controls.querySelectorAll('[mode=' + mode + ']'),
        'selected-mode');
    });
  }

  attrTypeChange(e, type, idx?) {
    console.log('selected type change: ', e, idx);
    const tempValue = Object.assign({}, e);

    if (type === 'node') { // node
      this.currentNodeData.attr[idx].type = tempValue.value;
      this.checkNodeType(this.currentNodeData);
    } else if (type === 'edge') { // edge
      this.currentEdgeData.data.attr[idx].type = tempValue.value;
    } else if (type === 'skill') { // skill
      this.selectedSkillInfo[idx] = tempValue.value;
    }
  }

  checkNodeType(nodeData: JSON) {
    let changedType = '';
    let systemCodeValue = '';

    for (let attr of nodeData['attr']) {
      systemCodeValue = this.defaultSkillComponent.changeIdToName(this.systemCodeList['nodeCode'], attr['type']);
      console.log('systemCodeValue: ', systemCodeValue);
      if (systemCodeValue === 'Transition') {
        changedType = 'goto';
      }
    }

    if (changedType !== '') {
      nodeData['type'] = changedType;
    } else if (nodeData['type'] !== 'start' && nodeData['type'] !== 'end') {
      nodeData['type'] = 'task';
    }

    let tmpData = this.toolkit.exportData();
    this.reloadToolkit(tmpData);
  }

  toggleSelectionNode(node: any) {
    console.log('toggleSelectionNode..', node);
    this.currentEdgeData = '';
    this.toolkit.clearSelection();
    this.toolkit.toggleSelection(node);
    this.selectType = node.objectType;
    this.currentNodeData = node.data;
    this.showEditSpace = true;
  }

  selectionEdge(edge: any) {
    console.log(edge);
    this.currentNodeData = '';
    this.toolkit.clearSelection();
    this.selectType = edge.objectType;
    this.currentEdgeData = edge;
    this.showEditSpace = true;
  }

  editEdge(edge: any, action: string) {
    this.currentNodeData = '';
    this.translate.get(['TITLE.EDGE_MODIFY', 'TITLE.EDGE_ADD', 'TITLE.EDGE_MODIFY',
      'LABEL.OK', 'LABEL.CANCEL',
      'MSG.ALERT_IMPORT_NODE_UPDATE',
      'VALUE.DEFAULT_EDGE_LABEL']).subscribe(trans => {

      const title = action === 'U' ? trans['TITLE.EDGE_MODIFY'] : trans['TITLE.EDGE_ADD'];
      this.removeEdgeButton.addEventListener('click', () => {this.removeEdge(edge)}, true);

      let okButton = document.createElement('button');
      okButton.innerHTML = trans['LABEL.OK'];
      okButton.setAttribute('jtk-commit', 'true');
      okButton.className = 'dialogsButtonClass';

      let cancelButton = document.createElement('button');
      cancelButton.innerHTML = trans['LABEL.CANCEL'];
      cancelButton.setAttribute('jtk-cancel', 'true');
      cancelButton.className = 'dialogsButtonClass';

      if (action === 'U' && edge.data.skillId) {
        Dialogs.show({
          id: 'dlgMessage',
          title: title,
          data: {
            msg: trans['MSG.ALERT_IMPORT_EDGE_UPDATE'],
          },
          buttons: [okButton]
        });
      } else {
        if (action === 'U') {
          Dialogs.show({
            id: 'dlgRadio',
            title: title,
            data: {
              radio: edge.data.type ? edge.data.type : this.edgeTypeList[0]['value'],
              text: edge.data.label ? edge.data.label : '',
            },
            buttons: [okButton, cancelButton, this.removeEdgeButton],
            onOK: (data: any) => {
              data.text.trim();
              if (data.text.length > 0) {
                data.text = data.text.length <= 30 ? data.text : data.text.substr(0, 30);
              } else {
                data.text = trans['VALUE.DEFAULT_EDGE_LABEL'];
              }

              let preType = edge['data']['type'];
              this.toolkit.updateEdge(edge, {label: data.text, type: data.radio});
              if (preType !== edge['data']['type']) {
                // edge의 이전 type과 data.radio로 업데이트된 타입을 비교하여 다르면 reload
                let tmpData = this.toolkit.exportData();
                this.reloadToolkit(tmpData);
              }
            }
          });
        } else if (action === 'A') {
          Dialogs.show({
            id: 'dlgRadio',
            title: title,
            data: {
              radio: this.edgeTypeList[0]['value'],
              text: '',
            },
            onOK: (data: any) => {
              data.text.trim();
              if (data.text.length > 0) {
                data.text = data.text.length <= 30 ? data.text : data.text.substr(0, 30);
              } else {
                data.text = trans['VALUE.DEFAULT_EDGE_LABEL'];
              }

              this.toolkit.updateEdge(edge,
                {
                  label: data.text,
                  attr: [{text: '', type: ''}],
                  desc: '',
                  status: '',
                  type: data.radio
                });
              this.toolkit.setSelection(edge);
            },
            onCancel: (data: any) => {
              this.toolkit.updateEdge(edge,
                {label: trans['VALUE.DEFAULT_EDGE_LABEL'], attr: [{text: '', type: ''}], desc: '', status: '', type: this.edgeTypeList[0]['value']});
              this.toolkit.setSelection(edge);
              console.log('onCancel...', data);
            }
          });
        }
      }
    });
  }

  removeEdge(edge: any) {
    this.translate.get(['TITLE.EDGE_DELETE', 'MSG.ALERT_IMPORT_EDGE_DELETE', 'MSG.CONFIRM_DELETE_EDGE'], {label : edge.data.label}).subscribe(trans => {

      if (edge.data.skillId) {
        Dialogs.show({
          id: 'dlgMessage',
          title: trans['TITLE.EDGE_DELETE'],
          data: {
            msg: trans['MSG.ALERT_IMPORT_EDGE_DELETE'],
          },
        });
      } else {
        Dialogs.show({
          id: 'dlgConfirm',
          title: trans['TITLE.EDGE_DELETE'],
          data: {
            msg: trans['MSG.CONFIRM_DELETE_EDGE'],
          },
          onOK: () => {
            this.toolkit.removeEdge(edge);
          },
        });
      }
    });
  }

  appendDefaultNode(metaYn: string) {
    if (this.selectedSkillInfo) {
      if (metaYn === 'N') {
        this.toolkit.addNode({
          id: jsPlumbUtil.uuid(),
          type: 'start',
          text: '시작',
          left: 50,
          top: 50,
          w: 100,
          h: 70,
          attr: [{type: '', text: ''}],
          desc: '',
        });

        this.toolkit.addNode({
          id: jsPlumbUtil.uuid(),
          type: 'end',
          text: '종료',
          left: 900,
          top: 50,
          w: 100,
          h: 70,
          attr: [{type: '', text: ''}],
          desc: '',
        });
        this.surface.zoomToFit({fill: 0.9, padding: 10});
      } else if (metaYn === 'Y') {
        this.toolkit.addNode({
          id: jsPlumbUtil.uuid(),
          type: 'task',
          text: 'node1',
          left: 550,
          top: 50,
          w: 100,
          h: 70,
          attr: [{type: '', text: ''}],
          desc: '',
          status: '',
        });
        this.surface.zoomToFit({fill: 0.9, padding: 10});
      }
    }
  }

  appendTaskNode = (action?: string, node?: any) => {
    this.translate.get(['TITLE.NODE_MODIFY', 'TITLE.NODE_ADD', 'TITLE.NODE_MODIFY',
      'LABEL.OK', 'LABEL.CANCEL',
      'MSG.ALERT_START_NODE_UPDATE', 'MSG.ALERT_END_NODE_UPDATE', 'MSG.ALERT_IMPORT_NODE_UPDATE', 'MSG.VALIDATE_NODE_LENGTH']).subscribe(trans => {

      const title = action === 'U' ? trans['TITLE.NODE_MODIFY'] : trans['TITLE.NODE_ADD'];
      let msg = '';

      let okButton = document.createElement('button');
      okButton.innerHTML = trans['LABEL.OK'];
      okButton.setAttribute('jtk-commit', 'true');
      okButton.className = 'dialogsButtonClass';

      let cancelButton = document.createElement('button');
      cancelButton.innerHTML = trans['LABEL.CANCEL'];
      cancelButton.setAttribute('jtk-cancel', 'true');
      cancelButton.className = 'dialogsButtonClass';

      if (node) {
        if (node.data.type === 'start') {
          msg = trans['MSG.ALERT_START_NODE_UPDATE'];
        } else if (node.data.type === 'end') {
          msg = trans['MSG.ALERT_END_NODE_UPDATE'];
        } else if (node.data.skillId) {
          msg = trans['MSG.ALERT_IMPORT_NODE_UPDATE'];
        }
      }

      if (action === 'U') {
        if (msg) {
          Dialogs.show({
            id: 'dlgMessage',
            title: title,
            data: {
              msg: msg,
            },
            buttons: [okButton, this.removeNodeButton],
          });
        } else {
          Dialogs.show({
            id: 'dlgText',
            title: title,
            data: {
              text: node.data.text ? node.data.text : '',
            },
            buttons: [okButton, cancelButton, this.removeNodeButton],
            onOK: (data: any) => {

              if (data.text && data.text.length > 2 && data.text.length < 30) {
                if (data.text.length > 6) {
                  this.toolkit.updateNode(node, {w: (data.text.length * 17), text: data.text});
                } else {
                  this.toolkit.updateNode(node, {w: 100, text: data.text});
                }
              } else {
                Dialogs.show({
                  id: 'dlgMessage',
                  title: title,
                  data: {
                    msg: trans['MSG.VALIDATE_NODE_LENGTH'],
                  },
                });
              }
            }
          });
        }
      } else {
        Dialogs.show({
          id: 'dlgText',
          data: '',
          title: title,
          onOK: (data: any) => {
            if (data.text && data.text.length > 2 && data.text.length < 30) {
              const newNode = this.toolkit.addNode({
                id: jsPlumbUtil.uuid(),
                type: 'task',
                text: data.text,
                left: 300,
                top: 50,
                w: 100,
                h: 70,
                attr: [{type: '', text: ''}],
                desc: '',
                status: ''
              });
              this.toggleSelectionNode(newNode);
            } else {
              Dialogs.show({
                id: 'dlgMessage',
                title: title,
                data: {
                  msg: trans['MSG.VALIDATE_NODE_LENGTH'],
                },
              });
            }
          }
        });
      }
    });
  };

  removeTaskNode = () => {
    const selections = this.toolkit.getSelection().getNodes();

    if (selections.length === 1) {
      const node = selections[0];
      this.translate.get(['TITLE.NODE_DELETE', 'MSG.CONFIRM_DELETE_EDGE', 'MSG.CONFIRM_IMPORT_SKILL_DELETE'], {label: node.data.text}).subscribe(trans => {

        if (!node.data['skillId']) {
          Dialogs.show({
            id: 'dlgConfirm',
            title: trans['TITLE.NODE_DELETE'],
            data: {
              msg: trans['MSG.CONFIRM_DELETE_EDGE'],
            },
            onOK: () => {
              this.toolkit.removeNode(node);
            },
          });
        } else if (node.data['skillId']) {
          Dialogs.show({
            id: 'dlgConfirm',
            title: trans['TITLE.NODE_DELETE'],
            data: {
              msg: trans['MSG.CONFIRM_IMPORT_SKILL_DELETE'],
            },
            onOK: () => {
              let jsonData = this.selectedSkillInfo['jsonData'];
              let stringToJson = JSON.parse(jsonData);
              this.removeNode(node.data['skillId'], stringToJson);
              this.toolkit.removeNode(node);
            },
          });
        }
      });
    } else {
      this.translate.get(['TITLE.NODE_DELETE', 'MSG.ALERT_CHECK_SELECTED_NODE']).subscribe(trans => {

        Dialogs.show({
          id: 'dlgMessage',
          title: trans['TITLE.NODE_DELETE'],
          data: {
            msg: trans['MSG.ALERT_CHECK_SELECTED_NODE'],
          },
        });
      });
    }
  };

  removeNode(skillId, jsonData) {
    jsonData.nodes.forEach(data => {
      if (skillId === data['skillId']) {
        this.toolkit.removeNode(data);
      }
    });

    let importSkills = this.selectedSkillInfo['importSkills'];
    this.selectedSkillInfo['importSkills'] = importSkills.replace(skillId, '');
  }

  invisibleEditSpace() {
    console.log(this.showEditSpace);
    this.showEditSpace = false;
    console.log(this.showEditSpace);
  }

  appendNodeData() {
    this.currentNodeData.attr.push({type: '', text: ''});
    // this.cdr.detectChanges();
  }

  appendEdgeData() {
    this.currentEdgeData.data.attr.push({text: ''});
  }

  removeNodeData(index: number) {
    this.currentNodeData.attr.splice(index, 1);
  }

  removeEdgeData(index: number) {
    this.currentEdgeData.data.attr.splice(index, 1);
  }

  changeNodeTextarea($event) {
    this.currentNodeData.desc = $event.target.value ? $event.target.value : '';
  }

  changeEdgeTextarea($event) {
    this.currentEdgeData.data.desc = $event.target.value ?
      $event.target.value :
      '';
  }

  exportJson = () => {
    const json = JSON.stringify(this.toolkit.exportData(), null, 2);
    this.downloadService.downloadJsonFile(json, 'design');
    /* todo
     개발되어야함
    */
  };

  /*
   * commit할 때, 모든 node의 go to skill 체크하여 next skill list 변경
   */
  checkNextSkills(json): Promise<Boolean> {
    return new Promise((resolve) => {
      let jsonDataToJson = JSON.parse(json);
      let nodes = jsonDataToJson.nodes;
      let nextSkills: string[] = [];
      let gotoCodeId = this.defaultSkillComponent.changeNameToId(this.systemCodeList['nodeCode'], 'Transition');

      nodes.forEach(function (node) {
        let attrs = node.attr;
        attrs.forEach(function (attr) {
          if (gotoCodeId) {
            if (attr.type === gotoCodeId) {
              if (!nextSkills.includes(attr.text)) {
                nextSkills.push(attr.text);
              }
            }
          }
        });
      });
      this.selectedSkillInfo['nextSkills'] = nextSkills.toString();
      resolve();
    });
  }

  commit = () => {
    this.param = new DialogDesignEntity();
    this.param.workspaceId = this.workspaceId;
    this.param.updaterId = this.userId;
    if (this.userId) {
      const chartJson = JSON.stringify(this.toolkit.exportData(), null, 2);
      let json = JSON.parse(chartJson);

      this.checkNextSkills(chartJson).then(() => {
        for (let col in this.selectedSkillInfo) {
          if (col) {
            this.param[col] = this.selectedSkillInfo[col];
            if (col !== 'jsonData') {
              json[col] = this.selectedSkillInfo[col];
            }
          }
        }
        this.param['jsonData'] = JSON.stringify(json, null, 2);

        console.log('commit param: ', this.param);
        this.defaultSkillComponent.updateSkill(this.param);
      });
    } else {
      let title = 'TITLE.ERROR';
      let message = 'ERROR_NOT_FOUND_ID';
      this.translate.get([title, message]).subscribe(trans => {
        let ref = this.dialog.open(AlertComponent);
        ref.componentInstance.header = 'error';
        ref.componentInstance.title = trans[title];
        ref.componentInstance.message = trans[message];
      });
    }
  };

  loadDesignFile = ($event) => {
    if (!!$event.target.files[0]) {
      const targetFile = $event.target.files[0];
      let fr = new FileReader();
      fr.onloadend = (ev: ProgressEvent) => {
        this.designLoader.nativeElement.value = '';
        try {
          const target = (ev.target as FileReader);
          const design = JSON.parse(target.result as string);
          this.reloadToolkit(design);
          this.selectType = undefined;
        } catch (e) {
          console.log(e);
          Dialogs.show({
            id: 'dlgMessage',
            title: 'Error',
            data: {
              msg: getErrorString(e, `Failed to load the file '${targetFile.name}'.`),
            },
          });
        }
      };
      fr.onerror = () => {
        this.designLoader.nativeElement.value = '';
        Dialogs.show({
          id: 'dlgMessage',
          title: 'Error',
          data: {msg: `Failed to load the file '${targetFile.name}'.`}
        });
      };
      fr.readAsText(targetFile);
    } else {
      console.log('file is not selected.');
    }
  };

  getApiParam() {
    const exportData = this.toolkit.exportData();
    const returnParam = {};

    returnParam['name'] = this.scenarioName;
    returnParam['projectName'] = this.korTypeToEng.transform(this.scenarioName);
    returnParam['lang'] = 1;
    returnParam['description'] = this.scenarioName;
    returnParam['nodes'] = [];
    returnParam['edges'] = [];

    exportData['nodes'].forEach(function (node) {
      returnParam['nodes'].push({
        id: node.id,
        type: node.type,
        text: node.text,
        desc: node.desc,
        attr: node.attr ? node.attr : [],
        position: {
          left: node.left,
          top: node.top,
          w: node.w,
          h: node.h,
        },
      });
    });

    exportData['edges'].forEach(function (edge) {
      returnParam['edges'].push({
        source: edge.source,
        target: edge.target,
        data: {
          id: edge.data.id,
          type: edge.data.type,
          label: edge.data.label,
          desc: edge.data.desc,
          attr: edge.data.attr ? edge.data.attr : [],
        },
      });
    });

    console.log(returnParam);
    return returnParam;
  }

  showInfoPage() {
    this.currentNodeData = '';
    this.selectType = undefined;
    this.showEditSpace = true;
  }

  reloadToolkit(loadData?) {
    this.toolkit.clear();
    this.toolkit.load({data: loadData});
    this.surface.zoomToFit({fill: 0.9, padding: 10});
  }

  getSkillInfo(skillInfoJson?: string) {
    this.selectedSkillInfo = [];
    this.selectedSkillName = '';
    this.topicSentence = '';

    if (skillInfoJson) {
      this.showEditSpace = true;
      this.selectedSkillInfo = JSON.parse(skillInfoJson);
      this.selectedSkillName = this.selectedSkillInfo['skillName'];
      this.topicSentence = this.selectedSkillInfo['topicSentence'];

      if (this.selectedSkillInfo['nodes']) {
        this.designLoader.nativeElement.value = '';
        this.reloadToolkit(this.selectedSkillInfo);
        this.selectType = undefined;
      } else {
        this.toolkit.clear();
        this.surface = this.surfaceComponent.surface;
        const drawingTools = new DrawingTools({
          renderer: this.surface,
        });
        setTimeout(() => {
          this.appendDefaultNode(this.selectedSkillInfo['metaYn']);
          // this.surface.zoomToFit({fill: 0.9, padding: 10});
        }, 0);
      }
    } else {
      this.toolkit.clear();
      this.selectedSkillInfo = [];
      this.selectedSkillName = '';
      this.topicSentence = '';
      this.showEditSpace = false;
    }
  }

  /*
   * import하는 메타 스킬의 노드, 엣지에 import(flag로 사용), id(uuid) 추가해서 import
   */
  importSkill(metaInfo?: string[]) {
    console.log('metaInfo: ', metaInfo);
    const metaJson = JSON.parse(metaInfo['jsonData']);

    for (let nodeIdx in metaJson['nodes']) {
      if (nodeIdx) {
        metaJson['nodes'][nodeIdx]['skillId'] = metaInfo['id']
      }
    }
    this.toolkit.addNodes(metaJson['nodes']);

    for (let edge of metaJson['edges']) {
      if (edge) {
        edge['data']['skillId'] = metaInfo['id'];
        this.toolkit.addEdge(edge);
      }
    }

    if (this.selectedSkillInfo['importSkills']) {
      let list = this.selectedSkillInfo['importSkills'];
      if (!list.toString().includes(metaInfo['id'])) {
        this.selectedSkillInfo['importSkills'] += ',' + metaInfo['id'];
      }
    } else {
      this.selectedSkillInfo['importSkills'] = metaInfo['id'];
    }
  }
}
