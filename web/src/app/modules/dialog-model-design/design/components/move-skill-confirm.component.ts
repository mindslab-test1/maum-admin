import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './move-skill-confirm.component.html',
  styleUrls: ['./move-skill-confirm.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MoveSkillConfirmComponent implements OnInit {

  @Input() title: string = null;
  @Input() message: string = null;

  isSave = false;

  constructor(private translate: TranslateService,
              public dialogRef: MatDialogRef<any>) {
    // 현재 사용 언어 설정
    translate.use('ko');
  }

  ngOnInit() {
  }

}
