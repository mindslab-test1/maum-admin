import {Component, Inject, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertComponent} from '../../../../shared';
import {DialogDesignService} from '../services/dialog-design.service';
import {DialogDesignEntity} from '../entity/dialog-design.entity';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-dialog-design-add-skill-upsert',
  templateUrl: './dialog-design-add-skill-upsert.component.html',
  styleUrls: ['./dialog-design-add-skill-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DialogDesignAddSkillUpsertComponent implements OnInit {

  param = new DialogDesignEntity();

  skillForm;
  data: any;
  meta: any;
  next: any;
  service: any;
  newSkillName: string;

  selectedMetaYn = 'N';
  linkYn = 'N';
  group1 = '18';
  workspaceId: string;
  userId: string;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              public dialogRef: MatDialogRef<any>,
              private translate: TranslateService,
              private dialogDesignService: DialogDesignService,
              private fb: FormBuilder,
              public dialog: MatDialog,
              public  datepipe: DatePipe,
              @Inject(MAT_DIALOG_DATA) public skillData: Object) {

    this.skillForm = this.fb.group({
      name: ['', [
        Validators.required
      ]],
    });
  }

  ngOnInit() {
    this.next = this.dialogRef.componentInstance.next;
    let data = this.data = this.dialogRef.componentInstance.data;

    // apply data when update
    if (data) {
      this.skillForm.setValue({
        name: data.name
      });
    }

    this.skillForm = new FormGroup({
      name: new FormControl()
    });

    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.userId = this.storage.get('user').id;
  }

  radioChange(e) {
    this.selectedMetaYn = e.value;
  }

  submit() {
    if (this.newSkillName) {
      console.log('newSkillName: ', this.newSkillName);
    } else {
      // this.newSkillName = 'New_Skill_' + Date.now();
      this.newSkillName = 'NewSkill(' + this.datepipe.transform(Date.now(), 'yyyy-MM-dd HH:mm:ss') + ')';
      console.log('newSkillName: ', this.newSkillName);
    }
    this.param.workspaceId = this.workspaceId;
    this.param.group1 = this.group1;
    this.param.skillName = this.newSkillName;
    this.param.metaYn = this.selectedMetaYn;
    this.param.linkYn = this.linkYn;
    this.param.updaterId = this.userId;
    this.param.creatorId = this.userId;
    this.param.jsonData = JSON.stringify(this.param, null, 2);
    this.createNewSkill();
  }

  createNewSkill() {
    this.dialogDesignService.updateSkill(this.param).subscribe(res => {
        if (res) {
          console.log('success create new skill!', res);
          this.dialogRef.close(res);
        }
      },
      err => {
        // console.log('error msg: ', err.error.error);
        this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_ADD_SKILL', 'error');
      }
    );
  }

  openAlertDialog(title, message, status) {
    this.translate.get([title, message]).subscribe(trans => {
      let ref = this.dialog.open(AlertComponent);
      ref.componentInstance.header = status;
      ref.componentInstance.title = trans[title];
      ref.componentInstance.message = trans[message];
    });
  }
}
