import {Dialogs, Node} from 'jsplumbtoolkit';
import {BaseNodeComponent} from 'jsplumbtoolkit-angular';
import {Component} from '@angular/core';

function isNode(obj: any): obj is Node {
  return obj.objectType === 'Node';
}

function colorInit(name) {
  let res = '#2978F5';
  console.log('colorInit nodeCodeList: ', nodeCode);
  if (nodeCode) {
    nodeCode.forEach(code => {
      if (code['description']) {
        if (code['name'] === name) {
          let description = JSON.parse(code['description']);
          res = description['color'];
        }
      }
    });
  }
  return res;
}

let nodeCode: any;
let nodeType: string;
// /**
//  * This is the base class for editable nodes in this demo. It extends `BaseNodeComponent`
//  */
export class BaseEditableNodeComponent extends BaseNodeComponent {

  setNodeCodeList(nodeCodeList) {
    console.log('setNodeCodeList nodeCodeList: ', nodeCodeList);
    nodeCode = nodeCodeList;
  }

  setNodeType(nt) {
    if (nt) {
      nodeType = nt;
    }
  }

  removeNode() {
    console.log('removeNode...');
    const obj = this.getNode();
    if (obj != null) {
      if (isNode(obj)) {
        Dialogs.show({
          id: 'dlgConfirm',
          title: '노드 삭제',
          data: {
            msg: '[' + obj.data.text + ']를 삭제 하시겠습니까?'
          },
          onOK: () => {
            this.toolkit.removeNode(<Node>obj);
          }
        });
      }
    }
  }

  editNode() {
    console.log('editNode...');
    const obj = this.getNode();
    Dialogs.show({
      id: 'dlgText',
      data: obj.data,
      title: '노드 타이틀 수정',
      onOK: (data: any) => {
        if (data.text && data.text.length > 2) {
          // if name is at least 2 chars long, update the underlying data and
          // update the UI.
          this.toolkit.updateNode(obj, data);
        }
      }
    });
  }

}


// ----------------- start node -------------------------------
@Component({
  templateUrl: 'template/start.html',
  styleUrls: ['./dialog-designs-design.component.scss']
})
export class StartNodeComponent extends BaseEditableNodeComponent {
}

// ----------------- end node -------------------------------
@Component({
  templateUrl: 'template/end.html',
  styleUrls: ['./dialog-designs-design.component.scss']
})
export class EndNodeComponent extends BaseEditableNodeComponent {
}

// ----------------- from node -------------------------------
@Component({
  templateUrl: 'template/from.html',
  styleUrls: ['./dialog-designs-design.component.scss']
})
export class FromNodeComponent extends BaseEditableNodeComponent {
}

// ----------------- to node -------------------------------
@Component({
  templateUrl: 'template/to.html',
  styleUrls: ['./dialog-designs-design.component.scss']
})
export class ToNodeComponent extends BaseEditableNodeComponent {
}


// ----------------- task node -------------------------------
@Component({
  templateUrl: 'template/task.html',
  styleUrls: ['./dialog-designs-design.component.scss']
})
export class TaskNodeComponent extends BaseEditableNodeComponent {
}

// ----------------- goto node -------------------------------
@Component({
  templateUrl: 'template/goto.html',
  styleUrls: ['./dialog-designs-design.component.scss']
})
export class GotoNodeComponent extends BaseEditableNodeComponent {
  // gotoColor = '#2978F5';
  // gotoColor = colorInit(nodeType);
}

