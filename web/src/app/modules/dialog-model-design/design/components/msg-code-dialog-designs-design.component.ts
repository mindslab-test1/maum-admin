import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Store} from '@ngrx/store';
import {Dialogs, DrawingTools, jsPlumb, jsPlumbToolkit, jsPlumbUtil, Surface} from 'jsplumbtoolkit';
import {
  jsPlumbService,
  jsPlumbSurfaceComponent
} from 'jsplumbtoolkit-angular';

import {
  BaseEditableNodeComponent,
  StartNodeComponent,
  EndNodeComponent,
  FromNodeComponent,
  ToNodeComponent,
  TaskNodeComponent,
  GotoNodeComponent,
} from './components';
import {ROUTER_LOADED} from '../../../../core/actions';
import {
  AlertComponent, ConfirmComponent,
  DownloadService,
  getErrorString,
  KorTypeToEngPipe,
} from '../../../../shared';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {DialogDesignEntity} from '../entity/dialog-design.entity';
import {DefaultSkillListComponent} from './default-skill-list.component';
import {DialogDesignService} from '../services/dialog-design.service';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {WebEditorComponent} from '../../../../shared/components/web-editor/web-editor.component';
import {deepStrictEqual} from 'assert';
import {MoveSkillConfirmComponent} from './move-skill-confirm.component';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-d-scenario-outline',
  templateUrl: './msg-code-dialog-designs-design.component.html',
  styleUrls: ['./dialog-designs-design.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MsgCodeDialogDesignsDesignComponent {

  @ViewChild('designLoader') designLoader: ElementRef;
  @ViewChild(jsPlumbSurfaceComponent) surfaceComponent: jsPlumbSurfaceComponent;
  @ViewChild('upsertDialogName') upsertDialogName: ElementRef;
  @ViewChild('patternInput') patternInput: ElementRef;
  @ViewChild(DefaultSkillListComponent) defaultSkillComponent: DefaultSkillListComponent;

  korTypeToEng;

  actions: any[] = [];
  save: any;
  scenarioName: String;
  uploadId: string;
  isSimulation = false;
  changeFlag = false;

  toolkit: jsPlumbToolkit;
  toolkitId: string;
  toolkitParams = {};

  surface: Surface;
  surfaceId: string;

  nodeNames = ['start', 'task', 'end', 'sds', 'goto'];
  nodeTypes: any[] = [];

  tableTopic: any = [];
  systemCodeList: string[] = [];
  endList: string[] = ['최종텍스트', '최종전산', '최종업무'];
  gotoList: string[] = ['Go to SKILL', 'Go to BQA', 'Go to STACK'];
  edgeTypeList = [
    {'value': 'text', 'label': 'Text', 'color': 'rgb(153, 115, 235)'},
    {'value': 'button', 'label': 'Button', 'color': 'rgb(123, 212, 189)'},
    {'value': 'transaction', 'label': 'Transaction', 'color': 'rgb(245, 135, 91)'}
  ];
  baseEditableNodeComponent = new BaseEditableNodeComponent();
  orderProperty: string;
  orderDirection: string;

  param = new DialogDesignEntity();

  workspaceId: string;
  userId: string;
  selectedSkillInfo = [];
  initialSkillInfo = [];
  selectedSkillName: string;
  topicSentence: string;
  showEditSpace = false;
  sideOpened = 'block';

  cursor = 'default';
  width = 410;
  x = 0;
  grabber = false;
  // 추후 선 모양 선택할 수 있도록 구현할 때 활용 예정
  lineShape: string[] = ['Straight', 'Bezier', 'Flowchart', 'State Machine'];

  selectedCommit =
    {hashKey: undefined, commitTime: undefined, updaterId: undefined, updaterEmail: undefined};
  gitParam = {};
  commitList = [];
  currentJsonData: JSON;
  isOldVersion = false;

  msgCdList = [];
  COMMON_MSG_CODE = '999999';
  HAI_CHNL_DV_CD = 'ZZZ';
  WEB_EDITOR_TITLE_1 = '상세메세지';
  WEB_EDITOR_TITLE_2 = '상세메세지(HTML)';
  STATUS_CODE_IT_COMPLETE = 'IT적용완료';
  USE_YN = 'Y';
  scenario = [];

  removeNodeButton = document.createElement('button');
  removeEdgeButton = document.createElement('button');

  view = {
    nodes: {
      selectable: {
        events: {
          dblclick: (params: any) => {
            console.log('task-node double-clicked', params);
            this.appendTaskNode('U', params.node);
          },
          mousedown: (params: any) => {
            console.log('task-node mousedown', params);
            if (params['node']['data'] !== this.currentNodeData) {
              this.toggleSelectionNode(params.node);
            }
            this.showEditSpace = true;
          },
        },
      },
      'start': {
        parent: 'selectable',
        component: StartNodeComponent,
      },
      'end': {
        parent: 'selectable',
        component: EndNodeComponent,
      },
      'from': {
        // parent: 'selectable',
        component: FromNodeComponent,
        events: {
          click: (params: any) => {
            console.log('from-node clicked', params);
            this.toggleSelectionNode(params.node);
          },
          dblclick: (params: any) => {
            console.log('from-node double-clicked', params);
            this.designLoader.nativeElement.click();
          },
        },
      },
      'to': {
        // parent: 'selectable',
        component: ToNodeComponent,
        events: {
          dblclick: (params: any) => {
            console.log('to-node double-clicked', params);
            this.designLoader.nativeElement.click();
          },
          click: (params: any) => {
            console.log('to-node clicked', params);
            this.toggleSelectionNode(params.node);
          },
        },
      },
      'task': {
        parent: 'selectable',
        component: TaskNodeComponent,
      },
      'goto': {
        parent: 'selectable',
        component: GotoNodeComponent,
      },
    },
    edges: {
      'default': {
        endpoint: 'Blank',
        anchor: [
          [ 'Perimeter', { shape: 'Rectangle' } ],
          [ 'Perimeter', { shape: 'Ellipse' } ]
        ], // 'AutoDefault',
        connector: ['Straight'],
        // connector: ['Bezier', {curviness: 70}],
        paintStyle: {
          strokeWidth: 3,
          stroke: 'rgb(132, 172, 179)',
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'}, // hover paint style for this edge type.
        overlays: [
          ['Arrow', {location: 1, width: 11, length: 11}],
        ],
        events: {
          click: (params: any) => {
            if (params['edge']['data'] !== this.currentEdgeData) {
              console.log('process edge click', params);
              this.selectionEdge(params.edge);
            }
          }
        },
      },
      'connection': {
        parent: 'default',
        overlays: [
          [
            'Label', {
            label: '${label}',
            cssClass: 'aLabel',
            events: {
              dblclick: (params: any) => {
                console.log('connection edge click', params);
                this.editEdge(params.edge, 'U');
              },
            },
          },
          ],
        ],
      },
      'text': {
        parent: 'connection',
        paintStyle: {
          strokeWidth: 3,
          stroke: this.edgeTypeList[0]['color'],
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'}, // hover paint style for this edge type.
        overlays: [
          ['Arrow', {location: 1, width: 11, length: 11}],
        ],
      },
      'button': {
        parent: 'connection',
        paintStyle: {
          strokeWidth: 3,
          stroke: this.edgeTypeList[1]['color'],
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'}, // hover paint style for this edge type.
        overlays: [
          ['Arrow', {location: 1, width: 11, length: 11}],
        ],
      },
      'transaction': {
        parent: 'connection',
        paintStyle: {
          strokeWidth: 3,
          stroke: this.edgeTypeList[2]['color'],
          outlineWidth: 0,
          outlineStroke: 'transparent',
        },
        hoverPaintStyle: {strokeWidth: 4, stroke: 'rgb(67,67,67)'}, // hover paint style for this edge type.
        overlays: [
          ['Arrow', {location: 1, width: 11, length: 11}],
        ],
      }
    },
    ports: {
      'start': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'end': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'from': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'to': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'task': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
      'goto': {
        endpoint: 'Blank',
        anchor: 'AutoDefault',
        paintStyle: {fill: '#84acb3'},
        maxConnections: -1,
        edgeType: 'connection',
      },
    },
    states: {
      'mintyGreenState': {
        '*': {
          cssClass: 'mintyGreen'
        }
      }
    }
  };

  renderParams = {
    layout: {
      type: 'Spring',
    },
    events: {
      canvasClick: (params: any) => {
        if (this.currentNodeData) {
          console.log('canvasClick', params);
          this.currentNodeData = '';
          this.showEditSpace = false;
          this.toolkit.clearSelection();
          // this.surface.zoomToFit({fill: 0.9, padding: 10});
        }
      },
      edgeAdded: (params: any) => {
        if (params.addedByMouse) {
          console.log('params.addedByMouse', params);
          // this.editLabel(params.edge);
          if (!params.source.data.skillId || !params.target.data.skillId) {
            this.editEdge(params.edge, 'A');
          } else {
            this.toolkit.removeEdge(params.edge.data.id);
            Dialogs.show({
              id: 'dlgMessage',
              title: '엣지 등록',
              data: {
                msg: '임포트된 스킬의 노드간에는 엣지를 생성할 수 없습니다.',
              },
            });
          }
        }
      },
    },
    consumeRightClick: false,
    dragOptions: {
      filter: '.jtk-draw-handle, .node-action, .node-action i',
    },
  };

  selectType: String;
  currentNodeData: any;
  currentEdgeData: any;
  initialNodeData: any;

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private router: Router,
              private cdr: ChangeDetectorRef,
              private store: Store<any>, private $jsplumb: jsPlumbService,
              private translate: TranslateService,
              private dialog: MatDialog,
              private dialogDesignService: DialogDesignService,
              private elementRef: ElementRef,
              private downloadService: DownloadService) {
    // 현재 사용 언어 설정
    translate.use('ko');

    const dialogs = {
      dialogs: {
        dlgRadio: [
          '<form>' +
          ' <input type="radio" name="edge_radio" jtk-focus jtk-att="radio" value="' + this.edgeTypeList[0]['value'] + '" jtk-commit="true"/>' + this.edgeTypeList[0]['label'] +
          ' <input type="radio" name="edge_radio" jtk-focus jtk-att="radio" value="' + this.edgeTypeList[1]['value'] + '" jtk-commit="true"/>' + this.edgeTypeList[1]['label'] +
          ' <input type="radio" name="edge_radio" jtk-focus jtk-att="radio" value="' + this.edgeTypeList[2]['value'] + '" jtk-commit="true"/>' + this.edgeTypeList[2]['label'] +
          '</form>' +
          '<br/><br/>' +
          '<input type="text" size="50" jtk-focus jtk-att="text" value="${text}" jtk-commit="true"/>',
          'Select Type',
          true],
        dlgText: [
          '<input type="text" size="50" jtk-focus jtk-att="text" value="${text}" jtk-commit="true"/>',
          'Enter Text',
          true],
        dlgConfirm: ['${msg}?', 'Please Confirm', true],
        dlgMessage: ['${msg}', 'Message', false],
      },
    };

    Dialogs.initialize(dialogs);
    this.toolkitId = 'flowchart';
    this.surfaceId = 'flowchartSurface';
    this.toolkit = this.$jsplumb.getToolkit(this.toolkitId); // , this.toolkitParams);
  }

  ngOnInit() {
    console.log('ngOnInit toolkitId :: ', this.toolkitId);
    this.getSystemCode().then(() => {
      this.korTypeToEng = new KorTypeToEngPipe();
      this.translate.get(['LABEL.ADD_NODE', 'LABEL.SAVE', 'LABEL.IMPORT', 'LABEL.EXPORT', 'LABEL.SIMULATION', 'LABEL.SKILL_LIST',
        'TABLE.ID', 'TABLE.NAME', 'TABLE.TOPIC', 'TABLE.SUGGEST', 'TABLE.STATUS', 'TABLE.GROUP1', 'TABLE.GROUP2',
        'TABLE.GROUP3', 'TABLE.GROUP4', 'TABLE.STATUS']).subscribe(trans => {
        this.actions = [
          {
            type: 'add',
            text: trans['LABEL.ADD_NODE'],
            icon: 'add_circle_outline',
            callback: this.appendTaskNode,
            hidden: this.isSimulation,
          },
          {
            type: 'load',
            text: trans['LABEL.IMPORT'],
            icon: 'archive',
            callback: this.importJson,
            hidden: this.isSimulation,
          },
          {
            type: 'export',
            text: trans['LABEL.EXPORT'],
            icon: 'unarchive',
            callback: this.exportJson,
            hidden: this.isSimulation,
          },
          {
            type: 'simulation',
            text: trans['LABEL.SIMULATION'],
            icon: 'visibility',
            callback: this.execSimulation,
            hidden: this.isSimulation,
          },
          {
            type: 'skillList',
            text: trans['LABEL.SKILL_LIST'],
            icon: 'archive',
            callback: this.showSkillList,
            hidden: !this.isSimulation,
          },
        ];

        this.save = {
          type: 'save',
          text: trans['LABEL.SAVE'],
          icon: 'archive',
          callback: this.commit,
          hidden: this.isSimulation,
        };

        this.tableTopic = [{attr: 'skillName', label : trans['TABLE.NAME'], type: 'input', data: null},
          {attr: 'skillId', label : trans['TABLE.ID'], type: 'input', data: null},
          {attr: 'topicSentence', label : trans['TABLE.TOPIC'], type: 'input', data: null},
          {attr: 'suggestSkills', label : trans['TABLE.SUGGEST'], type: 'input_identifier', data: null},
          {attr: 'group1', label : trans['TABLE.GROUP1'], type: 'select', data: {options: this.systemCodeList['group1Code']}},
          {attr: 'group2', label : trans['TABLE.GROUP2'], type: 'input', data: null},
          {attr: 'group3', label : trans['TABLE.GROUP3'], type: 'input', data: null},
          {attr: 'group4', label : trans['TABLE.GROUP4'], type: 'input', data: null},
          {attr: 'status', label : trans['TABLE.STATUS'], type: 'select', data: {options: this.systemCodeList['statusCode']}}];
      });
    });

    this.store.dispatch({type: ROUTER_LOADED});
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.userId = this.storage.get('user').id;
    this.storage.remove('scenarioOrderProperty');
    this.storage.remove('scenarioOrderDirection');

    while (this.nodeNames.length) {
      let name = this.nodeNames.reverse().pop();
      this.nodeTypes.push({
        type: name,
        label: name,
        w: 80,
        h: 25,
      });
    }
  }

  ngAfterViewInit() {
    this.surface = this.surfaceComponent.surface;

    const drawingTools = new DrawingTools({
      renderer: this.surface,
    });

    this.translate.get(['LABEL.DELETE']).subscribe(trans => {
      this.removeNodeButton.innerHTML = trans['LABEL.DELETE'];
      this.removeNodeButton.setAttribute('remove-task-node', 'true');
      this.removeNodeButton.className = 'dialogsButtonClass clearButton';
      this.removeNodeButton.addEventListener('click', this.removeTaskNode, true);

      this.removeEdgeButton.innerHTML = trans['LABEL.DELETE'];
      this.removeEdgeButton.setAttribute('remove-task-edge', 'true');
      this.removeEdgeButton.className = 'dialogsButtonClass clearButton';
    });
  }

  getSystemCode(): Promise <boolean> {
    return new Promise ((resolve => {
      this.dialogDesignService.getSystemCode().subscribe(res => {
          if (res) {
            console.log('systemCode: ', res);
            this.systemCodeList = res;
            this.baseEditableNodeComponent.setNodeCodeList(this.systemCodeList['nodeCode']);
          }
          resolve(true);
        },
        err => {
          resolve(false);
        }
      );
    }));
  }

  sidenavChange() {
    if (this.sideOpened === 'none') {
      this.sideOpened = 'block';
      this.width = 410;
    } else if (this.sideOpened === 'block') {
      this.sideOpened = 'none';
      this.width = 0;
    }
  }

  getToolkit(): jsPlumbToolkit {
    return this.toolkit;
  }

  getNativeElement(component: any) {
    return (component.nativeElement || component._nativeElement || component.location.nativeElement).childNodes[1];
  }

  panMode() {
    console.log('panMode');
    this.surface.setMode('pan');
    this.modeChange();
  }

  selectMode() {
    console.log('selectMode');
    this.surface.setMode('select');
    this.modeChange();
  }

  zoomToFit() {
    this.surface.getToolkit().clearSelection();
    this.surface.zoomToFit();
  }

  modeChange() {
    this.surface.bind('modeChanged', (mode: String) => {
      const controls = this.getNativeElement(this.elementRef);
      jsPlumb.removeClass(controls.querySelectorAll('[mode]'), 'selected-mode');
      jsPlumb.addClass(controls.querySelectorAll('[mode=' + mode + ']'),
        'selected-mode');
    });
  }

  attrTypeChange(e, type, idx?) {
    console.log('selected type change: ', e, idx);
    const tempValue = Object.assign({}, e);

    if (type === 'node') { // node
      if (this.initialNodeData.attr !== undefined && this.initialNodeData.attr[idx] !== undefined) {
        if (this.initialNodeData.attr[idx].type !== tempValue.value) {
          this.currentNodeData.attr[idx]['changed'] = true;
        } else {
          this.currentNodeData.attr[idx]['changed'] = false;
        }
      } else {
        this.currentNodeData.attr[idx]['changed'] = true;
      }
      this.currentNodeData.attr[idx].type = tempValue.value;
      this.currentNodeData.attr[idx].dtlsMsgTypeCd = this.idToDtlsMsgType(tempValue.value);
      this.checkNodeType(this.currentNodeData);
    } else if (type === 'edge') { // edge
      this.currentEdgeData.data.attr[idx].type = tempValue.value;
    } else if (type === 'skill') { // skill
      this.selectedSkillInfo[idx] = tempValue.value;
    }
  }

  checkNodeType(nodeData: JSON) {
    let changedType = '';
    let systemCodeValue = '';
    let endYn = '';
    let isEnd = false;

    for (let attr of nodeData['attr']) {
      systemCodeValue = this.changeIdToName(this.systemCodeList['nodeCode'], attr['type']);
      console.log('systemCodeValue: ', systemCodeValue);
      // Go to ** attr 이 있는지 체크
      this.gotoList.forEach(gotoType => {
        if (systemCodeValue === gotoType) {
          changedType = 'goto';
          // this.baseEditableNodeComponent.setNodeType(systemCodeValue);
        }
      });
      // 최종 ** attr 이 있는지 체크
      this.endList.forEach(endType => {
        if (systemCodeValue === endType) {
          isEnd = true;
        }
      });
    }
    this.currentNodeData['isEnd'] = isEnd;

    if (changedType !== '') {
      nodeData['type'] = changedType;
    } else if (nodeData['type'] !== 'start' && nodeData['type'] !== 'end') {
      nodeData['type'] = 'task';
    }

    let tmpData = this.toolkit.exportData();
    this.reloadToolkit(tmpData);
  }

  toggleSelectionNode(node: any) {
    console.log('toggleSelectionNode..', node);
    if (!this.selectedSkillInfo['skillId']) {
      this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_NOT_FOUND_SKILL_ID', 'error');
      node.data.attr = [];
    } else {
      if (!this.isOldVersion) {
        this.getMsgCdList(node.data).then();
        this.getMsgCodes(node.data).then(() => {
          this.currentNodeData = node.data;
          this.initialNodeData = this.cloneObject(node.data);
        });
      } else {
        this.currentNodeData = node.data;
        this.initialNodeData = this.cloneObject(node.data);
        this.msgCdList.push(this.currentNodeData['msgCd']);
      }
    }
    this.currentEdgeData = '';
    this.toolkit.clearSelection();
    this.toolkit.toggleSelection(node);
    this.selectType = node.objectType;
    this.showEditSpace = true;
  }

  selectionEdge(edge: any) {
    console.log(edge);
    this.currentNodeData = '';
    this.toolkit.clearSelection();
    this.selectType = edge.objectType;
    this.currentEdgeData = edge;
    this.showEditSpace = true;
  }

  editEdge(edge: any, action: string) {
    this.currentNodeData = '';
    this.translate.get(['TITLE.EDGE_MODIFY', 'TITLE.EDGE_ADD', 'TITLE.EDGE_MODIFY',
      'LABEL.OK', 'LABEL.CANCEL',
      'MSG.ALERT_IMPORT_EDGE_UPDATE',
      'VALUE.DEFAULT_EDGE_LABEL']).subscribe(trans => {

      const title = action === 'U' ? trans['TITLE.EDGE_MODIFY'] : trans['TITLE.EDGE_ADD'];
      this.removeEdgeButton.addEventListener('click', () => {this.removeEdge(edge)}, true);

      let okButton = document.createElement('button');
      okButton.innerHTML = trans['LABEL.OK'];
      okButton.setAttribute('jtk-commit', 'true');
      okButton.className = 'dialogsButtonClass';

      let cancelButton = document.createElement('button');
      cancelButton.innerHTML = trans['LABEL.CANCEL'];
      cancelButton.setAttribute('jtk-cancel', 'true');
      cancelButton.className = 'dialogsButtonClass';

      if (action === 'U' && edge.data.skillId) {
        Dialogs.show({
          id: 'dlgMessage',
          title: title,
          data: {
            msg: trans['MSG.ALERT_IMPORT_EDGE_UPDATE'],
          },
          buttons: [okButton]
        });
      } else {
        if (action === 'U') {
          Dialogs.show({
            id: 'dlgRadio',
            title: title,
            data: {
              radio: edge.data.type ? edge.data.type : this.edgeTypeList[0]['value'],
              text: edge.data.label ? edge.data.label : '',
            },
            buttons: [okButton, cancelButton, this.removeEdgeButton],
            onOK: (data: any) => {
              data.text.trim();
              if (data.text.length > 0) {
                data.text = data.text.length <= 30 ? data.text : data.text.substr(0, 30);
              } else {
                data.text = trans['VALUE.DEFAULT_EDGE_LABEL'];
              }

              this.toolkit.updateEdge(edge, {label: data.text, type: data.radio});
              this.reloadToolkit(this.toolkit.exportData());
            }
          });
        } else if (action === 'A') {
          Dialogs.show({
            id: 'dlgRadio',
            title: title,
            data: {
              radio: this.edgeTypeList[1]['value'],
              text: '',
            },
            onOK: (data: any) => {
              data.text.trim();
              if (data.text.length > 0) {
                data.text = data.text.length <= 30 ? data.text : data.text.substr(0, 30);
              } else {
                data.text = trans['VALUE.DEFAULT_EDGE_LABEL'];
              }

              this.toolkit.updateEdge(edge,
                {
                  label: data.text,
                  attr: [{text: '', type: ''}],
                  desc: '',
                  status: '',
                  type: data.radio
                });
              this.toolkit.setSelection(edge);
              this.selectionEdge(edge);
            },
            onCancel: (data: any) => {
              this.toolkit.removeEdge(edge.data.id);
              console.log('onCancel...', data);
            }
          });
        }
      }
    });
  }

  removeEdge(edge: any) {
    this.translate.get(['TITLE.EDGE_DELETE', 'MSG.ALERT_IMPORT_EDGE_DELETE', 'MSG.CONFIRM_DELETE_EDGE'],
      {label : edge.data.label}).subscribe(trans => {

      if (edge.data.skillId) {
        Dialogs.show({
          id: 'dlgMessage',
          title: trans['TITLE.EDGE_DELETE'],
          data: {
            msg: trans['MSG.ALERT_IMPORT_EDGE_DELETE'],
          },
        });
      } else {
        Dialogs.show({
          id: 'dlgConfirm',
          title: trans['TITLE.EDGE_DELETE'],
          data: {
            msg: trans['MSG.CONFIRM_DELETE_EDGE'],
          },
          onOK: () => {
            this.toolkit.removeEdge(edge);
            this.currentEdgeData = '';
            this.showEditSpace = false;
          },
        });
      }
    });
  }

  appendDefaultNode(metaYn: string) {
    if (this.selectedSkillInfo) {
      if (metaYn === 'N') {
        this.toolkit.addNode({
          id: jsPlumbUtil.uuid(),
          type: 'start',
          text: '시작',
          left: 50,
          top: 50,
          w: 100,
          h: 70,
          attr: [{type: '', text: ''}],
          desc: '',
        });

        // this.toolkit.addNode({
        //   id: jsPlumbUtil.uuid(),
        //   type: 'end',
        //   text: '종료',
        //   left: 900,
        //   top: 50,
        //   w: 100,
        //   h: 70,
        //   attr: [{type: '', text: ''}],
        //   desc: '',
        // });
        this.surface.zoomToFit({fill: 0.9, padding: 10});
      } else if (metaYn === 'Y') {
        this.toolkit.addNode({
          id: jsPlumbUtil.uuid(),
          type: 'task',
          text: 'node1',
          left: 550,
          top: 50,
          w: 100,
          h: 70,
          attr: [{type: '', text: ''}],
          desc: '',
          status: '',
        });
        this.surface.zoomToFit({fill: 0.9, padding: 10});
      }
    }
    let initialNodeInfo = this.toolkit.exportData();
    for (let idx in initialNodeInfo) {
      if (idx === 'nodes') {
        initialNodeInfo['nodes'].forEach(node => {
          node['attr'] = [];
        });
      }
      this.initialSkillInfo[idx] = initialNodeInfo[idx];
    }
  }

  appendTaskNode = (action?: string, node?: any) => {
    this.translate.get(['TITLE.NODE_MODIFY', 'TITLE.NODE_ADD', 'TITLE.NODE_MODIFY',
      'LABEL.OK', 'LABEL.CANCEL',
      'MSG.ALERT_START_NODE_UPDATE', 'MSG.ALERT_END_NODE_UPDATE', 'MSG.ALERT_IMPORT_NODE_UPDATE', 'MSG.VALIDATE_NODE_LENGTH']).subscribe(trans => {

      const title = action === 'U' ? trans['TITLE.NODE_MODIFY'] : trans['TITLE.NODE_ADD'];
      let msg = '';

      let okButton = document.createElement('button');
      okButton.innerHTML = trans['LABEL.OK'];
      okButton.setAttribute('jtk-commit', 'true');
      okButton.className = 'dialogsButtonClass';

      let cancelButton = document.createElement('button');
      cancelButton.innerHTML = trans['LABEL.CANCEL'];
      cancelButton.setAttribute('jtk-cancel', 'true');
      cancelButton.className = 'dialogsButtonClass';

      if (node) {
        if (node.data.type === 'start') {
          msg = trans['MSG.ALERT_START_NODE_UPDATE'];
        } else if (node.data.type === 'end') {
          msg = trans['MSG.ALERT_END_NODE_UPDATE'];
        } else if (node.data.skillId) {
          msg = trans['MSG.ALERT_IMPORT_NODE_UPDATE'];
        }
      }

      if (action === 'U') {
        if (msg) {
          Dialogs.show({
            id: 'dlgMessage',
            title: title,
            data: {
              msg: msg,
            },
            buttons: [okButton],
          });
        } else {
          Dialogs.show({
            id: 'dlgText',
            title: title,
            data: {
              text: node.data.text ? node.data.text : '',
            },
            buttons: [okButton, cancelButton, this.removeNodeButton],
            onOK: (data: any) => {

              if (data.text && data.text.length > 2 && data.text.length < 30) {
                if (data.text.length > 6) {
                  this.toolkit.updateNode(node, {w: (data.text.length * 17), text: data.text});
                } else {
                  this.toolkit.updateNode(node, {w: 100, text: data.text});
                }
              } else {
                Dialogs.show({
                  id: 'dlgMessage',
                  title: title,
                  data: {
                    msg: trans['MSG.VALIDATE_NODE_LENGTH'],
                  },
                });
              }
            }
          });
        }
      } else {
        Dialogs.show({
          id: 'dlgText',
          data: '',
          title: title,
          onOK: (data: any) => {
            if (data.text && data.text.length > 2 && data.text.length < 30) {
              let textLength = 100;
              if (data.text.length > 6) {
                textLength = data.text.length * 17;
              }
              const viewportBounds = this.surface.getViewportCenter();
              const newNode = this.toolkit.addNode({
                id: jsPlumbUtil.uuid(),
                type: 'task',
                text: data.text,
                left: Math.random() * (0 - viewportBounds[0]) + viewportBounds[0],
                top: Math.random() * (0 - viewportBounds[1]) + viewportBounds[1],
                w: textLength,
                h: 70,
                attr: [{type: '', text: ''}],
                desc: '',
                status: '',
                isEnd: false
              });
              this.toggleSelectionNode(newNode);
            } else {
              Dialogs.show({
                id: 'dlgMessage',
                title: title,
                data: {
                  msg: trans['MSG.VALIDATE_NODE_LENGTH'],
                },
              });
            }
          }
        });
      }
    });
  };

  removeTaskNode = () => {
    const selections = this.toolkit.getSelection().getNodes();

    if (selections.length === 1) {
      const node = selections[0];
      this.translate.get(['TITLE.NODE_DELETE',
          'MSG.CONFIRM_DELETE_EDGE', 'MSG.CONFIRM_IMPORT_SKILL_DELETE',
          'LABEL.OK', 'LABEL.CANCEL'],
        {label: node.data.text}).subscribe(trans => {

        let okButton = document.createElement('button');
        okButton.innerHTML = trans['LABEL.OK'];
        okButton.setAttribute('jtk-commit', 'true');
        okButton.className = 'dialogsButtonClass';

        let cancelButton = document.createElement('button');
        cancelButton.innerHTML = trans['LABEL.CANCEL'];
        cancelButton.setAttribute('jtk-cancel', 'true');
        cancelButton.className = 'dialogsButtonClass';

        if (!node.data['skillId']) {
          Dialogs.show({
            id: 'dlgConfirm',
            title: trans['TITLE.NODE_DELETE'],
            data: {
              msg: trans['MSG.CONFIRM_DELETE_EDGE'],
            },
            onOK: () => {
              this.toolkit.removeNode(node);
              this.currentNodeData = '';
              this.showEditSpace = false;
            },
          });
        } else if (node.data['skillId']) {
          Dialogs.show({
            id: 'dlgConfirm',
            title: trans['TITLE.NODE_DELETE'],
            data: {
              msg: trans['MSG.CONFIRM_IMPORT_SKILL_DELETE'],
            },
            buttons: [okButton, cancelButton],
            onOK: () => {
              let jsonData = this.selectedSkillInfo['jsonData'];
              let stringToJson = JSON.parse(jsonData);
              this.removeNode(node.data['skillId'], stringToJson);
              this.toolkit.removeNode(node);
              this.currentNodeData = '';
              this.showEditSpace = false;
            },
          });
        }
      });
    } else {
      this.translate.get(['TITLE.NODE_DELETE', 'MSG.ALERT_CHECK_SELECTED_NODE']).subscribe(trans => {

        Dialogs.show({
          id: 'dlgMessage',
          title: trans['TITLE.NODE_DELETE'],
          data: {
            msg: trans['MSG.ALERT_CHECK_SELECTED_NODE'],
          },
        });
      });
    }
  };

  removeNode(skillId, jsonData) {
    jsonData.nodes.forEach(data => {
      if (skillId === data['skillId']) {
        this.toolkit.removeNode(data);
      }
    });

    let importSkills = this.selectedSkillInfo['importSkills'];
    this.selectedSkillInfo['importSkills'] = importSkills.replace(skillId, '');
  }

  invisibleEditSpace() {
    console.log(this.showEditSpace);
    this.showEditSpace = false;
    console.log(this.showEditSpace);
  }

  appendNodeData() {
    this.insertMsgCode().then();
  }

  appendEdgeData() {
    this.currentEdgeData.data.attr.push({text: ''});
  }

  removeNodeData(index: number) {
    this.deleteMsgCode(this.currentNodeData.attr[index]).then(res => {
      if (res) {
        console.log('removeNodeData');
        this.currentNodeData.attr.splice(index, 1);
        this.initialNodeData.attr.splice(index, 1);
      }
    });
  }

  removeEdgeData(index: number) {
    this.currentEdgeData.data.attr.splice(index, 1);
  }

  changeNodeTextarea($event) {
    this.currentNodeData.desc = $event.target.value ? $event.target.value : '';
  }

  changeEdgeTextarea($event) {
    this.currentEdgeData.data.desc = $event.target.value ?
      $event.target.value :
      '';
  }

  importJson = () => {
    console.log('this.designLoader: ', this.designLoader);
    this.designLoader.nativeElement.click();
    // console.log('a: ', this.designLoader.nativeElement.click());
    // db에 저장하는 로직추가
    }

  exportJson = () => {
    const toolkitData = this.toolkit.exportData();
    this.selectedSkillInfo['edges'] = toolkitData['edges'];
    this.selectedSkillInfo['nodes'] = toolkitData['nodes'];
    const json = JSON.stringify(this.selectedSkillInfo, null, 2);
    this.downloadService.downloadJsonFile(json, this.selectedSkillName);
  };

  /*
   * commit할 때, 모든 node의 'Go to **' 체크하여 next skill list 변경
   */
  checkNextSkills(json): Promise<Boolean> {
    return new Promise((resolve) => {
      let jsonDataToJson = JSON.parse(json);
      let nodes = jsonDataToJson.nodes;
      let nextSkills: string[] = [];
      let gotoCodeIds: string[] = [];
      this.gotoList.forEach(gotoType => {
        gotoCodeIds.push(this.changeNameToId(this.systemCodeList['nodeCode'], gotoType));
      });

      nodes.forEach(node => {
        let attrs = node.attr;
        attrs.forEach(attr => {
          gotoCodeIds.forEach(gotoCodeId => {
            if (gotoCodeId) {
              if (attr.type === gotoCodeId) {
                if (!nextSkills.includes(attr.msgCtt)) {
                  nextSkills.push(attr.msgCtt);
                }
              }
            }
          });
        });
      });
      this.selectedSkillInfo['nextSkills'] = nextSkills.toString();
      resolve(true);
    });
  }

  commit = (flag?: string) => {
    this.param = new DialogDesignEntity();
    if (this.userId) {
      // const chartJson = JSON.stringify(this.toolkit.exportData(), null, 2);
      // let json = JSON.parse(chartJson);

      let graphData = this.cloneObject(this.toolkit.exportData());
      this.getAllMsgCodes(graphData).then(() => {
        let versionControllData = this.cloneObject(graphData);
        this.checkNextSkills(JSON.stringify(graphData, null, 2)).then(() => {
          for (let col in this.selectedSkillInfo) {
            if (col) {
              this.param[col] = this.selectedSkillInfo[col];
              if (col !== 'jsonData') {
                graphData[col] = this.selectedSkillInfo[col];
              }
            }
          }

          let copyJson = JSON.parse(JSON.stringify(graphData));
          copyJson['nodes'].forEach(node => {
            if (JSON.stringify(node).includes('http')) {
              this.param['linkYn'] = 'Y';
            }
            node['attr'] = [];
          });

          if (this.param['linkYn'] === null || this.param['linkYn'] === undefined) {
            this.param.linkYn = 'N';
          }

          this.initialSkillInfo = JSON.parse('{}');
          this.changeFlag = false;
          for (let col in this.param) {
            if (col) {
              if (col === 'constructor') {
                continue;
              } else if (col === 'nodes') {
                let paramNodes = this.cloneObject(this.param[col]);
                paramNodes.forEach(node => {
                  node['attr'] = [];
                });
                this.initialSkillInfo[col] = paramNodes;
              } else {
                this.initialSkillInfo[col] = this.param[col];
              }
            }
          }

          this.param['jsonData'] = JSON.stringify(copyJson, null, 2);
          this.param.workspaceId = this.workspaceId;
          this.param.updaterId = this.userId;

          // status가 'IT적용완료' 인 경우, ECT 데이터를 ECA로 업데이트 해준다.
          let itCompleteCode = this.changeNameToId(this.systemCodeList['statusCode'], this.STATUS_CODE_IT_COMPLETE);
          if (this.param['status'] === itCompleteCode) {
            flag = 'deploy';
          }

          let updateResult: Boolean;
          console.log('commit param: ', this.param);
          if (flag === 'import') {
            updateResult = this.defaultSkillComponent.updateSkill(this.param, 'MSG.COMPLETE_LOAD_SKILL');
          } else if (flag === 'deploy') {
            let deployParam: JSON = JSON.parse('{}');
            deployParam['haiSvcGrpCd'] = this.changeIdToName(
              this.systemCodeList['group1Code'], this.selectedSkillInfo['group1']);
            deployParam['haiChnlDvCd'] = this.HAI_CHNL_DV_CD;
            deployParam['skillCd'] = this.selectedSkillInfo['skillId'];
            deployParam['useYn'] = this.USE_YN;
            this.dialogDesignService.deployMsgCode(deployParam).subscribe(res => {
              updateResult = this.defaultSkillComponent.updateSkill(this.param, 'MSG.ALERT_DEPLOY_MSG_CODE');
            });
          } else {
            updateResult = this.defaultSkillComponent.updateSkill(this.param);
          }

          // 스킬이 정상적으로 저장된 경우에만 버전관리 api 호출
          if (updateResult) {
            // node attr 정보가 있는 상태로 버전관리용 데이터 저장
            // this.getAllMsgCodes(graphData).then(() => {
              this.param['jsonData'] = JSON.stringify(versionControllData, null, 2);
              this.setVersion(this.param);
            // });
          }
        });
      });
    } else {
      let title = 'TITLE.ERROR';
      let message = 'MSG.ERROR_NOT_FOUND_ID';
      this.translate.get([title, message]).subscribe(trans => {
        let ref = this.dialog.open(AlertComponent);
        ref.componentInstance.header = 'error';
        ref.componentInstance.title = trans[title];
        ref.componentInstance.message = trans[message];
      });
    }
  };

  loadDesignFile($event): Promise<Boolean>{
    return new Promise((resolve => {
      if (!!$event.target.files[0]) {
        const targetFile = $event.target.files[0];
        let fr = new FileReader();
        fr.onloadend = (ev: ProgressEvent) => {
          this.designLoader.nativeElement.value = '';
          try {
            const target = (ev.target as FileReader);
            // const design = JSON.parse(target.result as string);
            this.selectedSkillInfo = JSON.parse(target.result as string);
            this.selectedSkillName = this.selectedSkillInfo['skillName'];
            this.reloadToolkit(this.selectedSkillInfo);
            this.selectType = undefined;
            this.commit('import');
          } catch (e) {
            console.log(e);
            Dialogs.show({
              id: 'dlgMessage',
              title: 'Error',
              data: {
                msg: getErrorString(e, `Failed to load the file '${targetFile.name}'.`),
              },
            });
          }
        };
        fr.onerror = () => {
          this.designLoader.nativeElement.value = '';
          Dialogs.show({
            id: 'dlgMessage',
            title: 'Error',
            data: {msg: `Failed to load the file '${targetFile.name}'.`}
          });
        };
        fr.readAsText(targetFile);
        resolve(true);
      } else {
        console.log('file is not selected.');
        resolve(false);
      }
    }));
  };

  getApiParam() {
    const exportData = this.toolkit.exportData();
    const returnParam = {};

    returnParam['name'] = this.scenarioName;
    returnParam['projectName'] = this.korTypeToEng.transform(this.scenarioName);
    returnParam['lang'] = 1;
    returnParam['description'] = this.scenarioName;
    returnParam['nodes'] = [];
    returnParam['edges'] = [];

    exportData['nodes'].forEach(function (node) {
      returnParam['nodes'].push({
        id: node.id,
        type: node.type,
        text: node.text,
        desc: node.desc,
        attr: node.attr ? node.attr : [],
        position: {
          left: node.left,
          top: node.top,
          w: node.w,
          h: node.h,
        },
      });
    });

    exportData['edges'].forEach(function (edge) {
      returnParam['edges'].push({
        source: edge.source,
        target: edge.target,
        data: {
          id: edge.data.id,
          type: edge.data.type,
          label: edge.data.label,
          desc: edge.data.desc,
          attr: edge.data.attr ? edge.data.attr : [],
        },
      });
    });

    console.log(returnParam);
    return returnParam;
  }

  showInfoPage() {
    this.currentNodeData = '';
    this.selectType = undefined;
    this.showEditSpace = true;
  }

  reloadToolkit(loadData?) {
    this.toolkit.clear();
    this.toolkit.load({data: loadData});
    this.surface.zoomToFit({fill: 0.9, padding: 10});
  }

  getSkillInfo(skillInfoJson?: string) {
    this.selectType = undefined;
    this.selectedSkillInfo = [];
    this.initialSkillInfo = [];
    this.selectedSkillName = undefined;
    this.currentNodeData = '';
    this.currentEdgeData = '';
    this.topicSentence = '';

    if (skillInfoJson) {
      this.showEditSpace = true;
      this.selectedSkillInfo = JSON.parse(skillInfoJson);
      this.selectedSkillName = this.selectedSkillInfo['skillName'];
      this.topicSentence = this.selectedSkillInfo['topicSentence'];

      // 스킬이 선택될 때 마다 선택된 commit 버전 초기화
      this.selectedCommit = {hashKey: undefined, commitTime: undefined, updaterId: undefined, updaterEmail: undefined};
      this.getCommitList().then(() => {
                // select하는 값이 (group1, status 등) 존재하지 않으면 0번째에 있는 값으로 넣어준다.
        this.tableTopic.forEach(skillInfo => {
          if (skillInfo['type'] === 'select') {
            if (!this.selectedSkillInfo[skillInfo['attr']]) {
              this.selectedSkillInfo[skillInfo['attr']] = skillInfo['data']['options'][0]['id'];
            }
          }
        });

        this.initialSkillInfo = this.cloneObject(this.selectedSkillInfo);

        if (this.selectedSkillInfo['nodes']) {
          this.designLoader.nativeElement.value = '';
          this.reloadToolkit(this.selectedSkillInfo);
          this.selectType = undefined;
        } else {
          this.toolkit.clear();
          this.surface = this.surfaceComponent.surface;
          const drawingTools = new DrawingTools({
            renderer: this.surface,
          });
          setTimeout(() => {
            this.appendDefaultNode(this.selectedSkillInfo['metaYn']);
            // this.surface.zoomToFit({fill: 0.9, padding: 10});
          }, 0);
        }
      });
    } else {
      this.toolkit.clear();
      this.selectedSkillInfo = [];
      this.initialSkillInfo = [];
      this.selectedSkillName = undefined;
      this.topicSentence = '';
      this.showEditSpace = false;
    }
  }

  /*
   * import하는 메타 스킬의 노드, 엣지에 import(flag로 사용), id(uuid) 추가해서 import
   */
  importSkill(metaInfo?: string[]) {
    console.log('metaInfo: ', metaInfo);
    const metaJson = JSON.parse(metaInfo['jsonData']);

    for (let nodeIdx in metaJson['nodes']) {
      if (nodeIdx) {
        metaJson['nodes'][nodeIdx]['skillId'] = metaInfo['id']
      }
    }
    this.toolkit.addNodes(metaJson['nodes']);

    for (let edge of metaJson['edges']) {
      if (edge) {
        edge['data']['skillId'] = metaInfo['id'];
        this.toolkit.addEdge(edge);
      }
    }

    if (this.selectedSkillInfo['importSkills']) {
      let list = this.selectedSkillInfo['importSkills'];
      if (!list.toString().includes(metaInfo['id'])) {
        this.selectedSkillInfo['importSkills'] += ',' + metaInfo['id'];
      }
    } else {
      this.selectedSkillInfo['importSkills'] = metaInfo['id'];
    }
  }

  checkChangedSkill(row) {
    let dialogMsg = 'MSG.CONFIRM_MOVE_SKILL';
    let graphData = this.cloneObject(this.toolkit.exportData());
    for (let col in this.selectedSkillInfo) {
      if (col) {
        if (col !== 'jsonData') {
          graphData[col] = this.selectedSkillInfo[col];
        }
      }
    }

    let copyJson = JSON.parse(JSON.stringify(graphData));
    copyJson['nodes'].forEach(node => {
      node['attr'].forEach(attr => {
        if (attr.changed === true) {
          dialogMsg = 'MSG.CONFIRM_MOVE_SKILL_MSG';
        }
      });
      node['attr'] = [];
    });

    try {
      deepStrictEqual(this.initialSkillInfo, copyJson);
      this.changeFlag = false;
    } catch (e) {
      console.log('changed some skill information!: ', e);
      this.changeFlag = true;
      console.log(JSON.stringify(this.initialSkillInfo, null, 2));
      console.log(JSON.stringify(copyJson, null, 2));
    }

    if (dialogMsg === 'MSG.CONFIRM_MOVE_SKILL_MSG') {
      this.changeFlag = true;
    }

    if (this.changeFlag) {
      // 저장후이동, 그냥이동, 취소
      this.translate.get([dialogMsg]).subscribe(trans => {
        let ref = this.dialog.open(MoveSkillConfirmComponent);
        ref.componentInstance.message = trans[dialogMsg];
        ref.afterClosed()
        .subscribe(result => {
          if (result) {
            if (ref.componentInstance.isSave) {
              this.commit();
            }
            this.defaultSkillComponent.navigateSkillDetailView(row);
          }
        });
      });
    } else {
      // alert 안띄우고 바로 이동
      this.defaultSkillComponent.navigateSkillDetailView(row);
    }
  }

  checkChangedMsgCd(attr) {
    if (attr['text'] !== attr['msgCtt']) {
      attr['changed'] = true;
    } else {
      attr['changed'] = false;
    }
  }

  openWebEditor(nodeAttr, i) {
    console.log('nodeAttr: ', nodeAttr, i);

    const dialogRef = this.dialog.open(WebEditorComponent, {
      width: '700px'
    });

    dialogRef.componentInstance.title1 = this.WEB_EDITOR_TITLE_1;
    dialogRef.componentInstance.title2 = this.WEB_EDITOR_TITLE_2;
    dialogRef.componentInstance.text1 = nodeAttr['msgCtt'];

    dialogRef.afterOpened().subscribe(() => {
      dialogRef.componentInstance.setHtmlData(nodeAttr['tmplCtt']);
    });

    dialogRef.beforeClosed().subscribe(() => {
      let htmlData = dialogRef.componentInstance.getHtmlData()[1];
      let msgData = dialogRef.componentInstance.getTextareaData();

      // 메세지코드 저장 버튼 색 변경해주기위해 수정되었는지 체크
      if ((nodeAttr['msgCtt'] !== msgData) || (nodeAttr['tmplCtt'] !== htmlData)) {
        nodeAttr['changed'] = true;

        if (htmlData[0] === '&') {
          htmlData = htmlData.substring(6, );
        }
        nodeAttr['tmplCtt'] = htmlData;
        nodeAttr['msgCtt'] = msgData.trim();
      } else {
        if (nodeAttr['msgCtt'] === msgData) {
          nodeAttr['changed'] = false;
        }
        if (nodeAttr['tmplCtt'] === htmlData) {
          nodeAttr['changed'] = false;
        }
      }
    });
  }

  changeButtonHidden() {
    this.actions.forEach(action => {
      if (action['type'] === 'simulation' || action['type'] === 'skillList')
      action['hidden'] = !action['hidden'];
    });
    // this.save['hidden'] = !this.save['hidden'];
  }

  showSkillList = () => {
    this.isSimulation = !this.isSimulation;
    this.changeButtonHidden();
    this.surface.deactivateState('mintyGreenState', this.toolkit);
    this.reloadToolkit(this.toolkit.exportData());
  }

  execSimulation = () => {
    let graphData = this.cloneObject(this.toolkit.exportData());
    this.getAllMsgCodes(graphData).then(() => {
      console.log('execSimulation exportData: ', graphData);
      this.scenario = this.cloneObject(graphData);
      this.isSimulation = !this.isSimulation;
      this.changeButtonHidden();
    });
    this.getListOrderInfo();
  }

  getListOrderInfo() {
    this.storage.set('scenarioOrderProperty', this.defaultSkillComponent.orderProperty);
    this.storage.set('scenarioOrderDirection', this.defaultSkillComponent.orderDirection);
  }

  simulatorCallback(event) {
    let simulatingEdge = null;

    this.selectedSkillInfo['edges'].forEach(edge => {
      if (edge['data']['id'] === event['edgeId']) {
        simulatingEdge = edge;
      }
    });

    let target = simulatingEdge['target'];
    let source = simulatingEdge['source'];

    this.surface.deactivateState('mintyGreenState', this.toolkit);
    this.reloadToolkit(this.toolkit.exportData());
    this.surface.activateState('mintyGreenState', this.toolkit.getPath({source: source, target: target}));
  }

  getCommitList(): Promise<Boolean> {
    return new Promise((resolve => {
      this.gitParam = {};
      this.gitParam['workspaceId'] = this.storage.get('m2uWorkspaceId');
      this.gitParam['id'] = this.selectedSkillInfo['id'];
      if (this.checkEmpty(this.gitParam['workspaceId']) && this.checkEmpty(this.gitParam['id'])) {
        this.dialogDesignService.getCommitList(this.gitParam)
          .subscribe(response => {
              console.log('getCommitList: ', response);
              let res: any;
              res = response;
              if (res) {
                res.forEach(commit => {
                  commit['commitTime'] =
                    new DatePipe('en-US').transform(commit['commitTime'], 'yyyy/MM/dd HH:mm');
                });
                this.commitList = res;

                // commitList 불러오면서 selectedCommit 지정
                if (this.selectedCommit && this.selectedCommit.hashKey) {
                  this.selectedCommit =
                    this.commitList.find(commit => {
                      return commit.hashKey === this.selectedCommit.hashKey;
                    });
                } else {
                  this.selectedCommit = this.commitList[0];
                }
              }
              resolve(true);
            },
            err => {
              this.openAlertDialog('Error', 'Failed get commit list', 'error');
              resolve(false);
            });
      }
    }));
  }

  setVersion(param) {
    this.dialogDesignService.updateVersion(param).subscribe(() => {
      console.log('version update success!!');
      this.getCommitList().then(res => {
        this.selectedCommit = this.commitList[0];
      });
    },
      err => {
      console.error('version update failed: ', err);
    });
  }

  changeVersion(e) {
    console.log('changeVersion e: ', e);
    let isFind = false;
    if ((e.value === undefined) || e.value.hashKey === this.commitList[0]['hashKey']) {
      if (this.isOldVersion) {
        this.reloadToolkit(this.currentJsonData);
        this.currentJsonData['nodes'].forEach(node => {
          if (this.currentNodeData['id'] === node['id']) {
            isFind = true;
            this.currentNodeData = node;
          }
        });
        this.isOldVersion = false;
        if (!isFind) {
          this.showInfoPage();
        }
      }
    } else {
      this.gitParam = {};
      this.gitParam['workspaceId'] = this.storage.get('m2uWorkspaceId');
      this.gitParam['id'] = this.selectedSkillInfo['id'];
      this.gitParam['commitId'] = e.value.hashKey;

      this.dialogDesignService.getCommit(this.gitParam).subscribe(result => {
          if (result) {
            // 현재 데이터 백업
            if (!this.isOldVersion) {
              this.currentJsonData = this.toolkit.exportData();
              this.isOldVersion = true;
            }
            this.reloadToolkit(result);
            result['nodes'].forEach(node => {
              if (this.currentNodeData['id'] === node['id']) {
                isFind = true;
                this.currentNodeData = node;
              }
            });

            if (!isFind) {
              this.showInfoPage();
            }
          }
        },
        err => {
          this.openAlertDialog('Error', 'Failed get commit information', 'error');
        });
    }
  }

  getShortId(id) {
    if (id) {
      return id.substring(0, 8);
    } else {
      return '';
    }
  }

  checkEmpty(param) {
    if (param !== null && typeof (param) !== 'undefined' && param !== '') {
      return true;
    } else {
      return false;
    }
  }

  getAllMsgCodes = (graphData): Promise<any> => {
    let promises = [];
    graphData['nodes'].forEach(node => {
      if (node['msgCd']) {
        promises.push(new Promise(resolve => {
          this.getMsgCodes(node).then(() => {
            resolve();
          });
        }));
      }
    });
    return Promise.all(promises);
  }

  insertMsgCode(): Promise<Boolean> {
    return new Promise((resolve) => {
      console.log('call insertMsgCode');
      let param: JSON = JSON.parse('{}');
      let attrLength = this.currentNodeData['attr'].length;

      if (!this.selectedSkillInfo['skillId']) {
        this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_NOT_FOUND_SKILL_ID', 'error');
        resolve(false);
      } else {
        param['skillCd'] = this.selectedSkillInfo['skillId'];
        if (this.currentNodeData['msgCd']) {
          param['msgCd'] = this.currentNodeData['msgCd'];
          param['msgMgmtCd'] = this.currentNodeData['msgCd'].substr(0, 3);
          param['dtlsMsgMgntCd'] = this.currentNodeData['msgCd'].substr(3, 1);
          param['msgNo'] = this.currentNodeData['msgCd'].substr(4);
        }

        if (attrLength !== 0) {
          // node attr이 한개 이상 존재할 경우
          param['msgSeq'] = this.currentNodeData['attr'][attrLength - 1]['msgSeq'] + 1;
          param['orderSeq'] = this.currentNodeData['attr'][attrLength - 1]['orderSeq'] + 1;
        } else {
          // node attr이 하나도 존재하지 않는 경우
          param['msgSeq'] = 1;
          param['orderSeq'] = 1;
        }

        param['haiSvcGrpCd'] = this.changeIdToName(
          this.systemCodeList['group1Code'], this.selectedSkillInfo['group1']);
        param['haiChnlDvCd'] = this.HAI_CHNL_DV_CD;

        this.dialogDesignService.insertMsgCode(param).subscribe(res => {
            console.log('insertMsgCode res: ', res);
            if (res) {
              if (!this.currentNodeData['msgCd']) {
                this.getMsgCdList(this.currentNodeData).then();
              }
              this.currentNodeData['msgCd'] = res[attrLength]['msgCd'];
              this.getMsgCodes(this.currentNodeData).then();
              this.initialNodeData['attr'][attrLength] = this.currentNodeData['attr'][attrLength];
            }
            resolve(true);
          },
          err => {
            this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_INSERT_MSG_CODE', 'error');
            resolve(false);
          });
      }
    });
  }

  updateMsgCode(nodeAttr, i): void {
    console.log('nodeAttr, i', nodeAttr, i);
    nodeAttr['skillCd'] = this.selectedSkillInfo['skillId'];
    if (this.currentNodeData['msgCd']) {
      nodeAttr['msgNo'] = this.currentNodeData['msgCd'].substr(4);
    }
    nodeAttr['msgSeq'] = nodeAttr['msgSeq'];
    nodeAttr['orderSeq'] = nodeAttr['orderSeq'];

    console.log('call updateMsgCode ', nodeAttr);
    this.dialogDesignService.updateMsgCode(nodeAttr).subscribe(() => {
        this.openAlertDialog('TITLE.SUCCESS', 'MSG.COMPLETE_APPLY_MSG', 'success');
        nodeAttr['text'] = nodeAttr['msgCtt'];
        nodeAttr['changed'] = false;
        this.initialNodeData['attr'][i] = nodeAttr;
        if (this.isSimulation) {
          // 시뮬레이션 모드일 때, 저장하는 경우.
          let graphData = this.cloneObject(this.toolkit.exportData());
          this.getAllMsgCodes(graphData).then(() => {
            console.log('updateMsgCode exportData: ', graphData);
            this.scenario = this.cloneObject(graphData);
            this.surface.deactivateState('mintyGreenState', this.toolkit);
            this.reloadToolkit(this.toolkit.exportData());
          });
          this.getListOrderInfo();
        } else {
          // 메세지코드 저장할 때마다 전이스킬 리스트 업데이트
          this.checkNextSkills(JSON.stringify(this.toolkit.exportData())).then(() => {
            if (this.selectedSkillInfo['nextSkills'].length === 0) {
              this.defaultSkillComponent.showSkillList = [];
              this.defaultSkillComponent.lengthPart = 0;
            } else {
              this.defaultSkillComponent.showSkillInfo().then();
            }
          });
        }
      },
      err => {
        this.openAlertDialog( 'TITLE.ERROR', 'MSG.ERROR_UPDATE_MSG_CODE', 'error');
      });
  }

  dtlsMsgTypeToId(dtlsMsgTypeCd) {
    let type = '';
    this.systemCodeList['nodeCode'].forEach(nodeCode => {
      let nodeDesc = JSON.parse(nodeCode['description']);
      if (nodeDesc['dtlsMsgTypeCd'] === dtlsMsgTypeCd) {
        type = nodeCode['id'];
      }
    });
    return type;
  }

  idToDtlsMsgType(id) {
    let type = '';
    this.systemCodeList['nodeCode'].forEach(nodeCode => {
      if (nodeCode['id'] === id) {
        let nodeDesc = JSON.parse(nodeCode['description']);
        type = nodeDesc['dtlsMsgTypeCd'];
      }
    });
    return type;
  }

  getMsgCdList(node) {
    // 메세지 코드 목록 조회 ex) NORA00001, NORA00002 등
    return new Promise<any>((resolve, reject) => {
      let param = JSON.parse('{}');
      node['attr'] = [];
      param['haiSvcGrpCd'] = this.changeIdToName(this.systemCodeList['group1Code'], this.selectedSkillInfo['group1']);
      param['haiChnlDvCd'] = this.HAI_CHNL_DV_CD;
      param['skillCd'] = this.selectedSkillInfo['skillId'];
      param['useYn'] = this.USE_YN;
      console.log('param: ', param);
      this.dialogDesignService.getMsgCode(param).subscribe(res => {
          console.log('getMsgCdList res: ', res);
          if (res) {
            this.msgCdList = [];
            res.forEach(msgCdList => {
              // 중복제거
              if (!this.msgCdList.includes(msgCdList['msgCd'])) {
                this.msgCdList.push(msgCdList['msgCd']);
              }
            });
          }
          resolve();
        },
        err => {
          this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_GET_MSG_CODE_LIST', 'error');
          reject();
          console.log('getMsgCode failed!! ', err);
        });
    });
  }


  getMsgCodes(node) {
    // 메세지 코드 내용 조회 ex) [버튼, 내용], [Go to Skill, 00001] 등
    return new Promise<any>((resolve, reject) => {
      let param = JSON.parse('{}');
      node['attr'] = [];
      param['haiSvcGrpCd'] = this.changeIdToName(this.systemCodeList['group1Code'], this.selectedSkillInfo['group1']);
      param['haiChnlDvCd'] = this.HAI_CHNL_DV_CD;
      param['msgCd'] = node['msgCd'];
      param['skillCd'] = this.selectedSkillInfo['skillId'];
      param['useYn'] = this.USE_YN;
      console.log('param: ', param);
      this.dialogDesignService.getMsgCode(param).subscribe(res => {
          console.log('getMsgCodes res: ', res);
          if (res.length > 0) {
            if (node['msgCd']) {
              // 공통 메세지 코드인 경우, 수정 불가하도록 flag 설정
              if (res[0]['skillCd'] === this.COMMON_MSG_CODE) {
                node['commonMsgCd'] = true;
              } else {
                delete node['commonMsgCd'];
              }

              // orderSeq 순서대로 보여주기 위해 sorting
              let sortedRes = res.sort(function(a, b) {
                return a['orderSeq'] - b['orderSeq'];
              });

              sortedRes.forEach(attr => {
                attr['type'] = this.dtlsMsgTypeToId(attr['dtlsMsgTypeCd']);
                attr['changed'] = false;
                attr['text'] = attr['msgCtt'];
                node['attr'].push(attr);
              });
            }
          }
          resolve();
        },
        err => {
          this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_GET_MSG_CODE', 'error');
          reject();
          console.log('getMsgCode failed!! ', err);
        });
    });
  }

  setMsgCd(e) {
    this.currentNodeData['msgCd'] = e['value'];
    this.getMsgCodes(this.currentNodeData).then(() => {
      this.initialNodeData = this.cloneObject(this.currentNodeData);
    });
  }

  deleteMsgCode = (attr): Promise<any> => {
    return new Promise<any>((resolve => {
      let result = true;
      this.openConfirmDialog('MSG.CONFIRM_DELETE_MSG_CODE').then(res => {
        if (res) {
          console.log('deleteMsgCode attr: ', attr);
          let param = JSON.parse('{}');
          param['haiSvcGrpCd'] = this.changeIdToName(
            this.systemCodeList['group1Code'], this.selectedSkillInfo['group1']);
          param['haiChnlDvCd'] = this.HAI_CHNL_DV_CD;
          param['skillCd'] = this.selectedSkillInfo['skillId'];
          param['msgCd'] = attr['msgCd'];
          param['msgSeq'] = attr['msgSeq'];
          param['useYn'] = this.USE_YN;
          console.log('param: ', param);
          this.dialogDesignService.deleteMsgCode(param).subscribe(() => {
            if (this.currentNodeData.attr.length === 0) {
              this.currentNodeData['msgCd'] = undefined;
            }
          }, err => {
            console.log('err: ', err);
            if (err.status === 400) {
              this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_DELETE_MSG_CODE_BY_PARAM', 'error');
            } else {
              this.openAlertDialog('TITLE.ERROR', 'MSG.ERROR_DELETE_MSG_CODE', 'error');
            }
          });
        } else {
          result = false;
        }
        resolve(result);
      });
    }));
  }

  openConfirmDialog = (message): Promise<any> => {
    return new Promise<any>(resolve => {
      this.translate.get([message]).subscribe(trans => {
        let ref = this.dialog.open(ConfirmComponent);
        ref.componentInstance.message = trans[message];
        ref.afterClosed().subscribe(confirmed => {
          if (confirmed) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
    });
  }

  changeIdToName(datas?, id?) {
    let name = '';
    if (datas) {
      datas.forEach(data => {
        if (data['id'] === id) {
          name = data['name'];
        }
      });
    }
    return name;
  }

  changeNameToId(datas?, name?) {
    let id = '';
    if (datas) {
      datas.forEach(data => {
        if (data['name'] === name) {
          id = data['id'];
        }
      });
    }
    return id;
  }

  openAlertDialog(title, message, status) {
    this.translate.get([title, message]).subscribe(trans => {
      let ref = this.dialog.open(AlertComponent);
      ref.componentInstance.header = status;
      ref.componentInstance.title = trans[title];
      ref.componentInstance.message = trans[message];
    });
  }

  cloneObject(obj) {
    return JSON.parse(JSON.stringify(obj));
  }
}
