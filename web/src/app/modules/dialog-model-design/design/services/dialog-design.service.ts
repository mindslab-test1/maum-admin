import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DialogDesignEntity} from '../entity/dialog-design.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class DialogDesignService {
  API_URL;
  MSG_CODE_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.MSG_CODE_URL = environment.msgCodeApiUrl;
  }

  updateSkill(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/updateSkill', param);
  }

  getSkill(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/getSkill', param);
  }

  getSkillLists(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/getSkillLists', param);
  }

  getSubSkills(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/getSubSkills', param);
  }

  getImportSkills(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/getImportSkills', param);
  }

  deleteCheckedSkill(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/deleteCheckedSkill', param);
  }

  getSystemCode(): Observable<any> {
    return this.http.get(this.API_URL + '/sds/scenario/getSystemCode');
  }

  updateVersion(param: DialogDesignEntity): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/updateVersion', param);
  }

  getCommit(param): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/getCommit', param);
  }

  getCommitList(param): Observable<any> {
    return this.http.post(this.API_URL + '/sds/scenario/getCommitList', param);
  }

  getMsgCode(param): Observable<any> {
    return this.http.post(this.MSG_CODE_URL + '/msg-code/skill-msgs', param);
  }

  insertMsgCode(param): Observable<any> {
    return this.http.post(this.MSG_CODE_URL + '/msg-code/insert', param);
  }

  updateMsgCode(param): Observable<any> {
    return this.http.post(this.MSG_CODE_URL + '/msg-code/update', param);
  }

  deleteMsgCode(param): Observable<any> {
    return this.http.post(this.MSG_CODE_URL + '/msg-code/delete', param);
  }

  deployMsgCode(param): Observable<any> {
    return this.http.post(this.MSG_CODE_URL + '/msg-code/deploy_msgs', param);
  }
}
