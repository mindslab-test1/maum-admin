import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IntentFinderRoutingModule} from './intent-finder-routing.module';
import {IntentFinderPoliciesComponent} from './components/intent-finder-policies/intent-finder-policies.component';
import {IntentFinderPolicyUpsertComponent} from './components/intent-finder-policy-upsert/intent-finder-policy-upsert.component';
import {IntentFinderPolicyPanelComponent} from './components/intent-finder-policy-panel/intent-finder-policy-panel.component';
import {IntentFinderPolicyDetailComponent} from './components/intent-finder-policy-detail/intent-finder-policy-detail.component';
import {SharedModule} from '../../shared/shared.module';
import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  declarations: [
    IntentFinderPoliciesComponent,
    IntentFinderPolicyUpsertComponent,
    IntentFinderPolicyPanelComponent,
    IntentFinderPolicyDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IntentFinderRoutingModule,
    SharedModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    MatRadioModule,
    MatCardModule,
    MatButtonModule,
    MatTabsModule
  ]
})
export class IntentFinderModule {
}
