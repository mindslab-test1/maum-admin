import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderPolicyPanelComponent} from './intent-finder-policy-panel.component';

describe('IntentFinderPolicyPanelComponent', () => {
  let component: IntentFinderPolicyPanelComponent;
  let fixture: ComponentFixture<IntentFinderPolicyPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderPolicyPanelComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderPolicyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
