import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderPolicyUpsertComponent} from './intent-finder-policy-upsert.component';

describe('IntentFinderPolicyUpsertComponent', () => {
  let component: IntentFinderPolicyUpsertComponent;
  let fixture: ComponentFixture<IntentFinderPolicyUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderPolicyUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderPolicyUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
