import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {
  AppEnumsComponent,
  AppObjectManagerService,
  ErrorDialogComponent,
  ConfirmComponent
} from '../../../../shared';
import {ROUTER_LOADED} from '../../../../core/actions';
// import {DragulaService} from 'ng2-dragula';
import {VARIABLE_REGEX} from '../../../../shared/values/values';
import {GrpcApi} from '../../../../core/sdk/services/custom/Grpc';
import {getErrorString} from '../../../../shared/values/error-string';
import {DragulaService} from 'ng2-dragula';

enum ModelType {
  dnn_model = 0, cs_model = 1, hmd_model = 2, sc_model = 3
}

@Component({
  selector: 'app-intent-finder-policy-upsert',
  templateUrl: './intent-finder-policy-upsert.component.html',
  styleUrls: ['./intent-finder-policy-upsert.component.scss'],
})
/**
 * Intent Finder Policy ADD & EDIT Component
 */

export class IntentFinderPolicyUpsertComponent implements OnInit, AfterViewInit {

  dialogTitle: string;
  role: string;
  itfp: any;
  itfpName: string;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  languages = ['kor', 'eng'];
  classifyingMethods = [
    {attr: 'custom_script_mod', name: 'Custom Script'},
    {attr: 'pcre_mod', name: 'PCRE'},
    {attr: 'hmd_mod', name: 'HMD'},
    {attr: 'dnn_cl_mod', name: 'DNN'},
    {attr: 'final_rule', name: 'Final Rule'}
  ];

  runStyles = [
    {value: 'skillRunStyles', viewValue: 'Skill'},
    {value: 'IFC_HINT', viewValue: 'Hint'},
  ];

  skillRunStyles = [
    {value: 'IFC_SKILL_FOUND_RETURN', viewValue: 'Return'},
    {value: 'IFC_SKILL_FOUND_COLLECT_HINTS', viewValue: 'Continue'}
  ];

  selectedRunStyle = null;

  steps = [];
  customscript = [];
  pcres = [];
  hmds = [];
  dnns = [];

  selectedMethod = {attr: null, name: null}; // 선택된 분류 값을 담기 위한 변수
  categories = []; // Model 클릭 시 category 들이 담긴다.

  categoryFlag = false; // Model 선택 시, Category 화면 Hidden 플래그
  stepAddFlag = true; // step을 등록하기 위한 조건이 만족할때 버튼 활성화 플래그
  finalRuleFlag = false; // finalRule 등록을 하면, 더 이상 화면에 등록화면을 보여주지 않기 위한 플래그
  changeAddInputModeFlag = false;

  customscriptBeforeClickData: HTMLInputElement;
  customscriptCurrentClickData: HTMLInputElement;

  pcreBeforeClickData: HTMLInputElement;
  pcreCurrentClickData: HTMLInputElement;

  hmdBeforeClickData: HTMLInputElement;
  hmdCurrentClickData: HTMLInputElement;

  dnnBeforeClickData: HTMLInputElement;
  dnnCurrentClickData: HTMLInputElement;

  clickEditData: any;
  clickEditElementId: string;
  @ViewChildren('stepList') stepList: QueryList<any>;
  originStepName: string = null;
  originStep: any;

  isCustomEditMode = false;
  theLastStep: any;
  allHidden = false;

  constructor(private store: Store<any>,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              public snackBar: MatSnackBar,
              private cdr: ChangeDetectorRef,
              public appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private _formBuilder: FormBuilder,
              private dragulaService: DragulaService,
              private grpc: GrpcApi) {

    const bag: any = this.dragulaService.find('evented-bag');
    if (bag !== undefined) {
      this.dragulaService.destroy('evented-bag');
      this.dragulaService.setOptions('evented-bag', {revertOnSpill: true});
    }

    dragulaService.dropModel.subscribe((value) => {
      this.onDropModel(value.slice(1));
    });
  }

  /**
   * Category 개수에 따라 validate 동적으로 생성
   * @param {AbstractControl} control
   */
  validateDnnCutCoefficient(control: AbstractControl) {
    if (this.categories.length > 1) {
      control.setValidators([
        Validators.required,
        () => control.value >= 1.0 || control.value <= 0
          ? {rangeExceeding: true} : null
      ]);
    } else if (this.categories.length <= 1) {
      control.clearValidators();
    }
    control.updateValueAndValidity({
      emitEvent: false,
      onlySelf: true
    });
  }

  /**
   * 드래그 후 드랍시 이벤트 감지
   * @param args => drop 영역 내의 HTML 태그
   */
  onDropModel(args: any): void {
    let [el, target, source] = args;
    this.checkPriority();
  }

  ngOnInit(): void {
    let obm = this.objectManager.get('itfp');
    // /m2u/dialog-service/itfp/itf-default/edit 로 접근할수 있는 방법은 두가지 입니다
    // 1. itfp view에서 -> itfp edit로 버튼을 클릭해서 이동한 경우
    // 2.  사용자가 직접 url에/m2u/dialog-service/itfp/itf-default/edit를 입력하여 들어온 경우
    // 2 번의 경우objectManager 에itfp  값이 없습니다
    // 이 경우 itfp값을 획득하기 위한 예외처리는 아래와 같습니다
    let promise = new Promise((resolve, reject) => {
        this.route.params.subscribe(par => {
            if (this.route.toString().indexOf('new') > 0) { // new인 경우
              // url에 itfp/new가 있는지 확인합니다
              console.log('#@ There is new');
              // 신규생성 시 new를 add로 변경합니다.(기존 소스 유지)
              this.role = 'add';
            } else if (this.route.toString().indexOf('edit') > 0) {  // edit인 경우
              this.role = 'edit';
              this.itfpName = par['id'];
            } else {  // url이 잘못된 경우
              console.log('#@ There is no new');
              this.openErrorDialog('Failed', `Cannot find this url.`);
              this.backPage();
              reject();
            }
          }
        );
        if (this.role === 'edit' && !this.itfp) {
          // 사용자 클릭으로 넘어왔을경우 itfp name 이 유효하지 않으면 reject.
          // if ( this.itfp && this.itfp.name === undefined ) {
          // 사용자가 url로 edit 접근했으로 경우 grpc호출을 통해 itfp 정보를 받아온다.
          let temp = this.getIntentFinderPolicyInfo();
          temp.then(() => resolve());
          temp.catch(() => reject());
        } else {
          resolve();
        }
      }
    );

    promise.catch(() => {
      let message = `Cannot find [${this.itfpName}] policy`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.backPage();
      this.store.dispatch({type: ROUTER_LOADED});
      return;
    });

    this.firstFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      description: [''],
      lang: ['', Validators.required],
      default_skill: ['', Validators.required],
    });

    this.secondFormGroup = this._formBuilder.group({
      // 화면에서 쓰이는 변수
      classifyingMethods: [{attr: null, name: null}, Validators.required],
      stepType: '',
      stepRunStyle: '',
      secondRunStyle: '',
      seq: '',
      inputStep: ['',
        [
          Validators.required,
          Validators.pattern(VARIABLE_REGEX),
          () => this.isDuplicatedStep() ? {'duplicated': true} : null
        ],
      ],
      // Proto data
      type: '',
      run_style: [''],
      name: [{value: '', disabled: true},
        [
          Validators.required,
          Validators.pattern(VARIABLE_REGEX)
        ]
      ],
      description: [{value: '', disabled: true}],
      active: true,
      is_hint: false,
      categories: '',
      force_override_skill: false,
      test_rule: '',

      // custom script
      custom_script_mod: this._formBuilder.group({
        filename: '',
        classify_func: '',
        model: ''
      }),

      // pcre
      pcre_mod: this._formBuilder.group({
        model: '',
        lang: '',
      }),

      // hmd-model {
      hmd_mod: this._formBuilder.group({
        model: '',
        lang: ''
      }),

      // dnn
      dnn_cl_mod: this._formBuilder.group({
        model: '',
        lang: '',
        cut_coefficient: [
          0.5,
          this.validateDnnCutCoefficient.bind(this)
        ]
      }),

      // final rule
      final_rule: this._formBuilder.group({
        final_skill: ''
      }),

      step_condition: ''
    });

    this.secondFormGroup.get('run_style').disable();
    this.secondFormGroup.get('classifyingMethods').disable();
    this.secondFormGroup.get('step_condition').disable();

    promise.then(() => {
      // promise가 정상적으로 실행된 이후 동작합니다.
      switch (this.role) {
        case 'add':
          this.dialogTitle = 'Add Intent Finder Policy';
          break;
        case 'edit':
          if (this.itfp === undefined) {
            this.backPage();
            return;
          }
          this.dialogTitle = 'Edit Intent Finder Policy';
          let hmdFlag = false;
          let dnnFlag = false;
          let pcreFlag = false;
          let customScriptFlag = false;
          let finalRuleFlag = false;
          this.steps = this.itfp.steps;
          this.steps.some(step => {
            if (hmdFlag && dnnFlag && pcreFlag && customScriptFlag && customScriptFlag && finalRuleFlag) {
              return true;
            }

            if (step.test_rule === 'hmd_mod' && !hmdFlag) {
              this.getHmds().then(() => {
                hmdFlag = true;
                this.checkModel('hmd_mod');
              })
            } else if (step.test_rule === 'dnn_cl_mod' && !dnnFlag) {
              this.getDnnModels().then(() => {
                dnnFlag = true;
                this.checkModel('dnn_cl_mod');
              });
            } else if (step.test_rule === 'pcre_mod' && !pcreFlag) {
              this.getPcres().then(() => {
                pcreFlag = true;
                this.checkModel('pcre_mod');
              });
            } else if (step.test_rule === 'custom_script_mod' && !customScriptFlag) {
              this.getCustomScriptModels().then(() => {
                customScriptFlag = true;
                this.checkModel('custom_script_mod');
              });
            }
          });
          this.setEditPolicy(this.itfp);
          break;
        default:
          this.backPage();
          return;
      }
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  /**
   * edit중의 step을 실시간 으로 감지하여, style border 속성을 변경한다.
   */
  ngAfterViewInit() {
    // StepList 실시간으로 데이터 감지
    this.stepList.changes.subscribe((r) => {
      r._results.forEach((matListItem) => {
        // Add 후 <Add Step> Input 비 활성화 , Edit Mode 실행 중일 때 if문 실행
        if (this.changeAddInputModeFlag) {
          // Custom Edit Mode (사용자가 수정 버튼을 직접 눌렀을 때 실행)
          if (this.isCustomEditMode) {
            if (matListItem._element.nativeElement.id === this.clickEditElementId) {
              document.getElementById(this.clickEditElementId).style.border = '3px solid orange';
            } else {
              matListItem._element.nativeElement.style.border = 'none';
            }
          } else if (!this.isCustomEditMode) {
            // Flow Edit Mode (Add 후 자동적으로 Edit Mode 실행)
            // 가장 최근에 생성된 step (List의 마지막) 에 border 속성을 준다.
            if (matListItem._element.nativeElement.id === `step${this.theLastStep.key + 1}`) {
              matListItem._element.nativeElement.style.border = '3px solid orange';
            } else {
              matListItem._element.nativeElement.style.border = 'none';
            }
          }
        }
      });
    });
  }

  /**
   * View 화면에서 넘겨받은 IntentFinederPolicy 객체를 화면에 세팅 해준다.
   * @param itfp => {View 화면에서 넘겨 받은, 하나의 IntentFinderPolicy 객체}
   */
  setEditPolicy(itfp: any) {
    // Intent Finder Policy Information Setting
    if (itfp.lang === 'ko_KR') {
      itfp.lang = 'kor';
    } else {
      itfp.lang = 'eng';
    }

    this.firstFormGroup.patchValue({
      lang: itfp.lang,
      default_skill: itfp.default_skill,
      description: itfp.description
    });

    this.firstFormGroup.get('name').setValue(itfp.name);
    this.firstFormGroup.get('name').disable();

    // Intent Finder Policy Steps Setting
    // UI Setting
    this.steps.forEach((step, index) => {
      step['key'] = index + 1;
      this.theLastStep = index;

      // step_condition 값이 없을 경우
      if (!step.hasOwnProperty('step_condition')) {
        step['step_condition'] = '';
      }

      // UI에 Skill, Hint 여부 표시 하기 위하여 세팅
      if (step.run_style === 'IFC_SKILL_FOUND_RETURN'
        || step.run_style === 'IFC_SKILL_FOUND_COLLECT_HINTS') {
        step.stepRunStyle = 'S';
        step.secondRunStyle = step.run_style;
        step.run_style = 'skillRunStyles';
        step.is_hint = false;
      } else if (step.run_style === 'IFC_HINT') {
        step.stepRunStyle = 'H';
        step.secondRunStyle = '';
        step.is_hint = true;
      }

      // UI에 Step에 등록된 Model 정보 표시
      switch (step.test_rule) {
        case 'custom_script_mod':
          step.type = 'CLM_CLASSIFY_SCRIPT';
          step.stepType = `<CustomScript - ${step.custom_script_mod.model}>`;
          delete step.pcre_mod;
          delete step.dnn_cl_mod;
          delete step.hmd_mod;
          delete step.final_rule;
          break;
        case 'pcre_mod':
          step.type = 'CLM_PCRE';
          step.stepType = `<PCRE - ${step.pcre_mod.model}>`;
          delete step.custom_script_mod;
          delete step.dnn_cl_mod;
          delete step.hmd_mod;
          delete step.final_rule;
          break;
        case 'dnn_cl_mod':
          step.type = 'CLM_DNN_CLASSIFIER';
          step.stepType = `<DNN - ${step.dnn_cl_mod.model}>`;
          delete step.custom_script_mod;
          delete step.pcre_mod;
          delete step.hmd_mod;
          delete step.final_rule;
          break;
        case 'hmd_mod':
          step.type = 'CLM_HMD';
          step.stepType = `<HMD - ${step.hmd_mod.model}>`;
          delete step.custom_script_mod;
          delete step.pcre_mod;
          delete step.dnn_cl_mod;
          delete step.final_rule;
          break;
        case 'final_rule':
          step.type = 'CLM_FINAL_RULE';
          step.stepType = `<Final rule>`;
          delete step.custom_script_mod;
          delete step.pcre_mod;
          delete step.dnn_cl_mod;
          delete step.hmd_mod;
          break;
      }
    });

    // UI Setting (seq 설정 && steps 등록 조건 체크)
    this.checkModel();
    this.checkPriority(); // seq 설정
    this.isDone();
  }

  /**
   * ADD, FlowEdit Mode 일떄 UI의 변경을 세팅 해준다.
   * @param able (true : ADD MODE, false: FlowEditMode)
   */
  changeAddInputMode(able: boolean) {
    if (able) {
      // this.currentEditMode = EditMode.NOT;
      this.changeAddInputModeFlag = false;

      this.secondFormGroup.get('inputStep').setValue('');
      this.secondFormGroup.get('inputStep').enable();
      this.secondFormGroup.get('inputStep').setValidators(
        [Validators.required,
          Validators.pattern(VARIABLE_REGEX),
          () => this.isDuplicatedStep() ? {'duplicated': true} : null]);
      this.secondFormGroup.get('name').setValue('');
      this.secondFormGroup.get('name').disable();
      this.secondFormGroup.get('description').setValue('');
      this.secondFormGroup.get('description').disable();
      this.secondFormGroup.get('run_style').setValue('');
      this.secondFormGroup.get('run_style').disable();
      this.secondFormGroup.get('secondRunStyle').disable();
      this.secondFormGroup.get('classifyingMethods').disable();
      this.secondFormGroup.get('step_condition').disable();

      this.stepList.map(step => {
        step._element.nativeElement.style.border = 'none';
      });

    } else {
      // this.currentEditMode = EditMode.FLOW;
      this.changeAddInputModeFlag = true;
      this.secondFormGroup.get('inputStep').setValue('');
      this.secondFormGroup.get('inputStep').disable();
      this.secondFormGroup.get('inputStep').clearValidators();
      this.secondFormGroup.get('name').setValue('');
      this.secondFormGroup.get('name').enable();
      this.secondFormGroup.get('name').setValidators([
        Validators.required,
        Validators.pattern(VARIABLE_REGEX),
        () => this.isDuplicatedStep() ? {'duplicated': true} : null
      ]);
      this.secondFormGroup.get('description').enable();
      this.secondFormGroup.get('run_style').enable();
      this.secondFormGroup.get('run_style').setValue('');
      this.secondFormGroup.get('secondRunStyle').enable();
      this.secondFormGroup.get('classifyingMethods').enable();
      this.secondFormGroup.get('step_condition').enable();
    }
    this.cdr.detectChanges();
  }

  /**
   * step 이름을 매개변수로 넘겨 받아서, step에 default 데이터로 추가한다.
   * add 시 default 데이터
   * @param stepName
   */
  addStep(stepName: string) {
    if (this.isDuplicatedStep()) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Duplicated';
      ref.componentInstance.message = `Step name duplicated.`;
    } else {
      this.theLastStep = {key: 0};

      this.steps.forEach(step => {
        if (step.key > this.theLastStep.key) {
          this.theLastStep = JSON.parse(JSON.stringify(step));
        }
      });

      this.steps.push({name: stepName, key: this.theLastStep.key + 1});
      this.changeAddInputMode(false);
      this.secondFormGroup.patchValue({name: stepName});
      this.clickEditData = this.steps.filter(step => step.key === this.theLastStep.key + 1)[0];
      this.clickEditElementId = `step${this.theLastStep.key + 1}`;
      this.originStepName = stepName;
      // 새로 생긴 step 에 focus 주기
      if (this.stepList.last) {
        this.stepList.last._element.nativeElement.tabIndex = -1;
        this.stepList.last._element.nativeElement.focus();
      }

      this.cdr.detectChanges();
      this.isDuplicatedStep();
      this.checkPriority();
    }
  }

  /**
   * AddMode에서 step을 추가 한다.
   * @param event
   * @param isMouse (true: Icon클릭시 step add, false: Keyboard enter)
   */
  inputAddStep(event, isMouse) {
    if (this.secondFormGroup.get('inputStep').valid) {
      // 마우스를 클릭하였을 때
      if (isMouse) {
        this.addStep(event);
      }

      // 엔터 키 눌렀을 떄
      if (event.keyCode === 13) {
        this.addStep(event.target.value);
        event.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
      }
    }
  }

  /**
   * Model List 정렬 함수
   * @param param => Model List
   * @param type => Model Type
   */
  order(param: any [], type: ModelType): void {
    let list: any[] = param.slice(0);
    if (list.length > 0) {
      switch (type) {
        case ModelType.cs_model:
          list.sort((a, b) => a.model.localeCompare(b.model));
          this.customscript = list;
          break;
        case ModelType.hmd_model:
          list.sort((a, b) => a.model.localeCompare(b.model));
          this.hmds = list;
          break;
        case ModelType.dnn_model:
          list.sort((a, b) => a.model.localeCompare(b.model));
          this.dnns = list;
          break;
        case ModelType.sc_model:
          list.sort((a, b) => a.name.localeCompare(b.name));
          this.pcres = list;
          break;
      }
    }
  }

  /**
   * Intent Finder Policy Language 값이 변화 할시 기존 UI 등록된 데이터 초기화
   */
  changePolicyLang(): void {
    this.steps = [];
    this.selectedMethod = {attr: null, name: null};
    this.secondFormGroup.patchValue({
      classifyingMethods:
        [{attr: null, name: null}, Validators.required]
    });
    this.initData();
    this.pcres = [];
    this.dnns = [];
    this.hmds = [];
    this.customscript = [];
  }

  /**
   * classifyingMethod 별로 다른 리스트를 가져온다.
   */
  getClassifyingMethods(): void {
    this.selectedMethod = this.secondFormGroup.getRawValue().classifyingMethods;
    this.categoryFlag = false;
    this.checkAddStep();

    // 순차적인 editMode 일경우 classifyingMethod 변경시 inputStep의 값으로 세팅 시켜준다.
    if (this.clickEditData && this.changeAddInputModeFlag) {
      this.secondFormGroup.patchValue({name: this.clickEditData.name});
    }

    // 현재 설정되어 있는 classifyingMethods가 아닐 경우, 안에 들어가 있는 값을 제거 한다.
    if (this.clickEditData.hasOwnProperty('classifyingMethods')) {
      switch (this.clickEditData.classifyingMethods.attr) {
        case 'hmd_mod':
        case 'pcre_mod':
          this.secondFormGroup.get('custom_script_mod').patchValue({
            filename: '',
            classify_func: '',
          });
          this.secondFormGroup.get('dnn_cl_mod').patchValue({
            cut_coefficient: 0.5,
          });
          this.secondFormGroup.get('final_rule').patchValue({
            final_skill: ''
          });
          break;
        case 'dnn_cl_mod':
          this.secondFormGroup.get('custom_script_mod').patchValue({
            filename: '',
            classify_func: '',
          });
          this.secondFormGroup.get('final_rule').patchValue({
            final_skill: ''
          });
          break;
        case 'final_rule':
          this.secondFormGroup.get('custom_script_mod').patchValue({
            filename: '',
            classify_func: '',
          });
          this.secondFormGroup.get('dnn_cl_mod').patchValue({
            cut_coefficient: 0.5,
          });
          break;
        case 'custom_script_mod':
          this.secondFormGroup.get('dnn_cl_mod').patchValue({
            cut_coefficient: 0.5,
          });
          this.secondFormGroup.get('final_rule').patchValue({
            final_skill: ''
          });
          break;
      }
    }

    // final_rule 일 때는 무조건 RETURN 타입으로 세팅한다.
    if (this.selectedMethod.attr === 'final_rule') {
      this.secondFormGroup.patchValue({
        run_style: 'skillRunStyles',
        secondRunStyle: 'IFC_SKILL_FOUND_RETURN'
      });
      this.selectedRunStyle = this.secondFormGroup.get('run_style').value;
      this.secondFormGroup.get('run_style').disable();
      this.secondFormGroup.get('secondRunStyle').disable();
    } else {
      if (this.clickEditData.hasOwnProperty('run_style')) {
        this.secondFormGroup.patchValue({
          run_style: this.clickEditData.run_style,
          secondRunStyle: this.clickEditData.secondRunStyle
        });

        if (this.clickEditData.run_style === 'IFC_HINT') {
          this.selectedRunStyle = null;
        }
      }
      this.secondFormGroup.get('run_style').enable();
      this.secondFormGroup.get('secondRunStyle').enable();
    }

    if (this.selectedMethod.attr === 'pcre_mod') {
      if (this.pcreCurrentClickData && this.clickEditData.test_rule !== this.selectedMethod.attr) {
        this.pcreBeforeClickData = undefined;
        this.pcreCurrentClickData.style.backgroundColor = '#ffffff';
        this.pcreCurrentClickData = undefined;
      }
      this.getPcres().then(() => {
        if (this.clickEditData.hasOwnProperty('test_rule')
          && this.clickEditData.test_rule === this.selectedMethod.attr) {
          let _pcre = this.pcres.filter(pcre => pcre.name === this.clickEditData.pcre_mod.model)[0];
          this.getPcreCategories(_pcre);
        }
      });
    } else if (this.selectedMethod.attr === 'hmd_mod') {
      if (this.hmdCurrentClickData && this.clickEditData.test_rule !== this.selectedMethod.attr) {
        this.hmdBeforeClickData = undefined;
        this.hmdCurrentClickData.style.backgroundColor = '#ffffff';
        this.hmdCurrentClickData = undefined;
      }
      this.getHmds().then(() => {
        if (this.clickEditData.hasOwnProperty('test_rule')
          && this.clickEditData.test_rule === this.selectedMethod.attr) {
          let _hmd = this.hmds.filter(hmd => hmd.model === this.clickEditData.hmd_mod.model)[0];
          this.getHmdCategories(_hmd);
        }
      });
    } else if (this.selectedMethod.attr === 'dnn_cl_mod') {
      if (this.dnnCurrentClickData && this.clickEditData.test_rule !== this.selectedMethod.attr) {
        this.dnnBeforeClickData = undefined;
        this.dnnCurrentClickData.style.backgroundColor = '#ffffff';
        this.dnnCurrentClickData = undefined;
      }
      this.getDnnModels().then(() => {
        if (this.clickEditData.hasOwnProperty('test_rule')
          && this.clickEditData.test_rule === this.selectedMethod.attr) {
          let _dnn = this.dnns.filter(dnn => dnn.model === this.clickEditData.dnn_cl_mod.model)[0];
          this.getDnnCategories(_dnn);
        }
      });
    } else if (this.selectedMethod.attr === 'custom_script_mod') {
      if (this.customscriptCurrentClickData && this.clickEditData.test_rule !== this.selectedMethod.attr) {
        this.customscriptBeforeClickData = undefined;
        this.customscriptCurrentClickData.style.backgroundColor = '#ffffff';
        this.customscriptCurrentClickData = undefined;
      }
      this.getCustomScriptModels().then(() => {
        if (this.clickEditData.hasOwnProperty('test_rule')
          && this.clickEditData.test_rule === this.selectedMethod.attr) {
          let _cs = this.customscript.filter(cs => cs.model === this.clickEditData.custom_script_mod.model)[0];
          this.getCustomScriptCategories(_cs);
        }
      });
    } else if (this.selectedMethod.attr === 'final_rule') {
      this.checkModel();
    }
  }

  /**
   *  Rest를 이용하여, customscript Model 리스트를 가져온다.
   */
  getCustomScriptModels(): any {
    let promise = new Promise(resolve => {
      this.grpc.getCustomScriptModels().subscribe(
        res => {
          if (res) {
            if (this.customscript.length === 0) {
              let data = [];

              res.entries.forEach(entry => {
                data.push({model: entry});
              });
              this.customscript = data;
            } else {
              res.entries.forEach(entry => {
                if (!this.customscript.find(cs => cs.model === entry)) {
                  this.customscript.push({model: entry});
                }
              });
            }

            if (this.customscript.filter(cs => cs.hidden === true).length === this.customscript.length) {
              this.allHidden = true;
            } else {
              this.allHidden = false;
            }
            this.order(this.customscript, ModelType.cs_model);
            resolve();
          }
        }, err => {
          this.customscript = [];
          this.openErrorDialog('Failed', `Something went wrong. [${getErrorString(err)}]`);
        });
    });

    return new Promise(resolve => {
      promise.then(() => {
        if (!this.changeAddInputModeFlag) {
          this.checkModel();
        }
        resolve();
      });
    });
  }

  /**
   *  Rest를 이용하여, pcre Model 리스트를 가져온다.
   */
  getPcres(): any {
    let promise = new Promise(resolve => {
      this.grpc.getSimpleClassifierAllList().subscribe(
        res => {
          if (res) {
            if (this.pcres.length === 0) {
              let data = [];

              res['sc_list'].forEach(model => {
                if (model.lang === this.firstFormGroup.getRawValue().lang) {
                  data.push(model);
                }
              });
              this.pcres = data;
            } else {
              res['sc_list'].forEach(model => {
                if (!this.pcres.find(pcre => pcre.name === model.name)) {
                  this.pcres.push(model);
                }
              });
            }

            if (this.pcres.filter(pcre => pcre.hidden === true).length === this.pcres.length) {
              this.allHidden = true;
            } else {
              this.allHidden = false;
            }
            this.order(this.pcres, ModelType.sc_model);
            resolve();
          }
        }, err => {
          this.pcres = [];
          this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
        });
    });

    return new Promise(resolve => {
      promise.then(() => {
        if (!this.changeAddInputModeFlag) {
          this.checkModel();
        }
        resolve();
      });
    });
  }

  /**
   * Rest를 이용하여, Hmd Model 리스트를 가져온다.
   * (Hmd는 Model 리스트를 가져오면, Category 리스트도 같이 담겨있다.)
   */
  getHmds(): any {
    let promise = new Promise(resolve => {
      this.grpc.getHMDModels().subscribe(
        res => {
          if (res) {
            if (this.hmds.length === 0) {
              let data = [];

              res['models'].forEach(model => {
                if (model.lang === this.firstFormGroup.getRawValue().lang) {
                  data.push(model);
                }
              });

              this.hmds = data;
            } else {
              res['models'].forEach(model => {
                if (!this.hmds.find(hmd => hmd.model === model.model)) {
                  this.hmds.push(model);
                }
              });
            }

            if (this.hmds.filter(hmd => hmd.hidden === true).length === this.hmds.length) {
              this.allHidden = true;
            } else {
              this.allHidden = false;
            }
            this.order(this.hmds, ModelType.hmd_model);
            resolve();
          }
        }, err => {
          this.hmds = [];
          this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
        });
    });

    return new Promise(resolve => {
      promise.then(() => {
        if (!this.changeAddInputModeFlag) {
          this.checkModel();
        }
        resolve();
      });
    });
  }

  /**
   * Rest를 이용하여, DNN Model 리스트를 가져온다.
   */
  getDnnModels(): any {
    let promise = new Promise(resolve => {
      this.grpc.getDnnModels().subscribe(
        res => {
          if (res) {
            if (this.dnns.length === 0) {
              let data = [];

              res['models'].forEach(model => {
                if (model.lang === this.firstFormGroup.getRawValue().lang) {
                  data.push(model);
                }
              });

              this.dnns = data;
            } else {
              res['models'].forEach(model => {
                if (!this.dnns.find(dnn => dnn.model === model.model)) {
                  this.dnns.push(model);
                }
              });
            }

            if (this.dnns.filter(dnn => dnn.hidden === true).length === this.dnns.length) {
              this.allHidden = true;
            } else {
              this.allHidden = false;
            }
            this.order(this.dnns, ModelType.dnn_model);
            resolve();
          }
          this.cdr.detectChanges();
        },
        err => {
          this.dnns = [];
          this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
        });
    });

    return new Promise(resolve => {
      promise.then(() => {
        if (!this.changeAddInputModeFlag) {
          this.checkModel();
        }
        resolve();
      });
    });
  }


  /**
   * CustomScript Model 클릭시, 해당 Model의 Categories를 가져온다.
   * @param csModel => {CustomScript Model List 에서 선택한 CustomScript 객체}
   */
  getCustomScriptCategories(csModel: any): any {
    return new Promise(resolve => {
      this.grpc.getCustomScriptCategories(csModel.model).subscribe(
        res => {
          if (res) {
            let categories = res.entries;
            this.secondFormGroup.patchValue({custom_script_mod: {model: csModel.model}});
            this.secondFormGroup.patchValue({custom_script_mod: {filename: `${csModel.model}.py`}});
            this.secondFormGroup.patchValue({categories: categories});
            this.categoryFlag = true;
            this.categories = categories;
          }
          resolve();
        },
        err => {
          this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
        });
    });
  }

  /**
   * PCRE Model 클릭시, 클릭한 Model의 Categories를 가져온다.
   * @param pcre => {PCRE Model List 에서 선택한 PCRE 객체}
   */
  getPcreCategories(pcre: any): void {
    let categories = [];
    pcre['skill_list'].forEach(skill => {
      categories.push(skill.name);
    });

    this.secondFormGroup.patchValue({pcre_mod: {model: pcre.name}});
    this.secondFormGroup.patchValue({categories: categories});
    this.categoryFlag = true;
    this.categories = categories;
  }

  /**
   * HMD Model 클릭시, 클릭한 Model의 Categories를 가져온다.
   * @param hmd => {HMD Model List 에서 선택한 HMD 객체}
   */
  getHmdCategories(hmd: any): void {
    let set = new Set();
    hmd['rules'].forEach(item => {
      let category = item.categories.join('-');
      set.add(category);
    });

    let categories = Array.from(set);
    this.secondFormGroup.patchValue({hmd_mod: {model: hmd.model}});
    this.secondFormGroup.patchValue({categories: categories});
    this.categoryFlag = true;
    this.categories = this.secondFormGroup.getRawValue().categories;
  }

  /**
   * DNN Model 클릭시, 클릭한 Model의 Categories를 가져온다.
   * (DNN은 HMD 와 달리 Rest를 이용하여 Category 정보를 따로 가져와야 한다.)
   * @param dnnModel  => {DNN Model List 에서 선택한 DNN 객체}
   */
  getDnnCategories(dnnModel: any): any {
    let _dnnModel = dnnModel;
    if (dnnModel.hasOwnProperty('hidden')) {
      _dnnModel = {model: dnnModel.model, lang: dnnModel.lang}
    }

    return new Promise(resolve => {
      this.grpc.getDnnCategories(_dnnModel).subscribe(
        res => {
          if (res) {
            this.categories = res.categories;
            if (this.categories.length > 1) {
              let value: number = Number((0.7).toFixed(3));
              this.secondFormGroup.patchValue(
                {
                  dnn_cl_mod:
                    {cut_coefficient: value}
                });
            } else {
              this.secondFormGroup.patchValue({dnn_cl_mod: {cut_coefficient: 0.1}});
            }
            this.secondFormGroup.patchValue({dnn_cl_mod: {model: res['model']}});
            this.secondFormGroup.patchValue({categories: res['categories']});
            this.validateDnnCutCoefficient(this.secondFormGroup.get('dnn_cl_mod')['controls'].cut_coefficient);
            this.secondFormGroup.get('dnn_cl_mod').updateValueAndValidity(
              {emitEvent: false, onlySelf: true}
            );
            this.categoryFlag = true;
            this.checkAddStep();
            resolve();
          }
        },
        err => {
          this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
        });
    });
  }

  /**
   * 변화를 감지하여, step의 seq 값을 세팅한다.
   */
  checkPriority(): void {
    this.steps.forEach((step, index) => {
      step['seq'] = index + 1;
    });
  }

  /**
   * runStyle (radio button으로 구현) 클릭 시 값을 세팅 해준다.
   * runStyle 이 Skill일 경우에, 이중으로 radio button 을 구현하기 위하여 임시로 값을 secondRunStyle에 세팅한다.
   * @param runStyle => {radio button 에서 클릭한 runStyle 정보}
   */
  isSkill(runStyle: any): void {
    switch (runStyle.value) {
      case 'skillRunStyles':
        this.selectedRunStyle = runStyle;
        this.secondFormGroup.patchValue({run_style: 'skillRunStyles', secondRunStyle: ''});
        this.stepAddFlag = true;
        break;
      case 'IFC_SKILL_FOUND_RETURN':
        this.secondFormGroup.patchValue({
          run_style: 'skillRunStyles',
          secondRunStyle: 'IFC_SKILL_FOUND_RETURN'
        });
        this.checkAddStep();
        break;
      case 'IFC_SKILL_FOUND_COLLECT_HINTS':
        this.secondFormGroup.patchValue({
          run_style: 'skillRunStyles',
          secondRunStyle: 'IFC_SKILL_FOUND_COLLECT_HINTS'
        });
        this.checkAddStep();
        break;
      case 'IFC_HINT':
        this.secondFormGroup.patchValue({run_style: 'IFC_HINT', secondRunStyle: ''});
        this.selectedRunStyle = null;
        this.checkAddStep();
        break;
    }
    this.viewSynchronization();
  }

  /**
   * step edit Mode 일 떄 화면을 실시간 적으로 동기화 한다.
   * 실시간 Editing
   */
  viewSynchronization() {
    let value = this.secondFormGroup.getRawValue();

    this.steps.filter((step, index) => {
      if (step.key === this.clickEditData.key) {

        if (value.run_style === 'IFC_HINT') {
          value.stepRunStyle = 'H';
          value.is_hint = true;
          this.selectedRunStyle = null;
        } else if (value.run_style === 'skillRunStyles') {
          value.stepRunStyle = 'S';
          value.is_hint = false;
          this.selectedRunStyle = value.run_style;
        }

        if (this.selectedMethod.attr === 'custom_script_mod') {
          value.type = 'CLM_CLASSIFY_SCRIPT';
          value.stepType = `<CustomScript - ${value.custom_script_mod.model}>`;
          value.test_rule = 'custom_script_mod';

          delete value.pcre_mod;
          delete value.hmd_mod;
          delete value.dnn_cl_mod;
          delete value.final_rule;
        } else if (this.selectedMethod.attr === 'pcre_mod') {
          value.pcre_mod.lang = this.appEnum.lang[this.firstFormGroup.getRawValue().lang];
          value.type = 'CLM_PCRE';
          value.stepType = `<PCRE - ${value.pcre_mod.model}>`;
          value.test_rule = 'pcre_mod';

          delete value.custom_script_mod;
          delete value.hmd_mod;
          delete value.dnn_cl_mod;
          delete value.final_rule;
        } else if (this.selectedMethod.attr === 'hmd_mod') {
          value.hmd_mod.lang = this.appEnum.lang[this.firstFormGroup.getRawValue().lang];
          value.type = 'CLM_HMD';
          value.stepType = `<HMD - ${value.hmd_mod.model}>`;
          value.test_rule = 'hmd_mod';

          delete value.custom_script_mod;
          delete value.pcre_mod;
          delete value.dnn_cl_mod;
          delete value.final_rule;
        } else if (this.selectedMethod.attr === 'dnn_cl_mod') {
          value.dnn_cl_mod.lang = this.appEnum.lang[this.firstFormGroup.getRawValue().lang];
          value.type = 'CLM_DNN_CLASSIFIER';
          value.stepType = `<DNN - ${value.dnn_cl_mod.model}>`;
          value.test_rule = 'dnn_cl_mod';

          delete value.custom_script_mod;
          delete value.pcre_mod;
          delete value.hmd_mod;
          delete value.final_rule;
        } else if (this.selectedMethod.attr === 'final_rule') {
          value.type = 'CLM_FINAL_RULE';
          value.stepType = `<Final rule>`;
          value.test_rule = 'final_rule';
          if (value.final_rule.final_skill.trim() !== '') {
            value.categories = [value.final_rule.final_skill];
          }

          delete value.custom_script_mod;
          delete value.pcre_mod;
          delete value.hmd_mod;
          delete value.dnn_cl_mod;
        }

        value['key'] = step.key;
        this.steps.splice(index, 1, value);
        this.clickEditData = value;
      }
    });
    this.checkAddStep();
  }


  /**
   * Step이 EDIT 될 수 있는 조건을 체크 한다.
   * @param check => this.secondFormGroup.getRawValue();
   */
  checkCondition(check) {
    if (this.selectedMethod.attr === 'custom_script_mod') {
      if (this.secondFormGroup.valid && check.custom_script_mod.filename.trim() !== ''
        && check.custom_script_mod.classify_func.trim() !== '') {
        this.stepAddFlag = false;
      } else {
        this.stepAddFlag = true;
      }
    } else if (this.selectedMethod.attr === 'pcre_mod') {
      if (this.secondFormGroup.valid && this.pcreCurrentClickData !== undefined) {
        this.stepAddFlag = false;
      } else {
        this.stepAddFlag = true;
      }
    } else if (this.selectedMethod.attr === 'hmd_mod') {
      if (this.secondFormGroup.valid && this.hmdCurrentClickData !== undefined) {
        this.stepAddFlag = false;
      } else {
        this.stepAddFlag = true;
      }
    } else if (this.selectedMethod.attr === 'dnn_cl_mod') {
      if (this.secondFormGroup.valid && this.dnnCurrentClickData !== undefined) {
        this.stepAddFlag = false;
      } else {
        this.stepAddFlag = true;
      }
    } else if (this.selectedMethod.attr === 'final_rule') {
      if (this.secondFormGroup.valid && check.final_rule.final_skill !== '') {
        this.stepAddFlag = false;
      } else {
        this.stepAddFlag = true;
      }
    }
  }

  /**
   * step을 Edit할 수 있는 조건을 검사하여, step Edit 버튼의 활성화 유무를 결정한다.
   */
  checkAddStep(): void {
    let check = this.secondFormGroup.getRawValue();

    if (check.run_style.trim() === 'skillRunStyles') {
      if (check.secondRunStyle.trim() !== '') {
        this.checkCondition(check);
      } else {
        this.stepAddFlag = true;
      }
    } else if (check.run_style.trim() === 'IFC_HINT') {
      this.checkCondition(check);
    } else if (check.run_style.trim() === '') {
      this.stepAddFlag = true;
    }

    this.checkPriority();
  }

  /**
   * step 등록시, step list에 중복된 값이 있는지 체크 한다.
   */
  isDuplicatedStep(): any {
    // Add Step 진행 중일 떄
    if (!this.changeAddInputModeFlag) {
      return this.steps.find(step => step.name.trim() === this.secondFormGroup.get('inputStep').value);
    } else {
      // Edit Step 진행 중일 떄
      if (this.secondFormGroup) {
        // let check: boolean = !!this.steps.find(step => step.name.trim() === this.secondFormGroup.get('name').value);
        let check = false;
        this.steps.some(step => {
          if (step.name.trim() === this.secondFormGroup.get('name').value) {
            // 만약 현재 Edit중인 step의 원본 이름과 Step Name Input 입력창에 입력한 데이터랑 같을경우는 duplicated가 아니다.
            if (this.clickEditData) {
              if (this.clickEditData.key === step.key && this.secondFormGroup.get('name').value === this.originStepName) {
                check = false;
                return true;
              }

              // 현재 Edit 중이지 않은 step이 input 데이터와 비교하였을 시에 중복이 났을 경우
              if (this.clickEditData.key !== step.key && this.secondFormGroup.get('name').value === step.name) {
                check = true;
                return true;
              }
            }
          }
        });
        return check;
      } else {
        return false;
      }
    }
  }

  /**
   * Step에 등록된 Model과 비교하여, Model List에서 step에 있는 모델을 hidden 시킨다.
   * @param classifyingMethod
   * (Edit 들어올 시에만 동작 한다. Edit 화면에 들어 올 시에 step이 가지고 있는 classifyingMethod의 Model의
   * Hidden 값을 설정 한다.)
   */
  checkModel(classifyingMethod?): void {
    this.finalRuleFlag = false;
    let data;
    if (classifyingMethod) {
      data = classifyingMethod
    } else {
      data = this.selectedMethod.attr
    }

    switch (data) {
      case 'pcre_mod':
        this.steps.forEach(step => {
          if (step.test_rule === 'pcre_mod') {
            this.pcres.filter(pcre => {
              if (pcre.name === step.pcre_mod.model) {
                pcre['hidden'] = true;
              }
            });

            this.categoryFlag = false;
          }
        });
        this.order(this.pcres, ModelType.sc_model);
        break;
      case 'hmd_mod':
        this.steps.forEach(step => {
          if (step.test_rule === 'hmd_mod') {
            this.hmds.filter(hmd => {
              if (hmd.model === step.hmd_mod.model) {
                hmd['hidden'] = true;
              }
            });
            this.categoryFlag = false;
          }
        });
        this.order(this.hmds, ModelType.hmd_model);
        break;
      case 'dnn_cl_mod':
        this.steps.forEach(step => {
          if (step.test_rule === 'dnn_cl_mod') {
            this.dnns.filter(dnn => {
              if (dnn.model === step.dnn_cl_mod.model) {
                dnn['hidden'] = true;
              }
            });
            this.categoryFlag = false;
          }
        });
        this.order(this.dnns, ModelType.dnn_model);
        break;
      case 'custom_script_mod':
        this.steps.forEach(step => {
          if (step.test_rule === 'custom_script_mod') {
            this.customscript.filter(cs => {
              if (cs.model === step.custom_script_mod.model) {
                cs['hidden'] = true;
              }
            });
            this.categoryFlag = false;
          }
        });
        this.order(this.customscript, ModelType.cs_model);
        break;
      case 'final_rule':
        this.steps.forEach(step => {
          if (step.test_rule === 'final_rule' && this.clickEditData.test_rule !== 'final_rule') {
            this.finalRuleFlag = true;
          }
        });
        break;
    }
  }

  /**
   * Model 의 Hidden 상태를 변경한다.
   * @param step
   * @param isHidden
   */
  updateHiddenState(step, isHidden) {
    switch (step.test_rule) {
      case 'custom_script_mod':
        this.customscript.forEach(cs => {
          document.getElementById(`cs-model-${cs.model}`).style.backgroundColor = '#ffffff';
          if (cs.model === step.custom_script_mod.model) {
            if (isHidden) {
              cs.hidden = true;
            } else {
              cs.hidden = false;
            }
          }
        });
        break;
      case 'pcre_mod':
        this.pcres.forEach(pcre => {
          document.getElementById(`pcre-model-${pcre.name}`).style.backgroundColor = '#ffffff';
          if (pcre.name === step.pcre_mod.model) {
            if (isHidden) {
              pcre.hidden = true;
            } else {
              pcre.hidden = false;
            }
          }
        });
        break;
      case 'dnn_cl_mod':
        this.dnns.forEach(dnn => {
          document.getElementById(`dnn-model-${dnn.model}`).style.backgroundColor = '#ffffff';
          if (dnn.model === step.dnn_cl_mod.model) {
            if (isHidden) {
              dnn.hidden = true;
            } else {
              dnn.hidden = false;
            }
          }
        });
        break;
      case 'hmd_mod':
        this.hmds.forEach(hmd => {
          document.getElementById(`hmd-model-${hmd.model}`).style.backgroundColor = '#ffffff';

          if (hmd.model === step.hmd_mod.model) {
            if (isHidden) {
              hmd.hidden = true;
            } else {
              hmd.hidden = false;
            }
          }
        });
        break;
      case 'final_rule':
        if (isHidden) {
          this.finalRuleFlag = true;
        } else {
          this.finalRuleFlag = false;
        }
        this.categoryFlag = false;
        this.checkModel();
        break;
    }
  }

  /**
   * Edit, Delte시 화면의 변동이 일어날시에 UI의 변경사항을 Update 한다.
   * @param mode => delete 와 edit 두 가지가 존재 한다.
   * @param step
   * @param originStep
   */
  updateModelUi(mode, step, originStep?) {
    let data: any;
    return new Promise(resolve => {
      switch (step.test_rule) {
        case 'custom_script_mod':
          data = step.custom_script_mod;
          this.getCustomScriptModels().then(() => {
            this.customscript.filter(cs => {
              document.getElementById(`cs-model-${cs.model}`).style.backgroundColor = '#ffffff';

              if (mode === 'edit') {
                if (originStep && originStep.hasOwnProperty('custom_script_mod')) {
                  if (cs.model === originStep.custom_script_mod.model) {
                    cs.hidden = true;
                  }
                }
                if (cs.model === step.custom_script_mod.model) {
                  cs.hidden = false;
                  this.clickCustomScriptModel(cs);
                }
              } else if (mode === 'delete') {
                if (originStep) {
                  if (originStep.hasOwnProperty('custom_script_mod')) {
                    // 삭제한 Model 을 화면에 출력한다.
                    if (cs.model === originStep.custom_script_mod.model) {
                      cs.hidden = false;
                    }

                    // 기존에 작업중이던 Model 을 클릭 한 상태로 설정한다.
                    if (cs.model === step.custom_script_mod.model) {
                      cs.hidden = false;
                      this.clickCustomScriptModel(cs);
                    }
                  } else {
                    // 삭제하는 Step 이 새로 클릭한 step 과 다른 classifyingMethod 일 경우
                    this.updateHiddenState(originStep, false);

                    // 기존에 작업중이던 Model 을 클릭 한 상태로 설정한다.
                    if (cs.model === step.custom_script_mod.model) {
                      cs.hidden = false;
                      // 현재 보고 있는 classifyingMethod 값이 선택된 step 의 classifyingMethod 의 값과 같을 떄
                      if (this.selectedMethod.attr === step.test_rule) {
                        this.clickCustomScriptModel(cs);
                      } else {
                        this.clickCustomScriptModel(cs, true);
                      }
                    }
                  }
                } else {
                  // 지금 Edit 중이던 Step 을 삭제 할 때
                  if (cs.model === step.custom_script_mod.model) {
                    cs.hidden = false;
                  }
                }
              }
            });
            resolve(data);
          });
          break;
        case 'pcre_mod':
          data = step.pcre_mod;
          this.getPcres().then(() => {
            this.pcres.filter(pcre => {
              document.getElementById(`pcre-model-${pcre.name}`).style.backgroundColor = '#ffffff';

              if (mode === 'edit') {
                // 기존 수정 중인 Step 과 다른 Step 을 Edit 할 때 && 같은 모델일 때
                if (originStep && originStep.hasOwnProperty('pcre_mod')) {
                  // 사용자가 직접 Edit 아이콘을 클릭 하였을 떄
                  // 다른 Step 으로 Edit Mode 가 바뀔시에 기존 수정 중인 Step 을 ModelList 에서 감춘다.
                  if (pcre.name === originStep.pcre_mod.model) {
                    pcre.hidden = true;
                  }
                }
                // 사용자가 직접 Edit 아이콘을 클릭 하였을 때(처음 단계)
                if (pcre.name === step.pcre_mod.model) {
                  pcre.hidden = false;
                  this.clickPcreModel(pcre);
                }
              } else if (mode === 'delete') {
                // Edit 중 이지 않은 Step 을 삭제 할 때
                if (originStep) {
                  // 삭제하는 Step 이 새로 클릭한 step 과 같은 classifyingMethod 일 경우
                  if (originStep.hasOwnProperty('pcre_mod')) {
                    // 삭제한 Model 을 화면에 출력한다.
                    if (pcre.name === originStep.pcre_mod.model) {
                      pcre.hidden = false;
                    }

                    // 기존에 작업중이던 Model 을 클릭 한 상태로 설정한다.
                    if (pcre.name === step.pcre_mod.model) {
                      pcre.hidden = false;
                      this.clickPcreModel(pcre);
                    }
                  } else {
                    // 삭제하는 Step 이 새로 클릭한 step 과 다른 classifyingMethod 일 경우
                    this.updateHiddenState(originStep, false);

                    // 기존에 작업중이던 Model 을 클릭 한 상태로 설정한다.
                    if (pcre.name === step.pcre_mod.model) {
                      pcre.hidden = false;
                      // 현재 보고 있는 classifyingMethod 값이 선택된 step 의 classifyingMethod 의 값과 같을 떄
                      if (this.selectedMethod.attr === step.test_rule) {
                        this.clickPcreModel(pcre);
                      } else {
                        this.clickPcreModel(pcre, true);
                      }
                    }
                  }
                } else {
                  // 지금 Edit 중이던 Step 을 삭제 할 때
                  if (pcre.name === step.pcre_mod.model) {
                    pcre.hidden = false;
                  }
                }
              }
            });
            resolve(data);
          });
          break;
        case 'dnn_cl_mod':
          data = step.dnn_cl_mod;
          this.getDnnModels().then(() => {
            this.dnns.filter(dnn => {
              document.getElementById(`dnn-model-${dnn.model}`).style.backgroundColor = '#ffffff';

              if (mode === 'edit') {
                if (originStep && originStep.hasOwnProperty('dnn_cl_mod')) {
                  if (dnn.model === originStep.dnn_cl_mod.model) {
                    dnn.hidden = true;
                  }
                }

                if (dnn.model === step.dnn_cl_mod.model) {
                  dnn.hidden = false;
                  this.clickDnnModel(dnn);
                }
              } else if (mode === 'delete') {
                if (originStep) {
                  if (originStep.hasOwnProperty('dnn_cl_mod')) {
                    if (dnn.model === originStep.dnn_cl_mod.model) {
                      dnn.hidden = false;
                    }

                    if (dnn.model === step.dnn_cl_mod.model) {
                      dnn.hidden = false;
                      this.clickDnnModel(dnn);
                    }
                  } else {
                    // 삭제하는 Step이 새로 클릭한 step과 다른 classifyingMethod일 경우
                    this.updateHiddenState(originStep, false);

                    // 기존에 작업중이던 Model을 클릭 한 상태로 설정한다.
                    if (dnn.model === step.dnn_cl_mod.model) {
                      dnn.hidden = false;
                      // 현재 보고 있는 classifyingMethod 값이 선택된 step의 classifyingMethod의 값과 같을 떄
                      if (this.selectedMethod.attr === step.test_rule) {
                        this.clickDnnModel(dnn);
                      } else {
                        this.clickDnnModel(dnn, true);
                      }
                    }
                  }
                } else {
                  if (dnn.model === step.dnn_cl_mod.model) {
                    dnn.hidden = false;
                  }
                }
              }
            });
            resolve(data);
          });
          break;
        case 'hmd_mod':
          data = step.hmd_mod;
          this.getHmds().then(() => {
            this.hmds.filter(hmd => {
              document.getElementById(`hmd-model-${hmd.model}`).style.backgroundColor = '#ffffff';

              if (mode === 'edit') {
                if (originStep && originStep.hasOwnProperty('hmd_mod')) {
                  if (hmd.model === originStep.hmd_mod.model) {
                    hmd.hidden = true;
                  }
                }

                if (hmd.model === step.hmd_mod.model) {
                  hmd.hidden = false;
                  this.clickHmdModel(hmd);
                }
              } else if (mode === 'delete') {
                if (originStep) {
                  if (originStep.hasOwnProperty('hmd_mod')) {
                    if (hmd.model === originStep.hmd_mod.model) {
                      hmd.hidden = false;
                    }

                    if (hmd.model === step.hmd_mod.model) {
                      hmd.hidden = false;
                      this.clickHmdModel(hmd);
                    }
                  } else {
                    this.updateHiddenState(originStep, false);

                    if (hmd.model === step.hmd_mod.model) {
                      hmd.hidden = false;
                      if (this.selectedMethod.attr === step.test_rule) {
                        this.clickHmdModel(hmd);
                      } else {
                        this.clickHmdModel(hmd, true);
                      }
                    }
                  }
                } else {
                  if (hmd.model === step.hmd_mod.model) {
                    hmd.hidden = false;
                  }
                }
              }
            });
            resolve(data);
          });
          break;
        case 'final_rule':
          data = step.final_rule;
          if (mode === 'edit') {
            this.finalRuleFlag = false;
          }

          this.categoryFlag = false;
          resolve(data);
          break;
      }
    });
  }

  /**
   * step edit 아이콘 클릭시, Data를 화면에 세팅한다.
   * @param step => 현재 클릭한 step 객체
   * @param stepElementId => 현재 클릭한 step의 HTML nativeElement id
   */
  changeEditMode(step, stepElementId): void {
    // this.currentEditMode = EditMode.CUSTOM;
    this.isCustomEditMode = true;
    // custom edit Mode 실행 중 일 때 작업중인 데이터가 있을 시에
    if (this.clickEditElementId) {
      this.editCancel(false, step, stepElementId);
    } else {
      this.clickEditData = JSON.parse(JSON.stringify(step));
      let _originStep = this.originStep;
      this.originStep = JSON.parse(JSON.stringify(step));
      this.originStepName = step.name;
      this.clickEditElementId = stepElementId;
      // 처음에 step element를 수정한다. 이후에 element 이벤트 감지는 ngAfterViewInit에서 한다.
      document.getElementById(stepElementId).style.border = '3px solid orange';
      this.changeAddInputMode(false);

      this.classifyingMethods.filter(clm => {
        if (clm.attr === step.test_rule) {
          this.selectedMethod = {
            attr: clm.attr,
            name: clm.name
          };

          this.updateModelUi('edit', step, _originStep).then((data) => {
            this.allHidden = false;
            this.cdr.detectChanges();
            this.secondFormGroup.patchValue({
              classifyingMethods: clm,
              active: step.active,
              categories: step.categories,
              description: step.description,
              force_override_skill: step.force_override_skill,
              is_hint: step.is_hint,
              key: step.key,
              name: step.name,
              [step.test_rule]: data,
              seq: step.seq,
              stepRunStyle: step.stepRunStyle,
              stepType: step.stepType,
              test_rule: step.test_rule,
              type: step.type,
              step_condition: step.step_condition
            });

            // run style에 따라 다르게 데이터 세팅
            if (step.run_style === 'IFC_HINT') {
              this.secondFormGroup.patchValue({run_style: step.run_style, secondRunStyle: ''});
            } else if (step.run_style === 'skillRunStyles') {
              this.selectedRunStyle = this.skillRunStyles.filter(_secondRunStyle =>
                _secondRunStyle.value === step.secondRunStyle)[0];
              this.secondFormGroup.patchValue({
                run_style: 'skillRunStyles',
                secondRunStyle: this.selectedRunStyle.value
              });
            }

            if (this.selectedMethod.attr === 'final_rule') {
              this.secondFormGroup.get('run_style').disable();
              this.secondFormGroup.get('secondRunStyle').disable();
            } else {
              this.secondFormGroup.get('run_style').enable();
              this.secondFormGroup.get('secondRunStyle').enable();
            }

            // dnn_cl_mod 일 경우 cut_coefficient데이터 세팅
            if (step.test_rule === 'dnn_cl_mod') {
              this.secondFormGroup.patchValue({cut_coefficient: step.cut_coefficient});
            }
            this.viewSynchronization();
          });
        }
      });

      this.checkPriority();
    }
  }

  /**
   * Step list 에서 step 을 제거한다.
   * @param step => {delete icon 을 클릭하였을 때, 해당 step의 정보}
   */
  deleteStep(step: any): void {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.title = 'Delete';
    ref.componentInstance.message = `Do you want delete ${step.name} ?`;

    ref.afterClosed().subscribe(result => {
      if (result) {
        // 공통 로직
        // (Not Edit Mode) Add Step 시에 delete 버튼 클릭 하였을 떄는 step List에서 단순 삭제만 한다.
        this.steps.splice(this.steps.indexOf(step), 1);

        // Edit 중 일때 Step을 삭제 할 때
        if (this.changeAddInputModeFlag) {
          // 2. (Edit Mode) 현재 Edit 중인 Step을 삭제할 때
          if (step.key === this.clickEditData.key) {
            this.updateModelUi('delete', step);
            this.afterEditMode();
          } else {
            // 3. (Edit Mode) 현재 Edit 중이지 않은 Step을 삭제할 때
            this.updateModelUi('delete', this.clickEditData, step);
            this.checkModel();
          }
        } else {
          this.updateModelUi('delete', step);
          this.afterEditMode();
        }

        this.checkPriority();
        this.isDone();
      }
    });
  }

  /**
   * CustomScript Model 클릭 시, 클릭한 CustomScript Model의 색상을 변경한다.
   * 색상 변경 후, 해당 Model의 Category를 가져온다.
   * @param cs => {CustomScript Model List에서 클릭한 CustomScript Model 정보}
   * @param isNotViewSync => 화면 동기화 유무
   */
  clickCustomScriptModel(cs: any, isNotViewSync?): void {
    this.customscriptBeforeClickData = this.customscriptCurrentClickData;
    if (this.customscriptBeforeClickData !== undefined) {
      this.customscriptBeforeClickData.style.backgroundColor = '#ffffff';
    }
    this.customscriptCurrentClickData = <HTMLInputElement>document.getElementById(`cs-model-${cs.model}`);
    this.customscriptCurrentClickData.style.backgroundColor = '#c1d9ff';
    this.checkAddStep();
    this.getCustomScriptCategories(cs).then(() => {
      if (!isNotViewSync) {
        this.viewSynchronization();
      } else {
        this.categoryFlag = false;
      }
    });
  }

  /**
   * Pcre Model 클릭 시, 클릭한 Pcre Model의 색상을 변경한다.
   * 색상 변경 후, 해당 Model의 Category를 가져온다.
   * @param pcre => {PCRE Model List에서 클릭한 PCRE Model 정보}
   * @param isNotViewSync => {화면 동기화 유무}
   */
  clickPcreModel(pcre: any, isNotViewSync?): void {
    this.pcreBeforeClickData = this.pcreCurrentClickData;
    if (this.pcreBeforeClickData !== undefined && this.pcreBeforeClickData !== null) {
      this.pcreBeforeClickData.style.backgroundColor = '#ffffff';
    }
    this.pcreCurrentClickData = <HTMLInputElement>document.getElementById(`pcre-model-${pcre.name}`);
    this.pcreCurrentClickData.style.backgroundColor = '#c1d9ff';
    this.checkAddStep();
    this.getPcreCategories(pcre);
    if (!isNotViewSync) {
      this.viewSynchronization();
    } else {
      this.categoryFlag = false;
    }
  }

  /**
   * HMD Model 클릭 시, 클릭한 HMD Model의 색상을 변경한다.
   * 색상 변경 후, 해당 Model의 Category를 가져온다.
   * @param hmd => {HMD Model List에서 클릭한 HMD Model 정보}
   * @param isNotViewSync => {화면 동기화 유무}
   */
  clickHmdModel(hmd: any, isNotViewSync?): void {
    this.hmdBeforeClickData = this.hmdCurrentClickData;
    if (this.hmdBeforeClickData !== undefined && this.hmdBeforeClickData !== null) {
      this.hmdBeforeClickData.style.backgroundColor = '#ffffff';
    }
    this.hmdCurrentClickData = <HTMLInputElement>document.getElementById(`hmd-model-${hmd.model}`);
    this.hmdCurrentClickData.style.backgroundColor = '#c1d9ff';
    this.checkAddStep();
    this.getHmdCategories(hmd);
    if (!isNotViewSync) {
      this.viewSynchronization();
    } else {
      this.categoryFlag = false;
    }
  }

  /**
   * DNN Model 클릭 시, 클릭한 DNN Model의 색상을 변경한다.
   * 색상 변경 후, 해당 Model의 Category를 가져온다.
   * @param dnn => {DNN Model List에서 클릭한 HMD Model 정보}
   * @param dnn
   * @param isNotViewSync => 화면 동기화 유무
   */
  clickDnnModel(dnn: any, isNotViewSync?): void {
    this.dnnBeforeClickData = this.dnnCurrentClickData;
    if (this.dnnBeforeClickData !== undefined && this.dnnBeforeClickData !== null) {
      this.dnnBeforeClickData.style.backgroundColor = '#ffffff';
    }
    this.dnnCurrentClickData = <HTMLInputElement>document.getElementById(`dnn-model-${dnn.model}`);
    this.dnnCurrentClickData.style.backgroundColor = '#c1d9ff';
    this.checkAddStep();
    this.getDnnCategories(dnn).then(() => {
      if (!isNotViewSync) {
        this.viewSynchronization();
      } else {
        this.categoryFlag = false;
      }
    });
  }

  // navigetPcreDetail(pcre) {
  // this.objectManager.set('sc', pcre);
  // this.router.navigateByUrl('m2u/dialog-service/sc-detail');
  // }

  /**
   * 화면에서 Edit Step 버튼을 클릭할 떄 실행되는 함수 이다.
   */
  editStep(): void {
    if (this.isDuplicatedStep()) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Duplicated';
      ref.componentInstance.message = `Step name duplicated.`;
    } else {
      this.afterEditMode();
    }
  }

  /**
   * Edit Step이 끝난 후 기존에 사용한 데이터를 초기화 한다.
   */
  afterEditMode() {
    this.checkPriority(); // seq 설정
    this.checkModel(); // step 등록 시 UI 변경
    this.initData(); // 등록 데이터 초기화
    this.checkAddStep(); // step 등록 시, 유효성 체크
    this.isDone(); // steps 등록 조건 체크
    this.changeAddInputMode(true);

    this.isCustomEditMode = false;
    this.selectedMethod = {attr: null, name: null};
    this.secondFormGroup.patchValue({
      classifyingMethods:
        [{attr: null, name: null}, Validators.required]
    });

    this.originStepName = null;
    this.originStep = null;
    this.clickEditData = null;
    this.clickEditElementId = null;
  }

  /**
   * Edit 창에서 다른 Step을 클릭시 실행 되거나 Edit Step의 Cancel 버튼을 클릭하였을시에 실행되는 함수 이다.
   * @param isCancel
   * @param step
   * @param stepElementId
   */
  editCancel(isCancel, step?, stepElementId?) {
    const ref = this.dialog.open(ConfirmComponent);

    // 만약에 step이 존재하면 다른 step을 클릭하기 전 수정한 원본 데이터를 저장한다.
    if (step) {
      this.clickEditData = JSON.parse(JSON.stringify(step));
      ref.componentInstance.title = 'Change Edit';
      ref.componentInstance.message = `Do you want change edit step ? <br>
      *Previous edits will not be changed.`;
    } else {
      ref.componentInstance.title = 'Cancel';
      ref.componentInstance.message = `Do you want cancel step ? <br>
      *Previous edits will not be changed.`;
    }

    ref.afterClosed().subscribe(result => {
      if (result) {
        // 사용자 클릭 EditMode 일 떄
        if (this.isCustomEditMode) {
          this.isCustomEditMode = false;
          // 1. 기존 데이터 원본 데이터로 복구
          // 완성된 Step에서 다른 Step으로 변경 시도 할 떄
          if (this.originStep) {
            this.steps.forEach((value, index) => {
              if (value.key === this.originStep.key) {
                this.steps.splice(index, 1, JSON.parse(JSON.stringify(this.originStep)));
              }
            });
          } else {
            // 완성되지 않은 Step에서 다른 Step으로 변경 시도 할 떄 step에서 리스트를 삭제한다.
            this.steps.splice(this.steps.indexOf(value => value.key === step.key), 1);
          }

          // Cancel 버튼을 눌렀을 때
          if (isCancel) {
            this.afterEditMode();
          } else {
            // 다른 Edit 아이콘을 클릭하였을 때
            this.clickEditElementId = null;

            // 기존의 가지고 있던 Step의 Model 을 화면에서 제거 한다.
            if (this.originStep) {
              this.updateHiddenState(this.originStep, true);
            }

            this.changeEditMode(step, stepElementId);
          }
        }
      } else {
        // 만약에 취소 버튼을 누르면 현재 Edit중인 데이터를 수정한다.
        this.clickEditData = JSON.parse(JSON.stringify(this.originStep));
      }
    });
  }

  /**
   * 다른 ui에 영향을 끼치지 않기 위하여, 기존 ui에 사용하였던 변수들을 초기화 한다.
   */
  initData(): void {
    if (this.customscriptCurrentClickData) {
      this.customscriptBeforeClickData = undefined;
      this.customscriptCurrentClickData.style.backgroundColor = '#ffffff';
      this.customscriptCurrentClickData = undefined;
    }

    if (this.pcreCurrentClickData) {
      this.pcreBeforeClickData = undefined;
      this.pcreCurrentClickData.style.backgroundColor = '#ffffff';
      this.pcreCurrentClickData = undefined;
    }

    if (this.dnnCurrentClickData) {
      this.dnnBeforeClickData = undefined;
      this.dnnCurrentClickData.style.backgroundColor = '#ffffff';
      this.dnnCurrentClickData = undefined;
    }

    if (this.hmdCurrentClickData) {
      this.hmdBeforeClickData = undefined;
      this.hmdCurrentClickData.style.backgroundColor = '#ffffff';
      this.hmdCurrentClickData = undefined;
    }

    this.selectedRunStyle = null;
    this.categoryFlag = false;
    this.categories = [];
    this.secondFormGroup.patchValue({
      secondRunStyle: '',
      run_style: '',
      categories: '',
      name: '',
      description: '',
      test_rule: '',
      final_rule: {final_skill: ''},
      hmd_mod: {model: '', lang: ''},
      dnn_cl_mod: {model: '', lang: '', cut_coefficient: undefined},
      pcre_mod: {model: '', lang: ''},
      custom_script_mod: {
        model: '', filename: '', classify_func: ''
      },
      step_condition: ''
    });
    this.validateDnnCutCoefficient(this.secondFormGroup.get('dnn_cl_mod')['controls'].cut_coefficient);
    this.secondFormGroup.get('dnn_cl_mod').updateValueAndValidity(
      {emitEvent: false, onlySelf: true}
    );
  }


  /**
   * step 을 서버에 등록할 수 있는 조건이 맞는지 체크한다.
   * 적어도 step 이 1개 이상이 되어야 하며, Edit Mode에서는 Done 버튼이 활성화 되지 않는다.
   */
  isDone(): boolean {
    return (this.steps.length === 0 || this.changeAddInputModeFlag);
  }

  /**
   * role 에 따라 add, edit 호출
   */
  submit(): void {
    if (this.role === 'add') {
      this.openDialog('add');
    } else {
      this.openDialog('edit');
    }
  }

  /**
   * intent-finder-policy를 서버에 등록 요청 한다.
   */
  onAdd(): void {
    let param = JSON.parse(JSON.stringify(Object.assign({}, this.firstFormGroup.getRawValue(), {steps: this.steps})));
    param.lang = this.appEnum.lang[param.lang];
    param.steps.forEach(step => {
      delete step.stepType;
      delete step.stepRunStyle;
      delete step.seq;
      delete step.inputStep;
      delete step.key;
      delete step.classifyingMethods;

      if (step.run_style === 'IFC_HINT') {
        delete step.secondRunStyle;
      } else {
        step.run_style = step.secondRunStyle;
        delete step.secondRunStyle;
      }
    });

    // Intent finder policy name 중복 체크
    let promise = new Promise((resolve, reject) => {
      this.grpc.getIntentFinderPolicyAllList().subscribe(res => {
        if (res.itf_policy_list.map(item => item.name).indexOf(param.name) !== -1) {
          reject();
        } else {
          resolve();
        }
      });
    });

    promise.then(() => {
      this.grpc.insertIntentFinderPolicyInfo(param).subscribe(
        res => {
          this.snackBar.open(`Intent Finder Policy '${param.name}' created.`, 'Confirm', {duration: 3000});
          this.backPage();
        },
        err => {
          this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
        });
    }, () => {
      this.openErrorDialog('Failed', `Intent finder policy name Duplicated`);
    });
  };

  /**
   * intent-finder-policy를 서버에 수정 요청 한다.
   */
  onEdit(): void {
    let param = JSON.parse(JSON.stringify(Object.assign({}, this.firstFormGroup.getRawValue(), {steps: this.steps})));
    param.lang = this.appEnum.lang[param.lang];
    param.steps.forEach(step => {
      delete step.stepType;
      delete step.stepRunStyle;
      delete step.seq;
      delete step.inputStep;
      delete step.key;
      delete step.classifyingMethods;
      if (step.run_style === 'IFC_HINT') {
        delete step.secondRunStyle;
      } else {
        step.run_style = step.secondRunStyle;
        delete step.secondRunStyle;
      }
    });

    this.grpc.updateIntentFinderPolicyInfo(param).subscribe(
      res => {
        this.snackBar.open(`Intent Finder Policy '${param.name}' updated.`, 'Confirm', {duration: 3000});
        // this.router.navigate(['../itfp'], {relativeTo: this.route});

        this.router.navigate(['../'], {relativeTo: this.route});
      },
      err => {
        this.openErrorDialog('Failed', `Something went wrong.  [${getErrorString(err)}]`);
      });
  }

  /**
   * upsert를 계속 진행 할 것 인지 한 번 확인을 거친 이 후에, 확인이 완료 되면 upsert를 진행한다.
   * @param data => add, edit 둘 중 하나의 값
   */
  openDialog(data: string): void {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.firstFormGroup.getRawValue().name}?'`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  /**
   * dialog창으로 error 메세지를 보여준다.
   * @param {string} title  => {Error 창에서의 타이틀}
   * @param {string} message  => {Error 창에서의 에러 메세지}
   */
  openErrorDialog(title: string, message: string): void {
    let ref = this.dialog.open(ErrorDialogComponent);
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * Intent Finder Policy View 화면으로 이동한다.
   */
  backPage: any = (): void => {
    this.objectManager.clean('itfp');
    // this.router.navigate(['../itfp'], {relativeTo: this.route});
    // #@ itfp 화면으로 이동합니다
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  /**
   * itf policy name으로 정보가져오기
   * @returns {Promise<any>}
   */
  getIntentFinderPolicyInfo() {
    return new Promise((resolve, reject) => {
      this.grpc.getIntentFinderPolicyInfo(this.itfpName).subscribe(res => {
        if (res) {
          this.itfp = res;
          this.objectManager.set('itfp', this.itfp);
          resolve();
        } else {
          reject();
        }
      }, err => {
        reject();
      });
    });
  }
}
