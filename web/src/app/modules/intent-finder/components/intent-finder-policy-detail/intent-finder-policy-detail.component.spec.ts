import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderPolicyDetailComponent} from './intent-finder-policy-detail.component';

describe('IntentFinderPolicyDetailComponent', () => {
  let component: IntentFinderPolicyDetailComponent;
  let fixture: ComponentFixture<IntentFinderPolicyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderPolicyDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderPolicyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
