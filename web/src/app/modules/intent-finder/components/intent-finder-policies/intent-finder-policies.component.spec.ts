import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderPoliciesComponent} from './intent-finder-policies.component';

describe('IntentFinderPoliciesComponent', () => {
  let component: IntentFinderPoliciesComponent;
  let fixture: ComponentFixture<IntentFinderPoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderPoliciesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderPoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
