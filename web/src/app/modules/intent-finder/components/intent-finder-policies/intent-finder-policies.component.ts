import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {
  Component, OnInit, ViewChild, TemplateRef,
  ChangeDetectorRef, AfterViewInit, OnDestroy, ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {
  MatDialog,
  MatSnackBar,
  MatSidenav,
  MatTableDataSource,
} from '@angular/material';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {ROUTER_LOADED} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {
  AppObjectManagerService,
  ErrorDialogComponent,
  ConfirmComponent,
  TableComponent
} from '../../../../shared';
import {ConsoleUserApi} from '../../../../core/sdk';
import {GrpcApi} from '../../../../core/sdk/services/custom/Grpc';
import {getErrorString} from '../../../../shared/values/error-string';


@Component({
  selector: 'app-intent-finder-policy-view',
  templateUrl: './intent-finder-policies.component.html',
  styleUrls: ['./intent-finder-policies.component.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 * IntentFiderPolicy View 화면 (조회, 삭제, navigate Edit, Detail)
 */
export class IntentFinderPoliciesComponent implements OnInit, OnDestroy, AfterViewInit {

  actions: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  // @ViewChild('sidenav') sidenav: MatSidenav;
  // @ViewChild('infoPanel') infoPanel: IntentFinderPolicyPanelComponent;

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header: any[] = [];

  constructor(private store: Store<any>,
              private fb: FormBuilder,
              private cdr: ChangeDetectorRef,
              private router: Router,
              private dialog: MatDialog,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private snackBar: MatSnackBar,
              private grpc: GrpcApi,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    let id = 'itf';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
    };

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {attr: 'name', name: 'Name', isSort: true, onClick: this.navigateDetailITFPolicy},
      {attr: 'lang', name: 'Lang', onlyImage: this.getImageUrl},
      {attr: 'steps.length', name: 'Steps', isSort: true},
      {attr: 'default_skill', name: 'Default Skill', isSort: true}
    ];
    this.whenCheckChanged();
    // this.tableComponent.onRowClick = this.fillRowInfo;

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /**
   * IntentFinderPolicy 의 전체 리스트를 가져온다.
   * @returns {Promise<any>} => IntentFinderPolicy의 전체 리스트(json)
   */
  getIntentFinderPolicyAllList() {
    return new Promise(resolve => {
      this.grpc.getIntentFinderPolicyAllList().subscribe(
        res => {
          if (res) {
            res.itf_policy_list.forEach(itfPolicy => {
              itfPolicy.steps['length'] = itfPolicy.steps.length;
            });
            this.tableComponent.isCheckedAll = false;
            // 초기 name 순으로 정렬
            res.itf_policy_list.sort((a, b) => {
              return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
            });

            // console.log('#@ res.itf_policy_list :', res.itf_policy_list);
            this.rows = res.itf_policy_list;
            // console.log('#@ this.rows :', this.rows);
            resolve();
          }
        },
        err => {
          this.rows = [];
          let eref = this.dialog.open(ErrorDialogComponent);
          eref.componentInstance.title = 'Failed';
          eref.componentInstance.message = `Failed to get Intent Finder Policies.  [${getErrorString(err)}]`;
        });
      this.cdr.detectChanges();
    });
  }

  /**
   * IntentFinderPolicy 전체 리스트를 가져오고 난 후, Panel에 첫번째 IntentFinderPolicy 정보를 보여준다.
   */
  ngAfterViewInit() {
    this.getIntentFinderPolicyAllList().then(() => {
      if (this.rows.length > 0) {
        // this.fillRowInfo(this.rows[0]);
      }
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * Panel Open 유무를 설정한다.
   */
  // togglePanel: any = () => {
  //   this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
  //   this.sidenav.toggle();
  // };

  /**
   * Side Panel Open 전에, Steps의  ClaasifiredMethod 종류별로 Length를 세팅 해준다.
   * @param row {테이블에서 클릭한 IntentFinderPolicy json 객체}
   */
  setStepsLength(row: any) {
    // 세팅 시 새로운 변수 할당 및 초기화
    row['customLength'] = 0;
    row['pcreLength'] = 0;
    row['hmdLength'] = 0;
    row['dnnLength'] = 0;
    row['finalLength'] = 0;

    // ClassifiredMethod 종류별로 count를 + 해준다.
    row.steps.forEach(step => {
      switch (step.test_rule) {
        case 'custom_script_mod':
          row['customLength'] += 1;
          break;
        case 'pcre_mod':
          row['pcreLength'] += 1;
          break;
        case 'hmd_mod':
          row['hmdLength'] += 1;
          break;
        case 'dnn_cl_mod':
          row['dnnLength'] += 1;
          break;
        case 'final_rule':
          row['finalLength'] = 1;
          break;
      }
    });
  }

  /**
   * 선택한 테이블 row의 정보를 Panel에 세팅한다.
   * row가 없을 때는 기본적으로 Panel을 Close 하고 , row 1개 이상일 때 Panel을 Open 한다.
   * Panel Open 전에 setStepsLength(row) 함수를 호출하여 ClassifiredMethod별 Length를 세팅해준다.
   * @param row {테이블에서 클릭한 IntentFinderPolicy json 객체}
   */
  // fillRowInfo: any = (row: any) => {
  //   this.setStepsLength(row);
  //   this.infoPanel.itfpInfo = row;
  //   if (row === undefined) {
  //     this.sidenav.close();
  //     this.panelToggleText = 'Show Info Panel';
  //   } else {
  //     this.sidenav.open();
  //     this.panelToggleText = 'Hide Info Panel';
  //   }
  // };

  /**
   * 테이블에서 체크한 row의 개수에 따라, add, delete, delete disable 상태를 변경한다.
   */
  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        break;
    }
  };

  /**
   * add 버튼 클릭시 발생하는 함수
   * add 화면으로 이동한다.
   */
  add: any = () => {
    this.objectManager.clean('itfp');
    // this.router.navigateByUrl('m2u/dialog-service/itfp-upsert#add');
    // #@ itfp 신규 생성 페이지로 이동합니다
    this.router.navigate(['new'], {relativeTo: this.route});
  };

  /**
   * IntentFinderPolicy Edit 화면으로 이동 한다.
   * 테이블에서 checkbox에서 선택한 IntentFinderPolicy 정보를 objectManager에 set 한다.
   */
  edit: any = () => {
    let item = this.rows.filter(row => row.isChecked);
    this.objectManager.set('itfp', item[0]);
    // this.router.navigateByUrl('m2u/dialog-service/itfp-upsert#edit');
    // #@  itfp 수정페이지로 이동합니다
    this.router.navigate([item[0].name, 'edit'], {relativeTo: this.route});
  };

  /**
   * 테이블의 체크박스를 클릭한 후, Delete 버튼을 클릭 시 수행하는 함수 이다.
   * 복수개의 row를 삭제 할 수 있다.
   */
  delete: any = () => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.name);

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      } else {
        let keyList = names.map(key => {
          return {name: key};
        });
        this.grpc.deleteIntentFinderPolicies(keyList).subscribe(
          res => {
            this.snackBar.open(`Selected Intent Finder Policies Successfully Deleted.`,
              'Confirm', {duration: 3000});
            this.getIntentFinderPolicyAllList().then(() => {
              this.whenCheckChanged();
            });
          },
          err => {
            let eref = this.dialog.open(ErrorDialogComponent);
            eref.componentInstance.title = 'Failed';
            eref.componentInstance.message = `Failed to Delete a Intent Finder Policies. [${getErrorString(err)}]`;
            return;
          });
      }
    });
  };

  /**
   * 테이블의 row에서 lang에 해당하는 것들을 이미지로 보여준다.
   * @param row => {IntentFinderPolicy json 객체}
   * @returns {any} => {image가 있는 url 경로 리턴}
   */
  getImageUrl: any = (row: any) => {
    if (row.lang === 'ko_KR') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  };

  /**
   * 테이블의 row에서 name을 클릭시 IntentFinderPolicy Detail 화면으로 이동한다.
   * @param row => {테이블에서 클릭한 IntentFinderPolicy json 객체}
   */
  navigateDetailITFPolicy: any = (row: any) => {
    console.log('#@ itfp row :', row);
    this.objectManager.set('itfp', row);
    // this.router.navigateByUrl('m2u/dialog-service/itfp-detail');
    // #@ itfp detail 화면으로 이동합니다
    this.router.navigate([row.name], {relativeTo: this.route});
  };

  /**
   * 불필요한 메모리 사용을 방지 하기위해, unsubscribe 해줌
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
