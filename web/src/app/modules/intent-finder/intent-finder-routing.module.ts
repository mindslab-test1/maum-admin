import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IntentFinderPoliciesComponent} from './components/intent-finder-policies/intent-finder-policies.component';
import {IntentFinderPolicyDetailComponent} from './components/intent-finder-policy-detail/intent-finder-policy-detail.component';
import {IntentFinderPolicyUpsertComponent} from './components/intent-finder-policy-upsert/intent-finder-policy-upsert.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: IntentFinderPoliciesComponent,
      },
      {
        path: 'new',
        component: IntentFinderPolicyUpsertComponent
      },
      {
        path: ':id',
        component: IntentFinderPolicyDetailComponent
      },
      {
        path: ':id/edit',
        component: IntentFinderPolicyUpsertComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntentFinderRoutingModule {
}
