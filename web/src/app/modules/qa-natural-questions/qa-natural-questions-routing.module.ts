import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NqaCategoriesComponent} from './components/nqa-categories/nqa-categories.component';
import {NqaCategoryUpsertComponent} from './components/nqa-category-upsert/nqa-category-upsert.component';
import {QaSetDetailComponent} from './components/qa-set-detail/qa-set-detail.component';
import {QaSetsComponent} from './components/qa-sets/qa-sets.component';
import {QaSetIndexingComponent} from './components/qa-set-indexing/qa-set-indexing.component';
import {QaSetUpsertComponent} from './components/qa-set-upsert/qa-set-upsert.component';
import {NqaChannelsComponent} from './components/nqa-channels/nqa-channels.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'qa-sets',
      },
      {
        path: 'channels',
        component: NqaChannelsComponent,
      },
      {
        path: 'add',
        component: NqaCategoryUpsertComponent,
      },
      {
        path: 'qa-sets',
        component: QaSetsComponent,
      },
      {
        path: 'qa-sets/:categoryId/new',
        component: QaSetUpsertComponent
      },
      {
        path: 'qa-sets/:categoryId/update/:id/:copyId',
        component: QaSetUpsertComponent
      },
      {
        path: 'qa-sets/:id',
        component: QaSetDetailComponent
      },
      {
        path: 'qa-sets/:id/update',
        component: QaSetUpsertComponent
      },
      {
        path: 'indexing',
        component: QaSetIndexingComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QaNaturalQuestionsRoutingModule {
}
