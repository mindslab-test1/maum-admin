import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {QaNaturalQuestionsRoutingModule} from './qa-natural-questions-routing.module';
import {QaSetsComponent} from './components/qa-sets/qa-sets.component';
import {QaSetUpsertComponent} from './components/qa-set-upsert/qa-set-upsert.component';
import {QaSetDetailComponent} from './components/qa-set-detail/qa-set-detail.component';
import {QaSetIndexingComponent} from './components/qa-set-indexing/qa-set-indexing.component';
import {NqaCategoriesComponent} from './components/nqa-categories/nqa-categories.component';
import {NqaCategoryUpsertComponent} from './components/nqa-category-upsert/nqa-category-upsert.component';
import {SharedModule} from '../../shared/shared.module';
import {AgGridModule} from 'ag-grid-angular';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressBarModule,
  MatSelectModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {CategoryService} from './services/category/category.service';
import {QaSetService} from './services/qa-set/qa-set.service';
import {IndexingService} from './services/indexing/indexing.service';
import {NqaChannelsComponent} from './components/nqa-channels/nqa-channels.component';
import { NqaChannelUpsertComponent } from './components/nqa-channel-upsert/nqa-channel-upsert.component';
import {ChannelService} from './services/channel/channel.service';

@NgModule({
  declarations: [
    QaSetsComponent,
    QaSetUpsertComponent,
    QaSetDetailComponent,
    QaSetIndexingComponent,
    NqaCategoriesComponent,
    NqaCategoryUpsertComponent,
    NqaChannelsComponent,
    NqaChannelUpsertComponent],
  imports: [
    CommonModule,
    SharedModule,
    QaNaturalQuestionsRoutingModule,
    AgGridModule.withComponents([]),
    MatChipsModule,
    MatInputModule,
    MatIconModule,
    MatTabsModule,
    MatListModule,
    MatButtonModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatCardModule,
    MatSelectModule,
    MatProgressBarModule,
  ],
  providers: [
    ChannelService,
    CategoryService,
    QaSetService,
    IndexingService
  ],
  exports: [
    QaSetsComponent
  ],
  entryComponents: [
    NqaCategoryUpsertComponent,
    NqaChannelUpsertComponent
  ]
})
export class QaNaturalQuestionsModule {
}
