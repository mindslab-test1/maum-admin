export class LayerEntity {

  id: number;
  name: string;
  layerSection: number;
  creatorId: string;
  updaterId: string;
}
