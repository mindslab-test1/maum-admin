export class CategoryEntity {

  channelId: number;
  id: number;
  name: string;
  creatorId: string;
  createDtm: Date;
  updaterId: Date;
  updateDtm: Date;
  pageIndex: number;
  pageSize: number;

  answerCount: number;
  questionCount: number;
}
