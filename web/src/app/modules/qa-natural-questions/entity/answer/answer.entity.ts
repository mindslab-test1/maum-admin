import {LayerEntity} from '../layer/layer.entity';
import {QuestionEntity} from '../question/question.entity';

export class AnswerEntity {

  id: number;
  answer: string;
  answerView: string;
  categoryId: number;
  attr1: string;
  attr2: string;
  attr3: string;
  attr4: string;
  attr5: string;
  attr6: string;
  layer1: LayerEntity;
  layer2: LayerEntity;
  layer3: LayerEntity;
  layer4: LayerEntity;
  layer5: LayerEntity;
  layer6: LayerEntity;
  source: string;
  summary: string;
  tagStr: string;
  tags: string[];

  addedQuestions: QuestionEntity[];
  editedQuestions: QuestionEntity[];
  removedQuestions: QuestionEntity[];
}
