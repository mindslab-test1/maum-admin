export class QuestionEntity {

  id: number;
  answerId: number;
  answerCopyId: number;
  question: string;

}
