import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AnswerEntity} from '../../entity/answer/answer.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class QaSetService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getIndexedKeywords(channelId: number, categoryId: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/qa-sets/indexed-keywords/' + channelId + '/' + categoryId);
  }

  getPagingQaSets(categoryId: number, startRow: number, endRow: number, sortModel: string,
                  sortType: string, filterList: any): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/qa-sets/answers/category-id/' + categoryId + '/' + startRow
      + '/' + endRow + '/' + sortModel + '/' + sortType, filterList);
  }

  getQaSetByAnswerId(answerId: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/qa-sets/answers/' + answerId);
  }

  getLayerList(categoryId: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/qa-sets/layers/category-id/' + categoryId);
  }

  getTagList(): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/qa-sets/tags');
  }

  addQaSet(answerEntity: AnswerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/qa-sets/answers/add', answerEntity);
  }

  editQaSet(answerEntity: AnswerEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/qa-sets/answers/edit', answerEntity);
  }

  removeQaSets(ids: any): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/qa-sets/answers/remove', ids);
  }

  downloadFile(selectedChannelId: number, selectedCategoryId: number, listCount: number, sortModel: string,
               sortType: string, filterList: any): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/qa-sets/download-file/' + selectedChannelId + '/'
      + selectedCategoryId + '/' + listCount + '/' + sortModel + '/' + sortType,
      filterList, {responseType: 'blob'});
  }
}

