import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
// import {StorageBrowser} from 'mlt-shared';
import {CategoryEntity} from '../../entity/category/category.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class CategoryService {

  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getCategoryListByChannelId(channelId: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/categories/channel-id/' + channelId);
  }

  getCategoryListByName(categoryEntity: CategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/categories/name', categoryEntity);
  }

  getCategoryById(categoryId: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/categories/' + categoryId);
  }

  addQaCategory(categoryEntity: CategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/categories/add', categoryEntity);
  }

  editCategory(categoryEntity: CategoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/categories/edit', categoryEntity);
  }

  removeCategory(categoryIds: any): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/categories/remove', categoryIds);
  }
}

