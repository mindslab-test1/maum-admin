import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChannelEntity} from '../../entity/channel/channel.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class ChannelService {

  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getChannelList(): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/channels');
  }

  getChannelById(channelId: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/channels/id/' + channelId);
  }

  addChannel(channelEntity: ChannelEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/channels/add', channelEntity);
  }

  editChannel(channelEntity: ChannelEntity): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/channels/edit', channelEntity);
  }

  removeChannel(channelIds: any): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/channels/remove', channelIds);
  }
}

