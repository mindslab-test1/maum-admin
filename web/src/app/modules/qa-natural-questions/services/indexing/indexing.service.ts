import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class IndexingService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getChannelList(): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/indexing/channels');
  }

  getCategoryList(selectChannelId: number): Observable<any> {
    return this.http.post(this.API_URL + '/nqa/indexing/categories', selectChannelId);
  }

  fullIndexing(selectedChannelId: number, selectedCategoryId: number, selectedCollectionType: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}/nqa/indexing/fullIndexing/${selectedChannelId}/${selectedCategoryId}/${selectedCollectionType}`);
  }

  additionalIndexing(selectedChannelId: number, selectedCategoryId: number, selectedCollectionType: number): Observable<any> {
    return this.http.get(
      `${this.API_URL}/nqa/indexing/additionalIndexing/${selectedChannelId}/${selectedCategoryId}/${selectedCollectionType}`);
  }

  getIndexingStatus(): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/indexing/getIndexingStatus');
  }

  abortIndexing(selectedCollectionType: number): Observable<any> {
    return this.http.get(this.API_URL + '/nqa/indexing/abortIndexing/' + selectedCollectionType);
  }
}

