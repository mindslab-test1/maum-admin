import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NqaCategoryUpsertComponent} from './nqa-category-upsert.component';

describe('NqaCategoryUpsertComponent', () => {
  let component: NqaCategoryUpsertComponent;
  let fixture: ComponentFixture<NqaCategoryUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NqaCategoryUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NqaCategoryUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
