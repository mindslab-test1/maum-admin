import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef, MatPaginator,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {CategoryEntity} from '../../entity/category/category.entity';
import {AlertComponent, ConfirmComponent} from 'app/shared';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {CategoryService} from '../../services/category/category.service';


@Component({
  selector: 'app-nqa-category-upsert',
  templateUrl: 'nqa-category-upsert.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class NqaCategoryUpsertComponent implements OnInit {

  param: any;
  service: CategoryService;
  role: string;

  name: string;
  channelId: number;
  category: CategoryEntity;
  pageParam: MatPaginator;
  categoryNameValidator = new FormControl('', [Validators.required, this.noWhitespaceValidator]);
  disabled = true;
  data: any;

  constructor(private store: Store<any>,
              public dialogRef: MatDialogRef<NqaCategoryUpsertComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
    this.category = new CategoryEntity();
  }

  ngOnInit() {
    console.log('#@ ngOnInit NqaCategoryUpsertComponent');
    this.data = this.matData;
    this.role = this.data.role;
    this.channelId = this.data.channelId;
    console.log(this.data);

    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.service.getCategoryById(this.data.row).subscribe(res => {
        this.category = res;
      });
    }
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * ADD 버튼 클릭시 작동하는 함수, Category등록 기능
   */
  addQaCategory = () => {
    // this.workspace_id = parseInt(localStorage.getItem('m2uWorkspaceId'), 10);

    this.category.channelId = this.channelId;
    this.category.name = this.category.name.trim();
    this.category.id = this.storage.get('user').id;
    // this.category.pageIndex = this.pageParam.pageIndex;
    // this.category.pageSize = this.pageParam.pageSize;

    this.service.addQaCategory(this.category).subscribe(res => {
        if (res) {
          this.dialogRef.close('add');
          this.openAlertDialog('Success', 'Success Add Category', 'success');
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Add Category', 'error');
        console.log('error', err);
      }
    );
  };

  /**
   * Edit 버튼 클릭시 작동하는 함수, Category 수정 기능
   */
  editCategory = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Category?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.category.channelId = this.channelId;
        this.category.name = this.category.name.trim();
        this.service.editCategory(this.category).subscribe(res => {
            if (res) {
              this.dialogRef.close('edit');
              this.openAlertDialog('Success', `Success Edit Category.`, 'success');
            }
          },
          err => {
            this.openAlertDialog('Error', 'Failed Edit Category', 'error');
            console.log('error', err);
          }
        );
      }
    });
  };

  /**
   * Remove 버튼 클릭시 작동하는 함수, Category Remove 기능
   */
  removeCategory = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Category? All related Category will be removed';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let list = [];
        list.push(this.category.id);
        this.service.removeCategory(list).subscribe(res => {
            this.dialogRef.close('delete');
            this.openAlertDialog('Success', `Success Remove Category.`, 'success');

          },
          err => {
            this.openAlertDialog('Error', 'Failed Remove Category', 'error');
            console.log('error', err);
          }
        );
      }
    })
  };

  /**
   * 필수값 입력했는지 체크하는 함수, 필수값 입력완료시 등록버튼 활성화
   * @param $event
   */
  checkInvalid($event) {
    if ('INVALID' === this.categoryNameValidator.status) {
      this.disabled = true;
      return false;
    } else if ('VALID' === this.categoryNameValidator.status) {
      this.disabled = false;
      return true;
    }
  }

  /**
   * 빈 object인지 확인하는 함수
   * @param obj
   */
  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * 입력된 값이 빈값인지 확인하는 함수
   * @param control
   */
  noWhitespaceValidator(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : {'whitespace': true}
  }
}
