import {
  Component, OnInit, ViewChild,
  ViewEncapsulation, ChangeDetectorRef, AfterViewInit
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../../core/actions';
import {AuthService} from '../../../../core';
import {TableComponent} from '../../../dialog-model/shared/components/table/table.component';
import {ConsoleUserApi} from '../../../../core/sdk/services/custom';


import {CategoryService} from 'app/modules/qa-natural-questions/services/category/category.service';
import {AlertComponent} from 'app/shared';
import {NqaCategoryUpsertComponent} from '../nqa-category-upsert/nqa-category-upsert.component';
import {CategoryEntity} from '../../entity/category/category.entity';


@Component({
  selector: 'app-qa-sets-category-view',
  templateUrl: './nqa-categories.component.html',
  styleUrls: ['./nqa-categories.component.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 * NQA의 Category List 페이지
 *
 * */
export class NqaCategoriesComponent implements OnInit, AfterViewInit {
  // ViewChild 선언
  @ViewChild('tableComponent') tableComponent: TableComponent;

  actions: any; // category add,delete,edit

  data: any[] = [];
  inputName: string;  // search 할때 입력
  length: number; // tablecomponent에 전달
  pageParam: MatPaginator;
  dataSource: MatTableDataSource<any>;
  header;
  sort: MatSort;
  tempCategoryEntity: CategoryEntity;
  selectedCategory: number;

  constructor(private dialog: MatDialog,
              private categoryService: CategoryService,
              private cdr: ChangeDetectorRef,
              private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private consoleUserApi: ConsoleUserApi,
  ) {
  }

  ngOnInit() {
    let id = 'nqa';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = [
      {
        // [STEP] ADD Category
        type: 'add',
        text: 'Add Category',
        icon: 'add_circle_outline',
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles),
        disabled: false,
        callback: this.categoryAdd,
        params: 'add'
      },
      {
        // [STEP] Edit Category
        type: 'edit',
        text: 'Edit Category',
        icon: 'edit',
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles),
        disabled: true,
        callback: this.categoryEdit,
        params: 'edit'
      }
    ];

    // [STEP] header 정의
    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {
        attr: 'name',
        name: 'Category Name',
        isSort: true,
        onClick: this.navigateQaSetsList,
        width: '30%'
      },
      {attr: 'id', name: 'Category Id', isSort: true, width: '20%'},
    ];
  }

  ngAfterViewInit(): void {
    // [STEP] QA Category 정보를 조회
    this.getTotCntList();
    this.getList();
  }

  // [STEP] sort 함수
  setSort(matSort?: MatSort) {
    // console.log('#@ setSort matSort :', matSort);
    this.sort = matSort;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  // page가 변경되면 호출 (이후 list 갱신)
  getpaginator(page?: MatPaginator) {
    // console.log('#@ getpaginator page :', page, ', page._pageIndex :', page._pageIndex);
    this.pageParam = page;

    if (this.data.length > 0) {
      this.getList();
    }
  }

  // 전체 category list 조회 (전체 페이지 조회용)
  getTotCntList() {
    this.tempCategoryEntity = new CategoryEntity();

    // todo : not Using -> Delete
    this.categoryService.getCategoryListByChannelId(1).subscribe(res => {
      console.log('#@ getCategoryList res :', res);
      this.length = res.length; // list 총 갯수
    });
  }

  // category list 조회(search 기능 포함)
  getList() {
    // console.log('#@ getList this.pageParam :', this.pageParam);
    this.cdr.detectChanges();
    // Category Name으로 검색된 내용이 있는지 확인
    this.tempCategoryEntity = new CategoryEntity();
    this.tempCategoryEntity.name = this.inputName;
    this.tempCategoryEntity.pageIndex = this.pageParam.pageIndex * this.pageParam.pageSize;
    this.tempCategoryEntity.pageSize = this.pageParam.pageSize;
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  // [STEP] QA Sets List 이동
  navigateQaSetsList: any = (row: any) => {
    console.log('#@ navigateQaSetsList row :', row);

    // 선택된 Category 정보 전달
    this.router.navigateByUrl('mlt/nqa/qa-sets/categories/' + row.id);
  };
  // category 생성
  categoryAdd: any = () => {
    let paramData = {};

    paramData['role'] = 'add';
    paramData['row'] = {};
    paramData['service'] = this.categoryService;
    paramData['entity'] = new CategoryEntity();

    // category add popup 생성
    let dialogRef = this.dialog.open(NqaCategoryUpsertComponent, {
      data: paramData
    });
    // category add popup 결과
    dialogRef.afterClosed().subscribe(result => {
      // add 이후 리스트 갱신
      this.getList();
    });
  };

  // category 수정/삭제
  categoryEdit: any = () => {
    console.log('#@ categoryEdit checked length :', this.data.filter(row => row.isChecked).length);

    console.log('#@ categoryEdit :', this.data.filter(row => row.isChecked));

    // 선택된 category의 갯수가 1개가 아니면 에러
    let checkedRow = this.data.filter(row => row.isChecked);
    if (checkedRow.length < 1) {
      this.openAlertDialog('Error', 'Select items to Edit.', 'error');
      return;
    } else if (checkedRow.length > 1) {
      this.openAlertDialog('Error', 'Please select one.', 'error');
      return;
    }

    let selectedId = checkedRow[0].id;
    console.log('#@ selectedId :', selectedId);

    // 선택된 row 정보
    let paramData = {};
    paramData['role'] = 'edit';
    paramData['row'] = selectedId;
    paramData['service'] = this.categoryService;
    paramData['entity'] = new CategoryEntity();

    // category add popup 생성
    let dialogRef = this.dialog.open(NqaCategoryUpsertComponent, {
      data: paramData
    });
    // category add popup 결과
    dialogRef.afterClosed().subscribe(result => {
      // add 이후 리스트 갱신
      this.getList();
    });
  }
}
