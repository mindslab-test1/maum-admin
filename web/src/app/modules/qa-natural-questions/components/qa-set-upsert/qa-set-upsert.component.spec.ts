import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QaSetUpsertComponent} from './qa-set-upsert.component';

describe('QaSetUpsertComponent', () => {
  let component: QaSetUpsertComponent;
  let fixture: ComponentFixture<QaSetUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QaSetUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaSetUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
