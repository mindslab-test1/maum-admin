import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';

import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerEntity} from '../../entity/answer/answer.entity';
import {FormControl, Validators} from '@angular/forms';
import {QuestionEntity} from '../../entity/question/question.entity';
import {LayerEntity} from '../../entity/layer/layer.entity';
import {GridOptions} from 'ag-grid-community/dist/lib/entities/gridOptions';
import {QaSetService} from '../../services/qa-set/qa-set.service';
import {CategoryService} from '../../services/category/category.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {AlertComponent} from 'app/shared';
import {ROUTER_LOADED} from 'app/core/actions';
import {Store} from '@ngrx/store';
import {StorageBrowser} from "../../../../shared/storage/storage.browser";

@Component({
  selector: 'app-basic-qa-v2-qa-sets-upsert',
  templateUrl: './qa-set-upsert.component.html',
  styleUrls: ['./qa-set-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QaSetUpsertComponent implements OnInit {

  upsertFlag: string; // add, edit

  channelId: number;
  categoryId: number;
  answerId: number;
  answerCopyId: number;
  categoryName: string;
  answerEntity: AnswerEntity;
  questionEntity: QuestionEntity;
  questionEntities: QuestionEntity[] = [];
  addedQuestionEntities: QuestionEntity[] = [];
  editedQuestionEntities: QuestionEntity[] = [];
  removedQuestionEntities: QuestionEntity[] = [];
  layerEntities: LayerEntity[];
  layer1Entities: LayerEntity[];
  layer2Entities: LayerEntity[];
  layer3Entities: LayerEntity[];
  layer4Entities: LayerEntity[];
  layer5Entities: LayerEntity[];
  layer6Entities: LayerEntity[];
  selectedLayer1Id: number;
  selectedLayer2Id: number;
  selectedLayer3Id: number;
  selectedLayer4Id: number;
  selectedLayer5Id: number;
  selectedLayer6Id: number;
  invalidFlag: boolean;


  questionFormControl = new FormControl('', [
    Validators.required,
  ]);

  public gridApi;
  public gridColumnApi;
  public gridOptions: GridOptions;
  public defaultColDef;
  rowData = [];

  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes = [ENTER, COMMA];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private qaSetService: QaSetService,
              private categoryService: CategoryService) {
    this.invalidFlag = true;
  }

  ngOnInit() {
    console.log('#@ ngOnInit QaSetUpsertComponent');
    this.route.paramMap.subscribe(params => {
      this.categoryId = Number(params.get('categoryId'));
      this.categoryService.getCategoryById(this.categoryId).subscribe(res => {
        this.categoryName = res.name;
        this.channelId = res.channelId;
      });
    });
    this.answerEntity = new AnswerEntity();
    this.questionEntity = new QuestionEntity();
    this.getLayerList(this.categoryId).then(getLayerListFinish => {
      if (getLayerListFinish) {
        this.route.paramMap.subscribe(params => {
          if (params.get('id') === null || params.get('id') === undefined) {
            // add
            this.upsertFlag = 'Add';
          } else {
            // edit
            this.upsertFlag = 'Edit';
            this.answerId = Number(params.get('id'));
            this.answerCopyId = Number(params.get('copyId'));
            this.qaSetService.getQaSetByAnswerId(this.answerId).subscribe(res => {
              let answer = res.filter(answer => this.answerCopyId === answer.copyId)[0];
              this.answerEntity = answer;
              this.selectedLayer1Id = this.answerEntity.layer1.id;
              this.selectedLayer2Id = this.answerEntity.layer2.id;
              this.selectedLayer3Id = this.answerEntity.layer3.id;
              this.selectedLayer4Id = this.answerEntity.layer4.id;
              this.selectedLayer5Id = this.answerEntity.layer5.id;
              this.selectedLayer6Id = this.answerEntity.layer6.id;
              this.answerEntity.tagStr = this.answerEntity.tags.join(', ');
              this.gridOptions.api.updateRowData({add: answer.questions});
              this.invalidFlag = false;
            })
          }
        });
      }
    });

    /**
     * aggrid setting
     */
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.rowData,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
      }
    };

    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * ag grid column setting
   */
  createColumnDefs() {
    return [
      // {
      //   headerName: 'Id',
      //   field: 'id',
      //   width: 90
      // },
      {
        headerName: 'Question',
        field: 'question',
        editable: true
      }
    ];
  }

  /**
   * question add 버튼 클릭시 aggrid table에 빈 row 생성
   */
  addQuestionRow() {
    this.gridOptions.api.updateRowData({add: [{}]});
  }

  /**
   * 선택한 questionrow를 삭제
   */
  deleteQuestionRow() {
    let selectedData = this.gridOptions.api.getSelectedRows();
    selectedData.forEach(row => {
      if (row.id !== undefined && row.id !== null && row.id !== 0) {
        // 만약 DB에 저장된 값이라면
        this.removedQuestionEntities.push(row);
      }
    });
    this.gridOptions.api.updateRowData({remove: selectedData});
  }

  /**
   * layer 목록 조회
   */
  getLayerList(categoryId: number): Promise<boolean> {
    return new Promise(resolve => {
      this.qaSetService.getLayerList(categoryId).subscribe(res => {
        this.layerEntities = res;
        this.layer1Entities = [];
        this.layer2Entities = [];
        this.layer3Entities = [];
        this.layer4Entities = [];
        this.layer5Entities = [];
        this.layer6Entities = [];
        this.layerEntities.forEach(layer => {
          if (layer.layerSection === 1) {
            this.layer1Entities.push(layer);
          }
          if (layer.layerSection === 2) {
            this.layer2Entities.push(layer);
          }
          if (layer.layerSection === 3) {
            this.layer3Entities.push(layer);
          }
          if (layer.layerSection === 4) {
            this.layer4Entities.push(layer);
          }
          if (layer.layerSection === 5) {
            this.layer5Entities.push(layer);
          }
          if (layer.layerSection === 6) {
            this.layer6Entities.push(layer);
          }
        });
        resolve(true);
      }, err => {
        this.openAlertDialog('Failed', 'Failed Get LayerData', 'error');
      })
    });
  }

  /**
   * 필수값 입력했는지 확인하는 함수
   */
  checkValid() {
    if (this.answerEntity.answer === undefined || this.answerEntity.answer.trim() === '') {
      this.invalidFlag = true;
    } else {
      this.invalidFlag = false;
    }
  }

  /**
   * input 밑에 error message 표시해주는 함수
   */
  getErrorMessage() {
    return this.questionFormControl.hasError('required') ? 'Please enter question' :
      this.questionFormControl.hasError('duplicate') ? 'Question already exist' :
        '';
  }

  /**
   * Save버튼 클릭시 호출
   */
  qaSetSave() {
    let canSave = true;
    this.addedQuestionEntities = [];
    this.editedQuestionEntities = [];
    this.gridOptions.api.stopEditing();
    this.gridOptions.api.forEachNode(node => {
      if (node.data.question === undefined || node.data.question === '' || node.data.question === null) {
        // question ag grid 테이블에 필수값(question)내용이 안들어가있는경우 호출 및 등록 취소처리
        this.openAlertDialog('Warning', 'Enter required value', 'notice');
        canSave = false;
      }
      if (node.data.id === undefined || node.data.id === null || node.data.id === 0) {
        this.addedQuestionEntities.push(node.data);
      } else {
        this.editedQuestionEntities.push(node.data);
      }
    });
    if (canSave) {
      if (this.upsertFlag === 'Add') {
        this.answerEntity.categoryId = this.categoryId;
      }
      if (this.answerEntity.layer1 === undefined || this.answerEntity.layer1 === null) {
        this.answerEntity.layer1 = new LayerEntity();
      }
      if (this.answerEntity.layer2 === undefined || this.answerEntity.layer2 === null) {
        this.answerEntity.layer2 = new LayerEntity();
      }
      if (this.answerEntity.layer3 === undefined || this.answerEntity.layer3 === null) {
        this.answerEntity.layer3 = new LayerEntity();
      }
      if (this.answerEntity.layer4 === undefined || this.answerEntity.layer4 === null) {
        this.answerEntity.layer4 = new LayerEntity();
      }
      if (this.answerEntity.layer5 === undefined || this.answerEntity.layer5 === null) {
        this.answerEntity.layer5 = new LayerEntity();
      }
      if (this.answerEntity.layer6 === undefined || this.answerEntity.layer6 === null) {
        this.answerEntity.layer6 = new LayerEntity();
      }
      this.answerEntity.layer1.id = this.selectedLayer1Id;
      this.answerEntity.layer2.id = this.selectedLayer2Id;
      this.answerEntity.layer3.id = this.selectedLayer3Id;
      this.answerEntity.layer4.id = this.selectedLayer4Id;
      this.answerEntity.layer5.id = this.selectedLayer5Id;
      this.answerEntity.layer6.id = this.selectedLayer6Id;
      this.answerEntity.tags = [];
      if (this.answerEntity.tagStr !== undefined && this.answerEntity.tagStr !== '') {
        let tagArr = this.answerEntity.tagStr.split(',');
        tagArr.forEach((tag) => {
          // trim
          this.answerEntity.tags.push(tag.replace(/^\s+|\s+$/gm, ''));
        });
      }
      this.answerEntity.addedQuestions = this.addedQuestionEntities;
      this.answerEntity.editedQuestions = this.editedQuestionEntities;
      this.answerEntity.removedQuestions = this.removedQuestionEntities;
      if (this.upsertFlag === 'Add') {
        this.qaSetService.addQaSet(this.answerEntity).subscribe(res => {
          this.openAlertDialog('Success', 'Success Add', 'success');
          let extras = {queryParams:
              {channelId: this.channelId.toString(), categoryId: this.categoryId.toString()}};
          this.router.navigate(['m2u-builder', 'nqa', 'categories', 'qa-sets'], extras);
        }, err => {
          this.openAlertDialog('Failed', 'Failed Add', 'error');
        })
      } else {
        this.qaSetService.editQaSet(this.answerEntity).subscribe(res => {
          this.openAlertDialog('Success', 'Success Edit', 'success');
          this.goBack();
        }, err => {
          this.openAlertDialog('Failed', 'Failed Edit', 'error');
        })
      }
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  goBack() {
    let extras = {queryParams: {channelId: 0, categoryId: 0}};
    if (this.channelId !== undefined && this.categoryId !== undefined) {
      extras.queryParams.channelId = this.channelId;
      extras.queryParams.categoryId = this.categoryId;
    }
    this.router.navigate(['m2u-builder', 'nqa', 'categories', 'qa-sets'], extras);
    let filterStorage =  this.storage.get('filter');
    if (filterStorage !== null) {
      this.storage.set('filterModel', filterStorage);
    }
  }
}
