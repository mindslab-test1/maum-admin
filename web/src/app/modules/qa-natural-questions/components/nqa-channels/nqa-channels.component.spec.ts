import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NqaChannelsComponent } from './nqa-channels.component';

describe('NqaChannelsComponent', () => {
  let component: NqaChannelsComponent;
  let fixture: ComponentFixture<NqaChannelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NqaChannelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NqaChannelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
