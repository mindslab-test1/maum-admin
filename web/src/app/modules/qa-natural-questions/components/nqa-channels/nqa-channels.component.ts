import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {AlertComponent, ConfirmComponent, ErrorDialogComponent, TableComponent} from '../../../../shared';
import {ReplaySubject, Subject} from 'rxjs/index';
import {ActivatedRoute, Router} from '@angular/router';
import {AgGridNg2} from 'ag-grid-angular';
import {FormControl} from '@angular/forms';
import {GridOptions} from 'ag-grid-community/dist/lib/entities/gridOptions';
import {CategoryService} from '../../services/category/category.service';
import {takeUntil} from 'rxjs/operators';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {ChannelEntity} from '../../entity/channel/channel.entity';
import {ChannelService} from '../../services/channel/channel.service';
import {NqaChannelUpsertComponent} from '../nqa-channel-upsert/nqa-channel-upsert.component';
import {CategoryEntity} from '../../entity/category/category.entity';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {AuthService} from '../../../../core/auth.service';
import {getErrorString} from '../../../../shared/values/error-string';
import {ConsoleUserApi} from '../../../../core/sdk';
import {NqaCategoryUpsertComponent} from '../nqa-category-upsert/nqa-category-upsert.component';
import {QaSetService} from '../../services/qa-set/qa-set.service';

@Component({
  selector: 'app-nqa-channels',
  templateUrl: './nqa-channels.component.html',
  styleUrls: ['./nqa-channels.component.scss']
})
export class NqaChannelsComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridNg2;
  @ViewChild('tableComponent') tableComponent: TableComponent;

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header: any[] = [];

  colResizeDefault: string;

  channelActionsArray;
  channelList: ChannelEntity[];
  categoryList: CategoryEntity[];
  selectedChannelId: number;
  selectedCategoryId: number;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredChannelList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  isChannelSelected: boolean;
  isCategorySelected: boolean;

  categoryActionsArray;

  public gridOptions: GridOptions;
  public defaultColDef;

  rowData = [];

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private consoleUserApi: ConsoleUserApi,
              private channelService: ChannelService,
              private categoryService: CategoryService,
              private qaSetService: QaSetService) {
    this.colResizeDefault = 'shift';
    this.isChannelSelected = false;
  }

  ngOnInit() {

    this.categoryActionsArray = [
      {
        type: 'add',
        text: 'Add Category',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: true,
        callback: this.popItemCategoryUpsertDialog,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Edit Category',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: true,
        callback: this.popItemCategoryUpsertDialog,
        params: 'edit'
      },
      {
        text: 'Remove Category',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: true,
        callback: this.removeCategory,
        params: 'remove'
      },
    ];

    this.channelActionsArray = [
      {
        type: 'add',
        text: 'Add Channel',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.popItemChannelUpsertDialog,
        params: 'add'
      },
      {
        type: 'edit',
        text: 'Edit Channel',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: true,
        callback: this.popItemChannelUpsertDialog,
        params: 'edit'
      },
    ];

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {attr: 'id', name: 'Id', width: '5%', isSort: true},
      {attr: 'name', name: 'Name', isSort: true, onClick: this.navigateReleatedQaSets},
      {attr: 'answerCount', name: 'Answer Count', isSort: true},
      {attr: 'questionCount', name: 'Question Count', isSort: true},
      {attr: 'updateDtm', name: 'Update Date', format: 'date', isSort: true}
    ];

    this.whenCheckChanged();

    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.rowData,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      floatingFilter: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onCellClicked: (event) => {
        if (event.colDef.field === 'answer') {
          this.router.navigateByUrl('m2u-builder/nqa/categories/qa-sets/' + event.data.id);
        }
      },
      onCellValueChanged: (event) => {
        // this.updateDictionaryData(event.data);
      },
      onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
      }
    };

    this.selectSearchFormControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterChannel();
      });
  }

  ngAfterViewInit() {
    this.getChannelList().then(channelList => {
      this.route.queryParams.subscribe(
        query => {
          if (query['channelId'] !== undefined && query['channelId'] !== null) {
            this.selectedChannelId = query['channelId'] * 1;
            this.onChannelSelected();
            this.cdr.detectChanges();
          }
        });
    });
    this.store.dispatch({type: ROUTER_LOADED});
  }

  createColumnDefs() {
    return [
      {
        checkboxSelection: true,
        width: 35,
        suppressFilter: true,
        headerCheckboxSelection: true
      },
      {
        headerName: 'Id',
        field: 'id',
        width: 50,
        suppressFilter: true
      },
      {
        headerName: 'Answer',
        field: 'answer',
      },
      {
        headerName: 'Question',
        field: 'questionStringfy'
      },
      {
        headerName: 'Tags',
        field: 'tagStringfy',
        width: 80,
      },
      {
        headerName: 'Source',
        field: 'source',
        width: 80,
      },
      {
        headerName: 'Summary',
        field: 'summary',
        width: 100,
      },
      {
        headerName: 'Attr1',
        field: 'attr1',
        width: 80,
      },
      {
        headerName: 'Attr2',
        field: 'attr2',
        width: 80,
      },
      {
        headerName: 'Attr3',
        field: 'attr3',
        width: 80,
      },
      {
        headerName: 'Attr4',
        field: 'attr4',
        width: 80,
      },
      {
        headerName: 'layer1',
        field: 'layer1.name',
        width: 80,
      },
      {
        headerName: 'layer2',
        field: 'layer2.name',
        width: 80,
      },
      {
        headerName: 'layer3',
        field: 'layer3.name',
        width: 80,
      },
      {
        headerName: 'layer4',
        field: 'layer4.name',
        width: 80,
      },
    ];
  }

  /**
   * Channel SelectBox 검색 내용 초기화 해주는 함수
   */
  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  /**
   * Channel SelectBox에서 검색할수있도록 해주는 함수
   */
  private filterChannel() {
    if (!this.channelList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredChannelList.next(this.channelList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredChannelList.next(
      this.channelList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  /**
   * 서버에서 ChannelList를 조회해서 가져오는 함수
   * @returns {Promise<boolean>}
   */
  getChannelList(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.channelService.getChannelList().subscribe(res => {
        this.channelList = res;
        this.filteredChannelList.next(this.channelList.slice());
        resolve(true);
      }, err => {
        this.openAlertDialog('Error', 'Failed to get channel list', 'error');
        reject(true);
      })
    });
  }

  /**
   * Category List 조회 요청
   */
  getCategoryList() {
    if (this.selectedChannelId !== undefined && this.selectedChannelId !== 0) {
      this.categoryService.getCategoryListByChannelId(this.selectedChannelId)
        .subscribe(res => {
            this.categoryList = [];
            res.forEach(category => {
              category.answerCount = 0;
              category.questionCount = 0;
              let startRow = 0;
              let endRow = 0;
              let searchField = undefined;
              let searchValue = undefined;
              let sortModel = undefined;
              let sortType = undefined;
              let filterList: any[] = [];
              this.qaSetService.getPagingQaSets(category.id, startRow, endRow, sortModel, sortType, filterList).subscribe(qaSets => {
                qaSets.forEach(answer => {
                  category.answerCount = answer.listCount;
                  category.questionCount = answer.questionCount;
                });
              });
              this.categoryList.push(category);
            });
            this.rows = this.categoryList;
            this.whenCheckChanged();
            // this.filteredCategoryList.next(this.categoryList.slice());
          },
          err => {
            this.rows = [];
            let eref = this.dialog.open(ErrorDialogComponent);
            eref.componentInstance.title = 'Failed';
            eref.componentInstance.message = `Failed to get category list.  [${getErrorString(err)}]`;

          }
        );
    }
  }

  /**
   * Remove 버튼 클릭시 작동하는 함수, Category Remove 기능
   */
  removeCategory = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Category? All related QA set will be removed';
    ref.afterClosed().subscribe(result => {
      if (result) {
        let list = [];
        this.rows.filter(row => row.isChecked).forEach(row => {
          list.push(row.id);
        });
        this.categoryService.removeCategory(list).subscribe(res => {
            this.openAlertDialog('Success', `Success Remove Category.`, 'success');
            this.getCategoryList();
          },
          err => {
            this.openAlertDialog('Error', 'Failed Remove Category', 'error');
            console.log('error', err);
          }
        );
      }
    })
  };
  /**
   * 등록페이지로 이동합니다.
   */
  goAdd = () => {
    // category 등록 페이지가 될
    this.router.navigate(['m2u-builder', 'nqa', 'categories', 'qa-sets', this.selectedChannelId, 'new']);
  };

  /**
   * Channel Upsert Dialog Component 를 팝업으로 띄어주는 함수
   * @param type
   * @param row
   * @returns {boolean}
   */
  popItemChannelUpsertDialog = (type, row?) => {
    let paramData = {};

    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.selectedChannelId;
    } else {
      return false;
    }

    paramData['service'] = this.channelService;
    paramData['entity'] = new ChannelEntity();

    let dialogRef = this.dialog.open(NqaChannelUpsertComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit' || result === 'add') {
        this.getChannelList().then(channelList => {
        });
      }
      if (result === 'delete') {
        this.getChannelList().then(channelList => {
          this.selectedChannelId = 0;
          this.isChannelSelected = false;
          this.channelActionsArray[1].hidden = true;
          this.channelActionsArray[1].disabled = true;
        });
      }
    });
  };

  /**
   * Channel 선택되었을경우 isChannelSelected true값설정 및 Answer목록을 조회하는 함수 호출
   */
  onChannelSelected() {
    this.isChannelSelected = true;
    this.channelActionsArray[1].hidden = false;
    this.channelActionsArray[1].disabled = false;
    this.categoryActionsArray[0].hidden = false;
    this.categoryActionsArray[0].disabled = false;
    this.getCategoryList();
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * Index된 keyword로 목록에서 필터링 합니다.
   * @param key
   */
  filterByKeyword(keyword: string, column: string) {
    let filterComponent = this.gridOptions.api.getFilterInstance(column);
    filterComponent.setModel({
      type: 'contains',
      filter: keyword
    });
    this.gridOptions.api.onFilterChanged();
  }

  /**
   * 테이블에서 체크한 row의 개수에 따라, add, delete, delete disable 상태를 변경한다.
   */
  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.categoryActionsArray[1].disabled = true;
        this.categoryActionsArray[2].disabled = true;
        break;
      case 1:
        let selectedCategory = this.rows.filter(row => row.isChecked);
        this.selectedCategoryId = selectedCategory[0]['id'];
        this.categoryActionsArray[2].disabled = false;
        this.categoryActionsArray[1].disabled = false;
        break;
      default:
        this.categoryActionsArray[1].disabled = true;
        this.categoryActionsArray[2].disabled = false;
        break;
    }
  };

  /**
   * add 버튼 클릭시 발생하는 함수
   * add 화면으로 이동한다.
   */
  add: any = () => {
    // this.objectManager.clean('itfp');
    // // this.router.navigateByUrl('m2u/dialog-service/itfp-upsert#add');
    // // #@ itfp 신규 생성 페이지로 이동합니다
    // this.router.navigate(['new'], {relativeTo: this.route});
  };

  /**
   * IntentFinderPolicy Edit 화면으로 이동 한다.
   * 테이블에서 checkbox에서 선택한 IntentFinderPolicy 정보를 objectManager에 set 한다.
   */
  edit: any = () => {
    // let item = this.rows.filter(row => row.isChecked);
    // this.objectManager.set('itfp', item[0]);
    // // this.router.navigateByUrl('m2u/dialog-service/itfp-upsert#edit');
    // // #@  itfp 수정페이지로 이동합니다
    // this.router.navigate([item[0].name, 'edit'], {relativeTo: this.route});
  };

  /**
   * 테이블의 체크박스를 클릭한 후, Delete 버튼을 클릭 시 수행하는 함수 이다.
   * 복수개의 row를 삭제 할 수 있다.
   */
  delete: any = () => {
    // let names = this.rows.filter(row => row.isChecked).map(row => row.name);
    //
    // const ref = this.dialog.open(ConfirmComponent);
    // ref.componentInstance.message = `Delete '${names}?`;
    // ref.afterClosed().subscribe(confirmed => {
    //   if (!confirmed) {
    //     return;
    //   } else {
    //     let keyList = names.map(key => {
    //       return {name: key};
    //     });
    //     this.grpc.deleteIntentFinderPolicies(keyList).subscribe(
    //       res => {
    //         this.snackBar.open(`Selected Intent Finder Policies Successfully Deleted.`,
    //           'Confirm', {duration: 3000});
    //         this.getIntentFinderPolicyAllList().then(() => {
    //           this.whenCheckChanged();
    //         });
    //       },
    //       err => {
    //         let eref = this.dialog.open(ErrorDialogComponent);
    //         eref.componentInstance.title = 'Failed';
    //         eref.componentInstance.message = `Failed to Delete a Intent Finder Policies. [${getErrorString(err)}]`;
    //         return;
    //       });
    //   }
    // });
  };

  /**
   * Category Upsert Dialog Component 를 팝업으로 띄어주는 함수
   * @param type
   * @param row
   * @returns {boolean}
   */
  popItemCategoryUpsertDialog = (type, row?) => {
    let paramData = {};

    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.selectedCategoryId;
    } else {
      return false;
    }

    paramData['service'] = this.categoryService;
    paramData['entity'] = new CategoryEntity();
    paramData['channelId'] = this.selectedChannelId;

    let dialogRef = this.dialog.open(NqaCategoryUpsertComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit' || result === 'add') {
        this.getCategoryList();
      }
      if (result === 'delete') {
        this.getCategoryList();
        this.selectedCategoryId = 0;
        this.isCategorySelected = false;
        this.categoryActionsArray[1].disabled = true;
        this.categoryActionsArray[2].disabled = true;
      }
    });
  };

  /**
   * 테이블의 row에서 name을 클릭시 해당 category의 QA sets 화면으로 이동한다.
   * @param row => {테이블에서 클릭한 Category json 객체}
   */
  navigateReleatedQaSets: any = (row: any) => {
    console.log('#@ category row :', row);
    let params = 'categoryId=' + row.id + '&channelId=' + row.channelId;
    this.router.navigateByUrl('m2u-builder/nqa/categories/qa-sets?' + params);
  };
}

