import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ChangeDetectorRef, Inject,
} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AgGridNg2} from 'ag-grid-angular';
import {ActivatedRoute, Router} from '@angular/router';
import {CategoryEntity} from '../../entity/category/category.entity';
import {FormControl} from '@angular/forms';
import {Subject, ReplaySubject} from 'rxjs';
import {NqaCategoryUpsertComponent} from '../nqa-category-upsert/nqa-category-upsert.component';
import {QaSetService} from '../../services/qa-set/qa-set.service';
import {CategoryService} from '../../services/category/category.service';
import {takeUntil} from 'rxjs/operators';
import {GridOptions} from 'ag-grid-community/dist/lib/entities/gridOptions';
import {AlertComponent} from 'app/shared';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {ChannelService} from '../../services/channel/channel.service';
import {ChannelEntity} from '../../entity/channel/channel.entity';
import {ConfirmComponent, UploaderButtonComponent} from '../../../../shared';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {environment} from '../../../../../environments/environment';
import {GridApi} from 'ag-grid-community';

interface IDatasource {
  getRows(params: IGetRowsParams): void;

  destroy?(): void;
}

interface IGetRowsParams {
  startRow: number;
  endRow: number;
  sortModel: any,
  filterModel: any;
  context: any;

  successCallback(rowsThisBlock: any[], lastRow?: number): void;

  failCallback(): void;
}

@Component({
  selector: 'app-basic-qa-v2-qa-sets-list',
  templateUrl: './qa-sets.component.html',
  styleUrls: ['./qa-sets.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QaSetsComponent implements OnInit, AfterViewInit {

  @ViewChild('agGrid') agGrid: AgGridNg2;
  @ViewChild('uploader') uploader: UploaderButtonComponent;

  colResizeDefault: string;

  channelList: ChannelEntity[];
  categoryList: CategoryEntity[];
  selectedCategoryId: number;
  selectedCategoryName: string;
  selectedChannelId: number;
  uploadBtnEnable: boolean;
  downloadBtnEnable: boolean;
  gridApi: GridApi;
  listCount: number;
  sortModel: string;
  sortType: string;
  filterList: any;
  filterModel: any;
  down_btn: HTMLElement;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  private gridColumnApi;
  public filteredCategoryList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  isCategorySelected: boolean;

  qaSetActionsArray;


  public gridOptions: GridOptions;
  public defaultColDef;

  rowData = [];

  objectKeys = Object.keys;

  indexedQuestionKeywords = [];
  indexedQuestionKeywordsCnt: number = 0;
  indexedAnswerKeywords = [];
  indexedAnswerKeywordsCnt: number = 0;
  indexedTagKeywords = [];
  indexedAttr1Keywords = [];
  indexedAttr2Keywords = [];
  indexedAttr3Keywords = [];
  indexedAttr4Keywords = [];
  indexedAttr5Keywords = [];
  indexedAttr6Keywords = [];
  indexedLayer1Keywords = [];
  indexedLayer2Keywords = [];
  indexedLayer3Keywords = [];
  indexedLayer4Keywords = [];
  indexedLayer5Keywords = [];
  indexedLayer6Keywords = [];

  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      this.filterModel = params.filterModel;
      let filterKey = Object.getOwnPropertyNames(params.filterModel);
      let _filterList: any[] = [];
      filterKey.forEach((key) => {
        let keyMap = params.filterModel[key];
        let filterValue = keyMap['filter'];
        let entry = {searchField: key, searchValue: filterValue}
        _filterList.push(entry);
      });
      this.filterList = _filterList;
      // }
      let filter = JSON.stringify(params.filterModel);
      let filterModel = [] = filter.split('"');
      let searchField = filterModel[1];
      let searchValue = filterModel[9];
      let sortModel;
      let sortType;
      if (params.sortModel.length !== 0) {
        sortModel = params.sortModel[0].colId;
        sortType = params.sortModel[0].sort;
      }
      this.sortModel = sortModel;
      this.sortType = sortType;
      let startRow = params.startRow + 1;
      this.qaSetService.getPagingQaSets(this.selectedCategoryId, startRow,
        params.endRow, sortModel, sortType, this.filterList).subscribe(res => {
        res.forEach(answer => {
          let temp = [];
          let questionTemp = [];
          answer.questions.forEach(question => {
            questionTemp.push(question.question);
          });
          answer['questionStringfy'] = questionTemp;
          answer['tag'] = answer.tags.join();
          answer['rownum'] = startRow;
          startRow += 1;
          this.listCount = answer.listCount;
        });
        if (res.length === 0) {
          params.successCallback(
            res, 0)
        } else {
          params.successCallback(
            res, this.listCount)
        }
        this.gridOptions.api.setRowData([]); // 초기화
        this.gridOptions.api.updateRowData({add: res});
        if (this.gridOptions.api.getSelectedRows().length === 0) {
          this.qaSetActionsArray[1].hidden = true;
          this.qaSetActionsArray[2].hidden = false;
          this.downloadBtnEnable = true;
          this.changeBtnColor();
        }
      })
    }
  }

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private cdr: ChangeDetectorRef,
              private dialog: MatDialog,
              private channelService: ChannelService,
              private categoryService: CategoryService,
              private qaSetService: QaSetService) {
    this.colResizeDefault = 'shift';
    this.isCategorySelected = false;
  }

  ngOnInit() {
    this.qaSetActionsArray = [
      {
        type: 'add',
        text: 'Add QA Set',
        icon: 'add_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.goAdd,
        params: 'add'
      },
      {
        text: 'Remove QA Set',
        icon: 'remove_circle_outline',
        hidden: true,
        disabled: false,
        callback: this.removeAnswer,
        params: 'remove'
      },
      {
        text: 'Remove All QA Set',
        icon: 'remove_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.removeAllAnswer,
        params: 'remove'
      },
      {
        text: 'Edit QA Set',
        icon: 'edit_circle_outline',
        hidden: false,
        disabled: false,
        callback: this.goEdit,
        params: 'edit'
      }
    ];

    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      pagination: true,
      rowModelType: 'infinite',
      cacheBlockSize: 10,
      paginationPageSize: 10,
      rowData: this.rowData,
      enableServerSideSorting: true,
      enableServerSideFilter: true,
      enableColResize: true,
      enableFilter: true,
      enableSorting: false,
      animateRows: true,
      floatingFilter: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onCellClicked: (event) => {
        if (event.colDef.field === 'answer') {
          this.router.navigateByUrl('m2u-builder/nqa/categories/qa-sets/' + event.data.id);
          this.storage.set('filter', this.filterModel);
        }
      },
      onCellValueChanged: (event) => {
        // this.updateDictionaryData(event.data);
      },
    };

    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterCategory();
    });
  }

  ngAfterViewInit() {
    this.getChannelList().then(channelList => {
      this.route.queryParams.subscribe(
        query => {
          if (query['channelId'] !== undefined && query['channelId'] !== null
            && query['categoryId'] !== undefined && query['categoryId'] !== null) {
            this.selectedChannelId = query['channelId'] * 1;
            this.selectedCategoryId = query['categoryId'] * 1;
            this.onChannelSelected();
            this.onCategorySelected();
            this.cdr.detectChanges();

            this.uploadBtnEnable = true;
            this.downloadBtnEnable = true;
            this.changeBtnColor();
          }
        }
      )
    });

    this.store.dispatch({type: ROUTER_LOADED});
  }

  createColumnDefs() {
    return [
      {
        checkboxSelection: true,
        width: 40,
        suppressFilter: true,
        headerCheckboxSelection: true
      },
      {
        headerName: 'No',
        field: 'rownum',
        width: 60,
        suppressFilter: true
      },
      {
        headerName: 'Answer',
        field: 'answer',
        width: 350
      },
      {
        headerName: 'Question',
        field: 'questionStringfy',
        width: 350
      },
      {
        headerName: 'Tags',
        field: 'tag',
        width: 100
      },
      {
        headerName: 'Source',
        field: 'source',
        width: 120
      },
      {
        headerName: 'Summary',
        field: 'summary',
        width: 60
      },
      {
        headerName: 'layer1',
        field: 'layer1.name',
        width: 150
      },
      {
        headerName: 'layer2',
        field: 'layer2.name',
        width: 100
      },
      {
        headerName: 'layer3',
        field: 'layer3.name',
        width: 100
      },
      {
        headerName: 'layer4',
        field: 'layer4.name',
        width: 60
      },
      {
        headerName: 'layer5',
        field: 'layer5.name',
        width: 100
      },
      {
        headerName: 'layer6',
        field: 'layer6.name',
        width: 100
      },
      {
        headerName: 'Attr1',
        field: 'attr1',
        width: 80,
      },
      {
        headerName: 'Attr2',
        field: 'attr2',
        width: 80,
      },
      {
        headerName: 'Attr3',
        field: 'attr3',
        width: 80,
      },
      {
        headerName: 'Attr4',
        field: 'attr4',
        width: 80,
      },
      {
        headerName: 'Attr5',
        field: 'attr5',
        width: 80,
      },
      {
        headerName: 'Attr6',
        field: 'attr6',
        width: 80,
      },
    ];
  }

  /**
   * Category SelectBox 검색 내용 초기화 해주는 함수
   */
  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  doAction = (action) => {
    if (action) {
      action.callback(action.params);
    }
  };

  /**
   * Category SelectBox에서 검색할수있도록 해주는 함수
   */
  private filterCategory() {
    if (!this.categoryList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredCategoryList.next(this.categoryList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredCategoryList.next(
      this.categoryList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  /**
   * 서버에서 ChannelList를 조회해서 가져오는 함수
   * @returns {Promise<boolean>}
   */
  getChannelList(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.channelService.getChannelList().subscribe(res => {
        this.channelList = res;
        resolve(true);
      }, err => {
        this.openAlertDialog('Error', 'Failed to get channel list', 'error');
        reject(true);
      })
    });

  }

  /**
   * 서버에서 CategoryList를 조회해서 가져오는 함수
   * @returns {Promise<boolean>}
   */
  getCategoryListByChannelId(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.categoryService.getCategoryListByChannelId(this.selectedChannelId).subscribe(res => {
        this.categoryList = res;
        this.filteredCategoryList.next(this.categoryList.slice());
        resolve(true);
      }, err => {
        this.openAlertDialog('Error', 'Failed to get category list', 'error');
        reject(true);
      })
    });
  }

  /**
   * 서버에서 AnswerList 조회해서 가져오는 함수
   */
  /**
   * indexed keyword 리스트 요청 (answer, question, tag, attr, layer)
   */
  getIndexedKeyword() {
    this.qaSetService.getIndexedKeywords(this.selectedChannelId, this.selectedCategoryId).subscribe(res => {
      // console.log("#@ 1 :", res);

      // #@ Question를 value(index 갯수)기준으로 정렬합니다
      let sortableQuestion = [];
      for (let key in res.question) {
        sortableQuestion.push([key, res.question[key]]);
      }

      sortableQuestion.sort(function (a, b) {
        return b[1] - a[1];
      });
      // console.log('#@ sortable :', sortable);
      this.indexedQuestionKeywords = sortableQuestion;
      this.indexedQuestionKeywordsCnt = Object.keys(this.indexedQuestionKeywords).length;

      // #@ Answer를 value(index 갯수)기준으로 정렬합니다
      let sortableAnswer = [];
      for (let key in res.answer) {
        sortableAnswer.push([key, res.answer[key]]);
      }

      sortableAnswer.sort(function (a, b) {
        return b[1] - a[1];
      });
      // console.log('#@ sortable :', sortable);
      this.indexedAnswerKeywords = sortableAnswer;
      this.indexedAnswerKeywordsCnt = Object.keys(this.indexedAnswerKeywords).length;
      this.indexedLayer1Keywords = res.layer1;
      this.indexedLayer2Keywords = res.layer2;
      this.indexedLayer3Keywords = res.layer3;
      this.indexedLayer4Keywords = res.layer4;
      this.indexedLayer5Keywords = res.layer5;
      this.indexedLayer6Keywords = res.layer6;
      this.indexedAttr1Keywords = res.attr1;
      this.indexedAttr2Keywords = res.attr2;
      this.indexedAttr3Keywords = res.attr3;
      this.indexedAttr4Keywords = res.attr4;
      this.indexedAttr5Keywords = res.attr5;
      this.indexedAttr6Keywords = res.attr6;
      this.indexedTagKeywords = res.tag;
    }, err => {
      this.openAlertDialog('Error', 'Failed to get indexed Keyword', 'error');
    })
  }

  /**
   * 서버에 answer 삭제 요청
   */
  removeAnswer = () => {
    let selectedRows = this.gridOptions.api.getSelectedRows();
    if (selectedRows.length === 0) {
      this.openAlertDialog('Warning', 'Please Select Rows', 'notice');
    } else {
      let arr = [];
      selectedRows.forEach(row => {
        arr.push({id: row.id, copyId: row.copyId});
      });
      const ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = `Delete this QA Set?`;
      ref.afterClosed()
      .subscribe(confirmed => {
        if (!confirmed) {
          return;
        }
        this.qaSetService.removeQaSets(arr).subscribe(res => {
          this.openAlertDialog('Success', 'Success Remove', 'success');
          this.gridApi.setDatasource(this.dataSource);
        });
      });
    }
  };

  removeAllAnswer = () => {
    let startRow = 0;
    let searchField = undefined;
    let searchValue = undefined;
    let sortModel = undefined;
    let sortType = undefined;
    let filterList: any[] = [];
    let arr = [];

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete All QA Set?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      this.qaSetService.getPagingQaSets(this.selectedCategoryId, startRow,
        this.listCount, sortModel, sortType, this.filterList).subscribe(res => {
        res.forEach(answer => {
          arr.push({id: answer.id, copyId: answer.copyId});
        });
        this.qaSetService.removeQaSets(arr).subscribe(result => {
          this.openAlertDialog('Success', 'Success Remove', 'success');
          this.gridApi.setDatasource(this.dataSource);
        });
      });
    });
  };

  /**
   * 등록페이지로 이동합니다.
   */
  goAdd = () => {
    this.router.navigate(['m2u-builder', 'nqa', 'categories', 'qa-sets', this.selectedCategoryId, 'new']);
  };

  /**
   * 수정 페이지로 이동
   */
  goEdit = () => {
    let selectedRows = this.gridOptions.api.getSelectedRows();
    if (selectedRows.length === 0) {
      this.openAlertDialog('Warning', 'Please Select Row', 'notice');
    } else if (selectedRows.length > 1) {
      this.openAlertDialog('Warning', 'Please Select 1 Row', 'notice');
    } else {
      this.router.navigate(['m2u-builder', 'nqa', 'categories', 'qa-sets', this.selectedCategoryId, 'update',
        selectedRows[0].id, selectedRows[0].copyId]);
      this.storage.set('filter', this.filterModel);
    }
  };

  /**
   * Category Upsert Dialog Component 를 팝업으로 띄어주는 함수
   * @param type
   * @param row
   * @returns {boolean}
   */
  popItemCategoryUpsertDialog = (type, row?) => {
    let paramData = {};

    if (type === 'add') {
      paramData['role'] = 'add';
      paramData['row'] = {};
    } else if (type === 'edit') {
      paramData['role'] = 'edit';
      paramData['row'] = this.selectedCategoryId;
    } else {
      return false;
    }

    paramData['service'] = this.categoryService;
    paramData['entity'] = new CategoryEntity();

    let dialogRef = this.dialog.open(NqaCategoryUpsertComponent, {
      data: paramData
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'edit' || result === 'add') {
        this.getCategoryListByChannelId().then(getCategoryListByChannelIdResult => {
        });
      }
      if (result === 'delete') {
        this.getCategoryListByChannelId().then(getCategoryListByChannelIdResult => {
          this.selectedCategoryId = 0;
          this.isCategorySelected = false;

          this.uploadBtnEnable = false;
          this.downloadBtnEnable = false;
          this.changeBtnColor();
        });
      }
    });
  };

  /**
   * Channel 선택되었을경우 isChannelSelected true값설정 및 Answer목록을 조회하는 함수 호출
   */
  onChannelSelected() {
    this.getCategoryListByChannelId();

    this.uploadBtnEnable = false;
    this.downloadBtnEnable = false;
    this.changeBtnColor();
  }

  /**
   * Category 선택되었을경우 isCategorySelected true값설정 및 Answer목록을 조회하는 함수 호출
   */
  onCategorySelected() {
    this.isCategorySelected = true;
    this.getIndexedKeyword();
    this.uploadBtnEnable = true;
    this.downloadBtnEnable = true;
    this.changeBtnColor();

    // this.gridOptions.api.setFilterModel(null);
    let filterStorage = this.storage.get('filterModel');
    if (filterStorage === null) {
      this.gridOptions.api.setFilterModel(null);
    } else {
      this.gridOptions.api.setFilterModel(filterStorage);
    }
    this.storage.remove('filterModel');
    this.storage.remove('filter');
    this.gridApi.setDatasource(this.dataSource);
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * Index된 keyword로 목록에서 필터링 합니다.
   * @param key
   */
  filterByKeyword(keyword: string, column: string) {
    let filterComponent = this.gridOptions.api.getFilterInstance(column);
    filterComponent.setModel({
      type: 'contains',
      filter: keyword
    });
    this.gridOptions.api.onFilterChanged();
  }

  // 업로드 이후 호출되는 함수
  onUploadFile = () => {
    if (this.selectedChannelId !== undefined) {
      this.gridApi.setDatasource(this.dataSource);
    }
  };

  // 다운로드 함수
  downloadFile = () => {
    if (this.selectedChannelId !== undefined && this.selectedChannelId !== 0 &&
      this.selectedCategoryId !== undefined && this.selectedCategoryId !== 0) {
      this.qaSetService.downloadFile(this.selectedChannelId, this.selectedCategoryId,
        this.listCount, this.sortModel, this.sortType, this.filterList).subscribe(
        data => {
          let fileName = undefined;
          let url = window.URL.createObjectURL(data);
          let a = document.createElement('a');
          this.categoryList.forEach(category => {
            if (this.selectedCategoryId === category.id) {
              fileName = category.name;
            }
          })
          document.body.appendChild(a);
          a.download = 'NQAItemList-' + fileName + '.xlsx';
          a.href = url;
          a.click();
        })
    }
    // let url = environment.maumAiApiUrl + '/nqa/qa-sets/download-file/' + this.selectedChannelId + '/' + this.selectedCategoryId +
    //   '/' + this.listCount + '/' + this.filterList;
    // let a = document.createElement('a');
    // document.body.appendChild(a);
    // a.style.display = 'none';
    // a.href = url;
    // a.click();

  };
  onCellClicked = (event) => {
    if (event.colDef.field === 'answer') {
      this.router.navigateByUrl('m2u-builder/nqa/categories/qa-sets/' + event.data.id);
    }
  }

  changeBtnColor = () => {
    this.down_btn = <HTMLElement>document.getElementById('down-btn');
    this.down_btn.style.backgroundColor = this.downloadBtnEnable ? '#52627b' : '#a0a0a0';
  };

  onGridReady(params: any) {
    this.gridApi = params.api;
    // let filterStorage = this.storage.get('filterModel');
    // if (filterStorage !== null) {
    //   this.gridOptions.api.setFilterModel(filterStorage);
    // }
    this.gridApi.setDatasource(this.dataSource);
    this.gridColumnApi = params.columnApi;
  }

  onRowSelected(event) {
    if (this.gridOptions.api.getSelectedRows().length === 0) {
      this.qaSetActionsArray[1].hidden = true;
      this.qaSetActionsArray[2].hidden = false;
      this.downloadBtnEnable = true;
      this.changeBtnColor();
    } else {
      this.qaSetActionsArray[1].hidden = false;
      this.qaSetActionsArray[2].hidden = true;
      this.downloadBtnEnable = false;
      this.changeBtnColor();
    }
  }

  confirmUpload() {
    if (this.selectedChannelId !== undefined && this.selectedChannelId !== 0 &&
      this.selectedCategoryId !== undefined && this.selectedCategoryId !== 0) {
      this.uploader.url = '/nqa/qa-sets/upload-files/' + this.selectedChannelId + '/' + this.selectedCategoryId;
      this.uploader.preUploadCallbackResult = new Promise(resolve => {
        let ref = this.dialog.open(ConfirmComponent);
        ref.componentInstance.message = 'If you upload a file,<br>' +
          'All remained data will be deleted.<br>Continue?  ';
        ref.afterClosed().subscribe(result => {
          if (result) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
    }
  };
}
