import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QaSetsComponent} from './qa-sets.component';

describe('QaSetsComponent', () => {
  let component: QaSetsComponent;
  let fixture: ComponentFixture<QaSetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QaSetsComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaSetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
