import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QaSetDetailComponent} from './qa-set-detail.component';

describe('QaSetDetailComponent', () => {
  let component: QaSetDetailComponent;
  let fixture: ComponentFixture<QaSetDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QaSetDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaSetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
