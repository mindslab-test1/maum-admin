import {
  Component, Inject,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AgGridNg2} from 'ag-grid-angular';
import {ActivatedRoute, Router} from '@angular/router';
import {QaSetService} from '../../services/qa-set/qa-set.service';
import {AnswerEntity} from '../../entity/answer/answer.entity';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';
import {CategoryService} from '../../services/category/category.service';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';

@Component({
  selector: 'app-qa-set-detail',
  templateUrl: './qa-set-detail.component.html',
  styleUrls: ['./qa-set-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QaSetDetailComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridNg2;
  colResizeDefault: string;

  itemId: number;
  categoryId: number;
  channelId: number;

  answer: AnswerEntity;
  columnDefs = [
    {
      headerName: 'Id',
      field: 'id',
      width: 90,
    },
    {
      headerName: 'Question',
      field: 'question',
      width: 1600
    }
  ];

  rowDataList = [];
  qaSets = [];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private store: Store<any>,
              private route: ActivatedRoute,
              private router: Router,
              private qaSetService: QaSetService,
              private categoryService: CategoryService) {
    this.answer = new AnswerEntity();
  }

  ngOnInit() {
    console.log('#@ QaSetDetailComponent ngOnInit');

    this.route.paramMap.subscribe(params => {
      this.itemId = Number(params.get('id'));
      this.qaSetService.getQaSetByAnswerId(this.itemId).subscribe(res => {
        res.forEach( qaSet => {
          this.answer = qaSet;
          this.answer.tagStr = this.answer.tags.join(', ');
          this.rowDataList.push(qaSet['questions']);
          this.qaSets.push(this.answer);
        });
        this.categoryId = this.answer.categoryId;
        this.categoryService.getCategoryById(this.categoryId).subscribe(category => {
          this.channelId = category.channelId;
        });
      });
    });

    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * 수정 페이지로 이동
   */
  goEdit() {
    this.router.navigateByUrl('m2u-builder/nqa/categories/qa-sets/' + this.itemId + '/update');
  }

  /**
   * 뒤로가기 페이지로 이동
   */
  goBack() {
    let extras = {queryParams: {channelId: 0, categoryId: 0}};
    if (this.channelId !== undefined && this.categoryId !== undefined) {
      extras.queryParams.channelId = this.channelId;
      extras.queryParams.categoryId = this.categoryId;
    }
    this.router.navigate(['m2u-builder', 'nqa', 'categories', 'qa-sets'], extras);
    let filterStorage =  this.storage.get('filter');
    if (filterStorage !== null) {
      this.storage.set('filterModel', filterStorage);
    }
  }
}
