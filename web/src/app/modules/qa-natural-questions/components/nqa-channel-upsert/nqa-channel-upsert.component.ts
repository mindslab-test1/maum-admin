import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {AlertComponent, ConfirmComponent} from '../../../../shared';
import {Store} from '@ngrx/store';
import {StorageBrowser} from '../../../../shared/storage/storage.browser';
import {ROUTER_LOADED} from '../../../../core/actions';
import {FormControl, Validators} from '@angular/forms';
import {ChannelService} from '../../services/channel/channel.service';
import {ChannelEntity} from '../../entity/channel/channel.entity';

@Component({
  selector: 'app-nqa-channel-upsert',
  templateUrl: './nqa-channel-upsert.component.html',
  styleUrls: ['./nqa-channel-upsert.component.scss']
})
export class NqaChannelUpsertComponent implements OnInit {

  param: any;
  service: ChannelService;
  role: string;

  name: string;
  channel: ChannelEntity;
  pageParam: MatPaginator;
  channelNameValidator = new FormControl('', [Validators.required, this.noWhitespaceValidator]);
  disabled = true;
  data: any;

  constructor(private store: Store<any>,
              public dialogRef: MatDialogRef<NqaChannelUpsertComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
    this.channel = new ChannelEntity();
  }

  ngOnInit() {
    console.log('#@ ngOnInit NqaChannelUpsertComponent');
    this.data = this.matData;
    this.role = this.data.role;

    this.service = this.data['service'];
    if (this.role === 'edit') {
      this.service.getChannelById(this.data.row).subscribe(res => {
        this.channel = res;
      });
    }
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * ADD 버튼 클릭시 작동하는 함수, Channel등록 기능
   */
  addQaChannel = () => {
    // this.workspace_id = parseInt(localStorage.getItem('m2uWorkspaceId'), 10);

    this.channel.name = this.channel.name.trim();
    this.channel.creatorId = this.storage.get('user').id;
    this.channel.updaterId = this.storage.get('user').id;
    // this.channel.pageIndex = this.pageParam.pageIndex;
    // this.channel.pageSize = this.pageParam.pageSize;

    this.service.addChannel(this.channel).subscribe(res => {
        if (res) {
          this.dialogRef.close('add');
          this.openAlertDialog('Success', 'Success Add Channel', 'success');
        }
      },
      err => {
        this.openAlertDialog('Error', 'Failed Add Channel', 'error');
        console.log('error', err);
      }
    );
  };

  /**
   * Edit 버튼 클릭시 작동하는 함수, Channel 수정 기능
   */
  editChannel = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Channel?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.channel.name = this.channel.name.trim();
        this.service.editChannel(this.channel).subscribe(res => {
            if (res) {
              this.dialogRef.close('edit');
              this.openAlertDialog('Success', `Success Edit Channel.`, 'success');
            }
          },
          err => {
            this.openAlertDialog('Error', 'Failed Edit Channel', 'error');
            console.log('error', err);
          }
        );
      }
    });
  };

  /**
   * Remove 버튼 클릭시 작동하는 함수, Channel Remove 기능
   */
  removeChannel = () => {
    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Channel? ' +
      'All related category and QA set will be removed';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.service.removeChannel(this.channel.id).subscribe(res => {
            this.dialogRef.close('delete');
            this.openAlertDialog('Success', `Success Remove Channel.`, 'success');

          },
          err => {
            this.openAlertDialog('Error', 'Failed Remove Channel', 'error');
            console.error(err);
          }
        );
      }
    })
  };

  /**
   * 필수값 입력했는지 체크하는 함수, 필수값 입력완료시 등록버튼 활성화
   * @param $event
   */
  checkInvalid($event) {
    if ('INVALID' === this.channelNameValidator.status) {
      this.disabled = true;
      return false;
    } else if ('VALID' === this.channelNameValidator.status) {
      this.disabled = false;
      return true;
    }
  }

  /**
   * 빈 object인지 확인하는 함수
   * @param obj
   */
  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  /**
   * 입력된 값이 빈값인지 확인하는 함수
   * @param control
   */
  noWhitespaceValidator(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : {'whitespace': true}
  }
}
