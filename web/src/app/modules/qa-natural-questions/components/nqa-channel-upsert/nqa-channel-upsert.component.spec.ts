import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NqaChannelUpsertComponent } from './nqa-channel-upsert.component';

describe('NqaChannelUpsertComponent', () => {
  let component: NqaChannelUpsertComponent;
  let fixture: ComponentFixture<NqaChannelUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NqaChannelUpsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NqaChannelUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
