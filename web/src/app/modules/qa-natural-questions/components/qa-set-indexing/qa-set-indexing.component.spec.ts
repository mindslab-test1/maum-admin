import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QaSetIndexingComponent} from './qa-set-indexing.component';

describe('QaSetIndexingComponent', () => {
  let component: QaSetIndexingComponent;
  let fixture: ComponentFixture<QaSetIndexingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QaSetIndexingComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QaSetIndexingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
