import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {DatePipe} from '@angular/common';
// import {AlertComponent} from 'mlt-shared';
import {MatDialog} from '@angular/material';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {IndexingService} from '../../services/indexing/indexing.service';
import {AlertComponent} from 'app/shared';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from 'app/core/actions';

@Component({
  selector: 'app-basicqa-v2-indexing',
  templateUrl: './qa-set-indexing.component.html',
  styleUrls: ['./qa-set-indexing.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]

})
export class QaSetIndexingComponent implements OnInit, OnDestroy {
  indexing;
  history;
  progressStatus = false;
  intervalHandler: any;
  timeout;
  disabled = false;

  channelList;
  selectedChannelId = 0;
  categoryList;
  selectedCategoryId = 0;
  valid = true;
  selectedCollectionType = 2;

  selectSearchFormControl: FormControl = new FormControl();
  private _onDestroy = new Subject<void>();
  public filteredCategoryList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public filteredChannelList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  constructor(private store: Store<any>,
              private indexingService: IndexingService,
              private datePipe: DatePipe,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    console.log('#@ ngOnInit QaSetIndexingComponent');
    // todo
    this.selectSearchFormControl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      // this.filterChannel();
      this.filterCategory();
    });
    this.getChannelList();
    this.timeout = 5000;
    this.getIndexingStatus();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  ngOnDestroy() {
    if (this.intervalHandler) {
      clearInterval(this.intervalHandler);
    }
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * channel selectbox 검색 filter
   */
  private filterChannel() {
    console.log('filterChannel:: channelList::: {}', this.channelList);
    if (!this.channelList) {
      return;
    }

    // todo
    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredChannelList.next(this.channelList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredChannelList.next(
        this.channelList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  /**
   * category selectbox 검색 filter
   */
  private filterCategory() {
    console.log('filterCategory:: categoryList::: {}', this.categoryList);
    if (!this.categoryList) {
      return;
    }

    let keyword = this.selectSearchFormControl.value;

    if (!keyword) {
      this.filteredCategoryList.next(this.categoryList.slice());
      return;
    } else {
      keyword = keyword.toLowerCase();
    }
    this.filteredCategoryList.next(
        this.categoryList.filter(skill => skill.name.toLowerCase().indexOf(keyword) > -1)
    );
  }

  /**
   * category selectbox 검색 초기화
   */
  clearSelectSearchFormControlValue() {
    this.selectSearchFormControl.setValue('');
  }

  /**
   * getIndexingStatus Trigger
   */
  triggerInterval() {
    this.intervalHandler = setInterval(() => {
      this.getIndexingStatus()
    }, this.timeout);
  }

  /***
   * full indexing 요청
   */
  fullIndexing() {
    if (this.selectedChannelId === undefined || this.selectedChannelId === 0) {
      this.openAlertDialog('Notice', 'Select a Channel for indexing.', 'notice');
      return;
    }
    this.indexingService.fullIndexing(this.selectedChannelId, this.selectedCategoryId, this.selectedCollectionType).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        };
      } else {
        this.disabled = false;
        clearInterval(this.intervalHandler);
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. NQA Indexing failed.', 'error');
    })
  }

  /**
   * additionalIndexing 요청
   */
  additionalIndexing() {
    if (this.selectedChannelId === undefined || this.selectedChannelId === 0) {
      this.openAlertDialog('Notice', 'Select a Channel for indexing.', 'notice');
      return;
    }
    this.indexingService.additionalIndexing(this.selectedChannelId, this.selectedCategoryId, this.selectedCollectionType).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: ((res.fetched / res.total) * 100).toFixed(3),
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. NQA Indexing failed.', 'error');
    })
  }

  /**
   * abortIndexing 요청
   */
  abortIndexing() {
    this.indexingService.abortIndexing(this.selectedCollectionType).subscribe(res => {
      this.progressStatus = res.status;
      if (res.status) {
        this.disabled = true;
        clearInterval(this.intervalHandler);
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: res.progress,
          stopped: '',
          error: '',
          key: '',
        }
      } else {
        this.getIndexingStatus();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. NQA Indexing failed.', 'error');
    })
  }

  /**
   * getIndexingStatus요청
   */
  getIndexingStatus() {
    this.indexingService.getIndexingStatus().subscribe(res => {
      this.progressStatus = res.status;
      clearInterval(this.intervalHandler);
      if (res.status) {
        this.disabled = true;
        this.timeout = 450;
        this.triggerInterval();
        this.indexing = {
          progress: (res.total == 0) ? 0 : ((res.fetched / res.total) * 100).toFixed(3),
          stopped: '',
          error: '',
          key: '',
          fetched: res.fetched,
          total: res.total
        }
      } else {
        this.history = res.latestIndexingHistory;
        if (this.history != null) {
          if (this.history.stopYn) {
            if (this.history.total === 0) {
              this.history.progress = 0;
            } else {
              this.history.progress = ((this.history.fetched / this.history.total) * 100).toFixed(3);
            }
          } else {
            this.history.progress = 100;
          }
          let date = new Date(this.history.createdAt);
          this.history.createAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          if (this.history.stopYn) {
            date = new Date(this.history.updatedAt);
            this.history.updateAt = this.datePipe.transform(date, 'MM/dd/yyyy, HH: mm');
          }
        }
        this.disabled = false;
        this.timeout = 5000;
        this.triggerInterval();
      }
    }, err => {
      clearInterval(this.intervalHandler);
      this.openAlertDialog('Error', 'Server error. NQA Indexing failed.', 'error');
    })
  }

  /**
   * Channel List 조회 요청
   */
  getChannelList() {
    this.indexingService.getChannelList().subscribe(res => {
      this.channelList = res;
      this.filteredChannelList.next(this.channelList.slice());
    });
  }

  /**
   * Category List 조회 요청
   */
  getCategoryList() {
    if (this.selectedChannelId !== undefined && this.selectedChannelId !== 0) {
      this.indexingService.getCategoryList(this.selectedChannelId).subscribe(res => {
        this.categoryList = res;
        this.filteredCategoryList.next(this.categoryList.slice());
      });
    } else {
      this.categoryList = [];
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  checkValid(option?) {
    if (this.selectedChannelId !== undefined && this.selectedCategoryId !== undefined && this.selectedCollectionType !== undefined) {
      this.valid = true;
    } else {
      this.valid = false;
    }
    if (option === 'channel' && this.selectedChannelId !== undefined) {
      this.getCategoryList();
    }
  }
}
