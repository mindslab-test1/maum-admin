import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderInstancePanelComponent} from './intent-finder-instance-panel.component';

describe('IntentFinderInstancePanelComponent', () => {
  let component: IntentFinderInstancePanelComponent;
  let fixture: ComponentFixture<IntentFinderInstancePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderInstancePanelComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderInstancePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
