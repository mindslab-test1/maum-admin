import {Component, OnInit} from '@angular/core';
import {AppEnumsComponent} from '../../../../shared/values/app.enums';
import {ROUTER_LOADED} from '../../../../core/actions';
import {Store} from '@ngrx/store';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {AppObjectManagerService} from 'app/shared';
import {GrpcApi} from 'app/core/sdk';
import {getErrorString} from '../../../../shared/values/error-string';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorDialogComponent} from '../../../../shared';

@Component({
  selector: 'app-intent-finder-policy-test',
  templateUrl: './intent-finder-instance-test.component.html',
  styleUrls: ['./intent-finder-instance-test.component.scss']
})

/**
 * IntentFinder Test 화면
 */
export class IntentFinderInstanceTestComponent implements OnInit {
  dialogTitle: string;
  itfiName: string;
  actions: any = {};
  itfi: any;
  itfp = {steps: []};
  utterForm = new FormGroup({
    utter: new FormControl('', [Validators.required]),
    lang: new FormControl('', [Validators.required]),
    input_type: new FormControl('KEYBOARD', [Validators.required]),
  });

  testButtonFlag = true;
  inputTypes = [
    'SPEECH', 'KEYBOARD', 'TOUCH', 'IMAGE', 'IMAGE_DOCUMENT', 'VIDEO', 'OPEN_EVENT'
  ];
  resultSkill: any;
  resPair: { step: string, skill: string };
  findSkill: { step: string, skill: string };
  findHints: { step: string, skill: string }[] = [];
  resultNlu: any;
  resultFlag = true;

  constructor(private store: Store<any>,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              public appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    let obm = this.objectManager.get('itfi');
    if (obm === undefined) {
      this.route.params.subscribe(par => {
        this.itfiName = par['id'];
      });
    } else {
      this.itfiName = obm;
    }
    if (this.itfiName === undefined) {
      this.snackBar.open(`Undefined Find Intent Finder Instance Name.`,
        'Confirm', {duration: 3000});
      this.backPage();
      this.store.dispatch({type: ROUTER_LOADED});
      return;
    }
    this.dialogTitle = `Test Intent Finder Instance :${this.itfiName}`;
    this.objectManager.clean('itfi');
    this.retrieveData();
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * 초기 데이터 세팅
   * IntentFinderInstance 정보를 불러온 후,
   * 해당 IntentFinderInstance가 가지고 있는 IntentFinderPolicy 정보를 가져온다.
   */
  retrieveData(): void {
    this.getIntentFinderInstance().then(() => {
      this.getIntentFinderPolicy();
    });
  }

  /**
   * 하나의 IntentFinderInstance 객체를 가져온다.
   * @returns {Promise<any>} => {IntentFinderInstance}
   */
  getIntentFinderInstance(): Promise<any> {
    return new Promise(resolve => {
      this.grpc.getIntentFinderInstanceInfo(this.itfiName).subscribe(
        res => {
          this.itfi = res;
          resolve();
        },
        err => {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Failed';
          ref.componentInstance.message = `Failed to get a Intent Finder Instance. [${getErrorString(err)}]`;
          this.backPage();
        });
    });
  }

  /**
   * 하나의 IntentFinderInstance가 가지고 있는 IntentFinderPolicy 객체를 가져온다.
   */
  getIntentFinderPolicy(): void {
    this.grpc.getIntentFinderPolicyInfo(this.itfi.policy_name).subscribe(
      res => {
        this.itfp = res;
        let lang = 'kor';
        if (res.lang === 'ko_KR') {
          lang = 'kor';
        } else {
          lang = 'eng';
        }
        this.utterForm.setControl('lang', new FormControl({
          value: lang,
          disabled: true
        }));

        this.settingUIStep();
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to get a intent finder policy. [${getErrorString(err)}]`;
        this.backPage();
      });
  }

  /**
   * 화면에 들어올시 UI로 보여줘야 되는 변수 값을 세팅 해준다.
   */
  settingUIStep(): void {
    this.itfp.steps.forEach((step, index) => {
      step['seq'] = index + 1;

      // UI에 Skill, Hint 여부 표시 하기 위하여 세팅
      if (step.run_style === 'IFC_SKILL_FOUND_RETURN'
        || step.run_style === 'IFC_SKILL_FOUND_COLLECT_HINTS') {
        step.stepRunStyle = 'S';
        step.is_hint = false;
      } else if (step.run_style === 'IFC_HINT') {
        step.stepRunStyle = 'H';
        step.is_hint = true;
      }

      // UI에 Step에 등록된 Model 정보 표시
      switch (step.test_rule) {
        case 'custom_script_mod':
          step.type = 'CLM_CLASSIFY_SCRIPT';
          step.stepType = `<Custom Script>`;
          break;
        case 'pcre_mod':
          step.type = 'CLM_PCRE';
          step.stepType = `<PCRE - ${step.pcre_mod.model}>`;
          break;
        case 'dnn_cl_mod':
          step.type = 'CLM_DNN_CLASSIFIER';
          step.stepType = `<DNN - ${step.dnn_cl_mod.model}>`;
          break;
        case 'hmd_mod':
          step.type = 'CLM_HMD';
          step.stepType = `<HMD - ${step.hmd_mod.model}>`;
          break;
        case 'final_rule':
          step.type = 'CLM_FINAL_RULE';
          step.stepType = `<Final rule>`;
          break;
      }
    });
  }

  /**
   * Test 조건이 됬는지 확인 한 후, 정상적으로 등록 가능한 상태가 될 때 true를 반환 해준다.
   * @returns {boolean}
   */
  isValid(): boolean {
    if (this.utterForm.valid) {
      this.testButtonFlag = false;
    } else {
      this.testButtonFlag = true;
    }

    return this.utterForm.valid;
  }

  /**
   * 등록한 utter, lang, input type을 바탕으로,
   * IntentFinderInstance Test 한다.
   */
  submit(): void {
    this.findHints.length = 0;
    this.findSkill = {step: '', skill: ''};
    let utter = JSON.parse(JSON.stringify(this.utterForm.getRawValue()));
    utter['lang'] = this.appEnum.lang[utter.lang];
    utter['input_type'] = this.appEnum.inputType[utter.input_type];
    utter['alt_utters'] = [];

    let param = {
      utter: utter,
      host: `${this.itfi.ip}:${this.itfi.port}`
    };

    this.grpc.proveIntent(param).subscribe(
      res => {
        this.resultFlag = false;
        if (res.hasOwnProperty('test_find_intent')) {
          delete res.test_find_intent;
        }

        let run_results = res.skill.history.run_results;
        for (let i = 0; i < run_results.length; i++) {
          let detail: string = run_results[i].detail;
          if (detail.length > 0) {
            try {
              run_results[i].detail = JSON.parse(detail);
            } catch (e) {
            }
          }
        }

        this.resultSkill = Object.assign({}, {history: res.skill.history}, {hints: res.skill.hints});
        if (res.skill.skill) {
          let step = res.skill.history.run_results.filter(e => e.skill === res.skill.skill);
          if (step) {
            this.findSkill = {
              step: step[0].name,
              skill: res.skill.skill
            };
          }
        }
        let hints = res.skill.hints;
        Object.keys(hints).map(key => {
          this.resPair = {
            step: key, skill: hints[key]
          };
          this.findHints.push(this.resPair);
        });
        this.resultNlu = res.skill.nlu_result;
      },
      err => {
        this.resultFlag = true;
        let eref = this.dialog.open(ErrorDialogComponent);
        eref.componentInstance.title = 'Failed';
        eref.componentInstance.message = `Test intent error.  [${getErrorString(err)}]`;
      }
    )
  }

  /**
   * IntentFinderInstance View 화면으로 이동한다.
   */
  backPage: any = (): void => {
    this.objectManager.clean('itfi');
    // this.router.navigate(['../itfi'], {relativeTo: this.route});

    this.router.navigate(['../'], {relativeTo: this.route});
  };

}
