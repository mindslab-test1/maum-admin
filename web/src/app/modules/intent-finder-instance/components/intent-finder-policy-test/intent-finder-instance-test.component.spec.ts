import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderInstanceTestComponent} from './intent-finder-instance-test.component';

describe('IntentFinderPolicyTestComponent', () => {
  let component: IntentFinderInstanceTestComponent;
  let fixture: ComponentFixture<IntentFinderInstanceTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderInstanceTestComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderInstanceTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
