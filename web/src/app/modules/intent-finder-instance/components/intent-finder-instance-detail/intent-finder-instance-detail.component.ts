import {Component, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {
  AlertComponent,
  AppObjectManagerService,
  getErrorString,
  ErrorDialogComponent
} from '../../../../shared';
import {ConsoleUserApi} from '../../../../core/sdk';
import {AppEnumsComponent} from '../../../../shared/values/app.enums';
import {GrpcApi} from '../../../../core/sdk';
import {Location} from '@angular/common';

@Component({
  selector: 'app-intent-finder-instance-detail',
  templateUrl: './intent-finder-instance-detail.component.html',
  styleUrls: ['./intent-finder-instance-detail.component.scss'],
})

/**
 * IntentFinder Instance Detail 보여주는 Component
 */
export class IntentFinderInstanceDetailComponent implements OnInit {
  actions: any;
  itfi: any;
  itfp: any;
  itfiName: any;
  policyValid: Boolean = false;

  constructor(private store: Store<any>,
              private objectManager: AppObjectManagerService,
              private router: Router,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi,
              public appEnum: AppEnumsComponent,
              private location: Location) {
  }

  ngOnInit(): void {
    let id = 'itf';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    ////////////////////////////////////////////////////////////
    // objectManager안에 itfi 정보를 맴버변수 itfi에 저장합니다
    let obm = this.objectManager.get('itfi');

    // objectManager로 넘겨받은 정보가 있으나 itfi 이름의 정보가 존재하지 않으면 이전페이지로 이동합니다
    if (obm === undefined) {
      this.route.params.subscribe(par => {
        this.itfiName = par['id'];
      });
    } else {
      this.itfiName = obm.name;
      this.itfi = obm;
    }
    if (this.itfiName === undefined) {
      this.backPage();
      return;
    }
    this.getIntentFinderInstanceInfo().then((res) => {
      this.itfi = res;
      this.objectManager.set('itfi', this.itfi);
    });
    ////////////////////////////////////////////////////////////

    this.actions = {
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      test: {
        icon: 'streetview',
        text: 'Test',
        callback: this.test,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.READ, roles)
      }
    };

    // // objectManager안에 itfi 정보를 맴버변수 itfi에 저장합니다
    // this.itfi = this.objectManager.get('itfi');
    // // itfi 정보가 없는 경우엔 이전페이지로 이동합니다
    // if (this.itfi === undefined) {
    //   this.backPage();
    //   return;
    // }

    this.getIntentFinderPolicyAllList().then();

    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * IntentFinderPolicy 의 전체 리스트를 가져와서 현재 IntentFinderInstance의 policy명과 비교한다.
   * 전체 리스트에 현재 IntentFinderInstance의 policy명이 존재하는 경우에만, 클릭할 수 있도록 flag를 true로 변경한다.
   * @returns {Promise<any>} => IntentFinderPolicy의 전체 리스트(json)
   */
  getIntentFinderPolicyAllList() {
    return new Promise(resolve => {
      this.grpc.getIntentFinderPolicyAllList().subscribe(
        res => {
          if (res) {
            res.itf_policy_list.forEach(itfPolicy => {
              if (itfPolicy.name === this.itfi.policy_name) {
                this.policyValid = true;
              }
            });
            resolve();
          }
        },
        err => {
          console.log('failed get itf policy list');
        });
    });
  }

  /**
   * IntentFinder Instance 수정 페이지로 이동
   */
  edit: any = (): void => {
    // upsert 페이지에 전달하기 위한 itfi_name을 설정합니다
    this.objectManager.set('itfi_name', this.itfi.name);
    // #@ itfi 수정페이지로 이동합니다
    this.router.navigate(['edit'], {relativeTo: this.route});
  };

  /**
   * IntentFinder Instance 삭제 API 호출
   */
  delete: any = (): void => {
    let names = [];
    names.push(this.itfi.name);

    const ref = this.dialog.open(AlertComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      } else {
        let keyList = names.map(key => {
          return {name: key};
        });
        this.grpc.deleteIntentFinderInstances(keyList).subscribe(
          res => {
            this.snackBar.open(`Selected Intent Finder Instance Successfully Deleted.`,
              'Confirm', {duration: 3000});
            this.backPage();
          },
          err => {
            let eref = this.dialog.open(ErrorDialogComponent);
            eref.componentInstance.title = 'Failed';
            ref.componentInstance.message = `Failed to Delete a Intent Finder Instance. [${getErrorString(err)}]`;
            return;
          }
        )
      }
    });
  };

  /**
   * IntnetFinder Test 화면 이동
   */
  test: any = () => {
    this.objectManager.set('itfi', this.itfi.name);
    this.router.navigate(['test'], {relativeTo: this.route});
  };

  getIntentFinderPolicyInfo() {
    return new Promise(resolve => {
      this.grpc.getIntentFinderPolicyInfo(this.itfi.policy_name).subscribe(res => {
        if (res) {
          this.itfp = res;
          resolve();
        }
      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
    });
  }

  /**
   * IntentFinder Instance 리스트 조회 페이지로 이동
   */
  backPage: any = (): void => {
    this.objectManager.clean('itfi');
    // this.location.back();
    // #@ itfi 리스트 페이지로 이동합니다
    this.router.navigate(['..'], {relativeTo: this.route});

  };

  navigateItfp() {
    this.getIntentFinderPolicyInfo().then(() => {
      this.objectManager.set('itfp', this.itfp);
      // #@ itfp detail 화면으로 이동합니다
      this.router.navigate(['../../../m2u-builder/intent-finder', this.itfp.name], {relativeTo: this.route});
    });
  }

  getIntentFinderInstanceInfo() {
    return new Promise(resolve => {
      this.grpc.getIntentFinderInstanceInfo(this.itfiName).subscribe(res => {
        if (res) {
          resolve(res);
        }
      }, err => {
        let message = `Cannot get ITF instance. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        console.error('err', err.message);
      });
    });
  }
}
