import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderInstanceDetailComponent} from './intent-finder-instance-detail.component';

describe('IntentFinderInstanceDetailComponent', () => {
  let component: IntentFinderInstanceDetailComponent;
  let fixture: ComponentFixture<IntentFinderInstanceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderInstanceDetailComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderInstanceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
