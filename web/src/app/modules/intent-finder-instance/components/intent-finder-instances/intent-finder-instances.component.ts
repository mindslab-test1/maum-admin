import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild, ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSidenav, MatSnackBar, MatTableDataSource} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {
  AppObjectManagerService, TableComponent, ErrorDialogComponent,
  ConfirmComponent
} from '../../../../shared';
import {ConsoleUserApi} from '../../../../core/sdk';
import {getErrorString} from '../../../../shared/values/error-string';
import {GrpcApi} from '../../../../core/sdk';

@Component({
  selector: 'app-intent-finder-instances',
  templateUrl: './intent-finder-instances.component.html',
  styleUrls: ['./intent-finder-instances.component.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 * IntentFinder Instance List 조회 페이지 Component
 */
export class IntentFinderInstancesComponent implements OnInit, AfterViewInit, OnDestroy {
  actions: any;
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  // @ViewChild('sidenav') sidenav: MatSidenav;
  // @ViewChild('infoPanel') infoPanel: IntentFinderInstancePanelComponent;

  dataSource: MatTableDataSource<any>;
  header = [];
  rows: any[] = [];

  constructor(private store: Store<any>,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private fb: FormBuilder,
              private snackBar: MatSnackBar,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private cdr: ChangeDetectorRef,
              private grpc: GrpcApi) {
  }

  ngOnInit(): void {
    let id = 'itf';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      }
    };

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {attr: 'name', name: 'Name', isSort: true, onClick: this.detail},
      {attr: 'ip', name: 'IP', isSort: true},
      {attr: 'port', name: 'PORT', isSort: true},
      {attr: 'policy_name', name: 'Policy Name', isSort: true}
    ];

    this.whenCheckChanged();
    // this.tableComponent.onRowClick = this.fillRowInfo;

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  ngAfterViewInit(): void {
    this.retrieveData().then(() => {
      if (this.rows.length > 0) {
        // this.fillRowInfo(this.rows[0]);
      }
    });

    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * IntentFinder Instance 리스트 조회 API 호출
   * @returns {Promise<any>}
   */
  retrieveData(): Promise<any> {
    return new Promise(resolve => {
      this.grpc.getIntentFinderInstanceList().subscribe(
        res => {
          this.tableComponent.isCheckedAll = false;
          res.itfi_list.sort((a, b) => {
            return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
          });
          this.rows = res.itfi_list;
          this.cdr.detectChanges();
          resolve();
        },
        err => {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Failed';
          ref.componentInstance.message = `Failed to get Intent Finder Instance List. [${getErrorString(err)}]`;
        });
    });
  }

  /**
   * Panel 활성화시 상단에 Panel 숨김 보임 처리
   */
  // togglePanel: any = (): void => {
  //   this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
  //   this.sidenav.toggle();
  // };

  /**
   * Panel Component에 IntentFinder Instance 정보 전달
   * @param row IntentFinder Instance 정보 json 값
   */
  // fillRowInfo: any = (row: any): void => {
  //   this.infoPanel.itfiInfo = row;
  //   if (row === undefined) {
  //     this.sidenav.close();
  //     this.panelToggleText = 'Show Info Panel';
  //   } else {
  //     this.sidenav.open();
  //     this.panelToggleText = 'Hide Info Panel';
  //   }
  // };

  /**
   * row 클릭시 마다 상단 action 버튼들 활성화 비활서화 처리
   */
  whenCheckChanged: any = (): void => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        break;
    }
  };

  /**
   * IntentFinder Instance Detail 페이지로 이동
   * @param row IntentFinder Instance 정보 json 값
   */
  detail: any = (row: any): void => {
    this.objectManager.set('itfi', row);
    // #@ itfi detail 페이지로 이동합니다
    this.router.navigate([row.name], {relativeTo: this.route});
  };

  /**
   * IntentFinder Instance 등록 페이지로 이동
   */
  add: any = (): void => {
    this.objectManager.clean('itfi');
    // #@ itfi 등록 페이지로 이동합니다
    this.router.navigate(['new'], {relativeTo: this.route});
  };

  /**
   * IntentFinder Instance 수정 페이지로 이동
   */
  edit: any = (): void => {
    let name = this.rows.filter(row => row.isChecked).map(row => row.name);
    this.objectManager.set('itfi_name', name[0]);
    this.router.navigate([name[0], 'edit'], {relativeTo: this.route});
  };

  /**
   * IntentFinder Instance 삭제 API 호출
   */
  delete: any = (): void => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.name);
    if (names.length === 0) {
      let eref = this.dialog.open(ErrorDialogComponent);
      eref.componentInstance.title = 'Failed';
      eref.componentInstance.message = `Please Select Intent Finder Instance to Delete.`;
      return;
    }
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      } else {
        let keyList = names.map(key => {
          return {name: key};
        });
        this.grpc.deleteIntentFinderInstances(keyList).subscribe(
          res => {
            this.snackBar.open(`Selected Intent Finder Instance Successfully Deleted.`,
              'Confirm', {duration: 3000});
            this.retrieveData();
            this.tableComponent.isCheckedAll = false;
            this.whenCheckChanged();
          },
          err => {
            let eref = this.dialog.open(ErrorDialogComponent);
            eref.componentInstance.title = 'Failed';
            ref.componentInstance.message = `Failed to Delete a Intent Finder Instance. [${getErrorString(err)}]`;
            return;
          }
        )
      }
    });
  };

  /**
   * 불필요한 메모리 사용을 방지 하기위해, unsubscribe 해줌
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
