import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderInstancesComponent} from './intent-finder-instances.component';

describe('IntentFinderInstancesComponent', () => {
  let component: IntentFinderInstancesComponent;
  let fixture: ComponentFixture<IntentFinderInstancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderInstancesComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
