import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntentFinderInstanceUpsertComponent} from './intent-finder-instance-upsert.component';

describe('IntentFinderInstanceUpsertComponent', () => {
  let component: IntentFinderInstanceUpsertComponent;
  let fixture: ComponentFixture<IntentFinderInstanceUpsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntentFinderInstanceUpsertComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentFinderInstanceUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
