import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog, MatSelectionList, MatSelectionListChange,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {
  ConfirmComponent,
  ErrorDialogComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  FormErrorStateMatcher,
  getErrorString
} from '../../../../shared';
import {ROUTER_LOADED} from '../../../../core/actions';
import {IP_REGEX, PORT_REGEX, VARIABLE_REGEX} from '../../../../shared/values/values'
import {GrpcApi} from '../../../../core/sdk';

@Component({
  selector: 'app-intent-finder-instance-upsert',
  templateUrl: './intent-finder-instance-upsert.component.html',
  styleUrls: ['./intent-finder-instance-upsert.component.scss'],
  providers: [{
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  }]
})

/**
 * IntentFinder Instance Upsert 페이지 Component
 */
export class IntentFinderInstanceUpsertComponent implements OnInit {
  dialogTitle: string;
  role: string;

  itfiUpsertForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern(VARIABLE_REGEX)]),
    ip: new FormControl('', [Validators.required, Validators.pattern(IP_REGEX)]),
    port: new FormControl('', [Validators.required, Validators.pattern(PORT_REGEX)]),
    description: new FormControl(''),
    policy_name: new FormControl('', [Validators.required]),
    export_ip: new FormControl('', [Validators.pattern(IP_REGEX)])
  });

  canSubmit = false;
  policyName: string;
  policyList: any[] = [];
  itfiName: string;
  itfiList: any[] = [];

  @ViewChild(MatSelectionList) policyNameList: MatSelectionList;

  constructor(private store: Store<any>,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              public appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private cdr: ChangeDetectorRef,
              private grpc: GrpcApi) {
  }

  ngOnInit(): void {
    let promise = new Promise((resolve) => {
      if (this.route.toString().indexOf('new') > 0) { // new인 경우
        this.role = 'add';
        this.dialogTitle = 'Add Intent Finder Instance';
        new Promise((resolve1) => {
          this.retrieveData();
          resolve1();
        }).then(() => {
          this.store.dispatch({type: ROUTER_LOADED});
        });
      } else if (this.route.toString().indexOf('edit') > 0) { // edit인 경우
        let obm = this.objectManager.get('itfi_name');
        if (obm) {
          this.itfiName = obm;
          this.objectManager.clean('itfi_name');
        } else {
          this.route.params.subscribe(par => {
            this.itfiName = par['id'];
          });
        }
        if (this.itfiName === undefined) {
          this.snackBar.open(`Undefined Find Intent Finder Instance Name.`,
            'Confirm', {duration: 3000});
          this.backPage();
          this.store.dispatch({type: ROUTER_LOADED});
          return;
        }
        this.role = 'edit';
        this.dialogTitle = 'Edit Intent Finder Instance';
        resolve();
      } else {  // url이 잘못된 경우
        this.snackBar.open(`Cannot find this url.`,
          'Confirm', {duration: 3000});
        this.backListPage();
        this.store.dispatch({type: ROUTER_LOADED});
        return;
      }
      this.policyNameList.selectionChange.subscribe((s: MatSelectionListChange) => {
        this.policyName = s.option.value;
        this.itfiUpsertForm.patchValue({policy_name: s.option.value});
        this.policyNameList.deselectAll();
        s.option.selected = true;
        this.check();
      });
    });
    promise.then(() => {
      this.getIntentFinderInstanceInfo().then(() => {
        this.retrieveData();
        this.store.dispatch({type: ROUTER_LOADED});
      });
    }).catch(() => {
      this.snackBar.open(`Something wrong.`,
        'Confirm', {duration: 3000});
    });
  }

  /**
   * policyName 리스트 가져오는 API 호출
   */
  retrieveData(): void {
    if (this.role === 'edit') {
      this.grpc.getIntentFinderInstanceInfo(this.itfiName).subscribe(
        res => {
          this.policyName = res.policy_name;
          this.itfiUpsertForm.patchValue({
            ip: res.ip,
            port: res.port,
            policy_name: res.policy_name,
            description: res.description,
            export_ip: res.export_ip
          });
          this.itfiUpsertForm.setControl('name', new FormControl({
            value: res.name,
            disabled: true
          }, [Validators.required]));
        },
        err => {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Failed';
          ref.componentInstance.message = `Failed to get a Intent Finder Instance. [${getErrorString(err)}]`;
          this.backPage();
        }
      )
    }
    this.grpc.getIntentFinderInstanceList().subscribe(res => {
        this.itfiList = res.itfi_list;
        if (this.role === 'edit') {
          this.itfiList.splice(this.itfiList.findIndex(itfi => itfi.name === this.itfiName), 1);
        }
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Something went wrong. [${getErrorString(err)}]`;
      });

    this.grpc.getIntentFinderPolicyAllList().subscribe(
      res => {
        this.policyList = res.itf_policy_list;
      },
      err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to get a Intent Finder Policy List. [${getErrorString(err)}]`;
        this.backPage();
      }
    )
  }

  /**
   * role 에 따라 add, edit 호출
   */
  submit(): void {
    if (this.role === 'add') {
      this.openDialog('add');
    } else {
      this.openDialog('edit');
    }
  }

  /**
   * ITF Instance Name와 Port가 기존 데이터와 중복되는지 확인
   * @param {string} val 비교할 값(optional)
   * @param {string} type 'name', 'port' 중 check 대상 선택(optional)
   */
  check(val?: string, type?: string): void {
    if (val) {
      if (type === 'name') {
        if (this.itfiList.find(itfi => itfi.name === val)) {
          this.itfiUpsertForm.get('name').setErrors({'duplicated': true});
        }
      }
      if (type === 'port') {
        let ip = (<HTMLInputElement>document.getElementById('ip')).value;
        if (this.itfiList.find(itfi => itfi.port === parseInt(val, 10) && itfi.ip === ip)) {
          this.itfiUpsertForm.get('port').setErrors({'duplicated': true});
        }
      }
      if (type === 'ip') {
        let port = (<HTMLInputElement>document.getElementById('port')).value;
        if (this.itfiList.find(itfi => itfi.ip === val && itfi.port === parseInt(port, 10))) {
          this.itfiUpsertForm.get('port').setErrors({'duplicated': true});
        } else {
          this.itfiUpsertForm.get('port').setErrors(null);
        }
      }
      this.cdr.detectChanges();
    }

    if (this.itfiUpsertForm.valid) {
      this.cdr.detectChanges();
      this.canSubmit = true;
    }
  }

  /**
   * ITF Instance Add API 호출
   */
  onAdd(): void {
    let param = JSON.parse(JSON.stringify(Object.assign({}, this.itfiUpsertForm.getRawValue())));
    param.port = parseInt(param.port, 10);
    let promise = new Promise((resolve, reject) => {
      this.grpc.getIntentFinderInstanceList().subscribe(res => {
          if (res.itfi_list.map(item => item.name).indexOf(param.name) !== -1) {
            reject(new Error('duplicated ITF instanace name'));
          } else if ((res.itfi_list.map(item => item.port).indexOf(param.port) !== -1)
            && (res.itfi_list.map(item => item.ip).indexOf(param.ip) !== -1)) {
            reject(new Error('duplicated ITF instanace port number'));
          } else {
            resolve();
          }
        },
        err => {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Failed';
          ref.componentInstance.message = `Failed to get Intent Finder Instance. [${getErrorString(err)}]`;
        });
    });

    promise.then((resolve) => {
      this.grpc.insertIntentFinderInstance(param).subscribe(res => {
          this.snackBar.open(`Intent Finder Instance '${this.itfiUpsertForm.controls['name'].value}' created.`,
            'Confirm', {duration: 3000});
          // #@ itfiName에 신규로 생성된 itfiName을 입력합니다
          this.itfiName = param.name;
          this.backPage();
        }, err => {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Failed';
          ref.componentInstance.message = `Failed to create a Intent Finder Instance. [${getErrorString(err)}]`;
        }
      )
    }, (reject) => {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Failed';
      ref.componentInstance.message = reject.message;
    });
  }

  /**
   * ITF Instance Update API 호출
   */
  onEdit(): void {
    let param = JSON.parse(JSON.stringify(Object.assign({}, this.itfiUpsertForm.getRawValue())));
    param.port = parseInt(param.port, 10);
    this.grpc.updateIntentFinderInstance(param).subscribe(res => {
        this.snackBar.open(`Intent Finder Instance '${this.itfiUpsertForm.controls['name'].value}' updated.`,
          'Confirm', {duration: 3000});
        this.backPage();
      }, err => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed';
        ref.componentInstance.message = `Failed to update a Intent Finder Instance. [${getErrorString(err)}]`;
      }
    )
  }

  /**
   * role에 따라 add, save 버튼 이름 반환
   * @returns {string} 'Add' 또는 'Save'값이 반환됨
   */
  getButtonName(): string {
    return this.role === 'add' ? 'Add' : 'Save';
  }

  /**
   * upsert를 계속 진행 할 것 인지 한 번 확인을 거친 이 후에, 확인이 완료 되면 upsert를 진행한다.
   * @param {string} data 'add' 또는 'edit' 값 중 하나
   */
  openDialog(data: string): void {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.itfiUpsertForm.controls['name'].value}?'`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  /**
   * Intent Finder Instance detail View 화면으로 이동한다.
   */
  backPage: any = () => {
    this.router.navigate(['..'], {relativeTo: this.route});
  };

  /**
   * Intent Finder Instance list View 화면으로 이동한다.
   */
  backListPage: any = () => {
    if (this.role === 'edit') {
      this.router.navigate(['../../'], {relativeTo: this.route});
    } else if (this.role === 'add') {
      this.router.navigate(['..'], {relativeTo: this.route});
    } else {
      window.history.back();
    }
  };

  /**
   * radio button 클릭시 policy 값 변경 및 form value 패치
   * @param {string} policy
   */
  checkRadio(policy: string) {
    this.policyName = policy;
    this.itfiUpsertForm.patchValue({policy_name: this.policyName});
    this.cdr.detectChanges();
    this.canSubmit = true;
  }

  getIntentFinderInstanceInfo() {
    return new Promise(resolve => {
      this.grpc.getIntentFinderInstanceInfo(this.itfiName).subscribe(
        res => {
          resolve(res);
        },
        err => {
          this.snackBar.open(`Cannot Find a Intent Finder Instance Name. [${getErrorString(err)}]`,
            'Confirm', {duration: 3000});
          this.backListPage();
          return;
        }
      )
    });
  }
}
