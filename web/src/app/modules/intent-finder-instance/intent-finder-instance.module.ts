import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IntentFinderInstanceRoutingModule} from './intent-finder-instance-routing.module';
import {IntentFinderInstancesComponent} from './components/intent-finder-instances/intent-finder-instances.component';
import {IntentFinderInstanceDetailComponent} from './components/intent-finder-instance-detail/intent-finder-instance-detail.component';
import {IntentFinderInstanceUpsertComponent} from './components/intent-finder-instance-upsert/intent-finder-instance-upsert.component';
import {IntentFinderInstancePanelComponent} from './components/intent-finder-instance-panel/intent-finder-instance-panel.component';
import {SharedModule} from 'app/shared/shared.module';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import {IntentFinderInstanceTestComponent} from './components/intent-finder-policy-test/intent-finder-instance-test.component';

@NgModule({
  declarations: [
    IntentFinderInstancesComponent,
    IntentFinderInstanceDetailComponent,
    IntentFinderInstanceUpsertComponent,
    IntentFinderInstancePanelComponent,
    IntentFinderInstanceTestComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IntentFinderInstanceRoutingModule,
    SharedModule,
    MatSidenavModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatChipsModule
  ]
})
export class IntentFinderInstanceModule {
}
