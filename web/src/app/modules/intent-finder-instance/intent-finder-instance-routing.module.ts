import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IntentFinderInstancesComponent} from './components/intent-finder-instances/intent-finder-instances.component';
import {IntentFinderInstanceUpsertComponent} from './components/intent-finder-instance-upsert/intent-finder-instance-upsert.component';
import {IntentFinderInstanceDetailComponent} from './components/intent-finder-instance-detail/intent-finder-instance-detail.component';
import {IntentFinderInstanceTestComponent} from './components/intent-finder-policy-test/intent-finder-instance-test.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: IntentFinderInstancesComponent,
      },
      {
        path: 'new',
        component: IntentFinderInstanceUpsertComponent
      },
      {
        path: ':id/edit',
        component: IntentFinderInstanceUpsertComponent
      },
      {
        path: ':id/test',
        component: IntentFinderInstanceTestComponent
      },
      {
        path: ':id',
        component: IntentFinderInstanceDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntentFinderInstanceRoutingModule {
}
