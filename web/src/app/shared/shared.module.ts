import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatCommonModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatLineModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {PrettyJsonModule} from 'angular2-prettyjson';
import {TreeModule} from 'angular-tree-component';
import {NgxPaginationModule} from 'ngx-pagination';
import {ChartsModule} from 'ng2-charts';
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule
} from 'ngx-perfect-scrollbar';
import {FileUploadModule} from 'ng2-file-upload';
import {NgDragDropModule} from 'ng-drag-drop';
import {DragulaModule, DragulaService} from 'ng2-dragula/ng2-dragula';
// import shared components
import {
  AlertComponent,
  ErrorDialogComponent,
  AppEnumsComponent,
  AppObjectManagerService,
  AuthGuardDirective,
  CustomValidators,
  CustomValueUtil,
  DownloadService,
  ExcelService,
  DateFormatPipe,
  FileSizePipe,
  FilterPipe,
  FormControlErrorsComponent,
  FormControlInlineComponent,
  FormControlInputComponent,
  FormControlSectionComponent,
  FormErrorStateMatcher,
  KeysPipe,
  KeywordSectionComponent,
  KorTypeToEngPipe,
  Nl2BrPipe,
  ScriptSectionComponent,
  SecondsToTimePipe,
  TableComponent,
  TagInputComponent,
  TagInputItemComponent,
  TitleCase,
  ToolsComponent,
  UploaderButtonComponent,
  UploaderComponent
} from './index';
import {DragScrollModule} from 'ngx-drag-scroll/lib';
import {AudioPlayerComponent} from 'app/shared/components/audio-player/audio-player.component';
import {GitComponent} from 'app/shared/components/git/git.component';
import {TranscriptTextComponent} from 'app/shared/components/transcript-text/transcript.text.component';
// import {AlertComponent} from 'app/shared/components/dialog/alert.component';
import {CommitDialogComponent} from 'app/shared/components/dialog/commit-dialog.component';
import {ConfirmComponent} from 'app/shared/components/dialog/confirm.component';
import {DictionaryDictionaryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-dictionary-upsert-dialog.component';
import {DictionaryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-upsert-dialog.component';
import {DictionaryCategoryUpsertDialogComponent} from 'app/shared/components/dialog/dictionary-category-upsert-dialog.component';
import {TranscriptAlertDialogComponent} from 'app/shared/components/dialog/transcript-alert-dialog.component';
import {FileGroupDialogComponent} from 'app/shared/components/dialog/file-group-dialog.component';
import {TreeViewComponent} from 'app/shared/components/tree/tree.component';
import {TextareaAutoResizeDirective} from 'app/shared/directives/textarea-auto-resize.directive';
import {ClipboardService} from 'app/shared/services/clipboard.service';
import {PageParameters} from 'app/shared/entities/pageParameters.entity';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {CookieBrowser} from 'app/shared/storage/cookie.browser';
import {FilesComponent} from './components/test-file/file.manager';
import {AgGridModule} from 'ag-grid-angular';
import {WebEditorComponent } from './components/web-editor/web-editor.component';
import {ScenarioSimulatorComponent } from './components/scenario-simulator/scenario-simulator.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxPaginationModule,
    FileUploadModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatCommonModule,
    MatSidenavModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    MatPaginatorModule,
    MatLineModule,
    MatToolbarModule,
    MatSelectModule,
    MatCardModule,
    MatRippleModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatProgressBarModule,
    MatTooltipModule,
    ChartsModule,
    PerfectScrollbarModule,
    MatStepperModule,
    NgDragDropModule.forRoot(),
    DragulaModule,
    PrettyJsonModule,
    MatChipsModule,
    DragScrollModule,
    TreeModule,
    AgGridModule.withComponents([]),
  ],
  exports: [
    FormsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxPaginationModule,
    FileUploadModule,
    ChartsModule,
    PerfectScrollbarModule,
    MatStepperModule,
    NgDragDropModule,
    DragulaModule,
    PrettyJsonModule,
    DragScrollModule,
    ErrorDialogComponent,
    TableComponent,
    ToolsComponent,
    TagInputComponent,
    TagInputItemComponent,
    FormControlErrorsComponent,
    FormControlInputComponent,
    FormControlSectionComponent,
    FormControlInlineComponent,
    AlertComponent,
    CommitDialogComponent,
    DateFormatPipe,
    FilterPipe,
    TitleCase,
    Nl2BrPipe,
    FileSizePipe,
    SecondsToTimePipe,
    KeysPipe,
    KorTypeToEngPipe,
    AuthGuardDirective,
    UploaderComponent,
    UploaderButtonComponent,
    ScriptSectionComponent,
    KeywordSectionComponent,
    AudioPlayerComponent,
    GitComponent,
    TranscriptTextComponent,
    ConfirmComponent,
    DictionaryCategoryUpsertDialogComponent,
    DictionaryDictionaryUpsertDialogComponent,
    DictionaryUpsertDialogComponent,
    FileGroupDialogComponent,
    TranscriptAlertDialogComponent,
    TreeViewComponent,
    TextareaAutoResizeDirective,
    TreeModule,
    FilesComponent,
    WebEditorComponent,
    ScenarioSimulatorComponent
  ],
  providers: [
    AppObjectManagerService, DownloadService, ExcelService,
    AppEnumsComponent, CustomValidators, CustomValueUtil, FormErrorStateMatcher,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    DragulaService,
    ClipboardService,
    PageParameters,
    CookieBrowser,
    StorageBrowser,
    AppEnumsComponent
  ],
  declarations: [
    TableComponent, ToolsComponent, TagInputComponent, TagInputItemComponent,
    FormControlErrorsComponent, FormControlInputComponent, FormControlSectionComponent, FormControlInlineComponent,
    AlertComponent, CommitDialogComponent,
    DateFormatPipe, DateFormatPipe, FilterPipe, TitleCase, Nl2BrPipe, FileSizePipe, SecondsToTimePipe, KeysPipe, KorTypeToEngPipe,
    AuthGuardDirective, UploaderComponent, UploaderButtonComponent,
    ScriptSectionComponent,
    KeywordSectionComponent,
    AudioPlayerComponent,
    GitComponent,
    TranscriptTextComponent,
    ConfirmComponent,
    DictionaryCategoryUpsertDialogComponent,
    DictionaryDictionaryUpsertDialogComponent,
    DictionaryUpsertDialogComponent,
    FileGroupDialogComponent,
    TranscriptAlertDialogComponent,
    TreeViewComponent,
    TextareaAutoResizeDirective,
    ErrorDialogComponent,
    FilesComponent,
    WebEditorComponent,
    ScenarioSimulatorComponent
  ],
  entryComponents: [
    AlertComponent,
    CommitDialogComponent,
    ConfirmComponent,
    DictionaryCategoryUpsertDialogComponent,
    DictionaryDictionaryUpsertDialogComponent,
    DictionaryUpsertDialogComponent,
    FileGroupDialogComponent,
    TranscriptAlertDialogComponent,
    ErrorDialogComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
  ]
})
export class SharedModule {
}
