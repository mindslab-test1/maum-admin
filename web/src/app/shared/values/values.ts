// tslint:disable-next-line:max-line-length
export const IP_REGEX = /^((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))$|^((([a-zA-Z]|[a-zA-Z][a-zA-Z0-9-]*[a-zA-Z0-9]).)*([A-Za-z]|[A-Za-z][A-Za-z0-9-]*[A-Za-z0-9]))$/;
export const PORT_REGEX = /^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/;
// tslint:disable-next-line:max-line-length
export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const VARIABLE_REGEX = /^[A-Za-z0-9_-]*$/;
export const POSITIVE_INTEGER_REGEX = /^(?!0+$)[0-9]+$/;

// 적어도 대문자 1개 대문자 있는지 체크 => 1개 라도 있을 경우 true
export const CAPITAL_ONE = '(?=.*?[A-Z])';

// 연속된 문자 3개 찾기
export const EXCLUDE_SAME_THREE_CHARACTER = '(.)\\1\\1';

// 2개 이상의 알파벳 체크
export const TWO_ALPHABET = '(.*[a-zA-Z]){2}';

// 적어도 하나의 숫자 있는지 체크 => 1개라도 있을 경우 true
export const ONE_NUM = '[0-9]{1,}';

// @, #, % 있는지 체크 => 1개라도 있을 경우 false
export const EXCLUDE_CHARACTER = '^((?!@|#|%).)*$';

// 8글자~20자 미만 적어도 소문자 하나, 대문자 하나, 숫자 하나가 포함되어 있는지 체크
export const CHECK_PASS = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}/;

// 영문만 사용할 경우
export const ONLY_ENG = /^[a-zA-Z]+$/
