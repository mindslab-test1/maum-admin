import {Injectable, Output} from '@angular/core';

@Injectable()
export class AppEnumsComponent {

  @Output() langCode = {
    kor: 'Korean',
    eng: 'English'
  };

  @Output() langCodeKeys = Object.keys(this.langCode);

  @Output() lang = {
    kor: 0,
    eng: 1
  };

  @Output() langKeys = Object.keys(this.lang);

  @Output() policyLang = {
    ko_KR: 0,
    en_US: 1
  };
  @Output() policyLangKeys = Object.keys(this.policyLang);

  @Output() mediaType = {
    TEXT: 'Text only',
    ALL: 'Text & Audio',
    AUDIO: 'Audio only'
  };
  @Output() mediaTypeKeys = Object.keys(this.mediaType);

  @Output() customScriptPosition = {
    0: 'Before SC',
    1: 'Between SC and DNN',
    2: 'After DNN'
  };
  @Output() customScriptKeys = Object.keys(this.customScriptPosition);

  @Output() runtimeEnvironment = {
    RE_NATIVE: 'Native',
    RE_JAVA: 'Java',
    RE_NODE: 'Node.js',
    RE_PYTHON: 'Python',
    RE_RUBY: 'Ruby',
    RE_PHP: 'PHP'
  };
  @Output() runtimeEnvironmentKeys = Object.keys(this.runtimeEnvironment);

  @Output() dataType = {
    DATA_TYPE_INT: 'Integer',
    DATA_TYPE_STRING: 'String',
    DATA_TYPE_BOOL: 'Boolean',
    DATA_TYPE_INT_LIST: 'Integer list',
    DATA_TYPE_STRING_LIST: 'String list',
    DATA_TYPE_AUTH: 'Authentication'
  };
  @Output() dataTypeKeys = Object.keys(this.dataType);

  @Output() classifierType = {
    DNN: 'DNN Classifier',
    SC: 'Simple Classifier'
  };
  @Output() classifierTypeKeys = Object.keys(this.classifierType);

  @Output() dialogResult = {
    OK: 'OK',
    USER_INVALID: 'User Invalid Invlidated',
    SERVICE_GROUP_NOT_FOUND: 'Servic Group Not Found',
    SESSION_NOT_FOUND: 'Session Not Found',
    SESSION_INVLIDATED: 'Session Invlidated',

    EMPTY_IN: 'Empty In',

    SKILL_CLASSIFIER_FAILED: 'Skill Classifier Failed',
    DIALOG_AGENT_NOT_FOUND: 'Dialog Agent Not Found',
    DIALOG_AGENT_INVALIDATED: 'Dialog Aagent Invalidated',
    LAST_DIALOG_AGENT_LOST: 'Last Dialog Agent Lost',
    CHATBOT_NOT_FOUND: 'Chatbot Not Found',
    HUMAN_AGENT_NOT_FOUND: 'Human Agent Not Found',
    SECONDARY_DIALOG_CONFLICT: 'Secondary Dialog Conflict',
    LAST_AGENT_NOT_FOUND: 'Last Agent Not Found',
    DUPLICATED_SKILL_RESOLVED: 'Duplicated Skill Resolved',
    SECONDARY_DIALOG_AGENT_IS_MULTI_TURN: 'Secondary Dialog Agent is Multi-turn',

    INTERNAL_ERROR: 'Internal Error',
  };
  @Output() dialogResultKeys = Object.keys(this.dialogResult);

  @Output() dialogAgentSpec = {
    DAP_SPEC_V_1: 'Dialog Agent Provider V1',
    DAP_SPEC_V_2: 'Dialog Agent Provider V2',
    DAP_SPEC_V_3: 'Dialog Agent Provider V3',
  };
  @Output() dialogAgentSpecKeys = Object.keys(this.dialogAgentSpec);

  @Output() itfNlpLevel = {
    NLP_LEVEL_NOT_USE: 'Do Not Use',
    NLP_LEVEL_POS_TAGGING: 'Pos Tagging',
    // Reserved. Not Use Yet.
    // NLP_LEVEL_NAMED_ENTITY: 'Named Entity Recognition',
    // NLP_LEVEL_ANSWER_TYPE: 'Answer Type Analysis'
  };
  @Output() itfNlpLevelKeys = Object.keys(this.itfNlpLevel);

  @Output() periodType = {
    DAILY: 'Daily',
    MONTHLY: 'Monthly'
  };
  @Output() periodTypeKeys = Object.keys(this.periodType);

  @Output() searchOption = {
    date: 'date',
    sessionID: 'sessionID',
    talkID: 'talkID',
    inText: 'inText',
    outText: 'outText',
    engine: 'engine',
    category: 'category',
    result: 'result',
    satisfaction: 'satisfaction',
    accuracy: 'accuracy'
  };
  @Output() searchOptionKeys = Object.keys(this.searchOption);

  @Output() accessFromSearchValues = {
    ANONYMOUS: 0,
    WEBBROWSER: 1,
    TEXTMESSENGER: 2,
    SPEAKER: 20,
    TELEPHONE: 30,
    SMS: 40,

    // 1xx는 모바일
    MOBILE: 100,
    ANDROID: 101,
    IPHONE: 102,

    // 2xx는 SNS(카톡', 페북', ... )연동
    SNS: 200,
    KAKAOTALK: 201,
    FACEBOOK: 202,

    EXTERNALAPI: 1000
  };

  @Output() accessFromValues = {
    0: 'ANONYMOUS',
    1: 'WEB_BROWSER',
    2: 'TEXT_MESSENGER',
    20: 'SPEAKER',
    30: 'TELEPHONE',
    40: 'SMS',

    // 1xx는 모바일
    100: 'MOBILE',
    101: 'MOBILE_ANDROID',
    102: 'MOBILE_IPHONE',

    // 2xx는 SNS(카톡', 페북', ... )연동
    200: 'SNS',
    201: 'SNS_KAKAOTALK',
    202: 'SNS_FACEBOOK',

    1000: 'EXT_API'
  };

  @Output() accessFrom = {
    ANONYMOUS: 'Anonymous',
    WEB_BROWSER: 'Web Browser',
    TEXT_MESSENGER: 'Text Messenger',
    SPEAKER: 'Speaker',
    TELEPHONE: 'Telephone',
    SMS: 'SMS',

    // 1xx는 모바일
    MOBILE: 'Mobile',
    MOBILE_ANDROID: 'Android',
    MOBILE_IPHONE: 'iPhone',

    // 2xx는 SNS(카톡, 페북, ... )연동
    SNS: 'SNS',
    SNS_KAKAOTALK: 'KakaoTalk',
    SNS_FACEBOOK: 'Facebook',

    EXT_API: 'External API'
  };
  @Output() accessFromKeys = Object.keys(this.accessFrom);

  @Output() inputType = {
    // STT를 통한 입력
    SPEECH: 0,
    // 타이핑에 의한 입력
    KEYBOARD: 1,
    // CARD 등에 지정된 텍스트를 보내는 경우
    TOUTCH: 2,
    // 이미지 인식의 결과를 보내는 경우, 이미지 애노테이션을 통한 처리
    IMAGE: 3,
    // OCR 이미지 인식의 결과를 보내는 경우
    IMAGE_DOCUMENT: 4,
    // 이미지 인식의 결과를 보내는 경우
    VIDEO: 5,
    // 세션을 생성하는 등의 최초 이벤트, 프로그램 등의 명시적인 이벤트로 처리하는 방식
    OPEN_EVENT: 100,
  };
  @Output() inputTypeKeys = Object.keys(this.inputType);

  @Output() useYnCode = {
    Y: 'Use',
    N: 'Not Use'
  };
  @Output() useYnCodeKeys = Object.keys(this.useYnCode);

  @Output() mainYnCode = {
    Y: 'Use',
    N: 'Not Use'
  };
  @Output() mainYnCodeKeys = Object.keys(this.mainYnCode);

  @Output() paraphraseStep = {
    READY: 'R',
    INVERSION: 'I',
    WORD_EMBEDDING: 'W',
    SYNONYM: 'S',
    ENDING_POST_POSITION: 'E',
    COMPLETE: 'C'
  };

  @Output() paraphraseStepHtml = {
    R: 'Ready',
    I: 'Inversion',
    W: 'Word Embedding',
    S: 'Synonym',
    E: 'Ending/PostPosition',
    C: 'Complete'
  };

  constructor() {
  }

}
