import {AfterViewInit, Component, Inject, Input, OnInit, ViewEncapsulation} from '@angular/core';
import * as $ from 'jquery';
import 'jquery-ui';
import {MatDialogRef} from '@angular/material';
import {environment} from 'environments/environment';
// clare let require: any
// let $ = require('jquery');

@Component({
  selector: 'app-web-editor',
  templateUrl: './web-editor.component.html',
  styleUrls: ['./web-editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WebEditorComponent implements OnInit, AfterViewInit {

  @Input() title1: string = null;
  @Input() title2: string = null;

  @Input() text1: string = null;

  /**
   * web edit
   */
  EditPath = {
    EDIT: 'http://localhost:28084'
    , FILE: 'http://localhost:28084'
    , IMG: 'http://localhost:4200/assets/img/web_editor/'
  };

  FileExtension = {
    EXCEL: {
      img: 'i_filex16.png'
      , array: ['.xlsx', '.xlsm', '.xlsb', '.xltx', '.xls', '.xlt']
    }
    , HWP: {
      img: 'i_filex16.png'
      , array: ['.hwp']
    }
    , WORD: {
      img: 'i_filex16.png'
      , array: ['.doc', '.docx']
    }
    , PPT: {
      img: 'i_filex16.png'
      , array: ['.ppt', '.pptx']
    }
    , PDF: {
      img: 'i_filex16.png'
      , array: ['.pdf']
    }
    , ETC: {
      img: 'i_filex16.png'
      , array: []
    }
  };

  FontName = [{name: '바탕', value: '바탕'}, {name: '굴림', value: '굴림'}, {name: '돋움', value: '돋움'}, {name: '궁서', value: '궁서'}
    , {name: 'fantasy', value: 'fantasy'}, {name: 'cursive', value: 'cursive'}];

  FontSize = [10, 12, 14, 16, 20, 25];

  SymbolList = {
    '기호문자': [
      '＾', '＿', '｀', '｜', '￣', '、', '。', '·', '‥', '…', '―', '∥', '∼', '˘', '˚', '¸',
      '¡', '¿', 'ː', '‘', '’', '“', '”', '＋', '－', '＜', '＞', '±', '×', '÷', '≠', '≤',
      '≥', '∞', '∴', '♂', '♀', '∠', '⊥', '⌒', '∂', '∇', '≡', '≒', '≪', '≫', '√', '∽',
      '∝', '∵', '∫', '∬', '∈', '∋', '⊆', '⊇', '⊂', '⊃', '∪', '∩', '∧', '∨', '￢', '⇒',
      '⇔', '∀', '∃', '∮', '∑', '∏', '［', '］', '｛', '｝', '〔', '〕', '〈', '〉', '《', '》',
      '「', '」', '『', '』', '【', '】', '§', '※', '☆', '★', '○', '●', '◎', '◇', '◆', '□',
      '■', '△', '▲', '▽', '▼', '→', '←', '↑', '↓', '↔', '〓', '◁', '◀', '▷', '▶', '♤',
      '♠', '♡', '♥', '♧', '♣', '⊙', '◈', '▣', '◐', '▒', '▤', '▥', '▨', '▧', '▦', '▩',
      '♨', '☏', '☎', '☜', '☞', '¶', '†', '‡', '↕', '↗', '↙', '↖', '↘', '♭', '♩', '♪',
      '♬', '㉿', '㈜', '№', '㏇', '™', '㏂', '㏘', '℡', '®', 'ª', 'º'
    ]
    , '숫자/단위': [
      '½', '⅓', '⅔', '¼', '¾', '⅛', '⅜', '⅝', '⅞', '¹', '²', '³', '⁴', 'ⁿ', '₁', '₂',
      '₃', '₄', 'Ⅰ', 'Ⅱ', 'Ⅲ', 'Ⅳ', 'Ⅴ', 'Ⅵ', 'Ⅶ', 'Ⅷ', 'Ⅸ', 'Ⅹ', 'ⅰ', 'ⅱ', 'ⅲ', 'ⅳ',
      'ⅴ', 'ⅵ', 'ⅶ', 'ⅷ', 'ⅸ', 'ⅹ', '￦', '$', '￥', '￡', '€', '℃', 'Å', '℉', '￠', '¤',
      '‰', '㎕', '㎖', '㎗', 'ℓ', '㎘', '㏄', '㎣', '㎤', '㎥', '㎦', '㎙', '㎚', '㎛', '㎜', '㎝',
      '㎞', '㎟', '㎠', '㎡', '㎢', '㏊', '㎍', '㎎', '㎏', '㏏', '㎈', '㎉', '㏈', '㎧', '㎨', '㎰',
      '㎱', '㎲', '㎳', '㎴', '㎵', '㎶', '㎷', '㎸', '㎹', '㎀', '㎁', '㎂', '㎃', '㎄', '㎺', '㎻',
      '㎼', '㎽', '㎾', '㎿', '㎐', '㎑', '㎒', '㎓', '㎔', 'Ω', '㏀', '㏁', '㎊', '㎋', '㎌', '㏖',
      '㏅', '㎭', '㎮', '㎯', '㏛', '㎩', '㎪', '㎫', '㎬', '㏝', '㏐', '㏓', '㏃', '㏉', '㏜', '㏆'
    ]
    , '원/괄호': [
      '㈀', '㈁', '㈂', '㈃', '㈄', '㈅', '㈆', '㈇', '㈈', '㈉', '㈊', '㈋', '㈌', '㈍', '㈎', '㈏',
      '㈐', '㈑', '㈒', '㈓', '㈔', '㈕', '㈖', '㈗', '㈘', '㈙', '㈚', '㈛', '⒜', '⒝', '⒞', '⒟',
      '⒠', '⒡', '⒢', '⒣', '⒤', '⒥', '⒦', '⒧', '⒨', '⒩', '⒪', '⒫', '⒬', '⒭', '⒮', '⒯',
      '⒰', '⒱', '⒲', '⒳', '⒴', '⒵', '⑴', '⑵', '⑶', '⑷', '⑸', '⑹', '⑺', '⑻', '⑼', '⑽',
      '⑾', '⑿', '⒀', '⒁', '⒂', '㉠', '㉡', '㉢', '㉣', '㉤', '㉥', '㉦', '㉧', '㉨', '㉩', '㉪',
      '㉫', '㉬', '㉭', '㉮', '㉯', '㉰', '㉱', '㉲', '㉳', '㉴', '㉵', '㉶', '㉷', '㉸', '㉹', '㉺',
      '㉻', 'ⓐ', 'ⓑ', 'ⓒ', 'ⓓ', 'ⓔ', 'ⓕ', 'ⓖ', 'ⓗ', 'ⓘ', 'ⓙ', 'ⓚ', 'ⓛ', 'ⓜ', 'ⓝ', 'ⓞ',
      'ⓟ', 'ⓠ', 'ⓡ', 'ⓢ', 'ⓣ', 'ⓤ', 'ⓥ', 'ⓦ', 'ⓧ', 'ⓨ', 'ⓩ', '①', '②', '③', '④', '⑤',
      '⑥', '⑦', '⑧', '⑨', '⑩', '⑪', '⑫', '⑬', '⑭', '⑮'
    ]
    , '그리스/러시아어': [
      'Α', 'Β', 'Γ', 'Δ', 'Ε', 'Ζ', 'Η', 'Θ', 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π',
      'Ρ', 'Σ', 'Τ', 'Υ', 'Φ', 'Χ', 'Ψ', 'Ω', 'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ',
      'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'σ', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω',
      'Б', 'Д', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'С', 'У', 'Ц',
      'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё',
      'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х',
      'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
    ]
  };

  ColorPickers = [
    {code: '#000000', name: 'black'},
    {code: '#8B0000', name: 'darkred'},
    {code: '#006400', name: 'darkgreen'},
    {code: '#191970', name: 'midnightblue'},
    {code: '#FF8C00', name: 'darkorange'},
    {code: '#696969', name: 'dimgray'},
    {code: '#DC143C', name: 'crimson'},
    {code: '#228B22', name: 'forestgreen'},
    {code: '#00008B', name: 'darkblue'},
    {code: '#FFD700', name: 'gold'},
    {code: '#808080', name: 'gray'},
    {code: '#FF0000', name: 'red'},
    {code: '#008000', name: 'green'},
    {code: '#0000FF', name: 'blue'},
    {code: '#FFE400', name: 'yellow'},
    {code: '#A9A9A9', name: 'darkgray'},
    {code: '#FA8072', name: 'salmon'},
    {code: '#7CFC00', name: 'lawngreen'},
    {code: '#48D1CC', name: 'mediumturquoise'},
    {code: '#FFFACD', name: 'lemonchiffon'},
    {code: '#C0C0C0', name: 'silver'},
    {code: '#FFC0CB', name: 'pink'},
    {code: '#00FF00', name: 'lime'},
    {code: '#E0FFFF', name: 'lightcyan'},
    {code: '#FFFFE0', name: 'lightyellow'}
  ];

  // 특수 메뉴
  tableMenu;
  colorMenu;
  fontNameMenu;
  fontSizeMenu;
  symbolTable;
  attachFile;

  /**
   * ajax fn
   * @param {String} path api url
   * @param {String} methodType post, get, put etc..
   * @param {Object} params 전송 데이터
   * @param {Object} loadingBar ajax 통신 완료 전에 보여줄 로딩바
   * @param {Function} callBack ajax 통신 완료 후 실행할 함수
   * @param {Function} errorBack ajax 통신 에러 발생시 실행할 함수
   * @param {Boolean} async 비동기(true)/동기(false)
   */
  $SimpleAjax = function (path, methodType, params, loadingBar, callBack, errorBack, async) {
    $.ajax({
      url: path,
      type: methodType,
      data: params,
      headers: {'date-format': 'yyyy-MM-dd hh:mm:ss'},
      contentType: 'application/json',
      dataType: 'json',
      cache: false,
      async: async,
      beforeSend: function () {
        if (loadingBar) {
          loadingBar.show();
        }
      },
      success: function (data) {
        if (data.resultCode === 200) {
          if ($.isFunction(callBack)) {
            callBack(data.resultData);
          }
        } else {
          if ($.isFunction(errorBack)) {
            errorBack(data);
          }
        }
      },
      error: function (request, status, error) {
        let errorMsg = null;
        if (request.responseJSON) {
          errorMsg = request.responseJSON.message;
        } else {
          errorMsg = error ? error : '서버와의 통신에 실패하였습니다';
        }
        alert(errorMsg);
      },
      complete: function () {
        if (loadingBar) {
          loadingBar.hide();
        }
      }
    });
  }

  constructor(public dialogRef: MatDialogRef<any>) {
  }

  ngOnInit() {
    $('#editSymbolTablePopTemplate').hide();
  }

  ngAfterViewInit() {
    let component = this;

    $.fn['wecomsSimpleWebEdit'] = function(opt) {
      let $this = this;
      $this.wecomsSimpleWebEdit.option = $.extend({}, $this.wecomsSimpleWebEdit.option, opt || {});
      let _focusHandler = component.EditFocus($this);
      let activeEditArea;
      let options = $this.wecomsSimpleWebEdit.option;
      let _editToolBar = $('<div id="wecoms-simple-edit-tool">');
      let _editMenuBar = $('<div id="wecoms-simple-edit-menu">');
      let _menu = $('<div>').addClass('wecoms-edit-menu');
      let _editAreas = new Array();
      let _contextmenuYn = false;
      if (options.style) {
        let _style = $('<link href=" + options.style + " rel="stylesheet" />');
        $('head').append(_style);
      }
      $this.addClass('wecoms_simple_web_edit');
      $this.append(_editToolBar);
      $this.append(_editMenuBar);
      $.each(options.btns, function (i, v) {
        if (options.disabledBtnIndex.indexOf(i) > -1) {
          return true;
        }
        let btn = $(v.tag ? v.tag : '<button>').text(v.name);
        _editToolBar.append(btn);
        if (v.styleClass) {
          btn.addClass(v.styleClass);
        }
        if ($.isFunction(v.beforeFn)) {
          v.beforeFn.apply(btn, [$this]);
        }
        btn.on('click', function () {
          _menu.hide();
          _editMenuBar.children().hide();
          v.fn.apply(this, [$(activeEditArea), _focusHandler, btn]);
          return false;
        });
        if ($.isFunction(v.afterFn)) {
          v.afterFn.apply(btn, [$this]);
        }
      });
      $this.append($('<div>').addClass('section_edit_template'));
      $this.append($('<div>').addClass('section_html_template').hide());
      $this.addEditForm = function (customTemp, clearYn) {
        $this.clear();
        if (clearYn) {
          $.each(_editAreas, function (i, v) {
            v.remove();
          });
          _editAreas = new Array();
        }
        let $temp = (customTemp) ? $(customTemp) : ($.isFunction(options.selectTemplateFn)) ? options.selectTemplateFn() : $('<div class="basic-edit-area"><div class="wecoms-edit-area">');
        let $editArea = ($temp.find('.wecoms-edit-area').length > 0) ? $temp.find('.wecoms-edit-area') : $temp.find('div');
        $editArea.addClass('edit_template');
        $editArea.prop('contenteditable', true);
        $editArea.on('focus', function () {
          activeEditArea = this;
        });
        $editArea.on('blur', function () {
          if (!_contextmenuYn) {
            _focusHandler.saveCurrentRange();
          }
        });
        $editArea.on('click', function () {
          _editMenuBar.children().hide();
        });
        if (options.menu) {
          $.each(options.menu, function (i, v) {
            if (options.disabledMenuIndex.indexOf(i) > -1) {
              return true;
            }

            let item = $('<p>').text(v.name).on('click', function (e) {
              _menu.hide();
              v.fn.call(null, $(activeEditArea), _focusHandler);
            });
            _menu.append(item);
          });
          $editArea.on('contextmenu', function (e) {
            e.preventDefault();
            _contextmenuYn = true;
            _focusHandler.saveCurrentRange();
            _editMenuBar.children().hide();
            _editMenuBar.children().hide();
            _menu.css({top: e.pageY + 'px', left: e.pageX + 'px'});
            $this.append(_menu);
            _menu.show();
          }).on('click', function (e) {
            _contextmenuYn = false;
            _menu.hide();
          });
        }
        _editAreas.push($temp);
        $this.find('.section_edit_template').append($temp);
        activeEditArea = $editArea;
        _focusHandler.saveCurrentRange();
      }
      $this.setData = function (data, index) {
        if (index) {
          _editAreas[index].find('.edit_template').html(data);
        } else {
          _editAreas[0].find('.edit_template').html(data);
        }
      }
      $this.getData = function (index) {
        let result = '';
        if (index) {
          result = _editAreas[index].find('.edit_template').html();
        } else {
          result = _editAreas[0].find('.edit_template').html();
        }
        return result;
      }
      $this.getFindData = function (selector) {
        return _editAreas[0].find('.edit_template').find(selector);
      }
      $this.getAllData = function () {
        let result = new Array();
        $.each(_editAreas, function (i, v) {
          let contents = new Array();
          $.each(v.find('.edit_template'), function (i2, v2) {
            contents.push(v2.innerHTML);
          });
          result.push(contents);
        })
        return result;
      }
      $this.clear = function () {
        _editMenuBar.children().hide();
        _menu.empty();
        $this.find('.section_html_template').css('display', 'none');
        $this.find('.section_html_template').empty();
      }
      $this.addEditForm();
      $(document).off('scroll').on('scroll', function (e) {
        _editMenuBar.children().hide();
      });
      return $this;
    }

    // 색상메뉴
    $.fn['wecomsToggleColorMenu'] = function (paletteFn, area) {
      $('#wecoms-toggle-color-menu').remove();
      let $this = this;
      let _div = $('<div>');
      let colorPalette = $('<canvas></canvas>').get(0);
      let paletteContext = colorPalette[0].getContext('2d');
      let paletteWidth = 255;
      let paletteHeight = 255;
      let naviBar = $('<canvas></canvas>').css('margin-left', '10px').get(0);
      let naviBarContext = naviBar[0].getContext('2d');
      let naviBarWidth = 10;
      let naviBarHeight = 255;
      let paletteCallBack = paletteFn;

      function integerToHex(val) {
        let hex = val.toString(16);
        return hex.length === 1 ? '0' + hex : hex;
      }

      _div.attr('id', 'wecoms-toggle-color-menu');
      _div.width(paletteWidth + naviBarWidth + 10).height(paletteHeight);
      colorPalette['init'] = function () {
        $(colorPalette).attr({width: paletteWidth, height: paletteHeight}).css({width: paletteWidth, height: paletteHeight});
        $(colorPalette).off().on('click', function (e) {
          let _x = e.pageX - $(colorPalette).offset().left;
          let _y = e.pageY - $(colorPalette).offset().top;
          let _data = paletteContext.getImageData(_x, _y, 1, 1).data;
          let _colorCode = integerToHex(_data[0]) + integerToHex(_data[1]) + integerToHex(_data[2]);
          if ($.isFunction(paletteCallBack)) {
            paletteCallBack('#' + _colorCode);
          }
        });
      }

      colorPalette['drawing'] = function (r, g, b) {
        for (let i = 0; i < 255;) {
          let _color = 255 - i;
          let _paletteGradient = paletteContext.createLinearGradient(0, 0, paletteWidth, 0);
          _paletteGradient.addColorStop(0, 'rgb(' + _color + ', ' + _color + ',' + _color + ')');
          _paletteGradient.addColorStop(1, 'rgb(' + r + ', ' + g + ',' + b + ')');
          paletteContext.fillStyle = _paletteGradient;
          paletteContext.fillRect(0, i++, 255, i);
          if (r > 0) {
            r--;
          }
          if (g > 0) {
            g--;
          }
          if (b > 0) {
            b--;
          }
        }
      }

      naviBar['init'] = function () {
        $(naviBar).attr({width: naviBarWidth, height: naviBarHeight}).css({width: naviBarWidth, height: naviBarHeight});
        let _naviGradient = naviBarContext.createLinearGradient(0, 0, naviBarWidth, naviBarHeight);
        _naviGradient.addColorStop(0, '#FF0000');
        _naviGradient.addColorStop(0.166, '#FF00FF');
        _naviGradient.addColorStop(0.333, '#0000FF');
        _naviGradient.addColorStop(0.5, '#00FFFF');
        _naviGradient.addColorStop(0.666, '#00FF00');
        _naviGradient.addColorStop(0.834, '#FFFF00');
        _naviGradient.addColorStop(1, '#FF0000');
        naviBarContext.fillStyle = _naviGradient;
        naviBarContext.fillRect(0, 0, naviBarWidth, naviBarHeight);
        $(naviBar).off().on('click', function (e) {
          let _cursor = e.clientY - $(naviBar).offset().top;
          let _data = naviBarContext.getImageData(0, _cursor, 1, 1).data;
          colorPalette['drawing'](_data[0], _data[1], _data[2]);
        });
      }

      this.show = function () {
        this.fixedMenuPositioningByTarget($this, _div);
        _div.css('display', 'block');
      }
      this.hide = function () {
        _div.css('display', 'none');
      }
      this.paletteCallBack = function (callBack) {
        paletteCallBack = callBack;
      }
      _div.append(colorPalette);
      _div.append(naviBar);
      naviBar['init']();
      colorPalette['init']();
      colorPalette['drawing'](255, 0, 0);
      area.append(_div);
      _div.hide();
      return this;
    }

    $.fn['wecomsSimpleWebEdit']['option'] = {
      template: '',
      style: '',
      btns: [
        {name: '바탕', beforeFn: this.beforeModifyFontName, fn: this.modifyFontName, styleClass: 'select_box_btn', tag: '<button title="글꼴">'} /*글꼴*/
        , {name: '12px', beforeFn: this.beforeFontSize, fn: this.modifyFontSize, styleClass: 'select_box_btn', tag: '<button title="글씨크기">'} /*글씨크기*/
        , {name: '', beforeFn: this.beforeForeColor, fn: this.foreColor, styleClass: 'foreColor', tag: '<button title="글씨색상">'} /*글씨색상*/
        , {name: '', fn: this.execCommandBold, styleClass: 'execCommandBold', tag: '<button title="볼드체">'} /*볼드체*/
        , {name: '', fn: this.execCommandItalic, styleClass: 'execCommandItalic', tag: '<button title="이탤릭">'} /*이탤릭*/
        , {name: '', fn: this.execCommandUnderline, styleClass: 'execCommandUnderline', tag: '<button title="밑줄">'} /*밑줄*/
        , {name: '', fn: this.execCommandStrikeThrough, styleClass: 'execCommandStrikeThrough', tag: '<button title="취소선">'} /*취소선*/
        , {name: '', fn: this.execCommandJustifyleft, styleClass: 'execCommandJustifyleft', tag: '<button title="왼쪽정렬">'} /*왼쪽정렬*/
        , {name: '', fn: this.execCommandJustifyCenter, styleClass: 'execCommandJustifyCenter', tag: '<button title="가운데정렬">'} /*가운데정렬*/
        , {name: '', fn: this.execCommandJustifyRight, styleClass: 'execCommandJustifyRight', tag: '<button title="오른쪽정렬">'} /*오른쪽정렬*/
        , {name: '', fn: this.addLink, styleClass: 'addLink', tag: '<button title="링크삽입">'} /*링크삽입*/
        , {name: '', beforeFn: this.beforeAddTable, fn: this.addTable, styleClass: 'addTable', tag: '<button title="표삽입">'} /*표삽입*/
        , {name: '', beforeFn: this.beforeAddAttachFile, fn: this.addAttachFile, styleClass: 'sampleBtn', tag: '<button title="첨부파일">'} /*첨부파일*/
        , {name: '', beforeFn: this.beforeSymbolTable, fn: this.addSymbolTable, styleClass: 'addSymbolTable', tag: '<button title="특수문자">'} /*특수문자*/
        , {name: '', fn: this.addCaption, styleClass: 'addCaption', tag: '<button title="캡션">'} /*캡션*/
        , {name: 'HTML표출', fn: this.showHtml, styleClass: 'showHtml', tag: '<button>'},
      ],
      disabledBtnIndex: [],
      addBtns: [/*{name: '', beforeFn : function(){}, fn:function(){}, styleClass:'class', tag : '<button>'}*/],
      menu: [
        {name: '강조', fn: this.execCommandBold}
        , {name: '기울림', fn: this.execCommandItalic}
        , {name: '밑줄', fn: this.execCommandUnderline}
        , {name: '취소선', fn: this.execCommandStrikeThrough}
        , {name: '왼쪽정렬', fn: this.execCommandJustifyleft}
        , {name: '가운데정렬', fn: this.execCommandJustifyCenter}
        , {name: '오른쪽정렬', fn: this.execCommandJustifyRight}
      ],
      disabledMenuIndex: [],
      addMenu: [/*{name: '', fn : function () {} }*/],
      selectTemplateFn: null,
      bindTemplateEventFn: null
    }

    let _advPath = '../..';
    this.EditPath = {
      EDIT : _advPath,
      FILE : null,
      IMG : document.location.protocol + '//' + document.location.hostname + ':' + document.location.port + '/assets/img/web_editor/'
    }

    let opt = {
      disabledBtnIndex: [12]
    }

    $('#edit')['wecomsSimpleWebEdit'](opt);
  }


  addLink = ($focusArea, focusHandler, $this) => {
    let url = prompt('링크 입력', 'http://');
    if (!url) {
      alert('주소를 입력하지 않았습니다.');
      return false;
    }
    let _a = $('<a>').attr('href', url).attr('target', '_blank').text('링크');
    focusHandler.execCommand(function (selectContents) {
      _a.html(selectContents);
      return _a[0];
    }, _a[0]);
  }

  showHtml = ($focusArea, focusHandler, $this) => {
    let _data = [[this.getHtmlData()[1]]];
    $('.section_html_template').css('display', 'block');
    $('.section_html_template').empty();
    $.each(_data, function (i, v) {
      let _div = $('<div>');
      $.each(v, function (j, val) {
        _div.append($('<p>').text(val));
      });
      $('.section_html_template').append(_div);
    });
  }

  beforeAddTable = ($this) => {
    let component = this;
    component.tableMenu = $('<div>').addClass('table-input-box');
    let _inputTr = $('<input type="number" value=1>');
    let _inputTd = $('<input type="number" value=1>');
    let _renderTableBtn = $('<input type="button" value="확인">');
    component.tableMenu.append(_inputTr).append(_inputTd).append(_renderTableBtn);
    component.tableMenu.hide();
    component.tableMenu['clear'] = function () {
      _inputTr.val(1);
      _inputTd.val(1);
    }
    $this.find('#wecoms-simple-edit-menu').append(component.tableMenu);
    _renderTableBtn.on('click', function () {
      let _tr = component.tableMenu.find('input').eq(0).val();
      let _td = component.tableMenu.find('input').eq(1).val();
      if (1 > _tr || 20 < _tr) {
        alert('테이블 행은 최소 1, 최대 20행까지 가능합니다.');
        return;
      }
      if (1 > _td || 20 < _td) {
        alert('테이블 열은 최소 1, 최대 20열까지 가능합니다.');
        return;
      }
      let _table = $('<table>');
      for (let i = 0; i < _tr; i++) {
        _table.append($('<tr>'));
      }
      for (let j = 0; j < _td; j++) {
        _table.find('tr').append($('<td>&nbsp</td>'));
      }
      component.tableMenu.trigger('appendTable', [_table]);
      component.tableMenu.css('display', 'none');
    })
  }

  addTable = ($focusArea, focusHandler, $this) => {
    let component = this;
    component.fixedMenuPositioningByTarget($this, this.tableMenu);
    component.tableMenu.clear();
    component.tableMenu.off('appendTable').on('appendTable', function (e, table) {
      $focusArea.append(table);
      $focusArea.append($('<br>'));
    })
    component.tableMenu.css('display', 'block');
  }

  beforeForeColor = ($this) => {
    // colorMenu = $(this).wecomsToggleColorMenu(null, $this.find('#wecoms-simple-edit-menu'));
    let component = this;
    component.colorMenu = $('<div>').addClass('color-menu-area');
    let _maxCol = 5;
    let _area = $('<div>').addClass('color-table');
    $.each(component.ColorPickers, function (i, v) {
      let _div = $('<div>').addClass('color-table-cell')
        .css({'width': '15px', 'height': '15px', 'background-color': v.code})
        .attr({'title': v.name, 'data-code': v.code});
      if (i % _maxCol === 0) {
        _area.append($('<div>').addClass('color-table-row'));
      }
      _area.find('.color-table-row:last-child').append(_div);
    });
    component.colorMenu.append(_area);
    component.colorMenu['clear'] = function () {

    }
    component.colorMenu.find('.color-table-cell').on('click', function () {
      component.colorMenu.trigger('addColor', [$(this).attr('data-code')]);
    });
    $this.find('#wecoms-simple-edit-menu').append(component.colorMenu);
    component.colorMenu.clear();
    component.colorMenu.css('display', 'none');
  }

  foreColor = ($focusArea, focusHandler, $this) => {
    /*let _paletteCallBack = function (colorCode) {
            let _el = $('<font>').attr('color', colorCode);
            focusHandler.execCommand(function (selectContents){
                removeTag(selectContents, 'font[color]');
                _el.html(selectContents);
                return _el[0];
            }, _el[0]);
            colorMenu.hide()
        }
        colorMenu.paletteCallBack(_paletteCallBack);
        colorMenu.show();*/
    let component = this;
    this.fixedMenuPositioningByTarget($this, component.colorMenu);
    component.colorMenu.off('addColor').on('addColor', function (e, colorCode) {
      let _el = $('<font>').attr('color', colorCode);
      focusHandler.execCommand(function (selectContents) {
        component.removeTag(selectContents, 'font[color]');
        _el.html(selectContents);
        return _el[0];
      }, _el[0]);
      component.colorMenu.hide()
    });
    component.colorMenu.css('display', 'block');
  }

  beforeModifyFontName = ($this) => {
    let component = this;
    let ol = $('<ol>').addClass('select_box');
    $.each(component.FontName, function (i, v) {
      let _li = $('<li>').attr('data-value', v.value).text(v.name);
      ol.append($(_li));
    });
    ol.find('li').on('click', function () {
      $(this).trigger('changeFont');
    });
    ol.hide();
    $this.find('#wecoms-simple-edit-menu').append(ol);
    component.fontNameMenu = ol;
  }

  modifyFontName = ($focusArea, focusHandler, $this) => {
    let component = this;
    component.fixedMenuPositioningByTarget($this, component.fontNameMenu);
    component.fontNameMenu.css({'width': $this.outerWidth()});
    component.fontNameMenu.find('li').off('changeFont').on('changeFont', function () {
      let _val = $(this).attr('data-value');
      let _el = $('<font>').attr('face', _val);
      focusHandler.execCommand(function (selectContents) {
        component.removeTag(selectContents, 'font[face]');
        _el.html(selectContents);
        return _el[0];
      }, _el[0]);
      component.fontNameMenu.css('display', 'none');
      $this.text(_val);
    });
    component.fontNameMenu.css('display', 'block');
  }

  beforeFontSize = ($this) => {
    let component = this;
    let ol = $('<ol>').addClass('select_box');
    $.each(this.FontSize, function (i, v) {
      let _li = $('<li>').attr('data-value', v).text(v);
      ol.append(_li);
    });
    ol.find('li').on('click', function () {
      $(this).trigger('changeSize');
    });
    ol.hide();
    $this.find('#wecoms-simple-edit-menu').append(ol);
    component.fontSizeMenu = ol;
  }

  modifyFontSize = ($focusArea, focusHandler, $this) => {
    let component = this;
    component.fixedMenuPositioningByTarget($this, component.fontSizeMenu);
    component.fontSizeMenu.css({'width': $this.outerWidth()});
    component.fontSizeMenu.find('li').off('changeSize').on('changeSize', function () {
      let _val = $(this).attr('data-value');
      let _el = $('<span>').css('font-size', _val + 'px');
      focusHandler.execCommand(function (selectContents) {
        component.removeTag(selectContents, 'span');
        _el.html(selectContents);
        return _el[0];
      }, _el[0]);
      component.fontSizeMenu.css('display', 'none');
      $this.text(_val + 'px');
    });
    component.fontSizeMenu.css('display', 'block');
  }

  beforeSymbolTable = ($this) => {
    let component = this;
    let _maxCol = 15;
    let keys = Object.keys(component.SymbolList);
    component.symbolTable = $('#editSymbolTablePopTemplate');
    let _inputVal = component.symbolTable.find('#select-symbol-val');
    for (let index in keys) {
      let key = keys[index];
      let symbols = this.SymbolList[key];
      let _tab = $('<li>').text(key);
      let _table = $('<table>');
      _table.append('<tr>');
      component.symbolTable.find('ul').append(_tab);
      $.each(symbols, function (i: number, v) {
        let _td = $('<td>').text(v);
        if (i % _maxCol === 0) {
          _table.append('<tr>');
        }

        _table.find('tr:last-child').append(_td);
      });
      this.symbolTable.find('#edit-symbol-table').append(_table);
      _tab.on('click', function () {
        let _i = $(this).index();
        component.symbolTable.find('#edit-symbol-table table').css('display', 'none');
        component.symbolTable.find('#edit-symbol-table table').eq(_i).css('display', 'block');
      });
    }
    component.symbolTable['clear'] = function () {
      _inputVal.val('');
      component.symbolTable.find('#edit-symbol-table table').css('display', 'none');
      component.symbolTable.find('#edit-symbol-table table').eq(0).css('display', 'block');
    }
    component.symbolTable.find('table td').on('click', function () {
      _inputVal.val(_inputVal.val() + this.innerText);
    });
    component.symbolTable.find('#btn-add-symbol').on('click', function () {
      component.symbolTable.trigger('addSymbol', [_inputVal.val()]);
    });
    component.symbolTable.find('#pop-symbol-exit').on('click', function () {
      component.symbolTable.css('display', 'none');
    });
    $this.find('#wecoms-simple-edit-menu').append(this.symbolTable);
    component.symbolTable.clear();
    component.symbolTable.css('display', 'none');
  }

  addSymbolTable = ($focusArea, focusHandler, $this) => {
    let component = this;
    component.symbolTable.off('addSymbol').on('addSymbol', function (e, val) {
      if (val === '') {
        alert('특수문자를 선택해주세요.')
        return false;
      }
      let _selectSymbol = $('<span>').append(val);
      focusHandler.execCommand(function (selectContents) {
        return _selectSymbol[0];
      }, _selectSymbol[0]);
      component.symbolTable.css('display', 'none');
    });
    component.symbolTable.clear();
    component.symbolTable.css('display', 'block');
  }

  execCommandBold = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('bold');
  }

  execCommandItalic = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('Italic');
  }

  execCommandUnderline = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('Underline');
  }

  execCommandStrikeThrough = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('StrikeThrough');
  }

  execCommandJustifyleft = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('justifyLeft');
  }

  execCommandJustifyCenter = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('justifyCenter');
  }

  execCommandJustifyRight = ($focusArea, focusHandler, $this) => {
    focusHandler.loadSelection();
    document.execCommand('justifyRight');
  }

  beforeAddAttachFile = ($this) => {
    let component = this;
    component.attachFile = this.CreateAttachFilePopup(component.EditPath.FILE, $this.find('#wecoms-simple-edit-menu'));
    component.attachFile['attchFile'] = function () {
      $this.trigger('attachFile');
    }
    this.attachFile['addFileLink'] = function () {
      $this.trigger('addFileLink');
    }
    $this.find('#wecoms-simple-edit-menu').append(component.attachFile);
  }

  addAttachFile = ($focusArea, focusHandler, $this) => {
    let component = this;
    component.attachFile.clear();

    $this.off('attachFile').on('attachFile', function () {
      let selectFile = component.attachFile.getSelectFile();
      if (selectFile.length === 0) {
        alert('파일을 선택해주세요.');
        return;
      }
      let _fileData = selectFile.data('data');
      let _a = $('<a download>').attr('href', _fileData.filePath);
      _a.append(component.attachFile.getFileExtenstionIcon(_fileData.fileExtenstion)).append(_fileData.orginalfileName);
      $focusArea.append(_a);
      component.attachFile.hide();
    });
    $this.off('addFileLink').on('addFileLink', function () {
      let selectFile = component.attachFile.getSelectFile();
      if (selectFile.length === 0) {
        alert('파일을 선택해주세요.');
        return;
      }
      let _fileData = selectFile.data('data');
      let _a = $('<a download>').attr('href', _fileData.filePath).html('첨부');
      focusHandler.execCommand(function (selectContents) {
        _a.html((selectContents.textContent === '') ? '첨부' : selectContents);
        return _a[0];
      }, _a[0]);
      component.attachFile.hide();
    });
    component.attachFile.show();
  }

  addCaption = ($focusArea, focusHandler, $this) => {
    let component = this;
    let caption = prompt('툴팁 입력', '툴팁을 입력해주세요.');
    if (!caption) {
      alert('문구를 입력하지 않았습니다.');
      return false;
    }
    let _img = $('<img>').attr('src', component.EditPath.IMG + 'addCaption.png').attr('title', caption);
    focusHandler.execCommand(function (selectContents) {
      return _img[0];
    }, _img[0]);
  }

  CreateAttachFilePopup = (serverUrl, area) => {
    /*if (!serverUrl) {
      alert('파일서버 경로를 입력해주세요.');
      return;
    }
    let _this = this;
    let _serverUrl = serverUrl;
    let _dataPath = _serverUrl + '/rest/api/v1/files/';
    let _categoryPath = _serverUrl + '/rest/api/v1/file/category/';
    let _downloadPath = _serverUrl + '/rest/api/v1/file/download/';
    let popTemp = $('#editAttachFilePopTemplate').tmpl();
    let _fileForm = popTemp.find('form[name="edit - file"]');
    let _thead = popTemp.find('#header-table thead');
    let _tbody = popTemp.find('#tbody-table tbody');
    area.append(popTemp);

    function parseData(data) {
      data.radio = $('<input type="radio" name="file - radio">');
      data.filePath = _downloadPath + data.fileEntityId;
      if (data.updateDate) {
        data.updateDateTime = data.updateDate.split('.')[0];
      }

      return data;
    }

    this.$SimpleAjax(_categoryPath, 'GET', {}, null, function (data) {
      let _categoryBox = popTemp.find('select[name="fileCategoryEntityId"]');
      $.each(data.category, function (i, v) {
        _categoryBox.append($('<option>').val(v.fileCategoryEntityId).text(v.fileCategoryCode));
      });
    }, null, true);
    _this['dataRender'] = function (data) {
      _tbody.empty();
      let _rows = data.file;
      if (_rows.length > 0) {
        $.each(_rows, function (rowsIndex, rowsVal) {
          let _fileData = parseData(rowsVal);
          let _tr = $('<tr>').data('data', _fileData);
          $.each(_thead.find('th'), function (headIndex, headVal) {
            let _td = $('<td>');
            _td.append(_fileData[$(headVal).attr('data-name')]);
            _tr.append(_td);
          });
          _tbody.append(_tr);
        });
      } else {
        let _tr = $('<tr>');
        let _td = $('<td>');
        _td.attr('colspan', _thead.find('th').length);
        _td.text('검색 결과가 없습니다.');
        _tr.append(_td);
        _tbody.append(_tr);
      }
    }
    _this['addFileLink'] = function () {
      //custom
    }
    _this['attchFile'] = function () {
      //custom
    }
    _this['getSelectFile'] = function () {
      return _tbody.find('input[type="radio"]:checked').parents('tr');
    }
    _this['show'] = function () {
      popTemp.css('display', 'block');
    }
    _this['hide'] = function () {
      popTemp.css('display', 'none');
    }
    _this['getFileExtenstionIcon'] = function (extenstion) {
      let _icon = $('<img>');
      for (let obj in this.FileExtension) {
        let _val = this.FileExtension[obj];
        if (_val.array.indexOf(extenstion) > -1) {
          return _icon.attr('src', this.EditPath.IMG + _val.img);
        }
      }
      return _icon.attr('src', this.EditPath.IMG + this.FileExtension.ETC.img);
    }
    _this['clear'] = function () {
      _fileForm[0].reset();
      popTemp.find('select[name="searchOptWord"]').val('all');
      popTemp.find('input[name="searchInput"]').val('');
      popTemp.find('#btn-search-file').click();
    }
    popTemp.find('#btn-sumit-file').on('click', function () {
      if (!_fileForm.find('input[type="file"]').val()) {
        alert('파일을 첨부해주세요.');
        return;
      }
      _fileForm.attr('action', _dataPath).submit();
      _fileForm.click();
      _fileForm[0].reset();
    });
    popTemp.find('#btn-search-file').on('click', function () {
      let param = {
        searchOptWord: popTemp.find('select[name="searchOptWord]').val(), searchInput: popTemp.find('input[name="searchInput"]').val()
      }
      this.$SimpleAjax(_dataPath, 'GET', param, null, _this.dataRender, null, true);
    });
    popTemp.find('#pop-attach-exit').on('click', function () {
      popTemp.toggle();
    });
    popTemp.find('#btn-attach-file').on('click', function () {
      _this.attchFile();
    });
    popTemp.find('#btn-add-link').on('click', function () {
      _this.addFileLink();
    });
    popTemp.find('#btn-search-file').click();
    popTemp.hide();
    return _this;*/
  }


  removeTag = (target, selector) => {
    // let regex = new RegExp('<(\/'+preTag+'|'+endTag+')([^>]*)>');
    let tags = $(target).find(selector);
    $.each(tags, function (i, v) {
      $(v).replaceWith(v.innerHTML);
    });
  }


  EditFocus = (target) => {
    let $target = (target) ? target : $('body');
    let currentRange = null;
    let beforeContents = null;
    $target['getRange'] = function () {
      let _range;
      /*		if(activeEditArea){
                  activeEditArea.focus();
              }
      */
      if (window.getSelection) {
        _range = (window.getSelection().rangeCount > 0) ? window.getSelection().getRangeAt(0) : document.createRange();
      } else {
        // _range = document.body.createTextRange();
      }
      return _range;
    }
    $target['saveCurrentRange'] = function () {
      currentRange = this.getRange();
      beforeContents = this.getContents();
      return currentRange;
    }
    $target['loadSelection'] = function () {
      let _newRange = this.getRange();
      let _selction = document.getSelection();
      _newRange.setStart(currentRange.startContainer, currentRange.startOffset);
      _newRange.setEnd(currentRange.endContainer, currentRange.endOffset);
      _selction.removeAllRanges();
      _selction.addRange(_newRange);
    }
    $target['getContents'] = function () {
      return currentRange.cloneContents();
    }
    $target['defaultInjection'] = function (el) {
      let _selction = document.getSelection();
      $target.find('.wecoms-edit-area:last').append(el);
      _selction.removeAllRanges();
      _selction.selectAllChildren(el);
    }
    $target['execCommand'] = function (commandFn, changeEl) {
      let _selction = document.getSelection();
      let _renderEl;
      let _unkownFocusYn = ($target.find('.section_edit_template').find(currentRange.commonAncestorContainer).length > 0);
      if (!_unkownFocusYn) {
        this.defaultInjection(changeEl);
        return;
      }
      if (currentRange.collapsed) {
        changeEl.appendChild(document.createTextNode('\u00a0'));
        _renderEl = changeEl;
      } else {
        this.loadSelection();
        _renderEl = commandFn(beforeContents);
      }
      if (_renderEl) {
        currentRange.deleteContents();
        currentRange.insertNode(_renderEl);
        currentRange.selectNode(_renderEl);
        _selction.removeAllRanges();
        _selction.selectAllChildren(_renderEl);
      }
    }
    return $target;
  }

  fixedMenuPositioningByTarget = ($target, $fixedMenu) => {
    $fixedMenu.css({'left': $target.position().left});
  }

  getHtmlData() {
    let htmlData = [$('.edit_template').text(), $('.edit_template').html()];
    return htmlData;
  }

  setHtmlData(htmlData) {
    $('.edit_template').html(htmlData);
  }

  changeTextarea(textData) {
    this.text1 = textData.target.value ? textData.target.value : '';
  }

  getTextareaData() {
    return this.text1;
  }
}

