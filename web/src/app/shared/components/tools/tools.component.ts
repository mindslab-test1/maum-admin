import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit, OnDestroy {

  @Input() name: string;
  @Input() actions: any;
  @Input() goBack: any;
  @Input() showMoreVert = true;
  @Input() isOffSidePanel = true;  // 기본 sidepanel off
  @Input() clickFunction: any; // 특정 goBack 함수

  get actionsArray() {
    let arr = [];
    for (let k in this.actions) {
      if (k) {
        this.actions[k].type = k;
        arr.push(this.actions[k]);
      }
    }
    return arr;
  }

  subscription = new Subscription();

  constructor(private store: Store<any>
    , private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.name) {
      return;
    }

    /*
        if (this.activatedRoute.routeConfig.data.nav === undefined) {
          this.subscription.add(
            this.store.select(s => s.router)
            .subscribe(router => {
              let currentNav = (function findLastActiveNav(navs, activeNav) {
                let childActiveNav = navs && navs.find(nav => nav.active);
                if (childActiveNav) {
                  return findLastActiveNav(childActiveNav.children, childActiveNav);
                } else {
                  return activeNav;
                }
              })(router.navs, null);
              this.name = currentNav && currentNav.name || null;
            })
          );
        } else {
          // routes의 체계가 바뀌면서 activatedRoute에서 값을 조회
          this.name = this.activatedRoute.routeConfig.data.nav.name;
        }
    */
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  doAction(type) {
    let act = this.actions[type];
    if (act) {
      act.callback();
    }
  }

  doGoBack() {
    if (this.clickFunction !== undefined) {
      // 입력값으로 전달받은 함수가 존재하면 실행
      this.clickFunction();
    } else {
      // 이전화면으로 이동합니다
      window.history.back();
    }
  }
}
