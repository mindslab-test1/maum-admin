import {Subscription} from 'rxjs';
import {Component, Input, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {FormControl, FormGroup, AbstractControl} from '@angular/forms';
import {MatMenu} from '@angular/material';


@Component({
  selector: 'app-form-control-input',
  templateUrl: './form-control-input.component.html',
  styleUrls: ['./form-control-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FormControlInputComponent implements OnInit, OnDestroy {

  @Input() group: FormGroup;
  @Input() errorParams: any = {}; // pass to form-control-errors
  @Input() control: AbstractControl;

  @Input() name = 'noname';
  @Input() label: string = null;
  @Input() placeholder: string = null;
  @Input() value: any;
  @Input() type = 'text';

  // select or radio, checkbox
  @ViewChild('selectMenu') selectMenu: MatMenu;
  private _options: { key: any, value: any, desc?: any, hidden: boolean }[] = null; // for radio and select
  @Input()
  set options(opts) {
    this._options = opts;
    if (this.control) {
      this.control.setValue(this.control.value);
    }
  }

  get options() {
    return this._options;
  }

  selectedOption: { key: any, value: any, desc?: any } = null;

  // callbacks
  @Input() connect: any = null; // pass to callback
  @Input() onEnter: (control: AbstractControl, connect?: any) => void = null;
  @Input() onFocus: (control: AbstractControl, connect?: any) => void = null;
  @Input() onBlur: (control: AbstractControl, connect?: any) => void = null;

  subscription = new Subscription();

  constructor() {
  }

  handleKeyDown(e) {
    if (this.onEnter && e.keyCode === 13) {
      this.onEnter(this.control, this.connect);
    }
  }

  handleFocus(e) {
    if (this.onFocus) {
      this.onFocus(this.control, this.connect);
    }
  }

  handleBlur(e) {
    if (this.onBlur) {
      this.onBlur(this.control, this.connect);
    }
  }

  ngOnInit() {
    this.control = this.control || this.group && this.group.get(this.name) || new FormControl();
    if (this.options !== null) {
      this.subscription.add(
        this.control.valueChanges.subscribe(value => {
          this.selectedOption = this.options.find(opt => opt.value === value);
        })
      );
      this.control.setValue(this.control.value);
    }

    if (this.value) {
      this.control.setValue(this.value);
    }

    if (this.label === null) {
      this.label = this.name;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
