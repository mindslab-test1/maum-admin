import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {TableComponent} from '../table/table.component';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {StorageBrowser} from '../../storage/storage.browser';
import {FileUploader} from 'ng2-file-upload/ng2-file-upload'
import {environment} from '../../../../environments/environment';


@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
})

export class UploaderComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('url') url: string;
  // tslint:disable-next-line:no-input-rename
  @Input('accept') accept: '.*';
  // tslint:disable-next-line:no-input-rename
  @Input('groupId') groupId: string;
  // tslint:disable-next-line:no-output-rename
  @Output('uploadCallback') uploadCallback = new EventEmitter<boolean>();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('statusTemplate') statusTemplate: TemplateRef<any>;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;

  files: any[] = [];
  dataSource: MatTableDataSource<any>;
  headers = [];
  rows: any[] = [];
  pageParam: MatPaginator;
  uploader: FileUploader;
  paigingRows: any[] = [];

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser) {
  }

  ngOnInit() {
    this.uploader = new FileUploader({
      url: environment.maumAiApiUrl + this.url,
      autoUpload: true
    });

    this.uploader.onBuildItemForm = (fileItem, form) => {
      form.append('workspaceId', this.storage.get('m2uWorkspaceId'));
      if (this.groupId !== undefined && this.groupId !== '') {
        form.append('groupId', this.groupId);
      }
      return {fileItem, form};
    };

    this.headers = [{attr: 'no', name: 'No.', no: true},
      {attr: 'file.name', name: 'Name'},
      {attr: 'file.type', name: 'Type', width: '7%'},
      {attr: 'file.size', name: 'Size', width: '7%', format: 'file'},
      {attr: 'status', name: 'Status', template: this.statusTemplate},
      {attr: 'action', name: 'action', width: '7%', template: this.actionTemplate}];

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onErrorItem = (item: any, res: any) => {
      let response = JSON.parse(res) || {};
      if (response.error) {
        item.error = response.error;
      }
    };

    this.uploader.onSuccessItem = (item: any, res: any) => {
      let response = JSON.parse(res) || {};
      item.msg = response.msg || 'Complete';
    };

    this.uploader.onCompleteAll = () => {
      this.setRows();
      this.uploadCallback.emit(true);
    };
  }

  setRows() {
    this.rows = this.uploader.queue;
    console.log(5, this.rows);
    this.setPage(this.pageParam);
  }

  setPage(page?: MatPaginator) {
    if (page) {
      this.pageParam = page;
    }

    if (this.rows.length > 0) {
      let start: number = this.pageParam.pageIndex * this.pageParam.pageSize;
      let end: number = (this.pageParam.pageIndex + 1) * this.pageParam.pageSize - 1;

      if (end > this.rows.length) {
        end = this.rows.length - 1;
      } else {
        end = (this.pageParam.pageIndex + 1) * this.pageParam.pageSize - 1;
      }
    }
  }
}
