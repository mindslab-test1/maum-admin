import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {StorageBrowser} from '../../storage/storage.browser';
import {AlertComponent} from '../dialog/alert.component';
import {MatDialog} from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from 'environments/environment';
import {FileUploader} from 'ng2-file-upload';
import {ConfirmComponent} from '../..';
import {FormControl} from '@angular/forms';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-uploader-button',
  templateUrl: './uploader-button.component.html'
})

export class UploaderButtonComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('title') title: string;
  // tslint:disable-next-line:no-input-rename
  @Input('url') url: string;
  // tslint:disable-next-line:no-input-rename
  @Input('accept') accept: '.*';
  // tslint:disable-next-line:no-input-rename
  @Input('data') data: any;
  // tslint:disable-next-line:no-input-rename
  @Input('type') type: string;
  // tslint:disable-next-line:no-output-rename
  @Output('uploadCallback') uploadCallback = new EventEmitter<boolean>();
  // tslint:disable-next-line:no-output-rename
  @Output('preUploadCallback') preUploadCallback = new EventEmitter<boolean>();

  // TODO : preUploadCallback
  preUploadCallbackResult;
  uploader: FileUploader;
  nameFormControl = new FormControl('', []);

  constructor(private http: HttpClient, protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
  }

  addFiles(event) {
    const file = event.target.files || event.srcElement.files;
    console.log('$##addfiles', file);
    if (file || file.length > 0) {
      let formData = new FormData();
      let workspaceId = this.storage.get('m2uWorkspaceId');
      let userId = this.storage.get('user').id;
      let url = environment.maumAiApiUrl + this.url;
      let request = new XMLHttpRequest();
      if (file[0].name != null) {
        this.preUploadCallback.emit();
        formData.append('file', file[0]);
        formData.append('workspaceId', workspaceId);
        formData.append('data', this.data);
        formData.append('userId', userId);
        formData.append('type', this.type);
        this.preUploadCallbackResult.then(res => {
          if (res) {
            request.onreadystatechange = () => {
              if (request.readyState === 4) {
                let response = (() => {
                  try {
                    let resp = JSON.parse(request.responseText);
                    if (resp.message) {
                      resp.message = resp.message.replace(/\r?\n/g, '<br>')
                      .replace(/  +/g, s => ' ' + '&nbsp;'.repeat(s.length - 1));
                    }
                    return resp;
                  } catch (e) {
                    return {status: undefined, message: request.responseText};
                  }
                })();
                if (request.status === 200) {
                  this.openAlertDialog('Success',
                      response.message ? response.message : 'Success Upload File',
                      response.status ? response.status : 'success');
                  this.uploadCallback.emit(true);
                } else {
                  this.openAlertDialog('Failed',
                      response.message ? response.message : 'Failed Upload File',
                      response.status ? response.status : 'error');
                }
              }
            };
            request.open('POST', url);
            request.send(formData);
          }
        }).catch(err => {
          console.error('error' + err);
          this.openAlertDialog('Failed',
              err.error ? err.error : 'Failed Upload File',
              'error');
        });
      }
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
