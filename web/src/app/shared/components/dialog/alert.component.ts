import {Component, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input() title: string = null;
  @Input() message: string = null;
  @Input() header: string = null;

  constructor(public dialogRef: MatDialogRef<any>) {
  }

  ngOnInit() {
  }

  getIcon() {
    if (this.header === 'error') {
      return 'block';
    } else if (this.header === 'success') {
      return 'done';
    } else if (this.header === 'notice') {
      return 'error_outline';
    }
  }
}
