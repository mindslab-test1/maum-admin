import {Component, OnInit, Input} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {

  @Input() title: string = null;
  @Input() message: string = null;

  constructor(public dialogRef: MatDialogRef<any>) {
  }

  ngOnInit() {
  }

}
