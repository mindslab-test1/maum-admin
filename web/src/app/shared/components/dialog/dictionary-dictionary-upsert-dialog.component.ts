import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertComponent} from './alert.component';

@Component({
  selector: 'app-dictionary-dictionary-upsert-dialog',
  templateUrl: './dictionary-dictionary-upsert-dialog.component.html',
  styleUrls: ['./dictionary-dictionary-upsert-dialog.component.scss']
})
export class DictionaryDictionaryUpsertDialogComponent implements OnInit {

  @Input() title: string = null;

  model: FormGroup;
  data: any;
  workspaceId: any;
  meta: any;
  next: any;
  service: any;
  // insert 인지 update 상황인지 판단하는 boolean
  isUpdate: boolean;

  constructor(public dialogRef: MatDialogRef<any>,
              private fb: FormBuilder,
              public dialog: MatDialog) {
    this.model = this.fb.group({
      name: ['', [
        Validators.required
      ]],
      description: ['']
    });
  }

  ngOnInit() {
    this.next = this.dialogRef.componentInstance.next;
    let data = this.data = this.dialogRef.componentInstance.data;
    // apply data when update
    if (data) {
      this.isUpdate = true;
      this.model.setValue({
        name: data.name,
        description: data.description
      });
    }
  }

  submit() {
    let value = Object.assign({}, this.meta, this.model.value);

    // DB 컬럼 사이즈에 따른 value size check
    if (value.name !== undefined) {
      if (value.name.length > 50) {
        this.openAlertDialog('Limitation', 'Dictionary name is limited to 50 bytes.', 'error');
        return;
      }
    } else {
      // name 없을 시 처리하지 않음
      return;
    }
    if (value.description && value.description.length > 40) {
      this.openAlertDialog('Limitation', 'Dictionary description is limited to 40 bytes.', 'error');
      return;
    }

    if (this.isUpdate) {
      this.service.updateDictionary(value).subscribe(
        node => {
          if (node) {
            if (node.message === 'UPDATE_DUPLICATED') {
              this.openAlertDialog('Duplicate', 'Dictionary name duplicated.', 'error');
            } else if (node.message === 'UPDATE_SUCCESS') {
              this.openAlertDialog('Edit', `Dictionary '${node.dictionary.name}' updated.`, 'success');
              this.dialogRef.close(true);
            }
          } else {
            this.openAlertDialog('Edit', `Dictionary '${value.name}' updated.`, 'success');
            this.dialogRef.close(true);
          }
        },
        err => {
          this.openAlertDialog('Error', `Server Error. Dictionary '${value.name}' not updated.`, 'error');
        }
      );
    } else {
      this.service.insertDic(value).subscribe(
        node => {
          if (node.message === 'INSERT_DUPLICATED') {
            this.openAlertDialog('Duplicate', 'Dictionary name duplicated.', 'error');
          } else if (node.message === 'INSERT_SUCCESS') {
            this.openAlertDialog('Add', `Dictionary '${node.dictionary.name}' created.`, 'success');
            this.dialogRef.close(true);
          }
        },
        err => {
          this.openAlertDialog('Error', `Server Error. Dictionary '${value.name}' not created.`, 'error');
        }
      );
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
