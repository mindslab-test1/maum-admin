import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {StorageBrowser} from '../../storage/storage.browser';
import {ConfirmComponent} from './confirm.component';
import {AlertComponent} from './alert.component';

@Component({
  selector: 'app-file-group-dialog',
  templateUrl: 'file-group-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class FileGroupDialogComponent implements OnInit {

  param: any;
  service: any;

  name: string;
  nameValidator = new FormControl('', [Validators.required, this.noWhitespaceValidator]);
  disabled = true;
  data: any;
  rate: string;

  constructor(public dialogRef: MatDialogRef<FileGroupDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.name = this.data['row']['name'];
    this.param = this.data['entity'];
    this.service = this.data['service'];
  }

  insertAmGroup = () => {
    this.param = this.param.constructor();
    this.param.workspaceId = this.storage.get('m2uWorkspaceId');
    this.param.name = this.name.trim();
    let meta = {
      'rate': this.rate
    };
    console.log('param: ', this.param);
    this.param.meta = JSON.stringify(meta);

    this.service.insertAmGroup(this.param).subscribe(res => {
        this.dialogRef.close(true);
        this.openAlertDialog('Success', 'Success Add FileGroup', 'success');

      },
      err => {
        this.openAlertDialog('Failed', 'Failed Add FileGroup', 'error');
        console.log('error', err);
      }
    );
  };

  updateGroup = () => {
    if (!this.data['row']['id']) {
      this.openAlertDialog('Failed', 'Failed Edit FileGroup', 'error');
      this.dialogRef.close();
      return false;
    }

    this.param = this.param.constructor();
    this.param.id = this.data['row']['id'];
    this.param.name = this.name.trim();

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to edit Group?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.service.updateGroup(this.param).subscribe(res => {
            this.dialogRef.close(true);
            this.openAlertDialog('Success', 'Success Update FileGroup', 'success');
          },
          err => {
            this.openAlertDialog('Failed', 'Failed Edit FileGroup', 'error');
            console.log('error', err);
          }
        );
      }
    });
  };

  deleteGroup = () => {
    if (!this.data['row']['id']) {
      this.openAlertDialog('Failed', 'Failed Remove FileGroup', 'error');
      this.dialogRef.close();
      return false;
    }

    this.param = this.param.constructor();
    this.param.id = this.data['row']['id'];

    let ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Do you want to remove Group?';
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.service.deleteGroup(this.param).subscribe(res => {
            this.dialogRef.close(true);
            this.openAlertDialog('Success', 'Success Delete FileGroup', 'success');
          },
          err => {
            this.openAlertDialog('Failed', 'Failed Remove FileGroup', 'error');
            console.log('error', err);
          }
        );
      }
    })
  };

  checkInvalid($event) {
    if (this.isEmptyObject(this.data.row)) {
      // add
      if ('INVALID' === this.nameValidator.status) {
        this.disabled = true;
        return false;
      } else if ('VALID' === this.nameValidator.status && (this.rate !== undefined && this.rate !== '')) {
        this.disabled = false;
        return true;
      }
    } else {
      if ('INVALID' === this.nameValidator.status) {
        this.disabled = true;
        return false;
      } else if ('VALID' === this.nameValidator.status) {
        this.disabled = false;
        return true;
      }
    }

  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  noWhitespaceValidator(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : {'whitespace': true}
  }
}
