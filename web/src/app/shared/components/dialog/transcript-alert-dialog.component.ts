import {Component, Inject, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {StorageBrowser} from '../../storage/storage.browser';
import {AlertComponent} from './alert.component';

@Component({
  selector: 'app-transcript-alert-dialog',
  templateUrl: './transcript-alert-dialog.component.html',
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
})
export class TranscriptAlertDialogComponent implements OnInit {

  param: any;
  service: any;

  alert: string;
  alertValidator = new FormControl('', [Validators.required]);
  disabled = true;
  data: any;

  constructor(public dialogRef: MatDialogRef<TranscriptAlertDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public matData: any,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.data = this.matData;
    this.alert = this.data['row']['alert'];
    this.param = this.data['entity'];
    this.service = this.data['service'];
  }

  updateAlert = () => {
    if (!this.data['row']['id']) {
      return false;
    }

    this.param = this.param.constructor();
    this.param.id = this.data['row']['id'];
    this.param.alert = this.alert;


    this.service.updateAlert(this.param).subscribe(res => {
        if (res) {
          this.openAlertDialog('Success', 'Success Save Alert', 'success');
          this.dialogRef.close(res);
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Save Alert Message', 'error');
        console.log('error', err);
      }
    );
  };

  checkInvalid($event) {
    if ('INVALID' === this.alertValidator.status) {
      this.disabled = true;
      return false;
    } else if ('VALID' === this.alertValidator.status) {
      this.disabled = false;
      return true;
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
