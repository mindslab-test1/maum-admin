import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-form-control-section',
  templateUrl: './form-control-section.component.html',
  styleUrls: ['./form-control-section.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FormControlSectionComponent {

  @Input() label = 'No Label';
  @Input() tooltip: string = null;
  @Input() desc: string = null;

  constructor() {
  }

}
