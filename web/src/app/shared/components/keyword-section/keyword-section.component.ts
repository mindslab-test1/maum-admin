import {Component, Input} from '@angular/core';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-keyword-section',
  templateUrl: 'keyword-section.component.html',
  styleUrls: ['keyword-section.component.scss']
})
export class KeywordSectionComponent {

  @Input() keywords: Array<any> = [];

  constructor(private store: Store<any>) {
  }

}
