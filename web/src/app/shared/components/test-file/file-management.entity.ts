import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class FileManagementEntity extends PageParameters {

  id: string;
  ids: string[] = [];
  name: string;
  workspaceId: string;

  purpose: string;
  creatorId: string;

  fileGroupId: string;
}
