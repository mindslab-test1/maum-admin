import {
  Component,
  EventEmitter,
  Inject,
  Output,
  Input,
  ViewEncapsulation,
  SimpleChanges, ViewChild, ChangeDetectorRef
} from '@angular/core';
import {GridOptions} from 'ag-grid-community';
import {ROUTER_LOADED} from '../../../core/actions';
import {Store} from '@ngrx/store';
import {StorageBrowser} from '../../storage/storage.browser';
import {MatDialog} from '@angular/material';
import {AlertComponent, ConfirmComponent, UploaderButtonComponent} from '../../index';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {FileManagementEntity} from './file-management.entity';
import {environment} from 'environments/environment';

interface ModelInfo {
  id: string;
  name: string;
  type: string;
  part: string;
  workspaceId: string;
}

enum ModelType {
  dnn = 0,
  hmd = 1,
  morph = 2,
  ner = 3,
  xdc = 5
}

enum ModelPart {
  ta = 0,
  dictionary = 1,
}

interface FileInfo {
  id: string;
  name: string;
  size: number;
  createdAt: Date;
  creatorId: string;
}

type file_list = FileInfo[];

@Component({
  selector: 'app-files',
  templateUrl: './file.manager.html',
  styleUrls: ['./file.manager.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FilesComponent {
  @Input() type: string;
  @Input() name: string;
  @Input() workspaceId: string;
  @Input() id: string;
  @Output() selectFiles: EventEmitter<string[]> = new EventEmitter<string[]>();
  @Output() changeFileList: EventEmitter<string[]> = new EventEmitter<string[]>();
  public gridOptions: GridOptions;
  public defaultColDef;
  @ViewChild('uploader') uploader: UploaderButtonComponent;
  API_URL;

  model: ModelInfo = {
    id: '',
    type: '',
    part: '',
    name: '',
    workspaceId: ''
  };
  row_data: file_list = [];
  actions: any[];
  fileList: string[] = [];
  creatorId = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private store: Store<any>,
    public cdr: ChangeDetectorRef,
    @Inject(StorageBrowser) protected storage: StorageBrowser,
    private dialog: MatDialog) {
    this.API_URL = environment.maumAiApiUrl;
    this.creatorId = this.storage.get('user').id;
  }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      columnDefs: this.createColumnDefs(),
      rowData: this.row_data,
      enableColResize: true,
      enableFilter: true,
      enableSorting: true,
      animateRows: true,
      rowSelection: 'multiple',
      defaultColDef: this.defaultColDef,
      onRowSelected: this.onRowSelected,
      onGridReady(params) {
        params.api.sizeColumnsToFit();
      },
    };
    this.load();
    this.workspaceId = this.storage.get('m2uWorkspaceId');
    this.creatorId = this.storage.get('user').id;
    this.store.dispatch({type: ROUTER_LOADED});
    this.cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    try {
      if (changes.hasOwnProperty('workspaceId') &&
        changes.workspaceId.currentValue !== '' &&
        changes.workspaceId.currentValue !== undefined) {
        this.model.workspaceId = changes.workspaceId.currentValue.toString();
      }

      if (changes.hasOwnProperty('name') &&
        changes.name.currentValue !== '' &&
        changes.name.currentValue !== undefined) {
        this.model.name = changes.workspaceId.currentValue;
      }

      if (changes.hasOwnProperty('id') &&
        changes.name.currentValue !== '' &&
        changes.name.currentValue !== undefined) {
        this.model.id = changes.workspaceId.currentValue.toString();
      }

      if (changes.hasOwnProperty('type') && changes.type.currentValue !== '' && changes.type.currentValue !== undefined) {
        this.model.type = changes.type.currentValue;
        if (this.type in ModelType === false) {
          this.back('Error. Check a type of dictionary  in a url');
        }
        if (this.model.type === ModelType[0] || this.type === ModelType[1] || this.type === ModelType[5]) { // hmd, dnn, xdc
          this.model.part = ModelPart[0];
        } else if (this.type === ModelType[2] || this.type === ModelType[3]) { // 사전
          this.model.part = ModelPart[1];
        }
      }

      if (this.model.type !== '' && this.model.workspaceId !== '' && this.model.id !== '') {
        this.getFiles();
      }
    } catch (e) {
      this.openAlertDialog('Load Result', 'Something wrong!', 'error');
    }
  }

  load = () => {
    this.route.url.subscribe(par => {
      if (par[0].path in ModelType) {
        this.model.type = par[0].path.toLowerCase();
      } else {

      }
    }, err => {
      this.openAlertDialog('Url', 'Something wrong.', 'error');
      window.history.back();
    });

    this.actions = [{
      type: 'download',
      text: 'Download',
      icon: 'file_download',
      callback: this.download,
      disabled: true,
      hidden: false
    }, {
      type: 'delete',
      text: 'Delete',
      icon: 'delete_sweep',
      disabled: true,
      callback: this.delete,
      hidden: false
    }];
  };

  getFiles() {
    let fileEntity: FileManagementEntity = new FileManagementEntity();
    fileEntity.workspaceId = this.model.workspaceId;
    fileEntity.creatorId = this.creatorId;
    fileEntity.purpose = this.model.type;
    this.http.post(this.API_URL + '/ta/file-manage/getFileListWithType', fileEntity)
    .subscribe((response: any[]) => {
      this.row_data = [];
      if (response.length > 0) {
        response.forEach(elem => {
          let file_info: FileInfo = {
            id: undefined,
            name: undefined,
            size: undefined,
            createdAt: undefined,
            creatorId: undefined,
          };
          file_info.id = elem.id;
          file_info.name = elem.name;
          file_info.creatorId = elem.creatorId;
          file_info.createdAt = elem.createdAt;
          file_info.size = elem.size;
          this.row_data.push(file_info);
        });
      }
      this.gridOptions.api.setRowData(this.row_data);
      this.load();
    });
  }

  delete = () => {
    let selectedData = this.gridOptions.api.getSelectedRows();
    if (selectedData.length > 0) {
      let fileEntity: FileManagementEntity = new FileManagementEntity;
      fileEntity.ids = selectedData.map(i => i.id);
      fileEntity.workspaceId = this.model.workspaceId;
      this.http.post(this.API_URL + '/ta/file-manage/deleteFileList', fileEntity)
      .subscribe((response: any) => {
        this.row_data = [];
        if (response.hasOwnProperty('isSuccess') && response.isSuccess) {
          this.selectFiles.emit([]);
          this.actions.forEach(action => action.disabled = true);
          this.openAlertDialog('Delete', `Delete selected files successfully.`, 'success');
          this.getFiles();
        } else {
          this.getFiles();
          this.openAlertDialog('Delete', `Can't delete selected files.`, 'success');
        }
      }, err => {
        this.openAlertDialog('Delete', 'Something wrong.', 'error');
      });
    }
  };

  download = () => {
    const res = this.downloadWork();
  };

  delay(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  async downloadWork() {
    let selectedData = this.gridOptions.api.getSelectedRows();
    for (const item of selectedData) {
      await this.flow(item.id);
    }
    this.openAlertDialog('Download', `Download files successfully.`, 'success');
  }

  async flow(id: string) {
    return await this.process(id);
  }

  async process(id: string) {
    let url = environment.maumAiApiUrl + '/ta/file-manage/download-file/' + this.workspaceId + '/' + id;
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.click();
    await this.delay(500);
  }

  createColumnDefs() {
    return [{
      headerName: '#',
      field: 'no',
      width: 5,
      checkboxSelection: true,
      headerCheckboxSelection: true,
      suppressSorting: true,
      suppressMenu: true,
      pinned: true,
    }, {
      headerName: 'Id',
      field: 'id',
      width: 2,
      filter: 'agTextColumnFilter',
      editable: false,
      hide: true,
    }, {
      headerName: 'File name',
      field: 'name',
      width: 30,
      filter: 'agTextColumnFilter',
      editable: false,
    }, {
      headerName: 'size',
      field: 'size',
      width: 25,
      filter: 'agTextColumnFilter',
      editable: false
    }, {
      headerName: 'Created date',
      field: 'createdAt',
      width: 35,
      cellRenderer: this.dateCellRenderer,
      editable: false
    }, {
      headerName: 'Creator',
      field: 'creatorId',
      width: 25,
      editable: false,
      hide: true
    },
    ]
  }

  onRowSelected = (param) => {
    if (param.node && param.node.rowModel.selectionController.getSelectedNodes().length > 0) {
      let selectedData = this.gridOptions.api.getSelectedRows().map(x => x.id);
      this.selectFiles.emit(selectedData);
      this.actions.forEach(action => action.disabled = false);
    } else {
      this.selectFiles.emit([]);
      this.actions.forEach(action => action.disabled = true);
    }
  };

  onUploadFile() {
    this.actions.forEach(action => action.disabled = true);
    this.selectFiles.emit([]);
    this.getFiles();
  }

  dateCellRenderer = (params) => {
    let date: Date = new Date(params.value);
    let result = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
    return result;
  };

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  doAction(action) {
    if (action) {
      action.callback();
    }
  }

  back = (msg: string) => {
    if (msg !== '') {
      this.openAlertDialog('Failed', msg, 'error');
    }
    window.history.back();
  }

  confirmUpload() {
    this.uploader.preUploadCallbackResult = new Promise(resolve => {
      let ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = 'If you upload a file,<br> All data will be overwrited by data in file.<br>Continue?  ';
      ref.afterClosed().subscribe(result => {
        if (result) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

}

