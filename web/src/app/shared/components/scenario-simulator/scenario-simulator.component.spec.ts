import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioSimulatorComponent } from './scenario-simulator.component';

describe('ScenarioSimulatorComponent', () => {
  let component: ScenarioSimulatorComponent;
  let fixture: ComponentFixture<ScenarioSimulatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScenarioSimulatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioSimulatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
