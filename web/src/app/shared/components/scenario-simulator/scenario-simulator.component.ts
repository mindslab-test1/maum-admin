import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output, SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import * as $ from 'jquery';
import 'jquery-ui';
import 'jquery.tmpl';
import {UUID} from 'angular2-uuid';
import {ROUTER_LOADED} from '../../../core/actions';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-scenario-simulator',
  templateUrl: './scenario-simulator.component.html',
  styleUrls: ['./scenario-simulator.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ScenarioSimulatorComponent implements OnInit, AfterViewInit, AfterViewChecked {

  @ViewChild('divScroll') private divScroll: ElementRef;

  @Input() scenario;
  @Input() isUpdated = false;
  @Output() nodeButtonClickCallback = new EventEmitter<Object>();

  flowNodes = [];
  guid;

  // 템플릿 타입
  AI_TEMP = {
    COMMENT: '0101',
    BUTTON: '0301',
    FINAL: '0801',
    FINAL_DATA: '0802',
    FINAL_TASK: '0803',
    DATA: '0901',
    TASK: '1001',
    BQA: '1201',
    TRANSITION_BQA: '1401',
    TRANSITION_GO_TO_SKILL: '1402',
    TRANSITION_GO_TO_STACK: '1403'
  };

  constructor(private store: Store<any>) {
  }

  ngOnInit() {
    console.log('scenario :: ', this.scenario);
    this.store.dispatch({type: ROUTER_LOADED});
    this.guid = this.generateUUID();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('chaged: ', changes);
    this.scenario = changes['scenario'].currentValue;
    this.initSimulator();
  }

  ngAfterViewInit(): void {
    // this.initSimulator();
    // let startNode = this.cloneObject(this.scenario.nodes[0]);
    // startNode['time'] = this.getTime();
    // this.makeButtonObject(startNode);
    // this.flowNodes.push(startNode);
    // console.log('this.flowNodes :: ', this.flowNodes);
  }

  ngAfterViewChecked(): void {

  }

  $UserInfo = () => {
    return {
      user: {
        hanaCd: '01'
        , userId: 'Test'
        , userNm: '테스터'
      }
    }
  };

  initSimulator() {
    this.flowNodes = [];
    let startNode = this.cloneObject(this.scenario.nodes[0]);
    startNode['time'] = this.getTime();
    this.makeButtonObject(startNode);
    this.flowNodes.push(startNode);
    console.log('this.flowNodes :: ', this.flowNodes);
  }

  /**
   * 스크립트태그 제거
   * @param {String} text 텍스트
   * @returns {String} text
   */
  removeScriptTag(text) {
    if (!text) {
      return '';
    } else {
      return text.replace(/<(\/script|script)([^>]*)>/gi, '');
    }
  }

  /**
   * 현재 시:분:초 반환
   * @returns {String} now Date (hour:minute:second)
   */
  getTime() {
    let date = new Date();
    let hour = date.getHours().toString();
    let minute = date.getMinutes().toString();
    let second = date.getSeconds().toString();

    if (hour.length === 1) {
      hour = '0' + hour;
    }
    if (minute.length === 1) {
      minute = '0' + minute;
    }
    if (second.length === 1) {
      second = '0' + second;
    }
    return hour + ':' + minute + ':' + second;
  }

  /**
   * UUID 생성 - 그룹사코드 + 서비스그룹 + 기타3자리 + uuid 22자리 + 00
   * @returns {String} 생성된 UUID
   */
  generateUUID() {
    return this.$UserInfo().user.hanaCd + 'ECT' + '000' + UUID.UUID() + '00';
  }

  initScenarioData() {
    this.scenario = {
      nodes: [
        {
          id: '272f54b5-0d32-47ce-8436-8fd66534a093',
          text: '계좌상태에서',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '개인이세요. 법인이세요?'},
            {dtlsMsgTypeCd: '0901', tmplCtt: '[화면] 000 -> [항목] 고객자격'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '개인', selectedUtter: 'default', selected: false},
                  {title: '개인사업자', selectedUtter: 'default', selected: false},
                  {title: '법인', selectedUtter: 'default', selected: false},
                  {title: '임의단체', selectedUtter: 'default', selected: false}]}}
          ]
        },
        {
          id: '142606b5-3841-4457-82e1-97fa749cc54a',
          text: '프로세스 설정',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '거래중지계좌 업무 절차'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [{title: '계속 진행', selectedUtter: '계속 진행', selected: false}]}}
          ]
        },
        {
          id: '08cd8a3d-a1e9-4f4c-b08b-791c157d4cdf',
          text: '안내멘트 및 선택가능',
          attr: [
            {dtlsMsgTypeCd: '0901', tmplCtt: '[전산조작] [화면] 0009 -> [거래종류] 0010 -> [예외코드] 1619'},
            {dtlsMsgTypeCd: '0101', tmplCtt: '손님, 보이스피싱 등 금융사기 발생 예방을 위해 장기 미사용계좌의 경우 거래중지계좌로 편입되어 이용에 제한이 있으실수 있는데요. 거래중지계좌로 등록된 계좌의 경우 계좌해지를 하시거나 복원을 하실수 있는데 어느쪽으로 안내해드릴까요?'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '계좌해지하기', selectedUtter: '계좌해지하기', selected: false},
                  {title: '계좌복원하기', selectedUtter: '계좌복원하기', selected: false}]}}
          ]
        },
        {
          id: '2ec4999d-1db0-45ea-8cdb-98db558ac795',
          text: '계좌잔액 확인',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '중지된 계좌의 잔액은 0원 초과이신가요?'},
            {dtlsMsgTypeCd: '0901', tmplCtt: '[전산조작] [화면] 0009 -> [거래종류] 0001 -> [항목] 현재잔액'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '0원 초과입니다.', selectedUtter: '0원 초과', selected: false},
                  {title: '0원 입니다', selectedUtter: '0원 입니다', selected: false}]}}
          ]
        },
        {
          id: '32860c90-2116-4892-bf10-af191e76cca2',
          text: '입출금 계좌보유',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '현재 사용 가능한 하나은행 입출금 계좌가 있으신가요??'},
            {dtlsMsgTypeCd: '0901', tmplCtt: '[전산조작] [화면] 0009 -> [버튼] 돋보기 -> [여수신구분] 9 -> [활동구분] 1'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '입출금 계좌 보유', selectedUtter: '입출금 계좌 보유', selected: false},
                  {title: '입출금 계좌 미보유', selectedUtter: '입출금 계좌 미보유', selected: false}]}}
          ],
        },
        {
          id: 'b5a9e25d-3811-417f-9088-ed3d33e5ff4b',
          type: 'task',
          text: '이용채널탐색1',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '손님, 계좌해지 거래는 이용중이신 계좌번호를 알고계신 경우 저희 상담원을 통해 가능하시거나, 인터넷뱅킹 또는 스마트폰뱅킹에서 이체거래가 가능하신 경우 해당 채널을 통해 이용 가능합니다. 어떤 방법을 이용하시겠습니까?'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '콜센터', selectedUtter: '콜센터', selected: false},
                  {title: '인터넷뱅킹', selectedUtter: '인터넷뱅킹', selected: false},
                  {title: '스마트폰뱅킹', selectedUtter: '스마트폰뱅킹', selected: false},
                  {title: '영업점', selectedUtter: '영업점', selected: false}]}}
          ]
        },
        {
          id: '624193f9-36b2-4df8-98cd-8c48fd77ba41',
          text: '이용채널탐색2',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '활동계좌가 없는 경우 계좌통합관리서비스 또는 영업점을 통해 계좌해지 신청이 가능합니다. 어떤 방법으로 안내해드릴까요?'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '영업점', selectedUtter: '영업점', selected: false},
                  {title: '계좌정보통합관리서비스', selectedUtter: '계좌정보통합관리서비스', selected: false}]}}
          ]
        },
        {
          id: 'ea8bcbbb-2795-408a-9a72-b23c8bfea89f',
          text: '본인확인 가능여부',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '본인확인 진행 해 주세요.'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '본인확인 완료', selectedUtter: '본인확인 완료', selected: false},
                  {title: '본인확인 불가', selectedUtter: '본인확인 불가', selected: false}]}}
          ]
        },
        {
          id: '8618b63e-801f-4820-878c-84d3cba4d3a9',
          text: '콜센터를 통한 해지 가능여부 탐색',
          attr: [
            {dtlsMsgTypeCd: '0101', tmplCtt: '콜센터를 통해 해지 가능 여부를 확인해주세요 ★ 일부사유(뱅킹머니연결계좌 등)의 경우 해지 불가하므로 확인 후 진행해주세요'},
            {dtlsMsgTypeCd: '0301', tmplCtt: {items: [
                  {title: '해지 가능합니다.', selectedUtter: '해지 가능합니다.', selected: false},
                  {title: '해지 불가능합니다.', selectedUtter: '해지 불가능합니다.', selected: false}]}}
          ]
        }
      ],
      edges: [
        {
          source: '272f54b5-0d32-47ce-8436-8fd66534a093',
          target: '142606b5-3841-4457-82e1-97fa749cc54a',
          data: {
            id: 'ee212792-b21e-493c-8a11-5e4a162fdb19',
            type: 'connection',
            label: 'default',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: '142606b5-3841-4457-82e1-97fa749cc54a',
          target: '08cd8a3d-a1e9-4f4c-b08b-791c157d4cdf',
          data: {
            id: 'bb226f97-5caa-4866-b9f5-8688477951e4',
            type: 'connection',
            label: '계속 진행',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: '08cd8a3d-a1e9-4f4c-b08b-791c157d4cdf',
          target: '2ec4999d-1db0-45ea-8cdb-98db558ac795',
          data: {
            id: '27d9824b-8e97-4d56-94b9-5f52c6092f56',
            type: 'connection',
            label: '계좌해지하기',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: '2ec4999d-1db0-45ea-8cdb-98db558ac795',
          target: '32860c90-2116-4892-bf10-af191e76cca2',
          data: {
            id: '51581ac8-3e3c-48d7-ba59-8aa3ad369c69',
            type: 'connection',
            label: '0원 초과',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: '32860c90-2116-4892-bf10-af191e76cca2',
          target: 'b5a9e25d-3811-417f-9088-ed3d33e5ff4b',
          data: {
            id: 'b0bc903f-7082-4038-9f6d-679681c807f5',
            type: 'connection',
            label: '입출금 계좌 보유',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: '32860c90-2116-4892-bf10-af191e76cca2',
          target: '624193f9-36b2-4df8-98cd-8c48fd77ba41',
          data: {
            id: 'd25291c0-addc-4c62-9522-3a5502340728',
            type: 'connection',
            label: '입출금 계좌 미보유',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: 'b5a9e25d-3811-417f-9088-ed3d33e5ff4b',
          target: 'ea8bcbbb-2795-408a-9a72-b23c8bfea89f',
          data: {
            id: 'e8d27f1d-a161-40b5-914d-d5b25d14576a',
            type: 'connection',
            label: '콜센터',
            attr: [
              {
                text: ''
              }
            ],
          }
        },
        {
          source: 'ea8bcbbb-2795-408a-9a72-b23c8bfea89f',
          target: '8618b63e-801f-4820-878c-84d3cba4d3a9',
          data: {
            id: 'a7f9a4b9-9901-4cba-8963-aa7f9f3a5fb9',
            type: 'connection',
            label: '본인확인 완료',
            attr: [
              {
                text: ''
              }
            ],
          }
        }
      ]
    };

    this.scenario.nodes[0]['time'] = this.getTime();
    this.flowNodes.push(Object.assign({}, this.scenario.nodes[0]));
  }

  clickButton(sourceNodeId: string, item: any, msgIndex: number, items: any) {
    console.log(sourceNodeId, item);
    const edge = this.findEdge(sourceNodeId, item.title);

    if (!edge) {
      alert('edge가 없습니다.');
      return;
    }
    let targetNode = this.findTargetNode(edge.target);
    this.makeButtonObject(targetNode);
    targetNode['time'] = this.getTime();

    if (targetNode) {
      items.forEach(eachItem => {
        if (item.title === eachItem.title) {
          eachItem.selected = true;
        } else {
          eachItem.selected = false;
        }
      });

      // 상단의 노드 버튼을 누를시 하단 노드 삭제
      if (msgIndex < this.flowNodes.length) {
        this.flowNodes = this.flowNodes.slice(0, msgIndex + 1);
      }

      this.flowNodes.push(this.cloneObject(targetNode));

      // flow chart 영역으로 현재 edge id return
      let data = {
        edgeId: edge.data.id
      }
      this.nodeButtonClickCallback.emit(data);
      setTimeout(() => {
        this.scrollToBottom();
      });
    } else {
      alert('target node가 없습니다.');
    }
  }

  findEdge(sourceNodeId: string, title: string) {
    let resultEdge = null;
    this.scenario.edges.forEach(edge => {
      if (edge.source === sourceNodeId && edge.data.label === title) {
        resultEdge = Object.assign({}, edge);
      }
    });
    return resultEdge;
  }

  findTargetNode(id: string) {
    let targetNode = null;
    this.scenario.nodes.forEach(node => {
      if (node.id === id) {
        targetNode = Object.assign({}, node);
      }
    });
    return targetNode;
  }

  generateNumArray(itemsL: number, buttonL: number) {
    let length = 0;
    if (itemsL <= buttonL) {
      length = 1;
    } else {
      length = itemsL / buttonL;
      if (itemsL % buttonL > 0) {
        length++;
      }
    }
    return Array.from({length: length}, (v, k) => k * buttonL);
  }

  scrollToBottom(): void {
    try {
      this.divScroll.nativeElement.scrollTop = this.divScroll.nativeElement.scrollHeight;
    } catch (err) { }
  }

  cloneObject(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  makeButtonObject(msg) {
    msg.attr.forEach(item => {
      if (item.tmplCtt && (item.dtlsMsgTypeCd === this.AI_TEMP.BUTTON || item.dtlsMsgTypeCd === this.AI_TEMP.TRANSITION_BQA ||
        item.dtlsMsgTypeCd === this.AI_TEMP.TRANSITION_GO_TO_SKILL || item.dtlsMsgTypeCd === this.AI_TEMP.TRANSITION_GO_TO_STACK) &&
        typeof(item.tmplCtt) === 'string') {
        item.tmplCtt =  JSON.parse(item.tmplCtt);
      }
    });
  }

  isFinal(node) {
    let isFinal = false;
      if (node.attr) {
        node.attr.forEach(item2 => {
          if (item2.dtlsMsgTypeCd === this.AI_TEMP.FINAL || item2.dtlsMsgTypeCd === this.AI_TEMP.FINAL_DATA ||
            item2.dtlsMsgTypeCd === this.AI_TEMP.FINAL_TASK || item2.dtlsMsgTypeCd === this.AI_TEMP.TRANSITION_BQA ||
            item2.dtlsMsgTypeCd === this.AI_TEMP.TRANSITION_GO_TO_SKILL || item2.dtlsMsgTypeCd === this.AI_TEMP.TRANSITION_GO_TO_STACK) {
            isFinal = true;
          }
        });
      }
    return isFinal;
  }
}
