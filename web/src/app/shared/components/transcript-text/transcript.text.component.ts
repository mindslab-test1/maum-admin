import {debounceTime} from 'rxjs/operators';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Subscription} from 'rxjs';
import {TranscriptTextCharacters} from './transcript.text.characters';
import {StorageBrowser} from '../../storage/storage.browser';
import {ClipboardService} from '../../services/clipboard.service';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-transcript-text',
  templateUrl: './transcript.text.component.html',
  styleUrls: ['./transcript.text.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class TranscriptTextComponent implements OnInit, OnDestroy {
  @Input() url: string = null;
  @ViewChild('div') div: ElementRef;
  fontSize = 24;
  fontSizeStep = 4;
  fontMaxSize = 60;
  fontMinSize = 16;
  fontSizeCookieName = 'transcript-font-size';
  caretCharacter = '_';
  caretElementId = 'transcript-caret';

  subscription = new Subscription();
  changeEmitter = new EventEmitter();

  public isUpdated = false;
  @Input() langCode = 'kor';

  constructor(private storageBrowser: StorageBrowser,
              private clipboardService: ClipboardService) {
    // load cookie
    let fontSize = parseInt(this.storageBrowser.get(this.fontSizeCookieName), 10);
    if (!isNaN(fontSize)) {
      this.fontSize = fontSize;
    }
  }

  get errors() {
    let el = this.div.nativeElement;
    let errors = el.querySelectorAll('u');
    let errorTexts = [];
    let i = errors.length;
    while (i--) {
      errorTexts.unshift(errors[i].innerText);
    }
    return errorTexts;
  }

  get element() {
    return this.div.nativeElement;
  }

  get text() {
    const ws = String.fromCharCode(160); // replace 160 (non-breaking whitespace; not catched as whitespace) to whitespace
    return this.div.nativeElement.innerText.replace(new RegExp(ws, 'g'), ' ');
  }

  spellCheck = () => {
    // insert hidden speical element to current caret position
    this.element.focus();
    let caretText = document.createTextNode(this.caretCharacter);
    window.getSelection().getRangeAt(0).insertNode(caretText);
    this.element.blur();

    // get html and check spell errors
    const ws = String.fromCharCode(160); // replace 160 (non-breaking whitespace; not catched as whitespace) to whitespace
    const text = this.text;
    let html = '';
    let errorOpen = false;
    let errorCount = 0;
    let caretMade = false;

    for (let i = 0; i < text.length; i++) {
      let char: string = text.charAt(i);

      // restore caret
      if (!caretMade && char === this.caretCharacter) {
        caretMade = true;
        html += `<span id='${this.caretElementId}'></span>`;
        continue;
      }

      // catch error
      if (TranscriptTextCharacters[this.langCode].indexOf(char) === -1) {
        if (errorOpen) {
          html += char;
        } else {
          errorOpen = true;
          errorCount++;
          html += '<u>' + char;
        }
      } else {
        if (errorOpen) {
          errorOpen = false;
          html += '</u>' + char;
        } else {
          html += char;
        }
      }
    }

    // close unclosed one
    if (errorOpen) {
      html += '</u>';
    }

    // update html
    this.element.innerHTML = html.replace(/\n/g, '<br>');

    // find hidden element's range from new html
    let caretElement = document.getElementById(this.caretElementId);
    let range = document.createRange();
    range.selectNodeContents(caretElement);

    // restore range
    this.element.focus();
    let selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
    range.deleteContents();

    return errorCount === 0;
  };

  onConentChange(e: any) {
    this.changeEmitter.emit();
    this.isUpdated = true;
  }

  ngOnInit() {
    // detect invalid characters repeatedly (off in windows)
    let os = (navigator && navigator.platform || 'win').toLowerCase();
    let interval = os.indexOf('win') !== -1 ? 2000 : 300;
    this.subscription.add(
      this.changeEmitter.pipe(
        debounceTime(interval))
      .subscribe(this.spellCheck)
    );

    // detect paste event
    this.subscription.add(
      this.clipboardService.subscribe((lines: string[]) => {
        this.element.focus();
        let span = document.createElement('span');
        span.innerHTML = lines.join('<br>');

        let selection = window.getSelection();
        let range = selection.getRangeAt(0);
        range.deleteContents(); // remove content of selection
        range.collapse(false); // to paste after current node
        range.insertNode(span); // paste lines
        selection.removeAllRanges(); // reset selection

        range = document.createRange();
        range.selectNode(span); // get range of just pasted node
        range.setStart(range.endContainer, range.endOffset); // set caret to end of range
        selection.addRange(range); // apply range to selection
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  focus = () => {
    this.element.focus();
  };

  clearErrors = () => {
    let errors = this.element.querySelectorAll('u');
    let i = errors.length;
    while (i--) {
      errors[i].remove();
    }
  };

  setContent = (content = '') => {
    this.isUpdated = false;
    this.element.innerHTML = content;
    this.spellCheck();
  };

  zoomIn = () => {
    if (this.fontSize < this.fontMaxSize) {
      this.fontSize += this.fontSizeStep;
      this.storageBrowser.set(this.fontSizeCookieName, this.fontSize + '');
    }
  };

  zoomOut = () => {
    if (this.fontSize > this.fontMinSize) {
      this.fontSize -= this.fontSizeStep;
      this.storageBrowser.set(this.fontSizeCookieName, this.fontSize + '');
    }
  }
}
