import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-tag-input-item',
  templateUrl: './tag-input-item.component.html',
  styleUrls: ['./tag-input-item.component.scss'],
})

export class TagInputItemComponent {
  @Input() selected: boolean;
  @Input() text: string;
  @Input() index: number;
  @Output() tagRemoved: EventEmitter<number> = new EventEmitter();

  constructor() {
  }

  removeTag() {
    this.tagRemoved.emit(this.index);
  }
}
