import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {StorageBrowser} from '../../storage/storage.browser';
import {AlertComponent} from '../dialog/alert.component';
import {MatDialog} from '@angular/material';
import {DownloadService} from '../../services/download.service';
import {DatePipe} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-git',
  templateUrl: './git.component.html',
  styleUrls: ['./git.component.scss']
})
export class GitComponent implements OnInit, OnChanges {
  API_URL;

  @Input() getTitle = null;
  @Input() getCommitListUrl = null;
  @Input() getDiffUrl = null;
  @Input() fileId = null;
  @Input() getFileByCommitUrl = null;

  @ViewChild('diffContainer') diffContainer: ElementRef;

  commitList = [];

  selectedCommit1 =
    {hashKey: undefined, commitTime: undefined, updaterId: undefined, updaterEmail: undefined};
  selectedCommit2 =
    {hashKey: undefined, commitTime: undefined, updaterId: undefined, updaterEmail: undefined};

  emptyDiff = true;
  param = {};

  constructor(@Inject(StorageBrowser) protected storage: StorageBrowser,
              private dialog: MatDialog,
              private http: HttpClient,
              private _changeDetectionRef: ChangeDetectorRef,
              private downloadService: DownloadService) {
    this.API_URL = environment.maumAiApiUrl;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this._changeDetectionRef.detectChanges();
    this.getCommitList();
    this.clearDiff();
    if (this.fileId !== '') {
      this.changeLatestValue();
    }
  }

  getCommitList() {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('m2uWorkspaceId');
    this.param['id'] = this.fileId;
    if (this.checkEmpty(this.param['workspaceId']) && this.checkEmpty(this.param['id'])
      && this.checkEmpty(this.getCommitListUrl)) {
      this.http.post(this.API_URL + this.getCommitListUrl, this.param)
      .subscribe(response => {
          let res: any;
          res = response;
          if (res) {
            res.forEach(commit => {
              commit['commitTime'] =
                new DatePipe('en-US').transform(commit['commitTime'], 'yyyy/MM/dd HH:mm');
            });
            this.commitList = res;

            // commitList 불러오면서 selectedCommit 지정
            if (this.selectedCommit1 && this.selectedCommit1.hashKey) {
              this.selectedCommit1 =
                this.commitList.find(commit => {
                  return commit.hashKey === this.selectedCommit1.hashKey;
                });
            }
            if (this.selectedCommit2 && this.selectedCommit2.hashKey) {
              this.selectedCommit2 =
                this.commitList.find((commit) => {
                  return commit.hashKey === this.selectedCommit2.hashKey;
                });
            }
          }
        },
        err => {
          this.openAlertDialog('Error', 'Failed get commit list', 'error');
        });
    }
  }

  getDiff() {
    if (this.selectedCommit1 !== this.selectedCommit2 && this.getDiffUrl) {
      this.param = {};
      this.param['workspaceId'] = this.storage.get('m2uWorkspaceId');
      this.param['id'] = this.fileId;
      this.param['source'] = this.selectedCommit1.hashKey;
      this.param['target'] = this.selectedCommit2.hashKey;

      this.http.post(this.API_URL + this.getDiffUrl, this.param, {responseType: 'text'})
      .subscribe(res => {
          if (res) {
            const Diff2html = window['Diff2Html'];
            let diffHtml = Diff2html.getPrettyHtml(res, {
              inputFormat: 'diff',
              outputFormat: 'side-by-side',
              matching: 'lines',
              synchronisedScroll: true,
            });
            this.diffContainer.nativeElement.innerHTML = diffHtml;
            this.emptyDiff = false;

          } else {
            this.clearDiff();
          }
        },
        err => {
          this.openAlertDialog('Error', 'Failed get git diff', 'error');
        });
    } else {
      this.clearDiff();
    }
  }

  changeVersion() {
    if (this.selectedCommit1 && this.selectedCommit2 &&
      this.selectedCommit1.hashKey && this.selectedCommit2.hashKey) {
      if (this.selectedCommit1.hashKey === this.selectedCommit2.hashKey) {
        this.clearDiff();
      } else {
        this.getDiff();
      }
    }
  }

  clearDiff() {
    this.diffContainer.nativeElement.innerHTML = '';
    this.emptyDiff = true;
  }

  download() {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('m2uWorkspaceId');
    this.param['id'] = this.fileId;
    this.param['source'] = this.selectedCommit1.hashKey;
    if (this.selectedCommit1.hashKey === undefined || this.selectedCommit1.hashKey === '') {
      this.openAlertDialog('Error', 'Failed Get File By Commit', 'error');
    } else {
      this.http.post(this.API_URL + this.getFileByCommitUrl, this.param, {responseType: 'blob'})
      .subscribe(res => {
        this.downloadService.downloadTxtFile(res, 'git_' + this.selectedCommit1.hashKey.substring(0, 8));
      }, err => {
        this.openAlertDialog('Error', 'Failed download git', 'error');
      });
    }
  }

  getShortId(id) {
    if (id) {
      return id.substring(0, 8);
    } else {
      return '';
    }
  }

  checkEmpty(param) {
    if (param !== null && typeof (param) !== 'undefined' && param !== '') {
      return true;
    } else {
      return false;
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

  changeLatestValue() {
    this.param = {};
    this.param['workspaceId'] = this.storage.get('m2uWorkspaceId');
    this.param['id'] = this.fileId;
    if (this.checkEmpty(this.param['workspaceId']) && this.checkEmpty(this.param['id'])
      && this.checkEmpty(this.getCommitListUrl)) {
      this.http.post(this.API_URL + this.getCommitListUrl, this.param)
      .subscribe(response => {
          let res: any;
          res = response;
          if (res) {
            res.forEach(commit => {
              commit['commitTime'] =
                new DatePipe('en-US').transform(commit['commitTime'], 'yyyy/MM/dd HH:mm');
            });
            this.commitList = res;
            console.log('###commitList', res);
            if (this.commitList.length > 0) {
              let time_arr: any[] = this.commitList.map(x => x.commitTime).sort().reverse();
              let res_arr: any[];
              // 가장 최신 데이터를 selectedCommit2에, 그 다음 최신 데이터를 selectedCommit1에 지정.
              if (time_arr.length > 1) {
                res_arr = this.commitList.filter(y => y.commitTime === time_arr[0]);
                if (res_arr.length > 0) {
                  this.selectedCommit2 = res_arr[0];
                  res_arr = res_arr.slice(1);
                }
                if (res_arr.length > 0) {
                  res_arr = res_arr.filter(y => y.commitTime === time_arr[1]);
                  this.selectedCommit1 = res_arr[0];
                } else {
                  this.selectedCommit1 = this.commitList.filter(y => y.commitTime === time_arr[1])[0];
                }
              }
              this.changeVersion();
            }
          }
        },
        () => {
          this.openAlertDialog('Error', 'Failed set latest commits', 'error');
        });
    }
  }
}
