import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-form-control-inline',
  templateUrl: './form-control-inline.component.html',
  styleUrls: ['./form-control-inline.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FormControlInlineComponent {

  @Input() label = 'No Label';
  @Input() tooltip: string = null;
  @Input() desc: string = null;

  constructor() {
  }

}
