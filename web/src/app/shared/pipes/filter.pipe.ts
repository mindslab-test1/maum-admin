import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(rows: any, keyword: any, comparator: any): any {
    return (keyword) ? rows.filter(row => comparator(row, keyword)) : rows;
  }
}
