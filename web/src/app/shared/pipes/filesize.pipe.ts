import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'fileSize'})
export class FileSizePipe implements PipeTransform {
  private units = [
    'bytes',
    'KB',
    'MB',
    'GB',
    'TB',
    'PB'
  ];

  transform(bytes: any = 0, precision = 2): string {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
      return '?';
    }
    let unitIndex = 0;
    while (bytes >= 1024) {
      bytes /= 1024;
      unitIndex++;
    }
    return ((bytes % 1 === 0) ? bytes : bytes.toFixed(+precision)) + ' ' + this.units[unitIndex];
  }
}
