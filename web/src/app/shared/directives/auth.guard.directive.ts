import {OnInit, OnDestroy, Directive, ElementRef, Input} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {AuthService} from '../../core/auth.service';
import {LoopConfigFileApi} from '../../core/sdk/services/custom/LoopConfigFile';

@Directive({
  selector: '[appAuthGuard]'
})
export class AuthGuardDirective implements OnInit, OnDestroy {

  @Input() appAuthGuard: string[] | any = [];

  subscription: Subscription;
  originalDisplay: any;

  constructor(private el: ElementRef,
              private authService: AuthService,
              private configApi: LoopConfigFileApi) {
    this.originalDisplay = el.nativeElement.style.display;
  }

  ngOnInit() {
    // this.subscription = this.authService.checkWithACL(this.appAuthGuard.ACL)
    //   .subscribe(granted => {
    console.log('#@ this.appAuthGuard.ACL :', this.appAuthGuard);
    let passed = this.authService.checkWithACL(this.appAuthGuard.data.ACL);
    console.log('#@ passed :', passed);
    // if (passed) {
    //
    //     // console.log(granted, this.appAuthGuard, this.el.nativeElement);
    //     let displayCss = this.originalDisplay;
    //
    //   this.el.nativeElement.style.display = passed ? displayCss : 'none';
    // }
    // USER의 Role이 없다면 sidebar의 메뉴에서 보여주지 않는다
    if (!passed) {
      this.appAuthGuard.visible = false;
    }
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }
}
