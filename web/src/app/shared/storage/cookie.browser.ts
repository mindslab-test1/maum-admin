import {Injectable} from '@angular/core';

@Injectable()
export class CookieBrowser {

  get(key: string): any {
    let match = document.cookie.match(new RegExp(key + '=([^;]+)'));
    if (match) {
      return match[1];
    } else {
      return null
    }
  }


  delete(key: string) {
    let expireDate = new Date();
    // 어제 날짜를 쿠키 소멸 날짜로 설정한다.
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = key + '= ' + '; expires=' + expireDate.toUTCString() + '; path=/';
  }
}
