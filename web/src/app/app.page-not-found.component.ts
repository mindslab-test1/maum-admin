import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from './core/actions';
import {MenuService} from './core/menu.service';

import {Router} from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './app.page-not-found.component.html',
  styleUrls: ['./app.page-not-found.component.scss']
})
export class AppPageNotFoundComponent implements OnInit {

  urlInfo: string;
  hasNoLayout: boolean;

  constructor(
    private store: Store<any>,
    private menuService: MenuService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.store.dispatch({type: ROUTER_LOADED});
    let user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.urlInfo = 'dashboard';
      this.hasNoLayout = false;
    } else {
      this.urlInfo = 'auth/signin';
      this.hasNoLayout = true;
    }
  };

  backPage() {
    this.router.navigateByUrl(this.urlInfo);

  };

  goHome() {
    this.router.navigateByUrl('dashboard');
  };

}
