import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';

// import { ROUTER_LOADED } from '../core/actions';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AuthComponent implements OnInit {

  constructor(
    private store: Store<any>,
  ) {
  }

  ngOnInit() {
  }
}
