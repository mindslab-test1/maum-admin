import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component, Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageBrowser} from './core/sdk/storage/storage.browser';
import {Store} from '@ngrx/store';
import {MenuItem} from './core/menu-item';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app.sidebar.component.html',
})
export class AppSidebarComponent implements OnInit, OnChanges {

  @Input() siblingMenuItems: MenuItem[] = [];
  @Input() parentPath = '';
  @Input() root = null;
  @Input() dept = 1;
  @Input() dialogModelExist = false;

  constructor(private router: Router, private _changeDetectionRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    /*
        this._changeDetectionRef.detectChanges();

        let navi = this.router.getCurrentNavigation();
        console.log('sibling menu items', navi);
        if (navi  && this.siblingMenuItems) {
          this.siblingMenuItems.forEach(item => {
            // TODO: 현재 ROUTING의 변경이 없을 때에는 현재의 처음 메뉴로 LINK로 이동한다.

            // 이들 메뉴 중에서 어떤 메뉴가 선택되었는지를 인지할 것인가?
            if (item.routerLink && navi.finalUrl) {
              let final = navi.finalUrl.toString();
              let link = item.routerLink.urlTree.toString();
              if (final === link) {
                item.selected = true;
              }
            }
          });
        }
    */
  }

  /*
    click(nav, $event?) {
      this.clearActive(this.root);

      if (!nav.children) {
        nav['active'] = true;
        this.clearExpend([], this.root);

        let url = this.parentPath + '/' + nav.path;

        // variable path 이동시 :value로 되어있는 부분을 실제 데이터로 변경
        const currentModel = JSON.parse(localStorage.getItem('currentDialogModel'));

        if (currentModel) {
          url = url.replace(':modelId', currentModel.id);
        }

        this.router.navigateByUrl(url);
      } else {
        nav.open = !nav.open;
      }

      if ($event) {
        $event.preventDefault();
      }
    }

    clearActive(navs) {
      if (navs.length > 0) {
        navs.forEach(nav => {
          if (nav) {
            if (nav.children) {
              this.clearActive(nav.children);
            } else {
              nav['active'] = false;
            }
          }
        });
      }
    }

    clearExpend(pParent?, pRoot?) {
      let parent = pParent;
      let root = pRoot;

      root.forEach((item, index) => {
        if (item.children) {
          item['active'] = false;
          item['open'] = false;
          parent.push(item);

          this.clearExpend(parent, item.children);
        } else {
          if (item['active']) {
            parent.forEach(item2 => {
              item2['active'] = true;
              item2['open'] = true;
            });
          }
        }
      });
      parent.splice(0, parent.length);
    }

   */
}
