import {map, filter} from 'rxjs/operators';
import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Subscription} from 'rxjs';
import {
  ActivatedRoute,
  NavigationEnd, NavigationStart,
  Router
} from '@angular/router';
import {MatMenu, MatSidenav} from '@angular/material';
import {DEFAULT_INTERRUPTSOURCES, Idle} from '@ng-idle/core'
import {environment} from '../environments/environment';
import {menuTree} from '../environments/environment';
import {ConsoleUserApi, LoopBackConfig} from 'app/core/sdk';
import {AuthService} from 'app/core';
import {Store} from '@ngrx/store';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {MenuService} from './core/menu.service';
import {MenuItem} from './core/menu-item';
import {SecondsToTimePipe} from 'app/shared';
import {ROUTER_LOADED, ROUTER_LOADING} from './core/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit, OnDestroy {
  isLoading = true;

  user: any = null;
  notifications = [];
  lastTwinkle: number;
  autoSignOutSec: number;

  subscription = new Subscription();
  notiHandler: any = null;
  expireHandler: any = null;

  focusTree = null;
  currentSideBar = {};

  parentPath: '';
  currentNavs: any[] = [];

  userGroup = [];
  selectedUserGroupId = '';
  workspace: any;
  selectedWorkspaceId = '';

  hideNavDepthFlag = true;
  idx = 0;

  @ViewChild('sideNav') sideNav: MatSidenav;
  @ViewChild('authMenu') authMenu: MatMenu;

  constructor(private store: Store<any>,
              private router: Router,
              private idle: Idle,
              private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              @Inject(StorageBrowser) protected storage: StorageBrowser,
              private consoleUserApi: ConsoleUserApi,
              private menuService: MenuService) {

    if (environment.production) {
      // production mode
      environment.wsUrl = (location.protocol === 'https:' ? 'wss://' : 'ws://')
        + location.host + environment.apiPath;
      LoopBackConfig.setBaseURL('');
    } else {
      // development mode
      if (!!environment.apiBaseUrl) {
        environment.apiUrl = environment.apiBaseUrl + environment.apiPath;
        LoopBackConfig.setBaseURL(environment.apiBaseUrl);
      }
      if (!!environment.wsBaseUrl) {
        environment.wsUrl = environment.wsBaseUrl + environment.apiPath;
      } else {
        environment.wsUrl = (location.protocol === 'https:' ? 'wss://' : 'ws://')
          + location.host + environment.apiPath;
      }
      console.log('development loopBack path :', LoopBackConfig.getPath());
      console.log('development API path :', environment.apiUrl);
      console.log('development WS path :', environment.wsUrl);
      console.log('development maumAiApi url :', environment.maumAiApiUrl);
    }
  }

  ngOnInit() {

    // nav tree 초기화
    this.menuService.setScheme('chatbot-builder', menuTree);

    // localStorage에서 users 정보를 갖고 옵니다
    this.user = JSON.parse(localStorage.getItem('user'));

    // 자동로그아웃 idle 초기화
    this.idle.stop();
    this.idle.setIdle(1);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onIdleEnd.subscribe(() => this.lastTwinkle = Date.now());
    this.idle.setTimeout(3600);
    this.idle.onTimeoutWarning.subscribe((countdown: number) => {
      this.autoSignOutSec = countdown;
    });
    this.idle.onTimeout.subscribe(() => {
      this.logout();
    });

    // router navigation start event listen
    this.router.events.pipe(
      filter(event => event instanceof NavigationStart),
      map(() => {

      })).subscribe((result: any) => {
        // route 이동시 자동로그아웃 멈춤
        this.idle.stop();
        this.store.dispatch({type: ROUTER_LOADING});
     });

    // router navigation end event listen
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        // nav navList 변수초기화
        this.focusTree = null;
        this.hideNavDepthFlag = true;
      }))
    .subscribe((result: any) => {
      // Router(페이지 경로)가 변경되면 Workspace를 설정합니다
      this.initWorkspace();

      // 자동 로그아웃 idle 재시작
      if (this.router.url !== '/auth/signin') {
        this.idle.watch();
      }

      // USER의 Role이 없다면 sidebar의 메뉴에서 보여주지 않는다
      // console.log('#@ this.currentNavs :', this.currentNavs);
      // TODO, FIXME, 권한 점검하기
      if (this.currentNavs) {
        this.currentNavs.forEach((nav, index) => {
          let passed = this.authService.checkWithACL(nav.data.ACL);
          if (!passed) {
            nav.visible = false;
          }
        });
      }

      this.subscription.add(
      // 라우팅에 따른 UI 상태
        this.store.select(s => s.router)
        .subscribe(_router => {
          // sidenav의 메뉴
          // this.navs = _router.navs;

          // sidebar의 메뉴
          // this.currentNav = _router.navs.find(n => n.active) || null;

          // 로딩 중인지?
          // isLoading은 현재 로딩중이라면 m2u-main을 hidden, rolling을 open
          // 로딩이 끝나면 m2u-main을 open, rolling을 hidden 합니다
          this.isLoading = _router.loading;

          // reset notification
          // this.checkNoti();
        })
      );
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  get tree(): MenuItem[] {
    return this.menuService.getTree();
  }

  get current(): MenuItem {
    return this.menuService.getCurrent();
  }

  get currentPathTree(): MenuItem[] {
    return this.menuService.getCurrentPathTree();
  }

  get hasNoSidebar() {
    return this.menuService.hasNoSidebar();
  }

  get hasNoLayout() {
    return this.menuService.hasNoLayout();
  }

  select(item: MenuItem, name, idx): void {
    // 자식을 선택하는 경우이니, sideBar를 바로 보여줘야 한다.
    if (idx !== null) {
      this.menuBackGroundSetting(name, idx);
    } else {
      // 1 depth가 visible: false 인것은 화면에 표현되지 않기에 백그라운드 효과는 없음.
      let backGroundInit = <HTMLInputElement>document.getElementsByName(name)[(this.idx)];
      this.idx = 0;
      backGroundInit.style.background = '';
    }
    this.menuService.select(item);
  }

  checkExpired() {
    if (this.user && !this.authService.isTokenValid()) {
      if (this.authService.isTokenValid(this.lastTwinkle)) {
        this.authService.elongateTokenTTL();
      } else {
        this.authService.logout();
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    if (this.notiHandler) {
      clearInterval(this.notiHandler);
    }
    if (this.expireHandler) {
      clearInterval(this.expireHandler);
    }
  }

  profile() {
    let profileInfo;

    this.tree.forEach((row, index) => {
      console.log(index);
      if (row.label === 'Profile') {
        profileInfo = row;
      }
    });

    if (profileInfo !== undefined) {
      this.select(profileInfo, 'sideNavi', null);
      this.router.navigateByUrl('m2u-builder/profile');
    }
  }

  logout() {
    this.authService.logout();
    this.idle.stop();
  }


  linkSetting(url, name, idx) {
    // this.hasNoSidebar = true;
    this.menuBackGroundSetting(name, idx);
    this.router.navigateByUrl('/' + url[0]);
  }

  menuBackGroundSetting(name, idx) {
    // 기존 백그라운드 색을 준 메뉴는 백그라운드 초기 작업
    let backGroundInit = <HTMLInputElement>document.getElementsByName(name)[(this.idx)];
    this.idx = idx;
    if (backGroundInit !== undefined) {
      backGroundInit.style.background = '';
    }
    // 선택한 메뉴에 백그라운드 색 처리 작업
    if (document.getElementsByName(name)[idx] !== undefined) {
      document.getElementsByName(name)[idx].style.background = '#d0d0d0';
    }

  }

  goto(navs, nav, idx) {

    // 기존 백그라운드 색을 준 메뉴는 백그라운드 초기 작업
    let backGroundInit = <HTMLInputElement>document.getElementsByName('sideNavi')[this.idx];

    if (this.idx !== undefined) {
      backGroundInit.style.background = '';
    }
    this.idx = idx;

    // 선택한 메뉴에 백그라운드 색 처리 작업
    document.getElementById('sideNaviInside_' + idx).style.background = '#d0d0d0';

    let routeUrl = '';

    if (nav.parentPath === '') {
      routeUrl += '/' + nav.path;
    } else {
      routeUrl += '/' + nav.parentPath + '/' + nav.path;
    }

    navs.forEach(item => {
      item['active'] = false;
    });
    nav['active'] = true;

    if (this.sideNav.opened) {
      this.sideNav.close();
    }

    this.router.navigateByUrl(routeUrl);
  }

  goHome() {
    this.authService.goHome();
  }

  signOut() {
    // this.hasNoSidebar = true;
    this.authService.logout();
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  // users icon 클릭전에 현재 users 정보를 설정(authService에서 login 발생 이후 users 갱신)
  setCurrentUser(): void {
    // localStorage에서 users 정보를 갖고 옵니다
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  /**
   * 페이지가 시작되면 현재 UserGroup과 Workspace를 설정합니다
   * **/
  initWorkspace() {
    // 1.UserGroup 조회
    this.userGroup = this.storage.get('m2uUserGroup');

    // 2.workspace 조회
    this.workspace = this.storage.get('m2uWorkspace');

    // 3.selectedUserGroupId 설정
    if (this.storage.get('m2uUserGroupId') === null && this.storage.get('m2uUserGroup') !== null) {
      this.selectedUserGroupId = this.userGroup[0].groupId;
      this.storage.set('m2uUserGroupId', this.selectedUserGroupId);
    } else {
      this.selectedUserGroupId = this.storage.get('m2uUserGroupId');
    }

    // 4.selectedWorkspaceId 설정
    if (this.storage.get('m2uWorkspaceId') === null && this.storage.get('m2uWorkspace') !== null) {
      this.selectedWorkspaceId = this.workspace.id;
      this.storage.set('m2uWorkspaceId', this.selectedWorkspaceId);
    } else {
      this.selectedWorkspaceId = this.storage.get('m2uWorkspaceId');
    }
  }

  /**
   * Workspace가 변경되면 변수를 설정합니다
   * **/
  changeWorkspace() {
    this.storage.set('m2uWorkspaceId', this.selectedWorkspaceId);
  }

  /**
   * UserGroup이 변경되면 변수를 설정하고 GroupId에 해당하는 Workspace를 조회합니다
   * **/
  changeUserGroup() {
    this.storage.set('m2uUserGroupId', this.selectedUserGroupId);

    // #@ 그룹이 변경되면 GroupId에 해당하는 workspace를 조회 및 변수 설정
    let queryForWorkspace = {
      'queryParams': {
        'groupId': this.selectedUserGroupId
      }
    };
    this.consoleUserApi.getWorkspace(this.consoleUserApi.getCurrentId(), {}).subscribe(
      resultSet => {
        this.storage.set('m2uWorkspace', JSON.stringify(resultSet));
        this.workspace = this.storage.get('m2uWorkspace');

        this.selectedWorkspaceId = resultSet[0].id;
        this.storage.set('m2uWorkspaceId', this.selectedWorkspaceId);
      }, err => {
        console.log('#@ getWorkspace err :', err);
      }
    );
  }

  // 로그아웃 시간 연장
  refreshSignOutTime() {
    this.idle.stop();
    this.idle.watch();
  }

  // 1dept 메뉴 마우스 진입 이벤트
  mouseEnterNavList(item) {
    // 1dept 메뉴가 활성화 되있을 경우
    if (!this.hideNavDepthFlag && item) {
      this.focusTree = item;
    }
  }

  // 1dept 메뉴 마우스 이탈 이벤트
  mouseLeaveNavList() {
    // 하단의 빈영역이 아닐 경우
    if (!this.focusTree) {
      this.focusTree = null;
    }
  }

  // 컨텐츠 영역 마우스 진입 이벤트
  mouseEnterContent() {
    this.focusTree = null;
  }

  // 메뉴 하단 토글버튼 이벤트
  toggleMenu() {
    this.hideNavDepthFlag = !this.hideNavDepthFlag;

    if (this.hideNavDepthFlag) {
      this.focusTree = null;
    }
  }


}
