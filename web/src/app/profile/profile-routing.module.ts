import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AccountProfileComponent} from './components/view-profile/account-profile.component';
import {AuthService} from '../core';
import {WorkspacesComponent} from './components/workspaces/workspaces.component';
import {SettingsComponent} from './components/settings/settings.component';
import {GroupsComponent} from './components/groups/groups.component';
import {GroupDetailComponent} from './components/group-detail/group-detail.component';
import {GroupMembersComponent} from './components/group-members/group-members.component';
import {HistoryComponent} from './components/history/history.component';

const routes: Routes = [
  {
    // sideNav를 선택하면 /profile 로 이동합니다
    // /profile/view 으로 redirectTo 를 해줍니다
    path: '',
    children: [
      {
        // sideNav를 선택하면 /profile 로 이동합니다
        // /profile/view 으로 redirectTo 를 해줍니다
        path: '',
        component: AccountProfileComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ
            ]
          },
        }
      },
      {
        path: 'workspaces',
        component: WorkspacesComponent,
        data: {
          ACL: {
            roles: [
              'svd' + AuthService.ROLE.READ
            ]
          },
        },
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
      {
        path: 'history',
        component: HistoryComponent,
      },
      {
        path: 'groups',
        component: GroupsComponent,
        children: [
          {
            path: ':id',
            component: GroupDetailComponent,
          },
          {
            path: ':id/members',
            component: GroupMembersComponent,
          }
        ]
      },
    ],
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {
}
