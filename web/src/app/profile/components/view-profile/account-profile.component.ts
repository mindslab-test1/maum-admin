import {Component, OnInit} from '@angular/core';
import {
  ErrorStateMatcher,
  MatDialog,
  MatSnackBar,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';

import {ROUTER_LOADED} from '../../../core/actions';
import {
  AlertComponent,
  AppEnumsComponent, CHECK_PASS, ConfirmComponent,
  FormErrorStateMatcher,
  getErrorString,
  ONE_NUM
} from '../../../shared';
import {ConsoleUserApi} from '../../../core/sdk';
import {
  CAPITAL_ONE,
  EXCLUDE_CHARACTER,
  EXCLUDE_SAME_THREE_CHARACTER,
  TWO_ALPHABET
} from '../../../shared/values/values';


@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class AccountProfileComponent implements OnInit {
  subscription = new Subscription();
  user: any;
  currentPassword: string = null;
  newPassword: string = null;
  confirmPassword: string = null;

  currentPasswordFormControl = new FormControl('', [
    Validators.required
  ]);

  newPasswordFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(CHECK_PASS)
  ]);

  confirmPasswordFormControl = new FormControl('', [
    Validators.required
  ]);

  changeButtonFlag = false;
  currentPasswordMsg = null;
  newPasswordMsg = null;
  confirmPasswordMsg = null;

  constructor(private store: Store<any>,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              public appEnum: AppEnumsComponent,
              private consoleUserApi: ConsoleUserApi,
              public formErrorStateMatcher: FormErrorStateMatcher) {
  }

  ngOnInit() {
    let user = this.consoleUserApi.getCachedCurrent();
    if (!user) {
      return;
    }
    this.user = user;
    this.store.dispatch({type: ROUTER_LOADED});
  }

  /**
   * 현재 비밀번호랑 새로운 비밀번호랑 다른 문자가 3개 이상 있는지 조건 체크
   * @returns {boolean}
   */
  getComparePassword() {
    if (this.currentPassword !== null && this.newPassword !== null) {
      let count = 0;
      for (let i = 0; i < this.currentPassword.length; i++) {
        if (this.currentPassword.charAt(i) !== this.newPassword.charAt(i)) {
          count++;
        }
      }
      if (count < 4) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /***
   * 에러 상황에 맞게 에러메세지 세팅 함수
   * @param {string} controlName
   */
  getErrorMessage(controlName: string) {
    switch (controlName) {
      case 'currentPassword':
        if (this.currentPasswordFormControl.hasError('required')) {
          this.currentPasswordMsg = `Current password is required.`;
        } else if (this.currentPasswordFormControl.valid) {
          this.currentPasswordMsg = null;
        }
        break;
      case 'newPassword':
        if (this.newPasswordFormControl.hasError('required')) {
          this.newPasswordMsg = `New password is required`;
        } else if (this.newPasswordFormControl.hasError('pattern')) {
          this.newPasswordMsg = `8~20 characters(least 1 uppercase, 1 lowercase, and 1 number)`
        } else if (this.newPasswordFormControl.valid) {
          this.newPasswordMsg = null;
        }
        break;
      case 'confirmPassword':
        if (this.confirmPasswordFormControl.hasError('required')) {
          this.confirmPasswordMsg = `Confirm Password is required.`;
        } else if (this.confirmPasswordFormControl.hasError('matched')) {
          this.confirmPasswordMsg = `Confirm Password is not matched.`;
        } else if (this.confirmPasswordFormControl.valid) {
          this.confirmPasswordMsg = null;
        }
        break;
    }
  }

  /**
   * currentPassword, newPassword, confirmPassword가 다 만족이 될 시에 비밀번호 변경 버튼을 활성화 시킨다.
   */
  checkValid(controlName?: string) {
    this.getErrorMessage(controlName);
    if (this.newPasswordFormControl.valid &&
      this.currentPasswordFormControl.valid && this.confirmPasswordFormControl.valid) {
      this.changeButtonFlag = true;
    } else {
      this.changeButtonFlag = false;
    }
  }

  /**
   * 비밀번호 변경 함수
   */
  changePassword() {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = 'Change Password?';
    ref.afterClosed().subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      if (this.confirmPassword !== undefined && this.newPassword !== this.confirmPassword) {
        this.snackBar.open('Confirm Password is not matched.', null, {duration: 2000});
        return;
      }
      this.consoleUserApi.changePassword(this.currentPassword, this.newPassword).subscribe(
        res => {
          this.currentPasswordFormControl.reset();
          this.newPasswordFormControl.reset();
          this.confirmPasswordFormControl.reset();
          this.snackBar.open('Password changed.', 'Confirm', {duration: 2000});
        },
        err => {
          this.snackBar.open('Invalid Current Password.', null, {duration: 2000});
        }
      );
    });
  }

}
