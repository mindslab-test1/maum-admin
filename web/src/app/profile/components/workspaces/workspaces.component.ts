import {
  Component, OnInit, OnDestroy, ViewChild, TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSnackBar, MatSidenav, MatPaginator} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../core/actions';
import {
  AppObjectManagerService,
  TableComponent,
  getErrorString
} from '../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../core/sdk';
import {MatTableDataSource} from '@angular/material/table';
import {forkJoin} from 'rxjs/internal/observable/forkJoin';
import {map} from 'rxjs/operators';


@Component({
  selector: 'app-workspace-view',
  templateUrl: './workspaces.component.html',
  styleUrls: ['./workspaces.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class WorkspacesComponent implements OnInit {
  page_title: any;
  page_description: any;

  actions: any[] = [];
  table: any;
  filterKeyword: FormControl;
  panelToggleText: string;

  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;

  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  header = [];

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private grpc: GrpcApi,
              private userApi: ConsoleUserApi,
              private objectManager: AppObjectManagerService) {
  }

  ngOnInit() {
    let id = 'dam';
    let roles = this.userApi.getCachedCurrent().roles;

    this.header = [
      {attr: 'id', name: 'WorkspaceId', isSort: true, width: '40%'},
      {attr: 'path', name: 'path', isSort: true, onClick: this.setWorkspaceId},
      {attr: 'ownerType', name: 'Type', isSort: true},
      {attr: 'ownerId', name: 'Owner', isSort: true}
    ];

    /////////////////////////////////////////////////
    // localStorage에서 users 정보를 갖고 옵니다
    let user = JSON.parse(localStorage.getItem('user'));
    let tempUserId = user.id;

    // #@ 1.Workspace 조회
    let queryForWorkspace = {
      'queryParams': {
        'userId': tempUserId
      }
    };

    // 2개의 동시 호출 결과를 묶을 수 있다.
    forkJoin(
      // 사용자의 개인 workspace를 가져온다.
      this.userApi.getWorkspace(this.userApi.getCurrentId(), {}),
      // 사용자가 속한 그룹의 워크스페이스를 가져온다.
      this.userApi.getWorkspaces(this.userApi.getCurrentId()),
    ).pipe(
      // 이를 묶어서 파이프로 내보낸다.
      // 두개의 결과물을 하나의 배열로 만든다.
      map(data => [data[0]].concat(data[1]))
    ).subscribe(workspaces => {
      // 최종 데이터를 처리한다.
      this.rows = workspaces;
      // this.store.dispatch({type: WORKSPACE_UPDATE_ALL, payload: resultSet});
      this.store.dispatch({type: ROUTER_LOADED});
    }, err => {
      console.log('#@ getWorkspace err :', err);
      let message = `Something went wrong. [${getErrorString(err)}]`;
      this.snackBar.open(message, 'Confirm', {duration: 10000});
      this.store.dispatch({type: ROUTER_LOADED});
    });
  }

  /**
   * localstorage에 workspaceId를 설정
   * row에는 현재 선택된 row의 모든 값
   *
   * */
  setWorkspaceId: any = (row: any) => {
    // console.log('#@ setWorkspace row:', row);
    // localstorage에 선택한 workspace를 설정
    localStorage.setItem('m2uWorkspace', row.path);
    localStorage.setItem('m2uWorkspaceId', row.id);
    // 설정이 완료 되면 dashboard로 이동
    this.router.navigateByUrl('dashboard');
  };
  /////////////////////////////////////////////////


  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }
}
