import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HistoryEntity} from '../../entity/history/history.entity';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

@Injectable()
export class HistoryService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  getAllHistories(historyEntity: HistoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/management/histories/getAllHistories', historyEntity);
  }

  getTotalHistories(historyEntity: HistoryEntity): Observable<any> {
    return this.http.post(this.API_URL + '/management/history/getTotalHistories', historyEntity);
  }

}
