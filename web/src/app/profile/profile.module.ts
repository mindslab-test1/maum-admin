import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfileRoutingModule} from './profile-routing.module';
import {MyProfileComponent} from './components/my-profile/my-profile.component';
import {SettingsComponent} from './components/settings/settings.component';
import {GroupsComponent} from './components/groups/groups.component';
import {AccountProfileComponent} from './components/view-profile/account-profile.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatSidenavModule, MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../shared/shared.module';
import {WorkspacesComponent} from './components/workspaces/workspaces.component';
import {HistoryComponent} from './components/history/history.component';
import {GroupDetailComponent} from './components/group-detail/group-detail.component';
import {GroupMembersComponent} from './components/group-members/group-members.component';
import {HistoryService} from "./services/history/history.service";

@NgModule({
  declarations: [MyProfileComponent,
    SettingsComponent,
    GroupsComponent,
    AccountProfileComponent,
    WorkspacesComponent,
    HistoryComponent,
    GroupDetailComponent,
    GroupMembersComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    MatSidenavModule,
    SharedModule,
    MatSelectModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule
  ],
  exports: [
    WorkspacesComponent
  ],
  providers: [
    HistoryService
  ]
})
export class ProfileModule {
}
