import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from './core/actions';

@Component({
  selector: 'app-under-construction',
  templateUrl: './app.under-construction.component.html',
  styleUrls: ['./app.under-construction.component.scss']
})
export class AppUnderConstructionComponent implements OnInit {

  constructor(
    private store: Store<any>,
  ) {
  }

  ngOnInit() {
    this.store.dispatch({type: ROUTER_LOADED});
  }

}
