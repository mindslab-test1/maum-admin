import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSnackBar, MatSidenav} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED, ENGINE_UPDATE_ALL, ENGINE_DELETE} from '../../../../core/actions';
import {AuthService} from '../../../../core/auth.service';
import {
  AppObjectManagerService, AlertComponent, AppEnumsComponent, getErrorString
} from '../../../../shared';
import {ConsoleUserApi, GrpcApi} from '../../../../core/sdk';
import {EngineConfigurationPanelComponent} from '../engine-configuration-panel/engine-configuration-panel.component';
import {EngineApi} from '../../../../core/sdk/services/custom';


import {ConfirmComponent, TableComponent} from 'app/shared';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-engine-configuration-view',
  templateUrl: './engine-configuration-view.component.html',
  styleUrls: ['./engine-configuration-view.component.scss']
})
export class EngineConfigurationViewComponent implements OnInit {

  page_title: any; // = 'Dialog Agents'(보류);
  page_description: any; // = 'Add Dialog Agent Managers and update information about Dialog Agent Managers.'(보류);

  engine = {
    name: undefined,
    categories: [],
    bookmarkList: []
  };
  engines: any;
  actions: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  row: any;
  header;
  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('infoPanel') infoPanel: EngineConfigurationPanelComponent;

  constructor(private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private appEnum: AppEnumsComponent,
              private objectManager: AppObjectManagerService,
              private consoleUserApi: ConsoleUserApi,
              private grpc: GrpcApi,
              private engineApi: EngineApi) {
  }

  ngOnInit() {
    let id = 'engine';
    let roles = this.consoleUserApi.getCachedCurrent().roles;

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.CREATE, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.UPDATE, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        hidden: AuthService.isNotAllowed(id, AuthService.ROLE.DELETE, roles)
      },
    };

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {
        attr: 'name',
        name: 'Name',
        isSort: true,
        searchable: true,
        onClick: this.navigateEngineDetailView
      },
      {attr: 'categories.length', name: '# Categories', isSort: true, searchable: true},
      {attr: 'bookmarkList.length', name: '# Bookmarks', isSort: true},
    ];

    this.tableComponent.checkable = !(this.actions.edit.hidden && this.actions.delete.hidden && this.actions.test.hidden);
    this.tableComponent.onRowClick = this.fillRowInfo;
    // this.tableComponent.onCheckChanged = this.whenCheckChanged;

    // subscribe store
    this.subscription.add(
      this.store.select('engines')
      .subscribe(engines => {
        if (engines) {
          this.rows = engines;
          this.rows.forEach(row => row.isChecked = false);
          this.dataSource = new MatTableDataSource(this.rows);
          this.tableComponent.isCheckedAll = false;
          this.whenCheckChanged();
          setTimeout(() => {
            this.fillRowInfo(this.rows[0]);
          }, this.rows.length > 0 ? 500 : 0);
        }
      })
    );

    this.engineApi.find().subscribe(
      result => {
        this.engines = result;
        this.store.dispatch({type: ENGINE_UPDATE_ALL, payload: this.engines});
        this.store.dispatch({type: ROUTER_LOADED});
        this.engines.sort((a, b) => a.name > b.name);
      }, error => {
        let message = `Something went wrong. [${getErrorString(error)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      }
    );

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    // this.router.navigateByUrl('m2u/management/engine-upsert#add');
    this.router.navigateByUrl('m2u/management/engine-configuration/new');
  };

  edit: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    delete items[0].checked;
    this.objectManager.set('engine', items[0]);
    // this.router.navigateByUrl('m2u/management/engine-upsert#edit');
    this.router.navigateByUrl('m2u/management/engine-configuration/' + items[0].name + '/edit');
  };

  delete: any = () => {
    let names = this.rows.filter(row => row.isChecked).map(row => row.name);
    let keyList = this.rows.filter(row => row.isChecked).map(row => {
      return {id: row.id, name: row.name};
    });
    if (names.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${names}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      let deletedNames = '';
      keyList.forEach(key => {
        this.engineApi.deleteById(key.id).subscribe(
          result => {
            if (deletedNames !== '') {
              deletedNames += ',';
            }
            deletedNames += key.name;
            this.store.dispatch({type: ENGINE_DELETE, payload: key.id});
            if (names.toString() === deletedNames) {
              this.snackBar.open(`Engine '${deletedNames}' Deleted.`, 'Confirm', {duration: 3000});
            }
          },
          error => {
            let message = `Failed to Delete Engine Configuration. [${getErrorString(error)}]`;
            this.snackBar.open(message, 'Confirm', {duration: 10000});
          }
        );
      });
    });
  };

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  };

  fillRowInfo: any = (row: any) => {
    this.infoPanel.engineInfo = row;
    if (row === undefined) {
      this.sidenav.close();
      this.panelToggleText = 'Show Info Panel';
    } else {
      this.sidenav.open();
      this.panelToggleText = 'Hide Info Panel';
    }
  };

  whenCheckChanged: any = () => {
    switch (this.rows.filter(row => row.isChecked).length) {
      case 0:
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.delete.disabled = false;
        this.actions.edit.disabled = true;
        break;
    }
  };

  // DetailView 보류
  navigateEngineDetailView: any = (row: any) => {
    // this.objectManager.set('engine', row);
    // this.router.navigateByUrl('engine-configuration/engine-detail');
  };

  getImage: any = (row: any) => {
    if (row.lang === 'kor') {
      return '/assets/img/korean_05.png';
    } else if (row.lang === 'eng') {
      return '/assets/img/english_05.png';
    }
  }
}
