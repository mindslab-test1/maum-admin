import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  MatDialog, MatSnackBar,
  ErrorStateMatcher, ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {ROUTER_LOADED} from '../../../../core/actions';
import {
  AppObjectManagerService,
  AlertComponent,
  AppEnumsComponent,
  FormErrorStateMatcher,
  getErrorString,
  ErrorDialogComponent
} from '../../../../shared';
import {GrpcApi} from '../../../../core/sdk';
import {EngineApi} from '../../../../core/sdk/services/custom';
import {ConfirmComponent} from 'app/shared';

@Component({
  selector: 'app-engine-configuration-upsert',
  templateUrl: './engine-configuration-upsert.component.html',
  styleUrls: ['./engine-configuration-upsert.component.scss']
})
export class EngineConfigurationUpsertComponent implements OnInit {
  dialogTitle: string;
  role: any;

  engine = {
    name: undefined,
    categories: [],
    bookmarkList: []
  };
  engineList = [];
  org_engine: any;

  submitButton: any;

  selectedCategory: any;
  newCategory: any;
  newKeyword: any;
  newDisplayName: any;

  selectedBookmark: any;
  bookmarkOptions = new FormControl();

  engs: any;

  nameFormControl = new FormControl(undefined, [
    Validators.required,
    () => !!this.engine.name &&
    this.engineList.find(engine =>
      engine.toLowerCase() === this.engine.name.toLowerCase())
      ? {'duplicated': true} : null
  ]);

  keywordFormControl = new FormControl(undefined, [
    Validators.required,
    () => !!this.newKeyword &&
    this.engine.categories.find(category =>
      category.keyword.toLowerCase() === this.newKeyword.toLowerCase())
      ? {'duplicated': true} : null
  ]);

  displayNameFormControl = new FormControl('', [
    Validators.required,
    () => !!this.newDisplayName &&
    this.engine.categories.find(category =>
      category.displayName.toLowerCase() === this.newDisplayName.toLowerCase())
      ? {'duplicated': true} : null
  ]);

  bookmarkFormControl = new FormControl(undefined, [
    Validators.required,
    () => !!this.bookmarkOptions.value &&
    this.engine.bookmarkList.find(bookmark =>
      bookmark.toLowerCase() === this.bookmarkOptions.value.toLowerCase())
      ? {'duplicated': true} : null
  ]);

  constructor(private store: Store<any>,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              public appEnum: AppEnumsComponent,
              public formErrorStateMatcher: FormErrorStateMatcher,
              private objectManager: AppObjectManagerService,
              private grpc: GrpcApi,
              private engineApi: EngineApi,
              private cdrf: ChangeDetectorRef) {
  }

  ngOnInit() {
    // this.route.fragment.subscribe(
    //   role => {
    //     this.role = role;
    //     switch (this.role) {
    //       case 'add':
    //         this.submitButton = true;
    //         this.dialogTitle = 'Add Engine Configuration';
    //         break;
    //       case 'edit':
    //         let engine = this.objectManager.get('engine');
    //         if (engine === undefined) {
    //           this.backPage();
    //           return;
    //         }
    //         delete engine.checked;
    //         this.engine = engine;
    //         this.org_engine = JSON.parse(JSON.stringify(this.engine));
    //         this.dialogTitle = 'Edit Engine Configuration';
    //         break;
    //       default:
    //         this.backPage();
    //         // let message = 'Role is not properly assigned.';
    //         // this.snackBar.open(message, 'Confirm', {duration: 10000});
    //         return;
    //     }
    //     this.store.dispatch({type: ROUTER_LOADED});
    //   },
    //   err => {
    //     let ref = this.dialog.open(ErrorDialogComponent);
    //     ref.componentInstance.title = 'Failed';
    //     ref.componentInstance.message = `Something went wrong. [${getErrorString(err)}]`;
    //     this.store.dispatch({type: ROUTER_LOADED});
    //   });

    let tempRoute = this.route.toString();
    if (tempRoute.indexOf('engine-configuration/new') > 0) {
      this.role = 'add';
      this.submitButton = true;
      this.dialogTitle = 'Add Engine Configuration';
    } else if (tempRoute.indexOf('/edit') > 0) {
      let obm = this.objectManager.get('engine');
      new Promise((resolve, reject) => {
        if (obm && obm.name) {
          resolve();
        } else {
          let engineName;
          this.route.params.subscribe(par => {
            engineName = par['id'];
          });
          this.engineApi.find().subscribe(result => {
            this.engs = result;
            let res = this.engs.filter(elem => elem.name === engineName);
            if (res.length > 0) {
              obm = res[0];
              resolve();
            } else {
              reject();
            }
          });
        }
      }).then(() => {
        delete obm.checked;
        this.engine = obm;
        this.org_engine = JSON.parse(JSON.stringify(this.engine));
        this.dialogTitle = 'Edit Engine Configuration';
        this.cdrf.detectChanges();
        this.store.dispatch({type: ROUTER_LOADED});
      }).catch(() => {
        this.snackBar.open(`Cannot find Engine Name.`, 'Confirm', {duration: 3000});
        this.backPage();
        this.store.dispatch({type: ROUTER_LOADED});
        return;
      })
    } else {
      this.snackBar.open(`Cannot find this url.`, 'Confirm', {duration: 3000});
      this.backPage();
      this.store.dispatch({type: ROUTER_LOADED});
      return;
    }

    this.engineApi.find({}).subscribe(
      result => {
        this.engineList = [];
        result.map(engine => {
          this.engineList.push(engine['name']);
        });

      }, err => {
        let message = `Something went wrong. [${getErrorString(err)}]`;
        this.snackBar.open(message, 'Confirm', {duration: 10000});
      });
  }

  isFormError(control: FormControl): boolean {
    return this.formErrorStateMatcher.isErrorState(control, null);
  }

  submit() {
    if (this.check()) {
      if (this.role === 'edit') {
        if (!this.isModified()) {
          let ref = this.dialog.open(ErrorDialogComponent);
          ref.componentInstance.title = 'Not Changed.';
          ref.componentInstance.message = `Values are not changed. Please make some modification(s) first.`;
        } else {
          this.openDialog('edit');
        }
      } else {
        this.openDialog('add');
      }
    }
  }

  isModified() {
    this.engine.categories.sort((a, b) => a.displayName > b.displayName ? 1 : 0);
    this.engine.bookmarkList.sort((a, b) => a > b ? 1 : 0);
    this.org_engine.categories.sort((a, b) => a.displayName > b.displayName);
    this.org_engine.bookmarkList.sort((a, b) => a > b ? 1 : 0);
    return JSON.stringify(this.engine) !== JSON.stringify(this.org_engine);
  }

  onAdd(): void {
    this.engineApi.create(this.engine).subscribe(
      result => {
        this.snackBar.open(`Engine Configuration '${this.engine.name}' created.`,
          'Confirm', {duration: 3000});
        this.backPage();
      },
      error => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed.';
        ref.componentInstance.message = `Failed to create a Engine Configuration. [${getErrorString(error)})]`;
      }
    );
  }

  onEdit(): void {
    this.engineApi.updateAll({'name': this.engine.name}, this.engine).subscribe(
      result => {
        this.snackBar.open(`Engine Configuration '${this.engine.name}' updated.`,
          'Confirm', {duration: 3000});
        this.backPage();
      }, error => {
        let ref = this.dialog.open(ErrorDialogComponent);
        ref.componentInstance.title = 'Failed.';
        ref.componentInstance.message = `Failed to update the Engine Configuration. [${getErrorString(error)})]`;
      }
    );
  }

  addCategory() {
    if (this.isFormError(this.keywordFormControl) || this.newKeyword === undefined || this.newKeyword === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the Keyword.`;
    } else if (this.isFormError(this.displayNameFormControl) || this.newDisplayName === undefined || this.newDisplayName === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the DisplayName.`;
    } else if (this.engine.categories
    .find(item => this.newKeyword.toLowerCase() === item.keyword.toLowerCase())) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Duplicated';
      ref.componentInstance.message = `Duplicate are keyword.`;
    } else if (this.engine.categories
    .find(item => this.newDisplayName.toLowerCase() === item.displayName.toLowerCase())) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Duplicate are display name.`;
    } else {
      this.newCategory = {
        'keyword': this.newKeyword,
        'displayName': this.newDisplayName
      };
      this.engine.categories.push(this.newCategory);
      this.newKeyword = '';
      this.newDisplayName = '';
      this.newCategory = '';
      this.check();
    }
  }

  addBookmark() {
    if (this.isFormError(this.bookmarkFormControl) || this.bookmarkOptions.value === undefined || this.bookmarkOptions.value === '') {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Check';
      ref.componentInstance.message = `Please check the Bookmark.`;
    } else if (this.engine.bookmarkList.find(item => this.bookmarkOptions.value.toString() === item)) {
      let ref = this.dialog.open(ErrorDialogComponent);
      ref.componentInstance.title = 'Duplicated';
      ref.componentInstance.message = `Duplicate are Bookmark.`;
    } else {
      this.engine.bookmarkList.push(this.bookmarkOptions.value.toString());
      this.bookmarkOptions.setValue('');
      this.check();
    }
  }

  inputAddCategory(event) {
    if (event.keyCode === 13) { // enter
      this.addCategory();
      event.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
    }
  }

  deleteCategory(index) {
    let bookmark = '';
    let checkBookmark = true;
    for (bookmark of this.engine.bookmarkList) {
      let categoryArr = bookmark.split(',');
      if (categoryArr.indexOf(this.engine.categories[index].displayName) !== -1) {
        let ref_err = this.dialog.open(ErrorDialogComponent);
        ref_err.componentInstance.title = 'Check';
        ref_err.componentInstance.message = 'Please Check the bookmark list';
        checkBookmark = false;
        break;
      }
    }
    if (checkBookmark) {
      const ref = this.dialog.open(ConfirmComponent);
      ref.componentInstance.message = `Delete ${this.engine.categories[index].displayName} ?`;
      ref.afterClosed().subscribe(result => {
        if (result) {
          this.engine.categories.splice(index, 1);
          this.selectedCategory = undefined;
          this.check();
        }
      });
    }
  }

  deleteBookmark(index) {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete ${this.engine.bookmarkList[index]} ?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.engine.bookmarkList.splice(index, 1);
        this.selectedBookmark = undefined;
        this.check();
      }
    });
  }

  inputAddDisplayName(event) {
    if (event.keyCode === 13) {
      this.addCategory();
      event.preventDefault();
    }
  }

  getButtonName() {
    return this.role === 'add' ? 'Add' : 'Save';
  }

  check() {
    if (this.isFormError(this.nameFormControl) || this.engine.name === undefined) {
      this.submitButton = true;
    } else if (this.engine.categories.length === 0) {
      this.submitButton = true;
    } else {
      this.submitButton = false;
      return true;
    }
  }

  openDialog(data) {
    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `${this.role === 'add' ? 'Add' : 'Edit'} '${this.engine.name}'?`;
    ref.afterClosed().subscribe(result => {
      if (result) {
        if (data === 'add') {
          this.onAdd();
        } else {
          this.onEdit();
        }
      }
    });
  }

  backPage: any = () => {
    this.objectManager.clean('engine');
    this.router.navigateByUrl('/m2u/management/engine-configuration', {relativeTo: this.route});
  }

}
