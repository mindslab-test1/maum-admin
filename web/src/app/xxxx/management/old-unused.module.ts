import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EngineConfigurationViewComponent} from './components/engine-configuration-view/engine-configuration-view.component';
import {EngineConfigurationUpsertComponent} from './components/engine-configuration-upsert/engine-configuration-upsert.component';
import {EngineConfigurationPanelComponent} from './components/engine-configuration-panel/engine-configuration-panel.component';
import {OldUnusedRoutingModule} from './old-unused-routing.module';
import {
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    EngineConfigurationViewComponent,
    EngineConfigurationUpsertComponent,
    EngineConfigurationPanelComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatInputModule,
    SharedModule,
    MatSidenavModule,
    MatSelectModule,
    MatButtonModule,
    RouterModule,
    OldUnusedRoutingModule
  ]
})
export class OldUnusedModule {
}
