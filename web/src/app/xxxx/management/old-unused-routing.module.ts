import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EngineConfigurationPanelComponent} from './components/engine-configuration-panel/engine-configuration-panel.component';
import {EngineConfigurationUpsertComponent} from './components/engine-configuration-upsert/engine-configuration-upsert.component';
import {EngineConfigurationViewComponent} from './components/engine-configuration-view/engine-configuration-view.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: EngineConfigurationViewComponent,
      },
      {
        path: ':id/edit',
        component: EngineConfigurationUpsertComponent,
      },
      {
        path: ':id',
        component: EngineConfigurationPanelComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OldUnusedRoutingModule {
}
