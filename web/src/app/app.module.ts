import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {MomentModule} from 'angular2-moment';
import {NgIdleModule} from '@ng-idle/core';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DragScrollModule} from '../../node_modules/ngx-drag-scroll/lib/ngx-drag-scroll.module';
import {AppComponent} from 'app/app.component';
import {AuthSignUpComponent} from 'app/auth/components/auth-signup/auth.signup.component';
import {AuthSignInComponent} from 'app/auth/components/auth-signin/auth.signin.component';
import {AuthComponent} from 'app/auth/components/auth/auth.component';
import {CategoryEntryComponent} from 'app/category-entry.component';
import {AppPageNotFoundComponent} from 'app/app.page-not-found.component';
import {AppUnderConstructionComponent} from 'app/app.under-construction.component';
import {AppPermissionDeniedComponent} from 'app/app.permission-denied.component';
import {AppDummyComponent} from 'app/app.dummy.component';
import {AppSidebarComponent} from 'app/app.sidebar.component';
import {AdminRoute} from 'app/app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {COMPOSITION_BUFFER_MODE} from '@angular/forms';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatBadgeModule,
  MatInputModule,
  MatSelectModule,
  MatProgressBarModule,
  MatPaginatorModule,
  MatSlideToggleModule
} from '@angular/material';
import {AgGridModule} from 'ag-grid-angular';
import {FormsModule} from '@angular/forms';
import {GroupsService} from './admin/services/groups/groups.service';

@NgModule({
  declarations: [
    AppComponent,
    AppSidebarComponent,
    AppDummyComponent,
    AppPermissionDeniedComponent,
    AppUnderConstructionComponent,
    AppPageNotFoundComponent,
    CategoryEntryComponent,
    AuthComponent,
    AuthSignInComponent,
    AuthSignUpComponent
  ],
  bootstrap: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
    MomentModule,
    DragScrollModule,
    HttpModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    NgIdleModule.forRoot(),
    AdminRoute,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    FormsModule
  ],
  providers: [
    GroupsService,
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ]
})
export class AppModule {
}
