import {AuthGuard} from './core/auth.guard';
import {AuthService} from './core/auth.service';
import {AppPageNotFoundComponent} from 'app/app.page-not-found.component';
import {AuthSignUpComponent} from 'app/auth/components/auth-signup/auth.signup.component';
import {AuthSignInComponent} from 'app/auth/components/auth-signin/auth.signin.component';
import {AuthComponent} from 'app/auth/components/auth/auth.component';
import {AppPermissionDeniedComponent} from 'app/app.permission-denied.component';
import {AppDummyComponent} from 'app/app.dummy.component';
import {NoPreloading, PreloadAllModules, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

const AppRoutingModule = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/auth/signin',
  },
  {
    path: 'dummy',
    component: AppDummyComponent,
    data: {
      hasNoMenuBar: true
    }
  },
  {
    path: 'denied',
    component: AppPermissionDeniedComponent,
    data: {
      hasNoMenuBar: true
    }
  },
  {
    path: 'auth',
    component: AuthComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      ACL: {
        roles: [
          AuthService.ROLE.UNAUTHENTICATED
        ]
      },
      hasNoLayout: true,
      hasNoMenuBar: true,
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'signin',
      },
      {
        path: 'signin',
        data: {
          hasNoLayout: true,  // signin에는 sidebar가 필요 없습니다
          hasNoSidebar: true
        },
        component: AuthSignInComponent,
      },
      {
        path: 'signup',
        data: {
          hasNoLayout: true
        },
        component: AuthSignUpComponent,
      },
    ],
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    // component: DashboardMltComponent,
    // canActivate: [AuthGuard],
    // canActivateChild: [AuthGuard],
    data: {
      hasNoSidebar: true,
      ACL: {
        roles: ['dashboard' + AuthService.ROLE.READ]
      },
      fallback: ['denied'],
      nav: {
        name: 'Dashboard',
        comment: '전체적인 상태를 살펴봅니다.',
        icon: 'dashboard',
      },
    },
  },
  {
    path: 'm2u-builder',
    children: [
      {
        // path: ':wsid/chatbots',
        path: 'chatbots',
        loadChildren: './modules/chatbot/chatbot.module#ChatbotModule'
      },
      {
        // path: ':wsid/dialog-models',
        path: 'dialog-svcs',
        loadChildren: './modules/dialog-agent-instance/dialog-agent-instance.module#DialogAgentInstanceModule'
      },
      /*{
        // path: ':wsid/dialog-designs',
        path: 'dialog-agent-instances',
        loadChildren: './modules/dialog-agent-instance/dialog-agent-instance.module#DialogAgentInstanceModule'
      },
      {
        // path: ':wsid/dialog-designs',
        path: 'intent-finder-instances',
        loadChildren: './modules/intent-finder-instance/intent-finder-instance.module#IntentFinderInstanceModule'
      },*/
      {
        // path: ':wsid/dialog-designs',
        path: 'dialog-agents',
        loadChildren: './modules/dialog-agent/dialog-agent.module#DialogAgentModule'
      },
      {
        // path: ':wsid/simple-classifiers',
        path: 'intent-finder',
        loadChildren: './modules/intent-finder/intent-finder.module#IntentFinderModule'
      },
      {
        // path: ':wsid/hmd-models',
        path: 'intent-finder-instance',
        loadChildren: './modules/intent-finder-instance/intent-finder-instance.module#IntentFinderInstanceModule'
      },
      {
        // path: ':wsid/dnncl-models',
        path: 'dnncl-models',
        loadChildren: './modules/dnn-cl-model/dnn-cl-model.module#DnnClModelModule'
      },
      {
        // path: ':wsid/hmd-models',
        path: 'hmd-models',
        loadChildren: './modules/hmd-model/hmd-model.module#HmdModelModule'
      },
      {
        // path: ':wsid/xdccl-models',
        path: 'xdc-models',
        loadChildren: './modules/xdc-model/xdc-model.module#XdcModelModule'
      },
      {
        // path: ':wsid/simple-classifiers',
        path: 'simple-classifiers',
        loadChildren: './modules/simple-classifier/simple-classifier.module#SimpleClassifierModule'
      },
      {
        path: 'nlp-test',
        loadChildren: './modules/nlp-test/nlp-test.module#NlpTestModule'
      },
      {
        // path: ':wsid/dialog-models',
        path: 'dialog-models',
        loadChildren: './modules/dialog-model/dialog-model.module#DialogModelModule'
      },
      {
        // path: ':wsid/dialog-designs',
        path: 'dialog-designs',
        loadChildren: './modules/dialog-model-design/dialog-model-design.module#DialogModelDesignModule'
      },
      {
        // path: ':wsid/nqa/categories',
        path: 'nqa/categories',
        loadChildren:
          './modules/qa-natural-questions/qa-natural-questions.module#QaNaturalQuestionsModule'
      },
      {
        // path: ':wsid/bqa/categories',
        path: 'bqa/categories',
        loadChildren: './modules/qa-basic/qa-basic.module#QaBasicModule'
      },
      {
        // path: ':wsid/nlp3/morps',
        path: 'dict-nlp3/morps',
        loadChildren: './modules/dict-nlp3-pos/dict-nlp3-pos.module#DictNlp3PosModule'
      },
      {
        // path: ':wsid/dict-nlp3/ner',
        path: 'dict-nlp3/ner',
        loadChildren: './modules/dict-nlp3-ner/dict-nlp3-ner.module#DictNlp3NerModule'
      },
      {
        // path: ':wsid/dict-nlp3/replacement',
        path: 'dict-nlp3/replacement',
        loadChildren: './modules/dict-nlp3-replacement/dict-nlp3-replacement.module#DictNlp3ReplacementModule'
      },
      {
        // path: ':wsid/dict-nlp3/ner',
        path: 'dict-nlp3/test-result',
        loadChildren: './modules/dict-test-result-detail/dict-test-result-detail.module#DictTestResultDetailModule'
      },
      {
        // path: ':wsid/chatbots/log',
        path: 'chatbot-log',
        loadChildren: './modules/chatbot-log/chatbot-log.module#ChatbotLogModule'
      },
      {
        // path: ':wsid/chatbots/stat',
        path: 'chatbot-stat',
        loadChildren: './modules/chatbot-statistics/chatbot-statistics.module#ChatbotStatisticsModule'
      },

      {
        // path: ':wsid/intent-finders',
        path: 'simple-classifier',
        loadChildren: './modules/simple-classifier/simple-classifier.module#SimpleClassifierModule'
      },
      {
        // path: ':wsid/bqa/categories',
        path: 'chatbot',
        loadChildren: './modules/chatbot/chatbot.module#ChatbotModule'
      },
      {
        // path: ':wsid/nqa/categories',
        path: 'chatbot-log',
        loadChildren: './modules/chatbot-log/chatbot-log.module#ChatbotLogModule'
      },
      {
        // path: ':wsid/dict-nlp3/morps',
        path: 'process-monitor',
        loadChildren: './modules/process-monitor/process-monitor.module#ProcessMonitorModule'
      },
      {
        // path: ':wsid/dict-nlp3/ner',
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule'
      },
      {
        // path: ':wsid/dict-nlp3/ner',
        path: 'management',
        loadChildren: './xxxx/management/old-unused.module#OldUnusedModule'
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      },
      {
        // path: ':wsid/history'
        path: 'histories',
        loadChildren: './modules/version/version.module#VersionModule'
      },
      {
        // path: ':wsid/yy'
        path: 'chatbot-statistics',
        loadChildren: './modules/chatbot-statistics/chatbot-statistics.module#ChatbotStatisticsModule'
      },
      {
        path: 'stt',
        loadChildren: './modules/stt/stt.module#SttModule'
      },
      {
        path: 'paraphrase',
        loadChildren: './modules/paraphrase/paraphrase.module#ParaphraseModule'
      },
      {
        path: 'mrc',
        loadChildren: './modules/mrc-model/mrc-model.module#MrcModelModule'
      },
    ]
  },
  {
    path: '**',
    component: AppPageNotFoundComponent,
    data: {
      hasNoLayout: true,
      hasNoSidebar: true
    },
  },
];

// export const AdminRoute: ModuleWithProviders = RouterModule.forRoot(AppRoutingModule);
export const AdminRoute: ModuleWithProviders = RouterModule.forRoot(
  AppRoutingModule,
  {
    preloadingStrategy: NoPreloading,
    enableTracing: false,
    onSameUrlNavigation: 'reload'
  }
);
export const AppNavs = getNavs(AppRoutingModule);

// routes로 부터 네비게이션 추출
export function getNavs(routes: any[], navs: any[] = [], paths: string[] = []): any[] {
  if (!routes) {
    return navs;
  }

  let childNavs = routes

  // chlidren도 없고 nav도 아닌 abstract route를 필터링
  .filter(route => {
    return (route.data && route.data.nav) || route.children;
  })

  // 재귀적으로 nav 생성
  .map(route => {
    let childPaths = paths.concat([route.path]).filter(p => p !== '');

    // nav가 아님 (abstract route)
    if (!route.data || !route.data.nav) {
      // console.log(route.children);
      return getNavs(route.children, [], childPaths);
    }

    // nav인 경우
    let childNav: any = Object.assign({}, {
      paths: childPaths,
      active: false,
      open: false,
      ACL: route.data.ACL,
    }, route.data.nav);

    if (!childNav.ACL) {
      delete childNav.ACL;
    }

    if (route.children) {
      let childNavChildren = getNavs(route.children, [], childPaths);
      if (childNavChildren.length !== 0) {
        childNav.children = childNavChildren;
      }
    }

    return [childNav];
  })

  // flatten
  .reduce((arr, arr2) => arr.concat(arr2), []);

  return navs.concat(childNavs);
}
