import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {UsersComponent} from './components/users/users.component';
import {UserUpsertComponent} from './components/user-upsert/user-upsert.component';
import {UserUpsertTableComponent} from './components/user-upsert-table/user-upsert-table.component';
import {GroupsComponent} from './components/groups/groups.component';
import {PermissionComponent} from './components/permission/permission.component';
import {WorkspacesComponent} from './components/workspaces/workspaces.component';
import {SharedModule} from '../shared/shared.module';
import {
  ErrorStateMatcher,
  MatButtonModule, MatCardModule,
  MatCheckboxModule, MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule, MatTooltipModule, ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {GroupsService} from './services/groups/groups.service';
import {GroupUpsertComponent} from './components/group-upsert/group-upsert.component';
import {UsersService} from './services/users/users.service';
import {SystemAdminComponent} from "./components/system/system-admin-component";
import {SystemService} from "./services/system/system.service";
import {DownloadService} from "../shared";

@NgModule({
  declarations: [
    UsersComponent,
    GroupsComponent,
    GroupUpsertComponent,
    PermissionComponent,
    WorkspacesComponent,
    UserUpsertComponent,
    UserUpsertTableComponent,
    GroupUpsertComponent,
    SystemAdminComponent
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    UsersService, GroupsService, SystemService, DownloadService
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatButtonModule,
    SharedModule,
    MatIconModule,
    MatTooltipModule,
    MatCardModule,
  ]
})
export class AdminModule {
}
