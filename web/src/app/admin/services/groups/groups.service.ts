import {Component, Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';
import {GroupsEntity} from '../../entity/groups/groups.entity';

@Injectable()
export class GroupsService {
  API_URL;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
  }

  findGroups(param: GroupsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/group/findGroupList', param);
  }

  getGroup(param: GroupsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/group/getGroup', param);
  }

  addGroup(param: GroupsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/group/addGroup', param);
  }

  editGroup(param: GroupsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/group/editGroup', param);
  }

  deleteGroups(param: GroupsEntity): Observable<any> {
    return this.http.post(this.API_URL + '/group/deleteGroups', param);
  }

  getWorkspaces(id: String): Observable<any> {
    return this.http.post(this.API_URL + '/workspace/getWorkspaces', id);
  }


}
