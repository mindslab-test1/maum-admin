import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

interface DictionaryData {
  type: number,
  name: string
}

@Injectable()
export class SystemService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  downloadHzcBackup(): Observable<any> {
    return this.http.post(this.API_URL + '/admin/backup/hzc/all', {});
  }

  uploadHzcInitData(param: any): Observable<any> {
    return this.http.post(this.API_URL + '/admin/init/hzc/all', param);
  }

  /*exportToken(token: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/deleteUser', token);
  }*/

  /*getAllWorkspace() {
    return this.http.get(this.API_URL + '/auth/get-all-workspace/');
  }*/

}
