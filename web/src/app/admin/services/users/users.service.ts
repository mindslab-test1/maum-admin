import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StorageBrowser} from 'app/shared/storage/storage.browser';
import {environment} from 'environments/environment';

interface DictionaryData {
  type: number,
  name: string
}

@Injectable()
export class UsersService {
  API_URL;
  ID;
  WORKSPACE_ID;

  constructor(private http: HttpClient,
              @Inject(StorageBrowser) protected storage: StorageBrowser) {
    this.API_URL = environment.maumAiApiUrl;
    this.ID = this.storage.get('user').id;
    this.WORKSPACE_ID = this.storage.get('m2uWorkspaceId');
  }

  exportToken(token: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/deleteUser', token);
  }

  findUserList(): Observable<any> {
    return this.http.post(this.API_URL + '/user/findUserList', null);
  }

  deleteUser(deleteRows: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/deleteUser', deleteRows);
  }

  addUser(Account: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/addUser', Account);
  }

  updateUser(Account: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/updateUser', Account);
  }

  getRoleList(): Observable<any> {
    return this.http.post(this.API_URL + '/user/findRoleList', null);
  }

  getUserRoles(AccountId: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/getUserRoles', AccountId);
  }

  getUserInfo(userName: any): Observable<any> {
    return this.http.post(this.API_URL + '/user/getUserInfo', userName);
  }

}
