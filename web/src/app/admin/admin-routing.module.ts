import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GroupsComponent} from './components/groups/groups.component';
import {GroupUpsertComponent} from './components/group-upsert/group-upsert.component';
import {WorkspacesComponent} from './components/workspaces/workspaces.component';
import {UsersComponent} from './components/users/users.component';
import {UserUpsertComponent} from './components/user-upsert/user-upsert.component';
import {PermissionComponent} from './components/permission/permission.component';
import {ChatbotUpsertComponent} from '../modules/chatbot/components/chatbot-upsert/chatbot-upsert.component';
import {SystemAdminComponent} from "./components/system/system-admin-component";

const routes: Routes = [
  {
    path: 'groups',
    component: GroupsComponent,
  },
  {
    path: 'group/new',
    component: GroupUpsertComponent,
  },
  {
    path: 'group/:id/edit',
    component: GroupUpsertComponent,
  },
  {
    path: 'workspaces',
    component: WorkspacesComponent,
  },
  {
    path: 'accounts',
    component: UsersComponent,
    // children: [
  },
  {
    path: 'account/new',
    component: UserUpsertComponent,
  },
  {
    path: 'account/:id/edit',
    component: UserUpsertComponent,
  },
  {
    path: 'permission',
    component: PermissionComponent
  },
  {
    path: 'system',
    component: SystemAdminComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
