import {map} from 'rxjs/operators';
import {Component, Input, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {
  MatDialog,
  MatSnackBar,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';
import {Store} from '@ngrx/store';

import {AuthService} from '../../../core';
import {ConsoleUserApi} from '../../../core/sdk'
import {ACNT_DELETE, ROUTER_LOADED} from '../../../core/actions';
import {
  AlertComponent,
  AppObjectManagerService, ConfirmComponent, FormErrorStateMatcher,
  getErrorString
} from '../../../shared';
import {EMAIL_REGEX, CHECK_PASS} from '../../../shared';
import {UsersService} from '../../services/users/users.service';
import {any} from 'codelyzer/util/function';

@Component({
  selector: 'app-accounts-upsert',
  templateUrl: './user-upsert.component.html',
  styleUrls: ['./user-upsert.component.scss'],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})

export class UserUpsertComponent implements OnInit {
  dialogTitle: string;
  table: any;
  role: string;
  account = {
    username: undefined,
    email: undefined,
    password: undefined,
    activated: undefined,
    roles: undefined
  };
  orgAccount: any;
  accountId: number;
  roleList: any;
  orgRoleList: any;
  isAdmin = false;
  // roles: any;
  confirmPassword: any;

  nameFormControl = new FormControl('', []);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(CHECK_PASS)
  ]);
  confirmPasswordFormControl = new FormControl('', [
    Validators.required,
    // () => this.getComparePassword() ? {'matched': true} : null
  ]);


  constructor(private usersService: UsersService,
              private store: Store<any>,
              private snackBar: MatSnackBar,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MatDialog,
              private authService: AuthService,
              private objectManager: AppObjectManagerService,
              public matcher: FormErrorStateMatcher,
              private consoleUserApi: ConsoleUserApi) {
  }

  ngOnInit() {
    // if (AuthService.isNotAllowed(AuthService.ROLE.ADMIN, this.consoleUserApi.getCachedCurrent().roles)) {
    //   this.authService.logout();
    //   return;
    // }

    // Add Accounts의 권한을 줄수 있는 표

    this.table = {
      header: [
        {attr: 'abbr', name: 'Name', width: 80},
        {attr: 'allowed.C', name: 'C', checkbox: true, width: 29},
        {attr: 'allowed.R', name: 'R', checkbox: true, width: 29},
        {attr: 'allowed.U', name: 'U', checkbox: true, width: 29},
        {attr: 'allowed.D', name: 'D', checkbox: true, width: 29},
        {attr: 'allowed.X', name: 'X', checkbox: true, width: 29},
      ],
      rows: [],
    };

    new Promise((resolve, reject) => {
      // #@ 1.route 확인
      let tempRoute = this.route.toString();
      if (tempRoute.indexOf('account/new') > 0) {
        console.log('account/new');
        this.dialogTitle = 'Add a Account';
        this.role = 'add';
        resolve();
      } else if (tempRoute.indexOf('/edit') > 0) {
        let obm = this.objectManager.get('account');
        if (obm === undefined) {
          this.route.params.subscribe(par => {
            this.account.username = par['id'];
          });
          this.getUserInfo().then(() => resolve()).catch(() => reject());
        } else {
          delete obm.checked;
          obm.password = '';
          this.account = obm;
          this.accountId = obm.id;
          this.orgAccount = JSON.stringify(this.account);
          this.account.username = obm.username;
          this.dialogTitle = 'Edit the Account: ' + this.account.username;
          this.role = 'edit';
          resolve();
        }
      }
    }).catch(() => {  // #@ 2.account name을 못 찾은 경우
      this.snackBar.open('Cannot Find Account Name', 'Confirm', {duration: 10000});
      this.backPage();
    })
    .then(() =>
        this.usersService.getRoleList().pipe(map(roleList => {
          let _roleList: any[] = [];
          roleList.forEach(role => {
            let entry = {id: role[0], abbr: role[2], tip: role[3], allowed: {}};
            role[1].split('').forEach(value => entry.allowed[value] = false);
            _roleList.push(entry);
          });
          this.roleList = _roleList;
        })).toPromise())
    .then(() => {
      if (this.role !== 'edit') {
        return new Promise<void>(resolve => resolve());
      }
      return this.usersService.getUserRoles(this.accountId).pipe(map(roles => {
        this.isAdmin = AuthService.isAllowed(AuthService.ROLE.ADMIN, roles);
        this.roleList.forEach(entry => {
          Object.keys(entry.allowed).forEach(key =>
              entry.allowed[key] = roles[`${entry.id}^${key}`] === true
          )
        });
        this.orgRoleList = JSON.stringify(this.roleList);
      })).toPromise<void>();
    })
    .then(() => {
      this.table.rows = this.roleList;
      console.log('#@ roleList :', this.roleList);
      this.store.dispatch({type: ROUTER_LOADED});
    })
    .catch(err => {
      if (!!err) {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      } else {
        this.backPage();
        // let message = 'Role is not properly assigned.';
        // this.snackBar.open(message, 'Confirm', {duration: 10000});
      }
    });
  }

  getComparePassword() {
    if (this.confirmPassword !== undefined && this.account.password !== this.confirmPassword) {
      // alert(this.account.password);
      // alert(this.confirmPassword);
      return true;
    }
  }


  isFormError(control: FormControl): boolean {
    return this.matcher.isErrorState(control, null);
  }

  submit() {
    console.log(this.roleList);

    if (this.role === 'edit') {
      if (this.isFormError(this.passwordFormControl)) {
        this.snackBar.open('Please check the password.', 'Confirm', {duration: 2000});
      } else if (this.isFormError(this.nameFormControl)) {
        this.snackBar.open('Please check the name.', null, {duration: 2000});
      } else if (this.isFormError(this.emailFormControl)) {
        this.snackBar.open('Please check the email.', null, {duration: 2000});
        // } else if (this.selectedPermissions.length === 0) {
        //   this.snackBar.open('Please add at least a permission.', null, {duration: 2000});
        // } else if (this.isFormError(this.confirmPasswordFormControl)) {
        //     this.snackBar.open('Please check the Confirm Password.', null, {duration: 2000});
      } else {
        if (this.account.email !== undefined && this.account.password !== '' && this.confirmPassword !== undefined) {
          const ref = this.dialog.open(ConfirmComponent);
          ref.componentInstance.message = `Save '${this.account.username}'?`;

          ref.afterClosed().subscribe(result => {
            if (result) {
              this.onEdit();
            }
          });
        } else {
          if (this.account.email === undefined) {
            this.snackBar.open('Please check the email.', null, {duration: 2000});
          } else if (this.account.password === '') {
            this.snackBar.open('Please check the password.', null, {duration: 2000});
          } else if (this.confirmPassword === undefined) {
            this.snackBar.open('Please check the Confirm Password.', null, {duration: 2000});
          }
        }
      }
    } else {
      if (this.isFormError(this.nameFormControl)) {
        this.snackBar.open('Please check the name.', null, {duration: 2000});
      } else if (this.isFormError(this.emailFormControl)) {
        this.snackBar.open('Please check the email.', null, {duration: 2000});
      } else if (this.isFormError(this.passwordFormControl)) {
        this.snackBar.open('Please check the password.', null, {duration: 2000});
        // } else if (this.selectedPermissions.length === 0) {
        //   this.snackBar.open('Please add at least a permission.', null, {duration: 2000});
      } else {
        if (this.account.username !== undefined && this.account.email !== undefined && this.account.password !== undefined) {
          const ref = this.dialog.open(ConfirmComponent);
          ref.componentInstance.message = `Add '${this.account.username}?'`;
          ref.afterClosed().subscribe(result => {
            if (result) {
              this.onAdd();
            }
          });
        } else {
          if (this.account.username === undefined) {
            this.snackBar.open('Please check the name.', null, {duration: 2000});
          } else if (this.account.email === undefined) {
            this.snackBar.open('Please check the email.', null, {duration: 2000});
          } else if (this.account.password === undefined) {
            this.snackBar.open('Please check the password.', null, {duration: 2000});
          }
        }
      }
    }
  }

  aggregateRoles() {
    let roles = {};
    if (this.isAdmin) {
      roles[AuthService.ROLE.ADMIN] = true;
      if (this.role === 'edit') {
        let orgRoles = JSON.parse(this.orgRoleList);
        orgRoles.forEach(entry => Object.keys(entry.allowed).forEach(key => {
              if (entry.allowed[key]) {
                roles[`${entry.id}^${key}`] = false;
              }
            }
        ))
      }
    } else {
      if (this.role === 'edit') {
        let orgRoles = JSON.parse(this.orgRoleList);
        roles['admin'] = false;
        this.roleList.forEach(entry =>
            Object.keys(entry.allowed).forEach(key => {
                  if (entry.allowed[key] !== orgRoles.find(role => role.id === entry.id).allowed[key]) {
                    roles[`${entry.id}^${key}`] = entry.allowed[key];
                  }
                }
            ))
      } else {
        this.roleList.forEach(entry =>
            Object.keys(entry.allowed).forEach(key => {
                  if (entry.allowed[key] === true) {
                    roles[`${entry.id}^${key}`] = true;
                  }
                }
            ))
      }
    }
    this.account.roles = roles;
  }

  onAdd(): void {
    this.aggregateRoles();
    this.account.activated = this.account.activated === true ? 1 : 0;
    this.usersService.addUser(this.account).subscribe(
        res => {
          if (res.message === 'INSERT_DUPLICATED') {
            this.openAlertDialog('Duplicate', 'Name duplicated.', 'Error');
          } else {
            this.snackBar.open(`Account '${this.account.username}' created.`, 'Confirm', {duration: 3000});
            this.backPage();
          }
        },
        err => {
          this.snackBar.open(getErrorString(err, 'Add Failed.'), 'Confirm', {duration: 10000});
        });
  }

  onEdit(): void {

    // let account = JSON.parse(JSON.stringify(this.account));
    if (this.account.password === '') {
      delete this.account.password;
    }
    this.aggregateRoles();
    if (this.confirmPassword !== undefined && this.account.password !== this.confirmPassword) {
      this.snackBar.open('Confirm Password is not matched.', null, {duration: 2000});
      return;
    }
    this.account.activated = this.account.activated === true ? 1 : 0;
    this.usersService.updateUser(this.account).subscribe(
        res => {
          this.snackBar.open(`Account '${this.account.username}' updated.`, 'Confirm', {duration: 3000});
          this.backPage();
        },
        err => {
          this.snackBar.open(getErrorString(err, `Failed to update Account '${this.account.username}'.`),
              'Confirm', {duration: 10000});
        });
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  backPage: any = () => {
    this.objectManager.clean('account');
    if (this.role === 'edit') {
      this.router.navigate(['../../../accounts'], {relativeTo: this.route});
    } else {
      this.router.navigate(['../../accounts'], {relativeTo: this.route});
    }
  }

  getUserInfo() {
    return new Promise((resolve, reject) => {
      this.usersService.getUserInfo(this.account.username).subscribe(res => {
        if (res) {
          let obm = res;
          this.objectManager.set('account', obm);
          if (this.account.username === '') {  // 조회 결과가 비정상인 경우
            reject();
          }
          delete obm.checked;
          obm.password = '';
          this.account = obm;
          this.accountId = obm.id;
          this.orgAccount = JSON.stringify(this.account);
          this.account.username = obm.username;
          this.dialogTitle = 'Edit the Account: ' + this.account.username;
          this.role = 'edit';
          resolve();
        } else {
          this.backPage();
          reject();
          return;
        }
      }, err => {
        let message = 'Fail to get sc info';
        this.snackBar.open(message, 'Confirm', {duration: 10000});
        reject();
      });
    });
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
