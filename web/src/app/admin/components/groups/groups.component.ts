import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../core';
import {MatDialog, MatTableDataSource, MatPaginator, MatSort, MatSnackBar} from '@angular/material';
import {AlertComponent, TableComponent} from '../../../shared';
import {GroupsService} from '../../services/groups/groups.service';
import {Store} from '@ngrx/store';
import {GroupsEntity} from '../../entity/groups/groups.entity';
import {ROUTER_LOADED} from 'app/core/actions';
import {Router} from '@angular/router';

@Component({
  selector: 'app-group',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit, AfterViewInit {
  @ViewChild('tableComponent') tableComponent: TableComponent;

  // auth
  workspaceId: string;

  // title bar
  title: any = 'Group Management';
  actions: any;

  // table
  dataSource: MatTableDataSource<any>;
  pageParam: MatPaginator;
  header;
  rows: any[] = [];
  totalCount: number;

  // service
  param: GroupsEntity = new GroupsEntity();

  constructor(private router: Router,
              private groupsService: GroupsService,
              private dialog: MatDialog,
              private store: Store<any>,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.workspaceId = localStorage.getItem('m2uWorkspaceId');

    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: false,
        hidden: false
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: false
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: false
      }
    }

    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {
        attr: 'name',
        name: 'Name',
        isSort: true,
        onClick: null,
        width: '20%'
      },
      {
        attr: 'description',
        name: 'Description',
        isSort: true,
        onClick: null,
        width: '70%'
      },
      {
        attr: 'ownerId',
        name: 'Owner Id',
        isSort: true,
        template: null,
        width: '10%'
      }
    ];
  }

  ngAfterViewInit() {
    this.pageParam.pageSize = 10;
    this.pageParam.pageIndex = 0;
    this.find();

    // router load 완료
    this.store.dispatch({type: ROUTER_LOADED});
  }

  find() {
    this.param = new GroupsEntity();
    this.param.name = '';
    this.param.pageIndex = this.pageParam.pageIndex;
    this.param.pageSize = this.pageParam.pageSize;

    this.groupsService.findGroups(this.param).subscribe(res => {
        if (res) {
          this.tableComponent.isCheckedAll = false;
          this.tableComponent = res['totalElements'];
          this.rows = res['content'];
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Get Group FileList', 'error');
        console.log('error', err);
      }
    );
  }

  add: any = () => {
    this.router.navigate(['m2u-builder', 'admin', 'group', 'new']);
  }

  delete: any = () => {
    return false;
  }

  edit: any = (id: number) => {
    let items = this.rows.filter(row => row.isChecked);

    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }

    this.router.navigate(['m2u-builder', 'admin', 'group', items[0].id, 'edit']);
  }

  whenCheckChanged() {
    let checkedRows = this.rows.filter(row => row.isChecked);

    if (checkedRows.length === 1) {
      this.actions.edit.disabled = false;
    }

    if (checkedRows.length > 1) {
      this.actions.edit.disabled = true;
      this.actions.delete.disabled = false;
    }
  }

  setPage(page: MatPaginator) {
    this.pageParam = page;
    if (this.rows.length > 0) {
      this.find();
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
