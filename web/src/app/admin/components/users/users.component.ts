import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog, MatSidenav, MatSnackBar} from '@angular/material';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {ACNT_DELETE, ACNT_UPDATE_ALL, ROUTER_LOADED} from '../../../core/actions';
import {AuthService} from '../../../core/auth.service';
import {AppObjectManagerService, getErrorString, TableComponent} from '../../../shared';
import {ConsoleUserApi} from '../../../core/sdk'


import {MatTableDataSource} from '@angular/material/table';
import {ConfirmComponent} from 'app/shared/components/dialog/confirm.component';
import {UsersService} from '../../services/users/users.service';
import {GridOptions} from 'ag-grid-community';

interface UserInfo {
  id: string;
  activated: string;
  email: number;
  password: string;
  userName: string;
  lastUpdated: string;
}

type user_list = UserInfo[];

@Component({
  selector: 'app-accounts-view',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  public gridOptions: GridOptions;
  page_title: any; // = 'page titles is here.';
  page_description: any; // = 'page description is here.';
  rowData: user_list = [];
  title: any = 'Accounts management';

  actions: any;
  filterKeyword: FormControl;
  panelToggleText: string;
  username: string;
  row: any;
  header;
  rows: any[] = [];
  dataSource: MatTableDataSource<any>;
  subscription = new Subscription();

  @ViewChild('tableComponent') tableComponent: TableComponent;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('accountsNameTemplate') accountsNameTemplate: TemplateRef<any>;

  constructor(private usersService: UsersService,
              private store: Store<any>,
              private router: Router,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private fb: FormBuilder,
              private authService: AuthService,
              private consoleUserApi: ConsoleUserApi,
              private activatedRoute: ActivatedRoute,
              private objectManager: AppObjectManagerService) {
  }

  ngOnInit() {
    let user = this.consoleUserApi.getCachedCurrent();
    console.log('#@ users :', user);

    this.username = user.username;
    let roles = user.roles;

    // ROLE이 ADMIN 아니면 logout 처리
    if (AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)) {
      this.authService.logout();
      return;
    }
    // 상단 Add, Delete, Edit 버튼 정의
    this.actions = {
      add: {
        icon: 'add_circle_outline',
        text: 'Add',
        callback: this.add,
        disabled: true,
        hidden: AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)
      },
      delete: {
        icon: 'delete',
        text: 'Delete',
        callback: this.delete,
        disabled: true,
        hidden: AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)
      },
      edit: {
        icon: 'edit',
        text: 'Edit',
        callback: this.edit,
        disabled: true,
        hidden: AuthService.isNotAllowed(AuthService.ROLE.ADMIN, roles)
      }
    };
    this.header = [
      {attr: 'checkbox', name: 'checkbox', checkbox: true},
      {
        attr: 'username',
        name: 'Name',
        isSort: true,
        searchable: true,
        template: this.accountsNameTemplate /*onClick: this.navigateAccountDetailView*/
      },
      {attr: 'email', name: 'Email', isSort: true, searchable: true},
      {attr: 'activated', name: 'Activated', asIcon: true, isSort: true},
    ];

    this.tableComponent.checkable = !(this.actions.delete.hidden && this.actions.edit.hidden);
    // this.tableComponent.onCheckChanged = this.whenCheckChanged;

    // 구독신청 이후 store.dispatch가 호출되면 값이 전달됨
    this.subscription.add(
      this.store.select('accounts')
      .subscribe(accounts => {
        console.log('#@ accounts :', accounts);
        if (accounts) {
          this.rows = accounts;
          this.rows.forEach(row => {
            row.checked = false;
            row.activated = (row.activated === '1' || row.activated === true) ? true : false;
          });
          this.dataSource = new MatTableDataSource(this.rows);
          this.tableComponent.isCheckedAll = false;
          this.whenCheckChanged();
          /*
                      setTimeout(() => {
                        this.fillRowInfo(this.rows[0]);
                      }, this.rows.length > 0 ? 500 : 0);
          */
        }
      })
    );

    // accounts 조회 이후 subscribe로 전달
    this.usersService.findUserList().subscribe(
      accounts => {
        this.store.dispatch({type: ACNT_UPDATE_ALL, payload: accounts});
        this.store.dispatch({type: ROUTER_LOADED});
      },
      err => {
        this.snackBar.open(getErrorString(err, 'Something went wrong.'), 'Confirm', {duration: 10000});
        this.store.dispatch({type: ROUTER_LOADED});
      });

    // search
    this.filterKeyword = this.fb.control('');
    this.subscription.add(
      this.filterKeyword.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),)
      .subscribe(keyword => this.tableComponent.applyFilter(keyword))
    );
  }

  /** action callbacks **/
  add: any = () => {
    this.objectManager.clean('account');
    // this.router.navigateByUrl('m2u/management/accounts-upsert?role=add');
    this.router.navigate(['m2u-builder', 'admin', 'account', 'new']);
  };

  edit: any = () => {
    let items = this.rows.filter(row => row.isChecked);
    if (items.length === 0) {
      this.snackBar.open('Select items to Edit.', 'Confirm', {duration: 3000});
      return;
    } else if (items.length >= 2) {
      this.snackBar.open('Please select one.', 'Confirm', {duration: 3000});
      return;
    }
    this.objectManager.set('account', items[0]);
    // this.router.navigateByUrl('m2u/management/accounts-upsert?role=edit');
    this.router.navigate(['m2u-builder', 'admin', 'account', items[0].username, 'edit']);
  };

  delete: any = () => {
    let deleteRows = this.rows.filter(row => row.isChecked);
    if (deleteRows.length === 0) {
      this.snackBar.open('Select items to delete.', 'Confirm', {duration: 3000});
      return;
    }
    if (deleteRows[0].username === this.username) {
      this.snackBar.open(`You shouldn't delete your own account.`, 'Confirm', {duration: 3000});
      return;
    }
    console.log(deleteRows[0]);

    const ref = this.dialog.open(ConfirmComponent);
    ref.componentInstance.message = `Delete '${deleteRows[0].username}?`;
    ref.afterClosed()
    .subscribe(confirmed => {
      if (!confirmed) {
        return;
      }
      this.usersService.deleteUser(deleteRows).subscribe(
        res => {
          // if (res.count === 1) {
          this.store.dispatch({type: ACNT_DELETE, payload: [deleteRows[0].id]});
          this.snackBar.open(`Dialog Agent Manager '${deleteRows[0].username}' Deleted.`, 'Confirm', {duration: 3000});
          // }
        },
        err => {
          let message = `Failed to Delete Dialog Agent Manager '${deleteRows[0].username}'. [${getErrorString(err)}]`;
          this.snackBar.open(message, 'Confirm', {duration: 10000});
        }
      );
    });
  };

  togglePanel: any = () => {
    this.panelToggleText = this.sidenav.opened ? 'Show Info Panel' : 'Hide Info Panel';
    this.sidenav.toggle();
  };


      // fillRowInfo: any = (row: any) => {
      //   this.infoPanel.setContent(row);
      //   if (row === undefined) {
      //     this.sidenav.close();
      //     this.panelToggleText = 'Show Info Panel';
      //   } else {
      //     this.sidenav.open();
      //     this.panelToggleText = 'Hide Info Panel';
      //   }
      // };

  fillRowInfo: any = (row: any) => {
    this.objectManager.set('account', row);
    // this.router.navigateByUrl('m2u/management/accounts-upsert?role=edit');
    this.router.navigate(['m2u-builder', 'admin', 'account', row.username, 'edit']);
  };


  whenCheckChanged: any = () => {
    let checkedRows = this.rows.filter(row => row.isChecked);
    switch (checkedRows.length) {
      case 0:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
      case 1:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = checkedRows[0].username === this.username;
        this.actions.edit.disabled = false;
        break;
      default:
        this.actions.add.disabled = false;
        this.actions.delete.disabled = true;
        this.actions.edit.disabled = true;
        break;
    }
  };

  // navigateAccountDetailView: any = (row: any) => {
  //   this.objectManager.set('account', row);
  //   this.router.navigateByUrl('accounts/detail');
  // }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
