import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupsService} from '../../services/groups/groups.service';
import {ErrorStateMatcher, MatDialog, MatSnackBar, ShowOnDirtyErrorStateMatcher} from '@angular/material';
import {Store} from '@ngrx/store';
import {ROUTER_LOADED} from '../../../core/actions';
import {FormControl, Validators} from '@angular/forms';
import {
  AlertComponent,
  AppObjectManagerService, ConfirmComponent, FormErrorStateMatcher,
  getErrorString
} from '../../../shared';
import {GroupsEntity} from '../../entity/groups/groups.entity';

@Component({
  selector: 'app-group-upsert',
  templateUrl: './group-upsert.component.html',
  styleUrls: ['./group-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})
export class GroupUpsertComponent implements OnInit {

  // route param
  role: any = 'add';
  groupId: number;

  // title
  title: any = 'Add a Group';

  // form
  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  group: GroupsEntity = new GroupsEntity();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private groupsService: GroupsService,
              private dialog: MatDialog,
              private store: Store<any>,
              private matcher: FormErrorStateMatcher,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.role = this.router.url.split('/')[this.router.url.split('/').length - 1] === 'edit' ? 'edit' : 'add';
    this.route.params.subscribe(params => {
      this.groupId = params.id
    });


    console.log(this.role);
    console.log(this.groupId);

    if (this.role === 'add') {
      if (this.isFormError(this.nameFormControl)) {
        this.snackBar.open('Please check the name.', null, {duration: 2000});
      }
    }

    // router load 완료
    this.store.dispatch({type: ROUTER_LOADED});
  }

  submit() {
    this.group = new GroupsEntity();
    //this.group.name = 'jsleeAdded';
    //this.group.description = 'description111';

    this.groupsService.addGroup(this.group).subscribe(res => {
        if (res) {
          this.router.navigate(['../../groups'], {relativeTo: this.route});
        } else {
          this.openAlertDialog('Duplicated Group Name', 'Failed Get Group FileList', 'notice');
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Add Group', 'error');
        console.log('error', err);
      }
    );

    console.log('click submit.....');
  }

  isFormError(control: FormControl): boolean {
    return this.matcher.isErrorState(control, null);
  }

  getButtonName() {
    if (this.role === 'edit') {
      return 'Save';
    } else {
      return 'Add';
    }
  }

  backPage: any = () => {
    this.router.navigate(['../../groups'], {relativeTo: this.route});
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }

}
