import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserUpsertTableComponent} from './user-upsert-table.component';

describe('UserUpsertTableComponent', () => {
  let component: UserUpsertTableComponent;
  let fixture: ComponentFixture<UserUpsertTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserUpsertTableComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserUpsertTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
