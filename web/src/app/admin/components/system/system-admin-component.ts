import {
  AfterViewInit,
  Component,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import { DatePipe } from '@angular/common';
import {ROUTER_LOADED} from "../../../core/actions";
import {Store} from "@ngrx/store";
import {SystemService} from "../../services/system/system.service";
import {AlertComponent, DownloadService} from "../../../shared";
import {MatDialog} from "@angular/material";


@Component({
  selector: 'app-system-admin',
  templateUrl: './system-admin-component.html',
  styleUrls: ['./system-admin-component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class SystemAdminComponent implements OnInit, AfterViewInit {

  constructor(private store: Store<any>,
              private systemService: SystemService,
              private dialog: MatDialog,
              private downloadService: DownloadService) {

  }

  fileReader: any;

  ngOnInit() {
    this.store.dispatch({type: ROUTER_LOADED});

  }

  ngAfterViewInit(): void {
  }

  downloadHzcBackup() {

    this.systemService.downloadHzcBackup().subscribe(res => {
        if (res) {
          this.downloadService.downloadTxtFile(JSON.stringify(res), "HazelcastBackup_" + new Date());
        }
      },
      err => {
        this.openAlertDialog('Failed', 'Failed Download Hazelcast Backup Data', 'error');
        console.log('error', err);
      }
    );
  }

  uploadHzcInitData() {
    let file = (<HTMLInputElement>document.getElementById('file')).files[0];

    if (!file) {
      this.openAlertDialog('Failed', `File upload failed.`, 'error');
    } else if (!file.type.match('text.*')) {
      this.openAlertDialog('Failed', `File type miss match. Only use text file.`, 'error');
    } else {
      // 1. fileReader 객체 생성
      this.fileReader = new FileReader();
      // 2. 파일을 읽는다.
      this.fileReader.readAsText(file);
      // 3. 읽을 파일의 데이터를 this.readText에 저장한다.
      this.fileReader.onload = () => {
        if (this.fileReader.result !== null || this.fileReader.result.trim() === '') {

          let uploadData = {jsonStr :this.fileReader.result};

          (<HTMLInputElement>document.getElementById('file')).value = '';

          this.systemService.uploadHzcInitData(uploadData).subscribe(
            res => {

              this.openAlertDialog('Success', 'Success Upload File', 'success');
              (<HTMLInputElement>document.getElementById('file')).value = '';
            },
            err => {
              let msg = err.error.error;
              (<HTMLInputElement>document.getElementById('file')).value = '';

              if (msg) {
                this.openAlertDialog('Error', msg, 'error');
              } else {
                this.openAlertDialog('Error', `Server error. file upload failed.`, 'error');
              }
            });
        } else {
          (<HTMLInputElement>document.getElementById('file')).value = '';
          this.openAlertDialog('Failed', `Please enter a sentence.`, 'error');
        }
      }
    }
  }

  openAlertDialog(title, message, status) {
    let ref = this.dialog.open(AlertComponent);
    ref.componentInstance.header = status;
    ref.componentInstance.title = title;
    ref.componentInstance.message = message;
  }
}
