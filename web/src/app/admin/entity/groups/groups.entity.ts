import {PageParameters} from 'app/shared/entities/pageParameters.entity';

export class GroupsEntity extends PageParameters {

  id: number;
  ids: number[] = [];
  name: string;
  description: string;
  ownerId: any;
}
